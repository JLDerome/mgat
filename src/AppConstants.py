'''
AppConstants

Author: Jean-Luc Derome

Created: 28 Mars 2016
'''

import os
import time

AppVersionNumber = 0.9

#################################################################### Dates

strdays = "Mon  Tue  Wed  Thu  Fri  Sat Sun"
dictmonths = {'1':'Jan','2':'Feb','3':'Mar',
              '4':'Apr','5':'May','6':'Jun',
              '7':'Jul','8':'Aug','9':'Sep',
              '10':'Oct','11':'Nov','12':'Dec'}

year = time.localtime()[0]
month = time.localtime()[1]
day =time.localtime()[2]
todayDate = (str(year) +  "-" + dictmonths[str(month)] + "-" + str(day))

authorName = 'Jean-Luc Derome'
DBYearRevision = "2017"
DBDefaultName = 'GolfApp' + DBYearRevision + '.db'
BackupDirDefaultName = 'Backups'
ExportDirDefaultName = 'Exports'
ConvertDirectory2015 = 'DataImportFile2015'
DefaultDepartmentID = 1


AppHighlightBorderWidth = 2
AppRootDir=os.path.dirname(os.path.realpath(__file__)) 
AppmediaImagesPath = 'media/images/'
AppMainIcon = '{0}MyGolfIcon.ico'.format(AppmediaImagesPath)

#AppDefaultLocation = os.getenv("HOME") + "/MyGolfApp2016"
#AppDefaultLocation = os.getenv("HOME") + "/Dropbox/PyCharm/MyGolfApp2016"
if os.name == 'nt':
    AppDefaultLocation = os.path.expanduser('USERPROFILE') + "/Dropbox/MGAT_DB/MyGolfApp" + DBYearRevision
else:
    AppDefaultLocation = os.getenv("HOME") + "/Dropbox/MGAT_DB/MyGolfApp" + DBYearRevision
AppConfigFile = AppDefaultLocation + "/AppConfig.csv"
AppTitle = 'Myrtle Beach Golf Analysis Tool'
AppTitleAbbreviation = 'MGAT'
AppTitleFull = AppTitle + ' (' + AppTitleAbbreviation + ')'
AppWelcomeImg = '{0}MyGolfWelcome.png'.format(AppmediaImagesPath)
MenuImg = '{0}Golf-Ball-icon.png'.format(AppmediaImagesPath)
accessLevelList = ['Root', 'Maintenance', 'Update', 'View']

##################################################################### Convert 2015 DB
import2015GolferDataFilename = AppDefaultLocation + '/' + BackupDirDefaultName + '/' + ConvertDirectory2015 + '/' + 'MyGolfGolfers2015.csv'
import2015CoursesDataFilename = AppDefaultLocation + '/' + BackupDirDefaultName + '/' + ConvertDirectory2015 + '/' + 'MyGolfCourses2015.csv'
import2015TeesDataFilename = AppDefaultLocation + '/' + BackupDirDefaultName + '/' + ConvertDirectory2015 + '/' + 'MyGolfTees2015.csv'
import2015TeeDetailsDataFilename = AppDefaultLocation + '/' + BackupDirDefaultName + '/' + ConvertDirectory2015 + '/' + 'MyGolfTeeDetails2015.csv'
import2015ExpensesDataFilename = AppDefaultLocation + '/' + BackupDirDefaultName + '/' + ConvertDirectory2015 + '/' + 'MyGolfMBExpenses2015.csv'
import2015GamesDataFilename = AppDefaultLocation + '/' + BackupDirDefaultName + '/' + ConvertDirectory2015 + '/' + 'MyGolfGames2015.csv'
import2015GameDetailsDataFilename = AppDefaultLocation + '/' + BackupDirDefaultName + '/' + ConvertDirectory2015 + '/' + 'MyGolfGameDetails2015.csv'
import2015AddressesDataFilename = AppDefaultLocation + '/' + BackupDirDefaultName + '/' + ConvertDirectory2015 + '/' + 'MyGolfAddresses2015.csv'

##################################################################### Backup
#
#  Date manipulation
#
strtitle = "Calendar"
strdays = "Mon  Tue  Wed  Thu  Fri  Sat Sun"
dictmonths = {'1':'Jan','2':'Feb','3':'Mar',
              '4':'Apr','5':'May','6':'Jun',
              '7':'Jul','8':'Aug','9':'Sep',
              '10':'Oct','11':'Nov','12':'Dec'}
year = time.localtime()[0]
month = time.localtime()[1]
day =time.localtime()[2]
strdate = (str(year) +  "-" + dictmonths[str(month)] + "-" + str(day))


##################################################################### Backup
#
#  Export Tables Area (CSV format)
#
exportTeeFilename = 'MyGolfTees' + DBYearRevision + '.csv'
exportTeeDetailsFilename = 'MyGolfTeeDetails' + DBYearRevision + '.csv'
exportCourseFilename = 'MyGolfCourses' + DBYearRevision + '.csv'
exportGolfersFilename = 'MyGolfGolfers' + DBYearRevision + '.csv'
exportGamesFilename = 'MyGolfGames' + DBYearRevision + '.csv'
exportGameDetailsFilename = 'MyGolfGameDetails' + DBYearRevision + '.csv'
exportAddressFilename = 'MyGolfAddresses' + DBYearRevision + '.csv'
exportMBExpensesFilename = 'MyGolfMBExpenses' + DBYearRevision + '.csv'
exportLoginAccessFilename = 'MyGolfLoginAccess' + DBYearRevision + '.csv'
#exportTeeDetailsFilename = default_MyGolf_dir + "\\MyGolfTeeDetails.csv"
#exportCourseFilename = default_MyGolf_dir + "\\MyGolfCourses.csv"
#exportGolfersFilename = default_MyGolf_dir + "\\MyGolfGolfers.csv"
#exportGamesFilename = default_MyGolf_dir + "\\MyGolfGames.csv"
#exportGameDetailsFilename = default_MyGolf_dir + "\\MyGolfGameDetails.csv"
#exportAddressFilename = default_MyGolf_dir + "\\MyGolfAddresses.csv"
#exportMBExpensesFilename = default_MyGolf_dir + "\\MyGolfMBExpenses.csv"
baseScorecardDir = AppDefaultLocation + "/scorecards"
scannedScorecardDirName = AppDefaultLocation + "/scannedScorecards"

######################################################################
#
#  Image Constants
#
myImageFileFormats = [
    ('Portable Network Graphics','*.png'),
    ('Windows Bitmap','*.bmp'),
    ('JPEG / JFIF','*.jpg'),
    ('CompuServer GIF','*.gif'),
    ('Image files(ico)', '*.ico'),
    ('All files', '*')
    ]
nextRecImage = '{0}NextItemGreen.ico'.format(AppmediaImagesPath)
previousRecImage = '{0}PreviousItemGreen.ico'.format(AppmediaImagesPath)
lastRecImage = '{0}LastItemGreen.ico'.format(AppmediaImagesPath)
firstRecImage = '{0}FirstItemGreen.ico'.format(AppmediaImagesPath)
editRecImage = '{0}EditRecordGreen.ico'.format(AppmediaImagesPath)
editRecImageDisable = '{0}EditRecordGrey.ico'.format(AppmediaImagesPath)
editRecImageGIF = '{0}EditRecord.gif'.format(AppmediaImagesPath)
refreshRecImage = '{0}refreshCommandGreen.ico'.format(AppmediaImagesPath)
refreshRecImageDisable = '{0}refreshCommandGrey.ico'.format(AppmediaImagesPath)
findRecImage = '{0}findCommandGreen.ico'.format(AppmediaImagesPath)
saveRecImage = '{0}saveCommandGreen.ico'.format(AppmediaImagesPath)
saveRecImageDisable = '{0}saveCommandGrey.ico'.format(AppmediaImagesPath)
cancelCommandRecImage = '{0}cancelCommandGreen.ico'.format(AppmediaImagesPath)
cancelCommandRecImageDisable = '{0}cancelCommandGrey.ico'.format(AppmediaImagesPath)
deleteCommandRecImage = '{0}deleteCommandGreen.ico'.format(AppmediaImagesPath)
deleteCommandRecImageDisable = '{0}deleteCommandGrey.ico'.format(AppmediaImagesPath)
addCommandRecImage = '{0}addCommandGreen.ico'.format(AppmediaImagesPath)
addCommandRecImageDisable = '{0}addCommandGrey.ico'.format(AppmediaImagesPath)
duplicateRecImage = '{0}duplicateCommandGreen.ico'.format(AppmediaImagesPath)
duplicateRecImageDisable = '{0}duplicateCommandGrey.ico'.format(AppmediaImagesPath)
pullDownImage = '{0}pullDownGreen.ico'.format(AppmediaImagesPath)
removeFromSetCommandImage = '{0}removeFromSetCommand.ico'.format(AppmediaImagesPath)
undeleteCommandImage = '{0}undeleteGreen.png'.format(AppmediaImagesPath)
printerCommandImage = '{0}printerGreen.png'.format(AppmediaImagesPath)
exitCommandImage = '{0}exitCommandGreen.ico'.format(AppmediaImagesPath)
exitCommandImageDisable = '{0}exitCommandGrey.ico'.format(AppmediaImagesPath)
clearCommandImage = '{0}clearCommandGreen.ico'.format(AppmediaImagesPath)
clearCommandImageDisable = '{0}clearCommandGrey.ico'.format(AppmediaImagesPath)
viewCommandImage = '{0}viewGreen.png'.format(AppmediaImagesPath)
viewCommandImageDisable = '{0}viewGrey.png'.format(AppmediaImagesPath)
archiveCommandImage = '{0}archiveCommandGreen.ico'.format(AppmediaImagesPath)
archiveCommandImageDisable = '{0}archiveCommandGrey.ico'.format(AppmediaImagesPath)
show9Image = '{0}show9.png'.format(AppmediaImagesPath)
hide9Image = '{0}hide9.png'.format(AppmediaImagesPath)
addRoundCommandImage = '{0}addRoundCommandImage.png'.format(AppmediaImagesPath)
addRoundCommandImageDisable = '{0}addRoundCommandImageGrey.png'.format(AppmediaImagesPath)
scorecardActivateImage = '{0}activateScorecardImage.png'.format(AppmediaImagesPath)
scorecardActivateImageDisable = '{0}activateScorecardImageGrey.png'.format(AppmediaImagesPath)
theCheckmarkImage = '{0}checkMarkGreen.png'.format(AppmediaImagesPath)
theCheckmarkBoxedImage = '{0}checkMarkGreenBoxed.png'.format(AppmediaImagesPath)
theCheckmarkImageWhite = '{0}checkMarkWhite.png'.format(AppmediaImagesPath)
theEmptyCheckmarkImage = '{0}emptySquareForCheckMarkGreen.png'.format(AppmediaImagesPath)
theEmptyImage = '{0}empty.png'.format(AppmediaImagesPath)
previousPageImage = '{0}previousPage.png'.format(AppmediaImagesPath)
nextPageImage = '{0}nextPage.png'.format(AppmediaImagesPath)

iconWidth=15
iconHeight=15
buttonImageSizeHeight = 26
buttonImageSizeWidth = 26
AppPageNavButtonHeight = 30
AppPageNavButtonWidth = 40
offsetYFieldEntry = 3
AppDefaultAmountDecimal = 2 # default Value
AppDefaultAmountTotalDigit  = 9 # default Value
toolTipTime = 1500

######################################################################
#Welcome Display Configurations
#
welcomeDisplayWidth = 350
welcomeDisplayHeight = 350
welcomeLogoWidth = 140
welcomeLogoHeight = 125
appDialogPicWidth = 120
appDialogPicHeight = 138
appUnderConstructionPicWidth = 120
appUnderConstructionPicHeight = 138


welcomeLogoWidthLarge = 280
welcomeLogoHeightLarge = 250
######################################################################
#
#  App Login Data Details
loginDataFieldWidth = 40
######################################################################
#
#  Command Line Constants Configuration
commandButtonWidth = 7
commandButtonHeight = 5
commandLineButtonWidth = 7
commandLineButtonHeight = 3
gameListingButtonWidth = 14
gameListingButtonHeight = 6
######################################################################
#  App Menu
#
menuActiveForeground = 'dark green'
menuActiveBackground = 'yellow'

##################################################################### Dialog
warningPicWidth = 110
warningPicHeight = 100
warningImage = 'warningSymbol.png'
questionImage = 'questionSign.png'
underConstructionImage = 'under-construction.png'
questionPicWidth = 40
questionPicHeight = 60
errorimage = 'error.png'
eceLogoPicWidth = 120
eceLogoPicHeight = 138
AppMainLogo = 'MyGolfIcon.ico'
duplicationPicWidth = 75
duplicationPicHeight = 75
AppButtonSmallHeight = 17
AppButtonSmallWidth = 17
AppBorderWidthList = 3
AppButtonLargeHeight = 22
AppButtonLargeWidth = 22
######################################################################
#
#  Image Constants
#
myImageFileFormats = [
    ('Portable Network Graphics','*.png'),
    ('Windows Bitmap','*.bmp'),
    ('JPEG / JFIF','*.jpg'),
    ('CompuServer GIF','*.gif'),
    ('Image files(ico)', '*.ico'),
    ('All files', '*')
    ]


######################################################################
#
#  App Constants
#
#      FONTS
AppHighlightBorderWidth = 2
AppDeletedHighlightBorderWidth = 3
AppDeletedHighlightColor = 'red'
AppNegativeNumberColor = 'red'
AppDefaultForeground = 'darkgreen'
AppAccentBackground = 'grey81'
AppDefaultBackground = 'grey94'
AppTableHeaderBackground = 'grey80'
#AppDefaultEditBackground = 'lightyellow'
#AppDefaultEditBackground = 'yellow'
AppDefaultEditBackground = 'grey80'
AppHighlightThickness = 3
AppDefaultBorderWidth = 3
AppDeletedRecordBackground = 'pink'
AppDefaultDisabledForeground = 'grey45'
AppDefaultRequiredFieldBackground = 'pink'
AppStdForeground = 'black'
AppHighlightBackground = 'yellow'
pullDownAdjustmentWidth = 1
fontMinuscule=('Times', 2, 'normal')
fontMinusculeI=('Times', 2, 'italic')
fontMinusculeB=('Times', 2, 'bold')
fontTiny=('Times', 6, 'normal')
fontTinyI=('Times', 6, 'italic')
fontTinyB=('Times', 6, 'bold')
fontSmaller=('Times', 8, 'normal')
fontSmallerI=('Times', 8, 'italic')
fontSmallerB=('Times', 8, 'bold')
fontSmall=('Times', 10, 'normal')
fontSmallI=('Times', 10, 'italic')
fontSmallB=('Times', 10, 'bold')
fontAverage=('Times', 12, 'normal')
fontAverageI=('Times', 12, 'italic')
fontAverageB=('Times', 12, 'bold')
fontBig=('Times', 14, 'normal')
fontBigI=('Times', 14, 'italic')
fontBigB=('Times', 14, 'bold')
fontHuge=('Times', 16, 'normal')
fontHugeB=('Times', 16, 'bold')
fontHugeI=('Times', 16, 'italic')
fontLarger=('Times', 18, 'normal')
fontLargerI=('Times', 18, 'italic')
fontHuge2=('Times', 20, 'normal')
fontHuge2I=('Times', 20, 'italic')
fontMonstrous=('Times', 24, 'normal')
fontMonstrousI=('Times', 24, 'italic')
fontMonstrousB=('Times', 24, 'bold')
AppDefaultMenuFont = fontAverage
AppDefaultFontAvg = fontAverage
aMsg = ''' '''
fnta = ("Times", 10)
fnt = ("Times", 12)
fntc = ("Times", 12, 'bold')

MyGolfCurrentUSRate = 1.436643
MyGolfDefaultStartDate = 736006
MyGolfDefaultEndDate = 736014
MyGolfDefaultGolfer1ID = 1
MyGolfDefaultGolfer2ID = 3
MyGolfDefaultGolfer3ID = 2
MyGolfDefaultGolfer4ID = 4
MyGolfDefaultGolferMarker = 1

######################################################################
#
#  Personnel Window Specific Constants
golferPICPath = '{0}pictures'.format(AppmediaImagesPath)
golferDefaultPictureName = 'Default_Golfer_PIC.png'
golferDefaultPic = '{0}/{1}.jpg'.format(golferPICPath,golferDefaultPictureName)
appGolferPICDir = "{0}/{1}".format(AppRootDir, golferPICPath)
golferPicWidth = 85
golferPicHeight = 100


coursePICPath = '{0}course_logos'.format(AppmediaImagesPath)
courseDefaultPictureName = 'defaultCourseLogo.png'
courseDefaultPic = '{0}/{1}.jpg'.format(coursePICPath,courseDefaultPictureName)
appCoursePICDir = "{0}/{1}".format(AppRootDir, coursePICPath)
coursePicWidth = 80
coursePicHeight = 80

viewRecImage = '{0}viewBlue.png'.format(AppmediaImagesPath)
changeRecImage = '{0}changeRecordBlue.ico'.format(AppmediaImagesPath)
appDateWidth = 12
rankWidth = 8
employeeTypeWidth = 10
nameWidth = 35
rankCivilianList = ['Dr', 'Mr', 'Mrs', 'Miss'] 
rankArmyList = ['Gen','LGen','MGen','BGen','Col', 'LCol', 'Maj', 'Capt', 'Lt', '2-Lt', 'Ocdt', 'CWO', 'MWO', 'WO', 'Sgt', 'MCpl', 'Cpl', 'Pvt']
rankNavyList = ['Capt(N)', 'Cdr', 'LCdr', 'Lt(N)', 'SLt', 'A/Slt', 'NCdt', 'CPO1','CPO2', 'PO1','PO2','MS','AB','OS']
rankAirList = ['Gen','LGen','MGen','BGen','Col', 'LCol', 'Maj', 'Capt', 'Lt', 'Ocdt', 'CWO', 'MWO', 'WO', 'Sgt', 'MCpl', 'Cpl', 'Avr']
employeeTypeList = ['Civ', 'Mil-Air', 'Mil-Army', 'Mil-Navy']
employeeEmploymentList = ['Faculty', 'Technical Support', 'Administration', 'Researcher', 'PhD Student', 'Master Student', 'Undergrad 1', 'Undergrad 2', 'Undergrad 3', 'Undergrad 4']
employmentWidth = 20
positionList = ['Department Head', 'Professor', 'Lecturer', 'Technical Officer', 'Technician', 'Admin Assistant']
positionWidth = 20
extensionWidth = 10
officeWidth = 10
loginNameWidth = 25
passwordWidth = 25
accessWidth = 15
priWidth = 15


#####################################################################Games
gameCourseLongNameWidth = 38
gameTeeNameWidth = 24
gameHolesPlayedList = ['All', 'Front', 'Back']
gameHolesPlayedWidth = 6
gameRoundNumList = ['1', '2', '3', '4', '5']
gameRoundNumWidth = 3

#####################################################################Golfers
golferFNameWidth = 30
golferLNameWidth = 30
game9HoleBackground = 'yellow'



#####################################################################Courses
courseLongNameWidth = 50
courseShortNameWidth = 25
AppFullNameWidth = 20
appDescriptionWidth = 80
AppAmountWidth = 10
AppCurrencyWidth = 5
AppCurrencyList = ['CDN', 'US']
AppCountryList = ['Canada', 'United States']
courseTownWidth = 30
courseProv_stateWidth = 30
courseCountryWidth = 30
coursePc_zipWidth = 10
coursePhoneWidth = 15
courseWebsiteWidth = 40
courseStreet_numberWidth = 40
courseYearWidth = 6


#####################################################################Rounds
courseroundNameWidth = 20
courseroundRatingWidth = 10
courseroundSlopeWidth = 10
courseroundTypeWidth = 10
roundDetailsBorderWidth= 3
roundDetailsColumnCount = 23
roundDetailsRowHeading = ['Handicap', 'Yardage', 'Par', 'Score', 'Putts', 'Drv Length', 'Sand Trap']
roundDetailsColumnWeight = [20,3,3,3,3,3,3,3,3,3,9,3,3,3,3,3,3,3,3,3,9,15,15]
roundDetailsColumnWidth = [12,4,4,4,4,4,4,4,4,4,6,4,4,4,4,4,4,4,4,4,6,7,7]
roundDetailsColumnHeading = ['Hole','1','2','3','4','5','6','7','8','9','Out',
                           '10','11','12','13','14','15','16','17','18','In', 'Total','Net']

roundScoringStatsColumnHeading = ['Albatros', 'Eagles', 'Birdies', 'Pars', 'Bogies', 'D Bogies', 'T Bogies', 'Others']
roundScoringStatsColumnWeight = [4,4,4,4,4,4,4,4]
roundScoringStatsColumnWidth = [8,8,8,8,8,8,8,8]

roundFieldingStatsColumnHeading = ['GIRs', 'GIR %', 'Putts', '3 Putts +', 'Sand Traps', 'Sand Saves', 'Sand Save %']
roundFieldingStatsColumnWeight = [1,5,4,4,10,12,16]
roundFieldingStatsColumnWidth = [5,8,6,8,8,8,8]

roundFielding2StatsColumnHeading = ['Fairways', 'Left Miss', 'Right Miss', 'Fairway %', 'Average Drives']
roundFielding2StatsColumnWeight = [4,4,4,4,4]
roundFielding2StatsColumnWidth = [8,8,8,8,8]

roundOverAllStatsColumnHeading = ['Front','Back','Score', 'Index Before', 'Index After', 'Mileage (Kms)']
roundOverAllStatsColumnWeight = [2,2,2,4,4,8]
roundOverAllStatsColumnWidth = [6,6,6,8,8,8]

# gameStats.append(self.frontScoreTotal)        # Indice 0
# gameStats.append(self.backScoreTotal)         # Indice 1
# gameStats.append(self.scoreTotal)             # Indice 2
# gameStats.append(scoringStats[0])  # D Eagle    Indice 3
# gameStats.append(scoringStats[1])  # Eagle      Indice 4
# gameStats.append(scoringStats[2])  # Birdie     Indice 5
# gameStats.append(scoringStats[3])  # Par        Indice 6
# gameStats.append(scoringStats[4])  # Bogie      Indice 7
# gameStats.append(scoringStats[5])  # D Bogie    Indice 8
# gameStats.append(scoringStats[6])  # T Bogie    Indice 9
# gameStats.append(scoringStats[7])  # Others     Indice 10
# gameStats.append(self.GreensTotal)            # Indice 11
# gameStats.append(self.FairwaysTotal)          # Indice 12
# gameStats.append(drivingStats[0])  # left Miss  Indice 13
# gameStats.append(drivingStats[1])  # Right Miss Indice 14
# gameStats.append(drivingStats[2])  # Drv Yards  Indice 15
# gameStats.append(self.sandTrapTotal)          # Indice 16
# gameStats.append(self.SandSavesTotal)         # Indice 17
# gameStats.append(self.puttsTotal)             # Indice 18
# gameStats.append(self.putts3AndUp)            # Indice 19
# gameStats.append(self.yardageTotal)           # Indice 20

roundDetailsRelief = 'ridge'
roundDetailsHighlightRelief = 'groove'
roundDetailsFocusRelief = 'raised'
roundDetailsHeadingColor='yellow'
roundDetailsHandicapColor = 'bisque3'
roundDetailsYardageColor ='orange2'
roundDetailsParColor = 'khaki'
roundDetailsScoreColor = AppDefaultBackground
roundDetailsPuttsColor = AppDefaultBackground
roundDetailsDriveColor = AppDefaultBackground
roundDetailsSandTrapColor = AppDefaultBackground
roundDetailsWaterColor = AppDefaultBackground
roundDetailsGreensColor = 'grey85'
roundDetailsFairwaysColor = 'grey85'
roundDetailsSandSavesColor = 'grey85'
roundDetailsForegroundDisabledColor ='white'
roundDetailsBackgroundDisabledColor = 'grey60'
# gameStats.append(scoringStats[0])  # D Eagle    Indice 3
# gameStats.append(scoringStats[1])  # Eagle      Indice 4
# gameStats.append(scoringStats[2])  # Birdie     Indice 5
# gameStats.append(scoringStats[3])  # Par        Indice 6
# gameStats.append(scoringStats[4])  # Bogie      Indice 7
# gameStats.append(scoringStats[5])  # D Bogie    Indice 8
# gameStats.append(scoringStats[6])  # T Bogie    Indice 9
# gameStats.append(scoringStats[7])  # Others     Indice 10
pieColorsScoring = []
deagle_color = 'magenta'
eagle_color = 'blueviolet'
par_color ='green'
birdie_color = 'darkturquoise'
bogie_color = 'red'
dbogie_color = 'chocolate'
tbogie_color = 'blue'
others_color = 'brown'

# gameStats.append(scoringStats[0])  # D Eagle    Indice 3
# gameStats.append(scoringStats[1])  # Eagle      Indice 4
# gameStats.append(scoringStats[2])  # Birdie     Indice 5
# gameStats.append(scoringStats[3])  # Par        Indice 6
# gameStats.append(scoringStats[4])  # Bogie      Indice 7
# gameStats.append(scoringStats[5])  # D Bogie    Indice 8
# gameStats.append(scoringStats[6])  # T Bogie    Indice 9
# gameStats.append(scoringStats[7])  # Others     Indice 10
pieColorsScoring = [deagle_color, eagle_color, birdie_color, par_color,bogie_color, dbogie_color, tbogie_color, others_color]
# pieColorsScoring = [deagle_color, eagle_color, birdie_color, par_color,'red', 'chocolate', tbogie_color, others_color]
defaultBgColorReports='grey80'
defaultBgColorReportsAlternate='grey94'
defaultBgColorReportsLeader='powder blue'
golferNameWidth = 15
golferNameWeight = 10
statsRoundColumnWidth = 10
scoreCardMaxTeeSelection = 3


#####################################################################Tees
courseTeeNameWidth = 20
courseTeeRatingWidth = 10
courseTeeSlopeWidth = 10
courseTeeTypeWidth = 10
teeTypeList = ["Men's", "Women's", "Junior", "Senior"]
teeDetailsBorderWidth= 3
teeDetailsColumnCount = 22
teeDetailsRowHeading = ['Handicap', 'Yardage', 'Par']
teeDetailsColumnWeight = [15,3,3,3,3,3,3,3,3,3,9,3,3,3,3,3,3,3,3,3,9,15]
teeDetailsColumnWidth = [9,4,4,4,4,4,4,4,4,4,6,4,4,4,4,4,4,4,4,4,6,7]
teeDetailsColumnHeading = ['Hole','1','2','3','4','5','6','7','8','9','Out',
                           '10','11','12','13','14','15','16','17','18','In', 'Total']
teeDetailsRelief = 'ridge'
teeDetailsHighlightRelief = 'groove'
teeDetailsHeadingColor='yellow'
teeDetailsHandicapColor = 'bisque3'
teeDetailsYardageColor ='orange2'
teeDetailsParColor = 'khaki'
teeDetailsForegroundDisabledColor ='white'
teeDetailsBackgroundDisabledColor = 'grey60'


######################################################################
#
#  Date Constant
dateEntryFieldWidth = 20

######################################################################
#
# Record Navigation Specific Constants
recordNumWidth = 20
######################################################################
#
# Parts Window 
#
partsDescLenght = 70
partsFuncLenght = 35
partsBasicLenght = 30
partsNomenLenght = 20
partsDepartmentLenght = 23
partsQtyLenght = 6
partsLocLenght = 5
partsValueLenght = 9
partsMultiplierLenght = 2
partsToleranceLenght = 4
partsSizeLenght = 10
partsClassLenght = 2
partsUILenght = 3
partsNSN1Lenght = 4
partsNSN2Lenght = 2
partsNSN3Lenght = 3
partsNSN4Lenght = 4
partsSNCLenght = 2
partsSSILenght = 3
#partsSupplierLenght = 55
partsSupplierNumLenght = 12
partsCatNumLenght = 25
partsPriceLenght = 10
######################################################################
#
# Departments Window 
#
departmentNameLenght = 60
departmentMidNameLenght = 30
departmentShortNameLenght = 15
departmentSCAListLenght = 30
departmentSecurityLenght = 9
######################################################################
#
# Suppliers Window 
#
suppliersNameLenght = 55
suppliersNumberLenght = 12
