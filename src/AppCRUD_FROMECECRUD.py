'''
ECE Shop Application API

    Provides all DB interface for access to the SHOP Database

Author: Jean-Luc Derome

Revision:
    Creation: 
'''

import sqlite3 as sql
import os
import csv
from MyGolfApp2016.AppConstants import *
from MyGolfApp2016.AppProcFROMECE import *

db=None
cursor = None
lidGolfers = 0

######################################################
# ECE Personnel CRUD
#            cursor.execute('''CREATE TABLE Personnel(
#     0         personnelID INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
#     1         personnelFNAME          CHAR(30)    NOT NULL,
#     2         personnelLNAME          CHAR(30)    NOT NULL,
#     3         personnelType           CHAR(4),
#     4         personnelSalutation     CHAR(6),
#     5         personnelPIC_LOC        CHAR(100),
#     6         pri_SN                  CHAR(11),
#     7         officeNumber            CHAR(9),
#     8         extension               CHAR(5),
#     9         ECEWorkGroup            CHAR(30),
#     10        title_Position          CHAR(30),
#     11        loginName               CHAR(20),
#     12        password                CHAR(30),
#     13        access_Level            CHAR(15),
#     14        lastModified            INTEGER     NOT NULL,    
#     15        updaterID               INTEGER     NOT NULL
#              );''')

def insertPersonnelRoot(personnelFNAME,personnelLNAME,personnelType,personnelSalutation,
                    personnelPIC_LOC,pri_SN,officeNumber,extension,ECEWorkGroup,title_Position,
                    loginName,password,access_Level,lastModified,updaterID):
    # id is declared but is auto increment
    initDB(getDBLocationFullName())
    query = ''' 
    insert into Personnel 
    (personnelFNAME, personnelLNAME,personnelType,personnelSalutation,
     personnelPIC_LOC,pri_SN,officeNumber,extension,ECEWorkGroup,title_Position,
     loginName,password,access_Level,lastModified,updaterID)
    values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'''
    cursor.execute(query,(personnelFNAME, personnelLNAME,personnelType,personnelSalutation,
                          personnelPIC_LOC,pri_SN,officeNumber,extension,ECEWorkGroup,title_Position,
                          loginName,password,access_Level,lastModified,updaterID))
    closeDB()
    
def updatePersonnel(personnelID, personnelFNAME, personnelLNAME, personnelType, personnelSalutation,
                    personnelPIC_LOC, pri_SN, officeNumber, extension, ECEWorkGroup, title_Position,
                    loginName , password , access_Level, lastModified, updaterID):
    initDB(getDBLocationFullName())
    query = ''' update Personnel set personnelFNAME = '{1}', personnelLNAME = '{2}', personnelType = '{3}', personnelSalutation = '{4}',
                personnelPIC_LOC = '{5}', pri_SN = '{6}', officeNumber = '{7}', extension = '{8}', ECEWorkGroup = '{9}', title_Position = '{10}',
                loginName = '{11}', password = '{12}', access_Level = '{13}', lastModified = '{14}', updaterID = '{15}'
                where personnelID = {0} '''.format(personnelID, personnelFNAME, personnelLNAME, personnelType, personnelSalutation,
                                                   personnelPIC_LOC, pri_SN, officeNumber, extension, ECEWorkGroup, title_Position,
                                                   loginName , password , access_Level, lastModified, updaterID)
    cursor.execute(query)
    closeDB()
    
def updatePersonnelV1(personnelFNAME, personnelLNAME, personnelType, personnelSalutation,
                    personnelPIC_LOC, pri_SN, officeNumber, extension, ECEWorkGroup, title_Position,
                    loginName, password, access_Level, lastModified, updaterID, personnelID):
    initDB(getDBLocationFullName())
    query = ''' update Personnel set personnelFNAME = {0} , personnelLNAME = {1}, personnelType = {2}, personnelSalutation = {3},
                    personnelPIC_LOC = {4}, pri_SN = {5}, officeNumber = {6}, extension = {7}, ECEWorkGroup = {8}, title_Position = {9},
                    loginName = {10}, password = {11}, access_Level = {12}, lastModified = {13}, updaterID = {14} where personnelID = {15} '''.format(personnelFNAME,
                                    personnelLNAME, personnelType, personnelSalutation, personnelPIC_LOC, pri_SN,officeNumber,
                                    extension, ECEWorkGroup, title_Position, loginName, password, access_Level, lastModified, updaterID, personnelID)
    cursor.execute(query)
    closeDB()
    
def insertPersonnel(personnelFNAME,personnelLNAME,personnelType,personnelSalutation,
                    personnelPIC_LOC,pri_SN,officeNumber,extension,ECEWorkGroup,title_Position,
                    loginName, password, access_Level, lastModified,updaterID):
    # id is declared but is auto increment
    initDB(getDBLocationFullName())
    query = ''' 
    insert into Personnel 
    (personnelFNAME, personnelLNAME,personnelType,personnelSalutation,
     personnelPIC_LOC,pri_SN,officeNumber,extension,ECEWorkGroup,title_Position,
     loginName, password, access_Level, lastModified,updaterID)
    values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'''
    cursor.execute(query,(personnelFNAME, personnelLNAME,personnelType,personnelSalutation,
                          personnelPIC_LOC,pri_SN,officeNumber,extension,ECEWorkGroup,title_Position,
                          loginName, password, access_Level, lastModified,updaterID))
    closeDB()
    
def exportCSV_Personnel(fileName):
    initDB(getDBLocationFullName())
    query = '''select * from Personnel'''
    data = cursor.execute(query).fetchall()
    with open(fileName, 'w', newline='') as fout:
        for i in range(len(data)):
            csv.writer(fout).writerow(data[i])
    closeDB()

def importCSV_Personnel(fileName):
    # Delete default user automatically created (use for new DB)
    # Root user is in the backup.  Technique only use for this Table
    deletePersonnel(1)
    initDB(getDBLocationFullName())
    with open(fileName, 'r') as fin:
        reader = csv.reader(fin)
        for row in reader:
            query = '''
            insert into Personnel (personnelID, personnelFNAME, personnelLNAME, personnelType, personnelSalutation,
                    personnelPIC_LOC, pri_SN, officeNumber, extension, ECEWorkGroup, title_Position,
                    loginName , password , access_Level, lastModified, updaterID)
            values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'''
            cursor.execute(query, (row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7],row[8],row[9],
                                   row[10],row[11],row[12],row[13],row[14],row[15]))
    closeDB()


def getRankFullName(personnelID):
    #
    #  Does not include main ROOT
    #
    initDB(getDBLocationFullName())
    query = '''  
    select personnelFNAME, personnelLNAME, personnelSalutation from Personnel where personnelID = {0}'''.format(personnelID)
    data = cursor.execute(query).fetchall()
    return "{0} {1} {2}".format(data[0][2],data[0][0],data[0][1])
    closeDB()

def getFullName(personnelID):
    #
    #  Does not include main ROOT
    #
    initDB(getDBLocationFullName())
    query = '''  
    select personnelFNAME, personnelLNAME from Personnel where personnelID = {0}'''.format(personnelID)
    data = cursor.execute(query).fetchall()
    return "{0} {1}".format(data[0][0],data[0][1])
    closeDB()


def getPersonnel():
    #
    #  Does not include main ROOT
    #
    initDB(getDBLocationFullName())
    query = '''  
    select * from Personnel where loginName != 'root' ORDER BY personnelLNAME'''
    return cursor.execute(query).fetchall()
    closeDB()

def getLoginInfo(loginName):
    initDB(getDBLocationFullName())
    query = '''  
    select password, access_Level, personnelID  from Personnel where loginName = '{0}' '''.format(loginName)
    return cursor.execute(query).fetchall()
    closeDB()

def getPersonnelROOT():
    initDB(getDBLocationFullName())
    query = '''  
    select * from Personnel ORDER BY personnelLNAME'''
    return cursor.execute(query).fetchall()
    closeDB()

def deletePersonnel(personnelID):
    initDB(getDBLocationFullName())
    query = '''
    delete from Personnel where personnelID = '{0}' '''.format(personnelID)
    cursor.execute(query)
    closeDB()
    
######################################################
# ECE Departments CRUD
#            '''CREATE TABLE Departments(
#               departmentsID           INTEGER PRIMARY KEY AUTOINCREMENT  NOT NULL,
#               departmentsCode         CHAR(3),  NOT NULL,
#               departmentsNAME         CHAR(60)  NOT NULL,
#               departmentsMidSize      CHAR(20),
#               departmentsShort        CHAR(10),
#               departmentsDAList       CHAR(60),
#               departmentsSecurity     CHAR(8),
#               lastModified            INTEGER,
#               updaterID               INTEGER
#              );''')
def getDepartments():
    #
    #  
    #
    initDB(getDBLocationFullName())
    query = '''  
    select * from Departments ORDER BY departmentsNAME'''
    return cursor.execute(query).fetchall()
    closeDB()
    
def getDepartmentsDictionaryInfo():
    #
    #  
    #
    initDB(getDBLocationFullName())
    query = '''  
    select departmentsID, departmentsShort, departmentsCode from Departments ORDER by departmentsMidSize'''
    temp = cursor.execute(query).fetchall()
    returnList = []
    for i in range(len(temp)):
        newItem = (temp[i][0], '''{0} ({1})'''.format(temp[i][1],temp[i][2]))
        returnList.append(newItem)
    return returnList
    closeDB()

def getDepartmentsCode(departmentsID):
    #
    #  
    #
    print("Departments ID: ", departmentsID)
    initDB(getDBLocationFullName())
    query = '''  
    select departmentsCode from Departments where departmentsID = {0} '''.format(departmentsID)
    return cursor.execute(query).fetchall()
    closeDB()
    
def getDepartmentsIDTUTFromDeptCode(departmentsCode):
    #       
    #  Procedure to convert from D Database
    #
    query = '''  
    select departmentsID from Departments where departmentsCode = '{0}' '''.format(departmentsCode)
    return cursor.execute(query).fetchall()
      
def deleteDepartments(departmentsID):
    initDB(getDBLocationFullName())
    query = '''
    delete from Departments where departmentsID = '{0}' '''.format(departmentsID)
    cursor.execute(query)
    closeDB()

def insertDepartments(departmentsCode, departmentsNAME, departmentsMidSize,departmentsShort,
                       departmentsDAList, departmentsSecurity, lastModified, updaterID):
    initDB(getDBLocationFullName())
    query = '''
            insert into Departments (departmentsCode, departmentsNAME, departmentsMidSize,
                                     departmentsShort, departmentsDAList, departmentsSecurity, lastModified, updaterID)
            values (?,?,?,?,?,?,?,?)'''
    cursor.execute(query, (departmentsCode, departmentsNAME, departmentsMidSize, departmentsShort,
                            departmentsDAList, departmentsSecurity, lastModified, updaterID))
    closeDB()
    
def updateDepartments(departmentsID, departmentsCode, departmentsNAME, departmentsMidSize, departmentsShort,
                      departmentsDAList, departmentsSecurity, lastModified, updaterID):
    initDB(getDBLocationFullName())
    query = ''' update Departments set departmentsCode = '{1}', departmentsNAME = '{2}', departmentsMidSize = '{3}', departmentsShort = '{4}',
                departmentsDAList = '{5}', departmentsSecurity = '{6}', lastModified = '{7}', updaterID = '{8}' 
                where departmentsID = {0} '''.format(departmentsID, departmentsCode, departmentsNAME, departmentsMidSize, departmentsShort,
                                                   departmentsDAList, departmentsSecurity, lastModified, updaterID)
    cursor.execute(query)
    closeDB()

def exportCSV_Departments(fileName):
    initDB(getDBLocationFullName())
    query = '''select * from Departments'''
    data = cursor.execute(query).fetchall()
    with open(fileName, 'w', newline='') as fout:
        for i in range(len(data)):
            csv.writer(fout).writerow(data[i])
    closeDB()

def importCSV_Departments(fileName):
    initDB(getDBLocationFullName())
    with open(fileName, 'r') as fin:
        reader = csv.reader(fin)
        for row in reader:
            query = '''
            insert into Departments (departmentsID, departmentsCode, departmentsNAME, departmentsMidSize,
                                     departmentsShort, departmentsDAList, departmentsSecurity, lastModified, updaterID)
            values (?,?,?,?,?,?,?,?,?)'''
            cursor.execute(query, (row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7],row[8]))
#            query = '''
#            insert into Departments (departmentsCode, departmentsNAME, departmentsSecurity, departmentsMidSize,
#                                     departmentsShort, departmentsDAList, lastModified, updaterID)
#            values (?,?,?,?,?,?,?,?)'''
#            cursor.execute(query, (row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7]))
    closeDB()

######################################################
# ECE Parts CRUD
#  '''CREATE TABLE Parts(
#        0        partID                  INTEGER PRIMARY KEY AUTOINCREMENT  NOT NULL,
#        1        BASIC      CHAR(20),
#        2        VALUE      CHAR(3),
#        3        VALUED     CHAR(3),
#        4        MULT       CHAR(1),
#        5        TOLE       CHAR(5),
#        6        SIZE       CHAR(7),
#        7        DESCR      CHAR(40),
#        8        NSN1       CHAR(4),
#        9        NSN2       CHAR(2),
#        10       NSN3       CHAR(3),
#        11       NSN4       CHAR(4),
#        12       ORDER_NUM  CHAR(20),
#        13       SUPP_NUM   CHAR(10),
#        14       ITEM_NUM   CHAR(12),
#        15       UI         CHAR(2),
#        16       SNC        CHAR(1),
#        17       CLASS      CHAR(1),
#        18       PRICE      CHAR(5),
#        19       PRICED     CHAR(2),
#        20       SUP        CHAR(1),
#        21       SSI        CHAR(2),
#        22       RETAIL     CHAR(9),
#        23       QUANT      CHAR(4),
#        24       QMIN       CHAR(4),
#        25       QORDER     CHAR(4),
#        26       QUSE       CHAR(4),
#        27       QTEMP      CHAR(4),
#        28       FUNCTION   CHAR(20),
#        29       DESCR2     CHAR(60),
#        30       NOMEN      CHAR(15),
#        31       ROW        CHAR(2)
#        32       SIDE       CHAR(1)
#        33       SHELF      CHAR(1)
#        34       SECTION    CHAR(3)
#        35       DEPARTMENT CHAR(2),
#        36       S_NAME     CHAR(30), 
#        37       lastModified  INTEGER,
#        38       updaterID     INTEGER,
#        39       suppliersID   INTEGER,
#        40       departmentsID   INTEGER
#
def deleteParts(partID):
    initDB(getDBLocationFullName())
    query = '''
    delete from Parts where partID = '{0}' '''.format(partID)
    cursor.execute(query)
    closeDB()


def getParts():
    #
    #  Retieves all parts  
    #
    initDB(getDBLocationFullName())
    query = '''  
    select * from Parts ORDER BY BASIC'''
    return cursor.execute(query).fetchall()
    closeDB()
    
def getPartsID(BASIC,VALUE1, VALUE2, DESCR,DESCR2,FUNCTION,NOMEN,lastModified,updaterID):
    #
    #  
    #
    initDB(getDBLocationFullName())
    query = '''  
    select partID from Parts where BASIC = '{0}' and VALUE = '{1}' and VALUED = '{2}' and  DESCR = '{3}' and DESCR = '{4}' and FUNCTION = '{5}' and
                                   NOMEN = '{6}' and lastModified = '{7}' and updaterID = '{8}' '''.format(BASIC,VALUE1,VALUE2,DESCR,DESCR2,
                                                                                                           FUNCTION,NOMEN,lastModified,updaterID)
    return cursor.execute(query).fetchall()
    closeDB()


def updateParts(partID,BASIC,VALUE,VALUED,MULT,TOLE,SIZE,DESCR,NSN1,NSN2,NSN3,NSN4,
                ORDER_NUM,SUPP_NUM,ITEM_NUM,UI,SNC,CLASS,PRICE,PRICED,SUP,SSI,RETAIL,
                QUANT,QMIN,QORDER,QUSE,QTEMP,FUNCTION,DESCR2,NOMEN,ROW,
                SIDE,SHELF,SECTION,DEPARTMENT,S_NAME,lastModified,updaterID, suppliersID, departmentsID):
    initDB(getDBLocationFullName())
    query = ''' update Parts set BASIC = '{1}', VALUE = '{2}', VALUED = '{3}', MULT = '{4}', TOLE = '{5}',
                SIZE = '{6}', DESCR = '{7}', NSN1 = '{8}', NSN2 = '{9}', NSN3 = '{10}', NSN4 = '{11}', 
                ORDER_NUM = '{12}', SUPP_NUM = '{13}', ITEM_NUM = '{14}', UI = '{15}', SNC = '{16}', CLASS = '{17}',
                PRICE = '{18}', PRICED = '{19}', SUP = '{20}', SSI = '{21}', RETAIL = '{22}', 
                QUANT = '{23}', QMIN = '{24}', QORDER = '{25}', QUSE = '{26}', QTEMP = '{27}', FUNCTION = '{28}',
                DESCR2 = '{29}', NOMEN = '{30}', ROW = '{31}', SIDE = '{32}', SHELF = '{33}', SECTION = '{34}',
                DEPARTMENT = '{35}', S_NAME = '{36}', lastModified = '{37}', updaterID  = '{38}', suppliersID = '{39}',  
                departmentsID = '{40}' where partID = {0} '''.format(partID,BASIC,VALUE,VALUED,MULT,TOLE,SIZE,DESCR,NSN1,NSN2,NSN3,NSN4,
                ORDER_NUM,SUPP_NUM,ITEM_NUM,UI,SNC,CLASS,PRICE,PRICED,SUP,SSI,RETAIL,
                QUANT,QMIN,QORDER,QUSE,QTEMP,FUNCTION,DESCR2,NOMEN,ROW,
                SIDE,SHELF,SECTION,DEPARTMENT,S_NAME,lastModified,updaterID,suppliersID, departmentsID)
    cursor.execute(query)
    closeDB()

def insertParts(BASIC,VALUE,VALUED,MULT,TOLE,SIZE,DESCR,NSN1,NSN2,NSN3,NSN4,
                ORDER_NUM,SUPP_NUM,ITEM_NUM,UI,SNC,CLASS,PRICE,PRICED,SUP,SSI,RETAIL,
                QUANT,QMIN,QORDER,QUSE,QTEMP,FUNCTION,DESCR2,NOMEN,ROW,
                SIDE,SHELF,SECTION,DEPARTMENT,S_NAME,lastModified,updaterID, suppliersID, departmentsID):
    initDB(getDBLocationFullName())
    query = '''
            insert into Parts (BASIC,VALUE,VALUED,MULT,TOLE,SIZE,DESCR,NSN1,NSN2,NSN3,NSN4,
                ORDER_NUM,SUPP_NUM,ITEM_NUM,UI,SNC,CLASS,PRICE,PRICED,SUP,SSI,RETAIL,
                QUANT,QMIN,QORDER,QUSE,QTEMP,FUNCTION,DESCR2,NOMEN,ROW,
                SIDE,SHELF,SECTION,DEPARTMENT,S_NAME,lastModified,updaterID,suppliersID, departmentsID)
            values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'''
    cursor.execute(query, (BASIC,VALUE,VALUED,MULT,TOLE,SIZE,DESCR,NSN1,NSN2,NSN3,NSN4,
                ORDER_NUM,SUPP_NUM,ITEM_NUM,UI,SNC,CLASS,PRICE,PRICED,SUP,SSI,RETAIL,
                QUANT,QMIN,QORDER,QUSE,QTEMP,FUNCTION,DESCR2,NOMEN,ROW,
                SIDE,SHELF,SECTION,DEPARTMENT,S_NAME,lastModified,updaterID, suppliersID, departmentsID))
    closeDB()

def insertPartsForTUTImport(BASIC,VALUE,VALUED,MULT,TOLE,SIZE,DESCR,NSN1,NSN2,NSN3,NSN4,
                ORDER_NUM,SUPP_NUM,ITEM_NUM,UI,SNC,CLASS,PRICE,PRICED,SUP,SSI,RETAIL,
                QUANT,QMIN,QORDER,QUSE,QTEMP,FUNCTION,DESCR2,NOMEN,ROW,
                SIDE,SHELF,SECTION,DEPARTMENT,S_NAME,lastModified,updaterID, suppliersID,
                departmentsID):
#    initDB(getDBLocationFullName())
    query = '''
            insert into Parts (BASIC,VALUE,VALUED,MULT,TOLE,SIZE,DESCR,NSN1,NSN2,NSN3,NSN4,
                ORDER_NUM,SUPP_NUM,ITEM_NUM,UI,SNC,CLASS,PRICE,PRICED,SUP,SSI,RETAIL,
                QUANT,QMIN,QORDER,QUSE,QTEMP,FUNCTION,DESCR2,NOMEN,ROW,
                SIDE,SHELF,SECTION,DEPARTMENT,S_NAME,lastModified,updaterID,suppliersID,
                departmentsID)
            values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'''
    cursor.execute(query, (BASIC,VALUE,VALUED,MULT,TOLE,SIZE,DESCR,NSN1,NSN2,NSN3,NSN4,
                ORDER_NUM,SUPP_NUM,ITEM_NUM,UI,SNC,CLASS,PRICE,PRICED,SUP,SSI,RETAIL,
                QUANT,QMIN,QORDER,QUSE,QTEMP,FUNCTION,DESCR2,NOMEN,ROW,
                SIDE,SHELF,SECTION,DEPARTMENT,S_NAME,lastModified,updaterID,suppliersID,
                departmentsID))
#    closeDB()


def findParts(BASIC, DESCR, DESCR2, NOMEN, FUNCTION):
    initDB(getDBLocationFullName())
    query = '''
            select * from Parts where BASIC like '%{0}%' and DESCR  like '%{1}%' and DESCR2  like '%{2}%' and NOMEN  like '%{3}%' 
             and FUNCTION like '%{4}%' ORDER BY BASIC'''.format(BASIC, DESCR, DESCR2, NOMEN, FUNCTION)
    return cursor.execute(query).fetchall()
    closeDB()

def exportCSV_Parts(fileName):
    initDB(getDBLocationFullName())
    query = '''select * from Parts'''
    data = cursor.execute(query).fetchall()
    with open(fileName, 'w', newline='') as fout:
        for i in range(len(data)):
            csv.writer(fout).writerow(data[i])
    closeDB()

def updatePartsDepartmentID(partsID, departmentsID):
    query = ''' update Parts set  departmentsID  = '{1}' where partID = {0} '''.format(partsID, departmentsID) 
    cursor.execute(query)      

def crossRefPartsDepartments():
    allParts = getParts()
    initDB(getDBLocationFullName())
    for each in range(len(allParts)):
        deptID = getDepartmentsIDTUTFromDeptCode(allParts[each][35])
        if len(deptID) > 0:
            updatePartsDepartmentID(allParts[each][0], deptID[0][0]) 
    closeDB()
    
def updatePartsSupplierID(partsID, suppliersID):
    query = ''' update Parts set  suppliersID  = '{1}' where partID = {0} '''.format(partsID, suppliersID) 
    cursor.execute(query)      

def crossRefPartsSupplier():
    allParts = getParts()
    initDB(getDBLocationFullName())
    for each in range(len(allParts)):
        supID = getSuppliersIDTUTFromSNum(allParts[each][13])
        if len(supID) > 0:
            updatePartsSupplierID(allParts[each][0], supID[0][0]) 
    closeDB()

def importTUT_Parts(fileName):
    #
    #  Import of Tech Shop Parts DB (D Language)
    #  Reads a .TUT file obtain from Pierre
    #
    initDB(getDBLocationFullName())
    fieldSizes = (20,3,3,1,5,7,40,4,2,3,4,20,10,12,2,1,1,5,2,1,2,9,4,4,4,4,4,20,60,15,2,1,1,3,2,30)
    #
    #  File is organized in lenght of fields (above)
    #  Read each line and build the individual fields
    #  Add them individually.
    #    
    with open(fileName, 'r') as fin:
        array=[]
        for lines in fin:
            array=[]
            array.append(lines)
            idx = 0
            aField = ''
            aFieldArray = []
            for eachFieldSize in fieldSizes:
                for idx2 in range(eachFieldSize):
                    aField='''{0}{1}'''.format(aField, array[0][idx])
                    idx = idx + 1   
                aField = aField.rstrip()   # Remove trailing white spaces             
                aFieldArray.append(aField) # Create array of fields
                aField = ''
                
#            ID = getSuppliersIDTUTFromSNum(aFieldArray[12])
#            if len(ID) > 1:
#                supID = ID[0][0]
#            else:
            supID = ''
            deptID = ''

            insertPartsForTUTImport(aFieldArray[0],aFieldArray[1],aFieldArray[2],aFieldArray[3],aFieldArray[4],aFieldArray[5],aFieldArray[6],
                        aFieldArray[7],aFieldArray[8],aFieldArray[9],aFieldArray[10],aFieldArray[11],aFieldArray[12],aFieldArray[13],
                        aFieldArray[14],aFieldArray[15],aFieldArray[16],aFieldArray[17],aFieldArray[18],aFieldArray[19],aFieldArray[20],
                        aFieldArray[21],aFieldArray[22],aFieldArray[23],aFieldArray[24],aFieldArray[25],aFieldArray[26],aFieldArray[27],
                        aFieldArray[28],aFieldArray[29],aFieldArray[30],aFieldArray[31],aFieldArray[32],aFieldArray[33],aFieldArray[34],
                        aFieldArray[35],convertDateStringToOrdinal(todayDate),'1', supID,deptID)
    closeDB()

######################################################
# ECE Suppliers CRUD
#('''CREATE TABLE Suppliers(
#       0        suppliersID   INTEGER PRIMARY KEY AUTOINCREMENT  NOT NULL,
#       1        S_NUM         CHAR(10),
#       2        S_NAME        CHAR(30),
#       3        S_ADDR1       CHAR(30),
#       4        S_ADDR2       CHAR(30),
#       5        S_ADDR3       CHAR(30),
#       6        S_ADDR4       CHAR(30),
#       7        S_ADDR5       CHAR(30),
#       8        S_PHONE       CHAR(13),
#       9        S_FAX         CHAR(13),
#       10       S_CNTC        CHAR(20),
#       11       MAN_NUM       CHAR(10),
#       12       PHONE_AREA    CHAR(3),
#       13       PHONE_EXCH    CHAR(3),
#       14       PHONE_LOCAL   CHAR(4),
#       15       FAX_AREA      CHAR(3),
#       16       FAX_EXCH      CHAR(3),
#       17       FAX_LOCAL     CHAR(4),
#       18       DEPARTMENT    CHAR(2),
#       19       ACTIVE        CHAR(1),             
#       20       lastModified  INTEGER,
#       21       updaterID     INTEGER
#             );''')
def deleteSuppliers(suppliersID):
    initDB(getDBLocationFullName())
    query = '''
    delete from Suppliers where suppliersID = '{0}' '''.format(suppliersID)
    cursor.execute(query)
    closeDB()
    
def findSuppliers(S_NUM,S_NAME,S_ADDR1,S_ADDR2,S_ADDR3,S_ADDR4,S_ADDR5,S_PHONE,S_FAX,S_CNTC,
                  MAN_NUM,PHONE_AREA,PHONE_EXCH,PHONE_LOCAL,FAX_AREA,FAX_EXCH,FAX_LOCAL,
                  DEPARTMENT,ACTIVE):
    initDB(getDBLocationFullName())
    query = '''
            select * from Suppliers where S_NUM like '%{0}%' AND S_NAME like '%{1}%' AND S_ADDR1 like '%{2}%' AND S_ADDR2 like '%{3}%' AND
                                      S_ADDR3 like '%{4}%' AND S_ADDR4 like '%{5}%' AND S_ADDR5 like '%{6}%' AND S_PHONE like '%{7}%' AND
                                      S_FAX like '%{8}%' AND S_CNTC like '%{9}%' AND MAN_NUM like '%{10}%' AND PHONE_AREA like '%{11}%' AND
                                      PHONE_EXCH like '%{12}%' AND PHONE_LOCAL like '%{13}%' AND FAX_AREA like '%{14}%' AND FAX_EXCH like '%{15}%' AND
                                      FAX_LOCAL like '%{16}%' AND DEPARTMENT like '%{17}%' AND ACTIVE like '%{18}%' 
                                      ORDER BY S_NAME '''.format(S_NUM,S_NAME,S_ADDR1,S_ADDR2,S_ADDR3,S_ADDR4,S_ADDR5,S_PHONE,S_FAX,S_CNTC,
                                                                 MAN_NUM,PHONE_AREA,PHONE_EXCH,PHONE_LOCAL,FAX_AREA,FAX_EXCH,FAX_LOCAL,
                                                                 DEPARTMENT,ACTIVE)
    return cursor.execute(query).fetchall()
    closeDB()

def getSuppliers():
    #
    #  Retieves all parts  
    #
    initDB(getDBLocationFullName())
    query = '''  
    select * from Suppliers ORDER BY S_NAME'''
    return cursor.execute(query).fetchall()
    closeDB()
    
def getSuppliersIDTUTFromSNum(S_NUM):
    #
    #  
    #
    query = '''  
    select suppliersID from Suppliers where S_NUM = '{0}' '''.format(S_NUM)
    return cursor.execute(query).fetchall()

def getSuppliersDictionaryInfo():
    #
    #  
    #
    initDB(getDBLocationFullName())
    query = '''  
    select SuppliersID, S_NAME from Suppliers ORDER by S_NAME'''
    return cursor.execute(query).fetchall()
    closeDB()
    
def getSuppliersID(S_NUM,S_NAME,S_ADDR1,S_ADDR2,S_PHONE,S_FAX,S_CNTC,MAN_NUM,
                   DEPARTMENT,ACTIVE,lastModified,updaterID):
    #
    #  
    #
    initDB(getDBLocationFullName())
    query = '''  
    select suppliersID from Suppliers where S_NUM = '{0}' and S_NAME = '{1}' and S_ADDR1 = '{2}' and S_ADDR2 = '{3}' and S_PHONE = '{4}' and S_FAX = '{5}' and 
                                            S_CNTC = '{6}' and MAN_NUM = '{7}' and DEPARTMENT = '{8}' and ACTIVE = '{9}' and lastModified = '{10}' and 
                                            updaterID = '{11}' '''.format(S_NUM,S_NAME,S_ADDR1,S_ADDR2,S_PHONE,S_FAX,S_CNTC,MAN_NUM,
                                                                          DEPARTMENT,ACTIVE,lastModified,updaterID)
    return cursor.execute(query).fetchall()
    closeDB()

def updateSuppliers(suppliersID,S_NUM,S_NAME,S_ADDR1,S_ADDR2,S_ADDR3,S_ADDR4,S_ADDR5,S_PHONE,S_FAX,S_CNTC,
                    MAN_NUM,PHONE_AREA,PHONE_EXCH,PHONE_LOCAL,FAX_AREA,FAX_EXCH,FAX_LOCAL,
                    DEPARTMENT,ACTIVE,lastModified,updaterID):
    initDB(getDBLocationFullName())
    query = ''' update Suppliers set S_NUM = '{1}', S_NAME = '{2}', S_ADDR1 = '{3}', S_ADDR2 = '{4}', S_ADDR3 = '{5}', S_ADDR4 = '{6}', S_ADDR5 = '{7}',
                                     S_PHONE = '{8}', S_FAX = '{9}', S_CNTC = '{19}', MAN_NUM = '{11}', PHONE_AREA = '{12}', PHONE_EXCH = '{13}', PHONE_LOCAL = '{14}',
                                     FAX_AREA = '{15}', FAX_EXCH = '{16}', FAX_LOCAL = '{17}', DEPARTMENT = '{18}', ACTIVE = '{19}', lastModified = '{20}',
                                     updaterID = '{21}' where suppliersID = {0} '''.format(suppliersID,S_NUM,S_NAME,S_ADDR1,S_ADDR2,S_ADDR3,S_ADDR4,S_ADDR5,S_PHONE,S_FAX,S_CNTC,
                                                                                      MAN_NUM,PHONE_AREA,PHONE_EXCH,PHONE_LOCAL,FAX_AREA,FAX_EXCH,FAX_LOCAL,
                                                                                      DEPARTMENT,ACTIVE,lastModified,updaterID)
    cursor.execute(query)
    closeDB()
    
def insertSuppliers(S_NUM,S_NAME,S_ADDR1,S_ADDR2,S_ADDR3,S_ADDR4,S_ADDR5,S_PHONE,S_FAX,S_CNTC,
                    MAN_NUM,PHONE_AREA,PHONE_EXCH,PHONE_LOCAL,FAX_AREA,FAX_EXCH,FAX_LOCAL,
                    DEPARTMENT,ACTIVE,lastModified,updaterID):
    initDB(getDBLocationFullName())
    query = '''
            insert into Suppliers (S_NUM,S_NAME,S_ADDR1,S_ADDR2,S_ADDR3,S_ADDR4,S_ADDR5,S_PHONE,S_FAX,S_CNTC,
                                MAN_NUM,PHONE_AREA,PHONE_EXCH,PHONE_LOCAL,FAX_AREA,FAX_EXCH,FAX_LOCAL,
                                DEPARTMENT,ACTIVE,lastModified,updaterID)
            values (?,?,?,?,?,?,?,?,?,?,
                    ?,?,?,?,?,?,?,?,?,?,
                    ?)'''
    cursor.execute(query, (S_NUM,S_NAME,S_ADDR1,S_ADDR2,S_ADDR3,S_ADDR4,S_ADDR5,S_PHONE,S_FAX,S_CNTC,
                           MAN_NUM,PHONE_AREA,PHONE_EXCH,PHONE_LOCAL,FAX_AREA,FAX_EXCH,FAX_LOCAL,
                           DEPARTMENT,ACTIVE,lastModified,updaterID))
    closeDB()
    
def insertSuppliersForTUTImport(S_NUM,S_NAME,S_ADDR1,S_ADDR2,S_ADDR3,S_ADDR4,S_ADDR5,S_PHONE,S_FAX,S_CNTC,
                                MAN_NUM,PHONE_AREA,PHONE_EXCH,PHONE_LOCAL,FAX_AREA,FAX_EXCH,FAX_LOCAL,
                                DEPARTMENT,ACTIVE,lastModified,updaterID):
#    initDB(getDBLocationFullName())
    query = '''
            insert into Suppliers (S_NUM,S_NAME,S_ADDR1,S_ADDR2,S_ADDR3,S_ADDR4,S_ADDR5,S_PHONE,S_FAX,S_CNTC,
                                MAN_NUM,PHONE_AREA,PHONE_EXCH,PHONE_LOCAL,FAX_AREA,FAX_EXCH,FAX_LOCAL,
                                DEPARTMENT,ACTIVE,lastModified,updaterID)
            values (?,?,?,?,?,?,?,?,?,?,
                    ?,?,?,?,?,?,?,?,?,?,
                    ?)'''
    cursor.execute(query, (S_NUM,S_NAME,S_ADDR1,S_ADDR2,S_ADDR3,S_ADDR4,S_ADDR5,S_PHONE,S_FAX,S_CNTC,
                                MAN_NUM,PHONE_AREA,PHONE_EXCH,PHONE_LOCAL,FAX_AREA,FAX_EXCH,FAX_LOCAL,
                                DEPARTMENT,ACTIVE,lastModified,updaterID))
#    closeDB()

def importTUT_Suppliers(fileName):
    #
    #  Import of Tech Shop Parts DB (D Language)
    #  Reads a .TUT file obtain from Pierre
    #
    initDB(getDBLocationFullName())
    fieldSizes = (10,30,30,30,30,30,30,13,13,20,10,3,3,4,3,3,4,2,1)             
    #
    #  File is organized in lenght of fields (above)
    #  Read each line and build the individual fields
    #  Add them individually.
    #    
    with open(fileName, 'r') as fin:
        array=[]
        for lines in fin:
            array=[]
            array.append(lines)
            idx = 0
            aField = ''
            aFieldArray = []
            for eachFieldSize in fieldSizes:
                for idx2 in range(eachFieldSize):
                    aField='''{0}{1}'''.format(aField, array[0][idx])
                    idx = idx + 1   
                aField = aField.rstrip()   # Remove trailing white spaces             
                aFieldArray.append(aField) # Create array of fields
                aField = ''
            insertSuppliersForTUTImport(aFieldArray[0],aFieldArray[1],aFieldArray[2],aFieldArray[3],aFieldArray[4],aFieldArray[5],aFieldArray[6],
                        aFieldArray[7],aFieldArray[8],aFieldArray[9],aFieldArray[10],aFieldArray[11],aFieldArray[12],aFieldArray[13],
                        aFieldArray[14],aFieldArray[15],aFieldArray[16],aFieldArray[17],aFieldArray[18],
                        convertDateStringToOrdinal(todayDate),'1')
    closeDB()
 

def exportCSV_Suppliers(fileName):
    initDB(getDBLocationFullName())
    query = '''select * from Suppliers'''
    data = cursor.execute(query).fetchall()
    with open(fileName, 'w', newline='') as fout:
        for i in range(len(data)):
            csv.writer(fout).writerow(data[i])
    closeDB()

######################################################
# '''CREATE TABLE Accounts(
#            0                 accountsID      INTEGER PRIMARY KEY AUTOINCREMENT  NOT NULL,
#            1                 FIN            CHAR(20),
#            2                 SOURCE         CHAR(20),
#            3                 FUNDS          CHAR(7),
#            4                 APPROV         CHAR(30),
#            5                 A_TITLE        CHAR(60)
#            6                 ACTIVE         CHAR(1),
#            7                 DEPARTMENT     CHAR(2),
#            8                 SEC_MANAGE     CHAR(1),
#            9                 SEC_LEVEL      CHAR(2),
#            10                SEC_SECT       CHAR(1),
#            11                SEC_DIVIS      CHAR(1),
#            12                SEC_DEPTN      CHAR(2),
#            13                DAY            CHAR(2),
#            14                MONTH          CHAR(3),
#            15                YEAR           CHAR(4),
#            16                BUDGET_AMNT    REAL,
#            17                COMMIT_AMNT    REAL,
#            18                EXPEND_AMNT    REAL,
#            19                BALANCE_AMNT   REAL,
#            20                DESCR          CHAR(40),
#            21                D_NAME         CHAR(40),
#            22                RESOURCE       CHAR(15),
#            23                SPECIAL        CHAR(1),
#            24                FMAS_CODE      CHAR(2),
#            25                DOC            CHAR(3),
#            26                IO_NUMBER      CHAR(20),
#            27                CO_NUMBER      CHAR(20),
#            28                FCODE          CHAR(6),
#            29                CCENTER        CHAR(6),
#            30                GLEDGER        CHAR(5),
#            31                FUNDC          CHAR(5),
#            32                R_TYPE         CHAR(3),
#            33                lastModified   INTEGER,
#            34                updaterID      INTEGER,
#            35                departmentsID  INTEGER
#             );''')
def getAccounts():
    #
    #  Retieves all parts  
    #
    initDB(getDBLocationFullName())
    query = '''  
    select * from Accounts ORDER BY IO_NUMBER'''
    return cursor.execute(query).fetchall()
    closeDB()

def updateAccountsDepartmentID(accountsID, departmentsID):
    query = ''' update Accounts set  departmentsID  = '{1}' where accountsID = {0} '''.format(accountsID, departmentsID) 
    cursor.execute(query)      
    
def crossRefAccountsDepartments():
    allAccounts = getAccounts()
    initDB(getDBLocationFullName())
    for each in range(len(allAccounts)):
        deptID = getDepartmentsIDTUTFromDeptCode(allAccounts[each][7])
        if len(deptID) > 0:
            updateAccountsDepartmentID(allAccounts[each][0], deptID[0][0]) 
    closeDB()

def insertAccountsForTUTImport(FIN,SOURCE,FUNDS,APPROV,A_TITLE,ACTIVE,DEPARTMENT,SEC_MANAGE,SEC_LEVEL,
                               SEC_SECT,SEC_DIVIS,SEC_DEPTN,DAY,MONTH,YEAR,BUDGET_AMNT,COMMIT_AMNT,EXPEND_AMNT,
                               BALANCE_AMNT,DESCR,D_NAME,RESOURCE,SPECIAL,FMAS_CODE,DOC,IO_NUMBER,
                               CO_NUMBER,FCODE,CCENTER,GLEDGER,FUNDC,R_TYPE,lastModified,
                               updaterID,departmentsID):
#    initDB(getDBLocationFullName())
    query = '''
            insert into Accounts (FIN,SOURCE,FUNDS,APPROV,A_TITLE,ACTIVE,DEPARTMENT,SEC_MANAGE,SEC_LEVEL,
                               SEC_SECT,SEC_DIVIS,SEC_DEPTN,DAY,MONTH,YEAR,BUDGET_AMNT,COMMIT_AMNT,EXPEND_AMNT,
                               BALANCE_AMNT,DESCR,D_NAME,RESOURCE,SPECIAL,FMAS_CODE,DOC,IO_NUMBER,
                               CO_NUMBER,FCODE,CCENTER,GLEDGER,FUNDC,R_TYPE,lastModified,
                               updaterID,departmentsID)
            values (?,?,?,?,?,?,?,?,?,?,
                    ?,?,?,?,?,?,?,?,?,?,
                    ?,?,?,?,?,?,?,?,?,?,
                    ?,?,?,?,?)'''
    cursor.execute(query, (FIN,SOURCE,FUNDS,APPROV,A_TITLE,ACTIVE,DEPARTMENT,SEC_MANAGE,SEC_LEVEL,
                           SEC_SECT,SEC_DIVIS,SEC_DEPTN,DAY,MONTH,YEAR,BUDGET_AMNT,COMMIT_AMNT,EXPEND_AMNT,
                           BALANCE_AMNT,DESCR,D_NAME,RESOURCE,SPECIAL,FMAS_CODE,DOC,IO_NUMBER,
                           CO_NUMBER,FCODE,CCENTER,GLEDGER,FUNDC,R_TYPE,lastModified,
                           updaterID,departmentsID))
#    closeDB()

def importTUT_Accounts(fileName):
    #
    #  Import of Tech Shop Parts DB (D Language)
    #  Reads a .TUT file obtain from Pierre
    #
    initDB(getDBLocationFullName())
    fieldSizes = (20,20,7,30,60,1,2,1,2,1,1,2,2,3,4,9,9,9,9,40,40,15,1,2,3,20,20,6,6,5,5,3)             
    #
    #  File is organized in lenght of fields (above)
    #  Read each line and build the individual fields
    #  Add them individually.
    #    
    with open(fileName, 'r') as fin:
        array=[]
        for lines in fin:
            array=[]
            array.append(lines)
            idx = 0
            aField = ''
            aFieldArray = []
            for eachFieldSize in fieldSizes:
                for idx2 in range(eachFieldSize):
                    aField='''{0}{1}'''.format(aField, array[0][idx])
                    idx = idx + 1   
                aField = aField.rstrip()   # Remove trailing white spaces             
                aFieldArray.append(aField) # Create array of fields
                aField = ''
            insertAccountsForTUTImport(aFieldArray[0],aFieldArray[1],aFieldArray[2],aFieldArray[3],aFieldArray[4],aFieldArray[5],aFieldArray[6],
                                       aFieldArray[7],aFieldArray[8],aFieldArray[9],aFieldArray[10],aFieldArray[11],aFieldArray[12],aFieldArray[13],
                                       aFieldArray[14],aFieldArray[15],aFieldArray[16],aFieldArray[17],aFieldArray[18],aFieldArray[19],aFieldArray[20],
                                       aFieldArray[21],aFieldArray[22],aFieldArray[23],aFieldArray[24],aFieldArray[25],aFieldArray[26],aFieldArray[27],
                                       aFieldArray[28],aFieldArray[29],aFieldArray[30],aFieldArray[31],
                                       convertDateStringToOrdinal(todayDate),'1','')
    closeDB()
    
def exportCSV_Accounts(fileName):
    initDB(getDBLocationFullName())
    query = '''select * from Accounts'''
    data = cursor.execute(query).fetchall()
    with open(fileName, 'w', newline='') as fout:
        for i in range(len(data)):
            csv.writer(fout).writerow(data[i])
    closeDB()

def importCSV_Accounts(fileName):
    initDB(getDBLocationFullName())
    with open(fileName, 'r') as fin:
        reader = csv.reader(fin)
        for row in reader:
            query = '''
            insert into Accounts (accountsID,FIN,SOURCE,FUNDS,APPROV,A_TITLE,ACTIVE,DEPARTMENT,SEC_MANAGE,SEC_LEVEL,
                               SEC_SECT,SEC_DIVIS,SEC_DEPTN,DAY,MONTH,YEAR,BUDGET_AMNT,COMMIT_AMNT,EXPEND_AMNT,
                               BALANCE_AMNT,DESCR,D_NAME,RESOURCE,SPECIAL,FMAS_CODE,DOC,IO_NUMBER,
                               CO_NUMBER,FCODE,CCENTER,GLEDGER,FUNDC,R_TYPE,lastModified,
                               updaterID,departmentsID)
            values (?,?,?,?,?,?,?,?,?,?,
                    ?,?,?,?,?,?,?,?,?,?,
                    ?,?,?,?,?,?,?,?,?,?,
                    ?,?,?,?,?,?) '''
            cursor.execute(query, (row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7],row[8], row[9],
                                   row[10],row[11],row[12],row[13],row[14],row[15],row[16],row[17],row[18], row[19],
                                   row[20],row[21],row[22],row[23],row[24],row[25],row[26],row[27],row[28], row[29],
                                   row[30],row[31],row[32],row[33],row[34],row[35]))
    closeDB()

##### Database init and close #######

def initDB(filename):
    global db, cursor

    try:
        if not os.path.exists(filename):
            #
            #  myGolfDB does not exist, create an empty myGoldDB.db
            #  in the provided filename
            #
            db = sql.connect(filename)
            cursor = db.cursor()
            cursor.execute('PRAGMA Foreign_Keys=True')

            #
            #   Personnel Table
            #
            cursor.execute('''CREATE TABLE Personnel(
               personnelID             INTEGER PRIMARY KEY AUTOINCREMENT  NOT NULL,
               personnelFNAME          CHAR(30)  NOT NULL,
               personnelLNAME          CHAR(30)  NOT NULL,
               personnelType           CHAR(4),
               personnelSalutation     CHAR(6),
               personnelPIC_LOC        CHAR(100),
               pri_SN                  CHAR(11),
               officeNumber            CHAR(9),
               extension               CHAR(5),
               ECEWorkGroup            CHAR(30),
               title_Position          CHAR(30),
               loginName               CHAR(20),
               password                CHAR(30),
               access_Level            CHAR(15),
               lastModified            INTEGER,
               updaterID               INTEGER
              );''')          
            #
            #   Supplier Table
            #
            cursor.execute('''CREATE TABLE Suppliers(
                suppliersID   INTEGER PRIMARY KEY AUTOINCREMENT  NOT NULL,
                S_NUM         CHAR(10),
                S_NAME        CHAR(30),
                S_ADDR1       CHAR(30),
                S_ADDR2       CHAR(30),
                S_ADDR3       CHAR(30),
                S_ADDR4       CHAR(30),
                S_ADDR5       CHAR(30),
                S_PHONE       CHAR(13),
                S_FAX         CHAR(13),
                S_CNTC        CHAR(20),
                MAN_NUM       CHAR(10),
                PHONE_AREA    CHAR(3),
                PHONE_EXCH    CHAR(3),
                PHONE_LOCAL   CHAR(4),
                FAX_AREA      CHAR(3),
                FAX_EXCH      CHAR(3),
                FAX_LOCAL     CHAR(4),
                DEPARTMENT    CHAR(2),
                ACTIVE        CHAR(1),             
                lastModified  INTEGER,
                updaterID     INTEGER
             );''')
            #
            #   Departments Table
            #
            cursor.execute('''CREATE TABLE Departments(
               departmentsID           INTEGER PRIMARY KEY AUTOINCREMENT  NOT NULL,
               departmentsCode         CHAR(3)   NOT NULL,
               departmentsNAME         CHAR(60)  NOT NULL,
               departmentsMidSize      CHAR(20),
               departmentsShort        CHAR(10),
               departmentsDAList       CHAR(60),
               departmentsSecurity     CHAR(8),
               lastModified            INTEGER,
               updaterID               INTEGER
              );''')
            #
            #   Parts Table
            #
            cursor.execute('''CREATE TABLE Parts(
                partID     INTEGER PRIMARY KEY AUTOINCREMENT  NOT NULL,
                BASIC      CHAR(20),
                VALUE      CHAR(3),
                VALUED     CHAR(3),
                MULT       CHAR(1),
                TOLE       CHAR(5),
                SIZE       CHAR(7),
                DESCR      CHAR(40),
                NSN1       CHAR(4),
                NSN2       CHAR(2),
                NSN3       CHAR(3),
                NSN4       CHAR(4),
                ORDER_NUM  CHAR(20),
                SUPP_NUM   CHAR(10),
                ITEM_NUM   CHAR(12),
                UI         CHAR(2),
                SNC        CHAR(1),
                CLASS      CHAR(1),
                PRICE      CHAR(5),
                PRICED     CHAR(2),
                SUP        CHAR(1),
                SSI        CHAR(2),
                RETAIL     CHAR(9),
                QUANT      CHAR(4),
                QMIN       CHAR(4),
                QORDER     CHAR(4),
                QUSE       CHAR(4),
                QTEMP      CHAR(4),
                FUNCTION   CHAR(20),
                DESCR2     CHAR(60),
                NOMEN      CHAR(15),
                ROW        CHAR(2),
                SIDE       CHAR(1),
                SHELF      CHAR(1),
                SECTION    CHAR(3),
                DEPARTMENT CHAR(2),
                S_NAME     CHAR(30), 
                lastModified  INTEGER,
                updaterID     INTEGER,
                suppliersID   INTEGER,
                departmentsID   INTEGER
              );''')          
#                FOREIGN KEY(departmentsID) REFERENCES Departments(departmentsID),
#                FOREIGN KEY(suppliersID) REFERENCES Suppliers(suppliersID)
            #
            #   Account Table
            #
            cursor.execute('''CREATE TABLE Accounts(
                            accountsID      INTEGER PRIMARY KEY AUTOINCREMENT  NOT NULL,
                            FIN            CHAR(20),
                            SOURCE         CHAR(20),
                            FUNDS          CHAR(7),
                            APPROV         CHAR(30),
                            A_TITLE        CHAR(60),
                            ACTIVE         CHAR(1),
                            DEPARTMENT     CHAR(2),
                            SEC_MANAGE     CHAR(1),
                            SEC_LEVEL      CHAR(2),
                            SEC_SECT       CHAR(1),
                            SEC_DIVIS      CHAR(1),
                            SEC_DEPTN      CHAR(2),
                            DAY            CHAR(2),
                            MONTH          CHAR(3),
                            YEAR           CHAR(4),
                            BUDGET_AMNT    REAL,
                            COMMIT_AMNT    REAL,
                            EXPEND_AMNT    REAL,
                            BALANCE_AMNT   REAL,
                            DESCR          CHAR(40),
                            D_NAME         CHAR(40),
                            RESOURCE       CHAR(15),
                            SPECIAL        CHAR(1),
                            FMAS_CODE      CHAR(2),
                            DOC            CHAR(3),
                            IO_NUMBER      CHAR(20),
                            CO_NUMBER      CHAR(20),
                            FCODE          CHAR(6),
                            CCENTER        CHAR(6),
                            GLEDGER        CHAR(5),
                            FUNDC          CHAR(5),
                            R_TYPE         CHAR(3),
                            lastModified   INTEGER,
                            updaterID      INTEGER,
                            departmentsID  INTEGER
             );''')
#                            FOREIGN KEY(departmentsID) REFERENCES Departments(departmentsID)
            #
            #  Create main root user
            #           
            closeDB()
            
            insertPersonnelRoot('Super', 'User', '', '', '', '', '', '', '', '', 'root', 'q', 'Root', convertDateStringToOrdinal(todayDate), '1')
            
#            cursor.execute('''CREATE TABLE Addresses(
#               AddrID         INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
#               ownerTableName CHAR(30),
#               town           CHAR(30),
#               prov_state     CHAR(30),
#               country        CHAR(30),
#               ownerID        SMALLINT    NOT NULL,
#               pc_zip         CHAR(10),
#               street_number  CHAR(60),
#               appt           CHAR(8),
#               lastModified   INTEGER,
#               modifierID     INTEGER
#              );''')

#            cursor.execute('''CREATE TABLE Telephones(
#               PhoneID        INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
#               ownerTableName CHAR(30),
#               type           CHAR(10),
#               number         CHAR(11)    NOT NULL,
#               extension      CHAR(5),
#               lastModified   INTEGER,
#               modifierID     INTEGER
#              );''')
#        
        else:
            #
            #  myGolfDB exist, just open it
            #
            db = sql.connect(filename)
            cursor = db.cursor()
            cursor.execute('PRAGMA Foreign_Keys=True')
    except:
        print("Error connecting to", filename)
        cursor = None
        raise

def closeDB():
    #  Check if connection is openned
    if db:
        try:
            cursor.close()
            db.commit()
            db.close()
        except:
            print("problem closing database...")
            raise

def lastRowID():
    return(cursor.lastrowid)

if __name__ == "__main__": 
    initDB(DBDefaultName)  # use default file
#    print("Members:\n", get_members())
#    print("Items:\n",get_golfer())

