import tkinter.tix as Tix
import tkinter.ttk as ttk

class   Application(Tix.Frame): 
    def __init__(self,  master=None):
        Tix.Frame.__init__(self, master)    
        self.grid(sticky=Tix.N+Tix.S+Tix.E+Tix.W)   
        self.mainframe()
        self.run = Tix.Button(self, text="run")
        self.stop = Tix.Button(self, text="stop")
    
        self.data.grid(row=0, column=0, rowspan=4,
                       columnspan=2, sticky=Tix.N+Tix.E+Tix.S+Tix.W)
        self.data.columnconfigure(0, weight=1)
    
        self.run.grid(row=4,column=0,sticky=Tix.E+Tix.W)
        self.stop.grid(row=4,column=1,sticky=Tix.E+Tix.W)
    
        self.scrollbar.grid(column=2, sticky=Tix.N+Tix.S)

    def mainframe(self):                
        self.data = Tix.Listbox(self, bg='red')
        self.scrollbar = Tix.Scrollbar(self.data, orient=Tix.VERTICAL)
        self.data.config(yscrollcommand=self.scrollbar.set)
        self.scrollbar.config(command=self.data.yview)

        for i in range(1000):
            self.data.insert(Tix.END, str(i))

a = Application()
a.mainframe()
a.mainloop()
