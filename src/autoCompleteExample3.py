from tkinter import *
import tkinter.ttk as ttk
import tkinter.tix as Tix
import re
from MyGolfApp2016.AppConstants import *
from MyGolfApp2016.AppProcFROMECE import *
import random

lista = ['a', 'actions', 'additional', 'also', 'an', 'and', 'angle', 'are', 'as', 'be', 'bind', 'bracket', 'brackets', 'button', 'can', 'cases', 'configure', 'course', 'detail', 'enter', 'event', 'events', 'example', 'field', 'fields', 'for', 'give', 'important', 'in', 'information', 'is', 'it', 'just', 'key', 'keyboard', 'kind', 'leave', 'left', 'like', 'manager', 'many', 'match', 'modifier', 'most', 'of', 'or', 'others', 'out', 'part', 'simplify', 'space', 'specifier', 'specifies', 'string;', 'that', 'the', 'there', 'to', 'type', 'unless', 'use', 'used', 'user', 'various', 'ways', 'we', 'window', 'wish', 'you']


#        listFrame = Frame(self.modalPane)
#        listFrame.pack(side=TOP, padx=5, pady=5)
#        
#        scrollBar = Scrollbar(listFrame)
#        scrollBar.pack(side=RIGHT, fill=Y)
#        self.listBox = Listbox(listFrame, selectmode=SINGLE)
#        self.listBox.pack(side=LEFT, fill=Y)
#        scrollBar.config(command=self.listBox.yview)
#        self.listBox.config(yscrollcommand=scrollBar.set)
#        self.list.sort()
#        for item in self.list:
#            self.listBox.insert(END, item)

#        listFrame = Frame(self.modalPane)
#        listFrame.pack(side=TOP, padx=5, pady=5)
#        
#        scrollBar = Scrollbar(listFrame)
#        scrollBar.pack(side=RIGHT, fill=Y)

#
#
#  Example, idea for entering a new item from a scroll list (MyCB)
#

#   returnValue = True


#    list = [random.randint(1,100) for x in range(50)]
#    while returnValue:
#        returnValue = ListBoxChoice(root, "Number Picking", "Pick one of these crazy random numbers", list).returnValue()
#        print returnValue
def aProcNow():
    returnValue = True
    list = [random.randint(1,100) for x in range(50)]
    while returnValue:
        returnValue = ListBoxChoice(root, "Number Picking", "Pick one of these crazy random numbers", list).returnValue()
        print (returnValue)
    
class ListBoxChoice(object):
    def __init__(self, master=None, title=None, message=None, list=[]):
        self.master = master
        self.value = None
        self.list = list[:]
        
        self.modalPane = Toplevel(self.master)

        self.modalPane.transient(self.master)
        self.modalPane.grab_set()  # All app events are sent to self.modalPane (exclusive control until a grab_release is issued or is destroy)

        self.modalPane.bind("<Return>", self._choose)
        self.modalPane.bind("<Escape>", self._cancel)

        if title:
            self.modalPane.title(title)

        if message:
            Label(self.modalPane, text=message).pack(padx=5, pady=5)

        listFrame = Frame(self.modalPane)
        listFrame.pack(side=TOP, padx=5, pady=5)
        
        scrollBar = Scrollbar(listFrame)
        scrollBar.pack(side=RIGHT, fill=Y)
        self.listBox = Listbox(listFrame, selectmode=SINGLE)
        self.listBox.pack(side=LEFT, fill=Y)
        scrollBar.config(command=self.listBox.yview)
        self.listBox.config(yscrollcommand=scrollBar.set)
        self.list.sort()
        for item in self.list:
            self.listBox.insert(END, item)

        buttonFrame = Frame(self.modalPane)
        buttonFrame.pack(side=BOTTOM)

        chooseButton = Button(buttonFrame, text="Choose", command=self._choose)
        chooseButton.pack()

        cancelButton = Button(buttonFrame, text="Cancel", command=self._cancel)
        cancelButton.pack(side=RIGHT)

    def _choose(self, event=None):
        try:
            firstIndex = self.listBox.curselection()[0]
            self.value = self.list[int(firstIndex)]
        except IndexError:
            self.value = None
        self.modalPane.destroy()

    def _cancel(self, event=None):
        self.modalPane.destroy()
        
    def returnValue(self):
        self.master.wait_window(self.modalPane)
        return self.value
        self._cancel()


class AppCBVar(Tix.Frame):
    def __init__(self, parent, aVar, aList, aWidth):
        Tix.Frame.__init__(self, parent)
        
        self.config(border = 2, highlightcolor=AppDefaultForeground, highlightthickness = 2, takefocus=0)
        self.bind('<Enter>', widgetEnter)
        self.CB = ttk.Combobox(self,width=aWidth, font=fontBig,justify=CENTER, text=aVar, takefocus=0)
        self.CB.bind("<Enter>", self.reFocus)
        self.CB.grid(row=0, column=0, sticky=N+S+E+W)
        self.CB.bind("<ComboboxSelected>")
        self.CB.config(state=DISABLED)
        self.updateValuesList(aList)

    def reFocus(self, *args):
        print("refocus")
        self.focus()
        
    def focus(self):
        self.CB.focus()
        
    def get(self):
        return(self.CB.get())
 
    def clear(self):
        self.CB.config(state='normal')
        self.CB.delete(0, END)
        self.CB.config(state=DISABLED)
                  
    def load(self, aValue):
        self.CB.config(state=NORMAL)
        self.CB.delete(0, END)
        self.CB.insert(0, aValue)
        self.CB.config(state=DISABLED)
        
    def enable(self):
        self.CB.config(state=NORMAL, takefocus=1)
        self.bind('<Enter>', widgetEnter)
       
    def disable(self):
        self.CB.config(state=DISABLED, takefocus=0)
        self.unbind('<Enter>')
        
    def updateValuesList(self, aList):
        self.aList = aList
        self.CB.config(state=NORMAL, values=aList)
        self.CB.delete(0, END)
        self.CB.config(state=DISABLED)
        
class AppACListEntry(Entry):
    #
    #   AutoComplete Scroll List
    #
    def __init__(self, lista, fieldWidth, addItemProc, *args, **kwargs):
        self.fieldWidth = fieldWidth
        self.addItemProc = addItemProc
        Entry.__init__(self, *args, **kwargs)
        
        self.config(highlightcolor=AppDefaultForeground, highlightthickness = 2, width=self.fieldWidth, 
                    justify=LEFT, font=fontBig)
#        self.bind('<Enter>', self.enter)
        self.bind('<Button-1>', self.enter)
#        self.bind('<Return>', self.enterKeyEntered)
        self.bind('<FocusOut>', self.check)
        self.lista = lista        
        self.var = self["textvariable"]
        if self.var == '':
            self.var = self["textvariable"] = StringVar()

        self.var.trace('w', self.changed)
        self.bind("<Right>", self.enterKeyEntered)
        self.bind("<Return>", self.selection)
        self.bind("<Up>", self.up)
        self.bind("<Down>", self.down)
        self.bind("<MouseWheel>", self._on_mousewheel) 
        self.bind("<Key>", self.cancel)             
        self.lb_up = False
        self.lbName = None
        
    def cancel(self, event):
        if event.char == chr(27):
            print("Escape Detected")
            if self.lb_up:
                self.lb.destroy()
                self.lb_up = False
       
    def enterKeyEntered(self, *args):
        if not self.lb_up:
            self.lb = self.createLB()
            print(lista)
            for w in self.lista:
                self.lb.insert(END,w)
            index = 0
            self.lb.selection_set(first=index)
            self.lb.activate(index)
        
    def createLB(self):
        print("Creating the list here.")
        lb = Listbox(width=self.fieldWidth, font=fontBig, takefocus=0, height=10)
        lb.bind("<ButtonRelease-1>", self.selectItem)
        lb.bind("<Double-Button-1>", self.selection)
        lb.bind("<Right>", self.selection)
        lb.bind("<Up>", self.up)
        lb.bind("<Down>", self.down)
        lb.bind("<MouseWheel>", self._on_mousewheel)       
        self.lbName = lb.winfo_name()
        lb.place(x=self.winfo_x(), y=self.winfo_y()+self.winfo_height())
        self.lb_up = True
        return lb
    
    def _on_mousewheel(self, event):
        if event.delta > 0:
            self.lb.yview_scroll(-2, 'units')
        else:
            self.lb.yview_scroll(2, 'units')
                  
    def enter(self,event):
#        if len(self.var.get()) > 0:
#            self.changed(None,None,None)
#        else:
#            self.enterKeyEntered(None)
        self.focus()

    def check(self, event):
        print("LEAVING")
        aName = event.widget.focus_lastfor()
        aName2 = "{0}".format(aName).replace(".","")
        if aName2 != self.lbName:
            if not self.findInList():
                if len(self.var.get()) > 0:
                    print("item not in List. Youhoo.")
                    if self.addItemProc == None:
                        print("Must select item from list")
                        self.focus()
                    else:
                        self.addItemProc()
                else:
                    print("No entry selected.")
            if self.lb_up:
                self.lb.destroy()
                self.lb_up = False

    def changed(self, name, index, mode):  
        print("Changed detected!")
        if self.var.get() == '':
            pass
#            if self.lb_up:
#                self.lb.destroy()
#            self.lb_up = False
        else:
            words = self.comparison()
            print(words)
            if words:            
                if not self.lb_up:
                    self.lb = self.createLB()
                
                self.lb.delete(0, END)
                for w in words:
                    self.lb.insert(END,w)
                index = 0
                self.lb.selection_set(first=index)
                self.lb.activate(index)
            else:
                if self.lb_up:
                    self.lb.destroy()
                    self.lb_up = False

    def selectItem(self, event):
        print("Select Item")
        if self.lb_up:
            if self.lb.curselection() == ():
                print("I am here")
                index = '0'
            else:
                print("I am here now")
                index = self.lb.curselection()[0]
            self.lb.selection_clear(first=index)
            self.lb.selection_set(first=index)
            self.lb.activate(index)
            print("Index is: ", index)
#        self.reDisplayList(index)
        self.focus()
        print("After")
               
    def selection(self, event):
        print("SELECTION")

        if self.lb_up:
            self.var.set(self.lb.get(ACTIVE))
            self.lb.destroy()
            self.lb_up = False
            self.icursor(END)
            
            focusNext(self, self)
#            self.focus()

    def up(self, event):
        print("UP")

        if self.lb_up:
            if self.lb.curselection() == ():
                index = '0'
            else:
                index = self.lb.curselection()[0]
            if index != '0':                
                self.lb.selection_clear(first=index)
                index = str(int(index)-1)                
                self.lb.selection_set(first=index)
                self.lb.activate(index) 
            self.focus()

    def down(self, event):
        print("DOWN")

        if self.lb_up:
            if self.lb.curselection() == ():
                index = '0'
            else:
                index = self.lb.curselection()[0]
            if index != END:                        
                self.lb.selection_clear(first=index)
                index = str(int(index)+1)        
                self.lb.selection_set(first=index)
                self.lb.activate(index) 
            self.focus()

    def comparison(self):
        print("COMPARAISON")
        pattern = re.compile('.*' + self.var.get() + '.*')
        return [w for w in self.lista if re.match(pattern, w)]
        self.focus()

    def findInList(self, *args):
        try:
            self.lista.index(self.var.get())
            return True
        except:
            return False
        
#    def autowidth(self,maxwidth):
#        f = font.Font(font=self.cget("font"))
#        pixels = 0
#        for item in self.get(0, "end"):
#            pixels = max(pixels, f.measure(item))
        # bump listbox size until all entries fit
#        pixels = pixels + 10
#        width = int(self.cget("width"))
#        for w in range(0, maxwidth+1, 5):
#            if self.winfo_reqwidth() >= pixels:
#                break
#            self.config(width=width+w)
class AppACListEntryV2(Entry):
    #
    #   AutoComplete Scroll List
    #
    def __init__(self, lista, fieldWidth, *args, **kwargs):
        self.fieldWidth = fieldWidth
        Entry.__init__(self, *args, **kwargs)
        
        self.config(highlightcolor=AppDefaultForeground, highlightthickness = 2, width=self.fieldWidth, 
                    justify=LEFT, font=fontBig, takefocus=1)
        self.bind('<Enter>', self.enter)
#        self.bind('<Return>', self.enterKeyEntered)
        self.bind('<FocusOut>', self.check)
        self.lista = lista        
        self.var = self["textvariable"]
        if self.var == '':
            self.var = self["textvariable"] = StringVar()

        self.var.trace('w', self.changed)
        self.bind("<Right>", self.enterKeyEntered)
        self.bind("<Return>", self.selection)
        self.bind("<Up>", self.up)
        self.bind("<Down>", self.down)
        self.bind("<MouseWheel>", self._on_mousewheel)              
        self.bind("<Key>", self.cancel)              
        self.lb_up = False
        self.lbName = None
        
    def cancel(self, event):
        if event.char == chr(27):
            print("Escape Detected")
            if self.lb_up:
                self.lb.destroy()
                self.lb_up = False
        
    def enterKeyEntered(self, *args):
        print("Key Entered")
        if not self.lb_up:
            self.lb = self.createLB()
            print(lista)
        if len(self.var.get()) > 0:
            self.changed(None,None,None)
#            for w in self.lista:
#                self.lb.insert(END,w)
#            index = 0
#            self.lb.selection_set(first=index)
#            self.lb.activate(index)
        
    def createLB(self):
        print("Creating the list here.")
        lb = Listbox(width=self.fieldWidth, font=fontBig, takefocus=0, height=10)
        lb.bind("<ButtonRelease-1>", self.selectItem)
        lb.bind("<Double-Button-1>", self.selection)
        lb.bind("<Right>", self.selection)
        lb.bind("<Up>", self.up)
        lb.bind("<Down>", self.down)
        lb.bind("<MouseWheel>", self._on_mousewheel)       
        self.lbName = lb.winfo_name()
        lb.place(x=self.winfo_x(), y=self.winfo_y()+self.winfo_height())
        self.lb_up = True
        for w in self.lista:
            lb.insert(END,w)
        index = 0
        lb.selection_set(first=index)
        lb.activate(index)
        return lb
    
    def _on_mousewheel(self, event):
        if event.delta > 0:
            self.lb.yview_scroll(-2, 'units')
        else:
            self.lb.yview_scroll(2, 'units')
                  
    def enter(self,event):
#        if len(self.var.get()) > 0:
#            self.enterKeyEntered(None)            
#            self.changed(None,None,None)
#        else:
#            self.enterKeyEntered(None)
        self.focus()

    def check(self, event):
        print("LEAVING")
        aName = event.widget.focus_lastfor()
        aName2 = "{0}".format(aName).replace(".","")
        if aName2 != self.lbName:
            if not self.findInList():
                if len(self.var.get()) > 0:
                    print("item not in List. Youhoo.")
                else:
                    print("No entry selected.")
            if self.lb_up:
                self.lb.destroy()
                self.lb_up = False

    def changed(self, name, index, mode):  
        print("Changed detected!")
        if self.var.get() == '':
            if not self.lb_up:
                self.lb = self.createLB()               
        else:
            print("Is lb_up: ", self.lb_up)
            if not self.lb_up:
                self.lb = self.createLB()
            words = self.comparison()
            print(words)
            if words:            
#                if not self.lb_up:
#                    self.lb = self.createLB()
                
#                self.lb.delete(0, END)
#                for w in words:
#                    self.lb.insert(END,w)
                self.lb.select_clear(0, END)
                if len(words) > 0:
                    index = self.lista.index(words[0])
                else:
                    index = 0
                self.lb.selection_set(first=index)
                self.lb.activate(index)
                self.lb.see(index)
            else:
                print("Clearing selection")
                self.lb.select_clear(0, END)

    def selectItem(self, event):
        print("Select Item")
        if self.lb_up:
            if self.lb.curselection() == ():
                print("I am here")
                index = '0'
            else:
                print("I am here now")
                index = self.lb.curselection()[0]
            self.lb.selection_clear(first=index)
            self.lb.selection_set(first=index)
            self.lb.activate(index)
            print("Index is: ", index)
#        self.reDisplayList(index)
        self.focus()
        print("After")
               
    def selection(self, event):
        words = self.comparison()
        print("SELECTION")
        if self.lb_up:
            if len(words) > 0:
                self.var.set(self.lb.get(ACTIVE))
            self.lb.destroy()
            self.lb_up = False
            self.icursor(END)
            
#            focusNext(self)
            self.focus()

    def up(self, event):
        print("UP")

        if self.lb_up:
            if self.lb.curselection() == ():
                index = '0'
            else:
                index = self.lb.curselection()[0]
            if index != '0':                
                self.lb.selection_clear(first=index)
                index = str(int(index)-1)                
                self.lb.selection_set(first=index)
                self.lb.activate(index) 
            self.focus()

    def down(self, event):
        print("DOWN")

        if self.lb_up:
            if self.lb.curselection() == ():
                index = '0'
            else:
                index = self.lb.curselection()[0]
            if index != END:                        
                self.lb.selection_clear(first=index)
                index = str(int(index)+1)        
                self.lb.selection_set(first=index)
                self.lb.activate(index) 
            self.focus()

    def comparison(self):
        print("COMPARAISON")
        pattern = re.compile('.*' + self.var.get() + '.*')
        return [w for w in self.lista if re.match(pattern, w)]
#        new = [w for w in self.lista if re.match(pattern, w)]
#        print(new)
        self.focus()

    def findInList(self, *args):
        try:
            self.lista.index(self.var.get())
            return True
        except:
            return False
        
#    def autowidth(self,maxwidth):
#        f = font.Font(font=self.cget("font"))
#        pixels = 0
#        for item in self.get(0, "end"):
#            pixels = max(pixels, f.measure(item))
        # bump listbox size until all entries fit
#        pixels = pixels + 10
#        width = int(self.cget("width"))
#        for w in range(0, maxwidth+1, 5):
#            if self.winfo_reqwidth() >= pixels:
#                break
#            self.config(width=width+w)    #
    #   AutoComplete Scroll List
    #

if __name__ == '__main__':
    root = Tk()

#    entry =  AppACListEntry(lista, 25, aProcNow, root)
    entry =  AppACListEntry(lista, 25, None, root)
    entry.grid(row=0, column=0)
    entry4 =  AppACListEntryV2(lista, 25, root)
    entry4.grid(row=1, column=0)
#    def __init__(self, parent, aVar, aList, aWidth):
    departmentV = StringVar()
#    departmentV.trace('w', departmentEntered)
    entry2 = AppCBVar(root, departmentV, lista, 25)
    entry2.grid(row=0, column=1)
    entry3 = AppCBVar(root, departmentV, lista, 25)
    entry3.grid(row=1, column=1)
    Button(root, text='nothing', takefocus=0).grid(row=4, column=0, columnspan = 2)
    entry2.enable()
    entry3.enable()
    root.mainloop()