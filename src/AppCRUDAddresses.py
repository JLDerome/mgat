import sqlite3 as sql
import os
import csv
import AppCRUD as AppCRUD
from AppProc import getDBLocationFullName, convertDateStringToOrdinal, convertOrdinaltoString
from AppConstants import *

db=None
cursor = None
lidGolfers = 0

#cursor.execute('''CREATE TABLE Addresses(
#   uniqueID  INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
#   ownerTableName CHAR(30),
#   town           CHAR(30),
#   prov_state     CHAR(30),
#   country        CHAR(30),
#   ownerID        SMALLINT    NOT NULL,
#   pc_zip         CHAR(10),
#   phone          CHAR(10),
#   website        CHAR(40),
#   street_number  CHAR(60),
#   deleted        CHAR(1),
#   lastModified         INTEGER,
#   updaterID            INTEGER
#   );''')

def get_Addresses():
    AppCRUD.initDB(getDBLocationFullName())
    query = '''select * from Addresses'''
    ans = AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()
    return ans

def get_AddressesForOwnerID(ownerID, ownerTable, NoClose=False):
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                select uniqueID from Addresses where ownerID={0} and ownerTableName = "{1}"
            '''.format(ownerID, ownerTable)
    ans = AppCRUD.cursor.execute(query).fetchall()

    if NoClose == False:
        AppCRUD.closeDB()

    if len(ans) > 0:
        return ans
    else:
        return None

def get_AddresseID(ownerID, NoClose=False):
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                select uniqueID from Addresses where ownerID={0}
            '''.format(ownerID)
    ans = AppCRUD.cursor.execute(query).fetchall()

    if NoClose == False:
        AppCRUD.closeDB()

    if len(ans) > 0:
        return ans[0][0]
    else:
        return None
#
#  Kept for compatibility
#
def get_AllAddrDetails():
    AppCRUD.initDB(getDBLocationFullName())
    query = '''select * from Addresses'''
    ans = AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()
    return ans

def get_courseAddresses(courseID):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''select * from Addresses where ownerTableName='Courses' and ownerID = {0}'''.format(courseID)
    ans = AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()
    return ans

def get_golferAddresses(golferID):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''
                select * from Addresses where ownerTableName='Golfers'
                and ownerID = {0}'''.format(golferID)
    data = AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()
    return data

def update_addresses(uniqueID,ownerTable, town, provState, country, ownerID, pc_zip,phone,
                     website,street_number,lasModified, updaterID, NoClose=False):
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                update Addresses
                set ownerTableName=?, town=?, prov_state=?, country=?, ownerID=?, pc_zip=?,phone=?,website=?,street_number=?,
                    lastModified=?, updaterID=?
                where uniqueID == ?
            '''

    AppCRUD.cursor.execute(query, (ownerTable, town, provState, country, ownerID, pc_zip,phone,website, street_number,
                                   lasModified, updaterID, uniqueID))

    if NoClose == False:
        AppCRUD.closeDB()

def insert_addresses(ownerTable, town, provState, country, ownerID, pc_zip,phone,website,street_number,
                     deleted, lastModified, updaterID, Noclose=False):
    if Noclose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                insert into Addresses (ownerTableName, town, prov_state, country, ownerID,
                                       pc_zip,phone,website,street_number, deleted, lastModified, updaterID)
                values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            '''
    ans = AppCRUD.cursor.execute(query, (ownerTable, town, provState, country, ownerID, pc_zip,phone,
                                   website,street_number,deleted, lastModified, updaterID))

    if Noclose == False:
        AppCRUD.closeDB()
        
def exportCSV_addresses(fileName):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''select * from Addresses'''
    data = AppCRUD.cursor.execute(query).fetchall()
    file = AppDefaultLocation + '/' + BackupDirDefaultName +'/'+ fileName
    with open(file, 'w', newline='', encoding='ISO-8859-1') as fout:
        for i in range(len(data)):
            csv.writer(fout).writerow(data[i])
    AppCRUD.closeDB()
    return len(data)

def importCSV_addresses(fileName):
    AppCRUD.initDB(getDBLocationFullName())
    file = AppDefaultLocation + '/' + BackupDirDefaultName + '/' + fileName
    with open(file, 'r', encoding='ISO-8859-1') as fin:
        reader = csv.reader(fin)
        for row in reader:
            # row[5] must be a valid courseID
            query = '''
            insert into Addresses (uniqueID,ownerTableName,town,prov_state,country,ownerID,
                                   pc_zip,phone,website,street_number,deleted, lastModified,updaterID)
            values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'''
            AppCRUD.cursor.execute(query, (row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7],row[8],row[9],
                                           row[10],row[11], row[12]))
    AppCRUD.closeDB()
    return len(get_Addresses())

def importAddressesCSV_MyGolf2015(fileName):
    AppCRUD.initDB(getDBLocationFullName())
    with open(fileName, newline='', encoding='ISO-8859-1') as fin:
        reader = csv.reader(fin)
        for row in reader:
            query = '''
               insert into Addresses (uniqueID,ownerTableName,town,prov_state,country,ownerID,pc_zip,
                                      phone,website,street_number,deleted, lastModified,updaterID)

                           values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'''
            AppCRUD.cursor.execute(query, (row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9],
                                           'N', convertDateStringToOrdinal(todayDate), '1'))
    AppCRUD.closeDB()
    return len(get_Addresses())

#cursor.execute('''CREATE TABLE Addresses(
#   AddrID  INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
#   ownerTableName CHAR(30),
#   town           CHAR(30),
#   prov_state     CHAR(30),
#   country        CHAR(30),
#   ownerID        SMALLINT    NOT NULL,
#   pc_zip         CHAR(10),
#   phone          CHAR(10),
#   website        CHAR(40),
#   street_number  CHAR(60),
#   deleted        CHAR(1),
#   lastModified         INTEGER,
#   updaterID            INTEGER
#   );''')

