##########################################################
#
#   Departments Table
#
#   This class implements the specific manipulation of the
# data for the ECE Departments Table that the TECH Shop requires.
#

import tkinter.tix as Tix
import tkinter.ttk as ttk
#import ECECRUD as AppDB
#import ECECRUDPerson as AppDBPerson
#from ECECRUDDepartments import getDepartmentsDictionaryInfo, getDepartmentsCode
#from ECEAppDialogClass import AppDuplicatePerson, AppDisplayAbout, AppQuestionRequest
#import tkinter.filedialog as Fd
#import tkinter.messagebox as Mb
import textwrap, re, operator

from tkinter.constants import *

#from ECEAppMyClasses import *
#from ECEAppConstants import *

#from ECEShopProc import *

from PIL import Image

import tkinter.tix as Tix
import tkinter.ttk as ttk
import AppCRUD as CRUD
import AppCRUDGolfers as CRUDGolfers
import AppCRUDGames as CRUDGames
import AppCRUDTees as CRUDTees
import AppCRUDCourses as CRUDCourses
import tkinter.filedialog as Fd
import tkinter.messagebox as Mb
import textwrap, re
from dateutil import parser
from addEditTeePopUp import addEditTeePopUp

from tkinter.constants import *

from tableCommonWindowClass import tableCommonWindowClass
from AppDialogClass import AppDisplayUnderConstruction, AppDisplayMessage, AppDisplayErrorMessage
from AppClasses import *
from AppConstants import *
from AppProc import *

courseList = []
golfersList = []
teeList = []

class coursesWindow(tableCommonWindowClass):
    def __init__(self, aWindow, aCloseCommand, loginData):
        #
        #  loginInfo format: Updater Full Name, access_Level, loginID
        #
        self.windowTitle = 'Courses'
        self.tableName = 'Courses'
        tableCommonWindowClass.__init__(self, aWindow, aCloseCommand, self.windowTitle, loginData, self.tableName)
        self.loginData = loginData
        self.accessLevel = self.loginData[1]
        self.updaterID = self.loginData[2]
        self.updaterFullname = self.loginData[0]
        self.aCloseCommand = aCloseCommand
        self.aWindow = aWindow
        self.displayStartDateForGame = None
        self.displayEndDateForGame = None
        self.displayGameForCourse = None
        self.display9Holes = True
        self.displayToday = False
        self.gamesListingColumnCount = 14

        self.courseComboBoxData = CRUDCourses.getCourseCB()
        self.courseDictionary = {}
        self.reverseCourseDictionary = {}
        for i in range(len(self.courseComboBoxData)):
            self.courseDictionary.update({self.courseComboBoxData[i][1]: self.courseComboBoxData[i][0]})
            self.reverseCourseDictionary.update({self.courseComboBoxData[i][0]: self.courseComboBoxData[i][1]})

        self.teeComboBoxData = CRUDTees.get_tees(False)
        self.teeDictionary = {}
        self.reverseTeeDictionary = {}
        self.reverseTeeDictionaryCourseID = {}
        for i in range(len(self.teeComboBoxData)):
            self.teeDictionary.update({self.teeComboBoxData[i][1]: self.teeComboBoxData[i][0]})
            self.reverseTeeDictionary.update({self.teeComboBoxData[i][0]: self.teeComboBoxData[i][1]})
            self.reverseTeeDictionaryCourseID.update({self.teeComboBoxData[i][0]: self.teeComboBoxData[i][5]})

        #
        #  loginInfo format: password, access_Level, loginID, updaterID
        #
        self.updaterFullname = loginData[0]
        self.updaterID = self.loginData[2]
        self.displayCurUser.setUserName(self.updaterFullname)

        #######################################################################################
        # Inherited all from tableCommanWindowClass
        #
        #  This class intents to change the mainArea only.  Everything else must remain common
        #
        self.mainAreaRowCount = 0
        self.mainAreaColumnCount = 0
        self._reDisplayRecordFrame()
        self.mainArea.rowconfigure(self.mainAreaRowCount, weight=1)

    ############################################################ Area to add window specific commands
    #        self.closeButton = ttk.Button(self.commandFrame, text='Test', style='CommandButton.TButton',
    #                                 command= lambda: self.sampleCommand())
    #        self.closeButton.grid(row=0, column=0, sticky=N+S)


    ################################################################# Redefined commands
    #
    #  invoke from the tableCommonWindows.  These commands are specific to each tables
    #  The specific actions are redefinded here.
    #
    #    def displayRec(self, *args):
    #        print("Displaying a Record: ", self.curRecNum)
    #
    #    def save(self):
    #        #
    #        #  Table specific.  Return a True if succesful saved.
    #        #
    #        print("Saving a record (re-defined)")
    #        return True

    def tableSpecificMenu(self):
        pass
        '''
        self.viewsmenu.add_command
        self.viewsmenu.add_command(label="Active Golfers", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                   command=lambda viewID='Active Golfers': self.basicViewsList(viewID))

        sortsmenu = Menu(self.menubar, foreground=AppDefaultForeground, font=AppDefaultMenuFont, tearoff=0)
        sortsmenu.add_command(label="Default", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Default': self.sortingList(sortID))
        sortsmenu.add_command(label="Last Name", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Last Name': self.sortingList(sortID))
        sortsmenu.add_command(label="First Name", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='First Name': self.sortingList(sortID))
        sortsmenu.add_command(label="Birthday", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Birthday': self.sortingList(sortID))
        #
        # Next Command actually puts the menu up
        #
        self.menubar.add_cascade(label="Sorts", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                 menu=sortsmenu)
        '''
    def sort(self):
        pass
        if self.curOp == 'default':
            currentID = self.recList[self.curRecNumV.get()][0]
            if self.sortID.get() == 'Default':
                self.recList = sorted(self.recList, key=operator.itemgetter(1))
                '''
                elif self.sortID.get() == 'Last Name':
                    self.recList.sort(key=lambda row: row[2])
                elif self.sortID.get() == 'First Name':
                    self.recList.sort(key=lambda row: row[1])
                elif self.sortID.get() == 'Birthday':
                    self.recList.sort(key=lambda row: row[4])
                '''
            else:
                self.recList.sort(key=lambda row: row[0])
            newIdx = getIndex(currentID, self.recList)
            self.curRecNumV.set(newIdx)
        else:
            aMsg = "ERROR: Invalid request. Must be executed in default state only."
            self.myMsgBar.newMessage('error', aMsg)

    def _enableWindowCommand(self):
        #
        #  Any command specific must added here
        #
        pass

    #        self.closeButton.config(state=NORMAL)

    def _disableWindowCommand(self):
        #
        #  Any command specific must added here
        #
        pass

    #        self.closeButton.config(state=DISABLED)


#    cursor.execute('''CREATE TABLE Courses(
#       courseID INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
#       courseNAME          CHAR(60)    NOT NULL,
#       courseSNAME         CHAR(30)    NOT NULL,
#       courseLogo          CHAR(100),
#       deleted              CHAR(1),
#       lastModified         INTEGER,
#       updaterID            INTEGER
#       );''')
    def getTableData(self):
        self.recAll = CRUDCourses.get_courses()

    def _buildWindowsFields(self, aFrame):

        #        headerList1 = ('BASIC','VALUE','VALUED','MULT','TOLE','SIZE','DESCR','NSN1','NSN2','NSN3','NSN4')
        colTotal = 1
        self.fieldsFrame = AppFieldsFrame(aFrame, colTotal)
        self.fieldsFrame.grid(row=0, column=0, sticky=N + S + E + W)

        ########################################################### Main Identification Section
        self.identificationFrame = AppBorderFrame(self.fieldsFrame, 2)
        self.identificationFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column,
                                      sticky=E + W + N + S)

        self.identificationFrame.noStretchColumn(self.identificationFrame.column)
        self.identificationPICFrame = AppFrame(self.identificationFrame, 1)
        self.identificationPICFrame.grid(row=self.identificationFrame.row, column=self.identificationFrame.column,
                                         sticky=N + W + E + S)

        self.selectedPicture = AppPictureFrame(self.identificationPICFrame, courseDefaultPictureName, appCoursePICDir,
                                               coursePicWidth, coursePicHeight, self.myMsgBar)
        self.selectedPicture.grid(row=self.identificationPICFrame.row, column=self.identificationPICFrame.column,
                                  sticky=N + W + S)

        self.identificationFrame.addColumn()
        self.identificationDATADisplayFrame = AppBorderFrame(self.identificationFrame, 1)
        self.identificationDATADisplayFrame.grid(row=self.identificationFrame.row,
                                                 column=self.identificationFrame.column,
                                                 sticky=N + W + E + S)
        self.identificationDATADisplayFrame.noBorder()

        self.selectedCourseParticular = AppCourseIDFrame(self.identificationDATADisplayFrame, self.myMsgBar)
        self.selectedCourseParticular.grid(row=self.identificationDATADisplayFrame.row, column=self.identificationDATADisplayFrame.column,
                                            sticky=N + W + S)

        self.identificationDATAEditAddFrame = AppBorderFrame(self.identificationFrame, 1)
        self.identificationDATAEditAddFrame.grid(row=self.identificationFrame.row, column=self.identificationFrame.column,
                                                 sticky=N + W + E + S)
        self.identificationDATAEditAddFrame.noBorder()

        self.courseDetailsEditAddFrame = AppColumnAlignFieldFrame(self.identificationDATAEditAddFrame)
        self.courseDetailsEditAddFrame.grid(row=self.identificationDATAEditAddFrame.row, column=self.identificationDATAEditAddFrame.column,
                                            sticky=E + W + N + S)
        self.courseDetailsEditAddFrame.noBorder()

        self.selectedLongCourseName = AppFieldEntry(self.courseDetailsEditAddFrame, None, None,
                                                        courseLongNameWidth, self._aDirtyMethod)
        self.courseDetailsEditAddFrame.addNewField('Course Name (long)', self.selectedLongCourseName)

        self.selectedShortCourseName = AppFieldEntry(self.courseDetailsEditAddFrame, None, None,
                                                        courseShortNameWidth, self._aDirtyMethod)
        self.courseDetailsEditAddFrame.addNewField('Course Name (Short)', self.selectedShortCourseName)

        self.selectedEstablishedYear = AppFieldEntry(self.courseDetailsEditAddFrame, None, None,
                                                     courseYearWidth, self._aDirtyMethod)
        self.courseDetailsEditAddFrame.addNewField('Established Year', self.selectedEstablishedYear)

        self.identificationDATAEditAddFrame.addRow()
        self.addressesButton = AppCmdLineButton(self.identificationDATAEditAddFrame, 'Next', None, self.displayAddressesFrame)
        self.addressesButton.grid(row=self.identificationDATAEditAddFrame.row, column=self.identificationDATAEditAddFrame.column, sticky=N+S+E)
        self.addressesButton.addToolTip("Display Addresses Fields")

        self.identificationDATAAddressFrame = AppBorderFrame(self.identificationFrame, 1)
        self.identificationDATAAddressFrame.grid(row=self.identificationFrame.row, column=self.identificationFrame.column,
                                                 sticky=N + W + E + S)

        self.identificationDATAAddressFrame.noBorder()

        self.addressDetailsEditAddFrame = AppColumnAlignFieldFrame(self.identificationDATAAddressFrame)
        self.addressDetailsEditAddFrame.grid(row=self.identificationDATAAddressFrame.row, column=self.identificationDATAAddressFrame.column,
                                            sticky=E + W + N + S)
        self.addressDetailsEditAddFrame.noBorder()

        self.selectedStreet_number = AppFieldEntry(self.addressDetailsEditAddFrame, None, None,
                                                    courseStreet_numberWidth, self._aDirtyMethod)
        self.addressDetailsEditAddFrame.addNewField('Civic Address', self.selectedStreet_number)

        self.selectedTown = AppFieldEntry(self.addressDetailsEditAddFrame, None, None,
                                          courseTownWidth, self._aDirtyMethod)
        self.addressDetailsEditAddFrame.addNewField('City', self.selectedTown)

        self.selectedProv_state = AppFieldEntry(self.addressDetailsEditAddFrame, None, None,
                                                courseProv_stateWidth, self._aDirtyMethod)
        self.addressDetailsEditAddFrame.addNewField('State/Province', self.selectedProv_state)

        self.selectedPc_zip = AppFieldEntry(self.addressDetailsEditAddFrame, None, None,
                                           coursePc_zipWidth, self._aDirtyMethod)
        self.addressDetailsEditAddFrame.addNewField('Postal Code/ZIP Code', self.selectedPc_zip)

        self.selectedCountry = AppSearchLB(self.addressDetailsEditAddFrame, self, None, None, AppCountryList, courseCountryWidth,
                                           None, self.myMsgBar, self._aDirtyMethod, self._aCountryEntered)
        self.addressDetailsEditAddFrame.addNewField('Country', self.selectedCountry)

        self.selectedPhone = AppFieldEntryPhone(self.addressDetailsEditAddFrame, None, None,
                                             coursePhoneWidth, self._aDirtyMethod)
        self.selectedPhone.addValidation(self.myMsgBar)
        self.addressDetailsEditAddFrame.addNewField('Course Phone #', self.selectedPhone)

        self.selectedWebsite = AppFieldEntry(self.addressDetailsEditAddFrame, None, None,
                                             courseWebsiteWidth, self._aDirtyMethod)
        self.addressDetailsEditAddFrame.addNewField('Course Website', self.selectedWebsite)

        self.identificationDATAAddressFrame.addRow()
        self.courseFieldsButton = AppCmdLineButton(self.identificationDATAAddressFrame, 'Back', None, self.displayCoursesFieldsFrame)
        self.courseFieldsButton.grid(row=self.identificationDATAAddressFrame.row, column=self.identificationDATAAddressFrame.column, sticky=N+S+E)
        self.courseFieldsButton.addToolTip("Display Course Particulars")

        ########################################################### Main Identification Section
        self.fieldsFrame.addRow()
        self.fieldsFrame.stretchCurrentRow()
        self.teesFrame = AppBorderFrame(self.fieldsFrame, 1)
        self.teesFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column,
                                      sticky=E + W + N + S)
        self.addTeeButton = AppButton(self.teesFrame, self.theListAddImage, self.theListAddImageGREY, AppBorderWidthList,
                            self._addNewteeButtonActionPressed)
        self.addTeeButton.addToolTip("Add a Tee")

        self.teesFrame.addTitle("Available Tees", self.addTeeButton)

        self.teeListingColumnCount = 11
        self.teeListingColumnHeader = [' ', 'Tee Name', 'Tee Type', 'Rating', 'Slope', 'Yardage', ' ', ' ', ' ', ' ', ' ']
        self.teeListingColumnWidth = [2, 6, 6, 6, 6, 6, 2, 2, 2, 2, 2]
        self.teeListingColumnWeight = [1, 20, 20, 8, 8, 10, 0, 0, 0, 0, 0]  # Weight of 0 remove any strectching of the column

        self._CreateTeeHeaderRow()
        self.teesFrame.addRow()
        self.teesFrame.stretchCurrentRow()

        self.teesCanvasRow = self.teesFrame.row
        # This is where the canvas will go.

        #       self.identificationFrame.noStretchColumn(0)
        ########################################################### Footer Section
        self.fieldsFrame.addRow()
        self.lastUpdate = AppLastUpdateFormat(self.fieldsFrame)
        self.lastUpdate.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, columnspan=colTotal,
                             sticky=E + N + S)

        self.setRequiredFields()
        self.fieldsDisable()

    #    cursor.execute('''CREATE TABLE Courses(
    #       courseID INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
    #       courseNAME          CHAR(60)    NOT NULL,
    #       courseSNAME         CHAR(30)    NOT NULL,
    #       courseLogo          CHAR(100),
    #       deleted              CHAR(1),
    #       lastModified         INTEGER,
    #       updaterID            INTEGER
    #       );'''
    def _aCountryEntered(self):
        pass

    def displayAddressesFrame(self, *args):
        self.identificationDATAAddressFrame.grid()
        self.identificationDATAEditAddFrame.grid_remove()

    def displayCoursesFieldsFrame(self):
        self.identificationDATAAddressFrame.grid_remove()
        self.identificationDATAEditAddFrame.grid()

    def deleteDependencies(self, ID):
        pass
        # Need to delete the dependencies (tees and teeDetails associated with course).
        aTeeList = CRUDTees.get_teeIDs_for_Course(ID)
        if len(aTeeList) > 0:
            # The DB open/close is done manually.This speeds up the process by using the
            # Mass version of the delete (does not open and close everytime)
            CRUD.initDB(getDBLocationFullName())
            for i in range(len(aTeeList)):
                CRUD.deleteMass('Tees',aTeeList[i][0], convertDateStringToOrdinal(todayDate),self.updaterID)
                aTeeDetailList = CRUDTees.get_teeDetailsIDs_for_Tee_OPEN(aTeeList[i][0])
                for j in range(len(aTeeDetailList)):
                    CRUD.deleteMass('TeeDetails', aTeeDetailList[j][0], convertDateStringToOrdinal(todayDate),self.updaterID)
            CRUD.closeDB()

        anAddressList = CRUDAddresses.get_AddressesForOwnerID(ID, self.tableName, False)
        for i in range(len(anAddressList)):
            CRUD.delete('Addresses', int(anAddressList[i][0]), convertDateStringToOrdinal(todayDate), self.updaterID)

    def unDeleteDependencies(self, ID):
        pass
        # Need to unDelete the dependencies (tees and teeDetails associated with course).
        aTeeList = CRUDTees.get_teeIDs_for_Course(ID)
        if len(aTeeList) > 0:
            # The DB open/close is done manually.This speeds up the process by using the
            # Mass version of the unDelete (does not open and close everytime)
            CRUD.initDB(getDBLocationFullName())
            for i in range(len(aTeeList)):
                CRUD.unDeleteMass('Tees',aTeeList[i][0], convertDateStringToOrdinal(todayDate),self.updaterID)
                aTeeDetailList = CRUDTees.get_teeDetailsIDs_for_Tee_OPEN(aTeeList[i][0])
                for j in range(len(aTeeDetailList)):
                    CRUD.unDeleteMass('TeeDetails',aTeeDetailList[j][0], convertDateStringToOrdinal(todayDate),self.updaterID)
            CRUD.closeDB()

        anAddressList = CRUDAddresses.get_AddressesForOwnerID(ID, self.tableName, False)
        for i in range(len(anAddressList)):
            CRUD.unDelete('Addresses', anAddressList[i][0], convertDateStringToOrdinal(todayDate),self.updaterID)

    def _CreateTeeHeaderRow(self):
        self.teeHeadersRow = AppBorderFrame(self.teesFrame, self.teeListingColumnCount)
        self.teeHeadersRow.grid(row=self.teesFrame.row, column=self.teesFrame.column, columnspan=self.teesFrame.columnTotal, sticky="wens")
        self.teeHeadersRow.removeBorder()

        for i in range(len(self.teeListingColumnHeader)):
            Label(self.teeHeadersRow, border=AppBorderWidthList, relief='flat', justify=LEFT, width=self.teeListingColumnWidth[i],
                  font=fontAverageB, text=self.teeListingColumnHeader[i]).grid(row=self.teeHeadersRow.row,
                                                                               column=self.teeHeadersRow.column,
                                                                               sticky=E + W + N + S)
            self.teeHeadersRow.addColumn()
            self.teeHeadersRow.stretchSpecifyColumnAndWeight(i, self.teeListingColumnWeight[i])
        self.teeHeadersRow.grid_remove()

    def updateUponReturnMethod(self, *args):
        self.populateTees()

    def populateTees(self):
        CRUD.initDB(getDBLocationFullName())
        courseID = self.recList[self.curRecNumV.get()][0]
        currentCourseTeeList = CRUDTees.get_teeList_for_Course_WindowMASS(courseID)
        if len(currentCourseTeeList) > 0:
            self.teesFrame.changeTitle("Available Tees")
            self.teeHeadersRow.grid() # Show the header if tees are available
            #
            #  This try creates the new canvas. If it does exist,
            # destroy the old one and create the new one.
            #
            try:
                self.teesCanvas.grid_info()
                self.teesCanvas.destroyAll()
                self._createCanvas()
            except:
                self._createCanvas()

        else:
            #
            # Rename the title
            #
            self.teesFrame.changeTitle("No Available Tees")
            self.teeHeadersRow.grid_remove() # remove header as well
            try:
                self.teesCanvas.grid_info()
                self.teesCanvas.destroyAll()
                return
            except:
                return

        col1Width = 2
        col2Width = 6
        for i in range(len(currentCourseTeeList)):
            aLine = self.teesCanvas.addAFrame(self.teeListingColumnCount)

            Label(aLine, border=AppBorderWidthList, relief='groove', justify=LEFT, width=col1Width,
                  text="{0}".format(i + 1)).grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)

            aLine.addColumn()
            Label(aLine, border=AppBorderWidthList, relief='groove', justify=LEFT, width=col2Width,
                  text=currentCourseTeeList[i][1]).grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)

            aLine.addColumn()
            Label(aLine, border=AppBorderWidthList, relief='groove', justify=LEFT, width=col2Width,
                  text=currentCourseTeeList[i][2]).grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)

            aLine.addColumn()
            Label(aLine, border=AppBorderWidthList, relief='groove', justify=LEFT, width=col2Width,
                  text=currentCourseTeeList[i][3]).grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)

            aLine.addColumn()
            Label(aLine, border=AppBorderWidthList, relief='groove', justify=LEFT, width=col2Width,
                  text=currentCourseTeeList[i][4]).grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)

            aLine.addColumn()
            Label(aLine, border=AppBorderWidthList, relief='groove', justify=LEFT, width=col2Width,
                  text=self._getTotalYardageTee(currentCourseTeeList[i][0])).grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)

            aLine.addColumn()

            aButton = AppButton(aLine, self.theListAddRoundImage, self.theListAddRoundImageGREY, AppBorderWidthList,
                                lambda event=Event, mode='addFromTee', teeID=currentCourseTeeList[i][0]: self._teeButtonActionPressed(mode, teeID))
            aButton.grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)
            aButton.addToolTip("Add A Round")

            aLine.addColumn()
            aButton = AppButton(aLine, self.theListEditImage, self.theListEditImageGREY, AppBorderWidthList,
                                lambda event=Event, mode='edit',teeID=currentCourseTeeList[i][0]: self._teeButtonActionPressed(mode, teeID))
            aButton.grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)
            aButton.addToolTip("Edit Tee")

            aLine.addColumn()
            aButton = AppButton(aLine, self.theListViewImage, self.theListViewImageGREY, AppBorderWidthList,
                                lambda event=Event, mode='view',teeID=currentCourseTeeList[i][0]: self._teeButtonActionPressed(mode, teeID))
            aButton.grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)
            aButton.addToolTip("View Tee Details")

            aLine.addColumn()
            aButton = AppButton(aLine, self.theListDuplicateImage, self.theListDuplicateImageGREY, AppBorderWidthList,
                                lambda event=Event, mode='duplicate',teeID=currentCourseTeeList[i][0]: self._teeButtonActionPressed(mode, teeID))
            aButton.grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)
            aButton.addToolTip("Make a Copy")

            aLine.addColumn()
            aButton = AppButton(aLine, self.theListArchiveImage, self.theListArchiveImageGREY, AppBorderWidthList,
                                lambda event=Event, mode='archive',teeID=currentCourseTeeList[i][0]: self._teeButtonActionPressed(mode, teeID))
            aButton.grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)
            aButton.addToolTip("Archive Tee")
            #
            #   This allows proper column width
            #

            for j in range(len(self.teeListingColumnWeight)):
                aLine.stretchSpecifyColumnAndWeight(j, self.teeListingColumnWeight[j])

            self.teesCanvas.addRow()
            self.teesCanvas.resetColumn()

        self.teesCanvas.ResizeScrollBar()
        CRUD.closeDB()

    def _getTotalYardageTee(self, teeID):
        totalYardage = 0
        teeDetails = CRUDTees.get_teeDetails_for_curTeeMASS(teeID)
        for i in range(len(teeDetails)):
            totalYardage = totalYardage + teeDetails[i][1]
        return totalYardage

    def _addNewteeButtonActionPressed(self, *args):
        self.displayAddTeeWindow('add', self.recList[self.curRecNumV.get()][0], None)

    def _teeButtonActionPressed(self, mode, anID, *args):
        if mode == 'addFromTee':
            self.displayAddGameWindow(mode, None, None, anID)
            # AppDisplayUnderConstruction(self)
        elif mode == 'edit' or mode == 'view' or mode == 'duplicate':
            self.displayAddTeeWindow(mode, self.recList[self.curRecNumV.get()][0], anID)
        elif mode == 'archive':
            AppDisplayUnderConstruction(self)
        else:
            aMsg="ERROR Loading Window"
            AppDisplayMessage(self, "Error load Add/Edit Tee Popup Window", aMsg)

    def _createCanvas(self):
        theRow = self.teesCanvasRow
        theColumnSpan = self.teesFrame.columnTotal
#        Label(self.teesFrame, border=AppBorderWidthList, relief='groove', justify=LEFT, width=5,
#              text='Testing the header Row').grid(row=theRow, column=0, columnspan=theColumnSpan, sticky=E + W + N + S)
        self.teesCanvas = MyScrollFrameVertical(self.teesFrame, theColumnSpan,
                                                self.teeListingColumnCount, theRow)

    def displayRec(self, *args):
        if len(self.recList) > 0:
            self.fieldsClear()
            self.selectedPicture.load(self.recList[self.curRecNumV.get()][3])
            self.selectedCourseParticular.load(self.recList[self.curRecNumV.get()])
            self.selectedLongCourseName.load(self.recList[self.curRecNumV.get()][1])
            self.selectedShortCourseName.load(self.recList[self.curRecNumV.get()][2])
            self.selectedEstablishedYear.load(self.recList[self.curRecNumV.get()][4])

            self.recAddressCurrentRecord = CRUDAddresses.get_courseAddresses(self.recList[self.curRecNumV.get()][0])
            if len(self.recAddressCurrentRecord) > 0:
                self.selectedTown.load(self.recAddressCurrentRecord[0][2])
                self.selectedCountry.load(self.recAddressCurrentRecord[0][4])
                self.selectedProv_state.load(self.recAddressCurrentRecord[0][3])
                self.selectedStreet_number.load(self.recAddressCurrentRecord[0][9])
                self.selectedWebsite.load(self.recAddressCurrentRecord[0][8])
                self.selectedPc_zip.load(self.recAddressCurrentRecord[0][6])
                self.selectedPhone.load(self.recAddressCurrentRecord[0][7])

            self.lastUpdate.load(self.recList[self.curRecNumV.get()][(self.numberOfFields - 2)],
                                 self.recList[self.curRecNumV.get()][(self.numberOfFields - 1)])
            self.deletedV.set(self.recList[self.curRecNumV.get()][self.numberOfFields - 3])

            self.populateTees()
            self.dirty = False
        else:
            self._displayRecordFrame()

    def _dateEnteredAction(self, *args):
        self._aDirtyMethod(None)

    def _verifyFutureDate(self, aDate):
            aDateOrdinal = convertDateStringToOrdinal(aDate)
            todayOrdinal = convertDateStringToOrdinal(strdate)
            if aDateOrdinal > todayOrdinal:
                return False
            else:
                return True

    def _startAddEditGolfer(self, mode, golferData):
        pass

    def OnChildClose(self):
        self.AddEditGolferWindow.destroy()

    def fieldsClear(self):
        self.selectedPicture.clear()
        self.selectedLongCourseName.clear()
        self.selectedShortCourseName.clear()
        self.selectedEstablishedYear.clear()

        self.selectedTown.clear()
        self.selectedCountry.clear()
        self.selectedProv_state.clear()
        self.selectedStreet_number.clear()
        self.selectedWebsite.clear()
        self.selectedPc_zip.clear()
        self.selectedPhone.clear()

    def fieldsDisable(self):
        self.identificationDATAEditAddFrame.grid_remove()
        self.identificationDATAAddressFrame.grid_remove()
        self.identificationDATADisplayFrame.grid()
        self.selectedPicture.disable()
        self.selectedLongCourseName.disable()
        self.selectedShortCourseName.disable()
        self.selectedEstablishedYear.disable()

        self.selectedTown.disable()
        self.selectedCountry.disable()
        self.selectedProv_state.disable()
        self.selectedStreet_number.disable()
        self.selectedWebsite.disable()
        self.selectedPc_zip.disable()
        self.selectedPhone.disable()

    def fieldsEnable(self):
        pass
        if self.curOp == 'find':
            self.identificationDATAEditAddFrame.grid()
            self.identificationDATADisplayFrame.grid_remove()
            self.selectedLongCourseName.enable()
            self.selectedShortCourseName.enable()
            self.selectedEstablishedYear.enable()
            if len(self.addressesButton.grid_info()) > 0:
                self.addressesButton.grid_remove()

        else:
            self.identificationDATAEditAddFrame.grid()
            self.identificationDATADisplayFrame.grid_remove()
            self.selectedPicture.enable()
            self.selectedLongCourseName.enable()
            self.selectedShortCourseName.enable()
            self.selectedEstablishedYear.enable()

            self.selectedTown.enable()
            self.selectedCountry.enable()
            self.selectedProv_state.enable()
            self.selectedStreet_number.enable()
            self.selectedWebsite.enable()
            self.selectedPc_zip.enable()
            self.selectedPhone.enable()
            if len(self.addressesButton.grid_info()) == 0:
                self.addressesButton.grid()

    ################################################################# Redefined commands
    #
    #  invoke from the tableCommonWindows.  These commands are specific to each tables
    #  The specific actions are redefinded here.
    #
    #    def displayRec(self, *args):
    #        print("Displaying a Record: ", self.curRecNumV.get())
    #        self._displayRecordFrame()
    def setRequiredFields(self):
        pass
        self.selectedLongCourseName.setAsRequiredField()
        self.selectedShortCourseName.setAsRequiredField()
        self.selectedTown.setAsRequiredField()
        self.selectedCountry.setAsRequiredField()
        self.selectedProv_state.setAsRequiredField()

    def resetRequiredFields(self):
        pass
        self.selectedLongCourseName.resetAsRequiredField()
        self.selectedShortCourseName.resetAsRequiredField()
        self.selectedTown.resetAsRequiredField()
        self.selectedCountry.resetAsRequiredField()
        self.selectedProv_state.resetAsRequiredField()

    def find(self):
        self.myMsgBar.wildcardMessage()
        self.fieldsClear()
        self.resetRequiredFields()
        if len(self.findValues) > 0:
            self.selectedLongCourseName.load(self.findValues[0])
            self.selectedShortCourseName.load(self.findValues[1])
            self.selectedEstablishedYear.load(self.findValues[2])
        self.fieldsEnable()
        self.dirty = False

    def edit(self):
        self.fieldsEnable()
        self.dirty = False

    def add(self):
        if len(self.recList) == 0:
            self._displayRecordFrame()
        self.fieldsClear()
        self.fieldsEnable()
        self.dirty = False

    def clearPopups(self):
        pass
        self.selectedCountry.clearPopups()

    def validateRequiredFields(self):
        #
        #  required Fields:
        #
        #
        requiredFieldsEntered = True

        if len(self.selectedLongCourseName.get()) == 0:
            if len(self.identificationDATADisplayFrame.grid_info()) == 0:
                self.displayCoursesFieldsFrame()
            requiredFieldsEntered = False
            self.selectedLongCourseName.focus()

        elif len(self.selectedShortCourseName.get()) == 0:
            if len(self.identificationDATADisplayFrame.grid_info()) == 0:
                self.displayCoursesFieldsFrame()
            requiredFieldsEntered = False
            self.selectedShortCourseName.focus()

        elif len(self.selectedTown.get()) == 0:
            if len(self.identificationDATAAddressFrame.grid_info()) == 0:
                self.displayAddressesFrame()
            requiredFieldsEntered = False
            self.selectedTown.focus()

        elif len(self.selectedCountry.get()) == 0:
            if len(self.identificationDATAAddressFrame.grid_info()) == 0:
                self.displayAddressesFrame()
            requiredFieldsEntered = False
            self.selectedCountry.focus()

        elif len(self.selectedProv_state.get()) == 0:
            if len(self.identificationDATAAddressFrame.grid_info()) == 0:
                self.displayAddressesFrame()
            requiredFieldsEntered = False
            self.selectedProv_state.focus()

        if requiredFieldsEntered == False:
            self.myMsgBar.requiredFieldMessage()
        return requiredFieldsEntered

    def save(self):
        #
        #  Table specific.  Return a True if succesful saved.
        #
        #
        #  House Cleaning in case a popup still exist
        #
        requiredFieldsEntered = self.validateRequiredFields()
        if self.curOp == 'add' and requiredFieldsEntered == True:
            pass
            try:
                if CRUDCourses.findCourseDuplicate(self.selectedLongCourseName.get(),
                                                   self.selectedShortCourseName.get()) == None:

                    CRUDCourses.insert_courses(self.selectedLongCourseName.get(), self.selectedShortCourseName.get(),
                                               self.selectedPicture.get(), self.selectedEstablishedYear.get(),
                                               'N', convertDateStringToOrdinal(todayDate), self.updaterID)

                    ID = CRUDCourses.getCourseID(self.selectedLongCourseName.get(), self.selectedShortCourseName.get(),
                                                       self.selectedEstablishedYear.get(), convertDateStringToOrdinal(todayDate))

                    anItem = (
                        ID,
                        self.selectedLongCourseName.get(),
                        self.selectedShortCourseName.get(),
                        self.selectedPicture.get(),
                        self.selectedEstablishedYear.get(),
                        'N',
                        convertDateStringToOrdinal(todayDate), self.updaterID
                    )

                    CRUDAddresses.insert_addresses("Courses", self.selectedTown.get(), self.selectedProv_state.get(),
                                                   self.selectedCountry.get(), ID,
                                                   self.selectedPc_zip.get(), self.selectedPhone.get(), self.selectedWebsite.get(),
                                                   self.selectedStreet_number.get(), 'N',
                                                   convertDateStringToOrdinal(todayDate), self.updaterID)

                    self._updateRecList(ID, anItem)
                    aMsg = "Course {0} has been updated.".format(self.selectedLongCourseName.get())
                    self.myMsgBar.newMessage('info', aMsg)
                    return True
                else:
                    aMsg = "ERROR: Course {0} already exist.".format(self.selectedLongCourseName.get())
                    #self.myMsgBar.newMessage('error', aMsg)
                    AppDisplayMessage(self, "Database Error", aMsg)
                    return False
            except:
                aMsg = "ERROR: An error occurred while writting to the MyGolf Database."
                AppDisplayMessage(self, "Database Error", aMsg)
                return False

        elif self.curOp == 'edit' and requiredFieldsEntered == True:
            pass
            ID = self.recList[self.curRecNumV.get()][0]
            anItem = (
                        ID,
                        self.selectedLongCourseName.get(),
                        self.selectedShortCourseName.get(),
                        self.selectedPicture.get(),
                        self.selectedEstablishedYear.get(),
                        self.recList[self.curRecNumV.get()][self.numberOfFields - 3],
                        convertDateStringToOrdinal(todayDate), self.updaterID
            )


            try:
                ans = CRUDCourses.findIfEditCreatesCourseDuplicate(
                                                                    ID,
                                                                    self.selectedLongCourseName.get(),
                                                                    self.selectedEstablishedYear.get(),
                                                                    False
                                                                  )
                if ans == True:
                    aMsg = "ERROR: Golf course {0} already exist.\n" \
                           "Updates to current golf course can not be completed\n" \
                           "as it would create a duplicate golf course.".format(self.selectedLongCourseName.get())
                    AppDisplayErrorMessage(self, "Duplicate Golf Course", aMsg)
                    return False
                else:
                    CRUDCourses.update_courses(ID, self.selectedLongCourseName.get(), self.selectedShortCourseName.get(),
                                               self.selectedPicture.get(), self.selectedEstablishedYear.get(),
                                               self.recList[self.curRecNumV.get()][self.numberOfFields - 3],
                                               convertDateStringToOrdinal(todayDate), self.updaterID)

                    idxRecListAll = getIndex(self.recList[self.curRecNumV.get()][0], self.recAll)

                    addrID = CRUDAddresses.get_AddresseID(self.recList[self.curRecNumV.get()][0])

                    if addrID != None:
                        CRUDAddresses.update_addresses(addrID, "Courses", self.selectedTown.get(), self.selectedProv_state.get(),
                                                       self.selectedCountry.get(), self.recList[self.curRecNumV.get()][0],
                                                       self.selectedPc_zip.get(), self.selectedPhone.get(), self.selectedWebsite.get(),
                                                       self.selectedStreet_number.get(),
                                                       convertDateStringToOrdinal(todayDate), self.updaterID, False)
                    self._updateRecListAll(idxRecListAll, ID, anItem)
                    aMsg = "Course {0} has been updated.".format(self.selectedLongCourseName.get())
                    self.myMsgBar.newMessage('info', aMsg)
                    return True
            except:
                aMsg = "ERROR: An error occurred while writing to the MyGolf Database."
                AppDisplayMessage(self, "Database Error", aMsg)
                return False

        elif self.curOp == 'find':
            # Important if validation for amount is on.  May create non wanted error
            # Cause by displaying a $ in front of amount and it is a non valid character
            # in an amount field.
            self.fieldsDisable()
            self.recListTemp = CRUDCourses.findCourses(
                                                        self.selectedLongCourseName.get(),
                                                        self.selectedShortCourseName.get(),
                                                        self.selectedEstablishedYear.get(),
                                                        False
                                                      )
            self.findValues = []
            self.findValues.append(self.selectedLongCourseName.get())
            self.findValues.append(self.selectedShortCourseName.get())
            self.findValues.append(self.selectedEstablishedYear.get())

            if len(self.recListTemp) == 0:
                aMsg = "WARNING: No records were found using the given search criterias."
                if self.prevFind == True:
                    self.displayCurFunction.setFindResultMode()
                    self.curFind = True
                else:
                    self.displayCurFunction.setDefaultMode()
                    self.curFind = False
                self.myMsgBar.newMessage('warning', aMsg)
            elif len(self.recList) == 1:
                self.curFind = True
                self.recList = self.recListTemp
                self.displayCurFunction.setFindResultMode()
                aMsg = "One record was found using the given search criterias."
                self.curRecNumV.set(0)
                self.myMsgBar.newMessage('info', aMsg)
            else:
                self.curFind = True
                self.recList = self.recListTemp
                self.displayCurFunction.setFindResultMode()
                aMsg = "{0} records were found using the given search criterias.".format(len(self.recList))
                self.curRecNumV.set(0)
                self.myMsgBar.newMessage('info', aMsg)
            self.displayRec()
            self.clearPopups()
            self.setRequiredFields()
            return True
        else:
            aMsg = "ERROR: An error during the execution of the Save command.\n" \
                   "Contact your System Administrator."
            AppDisplayMessage(self, "Save Command Error", aMsg)
            self.clearPopups()
            return False


