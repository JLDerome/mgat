##########################################################
#
#   Departments Table
#
#   This class implements the specific manipulation of the
# data for the ECE Departments Table that the TECH Shop requires.
#

import tkinter.tix as Tix
import tkinter.ttk as ttk
from MyGolfApp2016 import AppCRUD_FROMECECRUD as AppDB
import tkinter.filedialog as Fd
import tkinter.messagebox as Mb

from tkinter.constants import *

from MyGolfApp2016.AppMyClasses import *
from MyGolfApp2016.AppConstants import *

from MyGolfApp2016.AppProcFROMECE import *

from PIL import Image

class itemsWindow(tableCommonWindowClass):
    def __init__(self, aWindow, aCloseCommand, loginData):
        tableCommonWindowClass.__init__(self, aWindow, aCloseCommand, 'Items Parts', loginData[1])
        self.windowTitle = 'Items'
        self.loginData = loginData
        self.accessLevel = self.loginData[1]
        self.updaterID = self.loginData[2]
#######################################################################################
# Inherited all from tableCommanWindowClass
#  
#  This class intents to change the mainArea only.  Everything else must remain common
#       
        self.mainAreaRowCount = 0
        self.mainAreaColumnCount = 0       
        self._getNewRecList()
        self._reDisplayRecordFrame()
        self.mainArea.rowconfigure(self.mainAreaRowCount, weight=1)
        
############################################################ Area to add window specific commands        
        self.closeButton = ttk.Button(self.commandFrame, text='Test', style='CommandButton.TButton', 
                                 command= lambda: self.sampleCommand())
        self.closeButton.grid(row=0, column=0, sticky=N+S)
        

################################################################# Redefined commands
#
#  invoke from the tableCommonWindows.  These commands are specific to each tables
#  The specific actions are redefinded here.
#       
#    def displayRec(self, *args):
#        print("Displaying a Record: ", self.curRecNum)
#        
#    def save(self):
#        #
#        #  Table specific.  Return a True if succesful saved.
#        #
#        print("Saving a record (re-defined)")
#        return True
    

    def _enableWindowCommand(self):
        self.closeButton.config(state=NORMAL)

    def _disableWindowCommand(self):
        self.closeButton.config(state=DISABLED)
               
    def sampleCommand(self):
        self.myMsgBar.clearMessage()
        aMsg = 'INFO: Command area, specific to current window (Parts)'
        self.myMsgBar.newMessage('info', aMsg)

    def _getNewRecList(self):
#        self.recList = AppDB.getDepartments()
        self.curRecNumV.set(self.curRecNumV.get()) # forces display of record number
        
    def _reDisplayRecordFrame(self):
        self.recordFrame.destroy()
        self._getNewRecList()
        self._displayRecordFrame()
        if self.curOp == 'add':
            self.fieldsEnable()
        
    def _displayRecordFrame(self):
        self.recordFrame = AppFrame(self.mainArea, 1) 
        self.recordFrame.grid(row=self.mainAreaRowCount, column=self.mainAreaColumnCount, columnspan= self.mainAreaColumnTotal, sticky=N+S+E+W)
        
        if len(self.recList) > 0:
            self._buildDepartmentsFields(self.recordFrame)
            self.recordFrame.rowconfigure(self.recordFrame.row, weight=0)
            if self.curOp != 'add':        
                self.displayRec()
                self.navigationEnable(self.accessLevel)
        else:
            wMsg = Tix.Label(self.recordFrame, text='No Departments Records Found', font=fontMonstrousB)
            wMsg.grid(row=0, column=0, columnspan=self.recordFrame.columnTotal)
            
    def _buildDepartmentsFields(self, aFrame):
        self.fieldsFrame = AppFrame(aFrame, 3)
        self.fieldsFrame.grid(row=0, column=0, sticky=N+S+E+W)

        # Empty row for separation
        empty = AppSpaceRow(self.fieldsFrame)
        empty.grid(row=self.fieldsFrame.row, column=0, sticky=N+S)
        
# Horizontal field       
#        self.fieldsFrame.row = self.fieldsFrame.row + 1
#        self.fieldsFrame.rowconfigure(self.fieldsFrame.row, weight=0)
#        self.selectedCode = AppHorizontalFieldFrame(self.fieldsFrame, 'Code', 5)
#        self.selectedCode.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, columnspan = 2, sticky=N+S)
        
        # Empty row for separation
        self.fieldsFrame.row = self.fieldsFrame.row + 1
        self.fieldsFrame.rowconfigure(self.fieldsFrame.row, weight=0)
        empty = AppSpaceRow(self.fieldsFrame)
        empty.grid(row=self.fieldsFrame.row, column=0, sticky=N+S)

        # Empty row for separation
        self.fieldsFrame.row = self.fieldsFrame.row + 1
        self.fieldsFrame.rowconfigure(self.fieldsFrame.row, weight=0)
        empty = AppSpaceRow(self.fieldsFrame)
        empty.grid(row=self.fieldsFrame.row, column=0, sticky=N+S)
        
        self.fieldsDisable()
#        self.fieldsFrame.rowconfigure(self.fieldsFrame.row, weight=0)
#        self.headerList = ['Code', 'Name']
#        for i in range(len(self.headerList)):       
#            aLabel = ttk.Label(self.fieldsFrame, text=self.headerList[i], style='RegularFieldTitle.TLabel')
#            aLabel.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, sticky=N+S)
#            self.fieldsFrame.column = self.fieldsFrame.column + 1

    def displayRec(self, *args):
        #   IDX CREATE TABLE Departments(
        #    0   departmentsID           INTEGER PRIMARY KEY AUTOINCREMENT  NOT NULL,
        #    1   departmentsCode         CHAR(3)   NOT NULL,
        #    2   departmentsNAME         CHAR(60)  NOT NULL,
        #    3   departmentsMidSize      CHAR(20),
        #    4   departmentsShort        CHAR(10),
        #    5   departmentsDAList       CHAR(60),
        #    6   departmentsSecurity     CHAR(8),
        #    7   lastModified            INTEGER,
        #    8   updaterID               INTEGER
        #      );''')
        
        if self.accessLevel == 'Root':
            pass
            
        self.fieldsDisable()
        
    def fieldsDisable(self):
        pass
        
    def fieldsEnable(self):
        pass

################################################################# Redefined commands
#
#  invoke from the tableCommonWindows.  These commands are specific to each tables
#  The specific actions are redefinded here.
#       
#    def displayRec(self, *args):
#        print("Displaying a Record: ", self.curRecNumV.get())
#        self._displayRecordFrame()

    def refresh(self):
        self._reDisplayRecordFrame()

    def delete(self):
        self._disableWindowCommand()
        aMsg = "Are you sure you want to delete record {0}-{1}?".format(self.recList[self.curRecNumV.get()][1],self.recList[self.curRecNumV.get()][2])
        ans = Mb.askyesno('Delete Record', aMsg)
        if ans == True:
            AppDB.deleteDepartments(self.recList[self.curRecNumV.get()][0])
            aMsg = "Record {0}-{1} has been deleted.".format(self.recList[self.curRecNumV.get()][1],self.recList[self.curRecNumV.get()][2])
            self.myMsgBar.newMessage('info', aMsg)
            self._getNewRecList()
            if self.curRecBeforeOp != 0:
                self.curRecNumV.set(self.curRecBeforeOp - 1)
            self._reDisplayRecordFrame()
        self._enableWindowCommand()     
        self.navigationEnable(self.accessLevel)
        self.myDatabaseBar.defaultState()    
        self.curOp = 'default'

    def edit(self):
        self.fieldsEnable()
                 
    def add(self):
        self._reDisplayRecordFrame() # Display the record area in Add operation instead

    def save(self):
        #
        #  Table specific.  Return a True if succesful saved.
        #
        if self.curOp == 'add':
            
            AppDB.insertDepartments(self.selectedCode.get(), self.selectedName.get(),  self.selectedMidName.get(), self.selectedShortName.get(), self.selectedDAList.get(),
                                    self.selectedSecurity.get(), convertDateStringToOrdinal(todayDate), self.updaterID)               
            aMsg = "Record {0}-{1} has been added to the {2} Table.".format(self.selectedCode.get(),self.selectedName.get(), self.windowTitle)
            self.myMsgBar.newMessage('info', aMsg)
            self._getNewRecList()
            self._reDisplayRecordFrame()
            
        elif self.curOp == 'edit':
            pass 
            AppDB.updateDepartments(self.recList[self.curRecNumV.get()][0], self.selectedCode.get(), self.selectedName.get(),  self.selectedMidName.get(), self.selectedShortName.get(), 
                                    self.selectedDAList.get(), self.selectedSecurity.get(), convertDateStringToOrdinal(todayDate), self.updaterID
                                    )
            aMsg = "Record {0}-{1} has been updated to the {2} Table.".format(self.selectedCode.get(),self.selectedName.get(), self.windowTitle)
            self.myMsgBar.newMessage('info', aMsg)
            self._getNewRecList()
            self._reDisplayRecordFrame()
        return True
     


        