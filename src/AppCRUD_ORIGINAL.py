'''
Lending library database API

Provides a CRUD interface to item and member entities 
and init and close functions for database control.
'''

import sqlite3 as sql
import os
import csv

db=None
cursor = None
lidGolfers = 0

##### CRUD functions for items ######
#'''CREATE TABLE Golfers(
#               golferID INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
#               golferFNAME          CHAR(30)    NOT NULL,
#               golferLNAME          CHAR(30)    NOT NULL,
#               golferPIC_LOC        CHAR(100),
#               golferBirthday       INTEGER

   
def insert_golfer(golferFNAME, golferLNAME,golferPIC_LOC, golferBirthday):
    # id is declared but is auto increment
    query = ''' 
    insert into Golfers 
    (golferFNAME, golferLNAME, golferPIC_LOC, golferBirthday)
    values (?,?,?,?)'''
    cursor.execute(query,(golferFNAME,golferLNAME,golferPIC_LOC, golferBirthday))
    
def exportCSV_golfers(fileName):
    query = '''select * from Golfers'''
    data = cursor.execute(query).fetchall()
    with open(fileName, 'w', newline='') as fout:
        for i in range(len(data)):
            csv.writer(fout).writerow(data[i])

def importCSV_golfers(fileName):
    with open(fileName, 'r') as fin:
        reader = csv.reader(fin)
        for row in reader:
            query = '''
            insert into Golfers (golferID,golferFNAME,golferLNAME,golferPIC_LOC, golferBirthday)
            values (?, ?, ?, ?, ?)'''
            cursor.execute(query, (row[0],row[1],row[2],row[3],row[4]))

def get_golfers():
    query = '''  
    select * from Golfers ORDER BY golferLNAME'''
    return cursor.execute(query).fetchall()

def getGolferCB():
    query = '''
    select golferID, golferFNAME, golferLNAME
    from Golfers ORDER BY golferLNAME'''
    return cursor.execute(query).fetchall()

def getGolferSpecial():
    query = '''
    select golferFNAME, golferLNAME
    from Golfers ORDER BY golferLNAME'''
    return cursor.execute(query).fetchall()

def get_col_names(tableName):
    """Retrieve column names for given table in database."""
    cursor.execute("PRAGMA table_info('{}')".format(tableName))
    # Currently only uses one value from returned tuple.
    # TODO: Add logic to utilize type, not null and PK fields.
    col_names = [x[1] for x in cursor.fetchall()]
    return col_names

def get_golfer_details(golferID):
    query = ''' 
    select *
    from Golfers
    where golferID = ?'''
    return cursor.execute(query,(golferID,)).fetchall()[0]

def get_golfer_name(golferID):
    return get_golfer_details(golferID)[0]
    
def update_Golfer(golferID, FName=None, LName=None,  golferPIC_LOC=None, golferBirthday=None):
    query = ''' 
    update Golfers 
    set golferFNAME=?, golferLNAME=?, golferPIC_LOC=?, golferBirthday=?
    where golferID=?'''
    data = get_golfer_details(golferID)
    if not FName: FName = data[0]
    if not LName: LName = data[1]
    if not golferPIC_LOC: golferPIC_LOC = data[2]
    if not golferBirthday: golferBirthday = data[3]

    cursor.execute(query, (FName,LName,golferPIC_LOC,golferBirthday,golferID))
    
def delete_golfer(golferID):
    query = '''
    delete from Golfers
    where golferID = ?'''
    cursor.execute(query,(golferID,))

##### CRUD functions for members ######
#CREATE TABLE Courses(
#   courseID INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
#   courseNAME          CHAR(60)    NOT NULL,
#   courseSNAME         CHAR(30)    NOT NULL,
#   courseLogo          CHAR(100)
#);


def find_course(nameValue, sNameValue):
    if len(nameValue) > 0 and len(sNameValue):
        query = '''
            select * from Courses where courseNAME like '{0}' or courseSNAME like '{1}' '''.format(nameValue, sNameValue)
    elif len(nameValue) > 0:
        query = '''
            select * from Courses where courseNAME like '{0}' '''.format(nameValue)
    elif len(sNameValue) > 0:
        query = '''
            select * from Courses where courseSNAME like '{0}' '''.format(sNameValue)
    return cursor.execute(query).fetchall()

def insert_courses(courseNAME, courseSNAME, courseLogo):
    query = '''
    insert into Courses (courseNAME, courseSNAME, courseLogo)
    values (?, ?, ?)'''
    cursor.execute(query, (courseNAME,courseSNAME, courseLogo))

def exportCSV_courses(fileName):
    query = '''
    select * from Courses ORDER BY courseNAME'''
    data = cursor.execute(query).fetchall()
    with open(fileName, 'w', newline='') as fout:
        for i in range(len(data)):
            csv.writer(fout).writerow(data[i])

def importCSV_courses(fileName):
    with open(fileName, 'r') as fin:
        reader = csv.reader(fin)
        for row in reader:
            # row[4] must be a valid courseID
            query = '''
            insert into Courses (courseID, courseNAME, courseSNAME, courseLogo)
            values (?, ?, ?, ?)'''
            cursor.execute(query, (row[0],row[1],row[2],row[3]))

def get_course_sname(courseID):
    query = '''select courseSNAME from Courses where courseID={0}'''.format(courseID)
    return cursor.execute(query).fetchall()

def get_courses():
    query = '''
    select * from Courses ORDER BY courseNAME'''
    return cursor.execute(query).fetchall()

def getCourseCB():
    query = '''
    select courseID, courseSNAME
    from Courses ORDER BY courseSNAME'''
    return cursor.execute(query).fetchall()

def get_course_details(idCourse):
    query = ''' 
    select * from Courses
    where courseID = ?'''
    return cursor.execute(query, (idCourse,)).fetchall()[0]

def get_course_name(idCourse):
    return get_course_details(idCourse)[0]

def update_courses(idCourse, Name=None, sName=None, courseLogo=None):
    query = '''
    update Courses 
    set courseNAME=?, courseSNAME=?, courseLogo=?
    where courseID = ?'''
    data = get_course_details(idCourse)
    if not Name: Name = data[1]
    if not sName: sName = data[2]
    if not courseLogo: courseLogo = data[3]
    cursor.execute(query, (Name, sName, courseLogo, idCourse))

def delete_course(idCourse):
    query = '''
    delete from Courses
    where courseID = ?'''    
    cursor.execute(query,(idCourse,))
    #
    # Make all associated Tees are also deleted
    #
    query = '''
    delete from Tees
    where courseID = ?''' 
    cursor.execute(query,(idCourse,))
    
#CREATE TABLE GameDetails(
#               gameDetailID  INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
#               holeNumber    SMALLINT    NOT NULL,
#               holeScore     SMALLINT    NOT NULL,
#               holePutts     SMALLINT,
#               holeGreens    SMALLINT,
#               holeFairways  CHAR(4),
#               holeDriveDistance  SMALLINT,
#               holeSands     CHAR(4),
#               gameID        SMALLINT    NOT NULL,
#               FOREIGN KEY(gameID) REFERENCES Games(gameID) ON DELETE CASCADE
#               );
def get_game_details():
    query = '''select * from GameDetails'''
    return cursor.execute(query).fetchall()

def delete_game_details_for_gameID(gameID):
    query = '''delete from GameDetails
    where gameID = {0}''' .format(gameID)
    cursor.execute(query)
    

def get_game_details_for_game(gameID):
    query = '''select * from GameDetails where gameID={0}'''.format(gameID)
    return cursor.execute(query).fetchall()

def update_game_details_for_gameDetailID(gameDetailID, holeNumber,holeScore,holePutts,holeGreens,holeFairways,holeDriveDistance, holeSands,gameID):
    query = '''
    update GameDetails
    set holeNumber=?, holeScore=?, holePutts=?, holeGreens=?, 
        holeFairways=?, holeDriveDistance=?, holeSands=?, gameID=? 
    where gameDetailID=?'''
    cursor.execute(query, (holeNumber,holeScore,holePutts,holeGreens,holeFairways,holeDriveDistance, holeSands, gameID, gameDetailID))

def insert_game_details(holeNumber,holeScore,holePutts,holeGreens,holeFairways,holeDriveDistance, holeSands,gameID):
    query = '''
    insert into GameDetails (holeNumber, holeScore, holePutts, holeGreens, holeFairways, holeDriveDistance, holeSands, gameID) 
    values (?, ?, ?, ?, ?, ?, ?, ?)'''
    cursor.execute(query, (holeNumber,holeScore,holePutts,holeGreens,holeFairways,holeDriveDistance, holeSands, gameID))

def exportCSV_gameDetails(fileName):
    query = '''select * from GameDetails'''
    data = cursor.execute(query).fetchall()
    with open(fileName, 'w', newline='') as fout:
        for i in range(len(data)):
            csv.writer(fout).writerow(data[i])
            
def importCSV_gameDetails(fileName):
    with open(fileName, 'r') as fin:
        reader = csv.reader(fin)
        for row in reader:
            # row[5] must be a valid courseID
            query = '''
            insert into GameDetails (gameDetailID, holeNumber, holeScore, holePutts, holeGreens, 
                                     holeFairways, holeDriveDistance, holeSands, gameID) 
            values (?, ?, ?, ?, ?, ?, ?, ?, ?)'''
            cursor.execute(query, (row[0],row[1],row[2],row[3],row[4], row[5], row[6], row[7], row[8]))
    


#CREATE TABLE Games(
#          0     gameID          INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
#          1     gameDate        INTEGER     NOT NULL,
#          2     gameNumber      SMALLINT    NOT NULL,
#          3     teeID           SMALLINT    NOT NULL,
#          4     golferID        SMALLINT    NOT NULL,
#          5     frontScore      SMALLINT,
#          6     backScore       SMALLINT,
#          7     grossScore      SMALLINT,
#          8     curHdcp         REAL,
#          9     dEagleCNT       SMALLINT,
#         10     eagleCNT        SMALLINT,
#         11     birdieCNT       SMALLINT,
#         12     parCNT          SMALLINT,
#         13     bogieCNT        SMALLINT,
#         14     dBogieCNT       SMALLINT,
#         15     tBogieCNT       SMALLINT,
#         16     othersCNT       SMALLINT,
#         17     greens          SMALLINT,
#         18     fairways        SMALLINT,
#         19     leftMiss        SMALLINT,
#         20     rightMiss       SMALLINT,
#         21     avgDrive        SMALLINT, has been changed to total driving yards
#         22     totalSands      SMALLINT,
#         23     sandSaves       SMALLINT,
#         24     gameRating      REAL,
#         25     gameSlope       SMALLINT,
#         26     holesPlayed     SMALLINT,
#         27     totalPutts      SMALLINT,
#         28     putts3AndUp     SMALLINT,
#         29     totalYard       SMALLINT,
#         30     courseID        SMALLINT,
#               );
def get_games():
    query = '''select * from Games ORDER BY gameDate'''
    return cursor.execute(query).fetchall()

def delete_game_for_gameID(gameID):
    query = '''delete from Games
    where gameID = {0}''' .format(gameID)
    cursor.execute(query)

def get_game_info_for_gameID(gameID):
    query = '''select * from Games where gameID = {0} ORDER BY gameDate'''.format(gameID)
    return cursor.execute(query).fetchall()

def get_games_forIdx_RangeDate_for_golfer(golferID, ordinalStartDate, ordinalEndDate):
    # latest played game first
    query = '''select gameDate, hdcpDiff, holesPlayed from Games where
             golferID = {0} and holesPlayed = 'All' and gameDate>={1} and gameDate<={2} ORDER BY gameDate DESC'''.format(golferID, ordinalStartDate, ordinalEndDate)
    return cursor.execute(query).fetchall()

def get_games_hdcp_diff_before_date_for_golfer(aOrdinalDate, golferID, curGameID, curGameNum):
    # All games before a date
#    query = '''select gameDate, hdcpDiff, gameNumber from Games where golferID = {0} and gameDate <= {1} and gameID != {2} and gameNumber < {3} ORDER BY gameDate DESC'''.format(golferID, aOrdinalDate, curGameID, curGameNum)
    query = '''select gameDate, hdcpDiff, gameNumber, holesPlayed from Games where golferID = {0} and gameDate <= {1} and holesPlayed = 'All'
               ORDER BY gameDate DESC, gameNumber DESC'''.format(golferID, aOrdinalDate, curGameID)
               
#    query = '''select gameDate, hdcpDiff, gameNumber from Games where golferID = {0} and gameDate <= {1} and gameID != {2} 
#               ORDER BY gameDate DESC, gameNumber DESC'''.format(golferID, aOrdinalDate, curGameID, curGameNum)
    return cursor.execute(query).fetchall()
    
def get_games_hdcp_diff_for_golfer(golferID):
    # latest played game first
    query = '''select gameDate, hdcpDiff, holesPlayed from Games where golferID = {0} and holesPlayed = 'All' ORDER BY gameDate DESC'''.format(golferID)
    return cursor.execute(query).fetchall()

def get_18hole_games_for_golfer(golferID):
    query = '''select * from Games where golferID = {0} and holesPlayed = 'All' ORDER BY gameDate DESC, gameNumber DESC'''.format(golferID)
    return cursor.execute(query).fetchall()

def get_18hole_games_for_golfer_on_course_dateRange(golferID, courseID, startOrdinal, endOrdinal):
    query = '''select * from Games where golferID = {0} and courseID={1} and gameDate >= {2} and gameDate <= {3} and holesPlayed = 'All'  
               ORDER BY gameDate DESC, gameNumber DESC'''.format(golferID, courseID, startOrdinal, endOrdinal)
    return cursor.execute(query).fetchall()

def get_18hole_games_for_golfer_dateRange(golferID, startOrdinal, endOrdinal):
    query = '''select * from Games where golferID = {0} and gameDate >= {1} and gameDate <= {2} and holesPlayed = 'All' 
               ORDER BY gameDate DESC, gameNumber DESC'''.format(golferID, startOrdinal, endOrdinal)
    return cursor.execute(query).fetchall()

def get_18hole_games_for_golfer_on_course(golferID, courseID):
    query = '''select * from Games where golferID = {0} and courseID={1} and holesPlayed = 'All' ORDER BY gameDate DESC, gameNumber DESC'''.format(golferID, courseID)
    return cursor.execute(query).fetchall()

def get_games_for_golfer_on_course_dateRange(golferID, courseID, startOrdinal, endOrdinal):
    query = '''select * from Games where golferID = {0} and courseID={1} and gameDate >= {2} and gameDate <= {3} 
               ORDER BY gameDate DESC, gameNumber DESC'''.format(golferID, courseID, startOrdinal, endOrdinal)
    return cursor.execute(query).fetchall()

def get_games_for_golfer_dateRange(golferID, startOrdinal, endOrdinal):
    query = '''select * from Games where golferID = {0} and gameDate >= {1} and gameDate <= {2} 
               ORDER BY gameDate DESC, gameNumber DESC'''.format(golferID, startOrdinal, endOrdinal)
    return cursor.execute(query).fetchall()

def get_games_for_golfer_on_course(golferID, courseID):
    query = '''select * from Games where golferID = {0} and courseID={1} ORDER BY gameDate DESC, gameNumber DESC'''.format(golferID, courseID)
    return cursor.execute(query).fetchall()

def get_games_for_golfer(golferID):
    query = '''select * from Games where golferID = {0} ORDER BY gameDate DESC, gameNumber DESC'''.format(golferID)
    return cursor.execute(query).fetchall()

def get_games_played_today_for_golfer(golferID, ordinalDate):
    query = '''select * from Games where golferID = {0} and gameDate = {1} ORDER BY gameDate DESC, gameNumber DESC'''.format(golferID, ordinalDate)
    return cursor.execute(query).fetchall()

def get_games_for_period_golfer(golferID, startDate, endDate):
    query = '''select * from Games where golferID = {0} and gamedate >= {1} and gameDate <= {2} ORDER BY holesPlayed DESC'''.format(golferID, startDate,endDate)
    return cursor.execute(query).fetchall()

def find_gamePlayedDay(gameDate, gameNum, golferID):
#    query ='''select gameID from Games where gameDate = {0} and gameNumber = {1} and teeID = {2} and golferID = {3}'''.format(gameDate, gameNumber, teeID, golferID)
    query ='''select gameID, gameNumber, holesPlayed from Games where gameDate = {0} and gameNumber = {1} and golferID = {2}'''.format(gameDate, gameNum, golferID)
    return cursor.execute(query).fetchall()

def get_gameID(gameDate, gameNumber, teeID, golferID):
#    query ='''select gameID from Games where gameDate = {0} and gameNumber = {1} and teeID = {2} and golferID = {3}'''.format(gameDate, gameNumber, teeID, golferID)
    query ='''select gameID from Games where gameDate = "{0}" and gameNumber = {1} and teeID = {2} and golferID = {3}'''.format(gameDate, gameNumber, teeID, golferID)
    return cursor.execute(query).fetchall()

def insert_games(gameDate,gameNumber,teeID,golferID,frontScore,backScore,grossScore,
                 hdcpDiff,dEagleCNT,eagleCNT,birdieCNT,parCNT,bogieCNT,dBogieCNT,tBogieCNT,othersCNT,
                 greens,fairways,leftMiss,rightMiss,avgDrive,totalSands,sandSaves,gameRating,gameSlope,
                 holesPlayed,totalPutts,putts3AndUp,totalYard,courseID):
    query = '''
    insert into Games(gameDate,gameNumber,teeID,golferID,frontScore,backScore,grossScore,
                     hdcpDiff,dEagleCNT,eagleCNT,birdieCNT,parCNT,bogieCNT,dBogieCNT,tBogieCNT,othersCNT,
                     greens,fairways,leftMiss,rightMiss,avgDrive,totalSands,sandSaves,gameRating,gameSlope,
                     holesPlayed,totalPutts,putts3AndUp,totalYard,courseID)
    values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
            ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'''
    cursor.execute(query, (gameDate,gameNumber,teeID,golferID,frontScore,backScore,grossScore,
                           hdcpDiff,dEagleCNT,eagleCNT,birdieCNT,parCNT,bogieCNT,dBogieCNT,tBogieCNT,othersCNT,
                           greens,fairways,leftMiss,rightMiss,avgDrive,totalSands,sandSaves,gameRating,gameSlope,
                           holesPlayed,totalPutts,putts3AndUp,totalYard,courseID))

def update_game_coursID(gameID, courseID):
    query = '''update Games set courseID={0} where gameID = {1}'''.format(courseID, gameID)
    cursor.execute(query)

def update_Games_Rating_Slope(gameID, gameRating, gameSlope):
    query = '''update Games set gameRating={0}, gameSlope={1}  where gameID = {2}'''.format(gameRating, gameSlope, gameID)
    cursor.execute(query)
    
def update_Games_HdcpDiff(gameID, gameHdcpDiff):
    query = '''update Games set hdcpDiff={0} where gameID = {1}'''.format(gameHdcpDiff, gameID)
    cursor.execute(query)
    
def update_game(gameID, gameDate,gameNumber,teeID,golferID,frontScore,backScore,grossScore,
                 hdcpDiff,dEagleCNT,eagleCNT,birdieCNT,parCNT,bogieCNT,dBogieCNT,tBogieCNT,othersCNT,
                 greens,fairways,leftMiss,rightMiss,avgDrive,totalSands,sandSaves,gameRating,gameSlope,
                 holesPlayed,totalPutts,putts3AndUp, totalYard,courseID):
    query = '''update Games set gameDate={1},gameNumber={2},teeID={3},golferID={4},frontScore={5},
                                backScore={6},grossScore={7},hdcpDiff={8},dEagleCNT={9},eagleCNT={10},
                                birdieCNT={11},parCNT={12},bogieCNT={13},dBogieCNT={14},tBogieCNT={15},
                                othersCNT={16},greens={17},fairways={18},leftMiss={19},rightMiss={20},
                                avgDrive={21},totalSands={22},sandSaves={23},gameRating={24},gameSlope={25},  
                                totalPutts={27},putts3AndUp={28},totalYard={29},courseID={30} where gameID = {0}'''.format(gameID, gameDate,gameNumber,teeID,golferID,
                                                                                                             frontScore,backScore,grossScore,hdcpDiff,dEagleCNT,
                                                                                                             eagleCNT,birdieCNT,parCNT,bogieCNT,dBogieCNT,tBogieCNT,
                                                                                                             othersCNT,greens,fairways,leftMiss,rightMiss,avgDrive,
                                                                                                             totalSands,sandSaves,gameRating,gameSlope,holesPlayed,
                                                                                                             totalPutts,putts3AndUp,totalYard,courseID)
    cursor.execute(query)
       
def exportCSV_games(fileName):
    query = '''select * from Games'''
    data = cursor.execute(query).fetchall()
    with open(fileName, 'w', newline='') as fout:
        for i in range(len(data)):
            csv.writer(fout).writerow(data[i])

def importCSV_games(fileName):
    with open(fileName, 'r') as fin:
        reader = csv.reader(fin)
        for row in reader:
            # row[5] must be a valid courseID
            query = '''
            insert into Games (gameID, gameDate, gameNumber, teeID, golferID, frontScore, backScore, grossScore,
                               hdcpDiff, dEagleCNT, eagleCNT, birdieCNT, parCNT, bogieCNT, dBogieCNT, tBogieCNT, 
                               othersCNT, greens, fairways, leftMiss, rightMiss, avgDrive, totalSands, sandSaves, 
                               gameRating, gameSlope, holesPlayed,totalPutts,putts3AndUp, totalYard, courseID)
            values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'''
            cursor.execute(query, (row[0],row[1],row[2],row[3],row[4], row[5], row[6], row[7], row[8], row[9],
                                   row[10],row[11],row[12],row[13],row[14], row[15], row[16], row[17], row[18], row[19],
                                   row[20],row[21],row[22],row[23],row[24], row[25], row[26], row[27], row[28], row[29],
                                   row[30]))
   
#CREATE TABLE Tees(
#        teeID INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
#        teeNAME         CHAR(15)    NOT NULL,
#        teeType         CHAR(8)    NOT NULL,
#        teeRATING       REAL        NOT NULL,
#        teeSLOPE        SMALLINT    NOT NULL,
#        courseID        INTEGER     NOT NULL,
#        archived        SMALLINT,   
#        FOREIGN KEY(courseID) REFERENCES Courses(courseID) ON DELETE CASCADE
#       );
def get_tees():
    query = '''select * from Tees where archived = 0 ORDER BY courseID '''
    return cursor.execute(query).fetchall()

def get_tee_info_for_teeID(teeID):
    query = '''select * from Tees where teeID = {0} ORDER BY courseID'''.format(teeID)
    return cursor.execute(query).fetchall()

def get_teeName_CourseID(teeID):
    query = '''select teeName, teeType, courseID from Tees where teeID = {0}'''.format(teeID)
    return cursor.execute(query).fetchall()

def get_teeList_for_Course_Window(courseID):
    query = '''select * from Tees where courseID = {0} order by teeType, teeSLOPE DESC'''.format(courseID)
    return cursor.execute(query).fetchall()

def get_tee_rating(teeID):
    query = '''select teeRATING from Tees where teeID = {0}'''.format(teeID)
    return cursor.execute(query).fetchall()

def get_tee_slope(teeID):
    query = '''select teeSLOPE from Tees where teeID = {0}'''.format(teeID)
    return cursor.execute(query).fetchall()

def get_tee_name_from_teeID(teeID):
    query = '''select teeNAME from Tees where teeID = {0}'''.format(teeID)
    return cursor.execute(query).fetchall()

def get_teeListCB_ForACourse(courseID):
    query = '''select teeID, teeNAME, teeType, teeRATING, teeSLOPE from Tees where courseID = {0}'''.format(courseID)
    return cursor.execute(query).fetchall()
   
def exportCSV_tees(fileName):
    query = '''select * from Tees'''
    data = cursor.execute(query).fetchall()
    with open(fileName, 'w', newline='') as fout:
        for i in range(len(data)):
            csv.writer(fout).writerow(data[i])

def importCSV_tees(fileName):
    with open(fileName, 'r') as fin:
        reader = csv.reader(fin)
        for row in reader:
            # row[5] must be a valid courseID
            query = '''
            insert into Tees (teeID, teeNAME,teeType,teeRATING,teeSLOPE,courseID,archived)
            values (?, ?, ?, ?, ?, ?, ?)'''
            cursor.execute(query, (row[0],row[1],row[2],row[3],row[4], row[5], row[6]))
    
def insert_tees(teeNAME,teeType,teeRATING,teeSLOPE,courseID, archived):
    query = '''
    insert into Tees (teeNAME,teeType,teeRATING,teeSLOPE,courseID, archived)
    values (?, ?, ?, ?, ?, ?)'''
    cursor.execute(query, (teeNAME,teeType,teeRATING,teeSLOPE,courseID, archived))


def update_tee_archive(archived, idTee):
    print("I am here: ", archived)
    query = '''
    update Tees set archived=? 
    where teeID = ?'''
    cursor.execute(query, (archived, idTee))

def update_tees(idTee, TeeName, Type, Rating, Slope, CourseID, archived):
    query = '''
    update Tees 
    set teeNAME=?, teeType=?, teeRATING=?, teeSLOPE=?, courseID=?, archived=? 
    where teeID = ?'''
    cursor.execute(query, (TeeName, Type, Rating, Slope, CourseID, archived, idTee))

def delete_tee(idTee):
    query = '''
    delete from Tees
    where teeID = ?'''    
    cursor.execute(query,(idTee,))
    
#CREATE TABLE TeeDetails(
#              teeDetailID INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
#              teeYardage  SMALLINT    NOT NULL,
#              teeHandicap SMALLINT    NOT NULL,
#              teePar      SMALLINT    NOT NULL,
#              teeID       INTEGER    NOT NULL,
#              FOREIGN KEY(teeID) REFERENCES Tees(teeID) ON DELETE CASCADE
#              );
def get_teeDetails():
    query = '''select * from TeeDetails'''
    return cursor.execute(query).fetchall()

def get_teeDetails_for_curTee(curTeeID):
    query = '''select * from TeeDetails where teeID == {0}'''.format(curTeeID)
    return cursor.execute(query).fetchall()

def get_teePar_for_curTee(curTeeID):
    query = '''select teePar from TeeDetails where teeID == {0} order by teeDetailID'''.format(curTeeID)
    return cursor.execute(query).fetchall()

def insert_teeDetails(teeYardage, teeHandicap ,teePar, teeID):
    query = '''
    insert into TeeDetails (teeYardage, teeHandicap ,teePar, teeID)
    values (?, ?, ?, ?)'''
    cursor.execute(query, (teeYardage, teeHandicap ,teePar, teeID))

def update_teeDetails(teeDetailID, teeYardage, teeHandicap ,teePar, teeID):
    query = '''
    update TeeDetails 
    set teeYardage=?, teeHandicap=?, teePar=?, teeID=? 
    where teeDetailID == ?'''
    cursor.execute(query, (teeYardage, teeHandicap ,teePar, teeID, teeDetailID))

def delete_teeDetails(idTee):
    query = '''
    delete from TeeDetails
    where teeID = ?'''    
    cursor.execute(query,(idTee,))

def exportCSV_teeDetails(fileName):
    query = '''select teeDetailID, teeYardage, teeHandicap ,teePar, teeID from TeeDetails'''
    data = cursor.execute(query).fetchall()
    with open(fileName, 'w', newline='') as fout:
        for i in range(len(data)):
            csv.writer(fout).writerow(data[i])
            
def importCSV_teeDetails(fileName):
    with open(fileName, 'r') as fin:
        reader = csv.reader(fin)
        for row in reader:
            # row[5] must be a valid courseID
            query = '''
            insert into TeeDetails (teeDetailID, teeYardage, teeHandicap ,teePar, teeID)
            values (?, ?, ?, ?, ?)'''
            cursor.execute(query, (row[0],row[1],row[2],row[3],row[4]))
    
#CREATE TABLE Addresses(
#               AddrID  INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
#               ownerTableName CHAR(30),
#               town           CHAR(30),
#               prov_state     CHAR(30),
#               country        CHAR(30),
#               ownerID        SMALLINT    NOT NULL
#               pc_zip         CHAR(10),
#               phone          CHAR(10),
#               website        CHAR(40),
#               street_number  CHAR(60)
#               );
def get_AllAddrDetails():
    query = '''select * from Addresses'''
    return cursor.execute(query).fetchall()

def get_courseAddresses(courseID):
    query = '''select * from Addresses where ownerTableName='Courses' and ownerID = {0}'''.format(courseID)
    return cursor.execute(query).fetchall()

def get_golferAddresses(golferID):
    query = '''select * from Addresses where ownerTableName='Golfers' and ownerID = {0}'''.format(golferID)
    return cursor.execute(query).fetchall()

def update_addresses(AddrID,ownerTable, town, provState, country, ownerID, pc_zip,phone,website,street_number):
    query = '''
    update Addresses 
    set ownerTableName=?, town=?, prov_state=?, country=?, ownerID=?, pc_zip=?,phone=?,website=?,street_number=?
    where AddrID == ?'''
    cursor.execute(query, (ownerTable, town, provState, country, ownerID, pc_zip,phone,website,street_number, AddrID))

def insert_addresses(ownerTable, town, provState, country, ownerID, pc_zip,phone,website,street_number):
    query = '''
    insert into Addresses (ownerTableName, town, prov_state, country, ownerID,
                           pc_zip,phone,website,street_number)
    values (?, ?, ?, ?, ?, ?, ?, ?, ?)'''
    cursor.execute(query, (ownerTable, town, provState, country, ownerID, pc_zip,phone,website,street_number))
    
    
def exportCSV_addresses(fileName):
    query = '''select * from Addresses'''
    data = cursor.execute(query).fetchall()
    with open(fileName, 'w', newline='') as fout:
        for i in range(len(data)):
            csv.writer(fout).writerow(data[i])

def importCSV_addresses(fileName):
    with open(fileName, 'r') as fin:
        reader = csv.reader(fin)
        for row in reader:
            # row[5] must be a valid courseID
            query = '''
            insert into Addresses (AddrID,ownerTableName,town,prov_state,country,ownerID,
                                   pc_zip,phone,website,street_number)
            values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'''
            cursor.execute(query, (row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7],row[8],row[9]))
            
# CREATE TABLE MBExpenses(
#               expenseID            INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
#               expenseDate          INTEGER    NOT NULL,
#               expenseItem          CHAR(100)  NOT NULL,
#               expenseAmount        REAL       NOT NULL,
#               currency             CHAR(5)    NOT NULL,
#               USExchangeRate       REAL,
#               expenseAdjusted      REAL,
#               golferID             INTEGER    NOT NULL,
#               FOREIGN KEY(golferID) REFERENCES Golfers(golferID) ON DELETE CASCADE
#               );''')
def get_MBExpenses():
    query = '''select * from MBExpenses order by golferID, expenseDate'''
    return cursor.execute(query).fetchall()

def get_MBExpensesForExpenseID(expenseID):
    query = '''select * from MBExpenses where expenseID = {0}'''.format(expenseID)
    return cursor.execute(query).fetchall()

def get_MBExpenses_Range(startDateOrd, endDateOrd):
    query = '''select * from MBExpenses where expenseDate >= {0} and expenseDate <= {1} order by golferID, expenseDate'''.format(startDateOrd, endDateOrd)
    return cursor.execute(query).fetchall()

def get_MBExpenses_Range_Golfer(startDateOrd, endDateOrd, golferID):
    query = '''select * from MBExpenses where expenseDate >= {0} and expenseDate <= {1} and golferID = {2} order by golferID, expenseDate'''.format(startDateOrd, endDateOrd, golferID)
    return cursor.execute(query).fetchall()

def insert_MBExpenses(expenseDate, expenseItem, expenseAmount, currency,
                           USExchangeRate, expenseAdjusted, golferID):
    query = '''
    insert into MBExpenses (expenseDate, expenseItem, expenseAmount, currency,
                           USExchangeRate, expenseAdjusted, golferID)
    values (?, ?, ?, ?, ?, ?, ?)'''
    cursor.execute(query, (expenseDate, expenseItem, expenseAmount, currency,
                           USExchangeRate, expenseAdjusted, golferID))

def update_MBExpenses(expenseID, expenseDate, expenseItem, expenseAmount, currency,
                                    USExchangeRate, expenseAdjusted, golferID):
    query = '''
    update MBExpenses 
    set expenseDate=?, expenseItem=?, expenseAmount=?, currency=?,
        USExchangeRate=?, expenseAdjusted=?, golferID=?
    where expenseID == ?'''
    cursor.execute(query, (expenseDate, expenseItem, expenseAmount, currency,
                                    USExchangeRate, expenseAdjusted, golferID, expenseID))

def delete_MBExpense(expenseID):
    query = '''
    delete from MBExpenses
    where expenseID = ?'''    
    cursor.execute(query,(expenseID,))

def exportCSV_MBExpenses(fileName):
    query = '''select * from MBExpenses'''
    data = cursor.execute(query).fetchall()
    with open(fileName, 'w', newline='') as fout:
        for i in range(len(data)):
            csv.writer(fout).writerow(data[i])
            
def importCSV_MBExpenses(fileName):
    with open(fileName, 'r') as fin:
        reader = csv.reader(fin)
        for row in reader:
            # row[5] must be a valid courseID
            query = '''
            insert into MBExpenses (expenseID, expenseDate, expenseItem, expenseAmount, currency,
                                    USExchangeRate, expenseAdjusted, golferID)
            values (?, ?, ?, ?, ?, ?, ?, ?)'''
            cursor.execute(query, (row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7]))
 

##### Database init and close #######

def initDB(filename):
    global db, cursor

    try:
        if not os.path.exists(filename):
            #
            #  myGolfDB does not exist, create an empty myGoldDB.db
            #  in the provided filename
            #
            db = sql.connect(filename)
            cursor = db.cursor()
            cursor.execute('PRAGMA Foreign_Keys=True')

            cursor.execute('''CREATE TABLE Golfers(
               golferID INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
               golferFNAME          CHAR(30)    NOT NULL,
               golferLNAME          CHAR(30)    NOT NULL,
               golferPIC_LOC        CHAR(100),
               golferBirthday       INTEGER
               );''')
            
            cursor.execute('''CREATE TABLE Courses(
               courseID INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
               courseNAME          CHAR(60)    NOT NULL,
               courseSNAME         CHAR(30)    NOT NULL,
               courseLogo          CHAR(100)
               );''')
            
            cursor.execute('''CREATE TABLE Tees(
               teeID INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
               teeNAME         CHAR(15)    NOT NULL,
               teeType         CHAR(8)     NOT NULL,
               teeRATING       REAL        NOT NULL,
               teeSLOPE        SMALLINT    NOT NULL,
               courseID        INTEGER     NOT NULL,
               archived        SMALLINT,   
               FOREIGN KEY(courseID) REFERENCES Courses(courseID) ON DELETE CASCADE
               );''')

            cursor.execute('''CREATE TABLE TeeDetails(
               teeDetailID  INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
               teeYardage   SMALLINT    NOT NULL,
               teeHandicap  SMALLINT    NOT NULL,
               teePar       SMALLINT    NOT NULL,
               teeID        INTEGER    NOT NULL,
               FOREIGN KEY(teeID) REFERENCES Tees(teeID) ON DELETE CASCADE
               );''')
 
            # field avgDrv has been changed to total driving yards           
            cursor.execute('''CREATE TABLE Games(
               gameID          INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
               gameDate        CHAR(15)    NOT NULL,
               gameNumber      SMALLINT    NOT NULL,
               teeID           SMALLINT    NOT NULL,
               golferID        SMALLINT    NOT NULL,
               frontScore      SMALLINT,
               backScore       SMALLINT,
               grossScore      SMALLINT,
               hdcpDiff        REAL,
               dEagleCNT       SMALLINT,
               eagleCNT        SMALLINT,
               birdieCNT       SMALLINT,
               parCNT          SMALLINT,
               bogieCNT        SMALLINT,
               dBogieCNT       SMALLINT,
               tBogieCNT       SMALLINT,
               othersCNT       SMALLINT,
               greens          SMALLINT,
               fairways        SMALLINT,
               leftMiss        SMALLINT,
               rightMiss       SMALLINT,
               avgDrive        SMALLINT,    
               totalSands      SMALLINT,
               sandSaves       SMALLINT,
               gameRating      REAL,
               gameSlope       SMALLINT,
               holesPlayed     CHAR(7),
               totalPutts      SMALLINT,
               putts3AndUp     SMALLINT,
               totalYard       SMALLINT,
               courseID        SMALLINT,
               FOREIGN KEY(golferID) REFERENCES Golfers(golferID),
               FOREIGN KEY(teeID) REFERENCES Tees(teeID) ON DELETE CASCADE
               );''')

            cursor.execute('''CREATE TABLE GameDetails(
               gameDetailID  INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
               holeNumber    SMALLINT    NOT NULL,
               holeScore     SMALLINT    NOT NULL,
               holePutts     SMALLINT,
               holeGreens    SMALLINT,
               holeFairways  CHAR(4),
               holeDriveDistance  SMALLINT,
               holeSands     CHAR(4),
               gameID        SMALLINT    NOT NULL,
               FOREIGN KEY(gameID) REFERENCES Games(gameID)
               );''')

            cursor.execute('''CREATE TABLE Addresses(
               AddrID  INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
               ownerTableName CHAR(30),
               town           CHAR(30),
               prov_state     CHAR(30),
               country        CHAR(30),
               ownerID        SMALLINT    NOT NULL,
               pc_zip         CHAR(10),
               phone          CHAR(10),
               website        CHAR(40),
               street_number  CHAR(60)
               );''')

            cursor.execute('''CREATE TABLE MBExpenses(
               expenseID            INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
               expenseDate          INTEGER    NOT NULL,
               expenseItem          CHAR(100)  NOT NULL,
               expenseAmount        REAL       NOT NULL,
               currency             CHAR(5)    NOT NULL,
               USExchangeRate       REAL,
               expenseAdjusted      REAL,
               golferID             INTEGER    NOT NULL,
               FOREIGN KEY(golferID) REFERENCES Golfers(golferID) ON DELETE CASCADE
               );''')
 
            
            
#            cursor.execute('''CREATE TABLE TeeHoles(
#               teeHOLEID       INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
#               teeHoleNUMBER   SMALLINT    NOT NULL,
#               teeHoleHANDICAP SMALLINT    NOT NULL,
#               teeHoleYARDAGE  SMALLINT    NOT NULL,
#               teeHolePar      SMALLINT    NOT NULL
#               );''')
            
        else:
            #
            #  myGolfDB exist, just open it
            #
            db = sql.connect(filename)
            cursor = db.cursor()
            cursor.execute('PRAGMA Foreign_Keys=True')
    except:
        print("Error connecting to", filename)
        cursor = None
        raise

def closeDB():
    #  Check if connection is openned
    if db:
        try:
            cursor.close()
            db.commit()
            db.close()
        except:
            print("problem closing database...")
            raise

def lastRowID():
    return(cursor.lastrowid)

if __name__ == "__main__": 
    initDB('MyGolfDB')  # use default file
#    print("Members:\n", get_members())
#    print("Items:\n",get_golfer())

