from tkinter import *
import tkinter.tix as Tix
import re
from MyGolfApp2016.AppConstants import *

lista = ['a', 'actions', 'additional', 'also', 'an', 'and', 'angle', 'are', 'as', 'be', 'bind', 'bracket', 'brackets', 'button', 'can', 'cases', 'configure', 'course', 'detail', 'enter', 'event', 'events', 'example', 'field', 'fields', 'for', 'give', 'important', 'in', 'information', 'is', 'it', 'just', 'key', 'keyboard', 'kind', 'leave', 'left', 'like', 'manager', 'many', 'match', 'modifier', 'most', 'of', 'or', 'others', 'out', 'part', 'simplify', 'space', 'specifier', 'specifies', 'string;', 'that', 'the', 'there', 'to', 'type', 'unless', 'use', 'used', 'user', 'various', 'ways', 'we', 'window', 'wish', 'you']


#        listFrame = Frame(self.modalPane)
#        listFrame.pack(side=TOP, padx=5, pady=5)
#        
#        scrollBar = Scrollbar(listFrame)
#        scrollBar.pack(side=RIGHT, fill=Y)
#        self.listBox = Listbox(listFrame, selectmode=SINGLE)
#        self.listBox.pack(side=LEFT, fill=Y)
#        scrollBar.config(command=self.listBox.yview)
#        self.listBox.config(yscrollcommand=scrollBar.set)
#        self.list.sort()
#        for item in self.list:
#            self.listBox.insert(END, item)
class AppListBoxChoice(object):
    def __init__(self, master=None, title=None, message=None, list=[]):
        self.master = master
        self.value = None
        self.list = list[:]
        
        self.modalPane = Frame(self.master)

#        self.modalPane.transient(self.master)
        self.modalPane.grab_set()

        self.modalPane.bind("<Return>", self._choose)
        self.modalPane.bind("<Double-Button-1>", self._choose)
        self.modalPane.bind("<Escape>", self._cancel)
        self.modalPane.bind("<Down>", self._down)
        self.modalPane.bind("<Up>", self._up)

        if title:
            self.modalPane.title(title)

        if message:
            Label(self.modalPane, text=message).pack(padx=5, pady=5)

        listFrame = Frame(self.modalPane)
        listFrame.pack(side=TOP, padx=5, pady=5)
        
        scrollBar = Scrollbar(listFrame)
        scrollBar.pack(side=RIGHT, fill=Y)
        self.listBox = Listbox(listFrame, selectmode=SINGLE, takefocus=0)
        self.listBox.pack(side=LEFT, fill=Y)
        scrollBar.config(command=self.listBox.yview)
        self.listBox.config(yscrollcommand=scrollBar.set)
        self.list.sort()
        for item in self.list:
            self.listBox.insert(END, item)
            
        self.listBox.select_set(0, 0)

        buttonFrame = Frame(self.modalPane)
        buttonFrame.pack(side=BOTTOM)

        chooseButton = Button(buttonFrame, text="Choose", command=self._choose)
        chooseButton.pack()

        cancelButton = Button(buttonFrame, text="Cancel", command=self._cancel)
        cancelButton.pack(side=RIGHT)

    def _down(self, event=None):
        print("Selected")
        try:
            curIndex = self.listBox.curselection()[0]
            print("current Indext is: ", curIndex, "List Lenght is: ", len(self.list))
            self.listBox.selection_clear(curIndex)
            if curIndex == self.listBox.size() - 1:
                newIndex = int(curIndex)
            else:
                newIndex = int(curIndex) + 1
            self.listBox.select_set(newIndex, newIndex)
        except IndexError:
            self.value = None

    def _up(self, event=None):
        print("Selected")
        try:
            curIndex = self.listBox.curselection()[0]
            print("current Indext is: ", curIndex, "List Lenght is: ", len(self.list))
            self.listBox.selection_clear(curIndex)
            if curIndex == 0:
                newIndex = 0
            else:
                newIndex = int(curIndex) - 1
            self.listBox.select_set(newIndex, newIndex)
        except IndexError:
            self.value = None

    def _choose(self, event=None):
        try:
            firstIndex = self.listBox.curselection()[0]
            self.value = self.list[int(firstIndex)]
        except IndexError:
            self.value = None
        self.modalPane.destroy()

    def _cancel(self, event=None):
        self.modalPane.destroy()
        
    def returnValue(self):
        self.master.wait_window(self.modalPane)
        return self.value
def widgetEnter(event):
    event.widget.focus()

def widgetLeave(event):
    event.widget.focus_displayof()

class AutocompleteEntry(Entry):
    def __init__(self, lista, fieldWidth, *args, **kwargs):
        self.fieldWidth = fieldWidth
        Entry.__init__(self, *args, **kwargs)
        self.config(highlightcolor=AppDefaultForeground, highlightthickness = 2, width=self.fieldWidth, justify=LEFT, font=fontBig)
        self.bind('<Enter>', self.enter)
#        self.bind('<Return>', self.enterKeyEntered)
        self.bind('<FocusOut>', self.check)
        self.lista = lista        
        self.var = self["textvariable"]
        if self.var == '':
            self.var = self["textvariable"] = StringVar()

        self.var.trace('w', self.changed)
        self.bind("<Right>", self.enterKeyEntered)
        self.bind("<Return>", self.selection)
        self.bind("<Up>", self.up)
        self.bind("<Down>", self.down)
        
        self.lb_up = False
        
    def enterKeyEntered(self, *args):
        print("Enter key entered")
        if not self.lb_up:
            print("creating the list")
            self.lb = self.createLB()
            print(lista)
            for w in self.lista:
                self.lb.insert(END,w)
            index = 0
            self.lb.selection_set(first=index)
            self.lb.activate(index)
        
    def createLB(self):
        print("Creating the list here.")
        lb = Listbox(width=self.fieldWidth, font=fontBig, takefocus=0)
        lb.bind("<ButtonRelease-1>", self.selectItem)
        lb.bind("<Double-Button-1>", self.selection)
        lb.bind("<Right>", self.selection)
        lb.bind("<Up>", self.up)
        lb.bind("<Down>", self.down)
        lb.place(x=self.winfo_x(), y=self.winfo_y()+self.winfo_height())
        self.lb_up = True
        return lb
        
    def enter(self,event):
        self.focus()

    def check(self, event):
        print("LEAVING")
        if self.lb_up:
            self.lb.destroy()
            self.lb_up = False

    def changed(self, name, index, mode):  
        print("Changed detected!")
        if self.var.get() == '':
            pass
#            if self.lb_up:
#                self.lb.destroy()
#            self.lb_up = False
        else:
            words = self.comparison()
            print(words)
            if words:            
                if not self.lb_up:
                    self.lb = self.createLB()
                
                self.lb.delete(0, END)
                for w in words:
                    self.lb.insert(END,w)
                index = 0
                self.lb.selection_set(first=index)
                self.lb.activate(index)
            else:
                if self.lb_up:
                    self.lb.destroy()
                    self.lb_up = False

    def selectItem(self, event):
        print("Select Item")
        if self.lb_up:
            if self.lb.curselection() == ():
                print("I am here")
                index = '0'
            else:
                print("I am here now")
                index = self.lb.curselection()[0]
            self.lb.selection_clear(first=index)
            self.lb.selection_set(first=index)
            self.lb.activate(index)
            print("Index is: ", index)
#        self.reDisplayList(index)
        self.focus()
        print("After")
               
    def selection(self, event):
        print("SELECTION")

        if self.lb_up:
            self.var.set(self.lb.get(ACTIVE))
            self.lb.destroy()
            self.lb_up = False
            self.icursor(END)
            self.focus()

    def up(self, event):
        print("UP")

        if self.lb_up:
            if self.lb.curselection() == ():
                index = '0'
            else:
                index = self.lb.curselection()[0]
            if index != '0':                
                self.lb.selection_clear(first=index)
                index = str(int(index)-1)                
                self.lb.selection_set(first=index)
                self.lb.activate(index) 
            self.focus()

    def down(self, event):
        print("DOWN")

        if self.lb_up:
            if self.lb.curselection() == ():
                index = '0'
            else:
                index = self.lb.curselection()[0]
            if index != END:                        
                self.lb.selection_clear(first=index)
                index = str(int(index)+1)        
                self.lb.selection_set(first=index)
                self.lb.activate(index) 
            self.focus()

    def comparison(self):
        print("COMPARAISON")
        pattern = re.compile('.*' + self.var.get() + '.*')
        return [w for w in self.lista if re.match(pattern, w)]
        self.focus()

if __name__ == '__main__':
    root = Tk()

    entry =  AutocompleteEntry(lista, 25, root)
    entry.grid(row=0, column=0)
    Button(text='nothing').grid(row=1, column=0)

    root.mainloop()