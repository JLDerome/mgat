from PIL import Image

import tkinter.tix as Tix
import tkinter.ttk as ttk
import AppCRUD as AppCRUD
import AppCRUDGolfers as CRUDGolfers
import AppCRUDGames as CRUDGames
import AppCRUDTees as CRUDTees
import AppCRUDCourses as CRUDCourses
import AppCRUDExpenses as CRUDExpenses
import tkinter.filedialog as Fd
import tkinter.messagebox as Mb
import textwrap, re
from dateutil import parser

from tkinter.constants import *

from tableCommonWindowClass import tableCommonWindowClass
from AppClasses import AppFieldsFrame, AppBorderFrame, AppFrame, AppPictureFrame
from AppClasses import AppUserIDFrame, AppLastUpdateFormat, MyScrollFrameVertical, AppFieldEntryDate
from AppClasses import AppCBList, AppSearchLB, AppStdButton, gameCellColumnDataFrame
from AppConstants import golferDefaultPictureName, appGolferPICDir, golferPicWidth, golferPicHeight
from AppConstants import appDateWidth, fontAverageB,AppStdForeground, AppDefaultForeground
from AppConstants import gameListingButtonWidth, strdate, todayDate, AppConfigFile

from AppProc import convertDateStringToOrdinal, getIndex, convertOrdinaltoString, updateMyAppConstant

courseList = []
golfersList = []
teeList = []

class expensesWindow(tableCommonWindowClass):
    def __init__(self, aWindow, aCloseCommand, loginData):
        #
        #  loginInfo format: password, access_Level, loginID, updaterID
        #
        self.windowTitle = 'Expenses'
        self.tableName = 'Expenses'
        tableCommonWindowClass.__init__(self, aWindow, aCloseCommand, self.windowTitle, loginData[1])
        self.loginData = loginData
        self.accessLevel = self.loginData[1]
        self.updaterID = self.loginData[2]
        self.updaterFullname = self.loginData[0]
        self.aCloseCommand = aCloseCommand
        self.aWindow = aWindow
        self.displayStartDateForGame = None
        self.displayEndDateForGame = None
        self.displayGameForCourse = None
        self.display9Holes = True
        self.displayToday = False
        self.gamesListingColumnCount = 14

        self.courseComboBoxData = CRUDCourses.getCourseCB()
        self.courseDictionary = {}
        self.reverseCourseDictionary = {}
        for i in range(len(self.courseComboBoxData)):
            self.courseDictionary.update({self.courseComboBoxData[i][1]: self.courseComboBoxData[i][0]})
            self.reverseCourseDictionary.update({self.courseComboBoxData[i][0]: self.courseComboBoxData[i][1]})

        self.teeComboBoxData = CRUDTees.get_tees()
        self.teeDictionary = {}
        self.reverseTeeDictionary = {}
        self.reverseTeeDictionaryCourseID = {}
        for i in range(len(self.teeComboBoxData)):
            self.teeDictionary.update({self.teeComboBoxData[i][1]: self.teeComboBoxData[i][0]})
            self.reverseTeeDictionary.update({self.teeComboBoxData[i][0]: self.teeComboBoxData[i][1]})
            self.reverseTeeDictionaryCourseID.update({self.teeComboBoxData[i][0]: self.teeComboBoxData[i][5]})

        #
        #  loginInfo format: password, access_Level, loginID, updaterID
        #
        if loginData[2] == 1:
            self.updaterFullname = "Super User"
        else:
            self.updaterFullname = CRUDGolfers.getGolferFullname(loginData[3])
        self.updaterID = self.loginData[2]
        self.displayCurUser.setUserName(self.updaterFullname)

        #######################################################################################
        # Inherited all from tableCommanWindowClass
        #
        #  This class intents to change the mainArea only.  Everything else must remain common
        #
        self.mainAreaRowCount = 0
        self.mainAreaColumnCount = 0
        self._reDisplayRecordFrame()
        self.mainArea.rowconfigure(self.mainAreaRowCount, weight=1)
        self.windowViewDefault()

    ############################################################ Area to add window specific commands
    #        self.closeButton = ttk.Button(self.commandFrame, text='Test', style='CommandButton.TButton',
    #                                 command= lambda: self.sampleCommand())
    #        self.closeButton.grid(row=0, column=0, sticky=N+S)


    ################################################################# Redefined commands
    #
    #  invoke from the tableCommonWindows.  These commands are specific to each tables
    #  The specific actions are redefinded here.
    #
    #    def displayRec(self, *args):
    #        print("Displaying a Record: ", self.curRecNum)
    #
    #    def save(self):
    #        #
    #        #  Table specific.  Return a True if succesful saved.
    #        #
    #        print("Saving a record (re-defined)")
    #        return True
    def tableSpecificMenu(self):
        pass
        '''
        self.viewsmenu.add_command
        self.viewsmenu.add_command(label="Active Golfers", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                   command=lambda viewID='Active Golfers': self.basicViewsList(viewID))

        sortsmenu = Menu(self.menubar, foreground=AppDefaultForeground, font=AppDefaultMenuFont, tearoff=0)
        sortsmenu.add_command(label="Default", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Default': self.sortingList(sortID))
        sortsmenu.add_command(label="Last Name", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Last Name': self.sortingList(sortID))
        sortsmenu.add_command(label="First Name", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='First Name': self.sortingList(sortID))
        sortsmenu.add_command(label="Birthday", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Birthday': self.sortingList(sortID))
        #
        # Next Command actually puts the menu up
        #
        self.menubar.add_cascade(label="Sorts", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                 menu=sortsmenu)
        '''
    def sort(self):
        pass
        '''
        if self.curOp == 'default':
            currentID = self.recList[self.curRecNumV.get()][0]
            if self.sortID.get() == 'Default':
                self.recList = sorted(self.recList, key=operator.itemgetter(2,1))
            elif self.sortID.get() == 'Last Name':
                self.recList.sort(key=lambda row: row[2])
            elif self.sortID.get() == 'First Name':
                self.recList.sort(key=lambda row: row[1])
            elif self.sortID.get() == 'Birthday':
                self.recList.sort(key=lambda row: row[4])
            else:
                self.recList.sort(key=lambda row: row[0])
            newIdx = getIndex(currentID, self.recList)
            self.curRecNumV.set(newIdx)
            self.displayRec()
        else:
            aMsg = "ERROR: Invalid request. Must be executed in default state only."
            self.myMsgBar.newMessage('error', aMsg)
        '''

    def displayAbout(self):
        #AppDisplayAbout(self)
        print("Working on it.....")

    def _enableWindowCommand(self):
        #
        #  Any command specific must added here
        #
        pass

    #        self.closeButton.config(state=NORMAL)

    def _disableWindowCommand(self):
        #
        #  Any command specific must added here
        #
        pass

    #        self.closeButton.config(state=DISABLED)

    def getTableData(self):
        self.recAll = CRUDGolfers.getGolfers()


    def _submitForm(self, event):
        pass
        print("Submit button has been pressed")
        # Must be defined in the popUp window.


    def _clearForm(self, event):
        pass
        print("Clear button has been pressed")
        # Must be defined in the popUp window.


    def _exitForm(self, event):
        pass
        print("Exit button has been pressed")
        # Must be defined in the popUp window.
