'''
Created on Jan 6, 2016

@author: derome
'''

import datetime
import os, sys, string, calendar, time
from dateutil import parser
import tkinter.filedialog as Fd
from telnetlib import theNULL
from PIL import Image, ImageTk
from MyGolfConstants import *
from MyGolfApp2016 import AppCRUD as myGolfData
import csv

fnta = ("Times", 10)
fnt = ("Times", 12)
fntc = ("Times", 12, 'bold')

strtitle = "Calendar"
strdays = "Mon  Tue  Wed  Thu  Fri  Sat Sun"
dictmonths = {'1':'Jan','2':'Feb','3':'Mar',
              '4':'Apr','5':'May','6':'Jun',
              '7':'Jul','8':'Aug','9':'Sep',
              '10':'Oct','11':'Nov','12':'Dec'}

year = time.localtime()[0]
month = time.localtime()[1]
day =time.localtime()[2]
strdate = (str(year) +  "-" + dictmonths[str(month)] + "-" + str(day))

rowHeaderColor = 'green'
parHeaderColor = 'yellow'
deagle_color = 'maroon1'
eagle_color = 'medium orchid'
par_color ='green'
birdie_color = 'SteelBlue2'
bogie_color = 'red2'
dbogie_color = 'chocolate3'
tbogie_color = 'brown3'
others_color = 'HotPink4'

def returnScoreResultColor(par, score):
    if score == int(par):
        return par_color
    if score == int(par)-1:
        return birdie_color
    if score == int(par)-2:
        return eagle_color
    if score == int(par)-3:
        return deagle_color
    if score == int(par)+1:
        return bogie_color
    if score == int(par)+2:
        return dbogie_color
    if score == int(par)+3:
        return tbogie_color
    if score > int(par)+3:
        return others_color
    
def stripChar(aList, aString):
    newString = aString
    for item in aList:
        newString = newString.replace(item,'')
    return newString
    
def isAlphaOnly(aChar):
    try:
        if not aChar.isalpha():
            raise Exception
        else:
            return True
    except:
            return False
        
def calculategolferStrokes(golferIndex, courseSlope):
    golferStrokes = (float(golferIndex) * float(courseSlope)) / float(113)
    return int(golferStrokes)

#Handicap Differential = (Gross Score - Course Rating) * 113 / (Course Slope)
def calculateHdcpDiff(scoreGross, courseRating, courseSlope):
    # scoreGross and courseSlope are integer
    # courseRating is a real (float)
    if courseSlope == 0:
        return 0.0
    else:
        diff =  (scoreGross-courseRating)
        calc = diff * 113 / courseSlope
        return "%.1f" % calc

#The Handicap Differential is a generic number that can be used to compare rounds played on differnt courses.
#This is the number that is graphed in the bars on the score graph the user profile screen. Your FairwayFiles Index is calculated by taking the average of the best 10 of your last 20 handicap differentials. Then taking 96% of that number.    
def calculateNewHdcp(lastGameHdcpDiff, curGolfGameDate, golferID):
    curGameList = myGolfData.get_games_hdcp_diff_for_golfer(golferID) # organized, latest first
    numberOfGames = len(curGameList)
    hdcpGameList = []
    if numberOfGames >= 20:
        if int(curGolfGameDate) < int(curGameList[19][0]):
            hdcpNumberOfGames = 20
        else:
            hdcpGameList.append(float(lastGameHdcpDiff)) # latest game is at the top
            hdcpNumberOfGames = 19                    
    else:
        hdcpGameList.append(float(lastGameHdcpDiff)) # latest game is at the top
        hdcpNumberOfGames = numberOfGames
        
    if numberOfGames == 0:
        newHdcp = float(lastGameHdcpDiff)
    else:
        for i in range(hdcpNumberOfGames):
            hdcpGameList.append(float(curGameList[i][1])) # add the remaining hcdpDiff
        hdcpGameList.sort()
        if len(hdcpGameList) >= 20:
            newHdcp = float(0.0)
            for i in range(10):
                newHdcp = newHdcp + float(hdcpGameList[i])
            newHdcp = newHdcp/float(10)
#            return"%.1f" % newHdcp
        elif len(hdcpGameList) == 19:
            newHdcp = float(0.0)
            for i in range(9):
                newHdcp = newHdcp + float(hdcpGameList[i])
            newHdcp = newHdcp/float(9)
#            return"%.1f" % newHdcp
        elif len(hdcpGameList) == 18:
            newHdcp = float(0.0)
            for i in range(8):
                newHdcp = newHdcp + float(hdcpGameList[i])
            newHdcp = newHdcp/float(8)
#            return"%.1f" % newHdcp
        elif len(hdcpGameList) == 17:
            newHdcp = float(0.0)
            for i in range(7):
                newHdcp = newHdcp + float(hdcpGameList[i])
            newHdcp = newHdcp/float(7)
#            return"%.1f" % newHdcp
        elif (len(hdcpGameList) == 15) or (len(hdcpGameList) == 16):
            newHdcp = float(0.0)
            for i in range(6):
                newHdcp = newHdcp + float(hdcpGameList[i])
            newHdcp = newHdcp/float(6)
#            return"%.1f" % newHdcp
        elif (len(hdcpGameList) == 13) or (len(hdcpGameList) == 14):
            newHdcp = float(0.0)
            for i in range(5):
                newHdcp = newHdcp + float(hdcpGameList[i])
            newHdcp = newHdcp/float(5)
#            return"%.1f" % newHdcp
        elif (len(hdcpGameList) == 11) or (len(hdcpGameList) == 12):
            newHdcp = float(0.0)
            for i in range(4):
                newHdcp = newHdcp + float(hdcpGameList[i])
            newHdcp = newHdcp/float(4)
#            return"%.1f" % newHdcp
        elif (len(hdcpGameList) == 9) or (len(hdcpGameList) == 10):
            newHdcp = float(0.0)
            for i in range(3):
                newHdcp = newHdcp + float(hdcpGameList[i])
            newHdcp = newHdcp/float(3)
#            return"%.1f" % newHdcp
        elif (len(hdcpGameList) == 7) or (len(hdcpGameList) == 8):
            newHdcp = float(0.0)
            for i in range(2):
                newHdcp = newHdcp + float(hdcpGameList[i])
            newHdcp = newHdcp/float(2)
        else:
            newHdcp = float(hdcpGameList[0])
    newHdcp = newHdcp * float(0.98)       
    return "%.1f" % newHdcp

def calculateAfterCurrentGameHdcp(aOrdinalDate, golferID, curGameID, curGameNumber):
    curGameList = myGolfData.get_games_hdcp_diff_before_date_for_golfer(aOrdinalDate, golferID, curGameID, curGameNumber) # organized, latest first
    numberOfGames = len(curGameList)
    if numberOfGames == 0:
        return 0.0
    else:
        howManyGamesToday=1
        removeTupleAmount=0
#        for i in range(len(curGameList)):
#            if i == len(curGameList)-1:
#                break
        i=1
        while i < len(curGameList):
            if curGameList[i-1][0] == curGameList[i][0]:
                howManyGamesToday = howManyGamesToday + 1
            else:
                break
            i = i + 1
                
        if howManyGamesToday > 1:
            for i in range(howManyGamesToday):
                if curGameList[i][2] == curGameNumber:
                    removeTupleAmount = i

        newGameList = []
        for i in range(removeTupleAmount,numberOfGames):
            newGameList.append(curGameList[i][1])
        
        index = calculateHdcpList(newGameList)
        return index

def calculateHdcpList(hdcpGameList):
        hdcpGameList.sort()
        if len(hdcpGameList) >= 20: 
            newHdcp = float(0.0)
            for i in range(10):
                newHdcp = newHdcp + float(hdcpGameList[i])
            newHdcp = newHdcp/float(10)
#            return"%.1f" % newHdcp
        elif len(hdcpGameList) == 19:
            newHdcp = float(0.0)
            for i in range(9):
                newHdcp = newHdcp + float(hdcpGameList[i])
            newHdcp = newHdcp/float(9)
#            return"%.1f" % newHdcp
        elif len(hdcpGameList) == 18:
            newHdcp = float(0.0)
            for i in range(8):
                newHdcp = newHdcp + float(hdcpGameList[i])
            newHdcp = newHdcp/float(8)
#            return"%.1f" % newHdcp
        elif len(hdcpGameList) == 17:
            newHdcp = float(0.0)
            for i in range(7):
                newHdcp = newHdcp + float(hdcpGameList[i])
            newHdcp = newHdcp/float(7)
#            return"%.1f" % newHdcp
        elif (len(hdcpGameList) == 15) or (len(hdcpGameList) == 16):
            newHdcp = float(0.0)
            for i in range(6):
                newHdcp = newHdcp + float(hdcpGameList[i])
            newHdcp = newHdcp/float(6)
#            return"%.1f" % newHdcp
        elif (len(hdcpGameList) == 13) or (len(hdcpGameList) == 14):
            newHdcp = float(0.0)
            for i in range(5):
                newHdcp = newHdcp + float(hdcpGameList[i])
            newHdcp = newHdcp/float(5)
#            return"%.1f" % newHdcp
        elif (len(hdcpGameList) == 11) or (len(hdcpGameList) == 12):
            newHdcp = float(0.0)
            for i in range(4):
                newHdcp = newHdcp + float(hdcpGameList[i])
            newHdcp = newHdcp/float(4)
#            return"%.1f" % newHdcp
        elif (len(hdcpGameList) == 9) or (len(hdcpGameList) == 10):
            newHdcp = float(0.0)
            for i in range(3):
                newHdcp = newHdcp + float(hdcpGameList[i])
            newHdcp = newHdcp/float(3)
#            return"%.1f" % newHdcp
        elif (len(hdcpGameList) == 7) or (len(hdcpGameList) == 8):
            newHdcp = float(0.0)
            for i in range(2):
                newHdcp = newHdcp + float(hdcpGameList[i])
            newHdcp = newHdcp/float(2)
        else:
            newHdcp = float(hdcpGameList[0])
            
        newHdcp = newHdcp * float(0.98) 
        return"%.1f" % newHdcp
    
def upcase_first_letter(s):
    return s[0].upper() + s[1:]

def calculateBeforeCurrentGameHdcp(aOrdinalDate, golferID, curGameID, curGameNumber):
    curGameList = myGolfData.get_games_hdcp_diff_before_date_for_golfer(aOrdinalDate, golferID, curGameID, curGameNumber) # organized, latest first
    numberOfGames = len(curGameList)
    if numberOfGames <= 1:
        return float(0.0)
    else:
        howManyGamesToday=1
        removeTupleAmount=0
        for i in range(len(curGameList)):
            if i == len(curGameList)-1:
                break
            elif curGameList[i][0] == curGameList[i+1][0]:
                howManyGamesToday = howManyGamesToday + 1
                if curGameNumber == curGameList[i][2]:
                    removeTupleAmount = i + 1
            else:
                break
            
        if howManyGamesToday > 1:
            if removeTupleAmount == 0:
                removeTupleAmount = howManyGamesToday
        else:
            removeTupleAmount = 1
            
        newGameList = []
        for i in range(removeTupleAmount,numberOfGames):
            newGameList.append(curGameList[i][1])
        
        index = calculateHdcpList(newGameList)
        return index
    
def calculateBeforeADateHdcp(aOrdinalDate, golferID, curGameID, curGameNumber):
    curGameList = myGolfData.get_games_hdcp_diff_before_date_for_golfer(aOrdinalDate, golferID, curGameID, curGameNumber) # organized, latest first
    numberOfGames = len(curGameList)
    hdcpGameList = []
    if numberOfGames == 0:
        return float(0.0)
    else:
        if numberOfGames >= 20:
            hdcpNumberOfGames = 20
        else:
            hdcpNumberOfGames = numberOfGames
        for i in range(hdcpNumberOfGames):
            hdcpGameList.append(float(curGameList[i][1])) # add the remaining hcdpDiff
        hdcpGameList.sort()   # make a lsit of all hdcp diff and sort them (lowest first)
        # The following if structure calculate based on the number of games played.
        if len(hdcpGameList) >= 20: 
            newHdcp = float(0.0)
            for i in range(10):
                newHdcp = newHdcp + float(hdcpGameList[i])
            newHdcp = newHdcp/float(10)
#            return"%.1f" % newHdcp
        elif len(hdcpGameList) == 19:
            newHdcp = float(0.0)
            for i in range(9):
                newHdcp = newHdcp + float(hdcpGameList[i])
            newHdcp = newHdcp/float(9)
#            return"%.1f" % newHdcp
        elif len(hdcpGameList) == 18:
            newHdcp = float(0.0)
            for i in range(8):
                newHdcp = newHdcp + float(hdcpGameList[i])
            newHdcp = newHdcp/float(8)
#            return"%.1f" % newHdcp
        elif len(hdcpGameList) == 17:
            newHdcp = float(0.0)
            for i in range(7):
                newHdcp = newHdcp + float(hdcpGameList[i])
            newHdcp = newHdcp/float(7)
#            return"%.1f" % newHdcp
        elif (len(hdcpGameList) == 15) or (len(hdcpGameList) == 16):
            newHdcp = float(0.0)
            for i in range(6):
                newHdcp = newHdcp + float(hdcpGameList[i])
            newHdcp = newHdcp/float(6)
#            return"%.1f" % newHdcp
        elif (len(hdcpGameList) == 13) or (len(hdcpGameList) == 14):
            newHdcp = float(0.0)
            for i in range(5):
                newHdcp = newHdcp + float(hdcpGameList[i])
            newHdcp = newHdcp/float(5)
#            return"%.1f" % newHdcp
        elif (len(hdcpGameList) == 11) or (len(hdcpGameList) == 12):
            newHdcp = float(0.0)
            for i in range(4):
                newHdcp = newHdcp + float(hdcpGameList[i])
            newHdcp = newHdcp/float(4)
#            return"%.1f" % newHdcp
        elif (len(hdcpGameList) == 9) or (len(hdcpGameList) == 10):
            newHdcp = float(0.0)
            for i in range(3):
                newHdcp = newHdcp + float(hdcpGameList[i])
            newHdcp = newHdcp/float(3)
#            return"%.1f" % newHdcp
        elif (len(hdcpGameList) == 7) or (len(hdcpGameList) == 8):
            newHdcp = float(0.0)
            for i in range(2):
                newHdcp = newHdcp + float(hdcpGameList[i])
            newHdcp = newHdcp/float(2)
        else:
            newHdcp = float(hdcpGameList[0])
        newHdcp = newHdcp * float(0.98)       
        return "%.1f" % newHdcp

def calculateIdxForDateRange(golferID, startDate, endDate):
    #use ordinal dates
    curGameList = myGolfData.get_games_forIdx_RangeDate_for_golfer(golferID, startDate, endDate) # organized, latest first
    numberOfGames = len(curGameList)
    if numberOfGames == 0:
        return 0.0
    else:
        newGameList = []
        for i in range(numberOfGames):
            newGameList.append(curGameList[i][1])
        
        index = calculateHdcpList(newGameList)
        return index
    

def calculateCurrentHdcp(golferID):
    curGameList = myGolfData.get_games_hdcp_diff_for_golfer(golferID) # organized, latest first
    numberOfGames = len(curGameList)
    if numberOfGames == 0:
        return 0.0
    else:
        newGameList = []
        for i in range(numberOfGames):
            newGameList.append(curGameList[i][1])
        
        index = calculateHdcpList(newGameList)
        return index

def calculateAvgDrive(totalDrive, TotalYards):
    if totalDrive == 0:
        return 0
    else:
        return int(TotalYards/totalDrive)

def verifyRealRange(aFloat, lowReal, highReal):
    try:
        if aFloat < lowReal or aFloat > highReal:
            raise Exception
        else:
            return False
                           
    except:
        return False

def comparePathEquality(path1, path2):
    path1Norm = os.path.normpath(path1)
    path2Norm = os.path.normpath(path2)
    if path1Norm == path2Norm:
        return True
    else:
        return False
    
def convertOrdinaltoString(ordinalDate):
    aDate=datetime.date.fromordinal(int(ordinalDate))
    aNewDate= (str(aDate.year) +  "-" + dictmonths[str(aDate.month)] + "-" + str(aDate.day))
    return(aNewDate)

def convertOrdinaltoFull(ordinalDate):
#    dt.strftime("%A, %d. %B %Y %I:%M%p")
    aDate=datetime.date.fromordinal(int(ordinalDate))
    aNewDate= aDate.strftime("%A, %d %B %Y")
    return(aNewDate)

def getAge(refDate, Birthday): #require ordinal dates
    deltaDate = refDate - Birthday
    return int(deltaDate/365)

def obtainTotalGameMileage(aList):
    totalMileage = 0
    if len(aList) == 0:
        return 0.00
    else:
        for i in range(len(aList)):
            if aList[i][29] == '':
                pass
            else: 
                totalMileage = totalMileage + int(aList[i][29])
    converted = convertYardsToKms(totalMileage)
    return "%.2f" % float(converted)

def exportMyGolfConfig(data, fileName):
    with open(fileName, 'w', newline='') as fout:
        csv.writer(fout).writerow(data)
        
def importMyGolfConfig(filename):
    data = []     
    with open(filename, 'r') as fin:
        reader = csv.reader(fin)
        for row in reader:
            data.append(row)
    return data

def convertYardsToKms(aLenght):
    kms = float(aLenght) * 0.0009144
    return "%.2f" % kms

def convertDateStringToOrdinal(dateStr):
    dt = parser.parse(dateStr)   # transform a date into a datetime (accepts several formats)
    return dt.toordinal()

def resize_image(image,h,w):
    size = (w, h)
    resized = image.resize(size,Image.ANTIALIAS)
    img = ImageTk.PhotoImage(resized)
    return(img)
    
def capFirstChar(aString):
    retString = ''''''
    for i in range(len(aString)):
        if i == 0:
            retString = retString + aString[i].capitalize()
        else:
            retString = retString + aString[i]
    return retString

def obtainGreensRelatedStatsFromList(aList, golferID, startDate, endDate):
    #(Girs, AvgGIRs, TotalPutts, puttsPerRound,putts3AndUp,totalSands, sandSaves,sandSavesPercent)
    gamesStats=obtainGameRelatedStatsFromList(aList, golferID, startDate, endDate)
    Girs=0
    TotalPutts=0
    puttsPerRound=0
    putts3AndUp=0
    totalSands = 0
    sandSaves=0
    for i in range(len(aList)):
        totalSands = totalSands + aList[i][22]
        sandSaves = sandSaves + aList[i][23]
        Girs = Girs + aList[i][17]
        TotalPutts = TotalPutts + aList[i][27]
        putts3AndUp = putts3AndUp + aList[i][28]
        
    if totalSands > 0:
        sandSavesPercent = (float(sandSaves)/float(totalSands)) * float(100)
    else:
        sandSavesPercent = 0.0

    if gamesStats[0] > 0:
        AvgGIRs =  float(Girs) / gamesStats[0]
    else:
        AvgGIRs = 0.0

    if gamesStats[0] > 0:
        AvgGIRs =  float(Girs) / gamesStats[0]
        puttsPerRound = float(TotalPutts) / gamesStats[0]
    else:
        AvgGIRs = 0.0
        puttsPerRound = 0.0
         
    returnList = (Girs, "%.1f" % AvgGIRs, TotalPutts, "%.1f" % puttsPerRound, putts3AndUp, totalSands, sandSaves ,"%.1f" % sandSavesPercent)
    return returnList    
    
    
def obtainFairwayRelatedStatsFromList(aList, golferID, startDate, endDate):
    #(Fairways, AvgFairways, DrvAccuracy, AvgDrives,TotalMissR,RMPercent,TotalMissL,LMPercent)
    gamesStats=obtainGameRelatedStatsFromList(aList, golferID, startDate, endDate)
    Fairways=0
    DrvAccuracy=0
    TotalYardsDrives=0.0
    TotalMissR=0
    RMPercent=0.0
    TotalMissL=0
    LMPercent=0.0
    for i in range(len(aList)):
        Fairways = Fairways + aList[i][18]
        TotalMissR = TotalMissR + aList[i][20] 
        TotalMissL = TotalMissL + aList[i][19]
        TotalYardsDrives = TotalYardsDrives + aList[i][21]

    TotalMiss = TotalMissL + TotalMissR
    allFairways = TotalMiss + Fairways

 
 
    if TotalMiss > 0:
        RMPercent = (float(TotalMissR)/float(allFairways)) * float(100)
        LMPercent = (float(TotalMissL)/float(allFairways)) * float(100)       
    else:
        RMPercent = 0.0
        LMPercent = 0.0
   
    if gamesStats[0] > 0:
        AvgFairways =  float(Fairways) / gamesStats[0]
    else:
        AvgFairways = 0.0
        
    if allFairways > 0:
        DrvAccuracy = (float(Fairways)/float(allFairways)) * float(100)
        AvgDrives = float(TotalYardsDrives) / float(Fairways)
    else:
        DrvAccuracy = 0.0 
        AvgDrives = 0.0
       
    returnList = (Fairways, "%.1f" % AvgFairways, "%.1f" % DrvAccuracy, "%.1f" % AvgDrives, TotalMissR, "%.1f" % RMPercent, TotalMissL, "%.1f" % LMPercent)
    return returnList    
    
def obtainScoringRelatedStatsFromList(aList, golferID, startDate, endDate):
    #(Deagles,Eagles,Birdies,Pars,Bogies,DBogies,TBogies,Others)
    Deagles=0
    Eagles=0
    Birdies=0
    Pars=0
    Bogies=0
    DBogies=0
    TBogies=0
    Others=0
    for i in range(len(aList)):
        Deagles = Deagles + aList[i][9]
        Eagles = Eagles + aList[i][10]
        Birdies = Birdies + aList[i][11]
        Pars = Pars + aList[i][12]
        Bogies = Bogies + aList[i][13]
        DBogies = DBogies + aList[i][14]
        TBogies = TBogies + aList[i][15]
        Others = Others + aList[i][16]
    returnList = (Deagles,Eagles,Birdies,Pars,Bogies,DBogies,TBogies,Others)
    return returnList    

def obtainGreensFromList(aList):
    greens = int(0)   
    for i in range(len(aList)):
        greens = greens + int(aList[i][17])
    return int(greens)

def obtainPuttsFromList(aList):
    putts = int(0)   
    for i in range(len(aList)):
        putts = putts + int(aList[i][27])
    return int(putts)

def obtain3PuttsFromList(aList):
    putts = int(0)   
    for i in range(len(aList)):
        putts = putts + int(aList[i][28])
    return int(putts)

def obtainFairwaysFromList(aList):
    fairways = int(0)   
    for i in range(len(aList)):
        fairways = fairways + int(aList[i][18])
    return int(fairways)

def obtainLeftMissFromList(aList):
    LeftMiss = int(0)   
    for i in range(len(aList)):
        LeftMiss = LeftMiss + int(aList[i][19])
    return int(LeftMiss)

def obtainRightMissFromList(aList):
    RightMiss = int(0)   
    for i in range(len(aList)):
        RightMiss = RightMiss + int(aList[i][20])
    return int(RightMiss)

def numberOfGamesFromList(aList):
    numOfGames = float(0)
    for i in range(len(aList)):
        if aList[i][26]  == 'Back':
            numOfGames = numOfGames + 0.5
                
        elif aList[i][26]  == 'Front':
            numOfGames = numOfGames + 0.5
            
        else:                
            numOfGames = numOfGames + float(1)
    return "%.1f" % numOfGames

def obtainBest18FromList(aList):
    if len(aList) == 0:
        return 0
    else:
        for i in range(len(aList)):
            if i == 0:
                best = aList[i][7]
            if aList[i][26] == 'All':
                if int(best) > int(aList[i][7]):
                    best = aList[i][7]
    return best

def obtainBest9FromList(aList):
    if len(aList) == 0:
        return 0
    else:
        for i in range(len(aList)):
            if i == 0 and aList[i][26] == 'Front':
                best = aList[i][5]
            elif i == 0 and aList[i][26] == 'Back':
                best = aList[i][6]
            elif i == 0 and aList[i][26] == 'All':
                if aList[i][6] > aList[i][5]:
                    best = aList[i][5]
                else:
                    best = aList[i][6]
              
            if aList[i][26] == 'All':
                if int(best) > int(aList[i][5]):
                    best = aList[i][5]
                if int(best) > int(aList[i][6]):
                    best = aList[i][6]
            elif aList[i][26] == 'Front':
                if int(best) > int(aList[i][5]):
                    best = aList[i][5]
            elif aList[i][26] == 'Back':
                if int(best) > int(aList[i][6]):
                    best = aList[i][6]
    return best

def obtainAverage9FromList(aList):
    numOf9s = 0.0
    totalScore = 0
    if len(aList) == 0:
        return 0.00
    else:
        for i in range(len(aList)):
            if aList[i][26] == 'All':
                numOf9s = numOf9s + 2
                totalScore = totalScore + int(aList[i][7])
            elif aList[i][26] == 'Front':
                numOf9s = numOf9s + 1
                totalScore = totalScore + int(aList[i][5])
            elif aList[i][26] == 'Back':
                numOf9s = numOf9s + 1
                totalScore = totalScore + int(aList[i][6])
    return "%.2f" % float(totalScore/numOf9s)

def obtainAverageDriveFromList(aList):
    numOfDrives = 0
    totalDriveDistance = 0
    if len(aList) == 0:
        return 0.00
    else:
        for i in range(len(aList)):
            numOfDrives = numOfDrives + int(aList[i][18]) # number of fairways
            totalDriveDistance = totalDriveDistance + int(aList[i][21]) # purpose changed, see CRUD
            
    if float(totalDriveDistance/numOfDrives) == float(0):
        return "%.2f" % float(0)
    else:
        return "%.2f" % float(totalDriveDistance/numOfDrives)

def obtainAverage18FromList(aList):
    numOfGames = 0.0
    totalScore = 0
    if len(aList) == 0:
        return 0.00
    else:
        for i in range(len(aList)):
            if aList[i][26] == 'All':
                numOfGames = numOfGames + 1
                totalScore = totalScore + int(aList[i][7])
            elif aList[i][26] == 'Front':
                numOfGames = numOfGames + 0.5
                totalScore = totalScore + int(aList[i][5])
            elif aList[i][26] == 'Back':
                numOfGames = numOfGames + 0.5
                totalScore = totalScore + int(aList[i][6])
    
    return "%.2f" % float(totalScore/numOfGames)
               

def obtainGameRelatedStatsFromList(aList, golferID, startDate, endDate):
    #(Number of Games,Best 18,Average 18, Best 9, Average 9, Current Index)
    #(10.5,84,89.4,42,44.5,9.4)
#    gamesStats=obtainGameRelatedStatsFromList(aList, golferID, startDate, endDate)
    best18 = 0
    first18 = False
    total18 = 0
    best9 = 0
    first9 = False
    total9 = 0
    numOf18 = 0
    numOf9 = 0
    numOfGames = float(0)
    for i in range(len(aList)):
        if aList[i][26]  == 'Back':
            numOfGames = numOfGames + 0.5
            total9 = aList[i][6] + total9
            numOf9 = 1 + numOf9
            if first9 == False:
                first9 =True
                best9 = int(aList[i][6])
            elif int(aList[i][6]) < best9:
                best9 = int(aList[i][6])
                
        elif aList[i][26]  == 'Front':
            if first9 == False:
                first9 =True
                best9 = int(aList[i][5])
            elif int(aList[i][5]) < best9:
                best9 = int(aList[i][5])
            numOfGames = numOfGames + 0.5
            total9 = aList[i][5] + total9
            numOf9 = 1 + numOf9
        else:
            if first18 == False:
                first18=True
                best18 = int(aList[i][7])
            elif int(aList[i][7]) < best18:
                best18 = int(aList[i][7])
            #check front 9    
            if first9 == False:
                first9 =True
                best9 = int(aList[i][5])
            elif int(aList[i][5]) < best9:
                best9 = int(aList[i][5])
            # check back9    
            if first9 == False:
                first9 =True
                best9 = int(aList[i][6])
            elif int(aList[i][6]) < best9:
                best9 = int(aList[i][6])
                
            numOfGames = numOfGames + float(1)
            # add front 9
            total9 = aList[i][5] + total9
            numOf9 = 1 + numOf9
            #add back 9
            total9 = aList[i][6] + total9
            numOf9 = 1 + numOf9
            
            total18 = aList[i][7] + total18
            numOf18 = 1 + numOf18

# Uses only 18 to calculate the average    
#    if numOf18 == 0:
#        listNum18=(0.0)
#    else:
#        listNum18=("%.1f" % (float(total18)/float(numOf18))) # Uses only 18 to calculate the average

# Uses all games (9's and 18's) to calculate the average
    if numOf9 == 0:
        listNum18=(0.0)
    else:
        listNum18=("%.1f" % (float(total9)/float(numOf9/2))) # Uses all the scores
        
    if numOf9 == 0:
        listNumof9=0.0
    else:
        listNumof9= ("%.1f" % (float(total9)/float(numOf9)))
        
    idx=(calculateIdxForDateRange(golferID, startDate, endDate))
    
    #(Number of Games,Best 18,Average 18, Best 9, Average 9, Current Index)
    returnList = (numOfGames, best18, listNum18, best9, listNumof9,idx)
    return returnList

def updateMyGolfConstant():
    if os.path.exists(MyGolfConfigFilename):
        configDataRead = importMyGolfConfig(MyGolfConfigFilename)
        return configDataRead
    else:
        return None
#        configDataRead = []
#        configDataRead.append([])
#        configDataRead[0].append(str(MyGolfDefaultStartDate))
#        configDataRead[0].append(str(MyGolfDefaultEndDate))
#        configDataRead[0].append(str(MyGolfDefaultGolfer1ID))
#        configDataRead[0].append(str(MyGolfDefaultGolfer2ID))
#        configDataRead[0].append(str(MyGolfDefaultGolfer3ID))
#        configDataRead[0].append(str(MyGolfDefaultGolfer4ID))
#        configDataRead[0].append(str(MyGolfDefaultGolferMarker))
#    return configDataRead
              
def askForDirectory(app):
    # defining options for opening a directory
    # return a directory name, starts in current directory
    dir_opt = options = {}
    options['initialdir'] = os.getcwd()
    options['mustexist'] = False
    options['parent'] = app
    options['title'] = 'Find a directory for Database MyGolf'
    return Fd.askdirectory(**dir_opt)

def getMonoFont():
    if os.name == 'nt':
        return ('courier','10','')
    else: 
        return ('mono','10','')
