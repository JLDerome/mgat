##########################################################
#
#   Departments Table
#
#   This class implements the specific manipulation of the
# data for the ECE Departments Table that the TECH Shop requires.
#

import tkinter.tix as Tix
import tkinter.ttk as ttk
#import ECECRUD as AppDB
#import ECECRUDPerson as AppDBPerson
#from ECECRUDDepartments import getDepartmentsDictionaryInfo, getDepartmentsCode
#from ECEAppDialogClass import AppDuplicatePerson, AppDisplayAbout, AppQuestionRequest
#import tkinter.filedialog as Fd
#import tkinter.messagebox as Mb
import textwrap, re, operator

from tkinter.constants import *

#from ECEAppMyClasses import *
#from ECEAppConstants import *

#from ECEShopProc import *

from PIL import Image

import tkinter.tix as Tix
import tkinter.ttk as ttk
import AppCRUD as AppCRUD
import AppCRUDGolfers as CRUDGolfers
import AppCRUDGames as CRUDGames
import AppCRUDTees as CRUDTees
import AppCRUDCourses as CRUDCourses
import tkinter.filedialog as Fd
import tkinter.messagebox as Mb
import textwrap, re
from dateutil import parser
from datetime import datetime

from tkinter.constants import *

from tableCommonWindowClass import tableCommonWindowClass
from AppClasses import *
from AppConstants import *

from AppProc import convertDateStringToOrdinal, getIndex, convertOrdinaltoString,getDBLocationFullName

from AppDialogClass import AppDisplayUnderConstruction, AppDisplayErrorMessage, AppDisplayMessage, AppQuestionRequest

courseList = []
golfersList = []
teeList = []

class golfersWindow(tableCommonWindowClass):
    def __init__(self, aWindow, aCloseCommand, loginData):
        #
        #  loginInfo format: Updater Full Name, access_Level, loginID
        #
        self.windowTitle = 'Golfers'
        self.tableName = 'Golfers'
        tableCommonWindowClass.__init__(self, aWindow, aCloseCommand, self.windowTitle, loginData, self.tableName)
        self.loginData = loginData
        self.accessLevel = self.loginData[1]
        self.updaterID = self.loginData[2]
        self.updaterFullname = self.loginData[0]
        self.aCloseCommand = aCloseCommand
        self.aWindow = aWindow
        self.displayStartDateForGame = None
        self.displayEndDateForGame = None
        self.displayGameForCourse = None
        self.display9Holes = True
        self.displayToday = False

        self.courseComboBoxData = CRUDCourses.getCourseCB()
        self.courseDictionary = {}
        self.reverseCourseDictionary = {}
        for i in range(len(self.courseComboBoxData)):
            self.courseDictionary.update({self.courseComboBoxData[i][1]: self.courseComboBoxData[i][0]})
            self.reverseCourseDictionary.update({self.courseComboBoxData[i][0]: self.courseComboBoxData[i][1]})

        self.teeComboBoxData = CRUDTees.get_tees()
        self.teeDictionary = {}
        self.reverseTeeDictionary = {}
        self.reverseTeeDictionaryCourseID = {}
        for i in range(len(self.teeComboBoxData)):
            self.teeDictionary.update({self.teeComboBoxData[i][1]: self.teeComboBoxData[i][0]})
            self.reverseTeeDictionary.update({self.teeComboBoxData[i][0]: self.teeComboBoxData[i][1]})
            self.reverseTeeDictionaryCourseID.update({self.teeComboBoxData[i][0]: self.teeComboBoxData[i][5]})

        #
        #  loginInfo format: password, access_Level, loginID, updaterID
        #
        self.updaterFullname = loginData[0]
        self.updaterID = self.loginData[2]
        self.displayCurUser.setUserName(self.updaterFullname)

        #######################################################################################
        # Inherited all from tableCommanWindowClass
        #
        #  This class intents to change the mainArea only.  Everything else must remain common
        #
        self.mainAreaRowCount = 0
        self.mainAreaColumnCount = 0
        self._reDisplayRecordFrame()
        self.mainArea.rowconfigure(self.mainAreaRowCount, weight=1)
        self.windowViewDefault()

    ############################################################ Area to add window specific commands
    #        self.closeButton = ttk.Button(self.commandFrame, text='Test', style='CommandButton.TButton',
    #                                 command= lambda: self.sampleCommand())
    #        self.closeButton.grid(row=0, column=0, sticky=N+S)


    ################################################################# Redefined commands
    #
    #  invoke from the tableCommonWindows.  These commands are specific to each tables
    #  The specific actions are redefinded here.
    #
    #    def displayRec(self, *args):
    #        print("Displaying a Record: ", self.curRecNum)
    #
    #    def save(self):
    #        #
    #        #  Table specific.  Return a True if succesful saved.
    #        #
    #        print("Saving a record (re-defined)")
    #        return True
    def tableSpecificMenu(self):
        sortsmenu = Menu(self.menubar, foreground=AppDefaultForeground, font=AppDefaultMenuFont, tearoff=0)
        sortsmenu.add_command(label="Last Name (Default)", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Default': self.sortingList(sortID))
        sortsmenu.add_command(label="Birthday", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Birthday': self.sortingList(sortID))

        self.menubar.add_cascade(label="Sorts", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                 menu=sortsmenu)

    def sort(self):
        pass
        if self.curOp == 'default':
            currentID = self.recList[self.curRecNumV.get()][0]
            if self.sortID.get() == 'Default':
                # Sorting on Date
                self.recList = sorted(self.recList, key=operator.itemgetter(2, 1))
            elif self.sortID.get() == 'Birthday':
                self.recList.sort(key=lambda row: row[4], reverse=True)
            # elif self.sortID.get() == 'Golfers':
            #     self.recList.sort(key=lambda row: row[7])
            # elif self.sortID.get() == 'Currency':
            #     self.recList.sort(key=lambda row: row[4])
            else:
                # Based on uniqueID
                self.recList.sort(key=lambda row: row[0])
            newIdx = getIndex(currentID, self.recList)
            self.curRecNumV.set(newIdx)
            self.displayRec()
        else:
            aMsg = "ERROR: Invalid request. Must be executed in default state only."
            self.myMsgBar.newMessage('error', aMsg)

    # def _enableWindowCommand(self):
    #     #
    #     #  Any command specific must added here
    #     #
    #     pass
    #
    # #        self.closeButton.config(state=NORMAL)
    #
    # def _disableWindowCommand(self):
    #     #
    #     #  Any command specific must added here
    #     #
    #     pass
    #
    # #        self.closeButton.config(state=DISABLED)

    def deleteDependencies(self, ID):
        pass
        # Need to delete the dependencies (tees and teeDetails associated with course).
        AppCRUD.initDB(getDBLocationFullName())

        gameList = CRUDGames.get_gamesID_for_GolferID(ID, True)
        for i in range(len(gameList)):
            AppCRUD.delete('Games',gameList[i][0],convertDateStringToOrdinal(todayDate), self.updaterID, True)
            aGameDetailList = CRUDGames.get_gameDetailsIDs_for_Game(gameList[i][0], True)
            for j in range(len(aGameDetailList)):
                AppCRUD.delete('GameDetails', aGameDetailList[j][0], convertDateStringToOrdinal(todayDate),
                                self.updaterID, True)

        anAddressList = CRUDAddresses.get_AddressesForOwnerID(ID, self.tableName, True)
        for i in range(len(anAddressList)):
            AppCRUD.delete('Addresses', int(anAddressList[i][0]), convertDateStringToOrdinal(todayDate), self.updaterID, True)

        AppCRUD.closeDB()

    def unDeleteDependencies(self, ID):
        # Need to unDelete the dependencies (tees and teeDetails associated with course).
        AppCRUD.initDB(getDBLocationFullName())

        gameList = CRUDGames.get_gamesID_for_GolferID(ID, True)
        for i in range(len(gameList)):
            AppCRUD.unDelete('Games',gameList[i][0],convertDateStringToOrdinal(todayDate), self.updaterID, True)
            aGameDetailList = CRUDGames.get_gameDetailsIDs_for_Game(gameList[i][0], True)
            for j in range(len(aGameDetailList)):
                AppCRUD.unDelete('GameDetails', aGameDetailList[j][0], convertDateStringToOrdinal(todayDate),
                                self.updaterID, True)

        anAddressList = CRUDAddresses.get_AddressesForOwnerID(ID, self.tableName, True)
        for i in range(len(anAddressList)):
            AppCRUD.unDelete('Addresses', int(anAddressList[i][0]), convertDateStringToOrdinal(todayDate), self.updaterID, True)

        AppCRUD.closeDB()

    def getTableData(self):
        self.recAll = CRUDGolfers.getGolfers()

    def _buildWindowsFields(self, aFrame):

        #        headerList1 = ('BASIC','VALUE','VALUED','MULT','TOLE','SIZE','DESCR','NSN1','NSN2','NSN3','NSN4')
        colTotal = 1
        self.fieldsFrame = AppFieldsFrame(aFrame, colTotal)
        self.fieldsFrame.grid(row=0, column=0, sticky=N + S + E + W)

        ########################################################### Main Identification Section
        self.identificationFrame = AppBorderFrame(self.fieldsFrame, 2)
        self.identificationFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column,
                                      sticky=E + W + N + S)
        self.identificationFrame.addTitle("Identification Section")

        self.identificationFrame.noStretchColumn(0)

        self.identificationPICFrame = AppFrame(self.identificationFrame, 1)
        self.identificationPICFrame.grid(row=self.identificationFrame.row, column=self.identificationFrame.column,
                                         sticky=N + W + E + S)


        self.selectedPicture = AppPictureFrame(self.identificationPICFrame, golferDefaultPictureName, appGolferPICDir,
                                               golferPicWidth, golferPicHeight, self.myMsgBar)
        self.selectedPicture.grid(row=self.identificationPICFrame.row, column=self.identificationPICFrame.column,
                                  sticky=N + W + E + S)

        self.identificationFrame.addColumn()

        self.identificationDATADisplayFrame = AppBorderFrame(self.identificationFrame, 1)
        self.identificationDATADisplayFrame.grid(row=self.identificationFrame.row,
                                                 column=self.identificationFrame.column,
                                                 sticky=N + W + E + S)
        self.identificationDATADisplayFrame.noBorder()


        self.selectedUserParticular = AppUserIDFrame(self.identificationDATADisplayFrame, self.myMsgBar)
        self.selectedUserParticular.grid(row=self.identificationDATADisplayFrame.row,
                                         column=self.identificationDATADisplayFrame.column,
                                         sticky=N + W + E + S)

        self.identificationDATAEditAddFrame = AppBorderFrame(self.identificationFrame, 1)
        self.identificationDATAEditAddFrame.grid(row=self.identificationFrame.row,
                                                 column=self.identificationFrame.column,
                                                 sticky=N + W + E + S)
        self.identificationDATAEditAddFrame.noBorder()


        self.golferDetailsEditAddFrame = AppColumnAlignFieldFrame(self.identificationDATAEditAddFrame)
        self.golferDetailsEditAddFrame.grid(row=self.identificationDATAEditAddFrame.row,
                                            column=self.identificationDATAEditAddFrame.column,
                                            sticky=E + W + N + S)
        self.golferDetailsEditAddFrame.noBorder()

        self.selectedFName = AppFieldEntry(self.golferDetailsEditAddFrame, None, None,
                                                    golferFNameWidth, self._aDirtyMethod)
        self.golferDetailsEditAddFrame.addNewField('First Name', self.selectedFName)

        self.selectedLName = AppFieldEntry(self.golferDetailsEditAddFrame, None, None,
                                           golferLNameWidth, self._aDirtyMethod)
        self.golferDetailsEditAddFrame.addNewField('Last Name', self.selectedLName)

        self.selectedBirthday = AppFieldEntryDate(self.golferDetailsEditAddFrame, None, None,
                                                  dateEntryFieldWidth, self._dateEnteredAction,
                                                  self.myMsgBar, self._aDirtyMethod)
        self.golferDetailsEditAddFrame.addNewField('Birthday', self.selectedBirthday)
        self.selectedBirthday.justifyLeft()


        self.identificationDATAEditAddFrame.addRow()
        self.addressesButton = AppCmdLineButton(self.identificationDATAEditAddFrame, 'Next', None, self.displayAddressesFrame)
        self.addressesButton.grid(row=self.identificationDATAEditAddFrame.row, column=self.identificationDATAEditAddFrame.column, sticky=N+S+E)
        self.addressesButton.addToolTip("Display Addresses Fields")

        self.identificationDATAAddressFrame = AppBorderFrame(self.identificationFrame, 1)
        self.identificationDATAAddressFrame.grid(row=self.identificationFrame.row, column=self.identificationFrame.column,
                                                 sticky=N + W + E + S)

        self.identificationDATAAddressFrame.noBorder()

        self.addressDetailsEditAddFrame = AppColumnAlignFieldFrame(self.identificationDATAAddressFrame)
        self.addressDetailsEditAddFrame.grid(row=self.identificationDATAAddressFrame.row, column=self.identificationDATAAddressFrame.column,
                                            sticky=E + W + N + S)
        self.addressDetailsEditAddFrame.noBorder()

        self.selectedStreet_number = AppFieldEntry(self.addressDetailsEditAddFrame, None, None,
                                                    courseStreet_numberWidth, self._aDirtyMethod)
        self.addressDetailsEditAddFrame.addNewField('Civic Address', self.selectedStreet_number)

        self.selectedTown = AppFieldEntry(self.addressDetailsEditAddFrame, None, None,
                                          courseTownWidth, self._aDirtyMethod)
        self.addressDetailsEditAddFrame.addNewField('City', self.selectedTown)

        self.selectedProv_state = AppFieldEntry(self.addressDetailsEditAddFrame, None, None,
                                                courseProv_stateWidth, self._aDirtyMethod)
        self.addressDetailsEditAddFrame.addNewField('State/Province', self.selectedProv_state)

        self.selectedPc_zip = AppFieldEntry(self.addressDetailsEditAddFrame, None, None,
                                           coursePc_zipWidth, self._aDirtyMethod)
        self.addressDetailsEditAddFrame.addNewField('Postal Code/ZIP Code', self.selectedPc_zip)

        self.selectedCountry = AppSearchLB(self.addressDetailsEditAddFrame, self, None, None, AppCountryList, courseCountryWidth,
                                           None, self.myMsgBar, self._aDirtyMethod, self._aCountryEntered)
        self.addressDetailsEditAddFrame.addNewField('Country', self.selectedCountry)

        self.selectedPhone = AppFieldEntryPhone(self.addressDetailsEditAddFrame, None, None,
                                             coursePhoneWidth, self._aDirtyMethod)
        self.selectedPhone.addValidation(self.myMsgBar)
        self.addressDetailsEditAddFrame.addNewField('Golfer Phone #', self.selectedPhone)

        self.selectedWebsite = AppFieldEntry(self.addressDetailsEditAddFrame, None, None,
                                             courseWebsiteWidth, self._aDirtyMethod)
        self.addressDetailsEditAddFrame.addNewField('Golfer Website', self.selectedWebsite)

        self.identificationDATAAddressFrame.addRow()
        self.courseFieldsButton = AppCmdLineButton(self.identificationDATAAddressFrame, 'Back', None, self.displayGolfersFieldsFrame)
        self.courseFieldsButton.grid(row=self.identificationDATAAddressFrame.row, column=self.identificationDATAAddressFrame.column, sticky=N+S+E)
        self.courseFieldsButton.addToolTip("Display Golfer Particulars")

#        ########################################################### Stats Section
#        self.fieldsFrame.addColumn()
#        self.statsFrame = AppBorderFrame(self.fieldsFrame, 1)
#        self.statsFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column,
#                                      columnspan=1, sticky=E + W + N + S)
#        self.statsFrame.addTitle("Statistics Section")

        ########################################################### Game Section
        self.fieldsFrame.addRow()
        self.fieldsFrame.stretchCurrentRow()
        self.gamesFrame = AppBorderFrame(self.fieldsFrame, 1)
        self.gamesFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column,
                                      sticky=E + W + N + S)
        self.addGameButton = AppButton(self.gamesFrame, self.theListAddImage, self.theListAddImageGREY, AppBorderWidthList,
                                       self._addNewGameButtonActionPressed)
        self.addGameButton.addToolTip("Add a Round")

        self.showHide9Button = AppShowHide9Button(self.gamesFrame, self.theListhide9Image, self.theListshow9Image, AppBorderWidthList,
                                                  self._showHide9ButtonActionPressed)
        self.showHide9Button.addToolTip("Hide 9's")
        self.showHide9Button.hide9Image()

        self.gamesFrame.addTitle("No Rounds Available for Display", self.addGameButton, self.showHide9Button)

        self.gameListingColumnCount = 15
        self.gameListingColumnHeader = [' ', 'Date', 'Front\nBack', 'Score', 'Strks', 'Net', 'Hdcp\nDiff',
                                       'Before\nIndex', 'After\nIndex', 'Course/Tee', 'Rating', 'Slope', ' ', ' ', ' ']
        self.gameListingColumnWidth = [3, 15, 6, 6, 6, 6, 6, 6, 6, 35, 6, 6, 2, 2, 2]
        self.gameListingColumnWeight = [1, 15, 4, 4, 4, 4, 4, 4, 4, 30, 4, 4, 0, 0, 0]  # Weight of 0 remove any strectching of the column

        self._CreateGameHeaderRow()
        self.gamesFrame.addRow()
        self.gamesFrame.stretchCurrentRow()

        self.gamesCanvasRow = self.gamesFrame.row

        ########################################################### Footer Section
        self.fieldsFrame.addRow()
        self.lastUpdate = AppLastUpdateFormat(self.fieldsFrame)
        self.lastUpdate.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, columnspan=colTotal,
                             sticky=E + N + S)

        self.setRequiredFields()
        self.fieldsDisable()

    def _aCountryEntered(self):
        pass

    def _CreateGameHeaderRow(self):
        self.gameHeadersRow = AppBorderFrame(self.gamesFrame, self.gameListingColumnCount)
        self.gameHeadersRow.grid(row=self.gamesFrame.row, column=self.gamesFrame.column, columnspan=self.gamesFrame.columnTotal, sticky="wens")
        self.gameHeadersRow.removeBorder()

        for i in range(len(self.gameListingColumnHeader)):
            Label(self.gameHeadersRow, border=AppBorderWidthList, relief='flat', justify=LEFT, width=self.gameListingColumnWidth[i],
                  font=fontAverageB, text=self.gameListingColumnHeader[i]).grid(row=self.gameHeadersRow.row,
                                                                               column=self.gameHeadersRow.column,
                                                                               sticky=E + W + N + S)
            self.gameHeadersRow.addColumn()
            self.gameHeadersRow.stretchSpecifyColumnAndWeight(i, self.gameListingColumnWeight[i])
        self.gameHeadersRow.grid_remove()  # Populate games will display if games are avaliable.

    def updateUponReturnMethod(self, *args):
        self.populateGames()

    def _showHide9ButtonActionPressed(self, *args):
        if self.display9Holes == True:
            self.display9Holes = False
            self.showHide9Button.changeTipMsg("Show 9's")
            self.showHide9Button.show9Image()
        else:
            self.display9Holes = True
            self.showHide9Button.changeTipMsg("Hide 9's")
            self.showHide9Button.hide9Image()
        self.populateGames()

    def _addNewGameButtonActionPressed(self, *args):
        self.displayAddGameWindow('add', self.recList[self.curRecNumV.get()][0])

    def _gameButtonActionPressed(self, mode, anID, *args):
        if mode == 'addRound':
            # AppDisplayUnderConstruction(self)
            self.displayAddGameWindow('add', self.recList[self.curRecNumV.get()][0])
        elif mode == 'edit':
            self.displayAddGameWindow(mode, self.recList[self.curRecNumV.get()][0], anID)
        elif mode == 'view':
            self.displayAddGameWindow(mode, None, anID)
        elif mode == 'archive':
            AppDisplayUnderConstruction(self)
        elif mode == 'delete':
            self._deleteGamePermanently(anID)
        else:
            aMsg="ERROR Loading Window"
            AppDisplayErrorMessage(self, "Error load Add/Edit Tee Popup Window", aMsg)

    def _deleteGamePermanently(self, gameID):
        aOrdinalDate = CRUDGames.get_GameDateOrdinal_From_GameID(gameID, False)
        aRoundNumber = CRUDGames.get_RoundNumber_From_GameID(gameID, False)

        aMsg ="A request to delete Round #{0} on {1} has been received. The command is irreversible. \n" \
              "You will not be able to recover the round data unless you have a recent backup.\n" \
              "YOUR ROUND WILL BE DELETED PERMANENTLY.\n\n"\
              "Do you wish to continue with the Delete Round Command?".format(aRoundNumber,
                                                                              convertOrdinaltoString(aOrdinalDate))

        ans = AppQuestionRequest(self, "Deleting a Round", aMsg)

        if ans.result == True:
            aGameDateOrdinal = CRUDGames.get_GameDateOrdinal_From_GameID(gameID, False)
            aGameDate = convertOrdinaltoString(aGameDateOrdinal)
            aRoundNumber = CRUDGames.get_RoundNumber_From_GameID(gameID, False)
            CRUDGames.delete_game_details_for_gameID(gameID, False)
            CRUDGames.delete_game_for_gameID(gameID, False)
            self.populateGames()
            aMsg = "Round #{0} on {1} was PERMANENTLY deleted from MyGolf.".format(aRoundNumber,aGameDate)
            self.myMsgBar.newMessage('info', aMsg)

    def displayAddressesFrame(self, *args):
        self.identificationDATAAddressFrame.grid()
        self.identificationDATAEditAddFrame.grid_remove()

    def displayGolfersFieldsFrame(self, *args):
        self.identificationDATAAddressFrame.grid_remove()
        self.identificationDATAEditAddFrame.grid()

    def _createCanvas(self):
        theRow = self.gamesCanvasRow
        theColumnSpan = self.gamesFrame.columnTotal
        self.gamesCanvas = MyScrollFrameVertical(self.gamesFrame, theColumnSpan,
                                                 self.gameListingColumnCount, theRow)

    def displayRec(self, *args):
        if len(self.recList) > 0:
            self.fieldsClear()
            self.selectedPicture.load(self.recList[self.curRecNumV.get()][3])
            self.selectedUserParticular.load(self.recList[self.curRecNumV.get()])

            self.selectedFName.load(self.recList[self.curRecNumV.get()][1])
            self.selectedLName.load(self.recList[self.curRecNumV.get()][2])
            self.selectedBirthday.load(convertOrdinaltoString(self.recList[self.curRecNumV.get()][4]))

            self.recAddressCurrentRecord = CRUDAddresses.get_golferAddresses(self.recList[self.curRecNumV.get()][0])
            if len(self.recAddressCurrentRecord) > 0:
                self.selectedTown.load(self.recAddressCurrentRecord[0][2])
                self.selectedCountry.load(self.recAddressCurrentRecord[0][4])
                self.selectedProv_state.load(self.recAddressCurrentRecord[0][3])
                self.selectedStreet_number.load(self.recAddressCurrentRecord[0][9])
                self.selectedWebsite.load(self.recAddressCurrentRecord[0][8])
                self.selectedPc_zip.load(self.recAddressCurrentRecord[0][6])
                self.selectedPhone.load(self.recAddressCurrentRecord[0][7])

            self.populateGames()
            self.lastUpdate.load(self.recList[self.curRecNumV.get()][(self.numberOfFields - 2)],
                                 self.recList[self.curRecNumV.get()][(self.numberOfFields - 1)])
            self.deletedV.set(self.recList[self.curRecNumV.get()][self.numberOfFields - 3])

            self.dirty = False
        else:
            self._displayRecordFrame()

    def _dateEnteredAction(self, *args):
        pass

    def _createHeaderFromCanvasForGame(self):
        self.parameterGameDataFrame = AppBorderFrame(self.gamesFrame, 5)
        self.parameterGameDataFrame.grid(column=self.gamesFrame.column, row=self.gamesFrame.row,
                                         columnspan=self.gamesFrame.columnTotal+1, sticky=N + S + W + E)
        #   We add a 1 for column span to offset the scroll bar of the canvas.

        self.selectedStartDate = AppFieldEntryDate(self.parameterGameDataFrame, 'Start Date', 'H', appDateWidth,
                                                   self._dateEnteredAction, self.myMsgBar)
        self.selectedStartDate.grid(row=self.parameterGameDataFrame.row, column=self.parameterGameDataFrame.column,
                                    sticky=N + S + E + W)

        self.parameterGameDataFrame.addColumn()
        self.selectedEndDate = AppFieldEntryDate(self.parameterGameDataFrame, 'End Date', 'H', appDateWidth,
                                                 self._dateEnteredAction, self.myMsgBar)
        self.selectedEndDate.grid(row=self.parameterGameDataFrame.row, column=self.parameterGameDataFrame.column,
                                  sticky=N + S + E + W)

        self.parameterGameDataFrame.addColumn()
        self.courseCBData = AppCBList(CRUDCourses.getCoursesDictionaryInfo())
        self.selectingCourse = AppSearchLB(self.parameterGameDataFrame, self, 'Select Golf Course', 'H',
                                           self.courseCBData.getList(), 40, None, self.myMsgBar, None)
        self.selectingCourse.grid(row=self.parameterGameDataFrame.row, column=self.parameterGameDataFrame.column,
                                  sticky=N + S)
        self.selectingCourse.enable()

        self.parameterGameDataFrame.addColumn()
        self.parameterGameDataFrame.noStretchColumn(self.parameterGameDataFrame.column)
        self.applyParameterButton = AppStdButton(self.parameterGameDataFrame, 'Apply', None, self._updateRoundListing)
        self.applyParameterButton.grid(row=self.parameterGameDataFrame.row, column=self.parameterGameDataFrame.column,
                                       sticky=N + E)
        self.applyParameterButton.addToolTip("Apply Round Parameter Listing")

        self.parameterGameDataFrame.addColumn()
        self.parameterGameDataFrame.noStretchColumn(self.parameterGameDataFrame.column)
        self.clearParameterButton = AppStdButton(self.parameterGameDataFrame, 'Clear', None,
                                                 self._clearGameListParameter)
        self.clearParameterButton.grid(row=self.parameterGameDataFrame.row, column=self.parameterGameDataFrame.column,
                                       sticky=N + W)
        self.clearParameterButton.addToolTip("Clear Round Parameter Listing")

        self._createListofGamesHeader(self.gamesFrame)

    def _clearGameListParameter(self, *args):
            self.displayStartDateForGame = None
            self.displayGameForCourse = None
            self.displayEndDateForGame = None

            self.selectedStartDate.clear()
            self.selectedEndDate.clear()
            self.selectingCourse.clear()

#            self._reloadGameCanvas()

    def _verifyFutureDate(self, aDate):
            aDateOrdinal = convertDateStringToOrdinal(aDate)
            todayOrdinal = convertDateStringToOrdinal(strdate)
            if aDateOrdinal > todayOrdinal:
                return False
            else:
                return True

    def _startAddEditGolfer(self, mode, golferData):
        pass

    def OnChildClose(self):
        self.AddEditGolferWindow.destroy()

    def _verifyDateRange(self):
            startOrdinal = convertDateStringToOrdinal(self.selectedStartDate.get())
            endOrdinal = convertDateStringToOrdinal(self.selectedEndDate.get())
            if endOrdinal >= startOrdinal:
                return True
            else:
                return False

    def _updateRoundListing(self, *args):
            aMsg = ''' '''
            self.myMsgBar.clearMessage()
            verified = False
            if len(self.selectedStartDate.get()) > 0 and len(self.selectedEndDate.get()) > 0 and len(
                    self.selectingCourse.get()) > 0:
                # All three field entered
                if self._verifyFutureDate(self.selectedStartDate.get()) == True and self._verifyFutureDate(
                        self.selectedEndDate.get()):
                    if self._verifyDateRange() == True:
                        verified = True
                        self.displayGameForCourse = self.selectingCourse.get()
                        self.displayStartDateForGame = self.selectedStartDate.get()
                        self.displayEndDateForGame = self.selectedEndDate.get()
                    else:
                        self.selectedEndDate.focus()
                        aMsg = "ERROR: End date can not be BEFORE the start date."
                else:
                    if self._verifyFutureDate(self.selectedStartDate.get()) == True:
                        self.selectedEndDate.focus()
                        aMsg = "ERROR: End date can not be in the future."
                    else:
                        self.selectedStartDate.focus()
                        aMsg = "ERROR: Start date can not be in the future."
            elif len(self.selectedStartDate.get()) > 0 and len(self.selectedEndDate.get()) > 0 and len(
                    self.selectingCourse.get()) == 0:
                # Dates entered only
                if self._verifyFutureDate(self.selectedStartDate.get()) == True and self._verifyFutureDate(
                        self.selectedEndDate.get()):
                    if self._verifyDateRange() == True:
                        verified = True
                        self.displayGameForCourse = None
                        self.displayStartDateForGame = self.selectedStartDate.get()
                        self.displayEndDateForGame = self.selectedEndDate.get()
                    else:
                        self.selectedEndDate.focus()
                        aMsg = "ERROR: End date can not be BEFORE the start date."
                else:
                    if self._verifyFutureDate(self.selectedStartDate.get()) == True:
                        self.selectedEndDate.focus()
                        aMsg = "ERROR: End date can not be in the future."
                    else:
                        self.selectedStartDate.focus()
                        aMsg = "ERROR: Start date can not be in the future."

            elif len(self.selectedStartDate.get()) == 0 and len(self.selectedEndDate.get()) == 0 and len(
                    self.selectingCourse.get()) > 0:
                # Course Entered only
                verified = True
                self.displayGameForCourse = self.selectingCourse.get()
                self.displayStartDateForGame = None
                self.displayEndDateForGame = None

            elif len(self.selectedStartDate.get()) > 0 and len(self.selectedEndDate.get()) == 0:
                self.selectedEndDate.focus()
                aMsg = "ERROR: End date is required if a start date is entered."

            elif len(self.selectedStartDate.get()) == 0 and len(self.selectedEndDate.get()) > 0:
                self.selectedStartDate.focus()
                aMsg = "ERROR: Start date is required if an end date is entered."

            else:
                verified = True
                self.displayGameForCourse = None
                self.displayStartDateForGame = None
                self.displayEndDateForGame = None

            if verified == True:
                self.myMsgBar.clearMessage()
                self._reloadGameCanvas()
            else:
                self.myMsgBar.newMessage('error', aMsg)

    def _courseEnteredAction(self, *args):
            self.myMsgBar.clearMessage()


    def populateGames(self):
        AppCRUD.initDB(getDBLocationFullName())
        golferID = self.recList[self.curRecNumV.get()][0]
        currentGameList = CRUDGames.get_games_for_golfer(golferID, True)

        if len(currentGameList) > 0:
            self.gamesFrame.changeTitle("My Rounds")
            self.showHide9Button.grid()
            self.gameHeadersRow.grid()  # Show the header if games are available
            #
            #  This try creates the new canvas. If it does exist,
            # destroy the old one and create the new one.
            #
            try:
                self.gamesCanvas.grid_info()
                self.gamesCanvas.destroyAll()
                self._createCanvas()
            except:
                self._createCanvas()

            gameNumber=0
            for i in range(len(currentGameList)):
                gameNumber = gameNumber + 1
                if currentGameList[i][26]  != 'All' and self.display9Holes == False:
                    gameNumber = gameNumber - 1
                else:
                    aLine = self.gamesCanvas.addAFrame(self.gameListingColumnCount)
                    if currentGameList[i][26]  == 'All':
                        lineBackground = AppDefaultBackground
                        HdcpBeforeCurrentGame = CRUDGames.calculateBeforeCurrentGameHdcp(currentGameList[i][1],
                                                                                          currentGameList[i][4],
                                                                                          currentGameList[i][0],
                                                                                          currentGameList[i][2],
                                                                                          True
                                                                                          )

                        HdcpAfterGame = CRUDGames.calculateAfterCurrentGameHdcp(currentGameList[i][1],
                                                                                currentGameList[i][4],
                                                                                currentGameList[i][0],
                                                                                currentGameList[i][2],
                                                                                True
                                                                               )

                        golferStrokes = CRUDGames.calculategolferStrokes(HdcpBeforeCurrentGame,
                                                                         currentGameList[i][25]
                                                                        )
                        netScore = currentGameList[i][7] - golferStrokes
                    else:
                        lineBackground = game9HoleBackground
                        HdcpBeforeCurrentGame = ''
                        HdcpAfterGame = ''
                        golferStrokes = ''
                        netScore = ''

                    gameListFont = fontAverage

                    Label(aLine, border=AppBorderWidthList, relief='groove', justify=LEFT, font = gameListFont,
                              width=self.gameListingColumnWidth[aLine.column], background = lineBackground,
                              text="{0}".format(gameNumber)).grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)

                    aLine.addColumn()
                    Label(aLine, border=AppBorderWidthList, relief='groove', justify=LEFT, font = gameListFont,
                          width=self.gameListingColumnWidth[aLine.column], background = lineBackground,
                          text=convertOrdinaltoString(currentGameList[i][1])).grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)

                    aLine.addColumn()
                    Label(aLine, border=AppBorderWidthList, relief='groove', justify=LEFT, font = gameListFont,
                          width=self.gameListingColumnWidth[aLine.column], background = lineBackground,
                          text="{0}/{1}".format(currentGameList[i][5],currentGameList[i][6])).grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)

                    aLine.addColumn()
                    Label(aLine, border=AppBorderWidthList, relief='groove', justify=LEFT, font = gameListFont,
                          width=self.gameListingColumnWidth[aLine.column], background = lineBackground,
                          text="{0}".format(currentGameList[i][7])).grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)

                    aLine.addColumn()
                    Label(aLine, border=AppBorderWidthList, relief='groove', justify=LEFT, font = gameListFont,
                          width=self.gameListingColumnWidth[aLine.column], background = lineBackground,
                          text="{0}".format(golferStrokes)).grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)

                    aLine.addColumn()
                    Label(aLine, border=AppBorderWidthList, relief='groove', justify=LEFT, font = gameListFont,
                          width=self.gameListingColumnWidth[aLine.column], background = lineBackground,
                          text="{0}".format(netScore)).grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)

                    aLine.addColumn()
                    Label(aLine, border=AppBorderWidthList, relief='groove', justify=LEFT, font = gameListFont,
                          width=self.gameListingColumnWidth[aLine.column], background = lineBackground,
                          text="{0}".format(currentGameList[i][8])).grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)

                    aLine.addColumn()
                    Label(aLine, border=AppBorderWidthList, relief='groove', justify=LEFT, font = gameListFont,
                          width=self.gameListingColumnWidth[aLine.column], background = lineBackground,
                          text="{0}".format(HdcpBeforeCurrentGame)).grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)

                    aLine.addColumn()
                    Label(aLine, border=AppBorderWidthList, relief='groove', justify=LEFT, font = gameListFont,
                          width=self.gameListingColumnWidth[aLine.column], background = lineBackground,
                          text="{0}".format(HdcpAfterGame)).grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)

                    aLine.addColumn()
                    courseSName = CRUDCourses.get_course_sname(True, currentGameList[i][30])
                    teeFullName = CRUDTees.get_full_tee_name_from_teeID(currentGameList[i][3],True)
                    Label(aLine, border=AppBorderWidthList, relief='groove', justify=LEFT,
                          width=self.gameListingColumnWidth[aLine.column], background = lineBackground, font = gameListFont,
                          text="{0}: {1}".format(courseSName, teeFullName)).grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)

                    aLine.addColumn()
                    if currentGameList[i][26] == 'All':
                        rating = float(currentGameList[i][24])
                        aText = "%.1f" % rating
                    else:
                        rating = float(currentGameList[i][24]) / float(2)
                        aText = "%.1f" % rating
                    Label(aLine, border=AppBorderWidthList, relief='groove', justify=LEFT, font = gameListFont,
                          width=self.gameListingColumnWidth[aLine.column], background = lineBackground,
                          text="{0}".format(aText)).grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)

                    aLine.addColumn()
                    Label(aLine, border=AppBorderWidthList, relief='groove', justify=LEFT, font = gameListFont,
                          width=self.gameListingColumnWidth[aLine.column], background = lineBackground,
                          text="{0}".format(currentGameList[i][25])).grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)

                    aLine.addColumn()
                    aButton = AppButton(aLine, self.theListEditImage, self.theListEditImageGREY, AppBorderWidthList,
                                        lambda event=Event, mode='edit',
                                               gameID=currentGameList[i][0]: self._gameButtonActionPressed(mode, gameID))
                    aButton.grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)
                    aButton.addToolTip("Edit Round")

                    aLine.addColumn()
                    aButton = AppButton(aLine, self.theListViewImage, self.theListViewImageGREY, AppBorderWidthList,
                                        lambda event=Event, mode='view',
                                               gameID=currentGameList[i][0]: self._gameButtonActionPressed(mode, gameID))
                    aButton.grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)
                    aButton.addToolTip("View Round Details")

                    aLine.addColumn()
                    aButton = AppButton(aLine, self.theListDeleteImage, self.theListDeleteImageGREY, AppBorderWidthList,
                                        lambda event=Event, mode='delete',
                                               gameID=currentGameList[i][0]: self._gameButtonActionPressed(mode, gameID))
                    aButton.grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)
                    aButton.addToolTip("Delete Round")

                    #
                    #   This allows proper column width
                    #

                    for j in range(len(self.gameListingColumnWeight)):
                        aLine.stretchSpecifyColumnAndWeight(j, self.gameListingColumnWeight[j])

                    self.gamesCanvas.addRow()
                    self.gamesCanvas.resetColumn()

            self.gamesCanvas.ResizeScrollBar()
            AppCRUD.closeDB()
        else:
            AppCRUD.closeDB()
            self.gamesFrame.changeTitle("No Rounds Available for Display")
            self.gameHeadersRow.grid_remove()
            self.showHide9Button.grid_remove()
            try:
                self.gamesCanvas.grid_info()
                self.gamesCanvas.destroyAll()
                return
            except:
                return

            # cursor.execute('''CREATE TABLE Games(
            # 0  uniqueID          INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
            # 1  gameDate        CHAR(15)    NOT NULL,
            # 2  gameNumber      SMALLINT    NOT NULL,
            # 3  teeID           SMALLINT    NOT NULL,
            # 4  golferID        SMALLINT    NOT NULL,
            # 5  frontScore      SMALLINT,
            # 6  backScore       SMALLINT,
            # 7  grossScore      SMALLINT,
            # 8  hdcpDiff        REAL,
            # 9  dEagleCNT       SMALLINT,
            # 10  eagleCNT        SMALLINT,
            # 11  birdieCNT       SMALLINT,
            # 12  parCNT          SMALLINT,
            # 13  bogieCNT        SMALLINT,
            # 14  dBogieCNT       SMALLINT,
            # 15  tBogieCNT       SMALLINT,
            # 16  othersCNT       SMALLINT,
            # 17  greens          SMALLINT,
            # 18  fairways        SMALLINT,
            # 19  leftMiss        SMALLINT,
            # 20  rightMiss       SMALLINT,
            # 21  avgDrive        SMALLINT,
            # 22  totalSands      SMALLINT,
            # 23  sandSaves       SMALLINT,
            # 24  gameRating      REAL,
            # 25  gameSlope       SMALLINT,
            # 26  holesPlayed     CHAR(7),
            # 27  totalPutts      SMALLINT,
            # 28  putts3AndUp     SMALLINT,
            # 29  totalYard       SMALLINT,
            # 30  courseID        SMALLINT,
            #   deleted         CHAR(1)     NOT NULL,
            #   lastModified         INTEGER,
            #   updaterID            INTEGER,
            #   FOREIGN KEY(golferID) REFERENCES Golfers(golferID),
            #   FOREIGN KEY(teeID) REFERENCES Tees(teeID) ON DELETE CASCADE
            #   );''')

    def _todayStatsOnly(self):
        self.displayToday = False
        self.golferStatsArea.destroy()
        self._createStatsArea()

    def _allTimeStatsOnly(self):
            self.displayToday = True
            self.golferStatsArea.destroy()
            self._createStatsArea()

    def fieldsClear(self):
        self.selectedPicture.clear()
        self.selectedFName.clear()
        self.selectedLName.clear()
        self.selectedBirthday.clear()
        self.selectedTown.clear()
        self.selectedCountry.clear()
        self.selectedProv_state.clear()
        self.selectedStreet_number.clear()
        self.selectedWebsite.clear()
        self.selectedPc_zip.clear()
        self.selectedPhone.clear()

    def fieldsDisable(self):
        self.identificationDATAEditAddFrame.grid_remove()
        self.identificationDATAAddressFrame.grid_remove()
        self.identificationDATADisplayFrame.grid()
        self.selectedPicture.disable()

    def fieldsEnable(self):
        if self.curOp == 'find':
            self.identificationDATAEditAddFrame.grid()
            self.identificationDATADisplayFrame.grid_remove()
            self.selectedFName.enable()
            self.selectedLName.enable()
            self.selectedBirthday.enable()
            if len(self.addressesButton.grid_info()) > 0:
                self.addressesButton.grid_remove()

        else:
            self.identificationDATAEditAddFrame.grid()
            self.identificationDATADisplayFrame.grid_remove()
            self.selectedPicture.enable()
            self.selectedFName.enable()
            self.selectedLName.enable()
            self.selectedBirthday.enable()

            self.selectedTown.enable()
            self.selectedCountry.enable()
            self.selectedProv_state.enable()
            self.selectedStreet_number.enable()
            self.selectedWebsite.enable()
            self.selectedPc_zip.enable()
            self.selectedPhone.enable()
            if len(self.addressesButton.grid_info()) == 0:
                self.addressesButton.grid()

    ################################################################# Redefined commands
    #
    #  invoke from the tableCommonWindows.  These commands are specific to each tables
    #  The specific actions are redefinded here.
    #
    #    def displayRec(self, *args):
    #        print("Displaying a Record: ", self.curRecNumV.get())
    #        self._displayRecordFrame()
    def setRequiredFields(self):
        pass
        self.selectedFName.setAsRequiredField()
        self.selectedLName.setAsRequiredField()
        self.selectedBirthday.setAsRequiredField()

        self.selectedTown.setAsRequiredField()
        self.selectedCountry.setAsRequiredField()
        self.selectedProv_state.setAsRequiredField()

    def resetRequiredFields(self):
        self.selectedFName.resetAsRequiredField()
        self.selectedLName.resetAsRequiredField()
        self.selectedBirthday.resetAsRequiredField()

        self.selectedTown.resetAsRequiredField()
        self.selectedCountry.resetAsRequiredField()
        self.selectedProv_state.resetAsRequiredField()

    def find(self):
        self.myMsgBar.wildcardMessage()
        self.fieldsClear()
        self.resetRequiredFields()
        self.fieldsEnable()
        if len(self.findValues) > 0:
            self.selectedFName.load(self.findValues[0])
            self.selectedLName.load(self.findValues[1])

    def edit(self):
        self.fieldsEnable()

    def add(self):
        if len(self.recList) == 0:
            self._displayRecordFrame()
        self.fieldsClear()
        self.fieldsEnable()
        self.dirty = False

    def clearPopups(self):
        # self.selectedDEPARTMENT.clearPopups()  # This is an example of the popups entry (usually a pull down menu)
        pass

    def validateRequiredFields(self):
        #
        #  required Fields:
        #
        #
        requiredFieldsEntered = True

        if len(self.selectedLName.get()) == 0:
            if len(self.identificationDATADisplayFrame.grid_info()) == 0:
                self.displayGolfersFieldsFrame()
            requiredFieldsEntered = False
            self.selectedLName.focus()

        elif len(self.selectedFName.get()) == 0:
            if len(self.identificationDATADisplayFrame.grid_info()) == 0:
                self.displayGolfersFieldsFrame()
            requiredFieldsEntered = False
            self.selectedFName.focus()

        elif len(self.selectedTown.get()) == 0:
            if len(self.identificationDATAAddressFrame.grid_info()) == 0:
                self.displayAddressesFrame()
            requiredFieldsEntered = False
            self.selectedTown.focus()

        elif len(self.selectedCountry.get()) == 0:
            if len(self.identificationDATAAddressFrame.grid_info()) == 0:
                self.displayAddressesFrame()
            requiredFieldsEntered = False
            self.selectedCountry.focus()

        elif len(self.selectedProv_state.get()) == 0:
            if len(self.identificationDATAAddressFrame.grid_info()) == 0:
                self.displayAddressesFrame()
            requiredFieldsEntered = False
            self.selectedProv_state.focus()

        if requiredFieldsEntered == False:
            self.myMsgBar.requiredFieldMessage()
        return requiredFieldsEntered

    def save(self):
        #
        #  Table specific.  Return a True if succesful saved.
        #
        #
        #  House Cleaning in case a popup still exist
        #
        requiredFieldsEntered = self.validateRequiredFields()
        if self.curOp == 'add' and requiredFieldsEntered == True:
            try:
                AppCRUD.initDB(getDBLocationFullName())
                ans = CRUDGolfers.findGolferDuplicate(self.selectedFName.get(), self.selectedLName.get(),
                                                      convertDateStringToOrdinal(self.selectedBirthday.get()), True)
                if ans == False:
                    ID = CRUDGolfers.insertGolfers(self.selectedFName.get(),
                                                   self.selectedLName.get(),
                                                   self.selectedPicture.get(),
                                                   convertDateStringToOrdinal(self.selectedBirthday.get()),
                                                   self.updaterID, True)

                    anItem = (
                        ID,
                        self.selectedFName.get(),
                        self.selectedLName.get(),
                        self.selectedPicture.get(),
                        convertDateStringToOrdinal(self.selectedBirthday.get()),
                        'N',
                        convertDateStringToOrdinal(todayDate), self.updaterID
                    )

                    CRUDAddresses.insert_addresses("Golfers", self.selectedTown.get(), self.selectedProv_state.get(),
                                                   self.selectedCountry.get(), ID,
                                                   self.selectedPc_zip.get(), self.selectedPhone.get(), self.selectedWebsite.get(),
                                                   self.selectedStreet_number.get(), 'N',
                                                   convertDateStringToOrdinal(todayDate), self.updaterID, True)

                    AppCRUD.closeDB()
                    self._updateRecList(ID, anItem)
                    aMsg = "Golfer {0} {1} has been added.".format(self.selectedFName.get(),
                                                                   self.selectedLName.get()
                                                                  )
                    self.myMsgBar.newMessage('info', aMsg)
                    return True
                else:
                    aMsg = "ERROR: Golfer {0} {1} born {2} already exist.".format(self.selectedFName.get(),
                                                                                  self.selectedLName.get(),
                                                                                  self.selectedBirthday.get()
                                                                                 )
                    AppDisplayErrorMessage(self, "Duplicate Golfer", aMsg)
                    AppCRUD.closeDB()
                    return False
            except:
                aMsg = "ERROR: An error occurred while writing to the MyGolf Database."
                AppDisplayErrorMessage(self, "Database Error", aMsg)
                AppCRUD.closeDB()
                return False

        elif self.curOp == 'edit' and requiredFieldsEntered == True:
            ID = self.recList[self.curRecNumV.get()][0]
            anItem = (
                        ID,
                        self.selectedFName.get(),
                        self.selectedLName.get(),
                        self.selectedPicture.get(),
                        convertDateStringToOrdinal(self.selectedBirthday.get()),
                        self.recList[self.curRecNumV.get()][self.numberOfFields - 3],
                        convertDateStringToOrdinal(todayDate), self.updaterID
                      )
            try:
                AppCRUD.initDB(getDBLocationFullName())
                ans = CRUDGolfers.findIfEditCreatesGolferDuplicate(
                                                                    ID,
                                                                    self.selectedFName.get(),
                                                                    self.selectedLName.get(),
                                                                    convertDateStringToOrdinal(self.selectedBirthday.get()),
                                                                    True
                                                                  )
                if ans == True:
                    AppCRUD.closeDB()
                    aMsg = "ERROR: Golfer {0} {1} born {2} already exist.\n" \
                           "Updates to current golfer can not be completed\n" \
                           "as it would create a duplicate golfer.".format(self.selectedFName.get(),
                                                                           self.selectedLName.get(),
                                                                           self.selectedBirthday.get()
                                                                           )
                    AppDisplayErrorMessage(self, "Duplicate Golfer", aMsg)
                    return False
                else:
                    CRUDGolfers.update_Golfer(ID,self.selectedFName.get(),
                                              self.selectedLName.get(), self.selectedPicture.get(),
                                              convertDateStringToOrdinal(self.selectedBirthday.get()),
                                              self.updaterID, True)

                    idxRecListAll = getIndex(self.recList[self.curRecNumV.get()][0], self.recAll)

                    addrID = CRUDAddresses.get_AddresseID(self.recList[self.curRecNumV.get()][0], True)

                    if addrID != None:
                        CRUDAddresses.update_addresses(addrID, "Golfers", self.selectedTown.get(), self.selectedProv_state.get(),
                                                       self.selectedCountry.get(), self.recList[self.curRecNumV.get()][0],
                                                       self.selectedPc_zip.get(), self.selectedPhone.get(), self.selectedWebsite.get(),
                                                       self.selectedStreet_number.get(),
                                                       convertDateStringToOrdinal(todayDate), self.updaterID, True)
                    AppCRUD.closeDB()
                    self._updateRecListAll(idxRecListAll, ID, anItem)
                    aMsg = "Golfer {0} {1} has been updated.".format(self.selectedFName.get(),
                                                                     self.selectedLName.get()
                                                                    )

                    self.myMsgBar.newMessage('info', aMsg)
                    return True
            except:
                AppCRUD.closeDB()
                aMsg = "ERROR: An error occurred while writing to the MyGolf Database."
                AppDisplayMessage(self, "Database Error", aMsg)
                return False

        elif self.curOp == 'find':
            self.fieldsDisable()
            self.recListTemp = CRUDGolfers.findGolfers(
                                                       self.selectedFName.get(),
                                                       self.selectedLName.get(),
                                                       False
                                                      )

            self.findValues = []
            self.findValues.append(self.selectedFName.get())
            self.findValues.append(self.selectedLName.get())

            if len(self.recListTemp) == 0:
                aMsg = "WARNING: No records were found using the given search criterias."
                if self.prevFind == True:
                    self.displayCurFunction.setFindResultMode()
                    self.curFind = True
                else:
                    self.displayCurFunction.setDefaultMode()
                    self.curFind = False
                self.myMsgBar.newMessage('warning', aMsg)
            elif len(self.recList) == 1:
                self.curFind = True
                self.recList = self.recListTemp
                self.displayCurFunction.setFindResultMode()
                aMsg = "One record was found using the given search criterias."
                self.curRecNumV.set(0)
                self.myMsgBar.newMessage('info', aMsg)
            else:
                self.curFind = True
                self.recList = self.recListTemp
                self.displayCurFunction.setFindResultMode()
                aMsg = "{0} records were found using the given search criterias.".format(len(self.recList))
                self.curRecNumV.set(0)
                self.myMsgBar.newMessage('info', aMsg)
            self.displayRec()
            self.clearPopups()
            self.setRequiredFields()
            return True
        else:
            aMsg = "ERROR: An error during the execution of the Save command.\n" \
                   "Contact your System Administrator."
            AppDisplayMessage(self, "Save Command Error", aMsg)
            self.clearPopups()
            return False
