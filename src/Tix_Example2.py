from tkinter import *
import tkinter.ttk as ttk 
import os, re
root = Tk()
root.minsize(500,300)
root.maxsize(550,310)

def go(Arg1, Arg2):
    print ("works" + Arg1 + Arg2)


class AutocompleteEntry(Entry):
    def __init__(self, lista, *args, **kwargs):
        Entry.__init__(self, *args, **kwargs)
        self.lista = lista
        self.var = self["textvariable"]
        if self.var == '':
            self.var = self["textvariable"] = StringVar()
        self.var.trace('w', self.changed)
        self.bind("<Right>", self.selection)
        self.bind("<Up>", self.up)
        self.bind("<Down>", self.down)
        self.lb_up = False


    def changed(self, name, index, mode):
        if self.var.get() == '':
            self.lb.destroy()
            self.lb_up = False
        else:
            words = self.comparison()
            print (words)
            if words:
                if not self.lb_up:
                    self.lb = Listbox()
                    self.lb.bind("<Double-Button-1>", self.selection)
                    self.lb.bind("<Right>", self.selection)
                    self.lb.place(x=self.winfo_x(), y=self.winfo_y()+self.winfo_height())
                    self.lb_up = True

                self.lb.delete(0, END)
                for w in words:
                    self.lb.insert(END,w)
            else:
                if self.lb_up:
                    self.lb.destroy()
                    self.lb_up = False

    def selection(self, event):
        if self.lb_up:
            self.var.set(self.lb.get(ACTIVE))
            self.list_out = self.lb.get(ACTIVE)

            self.lb.destroy()
            self.lb_up = False
            self.icursor(END)
        return self.list_out

    def up(self, event):
        if self.lb_up:
            if self.lb.curselection() == ():
                index = '0'
            else:
                index = self.lb.curselection()[0]
            if index != '0':
                self.lb.selection_clear(first=index)
                index = str(int(index)-1)
                self.lb.selection_set(first=index)
                self.lb.activate(index)

    def down(self, event):
        if self.lb_up:
            if self.lb.curselection() == ():
                index = '0'
            else:
                index = self.lb.curselection()[0]
            if index != END:
                self.lb.selection_clear(first=index)
                index = str(int(index)+1)
                self.lb.selection_set(first=index)
                self.lb.activate(index)

    def comparison(self):
        pattern = re.compile(self.var.get() + '.*')
        return [w for w in self.lista if re.match(pattern, w)]




class MyListbox:

    lista = ['a', 'actions', 'additional', 'also', 'an', 'and', 'angle', 'are', 'as', 'be', 'bind', 'bracket', 'brackets', 'button', 'can', 'cases', 'configure', 'course', 'detail', 'enter', 'event', 'events', 'example', 'field', 'fields', 'for', 'give', 'important', 'in', 'information', 'is', 'it', 'just', 'key', 'keyboard', 'kind', 'leave', 'left', 'like', 'manager', 'many', 'match', 'modifier', 'most', 'of', 'or', 'others', 'out', 'part', 'simplify', 'space', 'specifier', 'specifies', 'string;', 'that', 'the', 'there', 'to', 'type', 'unless', 'use', 'used', 'user', 'various', 'ways', 'we', 'window', 'wish', 'you']


    def __init__(self, parent, title):
        self.parent = parent
        self.parent.title(title)
        self.parent.protocol("WM_DELETE_WINDOW", self.closes)
        self.cities = ['New York', 'Vienna', 'Miami', 'Oslo']

        self.establishment()

    def combobox_handler(self, event):
        current = self.combobox.current()
        print (self.combobox.get())
        self.combo_out = self.combobox.get()
        self.entNumber.delete(0, END)

        print (self.lista)
        self.entNumber.delete(0, END)
        self.entNumber = AutocompleteEntry(self.lista, self.parent)
        self.entNumber.place(x=50, y=100)
        return self.combo_out



    def establishment(self):
        mainFrame = Frame(self.parent)
        mainFrame.pack(fill=BOTH, expand=YES)

        fr_left = Frame(mainFrame, bd=10)
        fr_left.pack(fill=BOTH, expand=YES, side=LEFT)

        self.combobox = ttk.Combobox(fr_left, values=self.cities)
        self.combobox.bind('<<ComboboxSelected>>', self.combobox_handler)
        self.combobox.pack()

        fr_right = Frame(mainFrame, bd=10)
        fr_right.pack(fill=BOTH, expand=YES, side=RIGHT)

        fr_up = Frame(fr_right)
        fr_up.pack(side=TOP, expand=YES)


        self.entNumber = Entry()
        self.entNumber.place(x=50, y=100)
        labb=Button(fr_left,text="OK",command= lambda: go(self.combo_out, AutocompleteEntry.list_out), bg="green")
        labb.pack(side=BOTTOM, fill=X, pady=5)


    def closes(self, event=None):
        self.parent.destroy()

if __name__ == '__main__':
    app = MyListbox(root, "Main Window")
    root.mainloop()