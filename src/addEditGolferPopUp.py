import tkinter.tix as Tix
import tkinter.filedialog as Fd
import random, shutil
from tkinter import *
import tkinter.ttk as ttk
import textwrap
from tkinter.constants import *
from AppConstants import *
from popUpWindowClass import popUpWindowClass
from AppClasses import *
from AppProc import resizeImage, widgetEnter,convertOrdinaltoString, focusNext
from AppProc import getAge, convertDateStringToOrdinal, updateMyAppConstant
from AppProc import stripChar
from PIL import Image, ImageTk
from PIL.Image import NORMAL
import AppCRUDGolfers as CRUDGolfers
import AppCRUDAddresses as CRUDAddresses
import AppCRUDExpenses as CRUDExpenses
from AppCRUDGames import calculateCurrentHdcp
import calendar, re
import platform
from dateutil import parser

class addEditGolferPopUp(popUpWindowClass):
    def __init__(self, aWindow, aCloseCommand, mode, expenseID, updateExpenseList, loginData):
        #
        #  loginInfo format: password, access_Level, loginID, updaterID
        #
        if mode == 'add':
            self.windowTitle = 'Add A Game'
        elif mode == 'edit':
            self.windowTitle = 'Edit A Game'
        else:
            self.windowTitle = 'An Error Occur in the Call'
        popUpWindowClass.__init__(self, aWindow, aCloseCommand, self.windowTitle, loginData)
        self.tableName = 'PopupExpenses'
        self.aCloseCommand = aCloseCommand
        self.aWindow = aWindow
        self.loginData = loginData

        self.expenseID = expenseID
        self.updateExpenseList = updateExpenseList
        self.thisExpense = []

        self.configDataRead = updateMyAppConstant(AppConfigFile)
        self.golferComboBoxData = CRUDGolfers.getGolferCB()
        self.golferDictionary = {}
        self.reverseGolferDictionary = {}
        for i in range(len(self.golferComboBoxData)):
            golfer_full = '''{0} {1}'''.format(self.golferComboBoxData[i][1], self.golferComboBoxData[i][2])
            self.golferDictionary.update({golfer_full: self.golferComboBoxData[i][0]})
            self.reverseGolferDictionary.update({self.golferComboBoxData[i][0]: golfer_full})

        #
        #  loginInfo format: Updater Full Name, access_Level, loginID
        #
        self.updaterFullname = loginData[0]
        self.updaterID = self.loginData[2]
        self.displayCurUser.setUserName(self.updaterFullname)

        #######################################################################################
        # Inherited all from tableCommanWindowClass
        #
        #  This class intents to change the mainArea only.  Everything else must remain common
        #
        self.mainAreaRowCount = 0
        self.mainAreaColumnCount = 0
        self._reDisplayRecordFrame()
        self.mainArea.rowconfigure(self.mainAreaRowCount, weight=1)
 #       self.windowViewDefault()

    ############################################################ Area to add window specific commands
    #        self.closeButton = ttk.Button(self.commandFrame, text='Test', style='CommandButton.TButton',
    #                                 command= lambda: self.sampleCommand())
    #        self.closeButton.grid(row=0, column=0, sticky=N+S)


    ################################################################# Redefined commands
    #
    #  invoke from the tableCommonWindows.  These commands are specific to each tables
    #  The specific actions are redefinded here.
    #
    #    def displayRec(self, *args):
    #        print("Displaying a Record: ", self.curRecNum)
    #
    #    def save(self):
    #        #
    #        #  Table specific.  Return a True if succesful saved.
    #        #
    #        print("Saving a record (re-defined)")
    #        return True
    def tableSpecificMenu(self):
        pass
        '''
        self.viewsmenu.add_command
        self.viewsmenu.add_command(label="Active Golfers", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                   command=lambda viewID='Active Golfers': self.basicViewsList(viewID))

        sortsmenu = Menu(self.menubar, foreground=AppDefaultForeground, font=AppDefaultMenuFont, tearoff=0)
        sortsmenu.add_command(label="Default", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Default': self.sortingList(sortID))
        sortsmenu.add_command(label="Last Name", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Last Name': self.sortingList(sortID))
        sortsmenu.add_command(label="First Name", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='First Name': self.sortingList(sortID))
        sortsmenu.add_command(label="Birthday", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Birthday': self.sortingList(sortID))
        #
        # Next Command actually puts the menu up
        #
        self.menubar.add_cascade(label="Sorts", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                 menu=sortsmenu)
        '''

    def sort(self):
        pass
        '''
        if self.curOp == 'default':
            currentID = self.recList[self.curRecNumV.get()][0]
            if self.sortID.get() == 'Default':
                self.recList = sorted(self.recList, key=operator.itemgetter(2,1))
            elif self.sortID.get() == 'Last Name':
                self.recList.sort(key=lambda row: row[2])
            elif self.sortID.get() == 'First Name':
                self.recList.sort(key=lambda row: row[1])
            elif self.sortID.get() == 'Birthday':
                self.recList.sort(key=lambda row: row[4])
            else:
                self.recList.sort(key=lambda row: row[0])
            newIdx = getIndex(currentID, self.recList)
            self.curRecNumV.set(newIdx)
            self.displayRec()
        else:
            aMsg = "ERROR: Invalid request. Must be executed in default state only."
            self.myMsgBar.newMessage('error', aMsg)
        '''

    def _buildWindowsFields(self, aFrame):
        pass
