from tkinter import filedialog
from AppClasses import *
from AppConstants import *

from PIL import Image, ImageEnhance, ImageFilter
from pytesser3 import *
from AppDigitRecognition import *
import pytesseract
import cv2
import numpy as np
import sys
import AppCRUDGolfers as CRUDGolfers
import AppCRUDCourses as CRUDCourses
import AppCRUDTees as CRUDTees
import AppCRUDGames as CRUDGames
import AppCRUD as CRUD
from AppDialogClass import AppDisplayErrorMessage, AppQuestionRequest

bufferSkewedScan = 5
topDistanceForDetection = 200
rightDistanceForDetection = 200
thresholdGrayValue = 210
widthLeftBorder = 15
widthtopBorder = 15
courseNameWidthSC = 1046
courseNameHeightSC = 135
golfer1topBorder = 317
golfer2topBorder = 490
golfer3topBorder = 750
golfer4topBorder = 920
dateLeftBorder = 2798
dateHeightSC = 65
dateWidthSC = 800
teeLeftBorder = 3417
teeHeightSC = 65
teeWidthSC = 400
teeTopBorder = 68
golfersNameHeightSC = 80
golfersNameWidthSC = 300
edgeErrorLenght = 6
GolfersFrontScoreOffsetX = 350
GolfersBackScoreOffsetX = 2300
GolfersScoreLenght = 1450
GolfersScoreHeigh = 120
GolfersDrivesHeight = 110
G1FrontScoreOffsetY = 320
G1FrontDrivesOffsetY = 400
G2FrontScoreOffsetY = 490
G2FrontDrivesOffsetY = 570
G3FrontScoreOffsetY = 735
G3FrontDrivesOffsetY = 830
G4FrontScoreOffsetY = 920
G4FrontDrivesOffsetY = 1020
lenghtScoreBox = 180
heightScoreBox = 75
heightDriveBox = 75


class AppDialogConfirm(Toplevel):
    def __init__(self, parent, title=None, data=None, image=None):

        Toplevel.__init__(self, parent)
        self.golferCBData = AppCBList(CRUDGolfers.getGolfersDictionaryInfo(True))
        self.dirty = False
        self.transient(parent)

        self.data = data  # Possible Data to display
        self.aTitle = title
        if title:
            self.title(title)

        self.row = 0
        self.column = 0
        self.rowconfigure(self.row, weight=0)
        self.columnconfigure(self.column, weight=0)

        self.parent = parent

        self.result = False

        self.image = image
        self.myMsgBar = messageBar(self)
        body = AppFrame(self, 1)
        body.noBorder()
        body.stretchCurrentRow()
        self.initial_focus = self.body(body)
        body.grid(row=self.row, column=self.column, sticky=N + S + E + W)

        self.buttonbox()
        ############################################################ Message Bar Area
        self.addRow()
        self.myMsgBar.grid(row=self.row, column=self.column, sticky=W + S,
                           padx=5)

        try:
            self.grab_set()
        except:
            pass

        if not self.initial_focus:
            self.initial_focus = self

        self.protocol("WM_DELETE_WINDOW", self.cancel)

        self.geometry("+%d+%d" % (parent.winfo_rootx() + 50,
                                  parent.winfo_rooty() + 50))

        self.initial_focus.focus_set()

        self.wait_window(self)

        self.g1Data = []
        self.g2Data = []
        self.g3Data = []
        self.g4Data = []
        self.gameData = []

    #
    # construction hooks
    def addRow(self):
        self.row = self.row + 1
        self.rowconfigure(self.row, weight=0)

    def addColumn(self):
        self.column = self.column + 1
        self.columnconfigure(self.column, weight=0)

    def body(self, master):
        # create dialog body.  return widget that should have
        # initial focus.  this method should be overridden

        pass

    def verifyEntries(self):
        # Depends on specific tasks
        return True

    def buttonbox(self):
        # add standard button box. override if you don't want the
        # standard buttons

        box = AppFrame(self, 2)
        box.noBorder()

        w = ttk.Button(box, text="Submit", style='CommandButton.TButton', command=self.ok, default=ACTIVE)
        w.grid(row=0, column=0, sticky=E + N)

        # self.bind("<Return>", self.ok)
        self.bind("<Escape>", self.cancel)

        self.addRow()
        box.grid(row=self.row, column=self.column, sticky=N + S + E + W)

    #
    # standard button semantics
    def cancel(self, event=None):
        # if self.dirty == False:
        #     self.result = True
        #     self.withdraw()
        #     self.update_idletasks()
        #     self.parent.focus_set()
        #     self.destroy()
        # else:
            aMsg = "Scorecard import has been initiated. Exiting now will cancel\n" \
                   "the current import command without saving the data.\n\n" \
                   "Do you wish to continue with the Exit/Cancel Command."
            ans = AppQuestionRequest(self, "Lost of Data", aMsg)
            if ans.result == True:
                self.result = True
                self.withdraw()
                self.update_idletasks()
                self.parent.focus_set()
                self.destroy()


    def ok(self, event=None):

        if self.verifyEntries():
            todayOrdinal = convertDateStringToOrdinal(todayDate)
            roundOrdinal = convertDateStringToOrdinal(self.selectedGameDate.get())
            teeID = self.teeCBData.getID(self.selectedTee.get())
            courseID = self.courseCBData.getID(self.selectedCourse.get())
            roundDate = self.selectedGameDate.get()

            self.g1Data.append(self.golferCBData.getID(self.selectedGolfer1.get()))
            self.g1Data.append(self.selectedHolesPlayedG1.get())

            self.g2Data.append(self.golferCBData.getID(self.selectedGolfer2.get()))
            self.g2Data.append(self.selectedHolesPlayedG2.get())

            self.g3Data.append(self.golferCBData.getID(self.selectedGolfer3.get()))
            self.g3Data.append(self.selectedHolesPlayedG3.get())

            self.g4Data.append(self.golferCBData.getID(self.selectedGolfer4.get()))
            self.g4Data.append(self.selectedHolesPlayedG4.get())

            if roundOrdinal > todayOrdinal:
                allDataItemsEntered = False
                aMsg = "ERROR: Game can not be played in the future, please adjust round date accordingly."
                self.myMsgBar.newMessage('error', aMsg)
            else:
                if len(self.g1Data) > 0:
                    # mode = None, golferID = None, gameID = None, teeID = None, courseID = None, *args)
                    self.parent.displayAddGameWindow('addFromImport', self.g1Data, None, teeID, courseID, roundDate)
                if len(self.g2Data) > 0:
                    # mode = None, golferID = None, gameID = None, teeID = None, courseID = None, *args)
                    self.parent.displayAddGameWindow('addFromImport', self.g2Data, None, teeID, courseID, roundDate)
                if len(self.g4Data) > 0:
                    # mode = None, golferID = None, gameID = None, teeID = None, courseID = None, *args)
                    self.parent.displayAddGameWindow('addFromImport', self.g4Data, None, teeID, courseID, roundDate)
                if len(self.g3Data) > 0:
                    # mode = None, golferID = None, gameID = None, teeID = None, courseID = None, *args)
                    self.parent.displayAddGameWindow('addFromImport', self.g3Data, None, teeID, courseID, roundDate)
                self.result = True
                self.withdraw()
                self.update_idletasks()
                self.parent.focus_set()
                self.destroy()
        else:
            self.myMsgBar.requiredFieldMessage()

class AppImportScorecardDialog(AppDialogConfirm):

    def body(self, master):
        self.courseID = None
        bodyFrame = AppBorderFrame(master, 2)
        bodyFrame.grid(row=0, column=0, sticky=N + S + E + W)

        # PICFrame = AppFrame(bodyFrame, 1)
        # PICFrame.noBorder()
        # PICFrame.grid(row=bodyFrame.row, cself.selectedTeeolumn=bodyFrame.column, sticky=N+W+E+S)
        #self.selectedGolfer1
        # Picture = AppPictureFrame(PICFrame, warningImage, AppmediaImagesPath,
        #                                        eceLogoPicWidth, eceLogoPicHeight, None)
        # Picture.grid(row=PICFrame.row, column=PICFrame.column, sticky=N+W+E+S)
        # Picture.disable()


        # bodyFrame.addColumn()
        messageFrame = AppFrame(bodyFrame, 4)
        messageFrame.noBorder()
        messageFrame.grid(row=bodyFrame.row, column=bodyFrame.column, sticky=N + W + E + S)

        aLabel = AppLineSectionTitle(messageFrame, self.aTitle)
        aLabel.grid(row=messageFrame.row, column=messageFrame.column, columnspan=messageFrame.columnTotal, sticky=N)

        messageFrame.addRow()
        self.selectedGameDate = AppFieldEntryDateV2(messageFrame, 'Date', 'V', appDateWidth,
                                                    None, self.myMsgBar, self._aDirtyMethod)
        self.selectedGameDate.stretchColumn()
        self.selectedGameDate.grid(row=messageFrame.row, column=messageFrame.column,
                                   columnspan = messageFrame.columnTotal, sticky=E + W + N + S)
        self.selectedGameDate.setSmallFont()
        self.selectedGameDate.setAsRequiredField()
        self.selectedGameDate.enable()

        messageFrame.addRow()
        self.courseCBData = AppCBList(CRUDCourses.getCoursesDictionaryInfo())
        # self.courseGameDataSelectionFrame.stretchSpecifyColumnAndWeight(self.courseGameDataSelectionFrame.column, 15)
        self.selectedCourse = AppSearchLB(messageFrame, self, 'Course', 'V',
                                          self.courseCBData.getList(), gameCourseLongNameWidth, None, self.myMsgBar,
                                          self._aDirtyMethod, self._verifyCourseEntry)
        self.selectedCourse.grid(row=messageFrame.row, column=messageFrame.column,
                                 columnspan = 2, sticky=N + S)
        self.selectedCourse.setSmallFont()
        self.selectedCourse.setAsRequiredField()

        messageFrame.addColumn()
        messageFrame.addColumn()
        self.teeCBData = None
        aList = []
        self.selectedTee = AppSearchLB(messageFrame, self, 'Tee', 'V',
                                          aList, gameTeeNameWidth, None, self.myMsgBar,
                                          self._aDirtyMethod, None)
        self.selectedTee.grid(row=messageFrame.row, column=messageFrame.column,
                              columnspan=2, sticky=N+S)
        self.selectedTee.setSmallFont()
        self.selectedTee.anEntry.disableSearch()
        self.selectedTee.setAsRequiredField()


        messageFrame.addRow()
        messageFrame.resetColumn()
        self.selectedGolfer1 = AppSearchLB(messageFrame, self, "Golfer #1", 'V',
                                           self.golferCBData.getList(), AppFullNameWidth, None, self.myMsgBar,
                                           self._aDirtyMethod, None)
        self.selectedGolfer1.grid(row=messageFrame.row, column=messageFrame.column,
                                 sticky=N + S)
        self.selectedGolfer1.setSmallFont()
        self.selectedGolfer1.anEntry.disableSearch()
        self.selectedGolfer1.setAsRequiredField()
        self.selectedGolfer1.enable()

        messageFrame.addColumn()
        self.selectedGolfer2 = AppSearchLB(messageFrame, self, "Golfer #2", 'V',
                                          self.golferCBData.getList(), AppFullNameWidth, None, self.myMsgBar,
                                          self._aDirtyMethod, None)
        self.selectedGolfer2.grid(row=messageFrame.row, column=messageFrame.column,
                                 sticky=N + S)
        self.selectedGolfer2.setSmallFont()
        self.selectedGolfer2.anEntry.disableSearch()
        self.selectedGolfer2.setAsRequiredField()
        self.selectedGolfer2.enable()

        messageFrame.addColumn()
        self.selectedGolfer3 = AppSearchLB(messageFrame, self, "Golfer #3", 'V',
                                          self.golferCBData.getList(), AppFullNameWidth, None, self.myMsgBar,
                                          self._aDirtyMethod, None)
        self.selectedGolfer3.grid(row=messageFrame.row, column=messageFrame.column,
                                 sticky=N + S)
        self.selectedGolfer3.setSmallFont()
        self.selectedGolfer3.anEntry.disableSearch()
        self.selectedGolfer3.setAsRequiredField()
        self.selectedGolfer3.enable()

        messageFrame.addColumn()
        self.selectedGolfer4 = AppSearchLB(messageFrame, self, "Golfer #4", 'V',
                                          self.golferCBData.getList(), AppFullNameWidth, None, self.myMsgBar,
                                          self._aDirtyMethod, None)
        self.selectedGolfer4.grid(row=messageFrame.row, column=messageFrame.column,
                                 sticky=N + S)
        self.selectedGolfer4.setSmallFont()
        self.selectedGolfer4.anEntry.disableSearch()
        self.selectedGolfer4.setAsRequiredField()
        self.selectedGolfer4.enable()

        messageFrame.addRow()
        messageFrame.resetColumn()
        self.selectedHolesPlayedG1 = AppSearchLB(messageFrame, self, 'Holes Played Golfer 1', 'V',
                                               gameHolesPlayedList, gameHolesPlayedWidth, None, self.myMsgBar,
                                               self._aDirtyMethod, None)
        self.selectedHolesPlayedG1.grid(row=messageFrame.row,
                                      column=messageFrame.column,
                                      columnspan=1, sticky=N + S)
        self.selectedHolesPlayedG1.setSmallFont()
        self.selectedHolesPlayedG1.justification(CENTER)
        self.selectedHolesPlayedG1.anEntry.disableSearch()
        self.selectedHolesPlayedG1.setAsRequiredField()

        messageFrame.addColumn()
        self.selectedHolesPlayedG2 = AppSearchLB(messageFrame, self, 'Holes Played Golfer 2', 'V',
                                               gameHolesPlayedList, gameHolesPlayedWidth, None, self.myMsgBar,
                                               self._aDirtyMethod, None)
        self.selectedHolesPlayedG2.grid(row=messageFrame.row,
                                      column=messageFrame.column,
                                      columnspan=1, sticky=N + S)
        self.selectedHolesPlayedG2.setSmallFont()
        self.selectedHolesPlayedG2.justification(CENTER)
        self.selectedHolesPlayedG2.anEntry.disableSearch()
        self.selectedHolesPlayedG2.setAsRequiredField()

        messageFrame.addColumn()
        self.selectedHolesPlayedG3 = AppSearchLB(messageFrame, self, 'Holes Played Golfer 3', 'V',
                                               gameHolesPlayedList, gameHolesPlayedWidth, None, self.myMsgBar,
                                               self._aDirtyMethod, None)
        self.selectedHolesPlayedG3.grid(row=messageFrame.row,
                                      column=messageFrame.column,
                                      columnspan=1, sticky=N + S)
        self.selectedHolesPlayedG3.setSmallFont()
        self.selectedHolesPlayedG3.justification(CENTER)
        self.selectedHolesPlayedG3.anEntry.disableSearch()
        self.selectedHolesPlayedG3.setAsRequiredField()

        messageFrame.addColumn()
        self.selectedHolesPlayedG4 = AppSearchLB(messageFrame, self, 'Holes Played Golfer 4', 'V',
                                               gameHolesPlayedList, gameHolesPlayedWidth, None, self.myMsgBar,
                                               self._aDirtyMethod, None)
        self.selectedHolesPlayedG4.grid(row=messageFrame.row,
                                      column=messageFrame.column,
                                      columnspan=1, sticky=N + S)
        self.selectedHolesPlayedG4.setSmallFont()
        self.selectedHolesPlayedG4.justification(CENTER)
        self.selectedHolesPlayedG4.anEntry.disableSearch()
        self.selectedHolesPlayedG4.setAsRequiredField()

        messageFrame.addRow()
        messageFrame.resetColumn()
        aMsg = "Verify the information above for accuracy before proceeding to\n" \
               "the final scorecard import task (reading the game details)\n\n" \
               "If required, Make the necessary changes and click submit."
        aLabel = Label(messageFrame, text=aMsg, font=fontHugeB)
        aLabel.grid(row=messageFrame.row, column=messageFrame.column, columnspan=messageFrame.columnTotal, sticky=N)

        self.dirty = False
        result = -1

        while result == -1:
            #
            #
            #
            fileName = filedialog.askopenfilename(initialdir=scannedScorecardDirName)
            self.g1Data, self.g2Data, self.g3Data, self.g4Data = self.findScorecardData(fileName)
            if len(self.g1Data) > 1:
                result = 1

    def disable(self, *args):
        self.selectedGameDate.disable()
        self.selectedCourse.disable()
        self.selectedTee.disable()
        self.selectedHolesPlayedG1.disable()
        self.selectedGolfer1.disable()
        self.selectedGolfer2.disable()
        self.selectedGolfer3.disable()
        self.selectedGolfer4.disable()
        self.selectedHolesPlayedG1.disable()
        self.selectedHolesPlayedG2.disable()
        self.selectedHolesPlayedG3.disable()
        self.selectedHolesPlayedG4.disable()
        self.dirty = False

    def enable(self, *args):
        self.selectedGameDate.enable()
        self.selectedCourse.enable()
        self.selectedTee.enable()
        self.selectedHolesPlayedG1.enable()
        self.selectedGolfer1.enable()
        self.selectedGolfer2.enable()
        self.selectedGolfer3.enable()
        self.selectedGolfer4.enable()
        self.selectedHolesPlayedG1.enable()
        self.selectedHolesPlayedG2.enable()
        self.selectedHolesPlayedG3.enable()
        self.selectedHolesPlayedG4.enable()
        self.dirty = False

    def verifyEntries(self):
        allDataItemsEntered = True
        if len(self.selectedCourse.get()) == 0:
            allDataItemsEntered = False
            self.selectedCourse.focus()
        elif len(self.selectedGameDate.get()) == 0:
            allDataItemsEntered = False
            self.selectedGameDate.focus()
        elif len(self.selectedTee.get()) == 0:
            allDataItemsEntered = False
            self.selectedTee.focus()
        elif len(self.selectedGolfer1.get()) == 0:
            allDataItemsEntered = False
            self.selectedGolfer1.focus()
        elif len(self.selectedGolfer2.get()) == 0:
            allDataItemsEntered = False
            self.selectedGolfer2.focus()
        elif len(self.selectedGolfer3.get()) == 0:
            allDataItemsEntered = False
            self.selectedGolfer3.focus()
        elif len(self.selectedGolfer4.get()) == 0:
            allDataItemsEntered = False
            self.selectedGolfer4.focus()
        elif len(self.selectedHolesPlayedG1.get()) == 0:
            allDataItemsEntered = False
            self.selectedHolesPlayedG1.focus()
        elif len(self.selectedHolesPlayedG2.get()) == 0:
            allDataItemsEntered = False
            self.selectedHolesPlayedG2.focus()
        elif len(self.selectedHolesPlayedG3.get()) == 0:
            allDataItemsEntered = False
            self.selectedHolesPlayedG3.focus()
        elif len(self.selectedHolesPlayedG4.get()) == 0:
            allDataItemsEntered = False
            self.selectedHolesPlayedG4.focus()

        return allDataItemsEntered

    def loadTeeList(self, courseID):
        self.teeCBData = AppCBList(CRUDTees.getTeesDictionaryInfo(courseID))
        aList = self.teeCBData.getList()
        self.selectedTee.updateValuesList(aList)

    def cropWhiteEdgesFromOriginalScorecard(self, anOriginalImage):
        gray = cv2.cvtColor(anOriginalImage, cv2.COLOR_BGR2GRAY)
        blurred = cv2.GaussianBlur(gray, (5, 5), 0)
        edged = cv2.Canny(blurred, 30, 150)
        (_, cnts, _) = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = sorted([(c, cv2.boundingRect(c)[0]) for c in cnts], key=lambda x: x[1])

        # cv2.imwrite("GGGGGEdgesStart.jpg", edged)
        # print("Number of rectangles full pic: ", len(cnts))
        for (c, _) in cnts:
            # compute the bounding box for the rectangle
            (x, y, w, h) = cv2.boundingRect(c)
            # print("size of rectangle is: ", x, ", ", y, ", ", w, ", ", h)
            # draw a rectangle around the digit, the show what the
            # digit was classified as
            if w > 4000 and x > 10:
                # print("Value of X: ", x)
                cv2.rectangle(anOriginalImage, (x, y), (x + w, y + h), (0, 255, 0), 1)
                im_th = edged[y:y + h, x:x + w]
                cv2.imwrite("tempFiles/GGGGGCropped.jpg", im_th)
                i = 0
                # for i in range(100):
                #     print("Value: ", im_th[i][6])
                while (im_th[i][25] == 0):
                    i = i + 1

                im_th2 = im_th[y + i:y + h - i, x:x + w]
                # cv2.imwrite("GGGGGCropped2.jpg", im_th)
                distanceFromTop = y + i
                distanceFromBottom = y + h - i
                distanceFromLeft = x
                distanceFromRight = x + w

        im_cropped = anOriginalImage[distanceFromTop:distanceFromBottom, distanceFromLeft:distanceFromRight]
        cv2.imwrite("tempFiles/imOriginalCropped.jpg", im_cropped)

        imNEW = Image.open("tempFiles/imOriginalCropped.jpg")
        return im_cropped, imNEW

    def findScorecardData(self, fn):
        im = cv2.imread(fn)
        if im is None:
            return -1

        if np.size(im, 1) < 4100:
            aMsg = "Image Resolution of the scorecard is too low. Please select a scorecard\n" \
                   "with a resolution of 300x300 dpi and ensure that it is as squared as \n" \
                   "possible otherwise errors in detection may occur."
            ans = AppDisplayErrorMessage(self, 'Scorecard Resolution', aMsg)
            if ans.result == True:
                return -1

        im_cropped, imNEW = self.cropWhiteEdgesFromOriginalScorecard(im)

        aCourseName = self.obtainStringFromImage(widthLeftBorder, courseNameHeightSC,
                                                 widthtopBorder, courseNameWidthSC,
                                                 imNEW)
        courseID = CRUDCourses.findCourseIDClosestFromCourseName(aCourseName)
        if courseID != None:
            self.selectedCourse.load(self.courseCBData.getName(courseID))
            self.loadTeeList(courseID)
            self.selectedTee.enable()
            self.selectedCourse.enable()
            self.selectedHolesPlayedG1.enable()
            self.selectedHolesPlayedG2.enable()
            self.selectedHolesPlayedG3.enable()
            self.selectedHolesPlayedG4.enable()
        else:
            self.selectedCourse.enable()

        roundTee = self.obtainStringFromImage(widthLeftBorder+teeLeftBorder, teeHeightSC, widthtopBorder + teeTopBorder,
                                              teeWidthSC, imNEW)
        selectedRoundTee = roundTee[5:]
        if courseID != None:
            try:
                anID = self.teeCBData.getID(selectedRoundTee)
                self.selectedTee.load(self.teeCBData.getName(anID))
            except:
                pass

        roundDate = self.obtainStringFromImage(widthLeftBorder+dateLeftBorder, dateHeightSC, widthtopBorder,
                                               dateWidthSC, imNEW)
        selectedRoundDate = roundDate[6:]
        try:
            anOrdinalDate = convertDateStringToOrdinal(selectedRoundDate)
            self.selectedGameDate.load(convertOrdinaltoString(anOrdinalDate))
        except:
            pass

        golfer1Name = self.obtainStringFromImage(widthLeftBorder, golfersNameHeightSC, widthtopBorder+golfer1topBorder,
                                                 golfersNameWidthSC, imNEW)
        golfer1ID = CRUDGolfers.findGolferIDClosestFromFName(golfer1Name)
        if golfer1ID != None:
            self.selectedGolfer1.load(self.golferCBData.getName(golfer1ID))

        golfer2Name = self.obtainStringFromImage(widthLeftBorder, golfersNameHeightSC, widthtopBorder+golfer2topBorder,
                                            golfersNameWidthSC, imNEW)

        golfer2ID = CRUDGolfers.findGolferIDClosestFromFName(golfer2Name)
        if golfer2ID != None:
            self.selectedGolfer2.load(self.golferCBData.getName(golfer2ID))


        golfer3Name = self.obtainStringFromImage(widthLeftBorder, golfersNameHeightSC, widthtopBorder+golfer3topBorder,
                                            golfersNameWidthSC, imNEW)
        golfer3ID = CRUDGolfers.findGolferIDClosestFromFName(golfer3Name)
        if golfer3ID != None:
            self.selectedGolfer3.load(self.golferCBData.getName(golfer3ID))


        golfer4Name = self.obtainStringFromImage(widthLeftBorder, golfersNameHeightSC, widthtopBorder+golfer4topBorder,
                                            golfersNameWidthSC, imNEW)
        golfer4ID = CRUDGolfers.findGolferIDClosestFromFName(golfer4Name)
        if golfer4ID != None:
            self.selectedGolfer4.load(self.golferCBData.getName(golfer4ID))

        G1score, G1putts = self.obtainScoreLine9Hole(GolfersFrontScoreOffsetX, G1FrontScoreOffsetY,
                                                     GolfersScoreHeigh, GolfersScoreLenght, im_cropped, "F")

        G1Bscore, G1Bputts = self.obtainScoreLine9Hole(GolfersBackScoreOffsetX, G1FrontScoreOffsetY,
                                                  GolfersScoreHeigh, GolfersScoreLenght, im_cropped, "B")

        G1drives, G1sandTraps = self.obtainDrivesSand9Hole(GolfersFrontScoreOffsetX, G1FrontDrivesOffsetY,
                                                      GolfersDrivesHeight, GolfersScoreLenght, im_cropped, "F")

        G1Bdrives, G1BsandTraps = self.obtainDrivesSand9Hole(GolfersBackScoreOffsetX, G1FrontDrivesOffsetY,
                                                        GolfersDrivesHeight, GolfersScoreLenght, im_cropped, "B")

        G2score, G2putts = self.obtainScoreLine9Hole(GolfersFrontScoreOffsetX, G2FrontScoreOffsetY,
                                                GolfersScoreHeigh, GolfersScoreLenght, im_cropped, "F")

        G2drives, G2sandTraps = self.obtainDrivesSand9Hole(GolfersFrontScoreOffsetX, G2FrontDrivesOffsetY,
                                                      GolfersDrivesHeight, GolfersScoreLenght, im_cropped, "F")

        G2Bscore, G2Bputts = self.obtainScoreLine9Hole(GolfersBackScoreOffsetX, G2FrontScoreOffsetY,
                                                  GolfersScoreHeigh, GolfersScoreLenght, im_cropped, "B")

        G2Bdrives, G2BsandTraps = self.obtainDrivesSand9Hole(GolfersBackScoreOffsetX, G2FrontDrivesOffsetY,
                                                        GolfersDrivesHeight, GolfersScoreLenght, im_cropped, "B")

        G3score, G3putts = self.obtainScoreLine9Hole(GolfersFrontScoreOffsetX, G3FrontScoreOffsetY,
                                                GolfersScoreHeigh, GolfersScoreLenght, im_cropped, "F")

        G3drives, G3sandTraps = self.obtainDrivesSand9Hole(GolfersFrontScoreOffsetX, G3FrontDrivesOffsetY,
                                                      GolfersDrivesHeight, GolfersScoreLenght, im_cropped, "F")

        G3Bscore, G3Bputts = self.obtainScoreLine9Hole(GolfersBackScoreOffsetX, G3FrontScoreOffsetY,
                                                  GolfersScoreHeigh, GolfersScoreLenght, im_cropped, "B")

        G3Bdrives, G3BsandTraps = self.obtainDrivesSand9Hole(GolfersBackScoreOffsetX, G3FrontDrivesOffsetY,
                                                        GolfersDrivesHeight, GolfersScoreLenght, im_cropped, "B")

        G4score, G4putts = self.obtainScoreLine9Hole(GolfersFrontScoreOffsetX, G4FrontScoreOffsetY,
                                                GolfersScoreHeigh, GolfersScoreLenght, im_cropped, "F")

        G4drives, G4sandTraps = self.obtainDrivesSand9Hole(GolfersFrontScoreOffsetX, G4FrontDrivesOffsetY,
                                                      GolfersDrivesHeight, GolfersScoreLenght, im_cropped, "F")

        G4Bscore, G4Bputts = self.obtainScoreLine9Hole(GolfersBackScoreOffsetX, G4FrontScoreOffsetY,
                                                  GolfersScoreHeigh, GolfersScoreLenght, im_cropped, "B")

        G4Bdrives, G4BsandTraps = self.obtainDrivesSand9Hole(GolfersBackScoreOffsetX, G4FrontDrivesOffsetY,
                                                        GolfersDrivesHeight, GolfersScoreLenght, im_cropped, "B")

        for i in range(9):
            G1score.append(G1Bscore[i])
            G1putts.append(G1Bputts[i])
            G1drives.append(G1Bdrives[i])
            G1sandTraps.append(G1BsandTraps[i])
            G2score.append(G2Bscore[i])
            G2putts.append(G2Bputts[i])
            G2drives.append(G2Bdrives[i])
            G2sandTraps.append(G2BsandTraps[i])
            G3score.append(G3Bscore[i])
            G3putts.append(G3Bputts[i])
            G3drives.append(G3Bdrives[i])
            G3sandTraps.append(G3BsandTraps[i])
            G4score.append(G4Bscore[i])
            G4putts.append(G4Bputts[i])
            G4drives.append(G4Bdrives[i])
            G4sandTraps.append(G4BsandTraps[i])

        # print("G1 Score List: ", G1score)
        # print("G1 Putt List: ", G1putts)
        # print("G1 Drives List: ", G1drives)
        # print("G1 sandTraps List: ", G1sandTraps)
        #
        # print("G2 Score List: ", G2score)
        # print("G2 Putts List: ", G2putts)
        # print("G2 Drives List: ", G2drives)
        # print("G2 sandTraps List: ", G2sandTraps)
        #
        # print("G3 Score List: ", G3score)
        # print("G3 Putts List: ", G3putts)
        # print("G3 Drives List: ", G3drives)
        # print("G3 sandTraps List: ", G3sandTraps)
        #
        # print("G4 Score List: ", G4score)
        # print("G4 Putts List: ", G4putts)
        # print("G4 Drives List: ", G4drives)
        # print("G4 sandTraps List: ", G4sandTraps)

        g1Data = []
        g1Data.append(G1score)
        g1Data.append(G1putts)
        g1Data.append(G1drives)
        g1Data.append(G1sandTraps)
        ans = self.determineHolesPlayed(G1score)
        self.selectedHolesPlayedG1.load(ans)

        g2Data = []
        g2Data.append(G2score)
        g2Data.append(G2putts)
        g2Data.append(G2drives)
        g2Data.append(G2sandTraps)
        ans = self.determineHolesPlayed(G2score)
        self.selectedHolesPlayedG2.load(ans)

        g3Data = []
        g3Data.append(G3score)
        g3Data.append(G3putts)
        g3Data.append(G3drives)
        g3Data.append(G3sandTraps)
        ans = self.determineHolesPlayed(G3score)
        self.selectedHolesPlayedG3.load(ans)

        g4Data = []
        g4Data.append(G4score)
        g4Data.append(G4putts)
        g4Data.append(G4drives)
        g4Data.append(G4sandTraps)
        ans = self.determineHolesPlayed(G4score)
        self.selectedHolesPlayedG4.load(ans)

        return g1Data, g2Data, g3Data, g4Data

    def determineHolesPlayed(self, aScoreList):
        # print("The list received: ", aScoreList)
        frontResults = 0
        backResults = 0
        for i in range(len(aScoreList)):
            if aScoreList[i] != '' and i < 9:
                frontResults = frontResults + 1
            if aScoreList[i] != '' and i > 8:
                backResults = backResults + 1

        # print("Front, Back: ", frontResults, ", ", backResults)
        if frontResults > 7 and backResults > 7:
            return 'All'
        elif frontResults > 7:
            return 'Front'
        elif backResults > 7:
            return 'Back'
        else:
            return None

    def obtainDrivesSand9Hole(self, startOffsetX, startOffsetY, height, lenght, im_passed, side):
        drives = []
        sandTraps = []
        anImage = im_passed[startOffsetY:startOffsetY + height, startOffsetX:startOffsetX + lenght]
        cv2.imwrite("tempFiles/HHHHHRectangles.jpg", anImage)
        imRect = cv2.imread("tempFiles/HHHHHRectangles.jpg")

        gray = cv2.cvtColor(anImage, cv2.COLOR_BGR2GRAY)
        blurred = cv2.GaussianBlur(gray, (5, 5), 0)
        edged = cv2.Canny(blurred, 30, 150)
        (_, cnts, _) = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = sorted([(c, cv2.boundingRect(c)[0]) for c in cnts], key=lambda x: x[1])

        cv2.imwrite("tempFiles/HHHHHEdges.jpg", edged)
        # print("Number of rectangles: ", len(cnts))
        i = 0
        lastx = 0
        for (c, _) in cnts:
            # compute the bounding box for the rectangle
            (x, y, w, h) = cv2.boundingRect(c)
            # print("Dimensions: ", cv2.boundingRect(c))
            if w > 100 and h > 70:
                if (x - lastx) > 40 or lastx == 0:
                    lastx = x
                    i = i + 1
                    # print("Rectangles Number ", i)
                    # print("x: ", x, " y: ", y, " w: ", w, " h: ", h)
                    G = gray[y + 1:y + 1 + h, x + 1:x + 1 + w]
                    fn = "tempFiles/{2}_Drives_{1}_{0}.jpg".format(i,startOffsetY, side)
                    cv2.imwrite(fn, G)

                    # print("Sending Hole #", i)
                    aListofDigits = identifyDigit(G)
                    # print("Digits Read: ", aListofDigits)

                    if len(aListofDigits) > 3:
                        aDigit = "{0}{1}{2}".format(aListofDigits[0], aListofDigits[1], aListofDigits[2])
                        drives.append(int(aDigit))

                        aDigit = int(aListofDigits[3])
                        if aDigit == 0:
                            sandTraps.append("N")
                        else:
                            sandTraps.append("Y")

                    elif len(aListofDigits) == 3:
                        aDigit = "{0}{1}{2}".format(aListofDigits[0], aListofDigits[1], aListofDigits[2])
                        drives.append(int(aDigit))
                        sandTraps.append("N")

                    elif len(aListofDigits) == 2:
                        aDigit = int(aListofDigits[0])
                        if aDigit == 0:
                            drives.append("L")
                        elif aDigit == 1:
                            drives.append("R")
                        else:
                            # It's a par 3 with a sand trap
                            drives.append('')

                        aDigit = int(aListofDigits[1])
                        if aDigit == 0:
                            sandTraps.append("N")
                        else:
                            sandTraps.append("Y")

                    elif len(aListofDigits) == 1:
                        aDigit = int(aListofDigits[0])
                        if aDigit == 0:
                            drives.append("L")
                        elif aDigit == 1:
                            drives.append("R")
                        else:
                            # It's a par 3 with a sand trap
                            drives.append('')
                        sandTraps.append("N")

                    else:
                        drives.append('')
                        sandTraps.append('')

        return drives, sandTraps

    def obtainScoreLine9Hole(self, startOffsetX, startOffsetY, height, lenght, im_passed, side):
        score = []
        putts = []
        anImage = im_passed[startOffsetY:startOffsetY + height, startOffsetX:startOffsetX + lenght]
        cv2.imwrite("tempFiles/GGGGGRectangles.jpg", anImage)
        imRect = cv2.imread("tempFiles/GGGGGRectangles.jpg")

        gray = cv2.cvtColor(anImage, cv2.COLOR_BGR2GRAY)
        blurred = cv2.GaussianBlur(gray, (5, 5), 0)
        edged = cv2.Canny(blurred, 30, 150)
        (_, cnts, _) = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = sorted([(c, cv2.boundingRect(c)[0]) for c in cnts], key=lambda x: x[1])

        cv2.imwrite("tempFiles/GGGGGEdges.jpg", edged)
        # print("Number of rectangles: ", len(cnts))
        i = 0
        lastx = 0
        for (c, _) in cnts:
            # compute the bounding box for the rectangle
            (x, y, w, h) = cv2.boundingRect(c)
            if w > 100 and h > 75:
                if (x - lastx) > 40 or lastx == 0:
                    lastx = x
                    i = i + 1
                    # print("Rectangles Number ", i)
                    # print("x: ", x, " y: ", y, " w: ", w, " h: ", h)
                    G = gray[y + 1:y + 1 + h, x + 1:x + 1 + w]
                    fn = "tempFiles/{2}_Score_{1}_{0}.jpg".format(i, startOffsetY, side)
                    cv2.imwrite(fn, G)

                    # print("Sending Hole #", i)
                    aListofDigits = identifyDigit(G)
                    # print("Digits Read: ", aListofDigits)
                    if len(aListofDigits) > 2:
                        aDigit = "{0}{1}".format(aListofDigits[0], aListofDigits[1])
                        score.append(int(aDigit))
                        putts.append(int(aListofDigits[2]))
                    elif len(aListofDigits) == 1:
                        score.append(int(aListofDigits[0]))
                        putts.append('')
                    elif len(aListofDigits) == 2:
                        score.append(int(aListofDigits[0]))
                        putts.append(int(aListofDigits[1]))
                    else:
                        score.append('')
                        putts.append('')
        return score, putts

    def obtainStringFromImage(self, widthFromLeft, theHeight, widthFromTop, theWidth, im_passed):
        anImage = im_passed.crop((widthFromLeft, widthFromTop,
                                  widthFromLeft + theWidth, widthFromTop + theHeight))
        anImage.filter(ImageFilter.SHARPEN)
        aString = pytesseract.image_to_string(anImage, lang='eng')
        charToReplace = " ~`!@#$%^&*()+={}|\][:;<,>?/ '"
        for char in charToReplace:
            aString = aString.replace(char, '')
        return aString

    def _verifyCourseEntry(self, *args):
        if len(self.selectedCourse.get()) > 0:
            try:
                courseID = self.courseCBData.getID(self.selectedCourse.get())
            except:
                return
            self.selectedTee.clear()
            self.teeCBData = AppCBList(CRUDTees.getTeesDictionaryInfo(courseID))
            aList = self.teeCBData.getList()
            self.selectedTee.updateValuesList(aList)
            self.selectedTee.enable()

    def _aDirtyMethod(self, *args):
        self.dirty = True