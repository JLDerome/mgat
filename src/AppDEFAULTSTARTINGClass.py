##########################################################
#
#   Departments Table
#
#   This class implements the specific manipulation of the
# data for the ECE Departments Table that the TECH Shop requires.
#

import tkinter.tix as Tix
import tkinter.ttk as ttk
#import ECECRUD as AppDB
#import ECECRUDPerson as AppDBPerson
#from ECECRUDDepartments import getDepartmentsDictionaryInfo, getDepartmentsCode
#from ECEAppDialogClass import AppDuplicatePerson, AppDisplayAbout, AppQuestionRequest
#import tkinter.filedialog as Fd
#import tkinter.messagebox as Mb
import textwrap, re, operator

from tkinter.constants import *

#from ECEAppMyClasses import *
#from ECEAppConstants import *

#from ECEShopProc import *

from PIL import Image

import tkinter.tix as Tix
import tkinter.ttk as ttk
import AppCRUD as CRUD
import AppCRUDGolfers as CRUDGolfers
import AppCRUDGames as CRUDGames
import AppCRUDTees as CRUDTees
import AppCRUDCourses as CRUDCourses
import tkinter.filedialog as Fd
import tkinter.messagebox as Mb
import textwrap, re
from dateutil import parser

from tkinter.constants import *

from tableCommonWindowClass import tableCommonWindowClass
from AppClasses import *
from AppConstants import *
from AppProc import *

courseList = []
golfersList = []
teeList = []

class enterYourTableNameHereWindow(tableCommonWindowClass):
    def __init__(self, aWindow, aCloseCommand, loginData):
        #
        #  loginInfo format: Updater Full Name, access_Level, loginID
        #
        self.windowTitle = 'Staring File For Add/Edit Window'
        self.tableName = 'Expenses'
        tableCommonWindowClass.__init__(self, aWindow, aCloseCommand, self.windowTitle, loginData)
        self.loginData = loginData
        self.accessLevel = self.loginData[1]
        self.updaterID = self.loginData[2]
        self.updaterFullname = self.loginData[0]
        self.aCloseCommand = aCloseCommand
        self.aWindow = aWindow
        self.displayStartDateForGame = None
        self.displayEndDateForGame = None
        self.displayGameForCourse = None
        self.display9Holes = True
        self.displayToday = False
        self.gamesListingColumnCount = 14

        self.courseComboBoxData = CRUDCourses.getCourseCB()
        self.courseDictionary = {}
        self.reverseCourseDictionary = {}
        for i in range(len(self.courseComboBoxData)):
            self.courseDictionary.update({self.courseComboBoxData[i][1]: self.courseComboBoxData[i][0]})
            self.reverseCourseDictionary.update({self.courseComboBoxData[i][0]: self.courseComboBoxData[i][1]})

        self.teeComboBoxData = CRUDTees.get_tees()
        self.teeDictionary = {}
        self.reverseTeeDictionary = {}
        self.reverseTeeDictionaryCourseID = {}
        for i in range(len(self.teeComboBoxData)):
            self.teeDictionary.update({self.teeComboBoxData[i][1]: self.teeComboBoxData[i][0]})
            self.reverseTeeDictionary.update({self.teeComboBoxData[i][0]: self.teeComboBoxData[i][1]})
            self.reverseTeeDictionaryCourseID.update({self.teeComboBoxData[i][0]: self.teeComboBoxData[i][5]})

        #
        #  loginInfo format: password, access_Level, loginID, updaterID
        #
        self.updaterFullname = loginData[0]
        self.updaterID = self.loginData[2]
        self.displayCurUser.setUserName(self.updaterFullname)

        #######################################################################################
        # Inherited all from tableCommanWindowClass
        #
        #  This class intents to change the mainArea only.  Everything else must remain common
        #
        self.mainAreaRowCount = 0
        self.mainAreaColumnCount = 0
        self._reDisplayRecordFrame()
        self.mainArea.rowconfigure(self.mainAreaRowCount, weight=1)
        self.windowViewDefault()

    ############################################################ Area to add window specific commands
    #        self.closeButton = ttk.Button(self.commandFrame, text='Test', style='CommandButton.TButton',
    #                                 command= lambda: self.sampleCommand())
    #        self.closeButton.grid(row=0, column=0, sticky=N+S)


    ################################################################# Redefined commands
    #
    #  invoke from the tableCommonWindows.  These commands are specific to each tables
    #  The specific actions are redefinded here.
    #
    #    def displayRec(self, *args):
    #        print("Displaying a Record: ", self.curRecNum)
    #
    #    def save(self):
    #        #
    #        #  Table specific.  Return a True if succesful saved.
    #        #
    #        print("Saving a record (re-defined)")
    #        return True
    def tableSpecificMenu(self):
        pass
        '''
        self.viewsmenu.add_command
        self.viewsmenu.add_command(label="Active Golfers", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                   command=lambda viewID='Active Golfers': self.basicViewsList(viewID))

        sortsmenu = Menu(self.menubar, foreground=AppDefaultForeground, font=AppDefaultMenuFont, tearoff=0)
        sortsmenu.add_command(label="Default", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Default': self.sortingList(sortID))
        sortsmenu.add_command(label="Last Name", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Last Name': self.sortingList(sortID))
        sortsmenu.add_command(label="First Name", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='First Name': self.sortingList(sortID))
        sortsmenu.add_command(label="Birthday", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Birthday': self.sortingList(sortID))
        #
        # Next Command actually puts the menu up
        #
        self.menubar.add_cascade(label="Sorts", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                 menu=sortsmenu)
        '''
    def sort(self):
        pass
        if self.curOp == 'default':
            currentID = self.recList[self.curRecNumV.get()][0]
            if self.sortID.get() == 'Default':
                self.recList = sorted(self.recList, key=operator.itemgetter(1))
                '''
                elif self.sortID.get() == 'Last Name':
                    self.recList.sort(key=lambda row: row[2])
                elif self.sortID.get() == 'First Name':
                    self.recList.sort(key=lambda row: row[1])
                elif self.sortID.get() == 'Birthday':
                    self.recList.sort(key=lambda row: row[4])
                '''
            else:
                self.recList.sort(key=lambda row: row[0])
            newIdx = getIndex(currentID, self.recList)
            self.curRecNumV.set(newIdx)
            self.displayRec()
        else:
            aMsg = "ERROR: Invalid request. Must be executed in default state only."
            self.myMsgBar.newMessage('error', aMsg)

    def displayAbout(self):
        #AppDisplayAbout(self)
        print("Working on it.....")

    def _enableWindowCommand(self):
        #
        #  Any command specific must added here
        #
        pass

    #        self.closeButton.config(state=NORMAL)

    def _disableWindowCommand(self):
        #
        #  Any command specific must added here
        #
        pass

        #cursor.execute('''CREATE TABLE MBExpenses(
        #   uniqueID            INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
        #   expenseDate          INTEGER    NOT NULL,
        #   expenseItem          CHAR(100)  NOT NULL,
        #   expenseAmount        REAL       NOT NULL,
        #   currency             CHAR(5)    NOT NULL,
        #   USExchangeRate       REAL,
        #   expenseAdjusted      REAL,
        #   golferID             INTEGER    NOT NULL,
        #   deleted              CHAR(1)    NOT NULL,
        #   lastModified         INTEGER,
        #   updaterID            INTEGER,
        #   FOREIGN KEY(golferID) REFERENCES Golfers(uniqueID) ON DELETE CASCADE
        #   );''')
    def getTableData(self):
        self.recAll = CRUDCourses.get_courses()

    def _buildWindowsFields(self, aFrame):
        #cursor.execute('''CREATE TABLE MBExpenses(
        #   uniqueID            INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
        #   expenseDate          INTEGER    NOT NULL,
        #   expenseItem          CHAR(100)  NOT NULL,
        #   expenseAmount        REAL       NOT NULL,
        #   currency             CHAR(5)    NOT NULL,
        #   USExchangeRate       REAL,
        #   expenseAdjusted      REAL,
        #   golferID             INTEGER    NOT NULL,
        #   deleted              CHAR(1)    NOT NULL,
        #   lastModified         INTEGER,
        #   updaterID            INTEGER,
        #   FOREIGN KEY(golferID) REFERENCES Golfers(uniqueID) ON DELETE CASCADE
        #   );''')
        colTotal = 1
        self.fieldsFrame = AppFieldsFrame(aFrame, colTotal)
        self.fieldsFrame.grid(row=0, column=0, sticky=N + S + E + W)

        ########################################################### Main Identification Section


        #       self.identificationFrame.noStretchColumn(0)
        ########################################################### Footer Section
        self.fieldsFrame.addRow()
        self.lastUpdate = AppLastUpdateFormat(self.fieldsFrame)
        self.lastUpdate.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, columnspan=colTotal,
                             sticky=E + N + S)

        self.setRequiredFields()
        self.fieldsDisable()

    # cursor.execute('''CREATE TABLE MBExpenses(
    #   uniqueID            INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
    #   expenseDate          INTEGER    NOT NULL,
    #   expenseItem          CHAR(100)  NOT NULL,
    #   expenseAmount        REAL       NOT NULL,
    #   currency             CHAR(5)    NOT NULL,
    #   USExchangeRate       REAL,
    #   expenseAdjusted      REAL,
    #   golferID             INTEGER    NOT NULL,
    #   deleted              CHAR(1)    NOT NULL,
    #   lastModified         INTEGER,
    #   updaterID            INTEGER,
    #   FOREIGN KEY(golferID) REFERENCES Golfers(uniqueID) ON DELETE CASCADE
    #   );''')
    def _aCountryEntered(self):
        pass

    def displayAddressesFrame(self):
        pass

    def displayCoursesFieldsFrame(self):
        self.identificationDATAAddressFrame.grid_remove()
        self.identificationDATAEditAddFrame.grid()

    def deleteDependencies(self, ID):
        pass
        # Need to delete the dependencies (tees and teeDetails associated with course).

    def unDeleteDependencies(self, ID):
        pass
        # Need to unDelete the dependencies (tees and teeDetails associated with course).

    def displayRec(self, *args):
        if len(self.recList) > 0:
            self.fieldsClear()
#            self.lastUpdate.load(self.recList[self.curRecNumV.get()][(self.numberOfFields - 2)],
#                                 self.recList[self.curRecNumV.get()][(self.numberOfFields - 1)])
#            self.deletedV.set(self.recList[self.curRecNumV.get()][self.numberOfFields - 3])
            self.dirty = False
        else:
            self._displayRecordFrame()

    def _dateEnteredAction(self, *args):
        self._aDirtyMethod(None)

    def _verifyFutureDate(self, aDate):
            aDateOrdinal = convertDateStringToOrdinal(aDate)
            todayOrdinal = convertDateStringToOrdinal(strdate)
            if aDateOrdinal > todayOrdinal:
                return False
            else:
                return True

    def _startAddEditGolfer(self, mode, golferData):
        pass

    def OnChildClose(self):
        self.AddEditGolferWindow.destroy()

    def fieldsClear(self):
        pass
#        self.selectedPicture.clear()


    def fieldsDisable(self):
        pass
#        self.identificationDATAEditAddFrame.grid_remove()


    def fieldsEnable(self):
        pass
 #       if self.curOp == 'find':
 #           self.identificationDATAEditAddFrame.grid()
 #           self.identificationDATADisplayFrame.grid_remove()
 #           self.selectedLongCourseName.enable()
 #           self.selectedShortCourseName.enable()
 #           self.selectedEstablishedYear.enable()
 #           if len(self.addressesButton.grid_info()) > 0:
 #               self.addressesButton.grid_remove()

#        else:
#            self.identificationDATAEditAddFrame.grid()
#            self.identificationDATADisplayFrame.grid_remove()
#            self.selectedPicture.enable()
#            self.selectedLongCourseName.enable()
#            self.selectedShortCourseName.enable()
#            self.selectedEstablishedYear.enable()#

#            self.selectedTown.enable()
#            self.selectedCountry.enable()
#            self.selectedProv_state.enable()
#            self.selectedStreet_number.enable()
#            self.selectedWebsite.enable()
#            self.selectedPc_zip.enable()
#            self.selectedPhone.enable()
#            if len(self.addressesButton.grid_info()) == 0:
#                self.addressesButton.grid()

    ################################################################# Redefined commands
    #
    #  invoke from the tableCommonWindows.  These commands are specific to each tables
    #  The specific actions are redefinded here.
    #
    #    def displayRec(self, *args):
    #        print("Displaying a Record: ", self.curRecNumV.get())
    #        self._displayRecordFrame()
    def setRequiredFields(self):
        pass
#        self.selectedLongCourseName.setAsRequiredField()

    def resetRequiredFields(self):
        pass
#        self.selectedLongCourseName.resetAsRequiredField()

    def find(self):
        self.fieldsClear()
        self.resetRequiredFields()
        if len(self.findValuesd) > 0:
            self.selectedLongCourseName.load(self.findValues[0])
#            self.findValues.append(self.selectedShortCourseName.get())
#            self.findValues.append(self.selectedEstablishedYear.get())
        self.fieldsEnable()
        self.dirty = False

    def edit(self):
        self.fieldsEnable()
        self.dirty = False

    def add(self):
        if len(self.recList) == 0:
            self._displayRecordFrame()
        self.fieldsClear()
        self.fieldsEnable()
        self.dirty = False

    def clearPopups(self):
        pass
#        self.selectedCountry.clearPopups()

    def validateRequiredFields(self):
        #
        #  required Fields:
        #
        #
        requiredFieldsEntered = True

#        if len(self.selectedLongCourseName.get()) == 0:
#            if len(self.identificationDATADisplayFrame.grid_info()) == 0:
#                self.displayCoursesFieldsFrame()
#            requiredFieldsEntered = False
#            self.selectedLongCourseName.focus()

#        elif len(self.selectedShortCourseName.get()) == 0:
#            if len(self.identificationDATADisplayFrame.grid_info()) == 0:
#                self.displayCoursesFieldsFrame()
#            requiredFieldsEntered = False
#            self.selectedShortCourseName.focus()


        if requiredFieldsEntered == False:
            self.myMsgBar.requiredFieldMessage()
        return requiredFieldsEntered

    def save(self):
        #
        #  Table specific.  Return a True if succesful saved.
        #
        #
        #  House Cleaning in case a popup still exist
        #
        requiredFieldsEntered = self.validateRequiredFields()

        if self.curOp == 'add' and requiredFieldsEntered == True:
            pass
            try:
                if CRUDCourses.findCourseDuplicate(self.selectedLongCourseName.get(),
                                                   self.selectedShortCourseName.get()) == None:

                    CRUDCourses.insert_courses(self.selectedLongCourseName.get(), self.selectedShortCourseName.get(),
                                               self.selectedPicture.get(), self.selectedEstablishedYear.get(),
                                               'N', convertDateStringToOrdinal(todayDate), self.updaterID)

                    ID = CRUDCourses.getCourseID(self.selectedLongCourseName.get(), self.selectedShortCourseName.get(),
                                                       self.selectedEstablishedYear.get(), convertDateStringToOrdinal(todayDate))

                    anItem = (
                        ID,
                        self.selectedLongCourseName.get(),
                        self.selectedShortCourseName.get(),
                        self.selectedPicture.get(),
                        self.selectedEstablishedYear.get(),
                        'N',
                        convertDateStringToOrdinal(todayDate), self.updaterID
                    )

                    CRUDAddresses.insert_addresses("Courses", self.selectedTown.get(), self.selectedProv_state.get(),
                                                   self.selectedCountry.get(), ID,
                                                   self.selectedPc_zip.get(), self.selectedPhone.get(), self.selectedWebsite.get(),
                                                   self.selectedStreet_number.get(), 'N',
                                                   convertDateStringToOrdinal(todayDate), self.updaterID)

                    self._updateRecList(ID, anItem)
                    aMsg = "Course {0} has been updated.".format(self.selectedLongCourseName.get())
                    self.myMsgBar.newMessage('info', aMsg)
                    return True
                else:
                    return False
                    aMsg = "ERROR: Course {0} already exist.".format(self.selectedLongCourseName.get())
                    self.myMsgBar.newMessage('error', aMsg)
            except:
                aMsg = "ERROR: An error occurred while trying to create a new Golf Course."
                self.myMsgBar.newMessage('error', aMsg)
                return False

        elif self.curOp == 'edit' and requiredFieldsEntered == True:
            pass
            ID = self.recList[self.curRecNumV.get()][0]
            anItem = (
                        ID,
                        self.selectedLongCourseName.get(),
                        self.selectedShortCourseName.get(),
                        self.selectedPicture.get(),
                        self.selectedEstablishedYear.get(),
                        self.recList[self.curRecNumV.get()][self.numberOfFields - 3],
                        convertDateStringToOrdinal(todayDate), self.updaterID
            )


            try:
                CRUDCourses.update_courses(ID, self.selectedLongCourseName.get(), self.selectedShortCourseName.get(),
                                           self.selectedPicture.get(), self.selectedEstablishedYear.get(),
                                           self.recList[self.curRecNumV.get()][self.numberOfFields - 3],
                                           convertDateStringToOrdinal(todayDate), self.updaterID)

                idxRecListAll = getIndex(self.recList[self.curRecNumV.get()][0], self.recAll)

                addrID = CRUDAddresses.get_AddresseID(self.recList[self.curRecNumV.get()][0])

                if addrID != None:
                    CRUDAddresses.update_addresses(addrID, "Courses", self.selectedTown.get(), self.selectedProv_state.get(),
                                                   self.selectedCountry.get(), self.recList[self.curRecNumV.get()][0],
                                                   self.selectedPc_zip.get(), self.selectedPhone.get(), self.selectedWebsite.get(),
                                                   self.selectedStreet_number.get(),
                                                   convertDateStringToOrdinal(todayDate), self.updaterID)

                self._updateRecListAll(idxRecListAll, ID, anItem)
                aMsg = "Course {0} has been updated.".format(self.selectedLongCourseName.get())
                self.myMsgBar.newMessage('info', aMsg)
                return True
            except:
                aMsg = "ERROR: An error occurred while writting to the MyGolf Database."
                self.myMsgBar.newMessage('error', aMsg)
                return False
                #                if addrID != None:


        elif self.curOp == 'find':
            # Important if validation for amount is on.  May create non wanted error
            # Cause by displaying a $ in front of amount and it is a non valid character
            # in an amount field.
            self.fieldsDisable()
            self.recListTemp = CRUDCourses.findCourses(
                                                        self.selectedLongCourseName.get(),
                                                        self.selectedShortCourseName.get(),
                                                        self.selectedEstablishedYear.get(),
            )
            self.findValues = []
            self.findValues.append(self.selectedLongCourseName.get())
            self.findValues.append(self.selectedShortCourseName.get())
            self.findValues.append(self.selectedEstablishedYear.get())

            if len(self.recListTemp) == 0:
                aMsg = "WARNING: No records were found using the given search criterias."
                if self.prevFind == True:
                    self.displayCurFunction.setFindResultMode()
                    self.curFind = True
                else:
                    self.displayCurFunction.setDefaultMode()
                    self.curFind = False
                self.myMsgBar.newMessage('warning', aMsg)
            elif len(self.recList) == 1:
                self.curFind = True
                self.recList = self.recListTemp
                self.displayCurFunction.setFindResultMode()
                aMsg = "One record was found using the given search criterias."
                self.curRecNumV.set(0)
                self.myMsgBar.newMessage('info', aMsg)
            else:
                self.curFind = True
                self.recList = self.recListTemp
                self.displayCurFunction.setFindResultMode()
                aMsg = "{0} records were found using the given search criterias.".format(len(self.recList))
                self.curRecNumV.set(0)
                self.myMsgBar.newMessage('info', aMsg)
            self.displayRec()
            self.clearPopups()
            self.setRequiredFields()
            return True
        else:
            self.clearPopups()
            return False


