from popUpWindowClass import popUpWindowClass
from AppClasses import *
from AppConstants import *
from AppProc import convertDateStringToOrdinal, updateMyAppConstant
from AppProc import getAYearFromOrdinal
import AppCRUDGolfers as CRUDGolfers
import AppCRUDCourses as CRUDCourses
import AppCRUDTees as CRUDTees
import AppCRUDGames as CRUDGames
import AppCRUD as CRUD
from AppDialogClass import AppQuestionRequest, AppDisplayErrorMessage, AppDisplayMessage, AppDialogOk
from AppProc import getDBLocationFullName, calculateHdcpDiff
from pylab import *
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib.pyplot as plt
from matplotlib import rcParams
from matplotlib.gridspec import GridSpec


class gameResultWindow(AppDialogOk):
    def frac(self, n):
        return 360. * n / 500

    def body(self, master):
        bodyFrame = AppBorderFrame(master, 1)
        bodyFrame.grid(row=0, column=0, sticky=N + S + E + W)
        bodyFrame.stretchCurrentRow()
        #
        #  Self.data format:
        #   0: gameID
        #

        if self.data !=None:
            # field avgDrv has been changed to total driving yards
            # cursor.execute('''CREATE TABLE Games(
            #  0   uniqueID          INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
            #  1   gameDate        CHAR(15)    NOT NULL,
            #  2   gameNumber      SMALLINT    NOT NULL,
            #  3   teeID           SMALLINT    NOT NULL,
            #  4   golferID        SMALLINT    NOT NULL,
            #  5   frontScore      SMALLINT,
            #  6   backScore       SMALLINT,
            #  7   grossScore      SMALLINT,
            #  8   hdcpDiff        REAL,
            #  9   dEagleCNT       SMALLINT,
            # 10   eagleCNT        SMALLINT,
            # 11   birdieCNT       SMALLINT,
            # 12   parCNT          SMALLINT,
            # 13   bogieCNT        SMALLINT,
            # 14   dBogieCNT       SMALLINT,
            # 15   tBogieCNT       SMALLINT,
            # 16   othersCNT       SMALLINT,
            # 17   greens          SMALLINT,
            # 18   fairways        SMALLINT,
            # 19   leftMiss        SMALLINT,
            # 20   rightMiss       SMALLINT,
            # 21   avgDrive        SMALLINT,
            # 22   totalSands      SMALLINT,
            # 23   sandSaves       SMALLINT,
            # 24   gameRating      REAL,
            # 25   gameSlope       SMALLINT,
            # 26   holesPlayed     CHAR(7),
            # 27   totalPutts      SMALLINT,
            # 28   putts3AndUp     SMALLINT,
            # 29   totalYard       SMALLINT,
            # 30   courseID        SMALLINT,
            # 31   deleted         CHAR(1)     NOT NULL,
            # 32   lastModified         INTEGER,
            # 33   updaterID            INTEGER,
            # 34   FOREIGN KEY(golferID) REFERENCES Golfers(uniqueID),
            # 35   FOREIGN KEY(teeID) REFERENCES Tees(uniqueID) ON DELETE CASCADE
            # 36   );''')

            # gameStats.append(self.frontScoreTotal)        # Indice 0
            # gameStats.append(self.backScoreTotal)         # Indice 1
            # gameStats.append(self.scoreTotal)             # Indice 2
            # gameStats.append(scoringStats[0])  # D Eagle    Indice 3
            # gameStats.append(scoringStats[1])  # Eagle      Indice 4
            # gameStats.append(scoringStats[2])  # Birdie     Indice 5
            # gameStats.append(scoringStats[3])  # Par        Indice 6
            # gameStats.append(scoringStats[4])  # Bogie      Indice 7
            # gameStats.append(scoringStats[5])  # D Bogie    Indice 8
            # gameStats.append(scoringStats[6])  # T Bogie    Indice 9
            # gameStats.append(scoringStats[7])  # Others     Indice 10
            # gameStats.append(self.GreensTotal)            # Indice 11
            # gameStats.append(self.FairwaysTotal)          # Indice 12
            # gameStats.append(drivingStats[0])  # left Miss  Indice 13
            # gameStats.append(drivingStats[1])  # Right Miss Indice 14
            # gameStats.append(drivingStats[2])  # Drv Yards  Indice 15
            # gameStats.append(self.sandTrapTotal)          # Indice 16
            # gameStats.append(self.SandSavesTotal)         # Indice 17
            # gameStats.append(self.puttsTotal)             # Indice 18
            # gameStats.append(self.putts3AndUp)            # Indice 19
            # gameStats.append(self.yardageTotal)           # Indice 20
            self.gameInfo = CRUDGames.get_game_info_for_gameID(self.data[0])
            self.golferID = self.gameInfo[0][4]
            self.gameID = self.data[0]
            self.teeID = self.gameInfo[0][3]
            self.courseID = self.gameInfo[0][30]
            self.roundNumber = self.gameInfo[0][2]
            self.dateordinal = self.gameInfo[0][1]
            self.gameStats = self.data[1]
            self.holesPlayed = self.data[2]
            self.hdcpBeforeRound = self.data[3]
            self.hdcpAfterRound = self.data[4]



        messageFrame = AppFieldsFrame(bodyFrame, 1)
        messageFrame.noBorder()
        messageFrame.grid(row=bodyFrame.row, column=bodyFrame.column, sticky=N + W + E + S)
        messageFrame.setBackgroundDefault()

        aLabel = AppLineSectionTitle(messageFrame, 'Round Stats Results for {0} on {1}, round #{2}'.format(CRUDGolfers.getGolferFullname(self.golferID),
                                                                                                          convertOrdinaltoString(self.dateordinal),
                                                                                                          self.roundNumber))
        aLabel.grid(row=messageFrame.row, column=messageFrame.column, columnspan=messageFrame.columnTotal, sticky=N)

        messageFrame.addRow()
        aLabel = AppLineSectionTitle(messageFrame, 'Course: {0} - {1} Tees'.format(CRUDCourses.get_course_sname(False, self.courseID),
                                                                              CRUDTees.get_tee_name_from_teeID(self.teeID)))
        aLabel.grid(row=messageFrame.row, column=messageFrame.column, columnspan=messageFrame.columnTotal, sticky=N)

        messageFrame.addRow()
        messageFrame.stretchCurrentRow()
        roundScoreCardFrame = AppBorderFrame(messageFrame, 1)
        roundScoreCardFrame.grid(row=messageFrame.row, column=messageFrame.column,
                                      columnspan=messageFrame.columnTotal, sticky=E + W + S + N)
        roundScoreCardFrame.addTitle("Round Scorecard")
        roundScoreCardFrame.stretchCurrentRow()


        roundDetailsTable = AppRoundDetailsTable(roundScoreCardFrame, None)
        roundDetailsTable.grid(row=roundScoreCardFrame.row, column=roundScoreCardFrame.column,
                                    sticky=E + N + W + S)
        teeDetails = CRUDTees.get_teeDetails_for_curTee(self.teeID)
        roundDetailsTable.populateTeeInformation(teeDetails, self.gameInfo[0][26],
                                                 self.golferID,
                                                 CRUDTees.get_tee_slope(self.teeID), False)
        aGameDetailList = CRUDGames.get_game_details_for_game(self.gameID, False)
        roundDetailsTable.enable()
        roundDetailsTable.populateRoundInformation(aGameDetailList)
        roundDetailsTable.disable()
        roundDetailsTable.enableView()
        totalPossibleFairways = roundDetailsTable.getTotalPossibleDrives()

        messageFrame.addRow()
        messageFrame.stretchCurrentRow()
        roundStatsFrame = AppBorderFrame(messageFrame, 2)
        roundStatsFrame.grid(row=messageFrame.row, column=messageFrame.column,
                                 columnspan=messageFrame.columnTotal, sticky=E + W + S + N)
        roundStatsFrame.addTitle("Various Round Statistics")
        roundStatsFrame.stretchCurrentRow()

        aPieFrame = AppBorderFrame(roundStatsFrame, 1)
        aPieFrame.grid(row=roundStatsFrame.row, column=roundStatsFrame.column,
                          sticky=E + N + W + S)
        aPieFrame.stretchCurrentRow()

        # gameStats.append(self.frontScoreTotal)        # Indice 0
        # gameStats.append(self.backScoreTotal)         # Indice 1
        # gameStats.append(self.scoreTotal)             # Indice 2
        # gameStats.append(scoringStats[0])  # D Eagle    Indice 3
        # gameStats.append(scoringStats[1])  # Eagle      Indice 4
        # gameStats.append(scoringStats[2])  # Birdie     Indice 5
        # gameStats.append(scoringStats[3])  # Par        Indice 6
        # gameStats.append(scoringStats[4])  # Bogie      Indice 7
        # gameStats.append(scoringStats[5])  # D Bogie    Indice 8
        # gameStats.append(scoringStats[6])  # T Bogie    Indice 9
        # gameStats.append(scoringStats[7])  # Others     Indice 10
        # gameStats.append(self.GreensTotal)            # Indice 11
        # gameStats.append(self.FairwaysTotal)          # Indice 12
        # gameStats.append(drivingStats[0])  # left Miss  Indice 13
        # gameStats.append(drivingStats[1])  # Right Miss Indice 14
        # gameStats.append(drivingStats[2])  # Drv Yards  Indice 15
        # gameStats.append(self.sandTrapTotal)          # Indice 16
        # gameStats.append(self.SandSavesTotal)         # Indice 17
        # gameStats.append(self.puttsTotal)             # Indice 18
        # gameStats.append(self.putts3AndUp)            # Indice 19
        # gameStats.append(self.yardageTotal)           # Indice 20

        pieLabels = []
        pieValues = []
        pieColors = []
        pieTotals = 0
        for i in range(len(roundScoringStatsColumnHeading)):
            if self.gameStats[i+3] != 0:
                pieLabels.append(roundScoringStatsColumnHeading[i])
                pieValues.append(self.gameStats[i+3])
                pieTotals = pieTotals + int(self.gameStats[i+3])
                pieColors.append(pieColorsScoring[i])
        #
        # print("Pie Labels: ", pieLabels)
        # print("Pie values: ", pieValues)
        # print("Pie Totals: ", pieTotals)


        # rcParams['font.family'] = fontSmallB[0]
        # font = {'family': fontSmallB[0],
        #         'weight': fontSmallB[2],
        #         'size': fontSmallB[1]
        #         }

        # fontSmaller = ('Times', 8, 'normal')

        # plt.rc('font', **font)
        # labels = ["Oranges", "Bananas", "Apples", "Kiwis", "Grapes", "Pears"]
        labels = pieLabels
        values = []
        for i in range(len(pieValues)):
            values.append(float(pieValues[i]/pieTotals))
        # values = [0.1, 0.4, 0.1, 0.2, 0.1, 0.1]

        # now to get the total number of failed in each section
        actualFigure = plt.figure(figsize=(4, 4))
        # actualFigure.suptitle("Fruit Stats", fontsize=18)

        # explode=(0, 0.05, 0, 0)
        # as explode needs to contain numerical values for each "slice" of the pie chart (i.e. every group needs to have an associated explode value)
        explode = list()
        for k in labels:
            # explode.append(0.1)
            explode.append(0)

        pie = plt.pie(values, autopct='%1.1f%%', labels=labels, colors=pieColors, explode=explode,
                      shadow=False)
        for i in range(len(pie[2])):
            pie[2][i].set_color(roundDetailsForegroundDisabledColor)
        for texts in pie[1]:
            texts.set_size(fontAverageB[1])
            texts.set_weight(fontAverageB[2])
            texts.set_color(AppDefaultForeground)
        for autotexts in pie[2]:
            autotexts.set_size(fontAverageB[1])

        # a tk.DrawingArea
        canvas = FigureCanvasTkAgg(actualFigure, master=aPieFrame)
        canvas.show()
        canvas.get_tk_widget().grid(row=0, column=0, sticky=N + W + E + S)


        roundStatsFrame.addColumn()
        aColumnFrame = AppBorderFrame(roundStatsFrame, 1)
        aColumnFrame.grid(row=roundStatsFrame.row, column=roundStatsFrame.column,
                               sticky=E + N + W + S)
        aColumnFrame.stretchCurrentRow()
        aColumnFrame.noBorder()

        roundDetailsTable = AppRoundStatsResultTable(aColumnFrame, None)
        roundDetailsTable.grid(row=aColumnFrame.row, column=aColumnFrame.column,
                               sticky=E + N + W + S)
        roundDetailsTable.populateStats(self.gameStats, self.holesPlayed, self.hdcpBeforeRound, self.hdcpAfterRound,
                                        totalPossibleFairways)


class addEditGamesPopUp(popUpWindowClass):
    def __init__(self, aWindow, aCloseCommand, mode, gameID, anUpdateUponReturnMethod, loginData, golferID=None,
                 teeID=None, courseID = None, aDate=None, aWithdrawCommand = None):
        #
        #  loginInfo format: Updater Full Name, Login ID, updaterID
        #
        if mode == 'add':
            self.windowTitle = 'Add a New Round'
        elif mode == 'edit':
            self.windowTitle = 'Edit an Existing Round'
        else:
            self.windowTitle = 'An Error Occur in the Call'
        popUpWindowClass.__init__(self, aWindow, aCloseCommand, self.windowTitle, loginData, mode)
        self.tableName = 'Games'
        self.aCloseCommand = aCloseCommand
        self.aWindow = aWindow
        self.loginData = loginData
        self.courseID = None
        self.previouslySelectedCourse = False
        self.anUpdateUponReturnMethod = anUpdateUponReturnMethod
        self.openningGolferID = golferID
        self.openningGameID = gameID
        self.openningteeID = teeID
        self.openningCourseID = courseID
        self.openningDate = aDate
        self.withdrawCommand = aWithdrawCommand
        self.displayed = False

        self.golferComboBoxData = CRUDGolfers.getGolferCB()
        self.golferDictionary = {}
        self.reverseGolferDictionary = {}
        for i in range(len(self.golferComboBoxData)):
            golfer_full = '''{0} {1}'''.format(self.golferComboBoxData[i][1], self.golferComboBoxData[i][2])
            self.golferDictionary.update({golfer_full: self.golferComboBoxData[i][0]})
            self.reverseGolferDictionary.update({self.golferComboBoxData[i][0]: golfer_full})
        self.bigSpender = self.reverseGolferDictionary[int(self.configDataRead[0][9])]
        #
        #  loginInfo format: Updater Full Name, access_Level, loginID
        #
        self.updaterFullname = loginData[0]
        self.updaterID = self.loginData[2]
        self.displayCurUser.setUserName(self.updaterFullname)

        #######################################################################################
        # Inherited all from tableCommanWindowClass
        #
        #  This class intents to change the mainArea only.  Everything else must remain common
        #
        self.mainAreaRowCount = 0
        self.mainAreaColumnCount = 0
        self._reDisplayRecordFrame()
        self.mainArea.rowconfigure(self.mainAreaRowCount, weight=1)
        if self.openningMode == 'add':
            self.clearButton.grid_remove()
            self.addCommand(None)

        if self.openningMode == 'addFromTee':
            self.addFromTeeCommand()

        if self.openningMode == 'edit':
            self.clearButton.grid_remove()
            self.editCommand(None)

        if self.openningMode == 'view':
            self.clearButton.grid_remove()
            self.viewCommand(None)

        if self.openningMode == 'addFromImport':
            #
            #  In this mode, golferID field contains the game data as well.
            #
            #       0: Golfer Scores
            #       1: Golfer Putts
            #       2: Golfer Drives
            #       3: Golfer Sand Traps
            #       4: GolferID
            #       5; Holes Played
            #
            self.gameData = golferID
            self.openningGolferID = self.gameData[4]
            self.openningHolesPlayed = self.gameData[5]
            self.addFromImportCommand()

    def scorecardBindings(self):
        try:
            self.aWindow.bind('<Control-a>', self._activateScorecard)
            self.aWindow.bind('<Control-A>', self._activateScorecard)
            self.aWindow.unbind('<Control-s>')
            self.aWindow.unbind('<Control-S>')
            self.aWindow.unbind('<Control-e>')
            self.aWindow.unbind('<Control-E>')
        except:
            pass

    def scorecardUnBindings(self):
        try:
            self.aWindow.unbind('<Control-a>')
            self.aWindow.unbind('<Control-A>')
            self.aWindow.bind('<Control-s>', self._submitForm)
            self.aWindow.bind('<Control-S>', self._submitForm)
            self.aWindow.bind('<Control-e>', self._editGameHeader)
            self.aWindow.bind('<Control-E>', self._editGameHeader)
        except:
            pass


    def formSpecificBindings(self):
        pass

    def formSpecificUnBindings(self):
        pass


    def addWindowSpecificCommand(self):

        self.editGameHeaderButton = AppButton(self.commandFrame, self.theCommandEditImage, self.theCommandEditImageGrey, AppBorderWidthList,
                                              self._editGameHeader)
        self.commandFrame.addNewCommandButton(self.editGameHeaderButton)
        self.editGameHeaderButton.addToolTip(   "Edit Game Header")

        self.scorecardButton = AppButton(self.commandFrame, self.theCommandActivateScorecardImage, self.theCommandActivateScorecardImageGrey,
                                      AppBorderWidthList, self._activateScorecard)
        self.commandFrame.addNewCommandButton(self.scorecardButton)
        self.scorecardButton.addToolTip("Activate Scorecard")

    def populateTeeDetails(self, NoTotals=False):
        teeID = self.teeCBData.getID(self.selectedTee.get())
        teeDetails = CRUDTees.get_teeDetails_for_curTee(teeID)
        self.roundDetailsTable.populateTeeInformation(teeDetails, self.selectedHolesPlayed.get(),
                                                      self.golferCBData.getID(self.selectedGolfer.get()),
                                                      CRUDTees.get_tee_slope(teeID), NoTotals)

    def add_additional(self, *args):
        self.scorecardBindings()
        self.submitButton.disable()
        self.editGameHeaderButton.enable()
        self.scorecardButton.enable()
        self.selectedGolfer.enable()
        self.selectedGolfer.clear()
        self.deletedV.set('N')
        self.previouslySelectedCourse = True
        self.roundDetailsTable.clearGameDetails()
        self.roundDetailsTable.disable()
        self.openningGolferID = None
        courseName = self.selectedCourse.get()
        courseID = self.courseCBData.getID(courseName)
        shortName = CRUDCourses.get_course_sname(False, courseID)
        aCourse = "{0} , {1}".format(shortName, self.selectedTee.get())
        self.changeFormTitle("Additional Round on {0}".format(aCourse))
        self.dirty = False
        self.selectedGolfer.focus()

    def add(self, *args):
        self.scorecardBindings()
        self.submitButton.disable()
        self.editGameHeaderButton.disable()
        self.fieldsEnable()
        self.lastUpdate.load(convertDateStringToOrdinal(todayDate),self.updaterID)
        self.deletedV.set('N')
        self.selectedGameDate.load(todayDate)
        self.selectedGameDate.focus()
        self.previouslySelectedCourse = False
        if self.openningGolferID != None:
            self.selectedGolfer.load(self.golferCBData.getName(self.openningGolferID))
            self.selectedGolfer.disable()
            self.selectedGolfer.setSmallFont()
            self.changeFormTitle("Adding a Round for {0}".format(self.golferCBData.getName(self.openningGolferID)))
            self.selectedCourse.focus()
        else:
            self.selectedGolfer.focus()
        self.dirty = False

    def addFromTeeCommand(self, *args):
        CRUD.initDB(getDBLocationFullName())
        aCourseID = CRUDTees.get_CourseID_From_TeeID(self.openningteeID, True)
        self._loadTeeList(aCourseID, True)
        courseSname = CRUDCourses.get_course_sname(True,aCourseID)
        aTeeName = self.teeCBData.getName(self.openningteeID)
        CRUD.closeDB()
        if aCourseID != None:
            self.selectedCourse.load(self.courseCBData.getName(aCourseID))
        if self.openningteeID != None:
            self.selectedTee.load(self.teeCBData.getName(self.openningteeID))
        self.selectedHolesPlayed.load('All')
        self.selectedRoundNumber.load('1')
        self.selectedGameDate.load(todayDate)
        self.changeFormTitle("Adding a Round : {0} - {1}".format(courseSname, aTeeName))
        self.lastUpdate.load(convertDateStringToOrdinal(todayDate), self.updaterID)
        self.deletedV.set('N')
        self.populateTeeDetails(True)
        self.roundDetailsTable.disable()
        self.scorecardButton.enable()
        self.editGameHeaderButton.disable()
        self.submitButton.disable()
        self.clearButton.grid_remove()
        self.selectedGameDate.enable()
        self.selectedGolfer.enable()
        self.selectedHolesPlayed.enable()
        self.selectedRoundNumber.enable()
        self.dirty = False
        self.selectedGameDate.focus()

    def addFromImportCommand(self, *args):
        self.selectedGolfer.load(self.golferCBData.getName(self.openningGolferID))
        self.selectedHolesPlayed.load(self.openningHolesPlayed)
        CRUD.initDB(getDBLocationFullName())
        aCourseID = self.openningCourseID
        self._loadTeeList(aCourseID, True)
        courseSname = CRUDCourses.get_course_sname(True,aCourseID)
        aTeeName = self.teeCBData.getName(self.openningteeID)
        CRUD.closeDB()
        if aCourseID != None:
            self.selectedCourse.load(self.courseCBData.getName(aCourseID))
        if self.openningteeID != None:
            self.selectedTee.load(self.teeCBData.getName(self.openningteeID))
        nextRoundNumber = CRUDGames.get_next_roundNum_today_for_golfer(self.openningGolferID,
                                                                       convertDateStringToOrdinal(self.openningDate))
        self.selectedRoundNumber.load(nextRoundNumber)
        self.selectedGameDate.load(self.openningDate)
        self.changeFormTitle("Import: {0} at {1} ({2})".format(self.selectedGolfer.get(), courseSname,
                                                                          aTeeName))

        self.lastUpdate.load(convertDateStringToOrdinal(todayDate), self.updaterID)
        self.deletedV.set('N')
        self.populateTeeDetails(True)
        self.roundDetailsTable.enable()
        self.roundDetailsTable.populateRoundFromImportData(self.gameData)
        self.scorecardButton.disable()
        self.editGameHeaderButton.enable()
        self.submitButton.enable()
        self.clearButton.grid_remove()
        self.selectedGameDate.disable()
        self.selectedGolfer.disable()
        self.selectedHolesPlayed.disable()
        self.selectedRoundNumber.disable()
        # self.dirty = False

    def edit(self, *args):
        CRUD.initDB(getDBLocationFullName())
        aGameDateOrdinal = CRUDGames.get_GameDateOrdinal_From_GameID(self.openningGameID, True)
        aGameDate = convertOrdinaltoString(aGameDateOrdinal)
        aGolferID = CRUDGames.get_GolferID_From_GameID(self.openningGameID, True)
        aCourseID = CRUDGames.get_CourseID_From_GameID(self.openningGameID, True)
        self._loadTeeList(aCourseID, True)
        aTeeID = CRUDGames.get_teeID_From_GameID(self.openningGameID, True)
        aHolesPLayed = CRUDGames.get_HolesPlayed_From_GameID(self.openningGameID, True)
        aRoundNumber = CRUDGames.get_RoundNumber_From_GameID(self.openningGameID, True)
        aUpdaterID = CRUDGames.get_UpdaterID_From_GameID(self.openningGameID, True)
        aGameDetailList = CRUDGames.get_game_details_for_game(self.openningGameID, True)
        CRUD.closeDB()
        if aGolferID != None:
            self.selectedGolfer.load(self.golferCBData.getName(aGolferID))
        if aCourseID != None:
            self.selectedCourse.load(self.courseCBData.getName(aCourseID))
        if aTeeID != None:
            self.selectedTee.load(self.teeCBData.getName(aTeeID))
        if aHolesPLayed != None:
            self.selectedHolesPlayed.load(aHolesPLayed)
        if aRoundNumber != None:
            self.selectedRoundNumber.load(aRoundNumber)
        if aGameDate != None:
            self.selectedGameDate.load(aGameDate)
        self.changeFormTitle("Editing Round {0} on {1} for {2}".format(aRoundNumber,
                                                                       aGameDate,
                                                                       self.selectedGolfer.get()
                                                                      ))
        self.scorecardButton.disable()
        self.lastUpdate.load(aGameDateOrdinal, aUpdaterID)
        self.deletedV.set('N')
        self.populateTeeDetails(True)
        self.roundDetailsTable.enable()
        self.roundDetailsTable.populateRoundInformation(aGameDetailList)
        self.dirty = False

    def view(self, *args):
        CRUD.initDB(getDBLocationFullName())
        aGameDateOrdinal = CRUDGames.get_GameDateOrdinal_From_GameID(self.openningGameID, True)
        aGameDate = convertOrdinaltoString(aGameDateOrdinal)
        aGolferID = CRUDGames.get_GolferID_From_GameID(self.openningGameID, True)
        aCourseID = CRUDGames.get_CourseID_From_GameID(self.openningGameID, True)
        self._loadTeeList(aCourseID, True)
        aTeeID = CRUDGames.get_teeID_From_GameID(self.openningGameID, True)
        aHolesPLayed = CRUDGames.get_HolesPlayed_From_GameID(self.openningGameID, True)
        aRoundNumber = CRUDGames.get_RoundNumber_From_GameID(self.openningGameID, True)
        aUpdaterID = CRUDGames.get_UpdaterID_From_GameID(self.openningGameID, True)
        aGameDetailList = CRUDGames.get_game_details_for_game(self.openningGameID, True)
        CRUD.closeDB()
        if aGolferID != None:
            self.selectedGolfer.load(self.golferCBData.getName(aGolferID))
        if aCourseID != None:
            self.selectedCourse.load(self.courseCBData.getName(aCourseID))
        if aTeeID != None:
            self.selectedTee.load(self.teeCBData.getName(aTeeID))
        if aHolesPLayed != None:
            self.selectedHolesPlayed.load(aHolesPLayed)
        if aRoundNumber != None:
            self.selectedRoundNumber.load(aRoundNumber)
        if aGameDate != None:
            self.selectedGameDate.load(aGameDate)
        self.changeFormTitle("Viewing Round {0} on {1} for {2}".format(aRoundNumber,
                                                                       aGameDate,
                                                                       self.selectedGolfer.get()
                                                                      ))
        self.editGameHeaderButton.grid_remove()
        self.submitButton.grid_remove()
        self.scorecardButton.grid_remove()
        self.lastUpdate.load(aGameDateOrdinal, aUpdaterID)
        self.deletedV.set('N')
        self.populateTeeDetails(True)
        self.roundDetailsTable.populateRoundInformation(aGameDetailList)
        self.roundDetailsTable.enableView()
        self.dirty = False

    def _activateScorecard(self, *args):
        ans = self._verifyHeaderData()
        if ans == True:
            self.fieldsDisable()
            self.scorecardButton.disable()
            self.scorecardUnBindings()
            self.editGameHeaderButton.enable()
            self.submitButton.enable()
            # self.teeCBData.getID(self.selectedTee.get())
            self.populateTeeDetails()
            self.roundDetailsTable.enable()
            self.scorecardUnBindings()

    def _submitForm(self, *args):
        if self.validateRequiredFields() == True:
            self.save()

    def _editGameHeader(self, *args):
        aMsg ="Updating the Game Course selection has an effect on Tee selection and has\n" \
              "scorecard implication for existing game details. It will change the results.\n"\
              "Data loses is highly probable in Par 3 Holes.\n" \
              "Round Number can NOT BE MODIFIED in edit Mode.\n" \
              "USE THIS FEATURE WITH EXTREME CAUTION.\n\n"\
              "Do you wish to continue with the Game Header Update?"

        # "Round Number and Number of Holes Played can NOT BE MODIFIED in edit Mode.\n" \

        ans = AppQuestionRequest(self, "Updating Game Header Section", aMsg)

        if ans.result == True:
            self.selectedCourse.enable()
            self.submitButton.disable()
            self.scorecardButton.enable()
            self.selectedTee.enable()
            self.scorecardBindings()
            self.editGameHeaderButton.disable()
            self.selectedGameDate.enable()
            self.selectedGolfer.enable()
            self.roundDetailsTable.disable()
            if self.openningMode == 'add':
                self.selectedRoundNumber.enable()
            self.selectedHolesPlayed.enable()
            if self.openningMode == 'addFromTee':
                self.selectedCourse.disable()
                self.selectedTee.disable()
                self.selectedRoundNumber.enable()
        else:
            return
            # self.selectedTee.enable()
            # self.selectedRoundNumber.enable()
            # self.selectedHolesPlayed.enable()

        # self.submitButton.disable()
        # self.scorecardButton.enable()
        # self.scorecardBindings()
        # self.editGameHeaderButton.disable()
        #
        # self.selectedGameDate.enable()
        # self.selectedGolfer.enable()
        # self.roundDetailsTable.disable()

    def _clearForm(self, *args):
        self.fieldsClear()
        # Must be defined in the popUp window.

    def tableSpecificMenu(self):
        pass
        '''
        self.viewsmenu.add_command
        self.viewsmenu.add_command(label="Active Golfers", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                   command=lambda viewID='Active Golfers': self.basicViewsList(viewID))

        sortsmenu = Menu(self.menubar, foreground=AppDefaultForeground, font=AppDefaultMenuFont, tearoff=0)
        sortsmenu.add_command(label="Default", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Default': self.sortingList(sortID))
        sortsmenu.add_command(label="Last Name", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Last Name': self.sortingList(sortID))
        sortsmenu.add_command(label="First Name", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='First Name': self.sortingList(sortID))
        sortsmenu.add_command(label="Birthday", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Birthday': self.sortingList(sortID))
        #
        # Next Command actually puts the menu up
        #
        self.menubar.add_cascade(label="Sorts", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                 menu=sortsmenu)
        '''

    def sort(self):
        pass
        '''
        if self.curOp == 'default':
            currentID = self.recList[self.curRecNumV.get()][0]
            if self.sortID.get() == 'Default':
                self.recList = sorted(self.recList, key=operator.itemgetter(2,1))
            elif self.sortID.get() == 'Last Name':
                self.recList.sort(key=lambda row: row[2])
            elif self.sortID.get() == 'First Name':
                self.recList.sort(key=lambda row: row[1])
            elif self.sortID.get() == 'Birthday':
                self.recList.sort(key=lambda row: row[4])
            else:
                self.recList.sort(key=lambda row: row[0])
            newIdx = getIndex(currentID, self.recList)
            self.curRecNumV.set(newIdx)
            self.displayRec()
        else:
            aMsg = "ERROR: Invalid request. Must be executed in default state only."
            self.myMsgBar.newMessage('error', aMsg)
        '''

    def _descriptionEnteredAction(self):
        pass

    def _buildWindowsFields(self, aFrame):
        #        heaExpensesderList1 = ('BASIC','VALUE','VALUED','MULT','TOLE','SIZE','DESCR','NSN1','NSN2','NSN3','NSN4')
        colTotal = 2
        self.fieldsFrame = AppFieldsFrame(aFrame, colTotal)
        self.fieldsFrame.grid(row=0, column=0, sticky=N + S + E + W)

        self.fieldsFrame.stretchSpecifyColumnAndWeight(self.fieldsFrame.column, 8)
        ########################################################### Main Identification Section
        self.golferDateSelectionFrame = AppBorderFrame(self.fieldsFrame, 2)
        self.golferDateSelectionFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column,
                                           sticky=E + W + S + N)
        self.golferDateSelectionFrame.addTitle("Golfer and Date Selection")

        self.golferCBData = AppCBList(CRUDGolfers.getGolfersDictionaryInfo(True))
        self.golferDateSelectionFrame.stretchSpecifyColumnAndWeight(self.golferDateSelectionFrame.column, 4)
        self.selectedGameDate = AppFieldEntryDateV2(self.golferDateSelectionFrame, 'Date', 'V', appDateWidth,
                                                    #self._verifyGolferDate, self.myMsgBar, self._aDirtyMethod)
                                                    None, self.myMsgBar, self._aDirtyMethod)
        self.selectedGameDate.stretchColumn()
        self.selectedGameDate.grid(row=self.golferDateSelectionFrame.row, column=self.golferDateSelectionFrame.column,
                                    sticky=E + W + N + S)
        self.selectedGameDate.setSmallFont()


        self.golferDateSelectionFrame.addColumn()
        self.golferDateSelectionFrame.stretchSpecifyColumnAndWeight(self.golferDateSelectionFrame.column, 14)
        self.selectedGolfer = AppSearchLB(self.golferDateSelectionFrame, self, "Golfer", 'V',
                                          self.golferCBData.getList(), AppFullNameWidth, None, self.myMsgBar,
                                          self._aDirtyMethod, self._golferEntered)
        self.selectedGolfer.grid(row=self.golferDateSelectionFrame.row, column=self.golferDateSelectionFrame.column, sticky=N+S)
        self.selectedGolfer.setSmallFont()
        self.selectedGolfer.anEntry.disableSearch()

        self.fieldsFrame.addColumn()
        self.fieldsFrame.stretchSpecifyColumnAndWeight(self.fieldsFrame.column, 16)
        self.courseGameDataSelectionFrame = AppBorderFrame(self.fieldsFrame, 2)
        self.courseGameDataSelectionFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column,
                                               sticky=E + W + S + N)
        self.courseGameDataSelectionFrame.addTitle("Course Details Selection")

        self.courseCBData = AppCBList(CRUDCourses.getCoursesDictionaryInfo())
        self.courseGameDataSelectionFrame.stretchSpecifyColumnAndWeight(self.courseGameDataSelectionFrame.column, 15)
        self.selectedCourse = AppSearchLB(self.courseGameDataSelectionFrame, self, 'Course', 'V',
                                          self.courseCBData.getList(), gameCourseLongNameWidth, None, self.myMsgBar,
                                          self._aDirtyMethod, self._verifyCourseEntry)
        self.selectedCourse.grid(row=self.courseGameDataSelectionFrame.row, column=self.courseGameDataSelectionFrame.column,
                                 sticky=N+S)
        self.selectedCourse.setSmallFont()

        self.courseGameDataSelectionFrame.addColumn()
        self.courseGameDataSelectionFrame.stretchSpecifyColumnAndWeight(self.courseGameDataSelectionFrame.column, 10)
        if self.courseID == None:
            self.teeCBData = None
            aList = []
        else:
            self.teeCBData = AppCBList(CRUDTees.getCoursesDictionaryInfo(self.courseID))
            aList = self.teeCBData.getList()

        self.selectedTee = AppSearchLB(self.courseGameDataSelectionFrame, self, 'Tee', 'V',
                                          aList, gameTeeNameWidth, None, self.myMsgBar,
                                          self._aDirtyMethod, None)
        self.selectedTee.grid(row=self.courseGameDataSelectionFrame.row, column=self.courseGameDataSelectionFrame.column,
                                 sticky=N+S)
        self.selectedTee.setSmallFont()
        self.selectedTee.anEntry.disableSearch()

        self.courseGameDataSelectionFrame.addColumn()
        self.courseGameDataSelectionFrame.stretchSpecifyColumnAndWeight(self.courseGameDataSelectionFrame.column, 1)
        self.selectedHolesPlayed = AppSearchLB(self.courseGameDataSelectionFrame, self, 'Holes', 'V',
                                          gameHolesPlayedList, gameHolesPlayedWidth, None, self.myMsgBar,
                                          self._aDirtyMethod, None)
        self.selectedHolesPlayed.grid(row=self.courseGameDataSelectionFrame.row, column=self.courseGameDataSelectionFrame.column,
                                 sticky=N+S)
        self.selectedHolesPlayed.setSmallFont()
        self.selectedHolesPlayed.justification(CENTER)
        self.selectedHolesPlayed.anEntry.disableSearch()

        self.courseGameDataSelectionFrame.addColumn()
        self.courseGameDataSelectionFrame.stretchSpecifyColumnAndWeight(self.courseGameDataSelectionFrame.column, 1)
        self.selectedRoundNumber = AppSearchLB(self.courseGameDataSelectionFrame, self, 'Rnd #', 'V',
                                          gameRoundNumList, gameRoundNumWidth, None, self.myMsgBar,
                                          self._aDirtyMethod, self._roundNumberEntered)
        self.selectedRoundNumber.grid(row=self.courseGameDataSelectionFrame.row, column=self.courseGameDataSelectionFrame.column,
                                 sticky=N+S)
        self.selectedRoundNumber.setSmallFont()
        self.selectedRoundNumber.justification(CENTER)
        self.selectedRoundNumber.anEntry.disableSearch()

        self.fieldsFrame.resetColumn()
        self.fieldsFrame.addRow()
        self.fieldsFrame.stretchCurrentRow()
        self.roundScoreCardFrame = AppBorderFrame(self.fieldsFrame, 1)
        self.roundScoreCardFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column,
                                      columnspan = self.fieldsFrame.columnTotal, sticky=E+W+S+N)
        self.roundScoreCardFrame.addTitle("Round Scorecard")
        self.roundScoreCardFrame.stretchCurrentRow()

        self.roundDetailsTable = AppRoundDetailsTable(self.roundScoreCardFrame, self.myMsgBar)
        self.roundDetailsTable.grid(row=self.roundScoreCardFrame.row, column = self.roundScoreCardFrame.column,
                                  sticky=E + N + W + S)
        ########################################################### Footer Section
        self.fieldsFrame.addRow()
        self.lastUpdate = AppLastUpdateFormat(self.fieldsFrame)
        self.lastUpdate.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, columnspan=colTotal,
                             sticky = E + N + S)

        self.setRequiredFields()
        self.fieldsDisable()

    def _golferEntered(self, *args):
        #
        #  Special handling:
        #       If you wish to change the golfer, title is adjusted and
        #       openning golferID adjusted to the new golfer.
        #
        if len(self.selectedGolfer.get()) > 0:
            if self.openningMode == 'add':
                self.changeFormTitle("Adding a Round for {0}".format(self.selectedGolfer.get()))
                self.openningGolferID = self.golferCBData.getID(self.selectedGolfer.get())


    def _roundNumberEntered(self, *args):
        if self._verifyHeaderData() == True:
            self._activateScorecard()
            self.roundDetailsTable.holeScore[0].focus()
        pass

    def _verifyHeaderData(self, *args):
        allDataItemsEntered = True
        if len(self.selectedCourse.get()) == 0:
            allDataItemsEntered = False
            self.selectedCourse.focus()
        elif len(self.selectedGolfer.get()) == 0:
            allDataItemsEntered = False
            self.selectedGolfer.focus()
        elif len(self.selectedGameDate.get()) == 0:
            allDataItemsEntered = False
            self.selectedTee.focus()
        elif len(self.selectedHolesPlayed.get()) == 0:
            allDataItemsEntered = False
            self.selectedTee.focus()
        elif len(self.selectedRoundNumber.get()) == 0:
            allDataItemsEntered = False
            self.selectedTee.focus()

        if allDataItemsEntered == True:
            gameListToday = CRUDGames.get_a_roundNum_today_for_golfer(
                self.golferCBData.getID(self.selectedGolfer.get()),
                convertDateStringToOrdinal(todayDate), int(self.selectedRoundNumber.get()),
                False)
            if len(gameListToday) > 0 and self.openningMode != 'edit':
                aMsg = "ERROR: Round number is invalid. It has already been used today"
                self.myMsgBar.newMessage('error', aMsg)
                return False
            else:
                return True
        else:
            self.myMsgBar.requiredFieldMessage()
            return False

    def _loadTeeList(self, aCourseID, noClose):
            self.selectedTee.clear()
            self.teeCBData = AppCBList(CRUDTees.getTeesDictionaryInfo(aCourseID, noClose))
            aList = self.teeCBData.getList()
            self.selectedTee.updateValuesList(aList)

    def _verifyCourseEntry(self, *args):
        if len(self.selectedCourse.get()) > 0:
            try:
                courseID = self.courseCBData.getID(self.selectedCourse.get())
            except:
                return
            self.selectedTee.clear()
            self.teeCBData = AppCBList(CRUDTees.getTeesDictionaryInfo(courseID))
            aList = self.teeCBData.getList()
            self.selectedTee.updateValuesList(aList)
            if self.previouslySelectedCourse == False:
                self.previouslySelectedCourse = True
                if len(self.selectedGolfer.get()) > 0:
                    gameListToday = CRUDGames.get_games_played_today_for_golfer(self.golferCBData.getID(self.selectedGolfer.get()),
                                                                                convertDateStringToOrdinal(todayDate),
                                                                                False)
                    self.selectedRoundNumber.load(len(gameListToday)+1)
                else:
                    self.selectedRoundNumber.load('1')
                self.selectedHolesPlayed.load('All')

            self.selectedRoundNumber.enable()
            self.selectedHolesPlayed.enable()
            self.selectedCourse.enable()
            self.selectedTee.enable()
            self.selectedTee.focus()

    def fieldsDisable(self):
        self.selectedGolfer.disable()
        self.selectedGameDate.disable()
        self.selectedTee.disable()
        self.selectedCourse.disable()
        self.selectedHolesPlayed.disable()
        self.selectedRoundNumber.disable()
        self.roundDetailsTable.disable()

    def fieldsEnable(self):
        if self.openningMode == 'add':
            self.selectedGolfer.enable()
            self.selectedGameDate.enable()
            self.selectedCourse.enable()

        # if self.enableRoundCanvas == True:
        #     self.roundCanvas.enable()

    def fieldsClear(self):
        pass

    def setRequiredFields(self):
        # Table specific.  Will be defined in calling class
        self.selectedGolfer.setAsRequiredField()
        self.selectedGameDate.setAsRequiredField()
        self.selectedCourse.setAsRequiredField()
        self.selectedTee.setAsRequiredField()
        self.selectedHolesPlayed.setAsRequiredField()
        self.selectedRoundNumber.setAsRequiredField()

    def resetRequiredFields(self):
        # Table specific.  Will be defined in calling class
        self.selectedGolfer.resetAsRequiredField()
        self.selectedGameDate.resetAsRequiredField()
        self.selectedCourse.resetAsRequiredField()
        self.selectedTee.resetAsRequiredField()
        self.selectedHolesPlayed.resetAsRequiredField()
        self.selectedRoundNumber.resetAsRequiredField()

    def validateRequiredFields(self):
        requiredFieldsEntered = True

        if len(self.selectedGolfer.get()) == 0:
           requiredFieldsEntered = False
           self.selectedGolfer.focus()

        elif len(self.selectedGameDate.get()) == 0:
           requiredFieldsEntered = False
           self.selectedGameDate.focus()

        elif len(self.selectedCourse.get()) == 0:
           requiredFieldsEntered = False
           self.selectedCourse.focus()

        elif len(self.selectedTee.get()) == 0:
           requiredFieldsEntered = False
           self.selectedTee.focus()

        elif len(self.selectedHolesPlayed.get()) == 0:
           requiredFieldsEntered = False
           self.selectedHolesPlayed.focus()

        elif len(self.selectedRoundNumber.get()) == 0:
           requiredFieldsEntered = False
           self.selectedRoundNumber.focus()

        if requiredFieldsEntered == False:
            self.myMsgBar.requiredFieldMessage()
        else:
            requiredFieldsEntered = self.roundDetailsTable.validateRequiredFields()
            if requiredFieldsEntered == False:
                self.myMsgBar.roundDetailsIncompleteMessage()
        return requiredFieldsEntered


    def save(self):
        if self.openningMode == 'add' or self.openningMode == 'addFromTee' or self.openningMode == 'addFromImport':
            #
            #  1. Verify game day and game number not already used.
            #  2. DONE, open DB at start and close at the end
            #  3. DONE, build a get the data from the table.
            #
            #
            CRUD.initDB(getDBLocationFullName())
            golferID = self.golferCBData.getID(self.selectedGolfer.get())
            gameDateOrdinal = convertDateStringToOrdinal(self.selectedGameDate.get())
            roundNumber = int(self.selectedRoundNumber.get())

            ans = CRUDGames.findDuplicateRoundNumbertoday(golferID, gameDateOrdinal, roundNumber, True)

            if ans == False:
                teeID = self.teeCBData.getID(self.selectedTee.get())
                gameStats = self.roundDetailsTable.getGameStats()
                gameRating = CRUDTees.get_tee_rating(teeID, True)
                gameSlope = CRUDTees.get_tee_slope(teeID, True)

                if self.selectedHolesPlayed.get() == 'All':
                    curGameHdcpDiff = calculateHdcpDiff(int(gameStats[2]), float(gameRating),
                                                        int(gameSlope))
                else:
                    halfRating = float(gameRating) / float(2)
                    curGameHdcpDiff = calculateHdcpDiff(int(gameStats[2]), halfRating, int(gameSlope))

                ID = CRUDGames.insert_games(
                    gameDateOrdinal, roundNumber, teeID, golferID,
                    gameStats[0], gameStats[1], gameStats[2],
                    curGameHdcpDiff,gameStats[3], gameStats[4], gameStats[5],
                    gameStats[6], gameStats[7], gameStats[8], gameStats[9], gameStats[10],
                    gameStats[11], gameStats[12], gameStats[13], gameStats[14],
                    gameStats[15], gameStats[16], gameStats[17],
                    gameRating, gameSlope, self.selectedHolesPlayed.get(),
                    gameStats[18], gameStats[19], gameStats[20],
                    self.courseCBData.getID(self.selectedCourse.get()), self.updaterID, True
                )
                #
                #   Even if played only 9 holes, 18 holes are saved
                #
                for i in range(18):
                    holeDetails = self.roundDetailsTable.getHoleDetails(i)
                    CRUDGames.insert_game_details(
                        i+1, holeDetails[0],holeDetails[1],holeDetails[2],
                        holeDetails[3], ID,
                        self.updaterID, True
                    )


                indexBefore = CRUDGames.calculateBeforeCurrentGameHdcp(gameDateOrdinal, golferID, self.openningGameID,
                                                                       roundNumber, True)
                indexAfter = CRUDGames.calculateAfterCurrentGameHdcp(gameDateOrdinal, golferID, self.openningGameID,
                                                                     roundNumber, True)

                CRUD.closeDB()
                # CRUD.closeDB()
                aMsg = 'Round #{0} on {1} has been added for {2}.'.format(self.selectedRoundNumber.get(),
                                                                         self.selectedGameDate.get(),
                                                                         self.selectedGolfer.get())
                self.myMsgBar.newMessage('info', aMsg)
                if self.anUpdateUponReturnMethod != None:
                    self.anUpdateUponReturnMethod()

                data = []
                data.append(ID)  # Adding game ID
                data.append(gameStats)
                data.append(self.selectedHolesPlayed.get())
                data.append(indexBefore)
                data.append(indexAfter)

                # gameResultWindow(self, "Result for", data)
                gameResultWindow(self, "Result for", data, None, True)

                if self.openningMode == 'addFromTee':
                    self.selectedGolfer.clear()
                    self.roundDetailsTable.clear()
                    self.roundDetailsTable.disable()
                    self.addFromTeeCommand()
                elif self.openningMode == 'addFromImport':
                    self.dirty = False
                    self._exitForm()
                else:
                    self.add_additional()

                # if ans.result == True:
                #     self._exitForm()

            else:
                CRUD.closeDB()
                aMsg = "ERROR: Round {0} on {1} already exist for Golfer {2}.".format(roundNumber,
                                                                                      self.selectedGameDate.get(),
                                                                                      self.selectedGolfer.get())
                AppDisplayErrorMessage(self, "Duplicate Game Error", aMsg)
                return False

            # gameStats.append(self.frontScoreTotal)        # Indice 0
            # gameStats.append(self.backScoreTotal)         # Indice 1
            # gameStats.append(self.scoreTotal)             # Indice 2
            # gameStats.append(scoringStats[0])  # D Eagle    Indice 3
            # gameStats.append(scoringStats[1])  # Eagle      Indice 4
            # gameStats.append(scoringStats[2])  # Birdie     Indice 5
            # gameStats.append(scoringStats[3])  # Par        Indice 6
            # gameStats.append(scoringStats[4])  # Bogie      Indice 7
            # gameStats.append(scoringStats[5])  # D Bogie    Indice 8
            # gameStats.append(scoringStats[6])  # T Bogie    Indice 9
            # gameStats.append(scoringStats[7])  # Others     Indice 10
            # gameStats.append(self.GreensTotal)            # Indice 11
            # gameStats.append(self.FairwaysTotal)          # Indice 12
            # gameStats.append(drivingStats[0])  # left Miss  Indice 13
            # gameStats.append(drivingStats[1])  # Right Miss Indice 14
            # gameStats.append(drivingStats[2])  # Drv Yards  Indice 15
            # gameStats.append(self.sandTrapTotal)          # Indice 16
            # gameStats.append(self.SandSavesTotal)         # Indice 17
            # gameStats.append(self.puttsTotal)             # Indice 18
            # gameStats.append(self.putts3AndUp)            # Indice 19
            # gameStats.append(self.yardageTotal)           # Indice 20

        elif self.openningMode == 'edit':
            #
            #  1. Verify game day and game number not already used.
            #  2. DONE, open DB at start and close at the end
            #  3. DONE, build a get the data from the table.
            #
            #
            CRUD.initDB(getDBLocationFullName())
            golferID = self.golferCBData.getID(self.selectedGolfer.get())
            gameDateOrdinal = convertDateStringToOrdinal(self.selectedGameDate.get())
            roundNumber = int(self.selectedRoundNumber.get())

            ans = CRUDGames.findDuplicateRoundNumbertodayFromEdit(golferID, gameDateOrdinal, roundNumber, self.openningGameID, True)

            if ans == False:
                teeID = self.teeCBData.getID(self.selectedTee.get())
                gameStats = self.roundDetailsTable.getGameStats()
                gameRating = CRUDTees.get_tee_rating(teeID, True)
                gameSlope = CRUDTees.get_tee_slope(teeID, True)

                if self.selectedHolesPlayed.get() == 'All':
                    curGameHdcpDiff = calculateHdcpDiff(int(gameStats[2]), float(gameRating),
                                                        int(gameSlope))
                else:
                    halfRating = float(gameRating) / float(2)
                    curGameHdcpDiff = calculateHdcpDiff(int(gameStats[2]), halfRating, int(gameSlope))

                gameDetailsIDList = CRUDGames.get_gameDetailsID_for_game(self.openningGameID, True)

                # gameStats.append(self.frontScoreTotal)        # Indice 0
                # gameStgolferIDats.append(self.backScoreTotal)         # Indice 1
                # gameStats.append(self.scoreTotal)             # Indice 2
                # gameStats.append(scoringStats[0])  # D Eagle    Indice 3
                # gameStats.append(scoringStats[1])  # Eagle      Indice 4
                # gameStats.append(scoringStats[2])  # Birdie     Indice 5
                # gameStats.append(scoringStats[3])  # Par        Indice 6
                # gameStats.append(scoringStats[4])  # Bogie      Indice 7
                # gameStats.append(scoringStats[5])  # D Bogie    Indice 8
                # gameStats.append(scoringStats[6])  # T Bogie    Indice 9
                # gameStats.append(scoringStats[7])  # Others     Indice 10
                # gameStats.append(self.GreensTotal)            # Indice 11
                # gameStroundNumberats.append(self.FairwaysTotal)          # Indice 12
                # gameStats.append(drivingStats[0])  # left Miss  Indice 13
                # gameStats.append(drivingStats[1])  # Right Miss Indice 14
                # gameStats.append(drivingStats[2])  # Drv Yards  Indice 15
                # gameStats.append(sself._exitForm()elf.sandTrapTotal)          # Indice 16
                # gameStats.append(self.SandSavesTotal)         # Indice 17
                # gameStats.append(self.puttsTotal)             # Indice 18
                # gameStats.append(self.putts3AndUp)            # Indice 19
                # gameStats.append(self.yardageTotal)           # Indice 20
                CRUDGames.update_game(self.openningGameID,
                                      gameDateOrdinal, roundNumber, teeID, golferID,
                                      gameStats[0], gameStats[1], gameStats[2],
                                      curGameHdcpDiff,gameStats[3], gameStats[4], gameStats[5],
                                      gameStats[6], gameStats[7], gameStats[8], gameStats[9], gameStats[10],
                                      gameStats[11], gameStats[12], gameStats[13], gameStats[14],
                                      gameStats[15], gameStats[16], gameStats[17],
                                      gameRating, gameSlope, self.selectedHolesPlayed.get(),
                                      gameStats[18], gameStats[19], gameStats[20],
                                      self.courseCBData.getID(self.selectedCourse.get()), self.updaterID, True
                                     )

                for i in range(18):
                    holeDetails = self.roundDetailsTable.getHoleDetails(i)
                    CRUDGames.update_game_details_for_gameDetailsID(gameDetailsIDList[i][0], gameDetailsIDList[i][1],
                                                                    holeDetails[0], holeDetails[1], holeDetails[2],
                                                                    holeDetails[3],self.updaterID, True
                                                                    )
                # aMsg = 'Round #{0} on {1} has been updated for {2}.'.format(self.selectedRoundNumber.get(),
                #                                                          self.selectedGameDate.get(),
                #                                                          self.selectedGolfer.get())
                # ans = AppDisplayMessage(self, "Game Editing Results", aMsg)

                indexBefore = CRUDGames.calculateBeforeCurrentGameHdcp(gameDateOrdinal,golferID,self.openningGameID,
                                                                       roundNumber, True)
                indexAfter = CRUDGames.calculateAfterCurrentGameHdcp(gameDateOrdinal, golferID, self.openningGameID,
                                                                       roundNumber, True)
                CRUD.closeDB()
                data = []
                data.append(self.openningGameID)  # Adding game ID
                data.append(gameStats)
                data.append(self.selectedHolesPlayed.get())
                data.append(indexBefore)
                data.append(indexAfter)
                self.withdrawCommand()

                ans = gameResultWindow(self, "Result for", data, None, True)

                if self.anUpdateUponReturnMethod != None:
                    self.anUpdateUponReturnMethod()

                self.dirty = False
                if ans.result == True:
                    self._exitForm()

            else:
                CRUD.closeDB()
                aMsg = "ERROR: Round {0} on {1} already exist for Golfer {2}.".format(roundNumber,
                                                                                      self.selectedGameDate.get(),
                                                                                      self.selectedGolfer.get())
                AppDisplayErrorMessage(self, "Duplicate Game Error", aMsg)
                return False
