import sqlite3 as sql
import os
import csv
import AppCRUD as AppCRUD
import datetime
from dateutil import parser
from AppProc import getDBLocationFullName, convertDateStringToOrdinal, convertOrdinaltoString
from AppConstants import *

db=None
cursor = None
lidGolfers = 0

##### CRUD functions for items ######
#'''CREATE TABLE Golfers(
#     0         uniqueID             INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
#     1         golferFNAME          CHAR(30)    NOT NULL,
#     2         golferLNAME          CHAR(30)    NOT NULL,
#     3         golferPIC_LOC        CHAR(100),
#     4         golferBirthday       INTEGER,
#     5         deleted              CHAR(1),
#               lastModified         INTEGER,
#               updaterID            INTEGER

def insertGolfers(golferFNAME, golferLNAME, golferPIC_LOC, golferBirthday, updaterID, Noclose=False):

    if Noclose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = ''' 
                insert into Golfers (golferFNAME, golferLNAME, golferPIC_LOC, golferBirthday,
                                     deleted, lastModified, updaterID)
                values (?,?,?,?,?,?,?)
            '''

    AppCRUD.cursor.execute(query,(golferFNAME,golferLNAME,golferPIC_LOC, golferBirthday,
                                  'N', convertDateStringToOrdinal(todayDate), updaterID))

    ID = getGolferID(golferFNAME,golferLNAME, golferBirthday, convertDateStringToOrdinal(todayDate), updaterID, Noclose)

    if Noclose == False:
        AppCRUD.closeDB()

    return ID

def getGolferID(golferFNAME,golferLNAME, golferBirthday,
                lastModified, updaterID, Noclose=False):
    if Noclose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                select uniqueID from Golfers where golferFNAME="{0}" and golferLNAME="{1}" and golferBirthday={2} and
                                                   lastModified={3} and updaterID={4}
            '''.format(golferFNAME,golferLNAME, golferBirthday,lastModified, updaterID)
    ans = AppCRUD.cursor.execute(query).fetchall()

    if Noclose == False:
        AppCRUD.closeDB()

    if len(ans) > 0:
        return ans[0][0]
    else:
        return None

def getGolfers():
    AppCRUD.initDB(getDBLocationFullName())
    query = '''  
    select * from Golfers ORDER BY golferLNAME'''
    return AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()

def getLoginInfo(loginName):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''
    select password, access_Level, uniqueID from Golfers where loginName = "{0}" '''.format(loginName)
    return AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()


def getGolfersDictionaryInfo(NonDeletedOnly=False):
    #
    #
    #
    AppCRUD.initDB(getDBLocationFullName())
    if NonDeletedOnly == True:
        query = '''
                        select uniqueID, golferFNAME, golferLNAME from Golfers
                               where deleted != 'Y'
                               ORDER by golferLNAME
                '''
    else:
        query = '''
                        select uniqueID, golferFNAME, golferLNAME from Golfers
                               ORDER by golferLNAME
                '''

    temp = AppCRUD.cursor.execute(query).fetchall()
    returnList = []
    for i in range(len(temp)):
        newItem = (temp[i][0], "{0} {1}".format(temp[i][1], temp[i][2]))
        returnList.append(newItem)
    AppCRUD.closeDB()
    return returnList


def getGolferCB():
    AppCRUD.initDB(getDBLocationFullName())
    query = '''
    select uniqueID, golferFNAME, golferLNAME
    from Golfers ORDER BY golferLNAME'''
    return AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()

def getGolferSpecial():
    AppCRUD.initDB(getDBLocationFullName())
    query = '''
    select golferFNAME, golferLNAME
    from Golfers ORDER BY golferLNAME'''
    return AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()

def get_col_names(tableName):
    """Retrieve column names for given table in database."""
    AppCRUD.initDB(getDBLocationFullName())
    AppCRUD.cursor.execute("PRAGMA table_info('{}')".format(tableName))
    # Currently only uses one value from returned tuple.
    # TODO: Add logic to utilize type, not null and PK fields.
    col_names = [x[1] for x in AppCRUD.cursor.fetchall()]
    return col_names
    AppCRUD.closeDB()

def get_golfer_details(uniqueID):
    AppCRUD.initDB(getDBLocationFullName())
    query = ''' 
    select *
    from Golfers
    where uniqueID = ?'''
    return AppCRUD.cursor.execute(query,(uniqueID,)).fetchall()[0]
    AppCRUD.closeDB()
    
def getGolferFullname(uniqueID):
    AppCRUD.initDB(getDBLocationFullName())
    query = ''' 
    select golferFNAME, golferLNAME
    from Golfers
    where uniqueID = ?'''
    temp = AppCRUD.cursor.execute(query,(uniqueID,)).fetchall()
    AppCRUD.closeDB()
    return "{0} {1}".format(temp[0][0], temp[0][1])

def get_golfer_name(uniqueID):
    return get_golfer_details(uniqueID)[0]
    
def update_Golfer(uniqueID, FName, LName,  golferPIC_LOC, golferBirthday,
                  updaterID, Noclose=False):
    if Noclose==False:
        AppCRUD.initDB(getDBLocationFullName())

    query = ''' 
                update Golfers
                set golferFNAME=?, golferLNAME=?, golferPIC_LOC=?, golferBirthday=?,
                    lastModified=?, updaterID=?
                where uniqueID=?
            '''

    AppCRUD.cursor.execute(query, (FName,LName,golferPIC_LOC,golferBirthday,
                                   convertDateStringToOrdinal(todayDate),updaterID,uniqueID))

    if Noclose == False:
        AppCRUD.closeDB()

def findGolfers(FName, LName, Noclose=False):
    if Noclose == False:
        AppCRUD.initDB(getDBLocationFullName())

    if len(FName) == 0:
        FNameModified = "%%"
    else:
        FNameModified = FName

    if len(LName) == 0:
        LNameModified = "%%"
    else:
        LNameModified = LName

    query = '''
                select * from Golfers where golferFNAME like "{0}" and golferLNAME like "{1}"
                ORDER BY golferLNAME
            '''.format(FNameModified, LNameModified)
    ans = AppCRUD.cursor.execute(query).fetchall()

    if Noclose == False:
        AppCRUD.closeDB()

    return ans

def findGolferIDClosestFromFName(fName, NoClose = False):
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    found = False
    searchedName = fName
    while found == False:
        query = '''
                    select uniqueID from Golfers where golferFNAME like "{0}%" COLLATE NOCASE
                                                       and deleted ='N'
                '''.format(searchedName)
        temp = AppCRUD.cursor.execute(query).fetchall()
        if len(temp) > 0:
            found = True
        else: # Remove last character to find closest match
            sizeOfSearchedName = len(searchedName)
            if sizeOfSearchedName < 1:
                break
            else:
                searchedName = searchedName[:(sizeOfSearchedName-1)]

    if NoClose == False:
        AppCRUD.closeDB()

    if len(temp) > 0:
        return temp[0][0]
    else:
        return None

def findIfEditCreatesGolferDuplicate(CurrentID, FName, LName, Birthday, Noclose=False):
    if Noclose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                select uniqueID from Golfers where golferFNAME="{0}" and  golferLNAME="{1}" and
                                                   golferBirthday="{2}" and uniqueID != {3}
            '''.format(FName, LName, Birthday, CurrentID)

    ans = AppCRUD.cursor.execute(query).fetchall()

    if Noclose == False:
        AppCRUD.closeDB()

    if len(ans) > 0:
        return True
    else:
        return False

def findGolferDuplicate(FName, LName, Birthday, Noclose=False):
    if Noclose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                select uniqueID from Golfers where golferFNAME="{0}" and  golferLNAME="{1}" and
                                                   golferBirthday="{2}"
            '''.format(FName, LName, Birthday)

    ans = AppCRUD.cursor.execute(query).fetchall()
    if Noclose == False:
        AppCRUD.closeDB()
    if len(ans) > 0:
        return True
    else:
        return False

def delete_golfer(uniqueID):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''
    delete from Golfers
    where uniqueID = ?'''
    AppCRUD.cursor.execute(query,(uniqueID,))
    AppCRUD.closeDB()

def exportCSV_golfers(fileName):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''select * from Golfers'''
    data = AppCRUD.cursor.execute(query).fetchall()
    file = AppDefaultLocation + '/' + BackupDirDefaultName +'/'+ fileName
    with open(file, 'w', newline='', encoding='ISO-8859-1') as fout:
        for i in range(len(data)):
            csv.writer(fout).writerow(data[i])
    AppCRUD.closeDB()
    return len(data)

def importCSV_golfers(fileName):
    AppCRUD.initDB(getDBLocationFullName())
    file = AppDefaultLocation + '/' + BackupDirDefaultName + '/' + fileName
    with open(file, 'r', encoding='ISO-8859-1') as fin:
        reader = csv.reader(fin)
        for row in reader:
            query = '''
            insert into Golfers (uniqueID,golferFNAME,golferLNAME,golferPIC_LOC,golferBirthday,deleted,lastModified,updaterID)
            values (?, ?, ?, ?, ?, ?, ?, ?)'''
            AppCRUD.cursor.execute(query, (row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7]))
    AppCRUD.closeDB()
    return len(getGolfers())

def importGolfersCSV_MyGolf2015(fileName):
    AppCRUD.initDB(getDBLocationFullName())
    with open(fileName, newline='', encoding='ISO-8859-1') as fin:
        reader = csv.reader(fin)
        for row in reader:
            try:
                separated = row[3].split("/")
                filename = separated[len(separated)-1]
            except:
                filename = " "
            query = '''
               insert into Golfers (uniqueID,golferFNAME,golferLNAME,golferPIC_LOC,
                                    golferBirthday,deleted,lastModified,updaterID)
                           values (?, ?, ?, ?, ?, ?, ?, ?)'''
            AppCRUD.cursor.execute(query, (row[0], row[1], row[2], filename, row[4], 'N', convertDateStringToOrdinal(todayDate), '1'))
    AppCRUD.closeDB()
    return len(getGolfers())
##### CRUD functions for items ######
#'''CREATE TABLE Golfers(
#     0         uniqueID INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
#     1         golferFNAME          CHAR(30)    NOT NULL,
#     2         golferLNAME          CHAR(30)    NOT NULL,
#     3         golferPIC_LOC        CHAR(100),
#     4         golferBirthday       INTEGER,
#               lastModified         INTEGER,
#               updaterID            INTEGER
