##########################################################
#
#   Departments Table
#
#   This class implements the specific manipulation of the
# data for the ECE Departments Table that the TECH Shop requires.
#

import tkinter.tix as Tix
import tkinter.ttk as ttk
from MyGolfApp2016 import AppCRUD_FROMECECRUD as AppDB
import tkinter.filedialog as Fd
import tkinter.messagebox as Mb
import textwrap, re

from tkinter.constants import *

from MyGolfApp2016.AppMyClasses import *
from MyGolfApp2016.AppConstants import *

from MyGolfApp2016.AppProcFROMECE import *

from PIL import Image

class partsWindow(tableCommonWindowClass):
    def __init__(self, aWindow, aCloseCommand, loginData):
        self.windowTitle = 'Parts'
        tableCommonWindowClass.__init__(self, aWindow, aCloseCommand, self.windowTitle, loginData[1])
        self.loginData = loginData
        self.accessLevel = self.loginData[1]
        self.updaterID = self.loginData[2]
        rankFullName=AppDB.getRankFullName(self.updaterID)
        self.displayCurUser.setUserName(rankFullName)
#######################################################################################
# Inherited all from tableCommanWindowClass
#  
#  This class intents to change the mainArea only.  Everything else must remain common
#       
        self.mainAreaRowCount = 0
        self.mainAreaColumnCount = 0       
        self._getNewRecList()
        self._reDisplayRecordFrame()
        self.mainArea.rowconfigure(self.mainAreaRowCount, weight=1)
        
############################################################ Area to add window specific commands        
#        self.closeButton = ttk.Button(self.commandFrame, text='Test', style='CommandButton.TButton', 
#                                 command= lambda: self.sampleCommand())
#        self.closeButton.grid(row=0, column=0, sticky=N+S)
        

################################################################# Redefined commands
#
#  invoke from the tableCommonWindows.  These commands are specific to each tables
#  The specific actions are redefinded here.
#       
#    def displayRec(self, *args):
#        print("Displaying a Record: ", self.curRecNum)
#        
#    def save(self):
#        #
#        #  Table specific.  Return a True if succesful saved.
#        #
#        print("Saving a record (re-defined)")
#        return True

    def _departmentEntered(self, *args):
        pass
#        self.myMsgBar.clearMessage()
#        print("Detected")
#        if anEvent.char=='\t':
#            print("I am here")
#            focusNext(anEvent.widget)
#            ans = self.selectedDepartment.findInList()
#            if ans == -1:
#                if len(self.selectedDepartment.get()) == 0:
#                    aMsg = 'ERROR: Department is a required field, please select from list.'
#                else:
#                    aMsg = 'ERROR: Invalid Department, please select from list.'
#                self.myMsgBar.newMessage('error', aMsg)
#                self.selectedDepartment.focus()
#            else:
 #               print("I am here")
 #               self.selectedBasic.focus()
        self.selectedBasic.focus()

    def _enableWindowCommand(self):
        #
        #  Any command specific must added here 
        #
        pass
#        self.closeButton.config(state=NORMAL)

    def _disableWindowCommand(self):
        #
        #  Any command specific must added here 
        #
        pass
#        self.closeButton.config(state=DISABLED)
               
    def sampleCommand(self):
        self.myMsgBar.clearMessage()
        aMsg = 'INFO: Command area, specific to current window (Parts)'
        self.myMsgBar.newMessage('info', aMsg)
        AppDB.importCSV_Parts(importTUTPartsFilename)
        
    def _supplierNameEntered(self, *args):
        self.myMsgBar.clearMessage()
        ans = self.selectedSupplierCB.findInList()
        if ans == -1:
            aMsg = 'ERROR: Invalid Supplier, please select from list.'
            self.myMsgBar.newMessage('error', aMsg)
            self.selectedSupplierCB.focus()
        else:
            self.selectedRow.focus()
            
    def _getNewRecList(self):
        self.recList = AppDB.getParts()
        self.curRecNumV.set(self.curRecNumV.get()) # forces display of record number
        
    def _reDisplayRecordFrame(self):
        self.recordFrame.destroy()
        self._getNewRecList()
        self._displayRecordFrame()
        if self.curOp == 'add':
            self.fieldsEnable()
        
    def _displayRecordFrame(self):
        self.recordFrame = AppFrame(self.mainArea, 1) 
        self.recordFrame.grid(row=self.mainAreaRowCount, column=self.mainAreaColumnCount, columnspan= self.mainAreaColumnTotal, sticky=N+S+E+W)
        
        if len(self.recList) > 0:
            self._buildWindowsFields(self.recordFrame)
            self.recordFrame.rowconfigure(self.recordFrame.row, weight=0)
            if self.curOp != 'add':        
                self.displayRec()
                self.navigationEnable(self.accessLevel)
            new_order = (
                                    self.selectedDepartment,
                                    self.selectedBasic,
                                    self.selectedValue,
                                    self.selectedMultiplier, 
                                    self.selectedTolerance, 
                                    self.selectedSize, 
                                    self.selectedFunction,
                                    self.selectedNomenclature,
                                    self.selectedDescription,
                                    self.selectedDescription2,
                                    self.selectedStock,      
                                    self.selectedMin,     
                                    self.selectedOnOrder,      
                                    self.selectedNeeded,    
                                    self.selectedToday,
                                    self.selectecNSN1,
                                    self.selectecNSN2,
                                    self.selectecNSN3,
                                    self.selectecNSN4,
                                    self.selectecSNC,
                                    self.selectecSSI,
                                    self.selectecCatNum,
                                    self.selectedClass,
                                    self.selectedUI,
                                    self.selectedPrice, 
#                                    self.selectecSupplier,
                                    self.selectedSupplierCB,
#                                    self.selectecSupplierNum,
                                    self.selectedRow,
                                    self.selectedSide,
                                    self.selectedShelf,
                                    self.selectedSection
                            )
            
            for widget in new_order:
                widget.lift()
        else:
            wMsg = Tix.Label(self.recordFrame, text='No Records Found in Current List.', font=fontMonstrousB)
            wMsg.grid(row=0, column=0, columnspan=self.recordFrame.columnTotal)
            
    def _buildWindowsFields(self, aFrame):
        ######################################################
        # ECE Parts CRUD
        #  '''CREATE TABLE Parts(
        #        0        partID                  INTEGER PRIMARY KEY AUTOINCREMENT  NOT NULL,
        #        1        BASIC      CHAR(20),
        #        2        VALUE      CHAR(3),
        #        3        VALUED     CHAR(3),
        #        4        MULT       CHAR(1),
        #        5        TOLE       CHAR(5),
        #        6        SIZE       CHAR(7),
        #        7        DESCR      CHAR(40),
        #        8        NSN1       CHAR(4),
        #        9        NSN2       CHAR(2),
        #        10       NSN3       CHAR(3),
        #        11       NSN4       CHAR(4),
        #        12       ORDER_NUM  CHAR(20),
        #        13       SUPP_NUM   CHAR(10),     NOT IN USE ANYMORE
        #        14       ITEM_NUM   CHAR(12),
        #        15       UI         CHAR(2),
        #        16       SNC        CHAR(1),
        #        17       CLASS      CHAR(1),
        #        18       PRICE      CHAR(5),
        #        19       PRICED     CHAR(2),
        #        20       SUP        CHAR(1),
        #        21       SSI        CHAR(2),
        #        22       RETAIL     CHAR(9),
        #        23       QUANT      CHAR(4),
        #        24       QMIN       CHAR(4),
        #        25       QORDER     CHAR(4),
        #        26       QUSE       CHAR(4),
        #        27       QTEMP      CHAR(4),
        #        28       FUNCTION   CHAR(20),
        #        29       DESCR2     CHAR(60),
        #        30       NOMEN      CHAR(15),
        #        31       ROW        CHAR(2)
        #        32       SIDE       CHAR(1)
        #        33       SHELF      CHAR(1)
        #        34       SECTION    CHAR(3)
        #        35       DEPARTMENT CHAR(2),
        #        36       S_NAME     CHAR(30),     NOT IN USE ANYMORE
        #        37       lastModified  INTEGER,
        #        38       updaterID     INTEGER,
        #        39       suppliersID   INTEGER,
        #        40       departmentsID   INTEGER
        #
#        headerList1 = ('BASIC','VALUE','VALUED','MULT','TOLE','SIZE','DESCR','NSN1','NSN2','NSN3','NSN4')
        colTotal = 2
        self.fieldsFrame = AppFrame(aFrame, colTotal)
        self.fieldsFrame.grid(row=0, column=0, sticky=N+S+E+W)

        # Empty row for separation
#        empty = AppSpaceRow(self.fieldsFrame)
#        empty.grid(row=self.fieldsFrame.row, column=0, sticky=N+S)

#        departmentV = Tix.StringVar()
#        departmentV.trace('w',self._departmentEntered)
#        departmentList = ['BX']
#        self.selectedDepartment = AppHorizontalCB(self.fieldsFrame, 'Department', departmentV, departmentList, partsDepartmentLenght)
#        self.selectedDepartment.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, columnspan=colTotal, sticky=N+S)
#        self.selectedDepartment.disable()

#        self.fieldsFrame.row = self.fieldsFrame.row + 1
#        self.fieldsFrame.rowconfigure(self.fieldsFrame.row, weight = 1)
#        self.fieldsFrame.column = 0

#        departmentV = Tix.StringVar()
#        departmentV.trace('w', self._departmentEntered)
        self.departmentsCBData = AppCBList(AppDB.getDepartmentsDictionaryInfo())
#        self.selectedDepartment = AppHorizontalCB3(self.fieldsFrame, 'Department', departmentV, self.departmentsCBData.getList(), partsDepartmentLenght)
        self.selectedDepartment = AppHorizontalCB_AC(self.fieldsFrame, 'Department', self.departmentsCBData.getList(), partsDepartmentLenght)
        self.selectedDepartment.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, columnspan=colTotal, sticky=N+S)
#        self.selectedDepartment.bind('<FocusOut>', lambda event: self._departmentEntered(event))
        self.selectedDepartment.disable()
 
        self.fieldsFrame.row = self.fieldsFrame.row + 1
        self.fieldsFrame.rowconfigure(self.fieldsFrame.row, weight = 1)
        self.fieldsFrame.column = 0
        
############################################# Basic Row 
        self.fieldsFrame.row = self.fieldsFrame.row + 1
        self.fieldsFrame.rowconfigure(self.fieldsFrame.row, weight = 1)
        self.fieldsFrame.column = 0
        
        self.basicFrame = AppBorderFrame(self.fieldsFrame, 5)
        self.basicFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, columnspan=colTotal, sticky=N+S+E+W)
        self.basicFrame.rowconfigure(self.basicFrame.row, weight=0)

        self.selectedBasic = AppVerticalFieldFrame(self.basicFrame, 'Basic Item Name', partsBasicLenght)
        self.selectedBasic.grid(row=self.basicFrame.row, column=self.basicFrame.column, sticky=N+S)

        self.basicFrame.column = self.basicFrame.column + 1
        self.selectedValue = AppVerticalFieldFrame(self.basicFrame, 'Value', partsValueLenght)
        self.selectedValue.grid(row=self.basicFrame.row, column=self.basicFrame.column, sticky=N+S)
        self.selectedValue.anEntry.bind('<Key>', self._selectedValueCheck)

        self.basicFrame.column = self.basicFrame.column + 1
        self.selectedMultiplier = AppVerticalFieldFrame(self.basicFrame, 'Multiplier', partsMultiplierLenght)
        self.selectedMultiplier.grid(row=self.basicFrame.row, column=self.basicFrame.column, sticky=N+S)

        self.basicFrame.column = self.basicFrame.column + 1
        self.selectedTolerance = AppVerticalFieldFrame(self.basicFrame, 'Tolerance', partsToleranceLenght)
        self.selectedTolerance.grid(row=self.basicFrame.row, column=self.basicFrame.column, sticky=N+S)

        self.basicFrame.column = self.basicFrame.column + 1
        self.selectedSize = AppVerticalFieldFrame(self.basicFrame, 'Size', partsSizeLenght)
        self.selectedSize.grid(row=self.basicFrame.row, column=self.basicFrame.column, sticky=N+S)
           
############################################# Func/Nom Row              
        self.fieldsFrame.row = self.fieldsFrame.row + 1
        self.fieldsFrame.column = 0
        self.fieldsFrame.rowconfigure(self.fieldsFrame.row, weight=0)
        
        self.funcNomFrame = AppFrame(self.fieldsFrame, 2)
        self.funcNomFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, columnspan=colTotal, sticky=N+S+E+W)
        self.funcNomFrame.rowconfigure(self.funcNomFrame.row, weight=0)

        self.selectedFunction = AppHorizontalFieldFrame(self.funcNomFrame, 'Functionality', partsFuncLenght)
        self.selectedFunction.grid(row=self.funcNomFrame.row, column=self.funcNomFrame.column, sticky=N+S)

        self.funcNomFrame.column = self.funcNomFrame.column + 1
        self.selectedNomenclature = AppHorizontalFieldFrame(self.funcNomFrame, 'Nomenclature', partsNomenLenght)
        self.selectedNomenclature.grid(row=self.funcNomFrame.row, column=self.funcNomFrame.column, sticky=N+S)

############################################## Description Rows              
        self.fieldsFrame.row = self.fieldsFrame.row + 1
        self.fieldsFrame.column = 0
        self.fieldsFrame.rowconfigure(self.fieldsFrame.row, weight=0)

        self.descFrame = AppFrame(self.fieldsFrame, 2)
        self.descFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, columnspan=colTotal, sticky=N+S+E+W)
        self.descFrame.rowconfigure(self.descFrame.row, weight=0)
        
        self.descFrame.rowconfigure(self.descFrame.row, weight=0)
        aLabel = ttk.Label(self.descFrame, text='Description: ', style='RegularFieldTitle.TLabel')
        aLabel.grid(row=self.descFrame.row, column=self.descFrame.column,sticky=E+N+S)
        
        self.descFrame.column = self.descFrame.column + 1       
        self.selectedDescription = AppEntry(self.descFrame, partsDescLenght)
        self.selectedDescription.grid(row=self.descFrame.row, column=self.descFrame.column, columnspan=colTotal, sticky=W+N+S)
        
        self.descFrame.row = self.descFrame.row + 1
        self.descFrame.rowconfigure(self.descFrame.row, weight=0)
        self.descFrame.column = 0
        
        self.descFrame.column = self.descFrame.column + 1       
        self.selectedDescription2 = AppEntry(self.descFrame, partsDescLenght)
        self.selectedDescription2.grid(row=self.descFrame.row, column=self.descFrame.column, columnspan=colTotal, sticky=W+N+S)
        
############################################# Qty Row                      
        
        self.fieldsFrame.row = self.fieldsFrame.row + 1
        self.fieldsFrame.rowconfigure(self.fieldsFrame.row, weight=0)
        self.fieldsFrame.column = 0

        self.qtyFrame = AppBorderFrame(self.fieldsFrame, 6)
        self.qtyFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, columnspan=colTotal, sticky=N+S+E+W)
        self.qtyFrame.rowconfigure(self.qtyFrame.row, weight=0)

        aLabel = ttk.Label(self.qtyFrame, text='Quantities:', style='RegularFieldTitle.TLabel')
        aLabel.grid(row=self.qtyFrame.row, column=self.qtyFrame.column, sticky=N)

        self.qtyFrame.column = self.qtyFrame.column + 1      
        self.selectedStock = AppVerticalFieldFrame(self.qtyFrame, 'Stock', partsQtyLenght)
        self.selectedStock.grid(row=self.qtyFrame.row, column=self.qtyFrame.column, sticky=N+S)
        
        self.qtyFrame.column = self.qtyFrame.column + 1      
        self.selectedMin = AppVerticalFieldFrame(self.qtyFrame, 'Minimum', partsQtyLenght)
        self.selectedMin.grid(row=self.qtyFrame.row, column=self.qtyFrame.column, sticky=N+S)
        
        self.qtyFrame.column = self.qtyFrame.column + 1      
        self.selectedOnOrder = AppVerticalFieldFrame(self.qtyFrame, 'On Order', partsQtyLenght)
        self.selectedOnOrder.grid(row=self.qtyFrame.row, column=self.qtyFrame.column, sticky=N+S)
        
        self.qtyFrame.column = self.qtyFrame.column + 1      
        self.selectedNeeded = AppVerticalFieldFrame(self.qtyFrame, 'Needed', partsQtyLenght)
        self.selectedNeeded.grid(row=self.qtyFrame.row, column=self.qtyFrame.column, sticky=N+S)
        
        self.qtyFrame.column = self.qtyFrame.column + 1      
        self.selectedToday = AppVerticalFieldFrame(self.qtyFrame, 'Today', partsQtyLenght)
        self.selectedToday.grid(row=self.qtyFrame.row, column=self.qtyFrame.column, sticky=N+S)
        
############################################# NATO Row                      
        
        self.fieldsFrame.row = self.fieldsFrame.row + 1
        self.fieldsFrame.rowconfigure(self.fieldsFrame.row, weight=0)
        self.fieldsFrame.column = 0

        self.natoFrame = AppFrame(self.fieldsFrame, 2)
        self.natoFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, columnspan=colTotal, sticky=N+S+E+W)
        self.natoFrame.rowconfigure(self.natoFrame.row, weight=0)
        
        self.natoSubFrame = AppFrame(self.natoFrame, 8)
        self.natoSubFrame.grid(row=self.natoFrame.row, rowspan = 4, column=self.natoFrame.column, sticky=N+S+E+W)
        self.natoSubFrame.rowconfigure(self.natoSubFrame.row, weight=0)
        
#        self.natoSubFrame.columnconfigure(self.natoSubFrame.column, weight=0)
        aLabel = ttk.Label(self.natoSubFrame, text='NATO Stock Number:', style='RegularFieldTitle.TLabel')
        aLabel.grid(row=self.natoSubFrame.row, column=self.natoSubFrame.column, sticky=E+N)
        
        self.natoSubFrame.column = self.natoSubFrame.column + 1
        self.natoSubFrame.columnconfigure(self.natoSubFrame.column, weight=0)
        self.selectecNSN1 = AppEntry(self.natoSubFrame, partsNSN1Lenght)
        self.selectecNSN1.grid(row=self.natoSubFrame.row, column=self.natoSubFrame.column, sticky=N+W)
        
        self.natoSubFrame.column = self.natoSubFrame.column + 1
        self.natoSubFrame.columnconfigure(self.natoSubFrame.column, weight=0)
        aLabel = ttk.Label(self.natoSubFrame, text='-', style='RegularFieldTitle.TLabel')
        aLabel.grid(row=self.natoSubFrame.row, column=self.natoSubFrame.column, sticky=W+N)

        self.natoSubFrame.column = self.natoSubFrame.column + 1
        self.natoSubFrame.columnconfigure(self.natoSubFrame.column, weight=0)
        self.selectecNSN2 = AppEntry(self.natoSubFrame, partsNSN2Lenght)
        self.selectecNSN2.grid(row=self.natoSubFrame.row, column=self.natoSubFrame.column, sticky=N+W)
        
        self.natoSubFrame.column = self.natoSubFrame.column + 1
        self.natoSubFrame.columnconfigure(self.natoSubFrame.column, weight=0)
        aLabel = ttk.Label(self.natoSubFrame, text='-', style='RegularFieldTitle.TLabel')
        aLabel.grid(row=self.natoSubFrame.row, column=self.natoSubFrame.column, sticky=W+N)

        self.natoSubFrame.column = self.natoSubFrame.column + 1
        self.natoSubFrame.columnconfigure(self.natoSubFrame.column, weight=0)
        self.selectecNSN3 = AppEntry(self.natoSubFrame, partsNSN3Lenght)
        self.selectecNSN3.grid(row=self.natoSubFrame.row, column=self.natoSubFrame.column, sticky=N+W)
        
        self.natoSubFrame.column = self.natoSubFrame.column + 1
        self.natoSubFrame.columnconfigure(self.natoSubFrame.column, weight=0)
        aLabel = ttk.Label(self.natoSubFrame, text='-', style='RegularFieldTitle.TLabel')
        aLabel.grid(row=self.natoSubFrame.row, column=self.natoSubFrame.column, sticky=W+N)

        self.natoSubFrame.column = self.natoSubFrame.column + 1
        self.natoSubFrame.columnconfigure(self.natoSubFrame.column, weight=0)
        self.selectecNSN4 = AppEntry(self.natoSubFrame, partsNSN4Lenght)
        self.selectecNSN4.grid(row=self.natoSubFrame.row, column=self.natoSubFrame.column, sticky=N+W)
        
        self.natoSubFrame.column = 0
        self.natoSubFrame.row = self.natoSubFrame.row + 1
        self.natoSubFrame.rowconfigure(self.natoSubFrame.row, weight=0)
        aLabel = ttk.Label(self.natoSubFrame, text='SNC:', style='RegularFieldTitle.TLabel')
        aLabel.grid(row=self.natoSubFrame.row, column=self.natoSubFrame.column, sticky=E+N)
        
        self.natoSubFrame.column = self.natoSubFrame.column + 1
        self.selectecSNC = AppEntry(self.natoSubFrame, partsSNCLenght)
        self.selectecSNC.grid(row=self.natoSubFrame.row, column=self.natoSubFrame.column, sticky=N+W)
        
        self.natoSubFrame.column = 0
        self.natoSubFrame.row = self.natoSubFrame.row + 1
        self.natoSubFrame.rowconfigure(self.natoSubFrame.row, weight=0)
        aLabel = ttk.Label(self.natoSubFrame, text='SSI:', style='RegularFieldTitle.TLabel')
        aLabel.grid(row=self.natoSubFrame.row, column=self.natoSubFrame.column, sticky=E+N)
 
        self.natoSubFrame.column = self.natoSubFrame.column + 1
        self.selectecSSI = AppEntry(self.natoSubFrame, partsSSILenght)
        self.selectecSSI.grid(row=self.natoSubFrame.row, column=self.natoSubFrame.column, sticky=N+W)

        self.natoSubFrame.column = 0
        self.natoSubFrame.row = self.natoSubFrame.row + 1
        self.natoSubFrame.rowconfigure(self.natoSubFrame.row, weight=0)
        aLabel = ttk.Label(self.natoSubFrame, text='Catalog Number:', style='RegularFieldTitle.TLabel')
        aLabel.grid(row=self.natoSubFrame.row, column=self.natoSubFrame.column, sticky=E+N)
 
        self.natoSubFrame.column = self.natoSubFrame.column + 1
        self.selectecCatNum = AppEntry(self.natoSubFrame, partsCatNumLenght)
        self.selectecCatNum.grid(row=self.natoSubFrame.row, column=self.natoSubFrame.column, columnspan= 7, sticky=N+W)
     
        self.natoFrame.column = self.natoFrame.column + 1
        self.selectedClass = AppVerticalFieldFrame(self.natoFrame, 'Class', partsClassLenght)
        self.selectedClass.grid(row=self.natoFrame.row, column=self.natoFrame.column, sticky=N)
        
        self.natoFrame.column = self.natoFrame.column + 1
        self.selectedUI = AppVerticalFieldFrame(self.natoFrame, 'Unit  of Issue (UI)', partsUILenght)
        self.selectedUI.grid(row=self.natoFrame.row, column=self.natoFrame.column, sticky=N)
        
        self.natoFrame.row = self.natoFrame.row + 1
        self.natoFrame.rowconfigure(self.natoFrame.row, weight = 0)
        self.natoFrame.column = 0
        
        self.natoFrame.row = self.natoFrame.row + 1
        self.natoFrame.rowconfigure(self.natoFrame.row, weight = 0)
        
        self.natoFrame.column = self.natoFrame.column + 1
        self.selectedPrice = AppHorizontalFieldFrame(self.natoFrame, "Price", partsPriceLenght)
        self.selectedPrice.grid(row=self.natoFrame.row, column=self.natoFrame.column, columnspan=2, sticky=N)

        
############################################# Supplier/Loc Row                      
        
        self.fieldsFrame.row = self.fieldsFrame.row + 1
        self.fieldsFrame.rowconfigure(self.fieldsFrame.row, weight=0)
        self.fieldsFrame.column = 0
        
        self.supplierFrame = AppBorderFrame(self.fieldsFrame, 5)
        self.supplierFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, sticky=N+S+E+W)
        self.supplierFrame.rowconfigure(self.supplierFrame.row, weight=0)

#        self.natoSubFrame.columnconfigure(self.natoSubFrame.column, weight=0)
#        aLabel = ttk.Label(self.supplierFrame, text='Supplier:', style='RegularFieldTitle.TLabel')
#        aLabel.grid(row=self.supplierFrame.row, column=self.supplierFrame.column, sticky=E+N)
        
#        self.supplierFrame.column = self.supplierFrame.column + 1
#        self.selectecSupplier = AppEntry(self.supplierFrame,  suppliersNameLenght)
#        self.selectecSupplier.grid(row=self.supplierFrame.row, column=self.supplierFrame.column, sticky=N+W)
        
#        self.supplierFrame.row = self.supplierFrame.row + 1
#        self.supplierFrame.rowconfigure(self.supplierFrame.row, weight=0)
#        self.supplierFrame.column = 0
        
#        aLabel = ttk.Label(self.supplierFrame, text='Supplier #:', style='RegularFieldTitle.TLabel')
#        aLabel.grid(row=self.supplierFrame.row, column=self.supplierFrame.column, sticky=E+N)
        
#        self.supplierFrame.column = self.supplierFrame.column + 1
#        self.selectecSupplierNum = AppEntry(self.supplierFrame, partsSupplierNumLenght)
#        self.selectecSupplierNum.grid(row=self.supplierFrame.row, column=self.supplierFrame.column, sticky=N+W)
#        
#        self.supplierFrame.row = self.supplierFrame.row + 1
#        self.supplierFrame.rowconfigure(self.supplierFrame.row, weight=0)
#        self.supplierFrame.column = 0
#
        aLabel = ttk.Label(self.supplierFrame, text='''Supplier's Name:''', style='RegularFieldTitle.TLabel')
        aLabel.grid(row=self.supplierFrame.row, column=self.supplierFrame.column, sticky=E+N)
        
        self.supplierFrame.column = self.supplierFrame.column + 1
        self.suppliersNameCBData = AppCBList(AppDB.getSuppliersDictionaryInfo())
#        supplierNameV = Tix.StringVar()
#        supplierNameV.trace('w',self._supplierNameEntered)
        self.selectedSupplierCB = AppCB(self.supplierFrame, self.suppliersNameCBData.getList(), suppliersNameLenght)
        self.selectedSupplierCB.grid(row=self.supplierFrame.row, column=self.supplierFrame.column, sticky=N+W)
#        self.selectedSupplierCB.bind('<FocusOut>', lambda event: self._supplierNameEntered())
        
        self.fieldsFrame.column = self.fieldsFrame.column + 1
        self.locFrame = AppBorderFrame(self.fieldsFrame, 5)
        self.locFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, sticky=N+S+E+W)
        self.locFrame.rowconfigure(self.locFrame.row, weight=0)

        aLabel = ttk.Label(self.locFrame, text='Location:', style='RegularFieldTitle.TLabel')
        aLabel.grid(row=self.locFrame.row, column=self.locFrame.column, sticky=N)
        
        self.locFrame.column = self.locFrame.column + 1      
        self.selectedRow = AppVerticalFieldFrame(self.locFrame, 'Row', partsQtyLenght)
        self.selectedRow.grid(row=self.locFrame.row, column=self.locFrame.column, sticky=N+S)
        
        self.locFrame.column = self.locFrame.column + 1      
        self.selectedSide = AppVerticalFieldFrame(self.locFrame, 'Side', partsQtyLenght)
        self.selectedSide.grid(row=self.locFrame.row, column=self.locFrame.column, sticky=N+S)
        
        self.locFrame.column = self.locFrame.column + 1      
        self.selectedShelf = AppVerticalFieldFrame(self.locFrame, 'Shelf', partsQtyLenght)
        self.selectedShelf.grid(row=self.locFrame.row, column=self.locFrame.column, sticky=N+S)
        
        self.locFrame.column = self.locFrame.column + 1      
        self.selectedSection = AppVerticalFieldFrame(self.locFrame, 'Section', partsLocLenght)
        self.selectedSection.grid(row=self.locFrame.row, column=self.locFrame.column, sticky=N+S)
        
############################################# Last Update Row
        self.fieldsFrame.row = self.fieldsFrame.row + 1
        self.fieldsFrame.column = 0
        self.fieldsFrame.rowconfigure(self.fieldsFrame.row, weight=0)
        
        self.lastUpdate = AppLastUpdateFormat(self.fieldsFrame, self.recList[self.curRecNumV.get()][37], self.recList[self.curRecNumV.get()][38])
        self.lastUpdate.grid(row=self.fieldsFrame.row,column=self.fieldsFrame.column, columnspan = colTotal, sticky=E+N+S)
               
        self.fieldsDisable()
#        self.fieldsFrame.rowconfigure(self.fieldsFrame.row, weight=0)
#        self.headerList = ['Code', 'Name']
#        for i in range(len(self.headerList)):       
#            aLabel = ttk.Label(self.fieldsFrame, text=self.headerList[i], style='RegularFieldTitle.TLabel')
#            aLabel.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, sticky=N+S)
#            self.fieldsFrame.column = self.fieldsFrame.column + 1

    def displayRec(self, *args):
        ######################################################
        # ECE Parts CRUD
        #  '''CREATE TABLE Parts(
        #        0        partID                  INTEGER PRIMARY KEY AUTOINCREMENT  NOT NULL,
        #        1        BASIC      CHAR(20),
        #        2        VALUE      CHAR(3),
        #        3        VALUED     CHAR(3),
        #        4        MULT       CHAR(1),
        #        5        TOLE       CHAR(5),
        #        6        SIZE       CHAR(7),
        #        7        DESCR      CHAR(40),
        #        8        NSN1       CHAR(4),
        #        9        NSN2       CHAR(2),
        #        10       NSN3       CHAR(3),
        #        11       NSN4       CHAR(4),
        #        12       ORDER_NUM  CHAR(20),
        #        13       SUPP_NUM   CHAR(10),     NOT IN USE ANYMORE
        #        14       ITEM_NUM   CHAR(12),
        #        15       UI         CHAR(2),
        #        16       SNC        CHAR(1),
        #        17       CLASS      CHAR(1),
        #        18       PRICE      CHAR(5),
        #        19       PRICED     CHAR(2),
        #        20       SUP        CHAR(1),
        #        21       SSI        CHAR(2),
        #        22       RETAIL     CHAR(9),
        #        23       QUANT      CHAR(4),
        #        24       QMIN       CHAR(4),
        #        25       QORDER     CHAR(4),
        #        26       QUSE       CHAR(4),
        #        27       QTEMP      CHAR(4),
        #        28       FUNCTION   CHAR(20),
        #        29       DESCR2     CHAR(60),
        #        30       NOMEN      CHAR(15),
        #        31       ROW        CHAR(2)
        #        32       SIDE       CHAR(1)
        #        33       SHELF      CHAR(1)
        #        34       SECTION    CHAR(3)
        #        35       DEPARTMENT CHAR(2),
        #        36       S_NAME     CHAR(30),    NOT IN USE ANYMORE
        #        37       lastModified  INTEGER,
        #        38       updaterID     INTEGER
        #        39       suppliersID   INTEGER
        #        40       departmentsID   INTEGER
        #
        # Example
        # self.selectedCode.load(self.recList[self.curRecNumV.get()][1])
        
        self.selectedBasic.load(self.recList[self.curRecNumV.get()][1])
        self.selectedValue.load(self._buildDisplayValue()) # Indices 2 and 3
        self.selectedMultiplier.load(self.recList[self.curRecNumV.get()][4]) 
        self.selectedTolerance.load(self.recList[self.curRecNumV.get()][5]) 
        self.selectedSize.load(self.recList[self.curRecNumV.get()][6]) 
        self.selectedDescription.load(self.recList[self.curRecNumV.get()][7])
        self.selectecNSN1.load(self.recList[self.curRecNumV.get()][8])
        self.selectecNSN2.load(self.recList[self.curRecNumV.get()][9])
        self.selectecNSN3.load(self.recList[self.curRecNumV.get()][10])
        self.selectecNSN4.load(self.recList[self.curRecNumV.get()][11])
        self.selectecCatNum.load(self.recList[self.curRecNumV.get()][12])
#        self.selectecSupplierNum.load(self.recList[self.curRecNumV.get()][13])                
        self.selectedUI.load(self.recList[self.curRecNumV.get()][15])
        self.selectecSNC.load(self.recList[self.curRecNumV.get()][16])
        self.selectedClass.load(self.recList[self.curRecNumV.get()][17])
        self.selectedPrice.load(self._buildDisplayPrice())  #indices 18-19           
        self.selectecSSI.load(self.recList[self.curRecNumV.get()][21])
        self.selectedStock.load(self.recList[self.curRecNumV.get()][23])      
        self.selectedMin.load(self.recList[self.curRecNumV.get()][24])     
        self.selectedOnOrder.load(self.recList[self.curRecNumV.get()][25])      
        self.selectedNeeded.load(self.recList[self.curRecNumV.get()][26])    
        self.selectedToday.load(self.recList[self.curRecNumV.get()][27])
        self.selectedFunction.load(self.recList[self.curRecNumV.get()][28])
        self.selectedDescription2.load(self.recList[self.curRecNumV.get()][29])   
        self.selectedNomenclature.load(self.recList[self.curRecNumV.get()][30])
        self.selectedRow.load(self.recList[self.curRecNumV.get()][31])
        self.selectedSide.load(self.recList[self.curRecNumV.get()][32])       
        self.selectedShelf.load(self.recList[self.curRecNumV.get()][33]) 
        self.selectedSection.load(self.recList[self.curRecNumV.get()][34])
#        self.selectedDepartment.load(self.recList[self.curRecNumV.get()][35])
        if self.recList[self.curRecNumV.get()][40] != '':
            self.selectedDepartment.load(self.departmentsCBData.getName(self.recList[self.curRecNumV.get()][40]))
        else:
            self.selectedDepartment.clear()
#        self.selectecSupplier.load(self.recList[self.curRecNumV.get()][36])
        self.lastUpdate.load(self.recList[self.curRecNumV.get()][37], self.recList[self.curRecNumV.get()][38])
        if self.recList[self.curRecNumV.get()][39] != '':
            self.selectedSupplierCB.load(self.suppliersNameCBData.getName(self.recList[self.curRecNumV.get()][39]))
        else:
            self.selectedSupplierCB.clear()
                              
        if self.accessLevel == 'Root':
            pass
            
        self.fieldsDisable()
        
    def _buildDisplayValue(self):
        return "{0}.{1}".format(self.recList[self.curRecNumV.get()][2],self.recList[self.curRecNumV.get()][3])
    
    def _parseSelectedValue(self,aValue):
        if len(aValue) == 0:
            aList = ['','']
        elif aValue.find('.') != -1:
            aList = aValue.split('.')
        else:
            aList = [aValue,'']
        return aList
            
    def _parseSelectedPrice(self, aValue):
        if len(aValue) == 0:
            aList = ['','']
        elif aValue.find('.') != -1:
            theValue = re.sub('[$]','',aValue)
            aList = theValue.split('.')
        else:
            theValue = re.sub('[$]','',aValue)
            aList = [theValue,'']
        return aList
            
    def _buildDisplayPrice(self):
        return "${0}.{1}".format(self.recList[self.curRecNumV.get()][18],self.recList[self.curRecNumV.get()][19])

    def _selectedValueCheck(self, event):
        #
        #  Format is ###.###
        #
        self.myMsgBar.clearMessage()
        aMsg = '''ERROR: format for Value is ###.### (ie: 345.233).''' 
        currentWidget = event.widget
        if currentWidget.select_present() == True:
            if event.char.isdigit() == True:
                currentWidget.delete(0, END)      
        currentContent = currentWidget.get()
        if event.char:
            try:
#                if event.char == "\t":
#                   self._focusNext(event.widget)                   
                if event.char != "\b":
                    if event.char == "\t":
                        if len(currentContent) > 0 and len(currentContent) < 8:
                            self.selectedMultiplier.focus()
                    elif event.char.isdigit() == False and event.char != '.':
                        raise Exception
                    else:
                        futureResult = "{0}{1}".format(currentContent,event.char)
                        if futureResult.find('.') != -1:
                            float(futureResult)
                            aSplit = self._parseSelectedValue(futureResult)
                            if len(aSplit[0]) > 3:
                                raise Exception
                            if len(aSplit[1]) == 3:
                                self.selectedMultiplier.focus()
                            elif len(aSplit[1]) > 3:
                                raise Exception
                            if len(futureResult) == 7:
                                self.selectedMultiplier.focus()
                        elif len(futureResult) == 3:  # force the . after the third digit
                            currentWidget.delete(0, END)
                            currentWidget.insert(0, futureResult+'.')
                            return "break" # overrides the current entry
            except:
                self.myMsgBar.newMessage('error', aMsg)
                return "break"
              
    def fieldsClear(self):
        self.selectedBasic.clear()
        self.selectedDescription.clear()
        self.selectedFunction.clear()
        self.selectedNomenclature.clear()
        self.selectedDepartment.clear()
        self.selectedRow.clear()
        self.selectedSide.clear()
        self.selectedShelf.clear()
        self.selectedSection.clear()
        self.selectedStock.clear()      
        self.selectedMin.clear()     
        self.selectedOnOrder.clear()      
        self.selectedNeeded.clear()    
        self.selectedToday.clear()
        self.selectedValue.clear()
        self.selectedMultiplier.clear() 
        self.selectedTolerance.clear() 
        self.selectedSize.clear() 
        self.selectedDescription2.clear()
        self.selectedClass.clear()
        self.selectedUI.clear()
        self.selectecSNC.clear()
        self.selectecNSN1.clear()
        self.selectecNSN2.clear()
        self.selectecNSN3.clear()
        self.selectecNSN4.clear()
        self.selectecSSI.clear()
#        self.selectecSupplier.clear()
#        self.selectecSupplierNum.clear()
        self.selectecCatNum.clear()
        self.selectedSupplierCB.clear(),
        self.selectedPrice.clear() 
                      
    def fieldsDisable(self):
        self.selectedBasic.disable()
        self.selectedDescription.disable()
        self.selectedFunction.disable()
        self.selectedNomenclature.disable()
        self.selectedDepartment.disable()
        self.selectedRow.disable()
        self.selectedSide.disable()
        self.selectedShelf.disable()
        self.selectedSection.disable()
        self.selectedStock.disable()      
        self.selectedMin.disable()     
        self.selectedOnOrder.disable()      
        self.selectedNeeded.disable()    
        self.selectedToday.disable()
        self.selectedValue.disable()
        self.selectedMultiplier.disable() 
        self.selectedTolerance.disable() 
        self.selectedSize.disable() 
        self.selectedDescription2.disable()
        self.selectedClass.disable()
        self.selectedUI.disable()
        self.selectecSNC.disable()
        self.selectecNSN1.disable()
        self.selectecNSN2.disable()
        self.selectecNSN3.disable()
        self.selectecNSN4.disable()
        self.selectecSSI.disable()
#        self.selectecSupplier.disable()
#        self.selectecSupplierNum.disable()
        self.selectecCatNum.disable()
        self.selectedSupplierCB.disable(),
        self.selectedPrice.disable()
                               
    def fieldsEnable(self):
        self.selectedBasic.enable()
        self.selectedDescription.enable()
        self.selectedFunction.enable()
        self.selectedNomenclature.enable()
        self.selectedDepartment.enable()
        self.selectedRow.enable()
        self.selectedSide.enable()       
        self.selectedShelf.enable() 
        self.selectedSection.enable()
        self.selectedStock.enable()      
        self.selectedMin.enable()     
        self.selectedOnOrder.enable()      
        self.selectedNeeded.enable()    
        self.selectedToday.enable()
        self.selectedValue.enable()
        self.selectedMultiplier.enable()
        self.selectedTolerance.enable() 
        self.selectedSize.enable() 
        self.selectedDescription2.enable()    
        self.selectedClass.enable()
        self.selectedUI.enable()
        self.selectecSNC.enable()
        self.selectecNSN1.enable()
        self.selectecNSN2.enable()
        self.selectecNSN3.enable()
        self.selectecNSN4.enable()
        self.selectecSSI.enable()
#        self.selectecSupplier.enable()
#        self.selectecSupplierNum.enable()
        self.selectecCatNum.enable()
        self.selectedPrice.enable()
        self.selectedSupplierCB.enable()
                
    ################################################################# Redefined commands
    #
    #  invoke from the tableCommonWindows.  These commands are specific to each tables
    #  The specific actions are redefinded here.
    #       
    #    def displayRec(self, *args):
    #        print("Displaying a Record: ", self.curRecNumV.get())
    #        self._displayRecordFrame()
    def duplicate(self):
        self.fieldsEnable()
    
    def find(self):
        #Table specific.  Will be defined in calling class
        self.fieldsClear()
        self.selectedBasic.enable()
        self.selectedDescription.enable()
        self.selectedDescription2.enable()
        self.selectedNomenclature.enable()
        self.selectedFunction.enable()
        self.selectedBasic.focus()

    def refresh(self):
        self._reDisplayRecordFrame()

    def delete(self):
        self._disableWindowCommand()
        aMsg = "Are you sure you want to delete record {0}?".format(self.recList[self.curRecNumV.get()][1])
        ans = Mb.askyesno('Delete Record', aMsg)
        if ans == True:
            AppDB.deleteParts(self.recList[self.curRecNumV.get()][0]) # remove from DB
            aMsg = "Record {0} has been deleted.".format(self.recList[self.curRecNumV.get()][1])
            self.myMsgBar.newMessage('info', aMsg)
            self.recList.pop(self.curRecNumV.get())  # remove from current list
            if self.curRecBeforeOp != 0:
                self.curRecNumV.set(self.curRecBeforeOp - 1)
            else:
                self.curRecNumV.set(self.curRecBeforeOp)
            self._displayRecordFrame()
        self._enableWindowCommand()     
        self.myDatabaseBar.defaultState()
        self.navigationEnable(self.accessLevel)
        self.resetCurOp()
        
    def edit(self):
        self.fieldsEnable()
        self.selectedDepartment.focus()
                 
    def add(self):
        self.fieldsClear()
        self.fieldsEnable()
        self.selectedDepartment.focus()
    
    def save(self):
        #
        #  Table specific.  Return a True if succesful saved.
        #
        if self.curOp == 'add':
            
            departmentsID=self.departmentsCBData.getID(self.selectedDepartment.get())
            departmentCode = AppDB.getDepartmentsCode(departmentsID)
            myValue = self._parseSelectedValue(self.selectedValue.get())
            myPrice = self._parseSelectedPrice(self.selectedPrice.get())
            AppDB.insertParts(self.selectedBasic.get(),myValue[0],myValue[1],self.selectedMultiplier.get(),self.selectedTolerance.get(),self.selectedSize.get(),self.selectedDescription.get(),
                        self.selectecNSN1.get(),self.selectecNSN2.get(),self.selectecNSN3.get(),self.selectecNSN4.get(),self.selectecCatNum.get(),
                        '','',self.selectedUI.get(),self.selectecSNC.get(),self.selectedClass.get(),
                        myPrice[0],myPrice[1],'',self.selectecSSI.get(), '',
                        self.selectedStock.get(),self.selectedMin.get(),self.selectedOnOrder.get(),self.selectedNeeded.get(),self.selectedToday.get(),
                        self.selectedFunction.get(),self.selectedDescription2.get(),self.selectedNomenclature.get(),self.selectedRow.get(),
                        self.selectedSide.get(),self.selectedShelf.get(),self.selectedSection.get(), departmentCode[0][0],'',
                        convertDateStringToOrdinal(todayDate), self.updaterID, self.suppliersNameCBData.getID(self.selectedSupplierCB.get()), departmentsID)
            
            ID = AppDB.getPartsID(self.selectedBasic.get(), myValue[0], myValue[1], self.selectedDescription.get(), self.selectedDescription2.get(), 
                                  self.selectedFunction.get(), self.selectedNomenclature.get(), convertDateStringToOrdinal(todayDate),  self.updaterID)
            
            aMsg = "Record {0} has been added.".format(self.selectedBasic.get())
            self.myMsgBar.newMessage('info', aMsg)
                       
            if self.curFind == True:
                anItem = (ID[0][0],self.selectedBasic.get(),myValue[0],myValue[1],self.selectedMultiplier.get(),self.selectedTolerance.get(),self.selectedSize.get(),self.selectedDescription.get(),
                        self.selectecNSN1.get(),self.selectecNSN2.get(),self.selectecNSN3.get(),self.selectecNSN4.get(),self.selectecCatNum.get(),
                        '','',self.selectedUI.get(),self.selectecSNC.get(),self.selectedClass.get(),
                        myPrice[0],myPrice[1],'',self.selectecSSI.get(), '',
                        self.selectedStock.get(),self.selectedMin.get(),self.selectedOnOrder.get(),self.selectedNeeded.get(),self.selectedToday.get(),
                        self.selectedFunction.get(),self.selectedDescription2.get(),self.selectedNomenclature.get(),self.selectedRow.get(),
                        self.selectedSide.get(),self.selectedShelf.get(),self.selectedSection.get(),departmentCode[0][0],'',
                        convertDateStringToOrdinal(todayDate), self.updaterID, self.suppliersNameCBData.getID(self.selectedSupplierCB.get()), departmentsID)
                self.recList.append(anItem)
                self.curRecNumV.set(len(self.recList)-1)               
                aNewList = sorted(self.recList, key=getKey)
                self.recList = aNewList
                newIdx = getIndex(ID[0][0], aNewList)
                self.curRecNumV.set(newIdx)
                self.displayRec()
            else:
                #
                #  If not in find mode, re_display the list and point
                # to the newly added item.
                #           
                self._getNewRecList()
                newIdx = getIndex(ID[0][0], self.recList)
                if newIdx != -1:
                    self.curRecNumV.set(newIdx)                
                self._reDisplayRecordFrame()
            self.resetCurOp()
           
        elif self.curOp == 'edit':
            pass 
            departmentsID=self.departmentsCBData.getID(self.selectedDepartment.get())
            departmentCode = AppDB.getDepartmentsCode(departmentsID)
            myValue = self._parseSelectedValue(self.selectedValue.get())
            myPrice = self._parseSelectedPrice(self.selectedPrice.get())
            AppDB.updateParts(self.recList[self.curRecNumV.get()][0], self.selectedBasic.get(),myValue[0],myValue[1],self.selectedMultiplier.get(),self.selectedTolerance.get(),
                        self.selectedSize.get(),self.selectedDescription.get(),self.selectecNSN1.get(),self.selectecNSN2.get(),self.selectecNSN3.get(),self.selectecNSN4.get(),
                        self.selectecCatNum.get(),self.recList[self.curRecNumV.get()][13],self.recList[self.curRecNumV.get()][14],self.selectedUI.get(),self.selectecSNC.get(),self.selectedClass.get(),
                        myPrice[0],myPrice[1],self.recList[self.curRecNumV.get()][20],self.selectecSSI.get(), self.recList[self.curRecNumV.get()][22],
                        self.selectedStock.get(),self.selectedMin.get(),self.selectedOnOrder.get(),self.selectedNeeded.get(),self.selectedToday.get(),
                        self.selectedFunction.get(),self.selectedDescription2.get(),self.selectedNomenclature.get(),self.selectedRow.get(),
                        self.selectedSide.get(),self.selectedShelf.get(),self.selectedSection.get(),departmentCode[0][0],
                        self.recList[self.curRecNumV.get()][36],
                        convertDateStringToOrdinal(todayDate), self.updaterID, self.suppliersNameCBData.getID(self.selectedSupplierCB.get()), departmentsID)
            aMsg = "Record {0} has been updated.".format(self.selectedBasic.get())
            self.myMsgBar.newMessage('info', aMsg)
            #
            # Add the item again but with modified data.            
            #
            anItem=(self.recList[self.curRecNumV.get()][0], self.selectedBasic.get(),myValue[0],myValue[1],self.selectedMultiplier.get(),self.selectedTolerance.get(),
                        self.selectedSize.get(),self.selectedDescription.get(),self.selectecNSN1.get(),self.selectecNSN2.get(),self.selectecNSN3.get(),self.selectecNSN4.get(),
                        self.selectecCatNum.get(),self.recList[self.curRecNumV.get()][13],self.recList[self.curRecNumV.get()][14],self.selectedUI.get(),self.selectecSNC.get(),self.selectedClass.get(),
                        myPrice[0],myPrice[1],self.recList[self.curRecNumV.get()][20],self.selectecSSI.get(), self.recList[self.curRecNumV.get()][22],
                        self.selectedStock.get(),self.selectedMin.get(),self.selectedOnOrder.get(),self.selectedNeeded.get(),self.selectedToday.get(),
                        self.selectedFunction.get(),self.selectedDescription2.get(),self.selectedNomenclature.get(),self.selectedRow.get(),
                        self.selectedSide.get(),self.selectedShelf.get(),self.selectedSection.get(),departmentCode[0][0],
                        self.recList[self.curRecNumV.get()][36],
                        convertDateStringToOrdinal(todayDate), self.updaterID, self.suppliersNameCBData.getID(self.selectedSupplierCB.get()), departmentsID)
            aRemovedItem = self.recList.pop(self.curRecNumV.get())  # Remove the item that was modified
            self.recList.insert(self.curRecNumV.get(),anItem)
            self.displayRec()
            self.resetCurOp()
            
        elif self.curOp == 'find':
            self.recListTemp = AppDB.findParts(self.selectedBasic.get(), self.selectedDescription.get(), self.selectedDescription2.get(), self.selectedNomenclature.get(), 
                                           self.selectedFunction.get())
            if len(self.recListTemp) == 0:
                aMsg = "WARNING: No records were found using the given search criterias."
#                self._reDisplayRecordFrame()
                self.curFind = False
                self.displayCurFunction.setDefaultMode()
                self.myMsgBar.newMessage('warning', aMsg)
            elif len(self.recList) == 1:
                self.curFind = True
                self.recList = self.recListTemp
                self.displayCurFunction.setFindResultMode()
                aMsg = "One record was found using the given search criterias."
                self.curRecNumV.set(0)
                self.myMsgBar.newMessage('info', aMsg)
            else:
                self.curFind = True
                self.recList = self.recListTemp
                self.displayCurFunction.setFindResultMode()
                aMsg = "{0} records were found using the given search criterias.".format(len(self.recList))
                self.curRecNumV.set(0)
                self.myMsgBar.newMessage('info', aMsg)
            self.displayRec()
        return True
     


        