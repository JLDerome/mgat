'''
Created on Feb 11, 2016

@author: derome
'''
import tkinter.tix as Tix
from MyGolfApp2016.AppConstants import AppDefaultForeground,fontMonstrousB
from tkinter.constants import E, W, N, S

class mainMenu(Tix.Frame):
    def __init__(self, parent, controller):
        Tix.Frame.__init__(self, parent)
        
        columnCount = 0
        rowCount = 0
        
        mainDisplay=Tix.Frame(self, border = 8)        
        mainDisplay.grid(row=rowCount, column=columnCount, sticky=E+W+S+N)
        
        mainDisplayRow = 0
        mainDisplayColumn = 0             
        wMsg = Tix.Label(mainDisplay, text='Main Menu', font=fontMonstrousB, fg=AppDefaultForeground)
        wMsg.grid(row=mainDisplayRow, column=mainDisplayColumn, sticky=N)
        self.rowconfigure(mainDisplayRow, weight=0)             