'''
Created on Feb 11, 2016

@author: derome
'''
import tkinter.tix as Tix
from MyGolfApp2016.AppConstants import AppTitle, AppWelcomeImg , welcomeLogoWidth, welcomeLogoHeight, AppDefaultForeground,fontBigB,fontMonstrousB,loginDataFieldWidth
from MyGolfApp2016.AppProcFROMECE import resizeImage, updateECEShopConstant,createOpenECEShopDB
from tkinter.constants import E, W, S, N
from PIL import Image
from ECEShopApp.mainMenu import mainMenu

class welcomeLoginDisplay(Tix.Frame):
    def __init__(self, parent, controller):
        Tix.Frame.__init__(self, parent)
        
        columnCount = 0
        rowCount = 0
        
        original = Image.open(AppWelcomeImg)
        image1 = resizeImage(original,welcomeLogoWidth,welcomeLogoHeight)
        panel1 = Tix.Label(self, image=image1)
        self.display = image1
        panel1.grid(row=rowCount, column=columnCount)
        
        columnCount = columnCount + 1
        mainDisplay=Tix.Frame(self, border = 8)        
        mainDisplay.grid(row=rowCount, column=columnCount, sticky=E+W+S+N)
        
        mainDisplayRow = 0
        mainDisplayColumn = 0             
        wMsg = Tix.Label(mainDisplay, text=AppTitle, font=fontMonstrousB, fg=AppDefaultForeground)
        wMsg.grid(row=mainDisplayRow, column=mainDisplayColumn, sticky=N)
        self.rowconfigure(mainDisplayRow, weight=0)
        
        mainDisplayRow = mainDisplayRow + 1
        loginFieldTitleFrame=Tix.Frame(mainDisplay)        
        loginFieldTitleFrame.grid(row=mainDisplayRow, column=mainDisplayColumn, sticky=W+N) 
               
        loginLine1Title = Tix.Label(loginFieldTitleFrame, text='Login ID:', font=fontBigB, fg=AppDefaultForeground)
        loginLine1Title.grid(row=0, column=0, sticky=N+E)
        
        selectLoginID = Tix.Entry(loginFieldTitleFrame, justify='left', width = loginDataFieldWidth, 
                                            font=fontBigB, fg=AppDefaultForeground)
        selectLoginID.grid(row=0, column=1, sticky=N+W)
        
        loginLine2Title = Tix.Label(loginFieldTitleFrame, text='Password:', font=fontBigB, fg=AppDefaultForeground)
        loginLine2Title.grid(row=1, column=0, sticky=N+E)
        selectPassword = Tix.Entry(loginFieldTitleFrame, justify='left', width = loginDataFieldWidth,
                                            font=fontBigB, fg=AppDefaultForeground)
        selectPassword.grid(row=1, column=1, sticky=N+W)
        
        self.rowconfigure(mainDisplayRow, weight=0)
              
        updateECEShopConstant()     

        mainDisplayRow = mainDisplayRow + 1
        startButton = Tix.Button(mainDisplay, text='Login', font=fontBigB, fg=AppDefaultForeground,  
                                   command=lambda: self._loginDetected())
        startButton.grid(row=mainDisplayRow, sticky=N)
        
    def _loginDetected(self):
        createOpenECEShopDB(self)
      
##        mainDisplayRowCount = mainDisplayRowCount + 1
 #       startButton = Tix.Button(mainDisplay, text='Login',  
 #                                  command=lambda: createOpenECEShopDB(controller, self))
 #       startButton.grid(row=mainDisplayRowCount, column=2)
#        
#        Tix.Frame.__init__(self, parent)
#        mainDisplay=Tix.Frame(self)        
#        wMsg = Tix.Label(mainDisplay, text=AppTitle)
#        wMsg.pack(side=Tix.TOP, ipadx=10, ipady=0)
#        
#        original = Image.open(AppWelcomeImg)
#        image1 = resizeImage(original,welcomeLogoWidth,welcomeLogoHeight)
#        panel1 = Tix.Label(mainDisplay, image=image1)
#        self.display = image1
#        panel1.pack(expand=YES)
#        
#        mainDisplay.pack()
        
