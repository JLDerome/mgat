import sqlite3 as sql
import os
import csv
import AppCRUD as AppCRUD
from AppProc import getDBLocationFullName, convertDateStringToOrdinal, calculateHdcpList
from AppConstants import *

db=None
cursor = None
lidGolfers = 0
#
#  Field holeDrives contains a distance 3 Char, a R or L
#  Fairways are deduced from there
#  Putts deduces the greens and
#  Sand saves are deduced from sandtraps
#
# cursor.execute('''CREATE TABLE GameDetails(
#    uniqueID       INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
#    holeNumber     SMALLINT    NOT NULL,
#    holeScore      SMALLINT    NOT NULL,
#    holePutts      SMALLINT,
#    holeDrives     CHAR(4),
#    holeSandTraps  CHAR(1),
#    gameID         INTEGER     NOT NULL,
#    deleted        CHAR(1)     NOT NULL,
#    lastModified   INTEGER,
#    updaterID      INTEGER,
#    FOREIGN KEY(gameID) REFERENCES Games(uniqueID)
#    );''')

def calculategolferStrokes(golferIndex, courseSlope):
    golferStrokes = (float(golferIndex) * float(courseSlope)) / float(113)
    return int(golferStrokes)

def calculateAfterCurrentGameHdcp(aOrdinalDate, golferID, curGameID, curGameNumber, NoClose=False):
    curGameList = get_games_hdcp_diff_before_date_for_golfer(aOrdinalDate, golferID, curGameID,
                                                                        curGameNumber, NoClose)  # organized, latest first
    numberOfGames = len(curGameList)
    if numberOfGames == 0:
        return 0.0
    else:
        howManyGamesToday = 1
        removeTupleAmount = 0
        #        for i in range(len(curGameList)):
        #            if i == len(curGameList)-1:
        #                break
        i = 1
        while i < len(curGameList):
            if curGameList[i - 1][0] == curGameList[i][0]:
                howManyGamesToday = howManyGamesToday + 1
            else:
                break
            i = i + 1

        if howManyGamesToday > 1:
            for i in range(howManyGamesToday):
                if curGameList[i][2] == curGameNumber:
                    removeTupleAmount = i

        newGameList = []
        for i in range(removeTupleAmount, numberOfGames):
            newGameList.append(curGameList[i][1])

        index = calculateHdcpList(newGameList)
        return index

def calculateAfterCurrentGameHdcpMASS(aOrdinalDate, golferID, curGameID, curGameNumber):
    curGameList = get_games_hdcp_diff_before_date_for_golferMASS(aOrdinalDate, golferID, curGameID,
                                                                        curGameNumber)  # organized, latest first
    numberOfGames = len(curGameList)
    if numberOfGames == 0:
        return 0.0
    else:
        howManyGamesToday = 1
        removeTupleAmount = 0
        #        for i in range(len(curGameList)):
        #            if i == len(curGameList)-1:
        #                break
        i = 1
        while i < len(curGameList):
            if curGameList[i - 1][0] == curGameList[i][0]:
                howManyGamesToday = howManyGamesToday + 1
            else:
                break
            i = i + 1

        if howManyGamesToday > 1:
            for i in range(howManyGamesToday):
                if curGameList[i][2] == curGameNumber:
                    removeTupleAmount = i

        newGameList = []
        for i in range(removeTupleAmount, numberOfGames):
            newGameList.append(curGameList[i][1])

        index = calculateHdcpList(newGameList)
        return index

# def calculateCurrentHdcp(golferID):
#     print("Golfer ID: ", golferID)
#     curGameList = get_games_hdcp_diff_for_golfer(golferID)  # organized, latest first
#     numberOfGames = len(curGameList)
#     if numberOfGames == 0:
#         return 0.0
#     else:
#         newGameList = []
#         for i in range(numberOfGames):
#             newGameList.append(curGameList[i][1])
#
#         index = calculateHdcpList(newGameList)
#         return index


def calculateBeforeCurrentGameHdcpMASS(aOrdinalDate, golferID, curGameID, curGameNumber):
    curGameList = get_games_hdcp_diff_before_date_for_golferMASS(aOrdinalDate, golferID, curGameID,
                                                                        curGameNumber)  # organized, latest first
    numberOfGames = len(curGameList)
    if numberOfGames <= 1:
        return float(0.0)
    else:
        howManyGamesToday = 1
        removeTupleAmount = 0
        for i in range(len(curGameList)):
            if i == len(curGameList) - 1:
                break
            elif curGameList[i][0] == curGameList[i + 1][0]:
                howManyGamesToday = howManyGamesToday + 1
                if curGameNumber == curGameList[i][2]:
                    removeTupleAmount = i + 1
            else:
                break

        if howManyGamesToday > 1:
            if removeTupleAmount == 0:
                removeTupleAmount = howManyGamesToday
        else:
            removeTupleAmount = 1

        newGameList = []
        for i in range(removeTupleAmount, numberOfGames):
            newGameList.append(curGameList[i][1])

        index = calculateHdcpList(newGameList)
        return index

def calculateBeforeCurrentGameHdcp(aOrdinalDate, golferID, curGameID, curGameNumber, NoClose):
    curGameList = get_games_hdcp_diff_before_date_for_golfer(aOrdinalDate, golferID, curGameID,
                                                                        curGameNumber, NoClose)  # organized, latest first
    numberOfGames = len(curGameList)
    if numberOfGames <= 1:
        return float(0.0)
    else:
        howManyGamesToday = 1
        removeTupleAmount = 0
        for i in range(len(curGameList)):
            if i == len(curGameList) - 1:
                break
            elif curGameList[i][0] == curGameList[i + 1][0]:
                howManyGamesToday = howManyGamesToday + 1
                if curGameNumber == curGameList[i][2]:
                    removeTupleAmount = i + 1
            else:
                break

        if howManyGamesToday > 1:
            if removeTupleAmount == 0:
                removeTupleAmount = howManyGamesToday
        else:
            removeTupleAmount = 1

        newGameList = []
        for i in range(removeTupleAmount, numberOfGames):
            newGameList.append(curGameList[i][1])

        index = calculateHdcpList(newGameList)
        return index


# def calculateBeforeCurrentGameHdcp(aOrdinalDate, golferID, curGameID, curGameNumber):
#     curGameList = get_games_hdcp_diff_before_date_for_golfer(aOrdinalDate, golferID, curGameID,
#                                                                         curGameNumber)  # organized, latest first
#     numberOfGames = len(curGameList)
#     if numberOfGames <= 1:
#         return float(0.0)
#     else:
#         howManyGamesToday = 1
#         removeTupleAmount = 0
#         for i in range(len(curGameList)):
#             if i == len(curGameList) - 1:
#                 break
#             elif curGameList[i][0] == curGameList[i + 1][0]:
#                 howManyGamesToday = howManyGamesToday + 1
#                 if curGameNumber == curGameList[i][2]:
#                     removeTupleAmount = i + 1
#             else:
#                 break
#
#         if howManyGamesToday > 1:
#             if removeTupleAmount == 0:
#                 removeTupleAmount = howManyGamesToday
#         else:
#             removeTupleAmount = 1
#
#         newGameList = []
#         for i in range(removeTupleAmount, numberOfGames):
#             newGameList.append(curGameList[i][1])
#
#         index = calculateHdcpList(newGameList)
#         return index


def calculateBeforeADateHdcp(aOrdinalDate, golferID, curGameID, curGameNumber):
    curGameList = get_games_hdcp_diff_before_date_for_golfer(aOrdinalDate, golferID, curGameID,
                                                                        curGameNumber)  # organized, latest first
    numberOfGames = len(curGameList)
    hdcpGameList = []
    if numberOfGames == 0:
        return float(0.0)
    else:
        if numberOfGames >= 20:
            hdcpNumberOfGames = 20
        else:
            hdcpNumberOfGames = numberOfGames
        for i in range(hdcpNumberOfGames):
            hdcpGameList.append(float(curGameList[i][1]))  # add the remaining hcdpDiff
        hdcpGameList.sort()  # make a lsit of all hdcp diff and sort them (lowest first)
        # The following if structure calculate based on the number of games played.
        if len(hdcpGameList) >= 20:
            newHdcp = float(0.0)
            for i in range(10):
                newHdcp = newHdcp + float(hdcpGameList[i])
            newHdcp = newHdcp / float(10)
        # return"%.1f" % newHdcp
        elif len(hdcpGameList) == 19:
            newHdcp = float(0.0)
            for i in range(9):
                newHdcp = newHdcp + float(hdcpGameList[i])
            newHdcp = newHdcp / float(9)
        # return"%.1f" % newHdcp
        elif len(hdcpGameList) == 18:
            newHdcp = float(0.0)
            for i in range(8):
                newHdcp = newHdcp + float(hdcpGameList[i])
            newHdcp = newHdcp / float(8)
        # return"%.1f" % newHdcp
        elif len(hdcpGameList) == 17:
            newHdcp = float(0.0)
            for i in range(7):
                newHdcp = newHdcp + float(hdcpGameList[i])
            newHdcp = newHdcp / float(7)
        # return"%.1f" % newHdcp
        elif (len(hdcpGameList) == 15) or (len(hdcpGameList) == 16):
            newHdcp = float(0.0)
            for i in range(6):
                newHdcp = newHdcp + float(hdcpGameList[i])
            newHdcp = newHdcp / float(6)
        # return"%.1f" % newHdcp
        elif (len(hdcpGameList) == 13) or (len(hdcpGameList) == 14):
            newHdcp = float(0.0)
            for i in range(5):
                newHdcp = newHdcp + float(hdcpGameList[i])
            newHdcp = newHdcp / float(5)
        # return"%.1f" % newHdcp
        elif (len(hdcpGameList) == 11) or (len(hdcpGameList) == 12):
            newHdcp = float(0.0)
            for i in range(4):
                newHdcp = newHdcp + float(hdcpGameList[i])
            newHdcp = newHdcp / float(4)
        # return"%.1f" % newHdcp
        elif (len(hdcpGameList) == 9) or (len(hdcpGameList) == 10):
            newHdcp = float(0.0)
            for i in range(3):
                newHdcp = newHdcp + float(hdcpGameList[i])
            newHdcp = newHdcp / float(3)
        # return"%.1f" % newHdcp
        elif (len(hdcpGameList) == 7) or (len(hdcpGameList) == 8):
            newHdcp = float(0.0)
            for i in range(2):
                newHdcp = newHdcp + float(hdcpGameList[i])
            newHdcp = newHdcp / float(2)
        else:
            newHdcp = float(hdcpGameList[0])
        newHdcp = newHdcp * float(0.98)
        return "%.1f" % newHdcp

def calculateIdxForDateRange(golferID, startDate, endDate):
    # use ordinal dates
    curGameList = get_games_forIdx_RangeDate_for_golfer(golferID, startDate,
                                                                   endDate)  # organized, latest first
    numberOfGames = len(curGameList)
    if numberOfGames == 0:
        return 0.0
    else:
        newGameList = []
        for i in range(numberOfGames):
            newGameList.append(curGameList[i][1])

        index = calculateHdcpList(newGameList)
        return index

def calculateCurrentHdcp(golferID):
    curGameList = get_games_hdcp_diff_for_golfer(golferID)  # organized, latest first
    numberOfGames = len(curGameList)
    if numberOfGames == 0:
        return 0.0
    else:
        newGameList = []
        for i in range(numberOfGames):
            newGameList.append(curGameList[i][1])

        index = calculateHdcpList(newGameList)
        return index

def get_game_details():
    AppCRUD.initDB(getDBLocationFullName())
    query = '''select * from GameDetails'''
    return AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()

def delete_game_details_for_gameID(gameID, NoClose=False):
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                delete from GameDetails
                where gameID = {0}
            ''' .format(gameID)
    AppCRUD.cursor.execute(query)

    if NoClose == False:
        AppCRUD.closeDB()

def get_gameDetailsIDs_for_Game(gameID, NoClose=False):
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                select uniqueID from GameDetails where gameID={0}
            '''.format(gameID)

    ans = AppCRUD.cursor.execute(query).fetchall()

    if NoClose == False:
        AppCRUD.closeDB()

    return ans

def get_game_details_for_game(gameID, NoClose = False):
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''select * from GameDetails where gameID={0}'''.format(gameID)
    ans = AppCRUD.cursor.execute(query).fetchall()

    if NoClose == False:
        AppCRUD.closeDB()
    return ans

def get_gameDetailsID_for_game(gameID, NoClose = False):

    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                select uniqueID, holeNumber from GameDetails
                where gameID={0}
                ORDER BY holeNumber
            '''.format(gameID)
    ans = AppCRUD.cursor.execute(query).fetchall()

    if NoClose == False:
        AppCRUD.closeDB()

    return ans

def update_game_details_for_gameDetailsID(uniqueID, holeNumber,holeScore,holePutts,holeDrives,
                                          holeSandTraps, updaterID, NoClose=False):

    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                update GameDetails
                set holeNumber=?, holeScore=?, holePutts=?, holeDrives=?,
                    holeSandTraps=?, updaterID=?, lastModified=?
                where uniqueID=?
            '''
    AppCRUD.cursor.execute(query, (holeNumber,holeScore,holePutts, holeDrives,
                                   holeSandTraps, updaterID,
                                   convertDateStringToOrdinal(todayDate),uniqueID))

    if NoClose == False:
        AppCRUD.closeDB()
#
#  Field holeDrives contains a distance 3 Char, a R or L
#  Fairways are deduced from there
#  Putts deduces the greens and
#  Sand saves are deduced from sandtraps
#
# cursor.execute('''CREATE TABLE GameDetails(
#    uniqueID       INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
#    holeNumber     SMALLINT    NOT NULL,
#    holeScore      SMALLINT    NOT NULL,
#    holePutts      SMALLINT,
#    holeDrives     CHAR(4),
#    holeSandTraps  CHAR(1),
#    holeWater      CHAR(1),
#    gameID         INTEGER     NOT NULL,
#    deleted        CHAR(1)     NOT NULL,
#    lastModified   INTEGER,
#    updaterID      INTEGER,
#    FOREIGN KEY(gameID) REFERENCES Games(uniqueID)
#    );''')


def insert_game_details(holeNumber,holeScore,holePutts,holeDrives,holeSandTraps,
                        gameID, updaterID, NoClose=False):
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                insert into GameDetails (holeNumber, holeScore, holePutts, holeDrives, holeSandTraps,
                                         gameID, deleted, lastModified, updaterID)
                values (?, ?, ?, ?, ?, ?, ?, ?, ?)
            '''
    AppCRUD.cursor.execute(query, (holeNumber,holeScore,holePutts,holeDrives,holeSandTraps,
                                   gameID, 'N', convertDateStringToOrdinal(todayDate), updaterID))

    if NoClose == False:
        AppCRUD.closeDB()

def exportCSV_gameDetails(fileName):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''select * from GameDetails'''
    data = AppCRUD.cursor.execute(query).fetchall()
    file = AppDefaultLocation + '/' + BackupDirDefaultName +'/'+ fileName
    with open(file, 'w', newline='', encoding='ISO-8859-1') as fout:
        for i in range(len(data)):
            csv.writer(fout).writerow(data[i])
    AppCRUD.closeDB()
    return len(data)

# def importCSV_gameDetailsUpgradeNov2016(fileName):
#     AppCRUD.initDB(getDBLocationFullName())
#     file = AppDefaultLocation + '/' + BackupDirDefaultName + '/' + fileName
#     with open(file, 'r', encoding='ISO-8859-1') as fin:
#         reader = csv.reader(fin)
#         for row in reader:
#             # row[4] unused anymore
#             # row[5] and row[6] are combined into one
#             if row[5] == 'Y':
#                 drives = row[6]
#             else:
#                 drives = row[5]
#             if row[7] == 'Y':
#                 sandTraps = 'Y'
#             elif row[7] == 'N':
#                 sandTraps = 'Y'
#             else:
#                 sandTraps = 'N'
#             query = '''
#             insert into GameDetails (uniqueID, holeNumber, holeScore, holePutts, holeDrives,
#                                      holeSandTraps, gameID, deleted, lastModified, updaterID )
#             values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'''
#             AppCRUD.cursor.execute(query, (row[0],row[1],row[2],row[3],drives, sandTraps, row[8], row[9], row[10],
#                                            row[11]))
#     AppCRUD.closeDB()
#     return len(get_game_details())
            
def importCSV_gameDetails(fileName):
    AppCRUD.initDB(getDBLocationFullName())
    file = AppDefaultLocation + '/' + BackupDirDefaultName + '/' + fileName
    with open(file, 'r', encoding='ISO-8859-1') as fin:
        reader = csv.reader(fin)
        for row in reader:
            # row[5] must be a valid courseID
            query = '''
                        insert into GameDetails (uniqueID, holeNumber, holeScore, holePutts, holeDrives,
                                                 holeSandTraps, holeWater, gameID, deleted, lastModified, updaterID )
                        values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
                    '''
            AppCRUD.cursor.execute(query, (row[0],row[1],row[2],row[3],row[4], row[5], row[6], row[7], row[8],
                                           row[9], row[10]))
    AppCRUD.closeDB()
    return len(get_game_details())

# def importGameDetailsCSV_MyGolf2015(fileName):
#     AppCRUD.initDB(getDBLocationFullName())
#     with open(fileName, newline='', encoding='ISO-8859-1') as fin:
#         reader = csv.reader(fin)
#         for row in reader:
#             query = '''
#                insert into GameDetails (uniqueID,holeNumber,holeScore ,holePutts,holeGreens,holeFairways,
#                                         holeDriveDistance,holeSands,gameID, deleted, lastModified,updaterID )
#
#                            values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
#                                   )'''
#             AppCRUD.cursor.execute(query, (row[0], row[1], row[2], row[3], row[4], row[5], row[6],row[7],row[8],
#                                            'N', convertDateStringToOrdinal(todayDate), '1'))
#     AppCRUD.closeDB()
#     return len(get_game_details())

#
#  Field holeDrives contains a distance 3 Char, a R or L
#  Fairways are deduced from there
#  Putts deduces the greens and
#  Sand saves are deduced from sandtraps
#
# cursor.execute('''CREATE TABLE GameDetails(
#    uniqueID       INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
#    holeNumber     SMALLINT    NOT NULL,
#    holeScore      SMALLINT    NOT NULL,
#    holePutts      SMALLINT,
#    holeDrives     CHAR(4),
#    holeSandTraps  CHAR(1),
#    holeWater      CHAR(1),
#    gameID         INTEGER     NOT NULL,
#    deleted        CHAR(1)     NOT NULL,
#    lastModified   INTEGER,
#    updaterID      INTEGER,
#    FOREIGN KEY(gameID) REFERENCES Games(uniqueID)
#    );''')

# field avgDrv has been changed to total driving yards
# cursor.execute('''CREATE TABLE Games(
#    uniqueID          INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
#    gameDate        CHAR(15)    NOT NULL,
#    gameNumber      SMALLINT    NOT NULL,
#    teeID           SMALLINT    NOT NULL,
#    golferID        SMALLINT    NOT NULL,
#    frontScore      SMALLINT,
#    backScore       SMALLINT,
#    grossScore      SMALLINT,
#    hdcpDiff        REAL,
#    dEagleCNT       SMALLINT,
#    eagleCNT        SMALLINT,
#    birdieCNT       SMALLINT,
#    parCNT          SMALLINT,
#    bogieCNT        SMALLINT,
#    dBogieCNT       SMALLINT,
#    tBogieCNT       SMALLINT,
#    othersCNT       SMALLINT,
#    greens          SMALLINT,
#    fairways        SMALLINT,
#    leftMiss        SMALLINT,
#    rightMiss       SMALLINT,
#    avgDrive        SMALLINT,
#    totalSands      SMALLINT,
#    sandSaves       SMALLINT,
#    gameRating      REAL,
#    gameSlope       SMALLINT,
#    holesPlayed     CHAR(7),
#    totalPutts      SMALLINT,
#    putts3AndUp     SMALLINT,
#    totalYard       SMALLINT,
#    courseID        SMALLINT,
#    deleted         CHAR(1)     NOT NULL,
#    lastModified         INTEGER,
#    updaterID            INTEGER,
#    FOREIGN KEY(golferID) REFERENCES Golfers(uniqueID),
#    FOREIGN KEY(teeID) REFERENCES Tees(uniqueID) ON DELETE CASCADE
#    );''')

def get_UpdaterID_From_GameID(gameID, NoClose = False):
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                select updaterID from Games where uniqueID = {0}
            '''.format(gameID)

    ans = AppCRUD.cursor.execute(query).fetchall()

    if NoClose == False:
        AppCRUD.closeDB()

    if len(ans) > 0:
        return ans[0][0]
    else:
        return None

def get_HolesPlayed_From_GameID(gameID, NoClose = False):
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                select holesPlayed from Games where uniqueID = {0}
            '''.format(gameID)

    ans = AppCRUD.cursor.execute(query).fetchall()
    if NoClose == False:
        AppCRUD.closeDB()

    if len(ans) > 0:
        return ans[0][0]
    else:
        return None

def get_RoundNumber_From_GameID(gameID, NoClose = False):
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                select gameNumber from Games where uniqueID = {0}
            '''.format(gameID)

    ans = AppCRUD.cursor.execute(query).fetchall()

    if NoClose == False:
        AppCRUD.closeDB()

    if len(ans) > 0:
        return ans[0][0]
    else:
        return None

def get_GameDateOrdinal_From_GameID(gameID, NoClose = False):
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                select gameDate from Games where uniqueID = {0}
            '''.format(gameID)

    ans = AppCRUD.cursor.execute(query).fetchall()

    if NoClose == False:
        AppCRUD.closeDB()

    if len(ans) > 0:
        return ans[0][0]
    else:
        return None

def get_GolferID_From_GameID(gameID, NoClose = False):
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                select golferID from Games where uniqueID = {0}
            '''.format(gameID)

    ans = AppCRUD.cursor.execute(query).fetchall()

    if NoClose == False:
        AppCRUD.closeDB()

    if len(ans) > 0:
        return ans[0][0]
    else:
        return None

def get_CourseID_From_GameID(gameID, NoClose=False):
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                select courseID from Games where uniqueID = {0}
            '''.format(gameID)

    ans = AppCRUD.cursor.execute(query).fetchall()

    if NoClose == False:
        AppCRUD.closeDB()

    if len(ans) > 0:
        return ans[0][0]
    else:
        return None

def get_teeID_From_GameID(gameID, NoClose=False):
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                select teeID from Games where uniqueID = {0}
            '''.format(gameID)

    ans = AppCRUD.cursor.execute(query).fetchall()

    if NoClose == False:
        AppCRUD.closeDB()

    if len(ans) > 0:
        return ans[0][0]
    else:
        return None

def get_games():
    AppCRUD.initDB(getDBLocationFullName())
    query = '''select * from Games ORDER BY gameDate'''
    ans =  AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()
    return ans

def delete_game_for_gameID(uniqueID, NoClose = False):
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                delete from Games
                where uniqueID = {0}
            '''.format(uniqueID)

    AppCRUD.cursor.execute(query)

    if NoClose == False:
        AppCRUD.closeDB()

def get_game_info_for_gameID(uniqueID):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''select * from Games where uniqueID = {0} ORDER BY gameDate'''.format(uniqueID)
    ans = AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()
    return ans

def get_games_forIdx_RangeDate_for_golfer(golferID, ordinalStartDate, ordinalEndDate):
    # latest played game first
    AppCRUD.initDB(getDBLocationFullName())
    query = '''select gameDate, hdcpDiff, holesPlayed from Games where
             golferID = {0} and holesPlayed = 'All' and gameDate>={1} and gameDate<={2} ORDER BY gameDate DESC'''.format(golferID, ordinalStartDate, ordinalEndDate)
    ans =  AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()
    return ans

def get_gamesID_for_GolferID(golferID, NoClose=False):
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                select uniqueID from Games where golferID = {0}
            '''.format(golferID)
    ans = AppCRUD.cursor.execute(query).fetchall()

    if NoClose == False:
        AppCRUD.closeDB()

    return ans

def get_games_hdcp_diff_before_date_for_golfer(aOrdinalDate, golferID, curGameID, curGameNum, NoClose=False):
    # All games before a date
#    query = '''select gameDate, hdcpDiff, gameNumber from Games where golferID = {0} and gameDate <= {1} and gameID != {2} and gameNumber < {3} ORDER BY gameDate DESC'''.format(golferID, aOrdinalDate, curGameID, curGameNum)

    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''select gameDate, hdcpDiff, gameNumber, holesPlayed from Games where golferID = {0} and gameDate <= {1} and holesPlayed = 'All'
               and deleted = 'N' ORDER BY gameDate DESC, gameNumber DESC'''.format(golferID, aOrdinalDate, curGameID)
               
#    query = '''select gameDate, hdcpDiff, gameNumber from Games where golferID = {0} and gameDate <= {1} and gameID != {2} 
#               ORDER BY gameDate DESC, gameNumber DESC'''.format(golferID, aOrdinalDate, curGameID, curGameNum)
    ans = AppCRUD.cursor.execute(query).fetchall()

    if NoClose == False:
        AppCRUD.closeDB()
    return ans


def get_games_hdcp_diff_before_date_for_golferMASS(aOrdinalDate, golferID, curGameID, curGameNum):
    # All games before a date
    #    query = '''select gameDate, hdcpDiff, gameNumber from Games where golferID = {0} and gameDate <= {1} and gameID != {2} and gameNumber < {3} ORDER BY gameDate DESC'''.format(golferID, aOrdinalDate, curGameID, curGameNum)

    query = '''select gameDate, hdcpDiff, gameNumber, holesPlayed from Games where golferID = {0} and gameDate <= {1} and holesPlayed = 'All'
               ORDER BY gameDate DESC, gameNumber DESC'''.format(golferID, aOrdinalDate, curGameID)

    #    query = '''select gameDate, hdcpDiff, gameNumber from Games where golferID = {0} and gameDate <= {1} and gameID != {2}
    #               ORDER BY gameDate DESC, gameNumber DESC'''.format(golferID, aOrdinalDate, curGameID, curGameNum)
    return AppCRUD.cursor.execute(query).fetchall()

def get_games_hdcp_diff_for_golfer(golferID):
    # latest played game first
    AppCRUD.initDB(getDBLocationFullName())
    query = '''
                select gameDate, hdcpDiff, holesPlayed from Games where golferID = {0} and holesPlayed = 'All'
                and deleted = 'N' ORDER BY gameDate DESC
            '''.format(golferID)
    ans = AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()
    return ans

def get_18hole_games_for_golfer(golferID):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''select * from Games where golferID = {0} and holesPlayed = 'All' ORDER BY gameDate DESC, gameNumber DESC'''.format(golferID)
    ans = AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()
    return ans

def get_18hole_games_for_golfer_on_course_dateRange(golferID, courseID, startOrdinal, endOrdinal):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''select * from Games where golferID = {0} and courseID={1} and gameDate >= {2} and gameDate <= {3} and holesPlayed = 'All'  
               ORDER BY gameDate DESC, gameNumber DESC'''.format(golferID, courseID, startOrdinal, endOrdinal)
    ans = AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()
    return ans

def get_18hole_games_for_golfer_dateRange(golferID, startOrdinal, endOrdinal):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''select * from Games where golferID = {0} and gameDate >= {1} and gameDate <= {2} and holesPlayed = 'All' 
               ORDER BY gameDate DESC, gameNumber DESC'''.format(golferID, startOrdinal, endOrdinal)
    ans = AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()
    return ans

def get_18hole_games_for_golfer_on_course(golferID, courseID):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''select * from Games where golferID = {0} and courseID={1} and holesPlayed = 'All' ORDER BY gameDate DESC, gameNumber DESC'''.format(golferID, courseID)
    ans = AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()
    return ans

def get_games_for_golfer_on_course_dateRange(golferID, courseID, startOrdinal, endOrdinal):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''select * from Games where golferID = {0} and courseID={1} and gameDate >= {2} and gameDate <= {3} 
               ORDER BY gameDate DESC, gameNumber DESC'''.format(golferID, courseID, startOrdinal, endOrdinal)
    ans = AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()
    return ans

def get_games_for_golfer_dateRange(golferID, startOrdinal, endOrdinal):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''select * from Games where golferID = {0} and gameDate >= {1} and gameDate <= {2} 
               ORDER BY gameDate DESC, gameNumber DESC'''.format(golferID, startOrdinal, endOrdinal)
    ans = AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()
    return ans

def get_games_for_golfer_on_course(golferID, courseID):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''select * from Games where golferID = {0} and courseID={1} ORDER BY gameDate DESC, gameNumber DESC'''.format(golferID, courseID)
    ans = AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()
    return ans

def get_games_for_golfer(golferID, NoClose):
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''select * from Games where golferID = {0} ORDER BY gameDate DESC, gameNumber DESC'''.format(golferID)
    ans =  AppCRUD.cursor.execute(query).fetchall()

    if NoClose == False:
        AppCRUD.closeDB()

    return ans

def get_a_roundNum_today_for_golfer(golferID, ordinalDate, roundNum, NoClose=False):
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                select * from Games where golferID = {0} and gameDate = {1} and gameNumber = {2}
                ORDER BY gameDate DESC, gameNumber DESC
            '''.format(golferID, ordinalDate, roundNum)
    ans = AppCRUD.cursor.execute(query).fetchall()

    if NoClose == False:
        AppCRUD.closeDB()
    return ans

def get_next_roundNum_today_for_golfer(golferID, ordinalDate, NoClose=False):
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                select gameNumber from Games where golferID = {0} and gameDate = {1}
                ORDER BY gameNumber DESC
            '''.format(golferID, ordinalDate)
    ans = AppCRUD.cursor.execute(query).fetchall()

    if NoClose == False:
        AppCRUD.closeDB()

    if len(ans) > 0:
        nextRoundNumber = ans[0][0] + 1
        return nextRoundNumber
    else:
        return 1

def get_games_played_today_for_golfer(golferID, ordinalDate, NoClose=False):
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''select * from Games where golferID = {0} and gameDate = {1} ORDER BY gameDate DESC, gameNumber DESC'''.format(golferID, ordinalDate)
    ans = AppCRUD.cursor.execute(query).fetchall()

    if NoClose == False:
        AppCRUD.closeDB()
    return ans

def get_games_for_period_golfer(golferID, startDate, endDate, NoClose = False):
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                select * from Games where golferID = {0} and gamedate >= {1} and gameDate <= {2} ORDER BY holesPlayed DESC
            '''.format(golferID, startDate,endDate)
    ans =  AppCRUD.cursor.execute(query).fetchall()

    if NoClose ==False:
        AppCRUD.closeDB()

    return ans

def find_gamePlayedDay(gameDate, gameNum, golferID):
#    query ='''select gameID from Games where gameDate = {0} and gameNumber = {1} and teeID = {2} and golferID = {3}'''.format(gameDate, gameNumber, teeID, golferID)
    AppCRUD.initDB(getDBLocationFullName())
    query ='''select uniqueID, gameNumber, holesPlayed from Games where gameDate = {0} and gameNumber = {1} and golferID = {2}'''.format(gameDate, gameNum, golferID)
    ans = AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()
    return ans

def get_gameID(gameDate, gameNumber, teeID, golferID):
#    query ='''select gameID from Games where gameDate = {0} and gameNumber = {1} and teeID = {2} and golferID = {3}'''.format(gameDate, gameNumber, teeID, golferID)
    AppCRUD.initDB(getDBLocationFullName())
    query ='''select uniqueID from Games where gameDate = "{0}" and gameNumber = {1} and teeID = {2} and golferID = {3}'''.format(gameDate, gameNumber, teeID, golferID)
    ans =  AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()
    return ans


# field avgDrv has been changed to total driving yards
# field totalYard is the tee distance
# cursor.execute('''CREATE TABLE Games(
#    uniqueID          INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
#    gameDate        CHAR(15)    NOT NULL,
#    gameNumber      SMALLINT    NOT NULL,
#    teeID           SMALLINT    NOT NULL,
#    golferID        SMALLINT    NOT NULL,
#    frontScore      SMALLINT,
#    backScore       SMALLINT,
#    grossScore      SMALLINT,
#    hdcpDiff        REAL,
#    dEagleCNT       SMALLINT,
#    eagleCNT        SMALLINT,
#    birdieCNT       SMALLINT,
#    parCNT          SMALLINT,
#    bogieCNT        SMALLINT,
#    dBogieCNT       SMALLINT,
#    tBogieCNT       SMALLINT,
#    othersCNT       SMALLINT,
#    greens          SMALLINT,
#    fairways        SMALLINT,
#    leftMiss        SMALLINT,
#    rightMiss       SMALLINT,
#    avgDrive        SMALLINT,
#    totalSands      SMALLINT,
#    sandSaves       SMALLINT,
#    gameRating      REAL,
#    gameSlope       SMALLINT,
#    holesPlayed     CHAR(7),
#    totalPutts      SMALLINT,
#    putts3AndUp     SMALLINT,
#    totalYard       SMALLINT,
#    courseID        SMALLINT,
#    deleted         CHAR(1)     NOT NULL,
#    lastModified         INTEGER,
#    updaterID            INTEGER,
#    FOREIGN KEY(golferID) REFERENCES Golfers(uniqueID),
#    FOREIGN KEY(teeID) REFERENCES Tees(uniqueID) ON DELETE CASCADE
#    );''')


def findDuplicateRoundNumbertodayFromEdit(golferID, gameDateOrdinal, roundNumber, gameID, NoClose=False):
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                select uniqueID from Games where golferID = {0} and gameDate = {1} and
                                                 gameNumber = {2} and uniqueID != {3}
            '''.format(golferID, gameDateOrdinal, roundNumber, gameID)

    ans = AppCRUD.cursor.execute(query).fetchall()

    if NoClose == False:
        AppCRUD.closeDB()

    if len(ans) > 0:
        return True
    else:
        return False

def findDuplicateRoundNumbertoday(golferID, gameDateOrdinal, roundNumber, NoClose=False):
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                select uniqueID from Games where golferID = {0} and gameDate = {1} and
                                                 gameNumber = {2}
            '''.format(golferID, gameDateOrdinal, roundNumber)

    ans = AppCRUD.cursor.execute(query).fetchall()

    if NoClose == False:
        AppCRUD.closeDB()

    if len(ans) > 0:
        return True
    else:
        return False

def insert_games(gameDate,gameNumber,teeID,golferID,frontScore,backScore,grossScore,
                 hdcpDiff,dEagleCNT,eagleCNT,birdieCNT,parCNT,bogieCNT,dBogieCNT,tBogieCNT,othersCNT,
                 greens,fairways,leftMiss,rightMiss,totalDrvYards,totalSands,sandSaves,gameRating,gameSlope,
                 holesPlayed,totalPutts,putts3AndUp,totalTeeYards,courseID, updaterID,NoClose=False):
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                insert into Games(gameDate,gameNumber,teeID,golferID,frontScore,backScore,grossScore,
                                  hdcpDiff,dEagleCNT,eagleCNT,birdieCNT,parCNT,bogieCNT,dBogieCNT,tBogieCNT,othersCNT,
                                  greens,fairways,leftMiss,rightMiss,avgDrive,totalSands,sandSaves,gameRating,gameSlope,
                                  holesPlayed,totalPutts,putts3AndUp,totalYard,courseID, deleted, lastModified, updaterID)
                values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
                        ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            '''

    AppCRUD.cursor.execute(query, (gameDate,gameNumber,teeID,golferID,frontScore,backScore,grossScore,
                                   hdcpDiff,dEagleCNT,eagleCNT,birdieCNT,parCNT,bogieCNT,dBogieCNT,tBogieCNT,othersCNT,
                                   greens,fairways,leftMiss,rightMiss,totalDrvYards,totalSands,sandSaves,gameRating,gameSlope,
                                   holesPlayed,totalPutts,putts3AndUp,totalTeeYards,courseID, 'N',
                                   convertDateStringToOrdinal(todayDate), updaterID))

    query = '''
                select uniqueID from Games
                ORDER BY uniqueID
            '''
    ans = AppCRUD.cursor.execute(query).fetchall()
    lastID = ans[len(ans)-1][0]

    if NoClose == False:
        AppCRUD.closeDB()

    return lastID


def update_game_coursID(uniqueID, courseID):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''update Games set courseID={0} where uniqueID = {1}'''.format(courseID, uniqueID)
    AppCRUD.cursor.execute(query)
    AppCRUD.closeDB()

def update_Games_Rating_Slope(uniqueID, gameRating, gameSlope):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''update Games set gameRating={0}, gameSlope={1}  where uniqueID = {2}'''.format(gameRating, gameSlope, uniqueID)
    AppCRUD.cursor.execute(query)
    
def update_Games_HdcpDiff(uniqueID, gameHdcpDiff):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''update Games set hdcpDiff={0} where uniqueID = {1}'''.format(gameHdcpDiff, uniqueID)
    AppCRUD.cursor.execute(query)
    AppCRUD.closeDB()


# cursor.execute('''CREATE TABLE Games(
#    uniqueID          INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
#    gameDate        CHAR(15)    NOT NULL,
#    gameNumber      SMALLINT    NOT NULL,
#    teeID           INTEGER    NOT NULL,
#    golferID        INTEGER    NOT NULL,
#    frontScore      SMALLINT,
#    backScore       SMALLINT,
#    grossScore      SMALLINT,
#    hdcpDiff        REAL,
#    dEagleCNT       SMALLINT,
#    eagleCNT        SMALLINT,
#    birdieCNT       SMALLINT,
#    parCNT          SMALLINT,
#    bogieCNT        SMALLINT,
#    dBogieCNT       SMALLINT,
#    tBogieCNT       SMALLINT,
#    othersCNT       SMALLINT,
#    greens          SMALLINT,
#    fairways        SMALLINT,
#    leftMiss        SMALLINT,
#    rightMiss       SMALLINT,
#    avgDrive        SMALLINT,
#    totalSands      SMALLINT,
#    sandSaves       SMALLINT,
#    gameRating      REAL,
#    gameSlope       SMALLINT,
#    holesPlayed     CHAR(7),
#    totalPutts      SMALLINT,
#    putts3AndUp     SMALLINT,
#    totalYard       SMALLINT,
#    courseID        INTEGER,
#    deleted         CHAR(1)     NOT NULL,
#    lastModified         INTEGER,
#    updaterID            INTEGER,
#    FOREIGN KEY(golferID) REFERENCES Golfers(uniqueID),
#    FOREIGN KEY(teeID) REFERENCES Tees(uniqueID)
#    );''')

def update_game(uniqueID, gameDate,gameNumber,teeID,golferID,frontScore,backScore,grossScore,
                 hdcpDiff,dEagleCNT,eagleCNT,birdieCNT,parCNT,bogieCNT,dBogieCNT,tBogieCNT,othersCNT,
                 greens,fairways,leftMiss,rightMiss,avgDrive,totalSands,sandSaves,gameRating,gameSlope,
                 holesPlayed,totalPutts,putts3AndUp, totalYard,courseID, updaterID, NoClose=False):

    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                update Games set gameDate={1},gameNumber={2},teeID={3},golferID={4},frontScore={5},
                                 backScore={6},grossScore={7},hdcpDiff={8},dEagleCNT={9},eagleCNT={10},
                                 birdieCNT={11},parCNT={12},bogieCNT={13},dBogieCNT={14},tBogieCNT={15},
                                 othersCNT={16},greens={17},fairways={18},leftMiss={19},rightMiss={20},
                                 avgDrive={21},totalSands={22},sandSaves={23},gameRating={24},gameSlope={25},
                                 holesPlayed="{26}",totalPutts={27},putts3AndUp={28},totalYard={29},courseID={30},
                                 lastModified={31},
                                 updaterID={32}
                where uniqueID = {0}
            '''.format(uniqueID, gameDate,gameNumber,teeID,golferID, frontScore,backScore,grossScore,hdcpDiff,dEagleCNT,
                       eagleCNT,birdieCNT,parCNT,bogieCNT,dBogieCNT,tBogieCNT, othersCNT,greens,fairways,
                       leftMiss,rightMiss,avgDrive, totalSands,sandSaves,gameRating,gameSlope,holesPlayed,
                       totalPutts,putts3AndUp,totalYard,courseID, convertDateStringToOrdinal(todayDate),
                       updaterID)

    AppCRUD.cursor.execute(query)

    if NoClose == False:
        AppCRUD.closeDB()
       
def exportCSV_games(fileName):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''select * from Games'''
    data = AppCRUD.cursor.execute(query).fetchall()
    file = AppDefaultLocation + '/' + BackupDirDefaultName +'/'+ fileName
    with open(file, 'w', newline='', encoding='ISO-8859-1') as fout:
        for i in range(len(data)):
            csv.writer(fout).writerow(data[i])
    AppCRUD.closeDB()
    return len(data)

def importCSV_games(fileName):
    AppCRUD.initDB(getDBLocationFullName())
    file = AppDefaultLocation + '/' + BackupDirDefaultName + '/' + fileName
    with open(file, 'r', encoding='ISO-8859-1') as fin:
        reader = csv.reader(fin)
        for row in reader:
            # row[5] must be a valid courseID
            query = '''
            insert into Games (uniqueID, gameDate, gameNumber, teeID, golferID, frontScore, backScore, grossScore,
                               hdcpDiff, dEagleCNT, eagleCNT, birdieCNT, parCNT, bogieCNT, dBogieCNT, tBogieCNT, 
                               othersCNT, greens, fairways, leftMiss, rightMiss, avgDrive, totalSands, sandSaves, 
                               gameRating, gameSlope, holesPlayed,totalPutts,putts3AndUp, totalYard, courseID,
                               deleted, lastModified,updaterID)
            values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'''
            AppCRUD.cursor.execute(query, (row[0],row[1],row[2],row[3],row[4], row[5], row[6], row[7], row[8], row[9],
                                   row[10],row[11],row[12],row[13],row[14], row[15], row[16], row[17], row[18], row[19],
                                   row[20],row[21],row[22],row[23],row[24], row[25], row[26], row[27], row[28], row[29],
                                   row[30], row[31], row[32], row[33]))
    AppCRUD.closeDB()
    return len(get_games())
   
def importGamesCSV_MyGolf2015(fileName):
    AppCRUD.initDB(getDBLocationFullName())
    with open(fileName, newline='', encoding='ISO-8859-1') as fin:
        reader = csv.reader(fin)
        for row in reader:
            query = '''
               insert into Games ( uniqueID,gameDate,gameNumber,teeID,golferID,frontScore,backScore,
                                     grossScore,hdcpDiff,dEagleCNT,eagleCNT,birdieCNT,parCNT,bogieCNT,
                                     dBogieCNT,tBogieCNT,othersCNT,greens,fairways,leftMiss,rightMiss,
                                     avgDrive,totalSands,sandSaves,gameRating,gameSlope,holesPlayed,
                                     totalPutts,putts3AndUp,totalYard,courseID,deleted, lastModified,updaterID)

                           values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
                                   ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
                                   ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
                                   ?, ?, ?, ?
                                  )'''
            AppCRUD.cursor.execute(query, (row[0], row[1], row[2], row[3], row[4], row[5], row[6],row[7],row[8],row[9],
                                           row[10], row[11], row[12], row[13], row[14], row[15], row[16],row[17],row[18],row[19],
                                           row[20], row[21], row[22], row[23], row[24], row[25], row[26],row[27],row[28],row[29],
                                           row[30], 'N', convertDateStringToOrdinal(todayDate), '1'))
    AppCRUD.closeDB()
    return len(get_games())

