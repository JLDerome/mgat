import tkinter.tix as Tix
import tkinter.ttk as ttk
import textwrap

from tkinter.constants import *

from AppConstants import AppDefaultForeground, fontMonstrousB

from AppConstants import *

from AppProcFROMECE import *

from PIL import Image
from PIL.Image import NORMAL


tkinter_umlauts=['odiaeresis', 'adiaeresis', 'udiaeresis', 'Odiaeresis', 'Adiaeresis', 'Udiaeresis', 'ssharp']

class AutocompleteEntry(Tix.Entry):
        """
        Subclass of Tkinter.Entry that features autocompletion.

        To enable autocompletion use set_completion_list(list) to define
        a list of possible strings to hit.
        To cycle through hits use down and up arrow keys.
        """
        def set_completion_list(self, completion_list):
                self._completion_list = sorted(completion_list, key=str.lower) # Work with a sorted list
                self._hits = []
                self._hit_index = 0
                self.position = 0
                self.bind('<KeyRelease>', self.handle_keyrelease)

        def autocomplete(self, delta=0):
                """autocomplete the Entry, delta may be 0/1/-1 to cycle through possible hits"""
                if delta: # need to delete selection otherwise we would fix the current position
                        self.delete(self.position, END)
                else: # set position to end so selection starts where textentry ended
                        self.position = len(self.get())
                # collect hits
                _hits = []
                for element in self._completion_list:
                        if element.lower().startswith(self.get().lower()):  # Match case-insensitively
                                _hits.append(element)
                # if we have a new hit list, keep this in mind
                if _hits != self._hits:
                        self._hit_index = 0
                        self._hits=_hits
                # only allow cycling if we are in a known hit list
                if _hits == self._hits and self._hits:
                        self._hit_index = (self._hit_index + delta) % len(self._hits)
                # now finally perform the auto completion
                if self._hits:
                        self.delete(0,END)
                        self.insert(0,self._hits[self._hit_index])
                        self.select_range(self.position,END)

        def handle_keyrelease(self, event):
                """event handler for the keyrelease event on this widget"""
                if event.keysym == "BackSpace":
                        self.delete(self.index(INSERT), END)
                        self.position = self.index(END)
                if event.keysym == "Left":
                        if self.position < self.index(END): # delete the selection
                                self.delete(self.position, END)
                        else:
                                self.position = self.position-1 # delete one character
                                self.delete(self.position, END)
                if event.keysym == "Right":
                        self.position = self.index(END) # go to end (no selection)
                if event.keysym == "Down":
                        self.autocomplete(1) # cycle to next hit
                if event.keysym == "Up":
                        self.autocomplete(-1) # cycle to previous hit
                if len(event.keysym) == 1 or event.keysym in tkinter_umlauts:
                        self.autocomplete()

class AutocompleteCombobox(ttk.Combobox):
            
        def set_completion_list(self, completion_list):
                """Use our completion list as our drop down selection menu, arrows move through menu."""
                self._completion_list = sorted(completion_list, key=str.lower) # Work with a sorted list
                self._hits = []
                self._hit_index = 0
                self.position = 0
                self.bind('<KeyRelease>', self.handle_keyrelease)
                self['values'] = self._completion_list  # Setup our popup menu

        def autocomplete(self, delta=0):
                """autocomplete the Combobox, delta may be 0/1/-1 to cycle through possible hits"""
                if delta: # need to delete selection otherwise we would fix the current position
                        self.delete(self.position, END)
                else: # set position to end so selection starts where textentry ended
                        self.position = len(self.get())
                # collect hits
                _hits = []
                for element in self._completion_list:
                        if element.lower().startswith(self.get().lower()): # Match case insensitively
                                _hits.append(element)
                # if we have a new hit list, keep this in mind
                if _hits != self._hits:
                        self._hit_index = 0
                        self._hits=_hits
                # only allow cycling if we are in a known hit list
                if _hits == self._hits and self._hits:
                        self._hit_index = (self._hit_index + delta) % len(self._hits)
                # now finally perform the auto completion
                if self._hits:
                        self.delete(0,END)
                        self.insert(0,self._hits[self._hit_index])
                        self.select_range(self.position,END)

        def handle_keyrelease(self, event):
                """event handler for the keyrelease event on this widget"""
                if event.keysym == "BackSpace":
                        self.delete(self.index(INSERT), END)
                        self.position = self.index(END)
                if event.keysym == "Left":
                        if self.position < self.index(END): # delete the selection
                                self.delete(self.position, END)
                        else:
                                self.position = self.position-1 # delete one character
                                self.delete(self.position, END)
                if event.keysym == "Right":
                        self.position = self.index(END) # go to end (no selection)
                if len(event.keysym) == 1:
                        self.autocomplete()
                # No need for up/down, we'll jump to the popup
                # list at the position of the autocompletion

class AppLastUpdateFormat(Tix.Label):
    def __init__(self, parent, aDate, updaterID):
        Tix.Label.__init__(self,parent)
        aText = "Last Updated: {0} ({1})".format(convertOrdinaltoString(aDate), AppDB.getFullName(updaterID))
        self.config(text=aText, font=fontAverageI)
        
    def load(self, aDate, updaterID):
        aText = "Last Updated: {0} ({1})".format(convertOrdinaltoString(aDate), AppDB.getFullName(updaterID))
        self.config(text=aText, font=fontAverageI)

class AppSpaceRow(Tix.Label):
    def __init__(self, parent):
        Tix.Label.__init__(self,parent)
        self.config(text="    ", font=fontHuge)
              
class AppMENUListFrame(Tix.Frame):
    def __init__(self, parent, columnTotal):
        Tix.Frame.__init__(self, parent)
        self.config(border=4, relief='ridge', highlightbackground='red')
        self.row=0
        self.column=0
        self.columnTotal=columnTotal
        for i in range(self.columnTotal):
            self.columnconfigure(i, weight = 1)
        
class AppBorderFrame(Tix.Frame):
    def __init__(self, parent, columnTotal):
        Tix.Frame.__init__(self, parent)
        self.config(border=2, relief='ridge')
        self.row=0
        self.column=0
        self.columnTotal=columnTotal
        for i in range(self.columnTotal):
            self.columnconfigure(i, weight = 1)
        
class AppFrame(Tix.Frame):
    def __init__(self, parent, columnTotal):
        Tix.Frame.__init__(self, parent)
        self.config(border=2)
        self.row=0
        self.column=0
        self.columnTotal=columnTotal
        for i in range(self.columnTotal):
            self.columnconfigure(i, weight = 1)

class AppRootFrame(Tix.Frame):
    def __init__(self, parent, columnTotal):
        Tix.Frame.__init__(self, parent)
        self.config(border=3, relief='ridge')
        self.row=0
        self.column=0
        self.columnTotal=columnTotal
        for i in range(self.columnTotal):
            self.columnconfigure(i, weight = 1)

class AppMENUFrame(Tix.Frame):
    def __init__(self, parent, CommandText, selectedMethod, focusMethod):
        Tix.Frame.__init__(self, parent)
        original = Image.open(AppWelcomeImg)
        image1 = resizeImage(original,15,20)
        
        self.config(border=3, relief='flat', width=personnelPicWidth, height=personnelPicHeight,
                    highlightcolor=AppDefaultForeground, highlightthickness = 2, takefocus=1)        
        self.bind('<Enter>', widgetEnter)

        self.bind('<Button-1>', selectedMethod)
        self.bind('<Enter>', widgetEnter)
        self.bind('<Return>', selectedMethod)
#        
        panel1 = ttk.Label(self, image=image1)
        panel1.bind('<Button-1>', selectedMethod)
        panel1.bind('<Enter>', focusMethod)
        self.display = image1
        panel1.grid(row=0, column=0)
        aMsg = ttk.Label(self, text=CommandText, style='MenuItems.TLabel')
        aMsg.grid(row=0, column=1, sticky=N+W)
        aMsg.bind('<Button-1>', selectedMethod)
        aMsg.bind('<Enter>', focusMethod)


class AppPICFrame(Tix.Frame):
    def __init__(self, parent):
        Tix.Frame.__init__(self, parent)
        self.config(border=3, relief='ridge', width=personnelPicWidth, height=personnelPicHeight,
                    highlightcolor=AppDefaultForeground, highlightthickness = 2, takefocus=1)        
        self.bind('<Enter>', widgetEnter)
        
    def disable(self):
        self.config(takefocus=0)
        self.unbind('<Enter>')
        
    def enable(self):
        self.config(takefocus=1)
        self.bind('<Enter>', widgetEnter)

class AppVerticalFieldFrame(Tix.Frame):
    def __init__(self, parent,  header, aWidth):
        self.aWidth = aWidth
        Tix.Frame.__init__(self, parent)
        
        aLabel = ttk.Label(self, text='{0}  '.format(header), style='RegularFieldTitle.TLabel')
        aLabel.grid(row=0,column=0,sticky=N+S)
        
        self.anEntry = Tix.Entry(self,highlightcolor=AppDefaultForeground, highlightthickness = 2, width=aWidth, font=fontBig)
        self.anEntry.grid(row=1,column=0,sticky=N+S)

    def focus(self):
        self.anEntry.focus() 
 
    def disable(self):
        self.anEntry.config(state=DISABLED)
        self.anEntry.unbind('<Enter>')
        
    def clear(self):
        self.anEntry.config(state='normal')
        self.anEntry.delete(0, END)
        self.anEntry.config(state=DISABLED)
               
    def get(self):
        return self.anEntry.get()
        
    def load(self, aValue):
        self.anEntry.config(state='normal')
        self.anEntry.delete(0, END)
        self.anEntry.insert(0, aValue)
        self.anEntry.config(state=DISABLED)
       
    def enable(self):
        self.anEntry.config(state='normal')
        self.anEntry.bind('<Enter>', widgetEnter)

class AppHorizontalFieldFrame(Tix.Frame):
    def __init__(self, parent,  header, aWidth):
        Tix.Frame.__init__(self, parent)
        
        aLabel = ttk.Label(self, text='{0}: '.format(header), style='RegularFieldTitle.TLabel')
        aLabel.grid(row=0,column=0,sticky=E+N+S)
        
        self.anEntry = Tix.Entry(self,highlightcolor=AppDefaultForeground, highlightthickness = 2, width=aWidth, font=fontBig)
        self.anEntry.grid(row=0,column=1,sticky=W+N+S)
        
    def focus(self):
        self.anEntry.focus() 
        
    def disable(self):
        self.anEntry.config(state=DISABLED)
        self.anEntry.unbind('<Enter>')
  
    def clear(self):
        self.anEntry.config(state='normal')
        self.anEntry.delete(0, END)
        self.anEntry.config(state=DISABLED)
       
    def get(self):
        return self.anEntry.get()
        
    def load(self, aValue):
        self.anEntry.config(state='normal')
        self.anEntry.delete(0, END)
        self.anEntry.insert(0, aValue)
        self.anEntry.config(state=DISABLED)
       
    def enable(self):
        self.anEntry.config(state='normal')
        self.anEntry.bind('<Enter>', widgetEnter)

class AppEntry(Tix.Entry):
    def __init__(self, parent, fieldWidth):
        Tix.Entry.__init__(self, parent)
        self.config(highlightcolor=AppDefaultForeground, highlightthickness = 2, width=fieldWidth, font=fontBig)
        self.bind('<Enter>', widgetEnter)
        
    def disable(self):
        self.config(state=DISABLED)
        self.unbind('<Enter>')
        
    def clear(self):
        self.config(state='normal')
        self.delete(0, END)
        self.config(state=DISABLED)
        
    def load(self, aValue):
        self.config(state='normal')
        self.delete(0, END)
        self.insert(0, aValue)
        self.config(state=DISABLED)
       
    def enable(self):
        self.config(state='normal')
        self.bind('<Enter>', widgetEnter)


class AppEntryCentered(Tix.Entry):
    def __init__(self, parent, fieldWidth):
        Tix.Entry.__init__(self, parent)
        self.config(highlightcolor=AppDefaultForeground, highlightthickness = 2, width=fieldWidth, justify=CENTER, font=fontBig)
        self.bind('<Enter>', widgetEnter)
        
    def disable(self):
        self.config(state=DISABLED)
        self.unbind('<Enter>')
        
    def clear(self):
        self.config(state='normal')
        self.delete(0, END)
        self.config(state=DISABLED)
        
    def load(self, aValue):
        self.config(state='normal')
        self.delete(0, END)
        self.insert(0, aValue)
        self.config(state=DISABLED)
       
    def enable(self):
        self.config(state='normal')
        self.bind('<Enter>', widgetEnter)
        
class AppCBList():
    def __init__(self, aComboBoxData): 
        self.updateDictionaries(aComboBoxData)      
            
    def getList(self):
        aList = []  
        for i in range(len(self.aComboBoxData)):
            aList.append(self.aComboBoxData[i][1]) 
        return aList
    
    def getName(self,ID):
        return self.aReverseDictionary[ID]     

    def getID(self,Name):
        return(self.aDictionary[Name])
    
    def updateDictionaries(self, aComboBoxData):    
        self.aComboBoxData = aComboBoxData
        self.aDictionary = {}
        self.aReverseDictionary = {}
        for i in range(len(aComboBoxData)):
            self.aDictionary.update({aComboBoxData[i][1]:aComboBoxData[i][0]})
            self.aReverseDictionary.update({aComboBoxData[i][0]:aComboBoxData[i][1]})

class AppCB(Tix.Frame):
    def __init__(self, parent, aList, aWidth):
        Tix.Frame.__init__(self, parent)
        
        self.config(border = 2, highlightcolor=AppDefaultForeground, highlightthickness = 2, takefocus=0)
        self.bind('<Enter>', widgetEnter)
        self.CB = AutocompleteCombobox(self,width=aWidth, font=fontBig,justify=CENTER)
        self.CB.grid(row=0, column=0, sticky=N+S+E+W)
        self.CB.set_completion_list(aList)
        self.CB.bind("<ComboboxSelected>")
        self.CB.config(state=DISABLED)
        self.updateValuesList(aList)
        
    def focus(self):
        self.CB.focus()
        
    def get(self):
        return(self.CB.get())
 
    def clear(self):
        self.CB.config(state='normal')
        self.CB.delete(0, END)
        self.CB.config(state=DISABLED)
                  
    def load(self, aValue):
        self.CB.config(state=NORMAL)
        self.CB.delete(0, END)
        self.CB.insert(0, aValue)
        self.CB.config(state=DISABLED)
        
    def enable(self):
        self.CB.config(state=NORMAL, takefocus=1)
        self.bind('<Enter>', widgetEnter)
       
    def disable(self):
        self.CB.config(state=DISABLED, takefocus=0)
        self.unbind('<Enter>')
        
    def updateValuesList(self, aList):
        self.aList = aList
        self.CB.config(state=NORMAL)
        self.CB.delete(0, END)
        self.CB.set_completion_list(aList)
        
    def findInList(self, *args):
        try:
            self.aList.index(self.CB.get())
            return 1
        except:
            return -1

class AppCBVar(Tix.Frame):
    def __init__(self, parent, aVar, aList, aWidth):
        Tix.Frame.__init__(self, parent)
        
        self.config(border = 2, highlightcolor=AppDefaultForeground, highlightthickness = 2, takefocus=0)
        self.bind('<Enter>', widgetEnter)
        self.CB = ttk.Combobox(self,width=aWidth, font=fontBig,justify=CENTER, text=aVar)
        self.CB.grid(row=0, column=0, sticky=N+S+E+W)
        self.CB.bind("<ComboboxSelected>")
        self.CB.config(state=DISABLED)
        self.updateValuesList(aList)
        
    def focus(self):
        self.CB.focus()
        
    def get(self):
        return(self.CB.get())
 
    def clear(self):
        self.CB.config(state='normal')
        self.CB.delete(0, END)
        self.CB.config(state=DISABLED)
                  
    def load(self, aValue):
        self.CB.config(state=NORMAL)
        self.CB.delete(0, END)
        self.CB.insert(0, aValue)
        self.CB.config(state=DISABLED)
        
    def enable(self):
        self.CB.config(state=NORMAL, takefocus=1)
        self.bind('<Enter>', widgetEnter)
       
    def disable(self):
        self.CB.config(state=DISABLED, takefocus=0)
        self.unbind('<Enter>')
        
    def updateValuesList(self, aList):
        self.aList = aList
        self.CB.config(state=NORMAL, values=aList)
        self.CB.delete(0, END)
        self.CB.config(state=DISABLED)


class AppHorizontalCBVar(Tix.Frame):
    def __init__(self, parent, aLabel, aVar, aList, aWidth):
        Tix.Frame.__init__(self, parent)
               
        self.aList = aList
        aLabel = ttk.Label(self, text='{0}: '.format(aLabel), style='RegularFieldTitle.TLabel')
        aLabel.grid(row=0,column=0,sticky=E+N+S)
                
        self.aVar=aVar
        self.CB = ttk.Combobox(self,width=aWidth, font=fontBig,justify=CENTER, text=aVar)
        self.CB.grid(row=0, column=0, sticky=N+S+E+W)
        self.CB.bind("<ComboboxSelected>")
        self.CB.config(state=DISABLED)
        self.updateValuesList(aList)
        
    def focus(self):
        self.CB.focus()
        
    def get(self):
        return(self.CB.get())
 
    def clear(self):
        self.CB.config(state='normal')
        self.CB.delete(0, END)
        self.CB.config(state=DISABLED)
                  
    def load(self, aValue):
        self.CB.config(state=NORMAL)
        self.CB.delete(0, END)
        self.CB.insert(0, aValue)
        self.CB.config(state=DISABLED)
        
    def enable(self):
        self.config(takefocus=1)
        self.CB.config(state=NORMAL)
        self.bind('<Enter>', widgetEnter)
       
    def disable(self):
        self.CB.config(state=DISABLED, takefocus=0)
        self.unbind('<Enter>')
        
    def updateValuesList(self, aList):
        self.aList = aList
        self.CB.config(state=NORMAL, values=aList)
        self.CB.delete(0, END)
        self.CB.config(state=DISABLED)
        
class AppHorizontalCB(Tix.Frame):
    def __init__(self, parent, aLabel, aList, aWidth):
        Tix.Frame.__init__(self, parent)
               
        self.aList = aList
        aLabel = ttk.Label(self, text='{0}: '.format(aLabel), style='RegularFieldTitle.TLabel')
        aLabel.grid(row=0,column=0,sticky=E+N+S)
                
#        self.aVar=aVar
        self.config(border = 2, highlightcolor=AppDefaultForeground, highlightthickness = 2, takefocus=0)
        self.bind('<Enter>', widgetEnter)
        self.CB = ttk.Combobox(self, width=aWidth, font=fontBig, justify=CENTER)
        self.CB.grid(row=0, column=1, sticky=W+N+S)
        self.CB.config(state=DISABLED)
        
    def focus(self):
        self.CB.focus()
        
    def get(self):
        return(self.CB.get())
 
    def clear(self):
        self.CB.config(state='normal')
        self.CB.delete(0, END)
        self.CB.config(state=DISABLED)
                  
    def load(self, aValue):
        self.CB.config(state=NORMAL)
        self.CB.delete(0, END)
        self.CB.insert(0, aValue)
        self.CB.config(state=DISABLED)
        
    def enable(self):
        self.config(takefocus=1)
        self.CB.config(state=NORMAL)
        self.bind('<Enter>', widgetEnter)
       
    def disable(self):
        self.CB.config(state=DISABLED, takefocus=0)
        self.unbind('<Enter>')
        
    def updateValuesList(self, aList):
        self.aList = aList
        self.CB.config(state=NORMAL, values=aList)
        self.CB.delete(0, END)
        self.CB.config(state=DISABLED)

class AppHorizontalCB_AC(Tix.Frame):
    def __init__(self, parent, aLabel, aList, aWidth):
        Tix.Frame.__init__(self, parent)
               
        self.aList = aList
        aLabel = ttk.Label(self, text='{0}: '.format(aLabel), style='RegularFieldTitle.TLabel')
        aLabel.grid(row=0,column=0,sticky=E+N+S)
                        
        self.config(border = 2, highlightcolor=AppDefaultForeground, highlightthickness = 2, takefocus=0)
        self.bind('<Enter>', widgetEnter)
        self.CB = AutocompleteCombobox(self,width=aWidth, font=fontBig,justify=CENTER)
        self.CB.grid(row=0, column=1, sticky=N+S+E+W)
        self.CB.set_completion_list(aList)
        self.CB.bind("<ComboboxSelected>")
        self.CB.config(state=DISABLED)
        self.updateValuesList(aList)
        
    def focus(self):
        self.CB.focus()
        
    def get(self):
        return(self.CB.get())
 
    def clear(self):
        self.CB.config(state='normal')
        self.CB.delete(0, END)
        self.CB.config(state=DISABLED)
                  
    def load(self, aValue):
        self.CB.config(state=NORMAL)
        self.CB.delete(0, END)
        self.CB.insert(0, aValue)
        self.CB.config(state=DISABLED)
        
    def enable(self):
        self.CB.config(state=NORMAL, takefocus=1)
        self.bind('<Enter>', widgetEnter)
       
    def disable(self):
        self.CB.config(state=DISABLED, takefocus=0)
        self.unbind('<Enter>')
        
    def updateValuesList(self, aList):
        self.aList = aList
        self.CB.config(state=NORMAL)
        self.CB.delete(0, END)
        self.CB.set_completion_list(aList)
        
    def findInList(self, *args):
        try:
            self.aList.index(self.CB.get())
            return 1
        except:
            return -1

class AppCB2(Tix.Frame):
    def __init__(self, parent, aVar, aList, aWidth):
        Tix.Frame.__init__(self, parent)
        self.aVar=aVar
        self.config(border = 2, highlightcolor=AppDefaultForeground, highlightthickness = 2, takefocus=0)
        self.bind('<Enter>', widgetEnter)
        self.CB = ttk.Combobox(self, text=aVar, width=aWidth, font=fontBig,
                                 justify=CENTER)
        self.CB.grid(row=0, column=0, sticky=N+S+E+W)
        self.CB.bind("<ComboboxSelected>")
        self.CB.config(state='readonly')
        self.CB.config(state=DISABLED)
        self.updateValuesList(aList)
        
    def focus(self):
        self.CB.focus()
        
    def get(self):
        return(self.aVar.get())
    
    def clear(self):
        self.CB.config(state='normal')
        self.CB.delete(0, END)
        self.CB.config(state=DISABLED)
                   
    def load(self, aValue):
        self.CB.config(state=NORMAL)
        self.CB.delete(0, END)
        self.CB.insert(0, aValue)
        self.CB.config(state=DISABLED)
        
    def enable(self):
        self.CB.config(state='readonly', takefocus=1)
        self.bind('<Enter>', widgetEnter)
       
    def disable(self):
        self.CB.config(state=DISABLED, takefocus=0)
        self.unbind('<Enter>')
        
    def updateValuesList(self, aList):
        self.CB.config(state=NORMAL)
        self.CB.delete(0, END)
        self.CB.config(state='readonly')
        self.CB.config(values=aList)
               
class AppHorizontalCB2(Tix.Frame):
    def __init__(self, parent, aLabel, aVar, aList, aWidth):
        Tix.Frame.__init__(self, parent)
               
        aLabel = ttk.Label(self, text='{0}: '.format(aLabel), style='RegularFieldTitle.TLabel')
        aLabel.grid(row=0,column=0,sticky=E+N+S)
                
        self.aVar=aVar
        self.config(border = 2, highlightcolor=AppDefaultForeground, highlightthickness = 2, takefocus=0)
        self.bind('<Enter>', widgetEnter)
        self.CB = ttk.Combobox(self, text=aVar, width=aWidth, font=fontBig,
                                 justify=CENTER)
        self.CB.grid(row=0, column=1, sticky=W+N+S)
        self.CB.bind("<ComboboxSelected>")
        self.CB.config(state='readonly')
        self.CB.config(state=DISABLED)
        self.updateValuesList(aList)
        
    def focus(self):
        self.CB.focus()
        
    def get(self):
        return(self.aVar.get())
 
    def clear(self):
        self.CB.config(state='normal')
        self.CB.delete(0, END)
        self.CB.config(state=DISABLED)
                  
    def load(self, aValue):
        self.CB.config(state=NORMAL)
        self.CB.delete(0, END)
        self.CB.insert(0, aValue)
        self.CB.config(state=DISABLED)
        
    def enable(self):
        self.CB.config(state='readonly', takefocus=1)
        self.bind('<Enter>', widgetEnter)
       
    def disable(self):
        self.CB.config(state=DISABLED, takefocus=0)
        self.unbind('<Enter>')
        
    def updateValuesList(self, aList):
        self.CB.config(state=NORMAL)
        self.CB.delete(0, END)
        self.CB.config(state='readonly')
        self.CB.config(values=aList)

class AppUserDisplayLabel(Tix.Label):         
    def __init__(self, parent, userName):
        Tix.Label.__init__(self, parent)
#        self.aFunction = Tix.Label(self, text='Default view', font=fontAverageI, justify=RIGHT)
#        self.aFunction.grid(row=0, column=0, sticky=E+S)
#        self.rowconfigure(0, weight=0)
        self.userName = userName
        self.config(text=userName, font=fontAverageI, justify=RIGHT)
        
    def setUserName(self, aName):
        self.config(text=aName)


class AppFunctionDisplayLabel(Tix.Label):         
    def __init__(self, parent):
        Tix.Label.__init__(self, parent)
#        self.aFunction = Tix.Label(self, text='Default view', font=fontAverageI, justify=RIGHT)
#        self.aFunction.grid(row=0, column=0, sticky=E+S)
#        self.rowconfigure(0, weight=0)
        self.config(text='Default view', font=fontAverageI, justify=RIGHT)
        
    def setAddMode(self):
        self.config(text='Add mode', font=fontAverageI, justify=RIGHT, foreground='red')
            
    def setFindMode(self):
        self.config(text='Find mode', font=fontAverageI, justify=RIGHT, foreground='green')
            
    def setFindResultMode(self):
        self.config(text='Find Result View', font=fontAverageI, justify=RIGHT, foreground='green')
            
    def setEditMode(self):
        self.config(text='Edit mode', font=fontAverageI, justify=RIGHT, foreground='red')
        
    def setDuplicateMode(self):
        self.config(text='Duplicate Record mode', font=fontAverageI, justify=RIGHT, foreground='red')
            
    def setDefaultMode(self):
        self.config(text='Default view', font=fontAverageI, justify=RIGHT, foreground=AppDefaultForeground)

         
class CreateToolTip(object):
    '''
    create a tooltip for a given widget
    '''
    def __init__(self, widget, text='widget info'):
        self.widget = widget
        self.text = text
        self.widget.bind("<Enter>", self.enter)
        self.widget.bind("<Leave>", self.close)
        
    def enter(self, event=None):
        x = y = 0
        x, y, cx, cy = self.widget.bbox("insert")
        x += self.widget.winfo_rootx() + 25
        y += self.widget.winfo_rooty() + 20
        # creates a toplevel window
        self.tw = Tix.Toplevel(self.widget)
        # Leaves only the label and removes the app window
        self.tw.wm_overrideredirect(True)
        self.tw.wm_geometry("+%d+%d" % (x, y))
        label = ttk.Label(self.tw, text=self.text, style='toolTip.TLabel')
        label.pack(ipadx=1)
        
    def close(self, event=None):
        if self.tw:
            self.tw.destroy()

class navDatabaseTool(Tix.Frame):
    def __init__(self, parent):
        self.parent=parent
        Tix.Frame.__init__(self, parent)
        navDisplay=Tix.Frame(self)
       
        self.recNumDisplay = Tix.Entry(navDisplay, relief='flat', width=recordNumWidth, disabledforeground=AppDefaultForeground)
        if len(parent.recList) > 0:
            self.myString = '''  Record {0} of {1}'''.format(parent.curRecNumV.get()+1, len(parent.recList))
        else:
            self.myString = '''  Record {0} of {1}'''.format(parent.curRecNumV.get(), len(parent.recList))
        self.recNumDisplay.insert(0, self.myString)
        self.recNumDisplay.pack(side=RIGHT)
        self.recNumDisplay.config(state=DISABLED)


#        b = tk.Button(self.middleButton, text="Add Addr", font=fontAverage, fg=MyGolfDefaultFGColor,
#                      command=self._startAddrWindow)
#        b.grid(row=0, column=1,  sticky=N+S)        
        nxtRecImage = resizeImage(Image.open(nextRecImage),iconWidth,iconHeight)
        prvRecImage = resizeImage(Image.open(previousRecImage),iconWidth,iconHeight)
        lstRecImage = resizeImage(Image.open(lastRecImage),iconWidth,iconHeight)
        fstRecImage = resizeImage(Image.open(firstRecImage),iconWidth,iconHeight)
                     
        self.buttonLast = ttk.Button(navDisplay, image=lstRecImage, command  = parent.lastRecord, style='CommandButton.TButton')
        self.buttonLast.img = lstRecImage
        self.buttonLast.pack(side=RIGHT)
        self.buttonLast_ttp = CreateToolTip(self.buttonLast, "Display Last Record")
        
        self.buttonNext = ttk.Button(navDisplay, image=nxtRecImage, command  = parent.nextRecord, style='CommandButton.TButton')
        self.buttonNext.img = nxtRecImage
        self.buttonNext.pack(side=RIGHT)  
        self.buttonNext_ttp = CreateToolTip(self.buttonNext, "Display Next Record")
        
        self.buttonPrevious = ttk.Button(navDisplay, image=prvRecImage, command  = parent.prvRecord, style='CommandButton.TButton')
        self.buttonPrevious.img = prvRecImage
        self.buttonPrevious.pack(side=RIGHT)
        self.buttonPrevious_ttp = CreateToolTip(self.buttonPrevious, "Display Previous Record")
        
        self.buttonFirst = ttk.Button(navDisplay, image=fstRecImage, command  = parent.firstRecord, style='CommandButton.TButton')
        self.buttonFirst.img = fstRecImage
        self.buttonFirst_ttp = CreateToolTip(self.buttonFirst, "Display First Record")
        self.buttonFirst.pack(side=RIGHT)        

        navDisplay.pack()
        
    def updateRecordNumber(self, *args):
        self.recNumDisplay.config(state='normal')
        self.recNumDisplay.delete(0, END)
        if len(self.parent.recList) > 0:
            self.myString = '''  Record {0} of {1}'''.format(self.parent.curRecNumV.get()+1, len(self.parent.recList))
        else:  
            self.myString = '''  Record {0} of {1}'''.format(self.parent.curRecNumV.get(), len(self.parent.recList))
        self.recNumDisplay.insert(0, self.myString)
        self.recNumDisplay.pack(side=RIGHT)
        self.recNumDisplay.config(state=DISABLED)
                      
    def enableNavigation(self):
        self.buttonFirst.config(state=NORMAL)
        self.buttonLast.config(state=NORMAL)
        self.buttonNext.config(state=NORMAL)
        self.buttonPrevious.config(state=NORMAL)

    def disableNavigation(self):
        self.buttonFirst.config(state=DISABLED)
        self.buttonLast.config(state=DISABLED)
        self.buttonNext.config(state=DISABLED)
        self.buttonPrevious.config(state=DISABLED)

class databaseCommands(Tix.Frame):
    def __init__(self, parent, access):
        Tix.Frame.__init__(self, parent)

        self.access = access

        databaseCommandsDisplay=Tix.Frame(self)
        databaseCommandsDisplay.grid(column=0, row=0)

        self.rowCount = 0
        self.columnCount = 0
        btnImage = resizeImage(Image.open(findRecImage),iconWidth,iconHeight)
        self.buttonFind = ttk.Button(databaseCommandsDisplay, image=btnImage, command=parent.findCommand, style='CommandButton.TButton') 
        self.buttonFind.grid(column=self.columnCount, row=0, sticky=W)
        self.buttonFind.img = btnImage
        self.buttonFind_ttp = CreateToolTip(self.buttonFind, "Find Record(s)")
        
        self.columnCount = self.columnCount + 1
        btnImage = resizeImage(Image.open(addCommandRecImage),iconWidth,iconHeight)
        self.addCommand = ttk.Button(databaseCommandsDisplay, image=btnImage, command=parent.addCommand, style='CommandButton.TButton') 
        self.addCommand.grid(column=self.columnCount, row=0, sticky=W)
        self.addCommand.img = btnImage
        self.addCommand_ttp = CreateToolTip(self.addCommand, "Add a New Record")
        
        self.columnCount = self.columnCount + 1
        btnImage = resizeImage(Image.open(editRecImage),iconWidth,iconHeight)
        self.editCommand = ttk.Button(databaseCommandsDisplay, image=btnImage, command=parent.editCommand, style='CommandButton.TButton') 
        self.editCommand.grid(column=self.columnCount, row=0, sticky=W)
        self.editCommand.img = btnImage
        self.editCommand_ttp = CreateToolTip(self.editCommand, "Edit Current Record")
        
        self.columnCount = self.columnCount + 1
        btnImage = resizeImage(Image.open(duplicateRecImage),iconWidth,iconHeight)
        self.duplicateCommand = ttk.Button(databaseCommandsDisplay, image=btnImage, command=parent.duplicateCommand, style='CommandButton.TButton') 
        self.duplicateCommand.grid(column=self.columnCount, row=0, sticky=W)
        self.duplicateCommand.img = btnImage
        self.duplicateCommand_ttp = CreateToolTip(self.duplicateCommand, "Create Duplicate Record")
        
        self.columnCount = self.columnCount + 1
        btnImage = resizeImage(Image.open(saveRecImage),iconWidth,iconHeight)
        self.saveCommand = ttk.Button(databaseCommandsDisplay, image=btnImage, command=parent.saveCommand, style='CommandButton.TButton') 
        self.saveCommand.grid(column=self.columnCount, row=0, sticky=W)
        self.saveCommand.img = btnImage
        self.saveCommand_ttp = CreateToolTip(self.saveCommand, "Save Current Record, or\nExecute Find Command")
        self.saveCommand.config(state=DISABLED)
        
        self.columnCount = self.columnCount + 1
        btnImage = resizeImage(Image.open(deleteCommandRecImage),iconWidth,iconHeight)
        self.deleteButton = ttk.Button(databaseCommandsDisplay, image=btnImage, command=parent.deleteCommand, style='CommandButton.TButton') 
        self.deleteButton.grid(column=self.columnCount, row=0, sticky=W)
        self.deleteButton.img = btnImage
        self.deleteButton_ttp = CreateToolTip(self.deleteButton, "Delete Current Command")
        
        self.columnCount = self.columnCount + 1
        btnImage = resizeImage(Image.open(cancelCommandRecImage),iconWidth,iconHeight)
        self.cancelButton = ttk.Button(databaseCommandsDisplay, image=btnImage, command=parent.cancelCommand, style='CommandButton.TButton') 
        self.cancelButton.grid(column=self.columnCount, row=0, sticky=W)
        self.cancelButton.img = btnImage
        self.cancelButton_ttp = CreateToolTip(self.cancelButton, "Cancel Current Command")
        
        self.columnCount = self.columnCount + 1
        btnImage = resizeImage(Image.open(refreshRecImage),iconWidth,iconHeight)
        self.buttonRefresh = ttk.Button(databaseCommandsDisplay, image=btnImage, command=parent.refreshWindow, style='CommandButton.TButton') 
        self.buttonRefresh.grid(column=self.columnCount, row=0, sticky=W)
        self.buttonRefresh.img = btnImage
        self.buttonRefresh_ttp = CreateToolTip(self.buttonRefresh, "Reload Default View\nRefreshes DB Records")
        
    def _setAccessRestriction(self):
        if self.access == 'View':
            self.duplicateCommand.config(state=DISABLED)
            self.addCommand.config(state=DISABLED)
            self.editCommand.config(state=DISABLED)
            self.deleteButton.config(state=DISABLED)
            
    def emptyDBState(self):
        self.duplicateCommand.config(state=DISABLED)
        self.buttonFind.config(state=DISABLED)
        self.addCommand.config(state=NORMAL)
        self.editCommand.config(state=DISABLED)
        self.saveCommand.config(state=DISABLED)
        self.deleteButton.config(state=DISABLED)
        self.cancelButton.config(state=NORMAL)
        self.buttonRefresh.config(state=NORMAL)
        self._setAccessRestriction()
                  
    def findState(self):
        self.duplicateCommand.config(state=DISABLED)
        self.buttonFind.config(state=DISABLED)
        self.addCommand.config(state=DISABLED)
        self.editCommand.config(state=DISABLED)
        self.saveCommand.config(state=NORMAL)
        self.deleteButton.config(state=DISABLED)
        self.cancelButton.config(state=NORMAL)
        self.buttonRefresh.config(state=DISABLED)
        self._setAccessRestriction()
        
    def addState(self):
        self.duplicateCommand.config(state=DISABLED)
        self.buttonFind.config(state=DISABLED)
        self.addCommand.config(state=DISABLED)
        self.editCommand.config(state=DISABLED)
        self.saveCommand.config(state=NORMAL)
        self.deleteButton.config(state=DISABLED)
        self.cancelButton.config(state=NORMAL)
        self.buttonRefresh.config(state=DISABLED)
        self._setAccessRestriction()
        
    def editState(self):
        self.duplicateCommand.config(state=DISABLED)
        self.buttonFind.config(state=DISABLED)
        self.addCommand.config(state=DISABLED)
        self.editCommand.config(state=DISABLED)
        self.saveCommand.config(state=NORMAL)
        self.deleteButton.config(state=DISABLED)
        self.cancelButton.config(state=NORMAL)
        self.buttonRefresh.config(state=DISABLED)
        self._setAccessRestriction()
        
    def deleteState(self):
        self.duplicateCommand.config(state=DISABLED)
        self.buttonFind.config(state=DISABLED)
        self.addCommand.config(state=DISABLED)
        self.editCommand.config(state=DISABLED)
        self.saveCommand.config(state=DISABLED)
        self.deleteButton.config(state=DISABLED)
        self.cancelButton.config(state=DISABLED)
        self.buttonRefresh.config(state=DISABLED)
        self._setAccessRestriction()
 
    def defaultState(self):
        self.duplicateCommand.config(state=NORMAL)
        self.buttonFind.config(state=NORMAL)
        self.addCommand.config(state=NORMAL)
        self.editCommand.config(state=NORMAL)
        self.saveCommand.config(state=DISABLED)
        self.deleteButton.config(state=NORMAL)
        self.cancelButton.config(state=NORMAL)
        self.buttonRefresh.config(state=NORMAL)
        self._setAccessRestriction()
        
    def cancelState(self):
        self.duplicateCommand.config(state=NORMAL)
        self.buttonFind.config(state=NORMAL)
        self.addCommand.config(state=NORMAL)
        self.editCommand.config(state=NORMAL)
        self.saveCommand.config(state=DISABLED)
        self.deleteButton.config(state=NORMAL)
        self.cancelButton.config(state=NORMAL)
        self.buttonRefresh.config(state=NORMAL)
        self._setAccessRestriction()
        
    def saveState(self):
        self.duplicateCommand.config(state=NORMAL)
        self.buttonFind.config(state=NORMAL)
        self.addCommand.config(state=NORMAL)
        self.editCommand.config(state=NORMAL)
        self.saveCommand.config(state=DISABLED)
        self.deleteButton.config(state=NORMAL)
        self.cancelButton.config(state=NORMAL)
        self.buttonRefresh.config(state=NORMAL)
        self._setAccessRestriction()
        
    def refreshState(self):
        self.duplicateCommand.config(state=NORMAL)
        self.buttonFind.config(state=NORMAL)
        self.addCommand.config(state=NORMAL)
        self.editCommand.config(state=NORMAL)
        self.saveCommand.config(state=DISABLED)
        self.deleteButton.config(state=NORMAL)
        self.cancelButton.config(state=NORMAL)
        self.buttonRefresh.config(state=NORMAL)
        self._setAccessRestriction()
       

#        self.buttonAdd = tk.Button(databaseCommandsDisplay, text='Add',  font=buttonFont,
#                                    fg=MyGolfDefaultFGColor,command= lambda: parent.addRecord()) 
#        self.buttonAdd.grid(column=1, row=0, sticky=W)
#        self.buttonAdd.pack(side=LEFT)       
#        new_order = (self.buttonFind, self.buttonAdd, self.buttonEdit, self.buttonDelete, self.buttonSave, self.buttonCancel, self.buttonRefresh)
#        for widget in new_order:
#            widget.lift()

#        courseCommandsDisplay.pack(side = TOP)

#    def disableDatabaseCommands(self, op):
#        if (op == 'add') or (op == 'edit') or (op== 'find'):
#            self.buttonFind.config(state=DISABLED)
#            self.buttonAdd.config(state=DISABLED)
#            self.buttonEdit.config(state=DISABLED)
#            self.buttonDelete.config(state=DISABLED)
#            self.buttonSave.config(state=NORMAL)
#            self.buttonCancel.config(state=NORMAL)
#        elif op=='del':
#            self.buttonFind.config(state=DISABLED)
#            self.buttonAdd.config(state=DISABLED)
#            self.buttonEdit.config(state=DISABLED)
#            self.buttonDelete.config(state=DISABLED)
#            self.buttonSave.config(state=DISABLED)
#            self.buttonCancel.config(state=DISABLED)

#    def enableDatabaseCommands(self, op):
#        if op == 'default':
#            self.buttonFind.config(state=NORMAL)
#            self.buttonAdd.config(state=NORMAL)
#            self.buttonEdit.config(state=NORMAL)
#            self.buttonDelete.config(state=NORMAL)
#            self.buttonSave.config(state=NORMAL)
#            self.buttonCancel.config(state=NORMAL)

class messageBar(Tix.Frame):
    def __init__(self, parent):
        Tix.Frame.__init__(self, parent)
        messageFrame=Tix.Frame(self)
        self.myMessage = '''   '''
        self.messageDisplay = Tix.Label(messageFrame, text=self.myMessage, relief='flat')
        self.messageDisplay.pack(side=LEFT, fill=X)
        messageFrame.pack()
        
    def clearMessage(self):
        self.myMessage = '''   '''
        self.messageDisplay.config(text=self.myMessage)
            
    def newMessage(self, msgType, msg):
        if msgType == 'info':
            self.messageDisplay.config(fg=AppDefaultForeground)
        elif msgType == 'error':
            self.bell()
            self.messageDisplay.config(fg='red')
        elif msgType == 'warning':
            self.bell()
            self.messageDisplay.config(fg='green')
        self.messageDisplay.config(text=msg)


class tableCommonWindowClass(Tix.Frame):
    def __init__(self, parent, aCloseCommand, windowTitle, accessLevel):
        Tix.Frame.__init__(self, parent)
        self.accessLevel = accessLevel
        self.CloseMe = aCloseCommand
        self.parent = parent
        self.curRecNumV = Tix.IntVar()
        self.curRecNumV.set(0)
        self.curRecBeforeOp = self.curRecNumV.get()
        self.curFind = False
        self.curOp='default'   
        self.recList = []
        self.accessLevel = ''
        self.updaterID = 0
        self.mainAreaRowCount = 0
        self.mainAreaColumnCount = 0       
                    
############################################################ Window Common Configuration
        self.dirty=False
        mainWInWidth = 400
        mainWinHeight = 400
        self.configure(width=mainWInWidth, height=mainWinHeight)
        self.columnCountTotal = 7
        for i in range(self.columnCountTotal):
            self.columnconfigure(i, weight=1)
        
############################################################ Window Title Area        
        self.rowCount = 0
        self.columnCount = 0       
        wMsg = Tix.Label(self, text=windowTitle, font=fontMonstrousB)
        wMsg.grid(row=self.rowCount, column=self.columnCount, columnspan=self.columnCountTotal, sticky=E+W)
        self.rowconfigure(self.rowCount, weight=0)
        
        self.displayCurFunction = AppFunctionDisplayLabel(self)
        self.displayCurFunction.grid(row=self.rowCount, column=self.columnCount, columnspan=self.columnCountTotal, sticky=E+S)
        
        self.displayCurUser = AppUserDisplayLabel(self, " ")
        self.displayCurUser.grid(row=self.rowCount, column=self.columnCount, columnspan=self.columnCountTotal, sticky=W+S)

############################################################ Main Frame Area        
        self.rowCount = self.rowCount + 1
        self.columnCount = 0       
        self.mainArea = Tix.Frame(self, border=3, relief='groove')        
        self.mainArea.grid(row=self.rowCount, column=self.columnCount, columnspan=self.columnCountTotal, sticky=N+S+E+W) 
        self.mainAreaColumnTotal = 21
        for i in range(self.mainAreaColumnTotal):
            self.mainArea.columnconfigure(i, weight=1)
        self.rowconfigure(self.rowCount, weight=1)
               
############################################################ Command Frame Area        
        self.rowCount = self.rowCount + 1
        self.columnCount = 0
        
        self.myDatabaseBar = databaseCommands(self, self.accessLevel)
        self.myDatabaseBar.grid(row=self.rowCount, column=self.columnCount, sticky=N+W+S)
        
        #  This frame is used for commands specific to current class
        self.columnCount = self.columnCount + 1
        self.commandFrame = Tix.Frame(self)
        self.commandFrame.grid(row=self.rowCount, column=self.columnCount, columnspan=self.columnCountTotal-2, sticky=N+S) 
                
        self.myNavBar = navDatabaseTool(self)
        self.myNavBar.grid(row=self.rowCount, column=self.columnCountTotal-1, sticky=N+E+S)
        self.curRecNumV.trace('w',self.myNavBar.updateRecordNumber)               

        self.rowconfigure(self.rowCount, weight=0)
        
############################################################ Message Bar Area       
        self.rowCount = self.rowCount + 1
        self.columnCount = 0       
        self.myMsgBar = messageBar(self)
        self.myMsgBar.grid(row=self.rowCount, column=self.columnCount, columnspan=self.columnCountTotal, sticky=W+S, padx=5)
        self.rowconfigure(self.rowCount, weight=0)
        
########################################################### Display records
        self.recordFrame = Tix.Frame(self.mainArea)        
        self.recordFrame.grid(row=0, column=0, columnspan= self.mainAreaColumnTotal, sticky=N+S+E+W)
        
        wMsg = Tix.Label(self.recordFrame, text='No Records Found', font=fontMonstrousB)
        wMsg.grid(row=0, column=0)
                    
                 
############################################################ Initial Navigation State
        if len(self.recList) == 0:
            self.navigationDisable('')

############################################################ Table Specific Commands
#
#   These commands will be redefined for each table that invokes this class
#   Manipulation of data is specific to each tables.
#                
    def nextRecord(self, *args):
        self.myMsgBar.clearMessage()
        if len(self.recList) > 0:
            if self.curRecNumV.get() < (len(self.recList)-1): # Don't go past last record
                self.curRecNumV.set(self.curRecNumV.get() + 1)
                self.displayRec()
                
    def prvRecord(self, *args):
        self.myMsgBar.clearMessage()
        if len(self.recList) > 0:
            if self.curRecNumV.get() > 0: # Don't go past first record
                self.curRecNumV.set(self.curRecNumV.get() - 1)
                self.displayRec()
        
    def lastRecord(self, *args):
        self.myMsgBar.clearMessage()
        if len(self.recList) > 0:
            self.curRecNumV.set((len(self.recList)-1))
            self.displayRec()
       
    def firstRecord(self, *args):
        self.myMsgBar.clearMessage()
        if len(self.recList) > 0:
            self.curRecNumV.set(0)
            self.displayRec()
     
    def displayRec(self, *args):
        # Table Specific
        pass
    
    def refresh(self, *args):
        # Table Specific
        pass
    
    def resetCurOp(self):
        if self.curFind == True:
            self.curOp = 'find'
            self.displayCurFunction.setFindResultMode()
        else:    
            self.curOp = 'default'
            self.displayCurFunction.setDefaultMode()
            
    def refreshWindow(self, *args):
        #
        #  Refresh all DB records
        #  (Sync DB with main ECE Data)
        #
        self.myMsgBar.clearMessage()
        self.myDatabaseBar.refreshState()
        self.refresh()
        self.curFind = False
        self.curOp = 'default'
        self.displayCurFunction.setDefaultMode()
        self.myNavBar.updateRecordNumber()
        self.fieldsDisable()           
        self.displayRec()
        
    def findCommand(self, *args):
        self.myMsgBar.clearMessage()
        self.myDatabaseBar.findState()
        self.myNavBar.disableNavigation()
        self.curFind = True
        self.curOp = 'find'
        self.displayCurFunction.setFindMode()
        self.find()

    def editCommand(self, *args):
        self.myMsgBar.clearMessage()
        self.myDatabaseBar.editState()
        self.myNavBar.disableNavigation()
        self.curRecBeforeOp = self.curRecNumV.get()
        self.curOp = 'edit'
        self.displayCurFunction.setEditMode()
        self.edit()

    def duplicateCommand(self, *args):
        self.myMsgBar.clearMessage()
        self.myDatabaseBar.addState()
        self.myNavBar.disableNavigation()
        self.curRecBeforeOp = self.curRecNumV.get()
        self.curOp = 'add'
        self.displayCurFunction.setDuplicateMode()
        self.duplicate()
        
    def duplicate(self):
        #Table specific.  Will be defined in calling class
        pass
    
    def edit(self):
        #Table specific.  Will be defined in calling class
        pass
    
    def find(self):
        #Table specific.  Will be defined in calling class
        pass
       
    def addCommand(self, *args):
        self.myMsgBar.clearMessage()
        self.myDatabaseBar.addState()
        self.myNavBar.disableNavigation()
        self.curOp = 'add'
        self.curRecBeforeOp = self.curRecNumV.get()
        self.displayCurFunction.setAddMode()
        self.add()
        
    def add(self):
        #Table specific.  Will be defined in calling class
        pass
    
    def delete(self):
        #Table specific.  Will be defined in calling class
        pass
        
    def saveCommand(self, *args):
        self.myMsgBar.clearMessage()
        if self.save() == True:
            self.myDatabaseBar.saveState()
            if len(self.recList) == 0:
                self.navigationDisable(self.accessLevel)
            else:
                self.myNavBar.enableNavigation()
            self.curOp = 'default'
            self.displayRec()
        
    def save(self):
        #Table specific.  Will be defined in calling class
        pass
    
    def navEmptyList(self):
        self.navigationDisable(self.accessLevel)
        
        
    def cancelCommand(self, *args):
        self.myMsgBar.clearMessage()
        self.myDatabaseBar.cancelState()
        if self.curFind == True:
            self.curOp = 'find'
            self.displayCurFunction.setFindResultMode()
        else:
            self.curOp = 'default'
            self.displayCurFunction.setDefaultMode()
        self.navigationEnable(self.accessLevel)
        self.fieldsDisable()           
        if len(self.recList) != 0:
            self.displayRec()
        
    def deleteCommand(self, *args):
        self.myMsgBar.clearMessage()
        self.myDatabaseBar.deleteState()
        self.myNavBar.disableNavigation()
        if len(self.recList) == 0:
            self.navigationDisable(self.accessLevel)
        self.curOp = 'delete'
        self.curRecBeforeOp = self.curRecNumV.get()
        self.delete()
        
    def fieldsEnable(self):
        # Table Specific
        pass
       
    def fieldsDisable(self):
        # Table Specific
        pass
        
    def navigationDisable(self, accessLevel):
        self.myDatabaseBar.duplicateCommand.config(state=DISABLED)
        self.myDatabaseBar.buttonFind.config(state=DISABLED)
        self.myDatabaseBar.editCommand.config(state=DISABLED)
        self.myDatabaseBar.deleteButton.config(state=DISABLED)
        self.myDatabaseBar.buttonRefresh.config(state=DISABLED)
        if accessLevel == 'View':
            self.myDatabaseBar.buttonFind.config(state=NORMAL)
        self.myNavBar.disableNavigation()
                
    def navigationEnable(self, accessLevel):
        if accessLevel == 'Root':
            self.myDatabaseBar.duplicateCommand.config(state=NORMAL)
            self.myDatabaseBar.buttonFind.config(state=NORMAL)
            self.myDatabaseBar.editCommand.config(state=NORMAL)
            self.myDatabaseBar.deleteButton.config(state=NORMAL)
            self.myDatabaseBar.addCommand.config(state=NORMAL)
            self.myDatabaseBar.buttonRefresh.config(state=NORMAL)
        elif accessLevel == 'Maintenance':
            self.myDatabaseBar.duplicateCommand.config(state=NORMAL)
            self.myDatabaseBar.buttonFind.config(state=NORMAL)
            self.myDatabaseBar.editCommand.config(state=NORMAL)
            self.myDatabaseBar.deleteButton.config(state=NORMAL)
            self.myDatabaseBar.addCommand.config(state=NORMAL)
            self.myDatabaseBar.buttonRefresh.config(state=NORMAL)
        elif accessLevel == 'Update':
            self.myDatabaseBar.duplicateCommand.config(state=DISABLED)
            self.myDatabaseBar.buttonFind.config(state=NORMAL)
            self.myDatabaseBar.editCommand.config(state=NORMAL)
            self.myDatabaseBar.deleteButton.config(state=DISABLED)
            self.myDatabaseBar.addCommand.config(state=DISABLED)
            self.myDatabaseBar.buttonRefresh.config(state=NORMAL)
        elif accessLevel == 'View':
            self.myDatabaseBar.duplicateCommand.config(state=DISABLED)
            self.myDatabaseBar.buttonFind.config(state=NORMAL)
            self.myDatabaseBar.editCommand.config(state=DISABLED)
            self.myDatabaseBar.deleteButton.config(state=DISABLED)
            self.myDatabaseBar.addCommand.config(state=DISABLED)
            self.myDatabaseBar.buttonRefresh.config(state=NORMAL)

        if len(self.recList) == 0:
            self.myDatabaseBar.emptyDBState()
        else:
            self.myNavBar.enableNavigation()
        
        