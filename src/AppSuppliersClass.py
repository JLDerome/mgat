##########################################################
#
#   Departments Table
#
#   This class implements the specific manipulation of the
# data for the ECE Departments Table that the TECH Shop requires.
#

import tkinter.tix as Tix
import tkinter.ttk as ttk
from MyGolfApp2016 import AppCRUD_FROMECECRUD as AppDB
import tkinter.filedialog as Fd
import tkinter.messagebox as Mb
import textwrap, re

from tkinter.constants import *

from MyGolfApp2016.AppMyClasses import *
from MyGolfApp2016.AppConstants import *

from MyGolfApp2016.AppProcFROMECE import *

from PIL import Image
#
# Starter frame for a new table
#
class suppliersWindow(tableCommonWindowClass):
    def __init__(self, aWindow, aCloseCommand, loginData):
        self.windowTitle = 'Suppliers'
        tableCommonWindowClass.__init__(self, aWindow, aCloseCommand, self.windowTitle, loginData[1])
        self.loginData = loginData
        self.accessLevel = self.loginData[1]
        self.updaterID = self.loginData[2]
        rankFullName=AppDB.getRankFullName(self.updaterID)
        self.displayCurUser.setUserName(rankFullName)
#######################################################################################
# Inherited all from tableCommanWindowClass
#  
#  This class intents to change the mainArea only.  Everything else must remain common
#       
        self.mainAreaRowCount = 0
        self.mainAreaColumnCount = 0       
        self._getNewRecList()
        self._reDisplayRecordFrame()
        self.mainArea.rowconfigure(self.mainAreaRowCount, weight=1)
        
############################################################ Area to add window specific commands        
#        self.closeButton = ttk.Button(self.commandFrame, text='Test', style='CommandButton.TButton', 
#                                 command= lambda: self.sampleCommand())
#        self.closeButton.grid(row=0, column=0, sticky=N+S)
        

################################################################# Redefined commands
#
#  invoke from the tableCommonWindows.  These commands are specific to each tables
#  The specific actions are redefinded here.
#       
#    def displayRec(self, *args):
#        print("Displaying a Record: ", self.curRecNum)
#        
#    def save(self):
#        #
#        #  Table specific.  Return a True if succesful saved.
#        #
#        print("Saving a record (re-defined)")
#        return True

    def _departmentEntered(self, *args):
        pass

    def _enableWindowCommand(self):
        #
        #  Any command specific must added here 
        #
        pass
#        self.closeButton.config(state=NORMAL)

    def _disableWindowCommand(self):
        #
        #  Any command specific must added here 
        #
        pass
#        self.closeButton.config(state=DISABLED)
               
    def sampleCommand(self):
        self.myMsgBar.clearMessage()
        aMsg = 'INFO: Command area, specific to current window ({0})'.format(self.windowTitle)
        self.myMsgBar.newMessage('info', aMsg)
        AppDB.importCSV_Parts(importTUTPartsFilename)

    def _getNewRecList(self):
        self.recList = AppDB.getSuppliers()
        self.curRecNumV.set(self.curRecNumV.get()) # forces display of record number
        
    def _reDisplayRecordFrame(self):
        self.recordFrame.destroy()
        self._getNewRecList()
        self._displayRecordFrame()
        if self.curOp == 'add':
            self.fieldsEnable()
        
    def _displayRecordFrame(self):
        self.recordFrame = AppFrame(self.mainArea, 1) 
        self.recordFrame.grid(row=self.mainAreaRowCount, column=self.mainAreaColumnCount, columnspan= self.mainAreaColumnTotal, sticky=N+S+E+W)
        
        if len(self.recList) > 0:
            self._buildWindowsFields(self.recordFrame)
            self.recordFrame.rowconfigure(self.recordFrame.row, weight=1)
            if self.curOp != 'add':        
                self.displayRec()
                self.navigationEnable(self.accessLevel)
            new_order = (
                            self.selectedSuppliersName, 
                            self.selectedS_NUM,
                            self.selectedS_ADDR1,
                            self.selectedS_ADDR2,
                            self.selectedS_ADDR3,
                            self.selectedS_ADDR4,
                            self.selectedS_ADDR5,
                            self.selectedS_PHONE,
                            self.selectedS_FAX,
                            self.selectedS_CNTC,
                            self.selectedMAN_NUM,
                            self.selectedPHONE_AREA,    
                            self.selectedPHONE_EXCH,
                            self.selectedPHONE_LOCAL,
                            self.selectedFAX_AREA,
                            self.selectedFAX_EXCH,
                            self.selectedFAX_LOCAL,
                            self.selectedDEPARTMENT,        
                            self.selectedACTIVE
                        )
            
            for widget in new_order:
                widget.lift()
        else:
            wMsg = Tix.Label(self.recordFrame, text='No Records Found in Current List.', font=fontMonstrousB)
            wMsg.grid(row=0, column=0, columnspan=self.recordFrame.columnTotal)
            
    def _buildWindowsFields(self, aFrame):
######################################################
# ECE Suppliers CRUD
#('''CREATE TABLE Suppliers(
#       0        suppliersID   INTEGER PRIMARY KEY AUTOINCREMENT  NOT NULL,
#       1        S_NUM         CHAR(10),
#       2        S_NAME        CHAR(30),
#       3        S_ADDR1       CHAR(30),
#       4        S_ADDR2       CHAR(30),
#       5        S_ADDR3       CHAR(30),
#       6        S_ADDR4       CHAR(30),
#       7        S_ADDR5       CHAR(30),
#       8        S_PHONE       CHAR(13),
#       9        S_FAX         CHAR(13),
#       10       S_CNTC        CHAR(20),
#       11       MAN_NUM       CHAR(10),
#       12       PHONE_AREA    CHAR(3),
#       13       PHONE_EXCH    CHAR(3),
#       14       PHONE_LOCAL   CHAR(4),
#       15       FAX_AREA      CHAR(3),
#       16       FAX_EXCH      CHAR(3),
#       17       FAX_LOCAL     CHAR(4),
#       18       DEPARTMENT    CHAR(2),
#       19       ACTIVE        CHAR(1),             
#       20       lastModified  INTEGER,
#       21       updaterID     INTEGER
#
        colTotal = 1
        self.fieldsFrame = AppFrame(aFrame, colTotal)
        self.fieldsFrame.grid(row=0, column=0, sticky=N+S+E+W)
        
        self.idFrame = AppBorderFrame(self.fieldsFrame, 4)
        self.idFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, sticky=N)
        self.idFrame.rowconfigure(self.idFrame.row, weight=1)
        
        self.selectedSuppliersName = AppVerticalFieldFrame(self.idFrame, '''Supplier's Name''', suppliersNameLenght)
        self.selectedSuppliersName.grid(row=self.idFrame.row, column=self.idFrame.column, sticky=N+W)
                
        self.idFrame.column = self.idFrame.column + 1
        self.selectedS_NUM  = AppVerticalFieldFrame(self.idFrame, '''Supplier's Number''', suppliersNumberLenght)
        self.selectedS_NUM.grid(row=self.idFrame.row, column=self.idFrame.column, sticky=N+W)
        
        self.fieldsFrame.row = self.fieldsFrame.row + 1
        self.idFrame.rowconfigure(self.idFrame.row, weight=1)
        self.selectedS_ADDR1 = AppHorizontalFieldFrame(self.fieldsFrame, ''' Addr1 ''', 30)
        self.selectedS_ADDR1.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, sticky=N)
        
        self.fieldsFrame.row = self.fieldsFrame.row + 1
        self.idFrame.rowconfigure(self.idFrame.row, weight=1)
        self.selectedS_ADDR2 = AppHorizontalFieldFrame(self.fieldsFrame, ''' Addr2 ''', 30)
        self.selectedS_ADDR2.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, sticky=N)
        
               
        self.fieldsFrame.row = self.fieldsFrame.row + 1
        self.idFrame.rowconfigure(self.idFrame.row, weight=1)
        self.selectedS_ADDR3 = AppHorizontalFieldFrame(self.fieldsFrame, ''' Addr3 ''', 30)
        self.selectedS_ADDR3.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, sticky=N)
        
        
        self.fieldsFrame.row = self.fieldsFrame.row + 1
        self.idFrame.rowconfigure(self.idFrame.row, weight=1)
        self.selectedS_ADDR4 = AppHorizontalFieldFrame(self.fieldsFrame, ''' Addr4 ''', 30)
        self.selectedS_ADDR4.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, sticky=N)
        
        
        self.fieldsFrame.row = self.fieldsFrame.row + 1
        self.idFrame.rowconfigure(self.idFrame.row, weight=1)
        self.selectedS_ADDR5 = AppHorizontalFieldFrame(self.fieldsFrame, ''' Addr5 ''', 30)
        self.selectedS_ADDR5.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, sticky=N)
        
        
        self.fieldsFrame.row = self.fieldsFrame.row + 1
        self.idFrame.rowconfigure(self.idFrame.row, weight=1)
        self.selectedS_PHONE  = AppHorizontalFieldFrame(self.fieldsFrame, ''' Phone ''', 20)
        self.selectedS_PHONE.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, sticky=N)
        
        
        self.fieldsFrame.row = self.fieldsFrame.row + 1
        self.idFrame.rowconfigure(self.idFrame.row, weight=1)
        self.selectedS_FAX = AppHorizontalFieldFrame(self.fieldsFrame, ''' Fax ''', 20)
        self.selectedS_FAX.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, sticky=N)
        
        
        self.fieldsFrame.row = self.fieldsFrame.row + 1
        self.idFrame.rowconfigure(self.idFrame.row, weight=1)
        self.selectedS_CNTC = AppHorizontalFieldFrame(self.fieldsFrame, ''' CNTC ''', 20)
        self.selectedS_CNTC.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, sticky=N)
        
        
        self.fieldsFrame.row = self.fieldsFrame.row + 1
        self.idFrame.rowconfigure(self.idFrame.row, weight=1)
        self.selectedMAN_NUM = AppHorizontalFieldFrame(self.fieldsFrame, ''' Man_Num ''', 20)
        self.selectedMAN_NUM.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, sticky=N)
        
        
        self.fieldsFrame.row = self.fieldsFrame.row + 1
        self.idFrame.rowconfigure(self.idFrame.row, weight=1)
        self.selectedPHONE_AREA = AppHorizontalFieldFrame(self.fieldsFrame, ''' Area ''', 20)
        self.selectedPHONE_AREA.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, sticky=N)
        
        
        self.fieldsFrame.row = self.fieldsFrame.row + 1
        self.idFrame.rowconfigure(self.idFrame.row, weight=1)
        self.selectedPHONE_EXCH = AppHorizontalFieldFrame(self.fieldsFrame, ''' Exch ''', 20)
        self.selectedPHONE_EXCH.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, sticky=N)
        
        
        self.fieldsFrame.row = self.fieldsFrame.row + 1
        self.idFrame.rowconfigure(self.idFrame.row, weight=1)
        self.selectedPHONE_LOCAL = AppHorizontalFieldFrame(self.fieldsFrame, ''' Local ''', 20)
        self.selectedPHONE_LOCAL.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, sticky=N)
        
        
        self.fieldsFrame.row = self.fieldsFrame.row + 1
        self.idFrame.rowconfigure(self.idFrame.row, weight=1)
        self.selectedFAX_AREA = AppHorizontalFieldFrame(self.fieldsFrame, ''' FArea ''', 20)
        self.selectedFAX_AREA.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, sticky=N)
        
        
        self.fieldsFrame.row = self.fieldsFrame.row + 1
        self.idFrame.rowconfigure(self.idFrame.row, weight=1)
        self.selectedFAX_EXCH = AppHorizontalFieldFrame(self.fieldsFrame, ''' FExch ''', 20)
        self.selectedFAX_EXCH.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, sticky=N)
        
        
        self.fieldsFrame.row = self.fieldsFrame.row + 1
        self.idFrame.rowconfigure(self.idFrame.row, weight=1)
        self.selectedFAX_LOCAL = AppHorizontalFieldFrame(self.fieldsFrame, ''' FLocal ''', 20)
        self.selectedFAX_LOCAL.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, sticky=N)
        
        
        self.fieldsFrame.row = self.fieldsFrame.row + 1
        self.idFrame.rowconfigure(self.idFrame.row, weight=1)
        self.selectedDEPARTMENT = AppHorizontalFieldFrame(self.fieldsFrame, ''' Department ''', 20)
        self.selectedDEPARTMENT.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, sticky=N)
               
        self.fieldsFrame.row = self.fieldsFrame.row + 1
        self.idFrame.rowconfigure(self.idFrame.row, weight=1)
        self.selectedACTIVE = AppHorizontalFieldFrame(self.fieldsFrame, ''' Active ''', 20)
        self.selectedACTIVE.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, sticky=N)
        


        # Empty row for separation
#        empty = AppSpaceRow(self.fieldsFrame)
#        empty.grid(row=self.fieldsFrame.row, column=0, sticky=N+S)
############################################# Last Update Row
        self.fieldsFrame.row = self.fieldsFrame.row + 1
        self.fieldsFrame.column = 0
        self.fieldsFrame.rowconfigure(self.fieldsFrame.row, weight=1)
        
        self.lastUpdate = AppLastUpdateFormat(self.fieldsFrame, self.recList[self.curRecNumV.get()][20], self.recList[self.curRecNumV.get()][21])
        self.lastUpdate.grid(row=self.fieldsFrame.row,column=self.fieldsFrame.column, columnspan = colTotal, sticky=E+S)
               
        self.fieldsDisable()
#        self.fieldsFrame.rowconfigure(self.fieldsFrame.row, weight=0)
#        self.headerList = ['Code', 'Name']
#        for i in range(len(self.headerList)):       
#            aLabel = ttk.Label(self.fieldsFrame, text=self.headerList[i], style='RegularFieldTitle.TLabel')
#            aLabel.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, sticky=N+S)
#            self.fieldsFrame.column = self.fieldsFrame.column + 1

    def displayRec(self, *args):
        ######################################################
        # ECE Parts CRUD (list Table Fields here
        #
        # Example
        # self.selectedCode.load(self.recList[self.curRecNumV.get()][1])
        self.selectedSuppliersName.load(self.recList[self.curRecNumV.get()][2])
        self.selectedS_NUM.load(self.recList[self.curRecNumV.get()][1])
        self.selectedS_ADDR1.load(self.recList[self.curRecNumV.get()][3])
        self.selectedS_ADDR2.load(self.recList[self.curRecNumV.get()][4])
        self.selectedS_ADDR3.load(self.recList[self.curRecNumV.get()][5])
        self.selectedS_ADDR4.load(self.recList[self.curRecNumV.get()][6])
        self.selectedS_ADDR5.load(self.recList[self.curRecNumV.get()][7])
        self.selectedS_PHONE.load(self.recList[self.curRecNumV.get()][8])
        self.selectedS_FAX.load(self.recList[self.curRecNumV.get()][9])
        self.selectedS_CNTC.load(self.recList[self.curRecNumV.get()][10])
        self.selectedMAN_NUM.load(self.recList[self.curRecNumV.get()][11])
        self.selectedPHONE_AREA.load(self.recList[self.curRecNumV.get()][12])    
        self.selectedPHONE_EXCH.load(self.recList[self.curRecNumV.get()][13])
        self.selectedPHONE_LOCAL.load(self.recList[self.curRecNumV.get()][14])
        self.selectedFAX_AREA.load(self.recList[self.curRecNumV.get()][15])
        self.selectedFAX_EXCH.load(self.recList[self.curRecNumV.get()][16])
        self.selectedFAX_LOCAL.load(self.recList[self.curRecNumV.get()][17])
        self.selectedDEPARTMENT.load(self.recList[self.curRecNumV.get()][18])        
        self.selectedACTIVE.load(self.recList[self.curRecNumV.get()][19])
                                    
        if self.accessLevel == 'Root':
            pass
            
        self.fieldsDisable()
                    
    def fieldsClear(self):
        self.selectedSuppliersName.clear() 
        self.selectedS_NUM.clear()
        self.selectedS_ADDR1.clear()
        self.selectedS_ADDR2.clear()
        self.selectedS_ADDR3.clear()
        self.selectedS_ADDR4.clear()
        self.selectedS_ADDR5.clear()
        self.selectedS_PHONE.clear()
        self.selectedS_FAX.clear()
        self.selectedS_CNTC.clear()
        self.selectedMAN_NUM.clear()
        self.selectedPHONE_AREA.clear()    
        self.selectedPHONE_EXCH.clear()
        self.selectedPHONE_LOCAL.clear()
        self.selectedFAX_AREA.clear()
        self.selectedFAX_EXCH.clear()
        self.selectedFAX_LOCAL.clear()
        self.selectedDEPARTMENT.clear()        
        self.selectedACTIVE.clear()
                      
    def fieldsDisable(self):
        self.selectedSuppliersName.disable() 
        self.selectedS_NUM.disable()
        self.selectedS_ADDR1.disable()
        self.selectedS_ADDR2.disable()
        self.selectedS_ADDR3.disable()
        self.selectedS_ADDR4.disable()
        self.selectedS_ADDR5.disable()
        self.selectedS_PHONE.disable()
        self.selectedS_FAX.disable()
        self.selectedS_CNTC.disable()
        self.selectedMAN_NUM.disable()
        self.selectedPHONE_AREA.disable()    
        self.selectedPHONE_EXCH.disable()
        self.selectedPHONE_LOCAL.disable()
        self.selectedFAX_AREA.disable()
        self.selectedFAX_EXCH.disable()
        self.selectedFAX_LOCAL.disable()
        self.selectedDEPARTMENT.disable()        
        self.selectedACTIVE.disable()
                               
    def fieldsEnable(self):
        self.selectedSuppliersName.enable() 
        self.selectedS_NUM.enable()
        self.selectedS_ADDR1.enable()
        self.selectedS_ADDR2.enable()
        self.selectedS_ADDR3.enable()
        self.selectedS_ADDR4.enable()
        self.selectedS_ADDR5.enable()
        self.selectedS_PHONE.enable()
        self.selectedS_FAX.enable()
        self.selectedS_CNTC.enable()
        self.selectedMAN_NUM.enable()
        self.selectedPHONE_AREA.enable()    
        self.selectedPHONE_EXCH.enable()
        self.selectedPHONE_LOCAL.enable()
        self.selectedFAX_AREA.enable()
        self.selectedFAX_EXCH.enable()
        self.selectedFAX_LOCAL.enable()
        self.selectedDEPARTMENT.enable()        
        self.selectedACTIVE.enable()
                
    ################################################################# Redefined commands
    #
    #  invoke from the tableCommonWindows.  These commands are specific to each tables
    #  The specific actions are redefinded here.
    #       
    #    def displayRec(self, *args):
    #        print("Displaying a Record: ", self.curRecNumV.get())
    #        self._displayRecordFrame()
    def duplicate(self):
        self.fieldsEnable()
    
    def find(self):
        #Table specific.  Will be defined in calling class
        self.fieldsClear()
        self.fieldsEnable()

    def refresh(self):
        self._reDisplayRecordFrame()

    def delete(self):
        self._disableWindowCommand()
        aMsg = "Are you sure you want to delete record {0}?".format(self.recList[self.curRecNumV.get()][1])
        ans = Mb.askyesno('Delete Record', aMsg)
        if ans == True:
            AppDB.deleteSuppliers(self.recList[self.curRecNumV.get()][0]) # remove from DB
            aMsg = "Record {0} has been deleted.".format(self.recList[self.curRecNumV.get()][1])
            self.myMsgBar.newMessage('info', aMsg)
            self.recList.pop(self.curRecNumV.get())  # remove from current list
            if self.curRecBeforeOp != 0:
                self.curRecNumV.set(self.curRecBeforeOp - 1)
            else:
                self.curRecNumV.set(self.curRecBeforeOp)
            self._displayRecordFrame()
        self._enableWindowCommand()     
        self.navigationEnable(self.accessLevel)
        self.myDatabaseBar.defaultState()
        self.resetCurOp()
        
    def edit(self):
        self.fieldsEnable()
        self.selectedSuppliersName.focus()
                 
    def add(self):
        self.fieldsClear()
        self.fieldsEnable()
        self.selectedSuppliersName.focus()     # Table Specific
    
    def save(self):
        #
        #  Table specific.  Return a True if succesful saved.
        #
        if self.curOp == 'add':
            self.recListTemp = AppDB.insertSuppliers(
                                                    self.selectedS_NUM.get(),
                                                    self.selectedSuppliersName.get(), 
                                                    self.selectedS_ADDR1.get(),
                                                    self.selectedS_ADDR2.get(),
                                                    self.selectedS_ADDR3.get(),
                                                    self.selectedS_ADDR4.get(),
                                                    self.selectedS_ADDR5.get(),
                                                    self.selectedS_PHONE.get(),
                                                    self.selectedS_FAX.get(),
                                                    self.selectedS_CNTC.get(),
                                                    self.selectedMAN_NUM.get(),
                                                    self.selectedPHONE_AREA.get(),    
                                                    self.selectedPHONE_EXCH.get(),
                                                    self.selectedPHONE_LOCAL.get(),
                                                    self.selectedFAX_AREA.get(),
                                                    self.selectedFAX_EXCH.get(),
                                                    self.selectedFAX_LOCAL.get(),
                                                    self.selectedDEPARTMENT.get(),        
                                                    self.selectedACTIVE.get(),                                               
                                                    convertDateStringToOrdinal(todayDate),
                                                    self.updaterID
                                                   )
            ID = AppDB.getSuppliersID(
                                                    self.selectedS_NUM.get(),
                                                    self.selectedSuppliersName.get(), 
                                                    self.selectedS_ADDR1.get(),
                                                    self.selectedS_ADDR2.get(),
                                                    self.selectedS_PHONE.get(),
                                                    self.selectedS_FAX.get(),
                                                    self.selectedS_CNTC.get(),
                                                    self.selectedMAN_NUM.get(),
                                                    self.selectedDEPARTMENT.get(),        
                                                    self.selectedACTIVE.get(),
                                                    convertDateStringToOrdinal(todayDate),
                                                    self.updaterID                                
                                  )
            
            print("New ID is : ", ID)
            aMsg = "Record {0} has been added.".format(self.selectedSuppliersName.get())
            self.myMsgBar.newMessage('info', aMsg)
                       
            if self.curFind == True:
                #
                #  To remain in find mode, you must add the item to the current find list
                #  and re-order the item in the current list.  Item has been added to the DB
                #  already.  In default mode, it will be pulled from the DB.
                #
                anItem=(
                                                    ID[0][0],
                                                    self.selectedS_NUM.get(),
                                                    self.selectedSuppliersName.get(), 
                                                    self.selectedS_ADDR1.get(),
                                                    self.selectedS_ADDR2.get(),
                                                    self.selectedS_ADDR3.get(),
                                                    self.selectedS_ADDR4.get(),
                                                    self.selectedS_ADDR5.get(),
                                                    self.selectedS_PHONE.get(),
                                                    self.selectedS_FAX.get(),
                                                    self.selectedS_CNTC.get(),
                                                    self.selectedMAN_NUM.get(),
                                                    self.selectedPHONE_AREA.get(),    
                                                    self.selectedPHONE_EXCH.get(),
                                                    self.selectedPHONE_LOCAL.get(),
                                                    self.selectedFAX_AREA.get(),
                                                    self.selectedFAX_EXCH.get(),
                                                    self.selectedFAX_LOCAL.get(),
                                                    self.selectedDEPARTMENT.get(),        
                                                    self.selectedACTIVE.get(),                                               
                                                    convertDateStringToOrdinal(todayDate),
                                                    self.updaterID
                       )

                self.recList.append(anItem)
                self.curRecNumV.set(len(self.recList)-1)               
                aNewList = sorted(self.recList, key=getKey)
                self.recList = aNewList
                newIdx = getIndex(ID[0][0], aNewList)
                self.curRecNumV.set(newIdx)
                self.displayRec()
            else:
                #
                #  If not in find mode, re_display the list and point
                # to the newly added item.
                #           
                self._getNewRecList()
                newIdx = getIndex(ID[0][0], self.recList)
                if newIdx != -1:
                    self.curRecNumV.set(newIdx)                
                self._reDisplayRecordFrame()
            self.resetCurOp()
           
        elif self.curOp == 'edit':
            AppDB.updateSuppliers(
                                                    self.recList[self.curRecNumV.get()][0],
                                                    self.selectedS_NUM.get(),
                                                    self.selectedSuppliersName.get(), 
                                                    self.selectedS_ADDR1.get(),
                                                    self.selectedS_ADDR2.get(),
                                                    self.selectedS_ADDR3.get(),
                                                    self.selectedS_ADDR4.get(),
                                                    self.selectedS_ADDR5.get(),
                                                    self.selectedS_PHONE.get(),
                                                    self.selectedS_FAX.get(),
                                                    self.selectedS_CNTC.get(),
                                                    self.selectedMAN_NUM.get(),
                                                    self.selectedPHONE_AREA.get(),    
                                                    self.selectedPHONE_EXCH.get(),
                                                    self.selectedPHONE_LOCAL.get(),
                                                    self.selectedFAX_AREA.get(),
                                                    self.selectedFAX_EXCH.get(),
                                                    self.selectedFAX_LOCAL.get(),
                                                    self.selectedDEPARTMENT.get(),        
                                                    self.selectedACTIVE.get(),                                               
                                                    convertDateStringToOrdinal(todayDate),
                                                    self.updaterID                                  
                                  )
            aMsg = "Record {0} has been updated.".format(self.selectedSuppliersName.get())
            self.myMsgBar.newMessage('info', aMsg)
            #
            # Add the item again but with modified data.            
            #
            anItem=(
                                                    self.recList[self.curRecNumV.get()][0],
                                                    self.selectedS_NUM.get(),
                                                    self.selectedSuppliersName.get(), 
                                                    self.selectedS_ADDR1.get(),
                                                    self.selectedS_ADDR2.get(),
                                                    self.selectedS_ADDR3.get(),
                                                    self.selectedS_ADDR4.get(),
                                                    self.selectedS_ADDR5.get(),
                                                    self.selectedS_PHONE.get(),
                                                    self.selectedS_FAX.get(),
                                                    self.selectedS_CNTC.get(),
                                                    self.selectedMAN_NUM.get(),
                                                    self.selectedPHONE_AREA.get(),    
                                                    self.selectedPHONE_EXCH.get(),
                                                    self.selectedPHONE_LOCAL.get(),
                                                    self.selectedFAX_AREA.get(),
                                                    self.selectedFAX_EXCH.get(),
                                                    self.selectedFAX_LOCAL.get(),
                                                    self.selectedDEPARTMENT.get(),        
                                                    self.selectedACTIVE.get(),                                               
                                                    convertDateStringToOrdinal(todayDate),
                                                    self.updaterID
                    
                    )
            aRemovedItem = self.recList.pop(self.curRecNumV.get())  # Remove the item that was modified
            self.recList.insert(self.curRecNumV.get(),anItem)
            self.displayRec()
            self.resetCurOp()
            
        elif self.curOp == 'find':
            self.recListTemp = AppDB.findSuppliers(
                                                    self.selectedS_NUM.get(),
                                                    self.selectedSuppliersName.get(), 
                                                    self.selectedS_ADDR1.get(),
                                                    self.selectedS_ADDR2.get(),
                                                    self.selectedS_ADDR3.get(),
                                                    self.selectedS_ADDR4.get(),
                                                    self.selectedS_ADDR5.get(),
                                                    self.selectedS_PHONE.get(),
                                                    self.selectedS_FAX.get(),
                                                    self.selectedS_CNTC.get(),
                                                    self.selectedMAN_NUM.get(),
                                                    self.selectedPHONE_AREA.get(),    
                                                    self.selectedPHONE_EXCH.get(),
                                                    self.selectedPHONE_LOCAL.get(),
                                                    self.selectedFAX_AREA.get(),
                                                    self.selectedFAX_EXCH.get(),
                                                    self.selectedFAX_LOCAL.get(),
                                                    self.selectedDEPARTMENT.get(),        
                                                    self.selectedACTIVE.get()                                               
                                                   )
            if len(self.recListTemp) == 0:
                aMsg = "WARNING: No records were found using the given search criterias."
#                self._reDisplayRecordFrame()
                self.curFind = False
                self.displayCurFunction.setDefaultMode()
                self.myMsgBar.newMessage('warning', aMsg)
            elif len(self.recList) == 1:
                self.curFind = True
                self.recList = self.recListTemp
                self.displayCurFunction.setFindResultMode()
                aMsg = "One record was found using the given search criterias."
                self.curRecNumV.set(0)
                self.myMsgBar.newMessage('info', aMsg)
            else:
                self.curFind = True
                self.recList = self.recListTemp
                self.displayCurFunction.setFindResultMode()
                aMsg = "{0} records were found using the given search criterias.".format(len(self.recList))
                self.curRecNumV.set(0)
                self.myMsgBar.newMessage('info', aMsg)
            self.displayRec()
        return True
    