from popUpWindowClass import popUpWindowClass
import csv
from AppClasses import *
from AppConstants import *
from AppProc import convertDateStringToOrdinal, updateMyAppConstant, getDBLocationFullName
from AppProc import getAYearFromOrdinal
import AppCRUDGolfers as CRUDGolfers
import AppCRUDCourses as CRUDCourses
import AppCRUDTees as CRUDTees
import AppCRUD as CRUD
from AppDialogClass import AppDisplayMessage, AppDisplayErrorMessage, AppQuestionRequest

class createScorecardPopUp(popUpWindowClass):
    def __init__(self, aWindow, aCloseCommand, mode, courseID, loginData):
        #
        #  loginInfo format: Updater Full Name, Login ID, updaterID
        #
        self.windowTitle = 'Build Scorecard'
        popUpWindowClass.__init__(self, aWindow, aCloseCommand, self.windowTitle, loginData, None)
        self.tableName = 'Scorecard'
        self.aCloseCommand = aCloseCommand
        self.aWindow = aWindow
        self.loginData = loginData
        self.openningMode = mode
        self.openningCourseID = courseID
        self.teesTableFontBold = fontBigB
        self.teesTableFont = fontBig
        self.teesTableRelief = 'groove'
        self.configDataRead = []
        self.golferComboBoxData = CRUDGolfers.getGolferCB()
        self.golferDictionary = {}
        self.reverseGolferDictionary = {}
        for i in range(len(self.golferComboBoxData)):
            golfer_full = '''{0} {1}'''.format(self.golferComboBoxData[i][1], self.golferComboBoxData[i][2])
            self.golferDictionary.update({golfer_full: self.golferComboBoxData[i][0]})
            self.reverseGolferDictionary.update({self.golferComboBoxData[i][0]: golfer_full})
        self.bigSpender = None
        #
        #  loginInfo format: Updater Full Name, access_Level, loginID
        #
        self.updaterFullname = loginData[0]
        self.updaterLoginID = self.loginData[2]
        self.displayCurUser.setUserName(self.updaterFullname)

        #######################################################################################
        # Inherited all from tableCommanWindowClass
        #
        #  This class intents to change the mainArea only.  Everything else must remain common
        #
        self.mainAreaRowCount = 0
        self.mainAreaColumnCount = 0
        self._reDisplayRecordFrame()
        self.mainArea.rowconfigure(self.mainAreaRowCount, weight=1)
        if self.openningMode == 'add':
            self.editButton.grid_remove()
            self.addCommand(True)

    def add(self, loadConfiguration = True, *args):
        if loadConfiguration == True:
            self.loadCurrentConfiguration()
        self.fieldsDisable()
        self.selectedCourse.enable()
        self.selectedCourse.focus()
        if self.openningCourseID != None:
            self.selectedCourse.load(self.courseCBData.getName(self.openningCourseID))
            self._courseEnteredAction(None)
            self.selectedDate.focus()
        self.dirty = False

    def setDefaultState(self):
        self.curOp='default'
        self.fieldsDisable()
        self.displayCurFunction.setDefaultMode()
        self.submitButton.disable()
        self.editButton.enable()
        self.cancelButton.disable()
        self.loadCurrentConfiguration()
        self.lastUpdate.load(self.configDataRead[0][11],int(self.configDataRead[0][10]))
        self.deletedV.set('N')  # In the absence of a table, creates the green outline on the main window.
        self.dirty = False

    def loadCurrentConfiguration(self):
        self.configDataRead = updateMyAppConstant(AppConfigFile)
        self.selectedGolfer1.load(self.golferCBData.getName(int(self.configDataRead[0][2])))
        self.selectedGolfer2.load(self.golferCBData.getName(int(self.configDataRead[0][3])))
        self.selectedGolfer3.load(self.golferCBData.getName(int(self.configDataRead[0][4])))
        self.selectedGolfer4.load(self.golferCBData.getName(int(self.configDataRead[0][5])))
        self.selectedMarker.load(self.golferCBData.getName(int(self.configDataRead[0][6])))

    def edit(self, *args):
        pass
        self.curOp='edit'
        self.displayCurFunction.setEditMode()
        self.editButton.disable()
        self.submitButton.enable()
        self.cancelButton.enable()
        self.fieldsEnable()
        self.dirty = False

    def cancel(self, *args):
        pass
        ans = False
        if self.dirty == True:
            aMsg = "The record currently in {0} mode has not been saved.\n" \
                   "Cancelling now would result in lost of data.\n" \
                   "Do you wish to continue with the cancel current command?".format(self.curOp)
            ans = AppQuestionRequest(self, 'Cancel Current Command', aMsg)
            if ans.result == False:
                return

        self.dirty=False
        self.fieldsClear()
        self.add(True)

    def _submitForm(self, *args):
        if self.validateRequiredFields() == True:
            self.save()
        # Must be defined in the popUp window.

    def _clearForm(self, *args):
        self.fieldsClear()
        # Must be defined in the popUp window.

    def tableSpecificMenu(self):
        pass
        '''
        self.viewsmenu.add_command
        self.viewsmenu.add_command(label="Active Golfers", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                   command=lambda viewID='Active Golfers': self.basicViewsList(viewID))

        sortsmenu = Menu(self.menubar, foreground=AppDefaultForeground, font=AppDefaultMenuFont, tearoff=0)
        sortsmenu.add_command(label="Default", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Default': self.sortingList(sortID))
        sortsmenu.add_command(label="Last Name", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Last Name': self.sortingList(sortID))
        sortsmenu.add_command(label="First Name", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='First Name': self.sortingList(sortID))
        sortsmenu.add_command(label="Birthday", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Birthday': self.sortingList(sortID))
        #
        # Next Command actually puts the menu up
        #
        self.menubar.add_cascade(label="Sorts", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                 menu=sortsmenu)
        '''

    def _exitForm(self, *args):
        if self.dirty == True:
            aMsg = "The record currently in {0} mode has not been saved.\n" \
                   "Exiting now would result in lost of data.\n" \
                   "Do you wish to continue with the exit command?".format(self.curOp)
            ans = AppQuestionRequest(self, 'Form Has Unsaved Data', aMsg)
            if ans == True:
                self.aCloseCommand()
        else:
            self.aCloseCommand()

    def sort(self):
        pass
        '''
        if self.curOp == 'default':
            currentID = self.recList[self.curRecNumV.get()][0]
            if self.sortID.get() == 'Default':
                self.recList = sorted(self.recList, key=operator.itemgetter(2,1))
            elif self.sortID.get() == 'Last Name':
                self.recList.sort(key=lambda row: row[2])
            elif self.sortID.get() == 'First Name':
                self.recList.sort(key=lambda row: row[1])
            elif self.sortID.get() == 'Birthday':
                self.recList.sort(key=lambda row: row[4])
            else:
                self.recList.sort(key=lambda row: row[0])
            newIdx = getIndex(currentID, self.recList)
            self.curRecNumV.set(newIdx)
            self.displayRec()
        else:
            aMsg = "ERROR: Invalid request. Must be executed in default state only."
            self.myMsgBar.newMessage('error', aMsg)
        '''

    def _descriptionEnteredAction(self, *args):
        pass

    def _dateEnteredAction(self, *args):
        pass

    def _courseEnteredAction(self, *args):
        if len(self.selectedCourse.get()) > 0:
            try:
                courseID = self.courseCBData.getID(self.selectedCourse.get())
            except:
                return
            self.fieldsEnable()
            self.populateTees()

    def _buildWindowsFields(self, aFrame):
        #        headerList1 = ('BASIC','VALUE','VALUED','MULT','TOLE','SIZE','DESCR','NSN1','NSN2','NSN3','NSN4')
        colTotal = 2
        self.fieldsFrame = AppFieldsFrame(aFrame, colTotal)
        self.fieldsFrame.grid(row=0, column=0, sticky=N + S + E + W)

        ########################################################### Main Identification Section
        self.courseSelectionFrame = AppBorderFrame(self.fieldsFrame, 1)
        self.courseSelectionFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column,
                                 columnspan=self.fieldsFrame.columnTotal, sticky=E + W + N + S)
        self.courseSelectionFrame.addColumn()
        self.courseSelectionFrame.stretchCurrentColumn()

        self.courseCBData = AppCBList(CRUDCourses.getCoursesDictionaryInfo())
        self.golferCBData = AppCBList(CRUDGolfers.getGolfersDictionaryInfo())
        self.selectedCourse = AppSearchLB(self.courseSelectionFrame, self, 'Course', 'V',
                                           self.courseCBData.getList(), courseLongNameWidth, None, self.myMsgBar,
                                           self._aDirtyMethod, self._courseEnteredAction)
        self.selectedCourse.grid(row=self.courseSelectionFrame.row, column=self.courseSelectionFrame.column,
                                  sticky=W + N + S)

        self.courseSelectionFrame.addColumn()
        self.courseSelectionFrame.stretchCurrentColumn()
        self.selectedDate = AppFieldEntryDate(self.courseSelectionFrame, "Game Date", 'V', appDateWidth,
                                              self._dateEnteredAction, self.myMsgBar, self._aDirtyMethod)
        self.selectedDate.grid(row=self.courseSelectionFrame.row, column=self.courseSelectionFrame.column,
                                sticky=W + E + N + S)

        self.courseSelectionFrame.addColumn()
        self.courseSelectionFrame.stretchCurrentColumn()
        self.selectedMarker = AppSearchLB(self.courseSelectionFrame, self, 'Offical Marker', 'V',
                                           self.golferCBData.getList(), AppFullNameWidth, None, self.myMsgBar,
                                           self._aDirtyMethod, None)
        self.selectedMarker.grid(row=self.courseSelectionFrame.row, column=self.courseSelectionFrame.column,
                                  sticky=E + W + N + S)

        # self.tripDateFrame = AppColumnAlignFieldFrame(self.dateFrame)
        # self.tripDateFrame.noBorder()
        # self.tripDateFrame.grid(row=self.dateFrame.row, column=self.dateFrame.column,
        #                         sticky=E + W + N + S)
        #
        # self.selectedStartDate = AppFieldEntryDate(self.tripDateFrame, None, None, appDateWidth,
        #                                    font=          self._dateEnteredAction, self.myMsgBar, self._aDirtyMethod)
        # self.tripDateFrame.addNewField('Trip Start Date', self.selectedStartDate)

        self.fieldsFrame.addRow()
        self.parameterFrame = AppBorderFrame(self.fieldsFrame, 1)
        self.parameterFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column,
                                 columnspan=self.fieldsFrame.columnTotal,sticky=E + W + N + S)

        self.parameterFrame.addColumn()
        self.parameterFrame.stretchCurrentColumn()
        self.selectedGolfer1 = AppSearchLB(self.parameterFrame, self, 'Golfer #1', 'V',
                                           self.golferCBData.getList(), AppFullNameWidth, None, self.myMsgBar,
                                           self._aDirtyMethod, None)
        self.selectedGolfer1.grid(row=self.parameterFrame.row, column=self.parameterFrame.column,
                                  sticky=E + W + N + S)

        self.parameterFrame.addColumn()
        self.parameterFrame.stretchCurrentColumn()
        self.selectedGolfer2 = AppSearchLB(self.parameterFrame, self, 'Golfer #2', 'V',
                                           self.golferCBData.getList(), AppFullNameWidth, None, self.myMsgBar,
                                           self._aDirtyMethod, None)
        self.selectedGolfer2.grid(row=self.parameterFrame.row, column=self.parameterFrame.column,
                                  sticky=E + W + N + S)

        self.parameterFrame.addColumn()
        self.parameterFrame.stretchCurrentColumn()
        self.selectedGolfer3 = AppSearchLB(self.parameterFrame, self, 'Golfer #3', 'V',
                                           self.golferCBData.getList(), AppFullNameWidth, None, self.myMsgBar,
                                           self._aDirtyMethod, None)
        self.selectedGolfer3.grid(row=self.parameterFrame.row, column=self.parameterFrame.column,
                                  sticky=E + W + N + S)

        self.parameterFrame.addColumn()
        self.parameterFrame.stretchCurrentColumn()
        self.selectedGolfer4 = AppSearchLB(self.parameterFrame, self, 'Golfer #4', 'V',
                                           self.golferCBData.getList(), AppFullNameWidth, None, self.myMsgBar,
                                           self._aDirtyMethod, None)
        self.selectedGolfer4.grid(row=self.parameterFrame.row, column=self.parameterFrame.column,
                                  sticky=E + W + N + S)

        # self.parameterFrame.addColumn()
        # self.parameterFrame.stretchCurrentColumn()
        # self.selectedMarker = AppSearchLB(self.parameterFrame, self, 'Offical Marker', 'V',
        #                                    self.golferCBData.getList(), AppFullNameWidth, None, self.myMsgBar,
        #                                    self._aDirtyMethod, None)
        # self.selectedMarker.grid(row=self.parameterFrame.row, column=self.parameterFrame.column,
        #                           sticky=E + W + N + S)
        ########################################################### Main Identification Section
        self.fieldsFrame.addRow()
        self.fieldsFrame.stretchCurrentRow()
        self.teesFrame = AppBorderFrame(self.fieldsFrame, 1)
        self.teesFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column,
                            columnspan = self.fieldsFrame.columnTotal, sticky=E + W + N + S)

        self.teesFrame.addTitle("Available Tees", None)

        self.teeListingColumnCount = 7
        self.teeListingColumnHeader = [' ', 'Tee Name', 'Tee Type', 'Rating', 'Slope', 'Yardage', ' ']
        self.teeListingColumnWidth = [2, 6, 6, 6, 6, 6, 4]
        self.teeListingColumnWeight = [1, 20, 20, 8, 8, 10, 0]  # Weight of 0 remove any strectching of the column

        self._CreateTeeHeaderRow()
        self.teesFrame.addRow()
        self.teesFrame.stretchCurrentRow()

        #
        #  This is where the canvas will go
        #
        self.teesCanvasRow = self.teesFrame.row

        ########################################################### Footer Section
        self.fieldsFrame.addRow()
        self.lastUpdate = AppLastUpdateFormat(self.fieldsFrame)
        self.lastUpdate.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, columnspan=colTotal,
                             sticky=E + N + S)

        self.setRequiredFields()
        self.fieldsDisable()

    def _CreateTeeHeaderRow(self):
        self.teeHeadersRow = AppBorderFrame(self.teesFrame, self.teeListingColumnCount)
        self.teeHeadersRow.grid(row=self.teesFrame.row, column=self.teesFrame.column, columnspan=self.teesFrame.columnTotal, sticky="wens")
        self.teeHeadersRow.removeBorder()

        for i in range(len(self.teeListingColumnHeader)):
            Label(self.teeHeadersRow, border=AppBorderWidthList, relief='flat', justify=LEFT, width=self.teeListingColumnWidth[i],
                  font=self.teesTableFontBold, text=self.teeListingColumnHeader[i]).grid(row=self.teeHeadersRow.row,
                                                                               column=self.teeHeadersRow.column,
                                                                               sticky=E + W + N + S)
            self.teeHeadersRow.addColumn()
            self.teeHeadersRow.stretchSpecifyColumnAndWeight(i, self.teeListingColumnWeight[i])
        self.teeHeadersRow.grid_remove()

    def _createCanvas(self):
        theRow = self.teesCanvasRow
        theColumnSpan = self.teesFrame.columnTotal
        self.teesCanvas = MyScrollFrameVertical(self.teesFrame, theColumnSpan,
                                                self.teeListingColumnCount, theRow)
    def populateTees(self):
        CRUD.initDB(getDBLocationFullName())
        try:
            courseID = self.courseCBData.getID(self.selectedCourse.get())
            currentCourseTeeList = CRUDTees.get_teeList_for_Course_WindowMASS(courseID)
        except:
            try:
                self.teesCanvas.grid_info()
                self.teesCanvas.destroyAll()
                self.teeHeadersRow.grid_remove()
            except:
                pass
            self.teesFrame.changeTitle("No Available Tees")
            CRUD.closeDB()
            return

        self.currentCourseTeeList = currentCourseTeeList
        if len(currentCourseTeeList) > 0:
            self.teesFrame.changeTitle("Available Tees")
            self.teeHeadersRow.grid() # Show the header if tees are available
            #
            #  This try creates the new canvas. If it does exist,
            # destroy the old one and create the new one.
            #
            try:
                self.teesCanvas.grid_info()
                self.teesCanvas.destroyAll()
                self._createCanvas()
            except:
                self._createCanvas()

        else:
            #
            # Rename the title
            #
            self.teesFrame.changeTitle("No Available Tees")
            self.teeHeadersRow.grid_remove() # remove header as well
            try:
                self.teesCanvas.grid_info()
                self.teesCanvas.destroyAll()
                self.teeHeadersRow.grid_remove()
                return
            except:
                return

        self.selectedTee = []
        for i in range(len(currentCourseTeeList)):
            num = len(self.selectedTee)
            aLine = self.teesCanvas.addAFrame(self.teeListingColumnCount)

            Label(aLine, border=AppBorderWidthList, relief=self.teesTableRelief,justify=LEFT, width=self.teeListingColumnWidth[aLine.column],
                  font=self.teesTableFont,
                  text="{0}".format(i + 1)).grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)

            aLine.addColumn()
            Label(aLine, border=AppBorderWidthList, relief=self.teesTableRelief,justify=LEFT, width=self.teeListingColumnWidth[aLine.column],
                  font=self.teesTableFont,
                  text=currentCourseTeeList[i][1]).grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)

            aLine.addColumn()
            Label(aLine, border=AppBorderWidthList, relief=self.teesTableRelief,justify=LEFT, width=self.teeListingColumnWidth[aLine.column],
                  font=self.teesTableFont,
                  text=currentCourseTeeList[i][2]).grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)

            aLine.addColumn()
            Label(aLine, border=AppBorderWidthList, relief=self.teesTableRelief,justify=LEFT, width=self.teeListingColumnWidth[aLine.column],
                  font=self.teesTableFont,
                  text=currentCourseTeeList[i][3]).grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)

            aLine.addColumn()
            Label(aLine, border=AppBorderWidthList, relief=self.teesTableRelief,justify=LEFT, width=self.teeListingColumnWidth[aLine.column],
                  font=self.teesTableFont,
                  text=currentCourseTeeList[i][4]).grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)

            aLine.addColumn()
            Label(aLine, border=AppBorderWidthList, relief=self.teesTableRelief,justify=LEFT, width=self.teeListingColumnWidth[aLine.column],
                  font=self.teesTableFont,
                  text=self._getTotalYardageTee(currentCourseTeeList[i][0])).grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)

            aLine.addColumn()

            self.selectedTee.append(AppCheckMarkV2(aLine, AppBorderWidthList,
                                     lambda event=Event, teeIDListOffset=i: self._teeButtonActionPressed(teeIDListOffset)))
            self.selectedTee[i].grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)
            self.selectedTee[i].addToolTip("Select Tee")

            # aLine.addColumn()
            # self.selectedTee.append(AppCheckMark(aLine, self.teesTableFont, self.teesTableRelief, AppDefaultBackground,
            #                                      self.teeListingColumnWidth[aLine.column]))
            # self.selectedTee[num].setBorderWidth(AppBorderWidthList)
            # self.selectedTee[num].grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)
            #
            #   This allows proper column width
            #

            for j in range(len(self.teeListingColumnWeight)):
                aLine.stretchSpecifyColumnAndWeight(j, self.teeListingColumnWeight[j])

            self.teesCanvas.addRow()
            self.teesCanvas.resetColumn()

        self.teesCanvas.ResizeScrollBar()
        CRUD.closeDB()

    def _teeButtonActionPressed(self, teeIDListOffset):
        #
        #  You can only select 3. Verify if limit exceeded and send message
        # accordingly
        #
        selectedNumberOfTees = 0
        if self.selectedTee[teeIDListOffset].get() == 'Y':
            self.selectedTee[teeIDListOffset].invertSelection(None)
        else:
            for i in range(len(self.currentCourseTeeList)):
                if self.selectedTee[i].get() == 'Y':
                    selectedNumberOfTees = selectedNumberOfTees + 1
            if selectedNumberOfTees >= scoreCardMaxTeeSelection:
                aMsg = "Scorecard Tee Selection Exceeded.\n" \
                       "A maximum of {0} Tees can be selected\n" \
                       "for addition to the scorecard.".format(scoreCardMaxTeeSelection)

                AppDisplayErrorMessage(self, 'Tee Selection Error', aMsg)
            else:
                self.selectedTee[teeIDListOffset].invertSelection(None)

    def _getTotalYardageTee(self, teeID):
        totalYardage = 0
        teeDetails = CRUDTees.get_teeDetails_for_curTeeMASS(teeID)
        for i in range(len(teeDetails)):
            totalYardage = totalYardage + teeDetails[i][1]
        return totalYardage

    def addWindowSpecificCommand(self):
        self.editButton = AppButton(self.commandFrame, self.theCommandEditImage, self.theCommandEditImageGrey, AppBorderWidthList,
                                    self.edit)
        self.commandFrame.addNewCommandButton(self.editButton)
        self.editButton.addToolTip("Edit Form")

        self.cancelButton = AppButton(self.commandFrame, self.theCommandCancelImage, self.theCommandCancelImageGrey,
                                      AppBorderWidthList, self.cancel)
        self.commandFrame.addNewCommandButton(self.cancelButton)
        self.cancelButton.addToolTip("Cancel Current Operation")

    def fieldsDisable(self):
        self.selectedDate.disable()
        self.selectedCourse.disable()
        self.selectedGolfer1.disable()
        self.selectedGolfer2.disable()
        self.selectedGolfer3.disable()
        self.selectedGolfer4.disable()
        self.selectedMarker.disable()

    def fieldsEnable(self):
        self.selectedDate.enable()
        self.selectedCourse.enable()
        self.selectedGolfer1.enable()
        self.selectedGolfer2.enable()
        self.selectedGolfer3.enable()
        self.selectedGolfer4.enable()
        self.selectedMarker.enable()


    def fieldsClear(self):
        self.selectedDate.clear()
        self.selectedCourse.clear()
        self.selectedGolfer1.clear()
        self.selectedGolfer2.clear()
        self.selectedGolfer3.clear()
        self.selectedGolfer4.clear()
        self.selectedMarker.clear()
        self.populateTees()

    def setRequiredFields(self):
        self.selectedCourse.setAsRequiredField()
        self.selectedGolfer1.setAsRequiredField()
        self.selectedGolfer2.setAsRequiredField()
        self.selectedGolfer3.setAsRequiredField()
        self.selectedGolfer4.setAsRequiredField()
        # self.selectedMarker.setAsRequiredField()

    def resetRequiredFields(self):
        self.selectedCourse.resetAsRequiredField()
        self.selectedGolfer1.resetAsRequiredField()
        self.selectedGolfer2.resetAsRequiredField()
        self.selectedGolfer3.resetAsRequiredField()
        self.selectedGolfer4.resetAsRequiredField()
        # self.selectedMarker.resetAsRequiredField()


    def validateRequiredFields(self):
        requiredFieldsEntered = True

        if len(self.selectedCourse.get()) == 0:
            requiredFieldsEntered = False
            self.selectedCourse.focus()

        elif len(self.selectedGolfer1.get()) == 0:
            requiredFieldsEntered = False
            self.selectedGolfer1.focus()

        elif len(self.selectedGolfer2.get()) == 0:
            requiredFieldsEntered = False
            self.selectedGolfer2.focus()


        elif len(self.selectedGolfer3.get()) == 0:
            requiredFieldsEntered = False
            self.selectedGolfer3.focus()


        elif len(self.selectedGolfer4.get()) == 0:
            requiredFieldsEntered = False
            self.selectedGolfer4.focus()

        #
        # elif len(self.selectedMarker.get()) == 0:
        #     requiredFieldsEntered = False
        #     self.selectedMarker.focus()

        if requiredFieldsEntered == False:
            self.myMsgBar.requiredFieldMessage()
        return requiredFieldsEntered

    def _dateEnteredAction(self, *args):
        self._aDirtyMethod(None)

    def _getTeeIDList(self):
        teeIDList = []
        for i in range(len(self.currentCourseTeeList)):
            if self.selectedTee[i].get() == 'Y':
                teeIDList.append(self.currentCourseTeeList[i][0])

        return teeIDList

    def _getPlayerFirstNameList(self):
        playerList = []
        #
        #  For golfers, just keep the first name
        #
        playerList.append(self.selectedGolfer1.get().split(" ")[0])
        playerList.append(self.selectedGolfer2.get().split(" ")[0])
        playerList.append(self.selectedGolfer3.get().split(" ")[0])
        playerList.append(self.selectedGolfer4.get().split(" ")[0])

        return playerList

    def _printScorecard(self, teeIdList, playerList):
        courseID = self.courseCBData.getID(self.selectedCourse.get())
        courseDetails = CRUDCourses.get_course_details(courseID)
        courseName = courseDetails[1]
        shortCourseName1 = courseDetails[2]
        shortCourseName = shortCourseName1.replace(" ", "")
        for i in range(len(teeIdList)):
            myTeeDetails = CRUDTees.get_teeDetails_for_curTee(teeIdList[i])

        myTees = CRUDTees.get_teeList_for_Course_Window(courseID)
        myTeeDetails = CRUDTees.get_teeDetails_for_curTee(myTees[0][0])
        anArray = []

        firstline = []
        firstline.append('Hole')
        for i in range(0, 18):
            firstline.append(i + 1)
        firstline.append(courseName)
        anArray.append(firstline)
        #
        #    Tee Yardage for selected Tees
        #
        for i in range(len(teeIdList)):
            myTeeDetails = CRUDTees.get_teeDetails_for_curTee(teeIdList[i])
            aline = []
            aline.append(CRUDTees.get_tee_name_from_teeID(teeIdList[i]))
            for j in range(0, 18):
                aline.append(myTeeDetails[j][1])
            if i == 0:
                aline.append(shortCourseName1)
            if i == 1:
                aline.append(("MB " + str(year)))
            anArray.append(aline)
        #
        #    Tee Pars for selected Tees
        #
        for i in range(2):
            aline = []
            if i == 0:
                aline.append('Par')
                for i in range(0, 18):
                    aline.append(myTeeDetails[i][3])
            else:
                aline.append('Handicap')
                for i in range(0, 18):
                    aline.append(myTeeDetails[i][2])
            anArray.append(aline)
        #
        #   Add the golfer list
        #
        anArray.append(playerList)

        myAddresses = CRUDAddresses.get_courseAddresses(courseID)
        miscData = []
        miscData.append(
            "{0}\n{1}, {2}, {3}\n{4}\n{5} ".format(myAddresses[0][9], myAddresses[0][2], myAddresses[0][3],
                                                   myAddresses[0][6], myAddresses[0][7], myAddresses[0][8]))
        Creator = "Created by {1}\n{0}".format(str(year), authorName)
        miscData.append(Creator)
        ############################################################################End Misc data for scorecard
        ############################################################################Rating and slope
        myRatingSlope = ''
        for i in range(len(teeIdList)):
            myTeeData = CRUDTees.get_tee_info_for_teeID(teeIdList[i])
            if i == 0:
                myRatingSlope = myRatingSlope + "{0}: {1}/{2}".format(myTeeData[0][1], myTeeData[0][3],
                                                                      myTeeData[0][4])
            else:
                myRatingSlope = myRatingSlope + "\n{0}: {1}/{2}".format(myTeeData[0][1], myTeeData[0][3],
                                                                        myTeeData[0][4])
        miscData.append(myRatingSlope)
        ############################################################################End Rating and slope
        ############################################################################Marker
        if len(self.selectedMarker.get()) > 0:
            miscData.append("Marker: {0}".format(self.selectedMarker.get()))
        else:
            miscData.append("Marker: ")
            ############################################################################End Marker
        ############################################################################Date
        if len(self.selectedDate.get()) > 0:
            miscData.append("Date: {0}".format(self.selectedDate.get()))
        else:
            miscData.append("Date: ")
            ############################################################################End Date
        anArray.append(miscData)

        exportScoreCardFilename = baseScorecardDir + "/" + shortCourseName + "MB" + str(year) + ".csv"

        with open(exportScoreCardFilename, 'w', newline='') as fout:
            for i in range(len(anArray)):
                csv.writer(fout).writerow(anArray[i])

        aMsg = "Your scorecard data for {0} has been saved in the scorecards directory:\n{1}" \
               "\n\nDo you wish to create another scorecard?".format(courseDetails[2], exportScoreCardFilename)
        ans = AppQuestionRequest(self, "Scorecard Data", aMsg)
        return ans.result

    def save(self):
        if self.openningMode == 'add':
            teeIDList = self._getTeeIDList()
            if len(teeIDList) != 3:
                aMsg = "Your scorecard creation generated an error.\n" \
                       "Three tees are required for the MyGolf Scorecard format.\n" \
                       "Please ensure that you have selected exactly three Tees.\n" \
                       "If you are missing Tees, create them to complete the scorecard."
                AppDisplayErrorMessage(self, "Scorecard Creation Error", aMsg)
                return

            playerList = self._getPlayerFirstNameList()

            ans = self._printScorecard(teeIDList, playerList)

            if ans == True:
                self.openningCourseID = None
                self.fieldsClear()
                self.add(False)
            else:
                self.dirty = False
                self._exitForm()
