from tkinter import *
import tkinter.ttk as ttk
from AppConstants import AppTitle, AppMainIcon, AppDefaultForeground, AppDefaultBackground
from AppConstants import AppWelcomeImg, welcomeLogoHeight, welcomeLogoWidth, loginDataFieldWidth
from AppConstants import fontAverage, fontMonstrousB, fontBig, fontBigB, fontBigI
from AppConstants import fontHugeB, fontHuge2I
from AppConstants import commandButtonHeight, commandButtonWidth
from AppConstants import menuActiveBackground, menuActiveForeground
from AppClasses import AppFrame, AppMENUListFrame
from AppClasses import messageBar
from PIL import Image, ImageTk
from AppProc import resizeImage
from AppCRUD import createOpenAppDB
import AppCRUD as AppDB
import AppCRUDLoginAccess as AppCRUDLoginAccess


class App(Tk):
    def __init__(self, parent):
        Tk.__init__(self, parent)

        self.parent = parent
        Tk.protocol(self, 'WM_DELETE_WINDOW', self.evQuit)
        Tk.option_add(self, "*foreground", AppDefaultForeground)  # Would have help before, global setting for the app width =200, height =220
        Tk.option_add(self, "*background", AppDefaultBackground)  # Would have help before, global setting for the app width =200, height =220

        self.style=ttk.Style()
        ################################ General Styles
        self.style.configure('.', font=fontBig , foreground=AppDefaultForeground, background=AppDefaultBackground)
        self.style.configure('WindowTitle.TLabel', font=fontMonstrousB)
        self.style.configure('RegularFieldTitle.TLabel', font=fontBigB)
        self.style.configure('LastUpdateDisplay.TLabel', font=fontBigI)
        self.style.configure('RegularFields.TEntry', font=fontBig, highlightcolor=AppDefaultForeground, highlightthickness = 2)
        ################################ Login Styles
        self.style.configure('Login.TButton', font=fontBigB)
        self.style.configure('LoginFieldTitle.TLabel', font=fontBigB)
        ################################ Menu Style
        self.style.configure('MenuItems.TLabel', font=fontHuge2I)
        ################################ Command Styles (buttons, etc)
        self.style.configure('CommandButton.TButton', font=fontBig, width=commandButtonWidth, height=commandButtonHeight)
        ################################ TooTip Styles
        self.style.configure('toolTip.TLabel', justify='left', foreground=AppDefaultForeground, relief='solid', borderwidth=1,
                             font=fontBig)

        self.container = Frame(self)
        self.container.pack(side=TOP, fill=BOTH, expand=True)
        self.container.grid_rowconfigure(0, weight=1)
        self.container.grid_columnconfigure(0, weight=1)

        self.loginData = []

        self.frames = {}

        self.loginID = ''  # ID number of person logged In
        self.accessLevel = 'TESTING'  # Login person access level

        self.addFrameToApp(mainMenu)
        self.showFrame(mainMenu, menu=False)
#        self.showFrame(TESTING, menu=False)

    def addFrameToApp(self, list_screen_name):
        frame = list_screen_name(self.container, self)
        self.frames[list_screen_name] = frame
        frame.grid(row=0, column=0, sticky="nwse")

    def showFrame(self, cont, menu):
        frame = self.frames[cont]
        frame.tkraise()

    def redrawMenu(self, cont):
        frame = self.frames[cont]
        frame.redraw()

    def evQuit(self):
#        AppDB.closeDB()
        MyGolf.withdraw()
        login.show()

class mainMenu(Frame):
    def __init__(self, parent, controller):
        Frame.__init__(self, parent)
        self.controller = controller
        self.parent = parent
        self.loginData = self.controller.loginData
        self.row = 0
        self.column = 0
        self.accessLevel = " "
        self.updaterID = 0
        self.displayMenu()

    def addRow(self):
        self.row = self.row + 1
        self.rowconfigure(self.row, weight=0)

    def addColumn(self):
        self.column = self.column + 1
        self.columnconfigure(self.column, weight=1)

    def redraw(self):
        self.mainDisplay.destroy()
        self.accessLevel = self.controller.loginData[1]
        self.updaterID = self.controller.loginData[2]
        self.displayMenu()

    def displayMenu(self):
        self.mainDisplayColumnTotal = 3
        self.mainDisplay = AppFrame(self, self.mainDisplayColumnTotal)
        self.mainDisplay.grid(row=self.row, column=self.column, sticky=E + W + S + N)
        wMsg = ttk.Label(self.mainDisplay, text='My Golf Application', style='WindowTitle.TLabel')

        wMsg.grid(row=self.mainDisplay.row, column=self.mainDisplay.column, columnspan=self.mainDisplayColumnTotal)
        self.mainDisplay.addRow()
        #########################################################################################
        # Main Menu Frame
        #
        self.mainMenuFrame = AppMENUListFrame(self.mainDisplay, 1)
        self.mainMenuFrame.grid(row=self.mainDisplay.row, column=self.mainDisplay.column, sticky=N+E+W+S)

        wMsg = ttk.Label(self.mainMenuFrame, text='Main Menu', style='WindowTitle.TLabel')
        wMsg.grid(row=self.mainMenuFrame.row, column=self.mainMenuFrame.column, sticky=N)

        self.mainMenuFrame.addMenuItem('Golfers', self._golfersSelected)
        self.mainMenuFrame.addMenuItem('Courses', self._coursesSelected)
        self.mainMenuFrame.addMenuItem('Expenses', self._expensesSelected)
        self.mainMenuFrame.addMenuItem('Logout', self._logOutSelected)

        #########################################################################################
        # Maintenance Menu Frame
        #
        if self.accessLevel == 'Root' or self.accessLevel == 'Update' or self.accessLevel == 'Maintenance':
            self.mainDisplay.addColumn()
            self.maintenanceMenuFrame = AppMENUListFrame(self.mainDisplay, 1)
            self.maintenanceMenuFrame.grid(row=self.mainDisplay.row, column=self.mainDisplay.column, sticky=N+E+W+S)

            wMsg = ttk.Label(self.maintenanceMenuFrame, text='Maintenance Menu', style='WindowTitle.TLabel')
            wMsg.grid(row=self.maintenanceMenuFrame.row, column=self.maintenanceMenuFrame.column, sticky=N)

            self.maintenanceMenuFrame.addMenuItem('My Golf Data Backup', self._backupSelected)
            self.maintenanceMenuFrame.addMenuItem('My Golf Data Restore', self._restoreSelected)
            self.maintenanceMenuFrame.addMenuItem('Convert My Golf 2015 data', self._restoreSelected)
            self.maintenanceMenuFrame.addMenuItem('Holder', self._restoreSelected)
            self.maintenanceMenuFrame.addMenuItem('Holder', self._restoreSelected)

        #########################################################################################
        # Root Menu Frame
        #
        if self.accessLevel == 'Root':
            self.mainDisplay.addColumn()
            self.rootMenuFrame = AppMENUListFrame(self.mainDisplay, 1)
            self.rootMenuFrame.grid(row=self.mainDisplay.row, column=self.mainDisplay.column, sticky=N+E+W+S)#

            wMsg = ttk.Label(self.rootMenuFrame, text='Root Menu', style='WindowTitle.TLabel')
            wMsg.grid(row=self.rootMenuFrame.row, column=self.rootMenuFrame.column, sticky=N)

            self.rootMenuFrame.addMenuItem('Add Login User', self._backupSelected)

    def _golfersSelected(self, event):
        #
        #  More actions maybe required
        #
        login.show()
        MyGolf.withdraw()

    def _backupSelected(self, event):
        #
        #  More actions maybe required
        #
        login.show()
        MyGolf.withdraw()

    def _restoreSelected(self, event):
        #
        #  More actions maybe required
        #
        login.show()
        MyGolf.withdraw()


    def _logOutSelected(self, event):
        #
        #  More actions maybe required
        #
        login.show()
        MyGolf.withdraw()

    def _coursesSelected(self, event):
        #
        #  More actions maybe required
        #
        login.show()
        MyGolf.withdraw()

    def _expensesSelected(self, event):
        #
        #  More actions maybe required
        #
        login.show()
        MyGolf.withdraw()

class welcomeLoginDisplay:
    def __init__(self, parent):
        self.window = parent
        
    def display(self):
        self.loginWindow = Toplevel()
        self.loginWindow.resizable(False, False)
        self.loginWindow.title(AppTitle)
        self.loginWindow.protocol('WM_DELETE_WINDOW', self.evQuit)
        
        columnCount = 0
        rowCount = 0
            
        original = Image.open(AppWelcomeImg)
        image1 = resizeImage(original,welcomeLogoWidth,welcomeLogoHeight)
        panel1 = Label(self.loginWindow, image=image1)
        self.display = image1
        panel1.grid(row=rowCount, column=columnCount)
            
        columnCount = columnCount + 1
        mainDisplay = Frame(self.loginWindow, border = 8)        
        mainDisplay.grid(row=rowCount, column=columnCount, sticky=E+W+S+N)
            
        mainDisplayRow = 0
        mainDisplayColumn = 0             
        wMsg = ttk.Label(mainDisplay, text=AppTitle, style='WindowTitle.TLabel', font=fontHugeB)
        wMsg.grid(row=mainDisplayRow, column=mainDisplayColumn, sticky=N)
        self.loginWindow.rowconfigure(mainDisplayRow, weight=0)
           
        mainDisplayRow = mainDisplayRow + 1
        loginFieldTitleFrame = Frame(mainDisplay)        
        loginFieldTitleFrame.grid(row=mainDisplayRow, column=mainDisplayColumn, sticky=W+N) 
                 
        loginLine1Title = ttk.Label(loginFieldTitleFrame, text='Login ID:', style='LoginFieldTitle.TLabel', font=fontBigB)
        loginLine1Title.grid(row=0, column=0, sticky=N+E)
            
        self.selectLoginName = Entry(loginFieldTitleFrame, font=fontBigB, justify='left', width=loginDataFieldWidth)
        self.selectLoginName.grid(row=0, column=1, sticky=N+W)

        loginLine2Title = ttk.Label(loginFieldTitleFrame, text='Password:', style='LoginFieldTitle.TLabel', font=fontBigB)
        loginLine2Title.grid(row=1, column=0, sticky=N+E)
        self.selectPassword = Entry(loginFieldTitleFrame, font=fontBigB, justify='left', width=loginDataFieldWidth, show="*")
        self.selectPassword.grid(row=1, column=1, sticky=N+W)
           
        self.loginWindow.rowconfigure(mainDisplayRow, weight=0)
                  
 #       updateECEShopConstant()     
        mainDisplayRow = mainDisplayRow + 1
        startButton = ttk.Button(mainDisplay, text='Login', style='Login.TButton', 
                                 command= self.loginButtonPressed)
        startButton.grid(row=mainDisplayRow, sticky=N)
        
############################################################ Message Bar Area       
        mainDisplayRow = mainDisplayRow + 1
        self.myMsgBar = messageBar(mainDisplay)
        self.myMsgBar.grid(row=mainDisplayRow, column=0, sticky=W+S, padx=5)
        
    def evQuit(self):
#        AppDB.closeDB()  # Close DB to enable saving of database
        MyGolf.quit()
        
    def hide(self):
        self.myMsgBar.clearMessage()
        self.loginWindow.withdraw()
        
    def show(self):
        self.myMsgBar.clearMessage()
        self.loginWindow.deiconify()
        
    def loginButtonPressed(self):
        #
        #  Will need to verify login into the DB.  Currently 
        #  no such scheme is implemented using the sqlite3 engine.
        #
        self.myMsgBar.clearMessage()
#
#  In the absence of login mechanism, use the following lines
#
#        self.window.deiconify()
#        self.hide()
#        self.window.loginData = ['Jean-Luc Derome', 'root', 0]
#        self.window.redrawMenu(mainMenu)
       
# Below is a login mechanism if required.  DB must be configured users with
# loginID and Passwords can be entered.        
#
        if len(self.selectLoginName.get()) > 0:
            createOpenAppDB()
            loginInfo=[]
            loginInfo = AppCRUDLoginAccess.getLoginUser(self.selectLoginName.get())
            print("You have reach this point.")
            AppDB.closeDB()
            print("This is good. loginInfo: ", loginInfo)
            print("TSize of loginInfo: ", len(loginInfo))
            if len(loginInfo) != 1:
                aMsg = "ERROR: Invalid login ID/password combination."
                self.myMsgBar.newMessage('error', aMsg)
            else:
                if self.selectPassword.get() == loginInfo[0][0]:
                    MyGolf.deiconify()
                    self.hide()
                    self.window.loginData = [(self.selectLoginName.get()), (loginInfo[0][1]), (loginInfo[0][2])]
                    self.window.redrawMenu(mainMenu)
                else:
                    print("The error is here.")
                    aMsg = "ERROR: Invalid login ID/password combination."
                    self.myMsgBar.newMessage('error', aMsg)
        else:
            aMsg = "ERROR: LoginID Required."
            self.myMsgBar.newMessage('error', aMsg)
            
if __name__ == '__main__':
    MyGolf = App(None)
    MyGolf.withdraw()
    MyGolf.resizable(False, False)
    MyGolf.wm_title(AppTitle)

    img = ImageTk.PhotoImage(file=AppMainIcon)
    MyGolf.wm_iconphoto(TRUE, img)

    login = welcomeLoginDisplay(MyGolf)
    login.display()

    MyGolf.mainloop()