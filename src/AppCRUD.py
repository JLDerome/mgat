from __future__ import print_function
from __future__ import unicode_literals

import argparse
import sqlite3 as sql
import shutil
import time
import os
from AppProc import getDBLocationFullName, convertDateStringToOrdinal, getBackupdirLocationFullName
from AppConstants import AppDefaultLocation, BackupDirDefaultName, ExportDirDefaultName, todayDate, BackupDirDefaultName

db=None
cursor = None
lidGolfers = 0

def delete(Table, recordID, lastModified, updaterID, NoClose=False):
    # Single delete command
    if NoClose == False:
        initDB(getDBLocationFullName())

    query = '''
                   update {3} set deleted = 'Y', lastModified = "{1}" , updaterID = "{2}"
                   where uniqueID = {0}
            '''.format(recordID, lastModified, updaterID, Table)
    cursor.execute(query)

    if NoClose == False:
        closeDB()

def deleteMass(Table, recordID, lastModified,updaterID):
    # Mass delete, must open and close DB manually
    query = '''
                   update {3} set deleted = 'Y', lastModified = "{1}" , updaterID = "{2}"
                   where uniqueID = {0}
            '''.format(recordID, lastModified, updaterID, Table)
    cursor.execute(query)

def unDelete(Table, recordID, lastModified,updaterID, NoClose=False):
    if NoClose == False:
        initDB(getDBLocationFullName())

    query = '''
                   update {3} set deleted = 'N', lastModified = "{1}" , updaterID = "{2}"
                   where uniqueID = {0}
            '''.format(recordID, lastModified, updaterID, Table)
    cursor.execute(query)

    if NoClose == False:
        closeDB()

def unDeleteMass(Table, recordID, lastModified,updaterID):
    # Mass unDelete, must open and close DB manually
    query = '''
                   update {3} set deleted = 'N', lastModified = "{1}" , updaterID = "{2}"
                   where uniqueID = F{0}
            '''.format(recordID, lastModified, updaterID, Table)
    cursor.execute(query)

def insertRootLoginAccess(loginName,password,access_Level,lastModified,updaterID):
    initDB(getDBLocationFullName())
    query = '''
    insert into LoginAccess 
    (loginName, password, access_Level, golferID, deleted, lastModified,updaterID)
    values (?,?,?,?,?,?,?)'''
    ans = cursor.execute(query,(loginName,password,access_Level,None, 'N', lastModified,updaterID))
    closeDB()
    
def createOpenAppDB():
    # verify if MyGolfDB exist.  First very that the default location dir exist
    # and create it if not.#
    if not os.path.exists(AppDefaultLocation):
        os.makedirs(AppDefaultLocation)
    #
    # Backup directory
    #
    dir = AppDefaultLocation + BackupDirDefaultName
    if not os.path.exists(dir):
        os.makedirs(dir)

    # fullAppDBName = AppDefaultLocation + "\\" + DBDefaultName
    fullAppDBName = getDBLocationFullName()
    #
    #  initDB function will create the model if it does not
    #  already esists.
    #
    initDB(fullAppDBName)

def initDB(filename):
    global db, cursor

    try:
        if not os.path.exists(filename):
            #
            #  myGolfDB does not exist, create an empty myGoldDB.db
            #  in the provided filename
            #
            db = sql.connect(filename)
            cursor = db.cursor()
            cursor.execute('PRAGMA Foreign_Keys=True')

            cursor.execute('''CREATE TABLE LoginAccess(
               uniqueID INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
               loginName            CHAR(15),
               password             CHAR(15),
               access_Level         CHAR(15),
               golferID             INTEGER,
               deleted              char(1),
               lastModified         INTEGER,
               updaterID            INTEGER
               );''')
#               FOREIGN KEY(updaterID) REFERENCES Golfers(golferID)

            cursor.execute('''CREATE TABLE Golfers(
               uniqueID INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
               golferFNAME          CHAR(30)    NOT NULL,
               golferLNAME          CHAR(30)    NOT NULL,
               golferPIC_LOC        CHAR(100),
               golferBirthday       INTEGER,
               deleted              CHAR(1),
               lastModified         INTEGER,
               updaterID            INTEGER
               );''')
            
            cursor.execute('''CREATE TABLE Courses(
               uniqueID            INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
               courseNAME          CHAR(60)    NOT NULL,
               courseSNAME         CHAR(30)    NOT NULL,
               courseLogo          CHAR(100),
               established         CHAR(4),
               deleted              CHAR(1),
               lastModified         INTEGER,
               updaterID            INTEGER
               );''')
            
            cursor.execute('''CREATE TABLE Tees(
               uniqueID INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
               teeNAME         CHAR(15)    NOT NULL,
               teeType         CHAR(8)     NOT NULL,
               teeRATING       REAL        NOT NULL,
               teeSLOPE        SMALLINT    NOT NULL,
               courseID        INTEGER     NOT NULL,
               archived        CHAR(1),
               deleted              CHAR(1),
               lastModified         INTEGER,
               updaterID            INTEGER,
               FOREIGN KEY(courseID) REFERENCES Courses(uniqueID) ON DELETE CASCADE
               );''')

            cursor.execute('''CREATE TABLE TeeDetails(
               uniqueID  INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
               teeYardage   SMALLINT    NOT NULL,
               teeHandicap  SMALLINT    NOT NULL,
               teePar       SMALLINT    NOT NULL,
               teeID        INTEGER    NOT NULL,
               archived        CHAR(1),
               deleted              CHAR(1),
               lastModified         INTEGER,
               updaterID            INTEGER,
               FOREIGN KEY(teeID) REFERENCES Tees(uniqueID) ON DELETE CASCADE
               );''')
 
            # field avgDrv has been changed to total driving yards           
            cursor.execute('''CREATE TABLE Games(
               uniqueID          INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
               gameDate        CHAR(15)    NOT NULL,
               gameNumber      SMALLINT    NOT NULL,
               teeID           INTEGER    NOT NULL,
               golferID        INTEGER    NOT NULL,
               frontScore      SMALLINT,
               backScore       SMALLINT,
               grossScore      SMALLINT,
               hdcpDiff        REAL,
               dEagleCNT       SMALLINT,
               eagleCNT        SMALLINT,
               birdieCNT       SMALLINT,
               parCNT          SMALLINT,
               bogieCNT        SMALLINT,
               dBogieCNT       SMALLINT,
               tBogieCNT       SMALLINT,
               othersCNT       SMALLINT,
               greens          SMALLINT,
               fairways        SMALLINT,
               leftMiss        SMALLINT,
               rightMiss       SMALLINT,
               avgDrive        SMALLINT,    
               totalSands      SMALLINT,
               sandSaves       SMALLINT,
               gameRating      REAL,
               gameSlope       SMALLINT,
               holesPlayed     CHAR(7),
               totalPutts      SMALLINT,
               putts3AndUp     SMALLINT,
               totalYard       SMALLINT,
               courseID        INTEGER,
               deleted         CHAR(1)     NOT NULL,
               lastModified         INTEGER,
               updaterID            INTEGER,
               FOREIGN KEY(golferID) REFERENCES Golfers(uniqueID),
               FOREIGN KEY(teeID) REFERENCES Tees(uniqueID)
               );''')
            #
            #  Field holeDrives contains a distance 3 Char, a R or L
            #  Fairways are deduced from there
            #  Putts deduces the greens and
            #  Sand saves are deduced from sandtraps
            #
            cursor.execute('''CREATE TABLE GameDetails(
               uniqueID       INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
               holeNumber     SMALLINT    NOT NULL,
               holeScore      SMALLINT    NOT NULL,
               holePutts      SMALLINT,
               holeDrives     CHAR(4),
               holeSandTraps  CHAR(1),
               holeWater      CHAR(1),
               gameID         INTEGER     NOT NULL,
               deleted        CHAR(1)     NOT NULL,
               lastModified   INTEGER,
               updaterID      INTEGER,
               FOREIGN KEY(gameID) REFERENCES Games(uniqueID) ON DELETE CASCADE
               );''')

            cursor.execute('''CREATE TABLE Addresses(
               uniqueID  INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
               ownerTableName CHAR(30),
               town           CHAR(30),
               prov_state     CHAR(30),
               country        CHAR(30),
               ownerID        INTEGER    NOT NULL,
               pc_zip         CHAR(10),
               phone          CHAR(10),
               website        CHAR(40),
               street_number  CHAR(60),
               deleted        CHAR(1)     NOT NULL,
               lastModified         INTEGER,
               updaterID            INTEGER
               );''')

            cursor.execute('''CREATE TABLE MBExpenses(
               uniqueID            INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
               expenseDate          INTEGER    NOT NULL,
               expenseItem          CHAR(100)  NOT NULL,
               expenseAmount        REAL       NOT NULL,
               currency             CHAR(5)    NOT NULL,
               USExchangeRate       REAL,
               expenseAdjusted      REAL,
               golferID             INTEGER    NOT NULL,
               deleted              CHAR(1)    NOT NULL,
               lastModified         INTEGER,
               updaterID            INTEGER,
               FOREIGN KEY(golferID) REFERENCES Golfers(uniqueID) ON DELETE CASCADE
               );''')
            
            closeDB()
            
            insertRootLoginAccess('root','q','Root',convertDateStringToOrdinal(todayDate),'1')

        else:
            #
            #  myGolfDB exist, just open it
            #
            db = sql.connect(filename)
            cursor = db.cursor()
            cursor.execute('PRAGMA Foreign_Keys=True')
    except:
        print("Error connecting to", filename)
        cursor = None
        raise

def closeDB():
    #  Check if connection is openned
    if db:
        try:
            cursor.close()
            db.commit()
            db.close()
        except:
            print("problem closing database...")
            raise
        
DESCRIPTION = """
              Create a timestamped SQLite database backup, and
              clean backups older than a defined number of days
              """
NO_OF_DAYS = 7

def sqlite3_backup(dbfile, backupdir):
    """Create timestamped database copy"""
    
    dbfile = getDBLocationFullName()
    backupdir = getBackupdirLocationFullName()

    if not os.path.isdir(backupdir):
        raise Exception("Backup directory does not exist: {}".format(backupdir))

    backup_file = os.path.join(backupdir, os.path.basename(dbfile) +
                               time.strftime("-%Y%m%d-%H%M%S"))

    initDB(dbfile)

    # Lock database before making a backup
    cursor.execute('begin immediate')
    # Make new backup file
    shutil.copyfile(dbfile, backup_file)
    print ("\nCreating {}...".format(backup_file))
    # Unlock database
    db.rollback()
    
    clean_data(backupdir)
    print ("\nBackup update has been successful.")
    
def clean_data(backup_dir):
    """Delete files older than NO_OF_DAYS days"""

    print ("\n------------------------------")
    print ("Cleaning up old backups")

    for filename in os.listdir(backup_dir):
        backup_file = os.path.join(backup_dir, filename)
        if os.stat(backup_file).st_ctime < (time.time() - NO_OF_DAYS * 86400):
            if os.path.isfile(backup_file):
                os.remove(backup_file)
#                print ("Deleting {}...".format(ibackup_file))
#
# Example below on how to parse arguments
#
def get_arguments():
    """Parse the commandline arguments from the user"""

    parser = argparse.ArgumentParser(description=DESCRIPTION)
    parser.add_argument('db_file',
                        help='the database file that needs backed up')
    parser.add_argument('backup_dir',
                         help='the directory where the backup'
                              'file should be saved')
    return parser.parse_args()