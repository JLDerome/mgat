'''
Created on Feb 11, 2016

@author: derome
'''

from MyGolfApp2016.AppConstants import *
from PIL import Image, ImageTk
import os, csv
from MyGolfApp2016 import AppCRUD_FROMECECRUD as AppDB
from dateutil import parser
import datetime

#def create_open_MyGolfDB(controller, self):
#    # verify if MyGolfDB exist.  First very that the default location dir exist
#    # and create it if not.#
#    if not os.path.exists(default_MyGolf_dir):
#        os.makedirs(default_MyGolf_dir)
#        
#    fullMyGolfDBName = default_MyGolf_dir + "\\" + defaultDBName
#    #
#    #  initDB function will create the model if it does not
#    #  already esists.
#    #   
#    myGolfData.initDB(fullMyGolfDBName)
#            
#    # Add remaining screens after DB is openned
#    for F in (Golfer, Course, Tee, Expenses, TestScrollFrame):
#        app.addFrameToMygolf(F)
# 
#    controller.show_frame(Golfer, True)

def getIndex(partID, aList):
    for i in range(len(aList)):
        if int(aList[i][0]) == int(partID):
            return i
    return -1

def getKey(item):
    return(item[1])

def convertOrdinaltoString(ordinalDate):
    aDate=datetime.date.fromordinal(int(ordinalDate))
    aNewDate= (str(aDate.year) +  "-" + dictmonths[str(aDate.month)] + "-" + str(aDate.day))
    return(aNewDate)

def convertOrdinaltoFull(ordinalDate):
#    dt.strftime("%A, %d. %B %Y %I:%M%p")
    aDate=datetime.date.fromordinal(int(ordinalDate))
    aNewDate= aDate.strftime("%A, %d %B %Y")
    return(aNewDate)

def convertDateStringToOrdinal(dateStr):
    dt = parser.parse(dateStr)   # transform a date into a datetime (accepts several formats)
    return dt.toordinal()

def focusNext(self, awidget):
    '''Return the next widget in tab order'''
    awidget = self.tk.call('tk_focusNext', awidget._w)
    if not awidget: return None
#        return self.nametowidget(widget.string)
    nextWidget=self.nametowidget(awidget.string)
    nextWidget.focus()
    return "break"

def widgetEnter(event):
    event.widget.focus()

def widgetLeave(event):
    event.widget.focus_displayof()

def comparePathEquality(path1, path2):
    path1Norm = os.path.normpath(path1)
    path2Norm = os.path.normpath(path2)
    if path1Norm == path2Norm:
        return True
    else:
        return False

def getDBLocationFullName():
    fullAppDBName = AppDefaultLocation + "\\" + DBDefaultName
    return fullAppDBName
    
def createOpenECEShopDB():
    # verify if MyGolfDB exist.  First very that the default location dir exist
    # and create it if not.#
    if not os.path.exists(AppDefaultLocation):
        os.makedirs(AppDefaultLocation)
        
    #
    # Backup directory
    #
    dir = AppDefaultLocation + AppBackupDir
    if not os.path.exists(dir):
        os.makedirs(dir)
      
#    fullAppDBName = AppDefaultLocation + "\\" + DBDefaultName
    fullAppDBName = getDBLocationFullName()
    #
    #  initDB function will create the model if it does not
    #  already esists.
    #   
    AppDB.initDB(fullAppDBName) 
            
def importECEShopConfig(filename):
    data = []     
    with open(filename, 'r') as fin:
        reader = csv.reader(fin)
        for row in reader:
            data.append(row)
    return data

def resizeImage(image,w,h):
    size = (w, h)
    resized = image.resize(size,Image.ANTIALIAS)
    img = ImageTk.PhotoImage(resized)
    return(img)

def showNewFrame(fr_name, controller, self):
    controller.show_frame(fr_name, False)
    
def updateECEShopConstant():
    if os.path.exists(AppConfigFile):
        configDataRead = importECEShopConfig(AppConfigFile)
        return configDataRead
    else:
        return None
