##########################################################
#
#   Departments Table
#
#   This class implements the specific manipulation of the
# data for the ECE Departments Table that the TECH Shop requires.
#

import tkinter.tix as Tix
import tkinter.ttk as ttk
from MyGolfApp2016 import AppCRUD_FROMECECRUD as AppDB
import tkinter.filedialog as Fd
import tkinter.messagebox as Mb

from tkinter.constants import *

from MyGolfApp2016.AppMyClasses import *
from MyGolfApp2016.AppConstants import *

from MyGolfApp2016.AppProcFROMECE import *

from PIL import Image

class departmentsWindow(tableCommonWindowClass):
    def __init__(self, aWindow, aCloseCommand, loginData):
        self.windowTitle = 'Departments'
        tableCommonWindowClass.__init__(self, aWindow, aCloseCommand, self.windowTitle, loginData[1])
        self.loginData = loginData
        self.accessLevel = self.loginData[1]
        self.updaterID = self.loginData[2]
        rankFullName=AppDB.getRankFullName(self.updaterID)
        self.displayCurUser.setUserName(rankFullName)
#######################################################################################
# Inherited all from tableCommanWindowClass
#  
#  This class intents to change the mainArea only.  Everything else must remain common
#       
        self.mainAreaRowCount = 0
        self.mainAreaColumnCount = 0       
        self._getNewRecList()
        self._reDisplayRecordFrame()
        self.mainArea.rowconfigure(self.mainAreaRowCount, weight=1)
        
############################################################ Area to add window specific commands        
#        self.closeButton = ttk.Button(self.commandFrame, text='Test', style='CommandButton.TButton', 
#                                 command= lambda: self.sampleCommand())
#        self.closeButton.grid(row=0, column=0, sticky=N+S)
        

################################################################# Redefined commands
#
#  invoke from the tableCommonWindows.  These commands are specific to each tables
#  The specific actions are redefinded here.
#       
#    def displayRec(self, *args):
#        print("Displaying a Record: ", self.curRecNum)
#        
#    def save(self):
#        #
#        #  Table specific.  Return a True if succesful saved.
#        #
#        print("Saving a record (re-defined)")
#        return True
    

    def _enableWindowCommand(self):
        pass
    #
    #  Enable the table speific commands
    #
    #    self.closeButton.config(state=NORMAL)

    def _disableWindowCommand(self):
        pass
    #
    #  Disable the table speific commands
    #
    #    self.closeButton.config(state=DISABLED)
               
    def sampleCommand(self):
        self.myMsgBar.clearMessage()
        aMsg = 'INFO: Command area, specific to current window ({0})'.format(self.windowTitle)
        self.myMsgBar.newMessage('info', aMsg)

    def _getNewRecList(self):
        self.recList = AppDB.getDepartments()
        self.curRecNumV.set(self.curRecNumV.get()) # forces display of record number
        
    def _reDisplayRecordFrame(self):
        self.recordFrame.destroy()
        self._getNewRecList()
        self._displayRecordFrame()
        if self.curOp == 'add':
            self.fieldsEnable()
        
    def _displayRecordFrame(self):
        self.recordFrame = AppFrame(self.mainArea, 1) 
        self.recordFrame.grid(row=self.mainAreaRowCount, column=self.mainAreaColumnCount, columnspan= self.mainAreaColumnTotal, sticky=N+S+E+W)
        
        if len(self.recList) > 0:
            self._buildDepartmentsFields(self.recordFrame)
            self.recordFrame.rowconfigure(self.recordFrame.row, weight=0)
            if self.curOp != 'add':        
                self.displayRec()
                self.navigationEnable(self.accessLevel)
        else:
            wMsg = Tix.Label(self.recordFrame, text='No Departments Records Found', font=fontMonstrousB)
            wMsg.grid(row=0, column=0, columnspan=self.recordFrame.columnTotal)
            
    def _buildDepartmentsFields(self, aFrame):
        colTotal = 2  # needs to be even
        self.fieldsFrame = AppFrame(aFrame, colTotal)
        self.fieldsFrame.grid(row=0, column=0, sticky=N+S+E+W)

        self.selectedCode = AppHorizontalFieldFrame(self.fieldsFrame, 'Code', 5)
        self.selectedCode.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, columnspan = colTotal, sticky=N+S)
                
        self.fieldsFrame.row = self.fieldsFrame.row + 1
        self.fieldsFrame.rowconfigure(self.fieldsFrame.row, weight=0)

        self.selectedName = AppHorizontalFieldFrame(self.fieldsFrame, '''Department's Name''', departmentNameLenght)
        self.selectedName.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, columnspan = colTotal, sticky=N+S)

        # Empty row for separation
        self.fieldsFrame.row = self.fieldsFrame.row + 1
        self.fieldsFrame.rowconfigure(self.fieldsFrame.row, weight=0)
        empty = AppSpaceRow(self.fieldsFrame)
        empty.grid(row=self.fieldsFrame.row, column=0, sticky=N+S)


        self.fieldsFrame.row = self.fieldsFrame.row + 1
        self.fieldsFrame.rowconfigure(self.fieldsFrame.row, weight=0)
###########################################################################
#  Frame for various Department Name Format
#        
        self.fieldsFrame.column = 0
        self.nameFrame = AppBorderFrame(self.fieldsFrame,2)
        self.nameFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, sticky=N+S+E+W)
        self.nameFrame.rowconfigure(self.nameFrame.row, weight=0)
        
        aLabel = ttk.Label(self.nameFrame, text='''Department's Medium Name: ''', style='RegularFieldTitle.TLabel')
        aLabel.grid(row=self.nameFrame.row, column=self.nameFrame.column, sticky=E+N)
        
        self.nameFrame.column = self.nameFrame.column + 1
        self.nameFrame.columnconfigure(self.nameFrame.column, weight=0)
        self.selectedMidName = AppEntry(self.nameFrame, departmentMidNameLenght)
        self.selectedMidName.grid(row=self.nameFrame.row, column=self.nameFrame.column, sticky=N+W)

        self.nameFrame.row = self.nameFrame.row + 1
        self.nameFrame.rowconfigure(self.nameFrame.row, weight=0)
        self.nameFrame.column = 0
        aLabel = ttk.Label(self.nameFrame, text='''Department's Short Name: ''', style='RegularFieldTitle.TLabel')
        aLabel.grid(row=self.nameFrame.row, column=self.nameFrame.column, sticky=E+N)
        
        self.nameFrame.column = self.nameFrame.column + 1
        self.nameFrame.columnconfigure(self.nameFrame.column, weight=0)
        self.selectedShortName = AppEntry(self.nameFrame, departmentShortNameLenght)
        self.selectedShortName.grid(row=self.nameFrame.row, column=self.nameFrame.column, sticky=N+W)
###########################################################################
#  Frame for Remaining Fields
#        

        self.fieldsFrame.column = self.fieldsFrame.column + 1
        self.remainingFieldsFrame = AppBorderFrame(self.fieldsFrame, 2)
        self.remainingFieldsFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, sticky=N+S+E+W)
        self.remainingFieldsFrame.rowconfigure(self.remainingFieldsFrame.row, weight=0)
        
        aLabel = ttk.Label(self.remainingFieldsFrame, text='''Department's SCA List: ''', style='RegularFieldTitle.TLabel')
        aLabel.grid(row=self.remainingFieldsFrame.row, column=self.remainingFieldsFrame.column, sticky=E+N)
        
        self.remainingFieldsFrame.column = self.remainingFieldsFrame.column + 1
        self.selectedDAList = AppEntry(self.remainingFieldsFrame, departmentSCAListLenght)
        self.selectedDAList.grid(row=self.remainingFieldsFrame.row, column=self.nameFrame.column, sticky=N+W)

        self.remainingFieldsFrame.column = 0
        self.remainingFieldsFrame.row = self.remainingFieldsFrame.row + 1
        self.remainingFieldsFrame.rowconfigure(self.remainingFieldsFrame.row, weight=0)
        aLabel = ttk.Label(self.remainingFieldsFrame, text='''Department's Security: ''', style='RegularFieldTitle.TLabel')
        aLabel.grid(row=self.remainingFieldsFrame.row, column=self.remainingFieldsFrame.column, sticky=E+N)
        
        self.remainingFieldsFrame.column = self.remainingFieldsFrame.column + 1
        self.selectedSecurity = AppEntry(self.remainingFieldsFrame, departmentSecurityLenght)
        self.selectedSecurity.grid(row=self.remainingFieldsFrame.row, column=self.nameFrame.column, sticky=N+W)


#        self.fieldsFrame.column = self.fieldsFrame.column + 1
#        self.selectedSecurity = AppHorizontalFieldFrame(self.fieldsFrame, '''Department's Security''', 9)
#        self.selectedSecurity.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, sticky=N+S)


        # Empty row for separation
#        self.fieldsFrame.row = self.fieldsFrame.row + 1
#        self.fieldsFrame.rowconfigure(self.fieldsFrame.row, weight=0)
#        empty = AppSpaceRow(self.fieldsFrame)
#        empty.grid(row=self.fieldsFrame.row, column=0, sticky=N+S)
        
############################################# Last Update Row
        self.fieldsFrame.row = self.fieldsFrame.row + 1
        self.fieldsFrame.column = 0
        self.fieldsFrame.rowconfigure(self.fieldsFrame.row, weight=0)
        
        self.lastUpdate = AppLastUpdateFormat(self.fieldsFrame, self.recList[self.curRecNumV.get()][7], self.recList[self.curRecNumV.get()][8])
        self.lastUpdate.grid(row=self.fieldsFrame.row,column=self.fieldsFrame.column, columnspan = colTotal, sticky=E+N+S)
               
        # Empty row for separation
        self.fieldsFrame.row = self.fieldsFrame.row + 1
        self.fieldsFrame.rowconfigure(self.fieldsFrame.row, weight=0)
        empty = AppSpaceRow(self.fieldsFrame)
        empty.grid(row=self.fieldsFrame.row, column=0, sticky=N+S)
        
        self.fieldsDisable()
#        self.fieldsFrame.rowconfigure(self.fieldsFrame.row, weight=0)
#        self.headerList = ['Code', 'Name']
#        for i in range(len(self.headerList)):       
#            aLabel = ttk.Label(self.fieldsFrame, text=self.headerList[i], style='RegularFieldTitle.TLabel')
#            aLabel.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, sticky=N+S)
#            self.fieldsFrame.column = self.fieldsFrame.column + 1

    def displayRec(self, *args):
        #   IDX CREATE TABLE Departments(
        #    0   departmentsID           INTEGER PRIMARY KEY AUTOINCREMENT  NOT NULL,
        #    1   departmentsCode         CHAR(3)   NOT NULL,
        #    2   departmentsNAME         CHAR(60)  NOT NULL,
        #    3   departmentsMidSize      CHAR(20),
        #    4   departmentsShort        CHAR(10),
        #    5   departmentsDAList       CHAR(60),
        #    6   departmentsSecurity     CHAR(8),
        #    7   lastModified            INTEGER,
        #    8   updaterID               INTEGER
        #      );''')
        
        self.selectedCode.load(self.recList[self.curRecNumV.get()][1])     
        self.selectedName.load(self.recList[self.curRecNumV.get()][2])
        self.selectedDAList.load(self.recList[self.curRecNumV.get()][5])
        self.selectedMidName.load(self.recList[self.curRecNumV.get()][3])
        self.selectedShortName.load(self.recList[self.curRecNumV.get()][4])
        self.selectedDAList.load(self.recList[self.curRecNumV.get()][5])
        self.selectedSecurity.load(self.recList[self.curRecNumV.get()][6])
        self.lastUpdate.load(self.recList[self.curRecNumV.get()][7], self.recList[self.curRecNumV.get()][8])

        if self.accessLevel == 'Root':
            pass
            
        self.fieldsDisable()
        
    def fieldsDisable(self):
        self.selectedCode.disable()
        self.selectedDAList.disable()
        self.selectedMidName.disable()
        self.selectedName.disable()
        self.selectedShortName.disable()
        self.selectedSecurity.disable()
    
    def fieldsClear(self):
        self.selectedCode.clear()
        self.selectedDAList.clear()
        self.selectedMidName.clear()
        self.selectedName.clear()
        self.selectedShortName.clear()
        self.selectedSecurity.clear()
    
    def fieldsEnable(self):
        self.selectedCode.enable()
        self.selectedDAList.enable()
        self.selectedMidName.enable()
        self.selectedName.enable()
        self.selectedShortName.enable()
        self.selectedSecurity.enable()
################################################################# Redefined commands
#
#  invoke from the tableCommonWindows.  These commands are specific to each tables
#  The specific actions are redefinded here.
#       
#    def displayRec(self, *args):
#        print("Displaying a Record: ", self.curRecNumV.get())
#        self._displayRecordFrame()
    def duplicate(self):
        self.fieldsEnable()
    
    def refresh(self):
        self._reDisplayRecordFrame()

    def delete(self):
        self._disableWindowCommand()
        aMsg = "Are you sure you want to delete record {0}-{1}?".format(self.recList[self.curRecNumV.get()][1],self.recList[self.curRecNumV.get()][2])
        ans = Mb.askyesno('Delete Record', aMsg)
        if ans == True:
            AppDB.deleteDepartments(self.recList[self.curRecNumV.get()][0])
            aMsg = "Record {0}-{1} has been deleted.".format(self.recList[self.curRecNumV.get()][1],self.recList[self.curRecNumV.get()][2])
            self.myMsgBar.newMessage('info', aMsg)
            self._getNewRecList()
            if self.curRecBeforeOp != 0:
                self.curRecNumV.set(self.curRecBeforeOp - 1)
            self._reDisplayRecordFrame()
        self._enableWindowCommand()     
        self.navigationEnable(self.accessLevel)
        self.myDatabaseBar.defaultState()    
        self.curOp = 'default'

    def edit(self):
        self.fieldsEnable()
                 
    def add(self):
        self.fieldsClear()
        self.fieldsEnable()

    def save(self):
        #
        #  Table specific.  Return a True if succesful saved.
        #
        if self.curOp == 'add':
            
            AppDB.insertDepartments(self.selectedCode.get(), self.selectedName.get(),  self.selectedMidName.get(), self.selectedShortName.get(), self.selectedDAList.get(),
                                    self.selectedSecurity.get(), convertDateStringToOrdinal(todayDate), self.updaterID)               
            aMsg = "Record {0}-{1} has been added to the Departments Table.".format(self.selectedCode.get(),self.selectedName.get())
            self.myMsgBar.newMessage('info', aMsg)
            self._getNewRecList()
            self._reDisplayRecordFrame()
            
        elif self.curOp == 'edit':
            pass 
            AppDB.updateDepartments(self.recList[self.curRecNumV.get()][0], self.selectedCode.get(), self.selectedName.get(),  self.selectedMidName.get(), self.selectedShortName.get(), 
                                    self.selectedDAList.get(), self.selectedSecurity.get(), convertDateStringToOrdinal(todayDate), self.updaterID
                                    )
            aMsg = "Record {0}-{1} has been updated to the Departments Table.".format(self.selectedCode.get(),self.selectedName.get())
            self.myMsgBar.newMessage('info', aMsg)
            self._getNewRecList()
            self._reDisplayRecordFrame()
        return True
     


        