from popUpWindowClass import popUpWindowClass
from AppClasses import *
from AppConstants import *
from AppProc import convertDateStringToOrdinal, updateMyAppConstant, convertOrdinaltoString
from AppProc import getAYearFromOrdinal
import AppCRUDGolfers as CRUDGolfers
import AppCRUDExpenses as CRUDExpenses
from addEditExpensePopUp import addEditExpensePopUp
import tkinter.messagebox as Mb

class expenseTripReportPopUp(popUpWindowClass):
    def __init__(self, aWindow, aCloseCommand, loginData):
        #
        #  loginInfo format: Updater Full Name, Login ID, updaterID
        #
        self.windowTitle = 'Myrtle Beach Expense Trip Report'

        popUpWindowClass.__init__(self, aWindow, aCloseCommand, self.windowTitle, loginData, 'report')
        self.tableName = 'MBExpenses'
        self.aCloseCommand = aCloseCommand
        self.aWindow = aWindow
        self.loginData = loginData
#        self.expenseID = expenseID
#        self.updateExpenseList = updateExpenseList
        self.thisExpense = []

        self.configDataRead = updateMyAppConstant(AppConfigFile)
        self.golferComboBoxData = CRUDGolfers.getGolferCB()
        self.golferDictionary = {}
        self.reverseGolferDictionary = {}
        for i in range(len(self.golferComboBoxData)):
            golfer_full = '''{0} {1}'''.format(self.golferComboBoxData[i][1], self.golferComboBoxData[i][2])
            self.golferDictionary.update({golfer_full: self.golferComboBoxData[i][0]})
            self.reverseGolferDictionary.update({self.golferComboBoxData[i][0]: golfer_full})
        self.bigSpender = self.reverseGolferDictionary[int(self.configDataRead[0][9])]
        #
        #  loginInfo format: Updater Full Name, access_Level, loginID
        #
        self.updaterFullname = loginData[0]
        self.updaterID = self.loginData[2]
        self.displayCurUser.setUserName(self.updaterFullname)

        #######################################################################################
        # Inherited all from tableCommanWindowClass
        #
        #  This class intents to change the mainArea only.  Everything else must remain common
        #
        self.mainAreaRowCount = 0
        self.mainAreaColumnCount = 0
        self.expenseListingColumnCount = 8
        self._reDisplayRecordFrame()
        self.mainArea.rowconfigure(self.mainAreaRowCount, weight=1)

        self.loadCurrentConfiguration()
        self.populateExpenses()
        self.clearButton.grid_remove()
        self.setDefaultState()

    def addWindowSpecificCommand(self):
        self.editButton = AppButton(self.commandFrame, self.theCommandEditImage, self.theCommandEditImageGrey, AppBorderWidthList,
                                    self.editCommand)
        self.commandFrame.addNewCommandButton(self.editButton)
        self.editButton.addToolTip("Edit Trip Parameters")

        self.cancelButton = AppButton(self.commandFrame, self.theCommandCancelImage, self.theCommandCancelImageGrey,
                                      AppBorderWidthList, self.cancel)
        self.commandFrame.addNewCommandButton(self.cancelButton)
        self.cancelButton.addToolTip("Cancel Current Operation")

        self.reloadButton = AppButton(self.commandFrame, self.theCommandReloadImage, self.theCommandReloadImageGrey, AppBorderWidthList,
                                      self.reloadExpenses)
        self.commandFrame.addNewCommandButton(self.reloadButton)
        self.reloadButton.addToolTip("Reload Expenses")

    def saveCurrentValues(self):
        self.currentStartDate = self.selectedStartDate.get()
        self.currentEndDate = self.selectedEndDate.get()
        self.currentGolfer1 = self.selectedGolfer1.get()
        self.currentGolfer2 = self.selectedGolfer2.get()
        self.currentGolfer3 = self.selectedGolfer3.get()
        self.currentGolfer4 = self.selectedGolfer4.get()

    def loadCurrentlySavedValues(self):
        self.selectedStartDate.load(self.currentStartDate)
        self.selectedEndDate.load(self.currentEndDate)
        self.selectedGolfer1.load(self.currentGolfer1)
        self.selectedGolfer2.load(self.currentGolfer2)
        self.selectedGolfer3.load(self.currentGolfer3)
        self.selectedGolfer4.load(self.currentGolfer4)

    def setDefaultState(self, *args):
        self.submitButton.disable()
        self.editButton.enable()
        self.reloadButton.enable()
        self.cancelButton.disable()
        self.fieldsDisable()
        self.displayCurFunction.setDefaultMode()
        self.dirty = False

    def cancel(self, *args):
        pass
        ans = False
        if self.dirty == True:
            aMsg = "The record currently in {0} mode has not been saved.\n" \
                   "Cancelling now would result in lost of data.\n" \
                   "Do you wish to continue with the cancel current command?".format(self.curOp)
            ans = Mb.askyesno('Cancel Current Command', aMsg)
            if ans == True:
                self.loadCurrentlySavedValues()
                self.setDefaultState()
        else:
            self.loadCurrentlySavedValues()
            self.setDefaultState()

    def displayEditExpensesWindow(self, ID):
        self.AddEditExpensesWindow = Tix.Toplevel()
        self.AddEditExpensesWindow.transient(self)

        myGeo = self.winfo_geometry()
        myNewGeo = '+{0}+{1}'.format((int(myGeo.split("+")[1]) + 40), (int(myGeo.split("+")[2]) + 40))
        self.AddEditExpensesWindow.geometry(myNewGeo)
        # golferData should replace the 3
        ans = addEditExpensePopUp(self.AddEditExpensesWindow, self.OnAddEditExpensesWindowClose, 'edit', ID, self.reloadExpenses, self.loginData).pack(fill=BOTH, expand=1)

    def reloadExpenses(self, *args):
        try:
            self.expenseCanvas.grid_info()
            self.expenseCanvas.destroyAll()
            self.summaryFrameData.destroy()
            self.populateExpenses()
        except:
            self.populateExpenses()

    def OnAddEditExpensesWindowClose(self):
        self.AddEditExpensesWindow.destroy()

    def add(self, *args):
        pass

    def edit(self, *args):
        pass
        self.submitButton.enable()
        self.editButton.disable()
        self.reloadButton.disable()
        self.cancelButton.enable()
        self.saveCurrentValues()

    def _submitForm(self, *args):
        pass
        if self.validateRequiredFields() == True:
            self.submitButton.disable()
            self.editButton.enable()
            self.reloadButton.enable()
            self.cancelButton.disable()
            self.reloadExpenses()
            self.fieldsDisable()
            self.displayCurFunction.setDefaultMode()
            self.dirty = False

    def _clearForm(self, *args):
        self.fieldsClear()
        # Must be defined in the popUp window.

    def tableSpecificMenu(self):
        pass
        '''
        self.viewsmenu.add_command
        self.viewsmenu.add_command(label="Active Golfers", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                   command=lambda viewID='Active Golfers': self.basicViewsList(viewID))

        sortsmenu = Menu(self.menubar, foreground=AppDefaultForeground, font=AppDefaultMenuFont, tearoff=0)
        sortsmenu.add_command(label="Default", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Default': self.sortingList(sortID))
        sortsmenu.add_command(label="Last Name", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Last Name': self.sortingList(sortID))
        sortsmenu.add_command(label="First Name", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='First Name': self.sortingList(sortID))
        sortsmenu.add_command(label="Birthday", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Birthday': self.sortingList(sortID))
        #
        # Next Command actually puts the menu up
        #
        self.menubar.add_cascade(label="Sorts", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                 menu=sortsmenu)
        '''

    def sort(self):
        pass
        '''
        if self.curOp == 'default':
            currentID = self.recList[self.curRecNumV.get()][0]
            if self.sortID.get() == 'Default':
                self.recList = sorted(self.recList, key=operator.itemgetter(2,1))
            elif self.sortID.get() == 'Last Name':
                self.recList.sort(key=lambda row: row[2])
            elif self.sortID.get() == 'First Name':
                self.recList.sort(key=lambda row: row[1])
            elif self.sortID.get() == 'Birthday':
                self.recList.sort(key=lambda row: row[4])
            else:
                self.recList.sort(key=lambda row: row[0])
            newIdx = getIndex(currentID, self.recList)
            self.curRecNumV.set(newIdx)
            self.displayRec()
        else:
            aMsg = "ERROR: Invalid request. Must be executed in default state only."
            self.myMsgBar.newMessage('error', aMsg)
        '''

    def _buildWindowsFields(self, aFrame):
        #        headerList1 = ('BASIC','VALUE','VALUED','MULT','TOLE','SIZE','DESCR','NSN1','NSN2','NSN3','NSN4')
        colTotal = 2
        self.fieldsFrame = AppFieldsFrame(aFrame, colTotal)
        self.fieldsFrame.grid(row=0, column=0, sticky=N + S + E + W)

        ########################################################### Parameter Section
        self.parameterFrame = AppBorderFrame(self.fieldsFrame, 1)
        self.parameterFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column,
                                 columnspan=self.fieldsFrame.columnTotal,sticky=E + W + N + S)

        self.selectedStartDate = AppFieldEntryDateV2(self.parameterFrame, 'Start Date', 'V', appDateWidth,
                                                   self._dateEnteredAction, self.myMsgBar, self._aDirtyMethod)
        self.selectedStartDate.stretchColumn()
        self.selectedStartDate.grid(row=self.parameterFrame.row, column=self.parameterFrame.column,
                                    sticky=E + W + N + S)

        self.parameterFrame.addColumn()
        self.parameterFrame.stretchCurrentColumn()
        self.selectedEndDate = AppFieldEntryDateV2(self.parameterFrame, 'End Date', 'V', appDateWidth,
                                                   self._dateEnteredAction, self.myMsgBar, self._aDirtyMethod)
        self.selectedEndDate.stretchColumn()
        self.selectedEndDate.grid(row=self.parameterFrame.row, column=self.parameterFrame.column,
                                    sticky=E + W + N + S)

        self.parameterFrame.addColumn()
        self.parameterFrame.stretchCurrentColumn()
        self.golferCBData = AppCBList(CRUDGolfers.getGolfersDictionaryInfo())
        self.selectedGolfer1 = AppSearchLB(self.parameterFrame, self, 'Golfer #1', 'V',
                                           self.golferCBData.getList(), AppFullNameWidth, None, self.myMsgBar,
                                           self._aDirtyMethod, None)
        self.selectedGolfer1.grid(row=self.parameterFrame.row, column=self.parameterFrame.column,
                                  sticky=E + W + N + S)

        self.parameterFrame.addColumn()
        self.parameterFrame.stretchCurrentColumn()
        self.selectedGolfer2 = AppSearchLB(self.parameterFrame, self, 'Golfer #2', 'V',
                                           self.golferCBData.getList(), AppFullNameWidth, None, self.myMsgBar,
                                           self._aDirtyMethod, None)
        self.selectedGolfer2.grid(row=self.parameterFrame.row, column=self.parameterFrame.column,
                                  sticky=E + W + N + S)

        self.parameterFrame.addColumn()
        self.parameterFrame.stretchCurrentColumn()
        self.selectedGolfer3 = AppSearchLB(self.parameterFrame, self, 'Golfer #3', 'V',
                                           self.golferCBData.getList(), AppFullNameWidth, None, self.myMsgBar,
                                           self._aDirtyMethod, None)
        self.selectedGolfer3.grid(row=self.parameterFrame.row, column=self.parameterFrame.column,
                                  sticky=E + W + N + S)

        self.parameterFrame.addColumn()
        self.parameterFrame.stretchCurrentColumn()
        self.selectedGolfer4 = AppSearchLB(self.parameterFrame, self, 'Golfer #4', 'V',
                                           self.golferCBData.getList(), AppFullNameWidth, None, self.myMsgBar,
                                           self._aDirtyMethod, None)
        self.selectedGolfer4.grid(row=self.parameterFrame.row, column=self.parameterFrame.column,
                                  sticky=E + W + N + S)

        self.fieldsFrame.addRow()
        self.fieldsFrame.stretchLargeCurrentColumn()
        self.expenseListFrame = AppBorderFrame(self.fieldsFrame,4)
        self.expenseListFrame.grid(row = self.fieldsFrame.row, column = self.fieldsFrame.column,
                                   sticky=E+W+S+N)
        self.expenseListFrame.addTitle('Expenses for Myrtle Beach Trip Period')
        self.expenseListFrame.stretchCurrentRow()

        # Note the canvas gets created only if expenses have been incured for the trip
        # Therefore the decision will be made by the populateExpenses Method
        #self._createCanvas('new')

        self.fieldsFrame.addColumn()
        self.fieldsFrame.stretchCurrentRow()
        self.expenseResultFrame = AppBorderFrame(self.fieldsFrame,1)
        self.expenseResultFrame.grid(row = self.fieldsFrame.row, column = self.fieldsFrame.column,
                                   sticky=E+W+S+N)
        self.expenseResultFrame.addTitle(''' Expenses' Summary Report ''')

        ########################################################### Footer Section
        self.fieldsFrame.addRow()
        self.lastUpdate = AppLastUpdateFormat(self.fieldsFrame)
        self.lastUpdate.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, columnspan=colTotal,
                             sticky=E + N + S)

        self.setRequiredFields()
        self.fieldsDisable()

    def _createSummaryFrame(self, expenseList):
        self.summaryFrameData = AppBorderFrame(self.expenseResultFrame, 2)
        self.summaryFrameData.grid(row=self.expenseResultFrame.row,
                                   column=self.expenseResultFrame.column,
                                   sticky=E+W+N+S)

        totalExpensesForPeriod = float(0)
        for i in range(len(expenseList)):
            totalExpensesForPeriod =  totalExpensesForPeriod + float(expenseList[i][6])

        aLabel = Label(self.summaryFrameData, text='Myrtle Beach Trip\nTotal Expenses', border = AppBorderWidthList,
                       relief = 'ridge', font=fontLarger)
        aLabel.grid(row=self.summaryFrameData.row, column=self.summaryFrameData.column,
                    sticky=E+W+N+S)

        self.summaryFrameData.addColumn()
        aLabel = Label(self.summaryFrameData, text="$ {0}".format(round(totalExpensesForPeriod, 2)), border = AppBorderWidthList,
                       relief = 'ridge', font=fontMonstrousB)
        aLabel.grid(row=self.summaryFrameData.row, column=self.summaryFrameData.column,
                    sticky=E+W+N+S)

        self.summaryFrameData.addRow()
        self.summaryFrameData.resetColumn()
        aLabel = Label(self.summaryFrameData, text='''Myrtle Beach Trip\nGolfer's Share''', border = AppBorderWidthList,
                       relief = 'ridge', font=fontLarger)
        aLabel.grid(row=self.summaryFrameData.row, column=self.summaryFrameData.column,
                    sticky=E+W+N+S)

        self.summaryFrameData.addColumn()

        golferShare = totalExpensesForPeriod / float(4)
        aLabel = Label(self.summaryFrameData, text="$ {0}".format(round(golferShare, 2)), border = AppBorderWidthList,
                       relief = 'ridge', font=fontMonstrousB)
        aLabel.grid(row=self.summaryFrameData.row, column=self.summaryFrameData.column,
                    sticky=E+W+N+S)

        self.summaryFrameData.addRow()
        self.summaryFrameData.resetColumn()

        golferNameWidth = 12
        golferOwedtoWidth = 16
        golferAmountWidth = 2
        self.expenseReportColList = ['Golfer', 'Paid By', 'Due/Owe', 'Owed To']
        self.expenseReportColListwidth = [golferNameWidth, golferAmountWidth, golferAmountWidth, golferAmountWidth, golferOwedtoWidth]
        self.expenseReportTableFrame = AppBorderFrame(self.summaryFrameData, len(self.expenseReportColList))
        self.expenseReportTableFrame.grid(column=self.summaryFrameData.column,
                                          row=self.summaryFrameData.row,
                                          columnspan=self.summaryFrameData.columnTotal,
                                          sticky=N + S + E + W)

        for i in range(len(self.expenseReportColList)):
            label = Tix.Label(self.expenseReportTableFrame, text=self.expenseReportColList[i], justify=CENTER,
                              font=fontAverageB, bg=AppTableHeaderBackground, width=golferNameWidth, relief = 'ridge', )
            label.grid(column=i, row=self.expenseReportTableFrame.row, sticky=W+N+S+E)

        selGolfers = [self.golferCBData.getID(self.selectedGolfer1.get()),
                      self.golferCBData.getID(self.selectedGolfer2.get()),
                      self.golferCBData.getID(self.selectedGolfer3.get()),
                      self.golferCBData.getID(self.selectedGolfer4.get())]

        golferTotals = self._calculateGolferTotals(selGolfers)
        golferOwed = self._calculateOwedDue(selGolfers, golferTotals, golferShare)
        owedTo = self._calculateOwedTo(selGolfers, golferOwed)
        print("GolferOwed: ", owedTo)

        self.expenseReportTableFrame.resetColumn()
        for i in range(len(selGolfers)):
            self.expenseReportTableFrame.addRow()
            self.expenseReportTableFrame.stretchSpecifyWeightCurrentColumn(7)
            label = Tix.Label(self.expenseReportTableFrame, text=self.golferCBData.getName(selGolfers[i]),
                              justify=CENTER, width=golferNameWidth, relief='ridge')
            label.grid(column=self.expenseReportTableFrame.column, row=self.expenseReportTableFrame.row,
                       sticky=W + N + S + E)

            self.expenseReportTableFrame.addColumn()
            label = Tix.Label(self.expenseReportTableFrame, text='''$ {0}'''.format(round(golferTotals[i], 2)),
                              justify=CENTER, width=golferAmountWidth, relief='ridge')
            label.grid(column=self.expenseReportTableFrame.column, row=self.expenseReportTableFrame.row,
                       sticky=W + N + S + E)

            self.expenseReportTableFrame.addColumn()
            if golferOwed[i] < float(0):
                foregroundValue=AppNegativeNumberColor
            else:
                foregroundValue = AppDefaultForeground
            label = Tix.Label(self.expenseReportTableFrame, text='''$ {0}'''.format(round(golferOwed[i], 2)),
                              foreground = foregroundValue, justify=CENTER,
                              width=golferAmountWidth, relief='ridge')
            label.grid(column=self.expenseReportTableFrame.column, row=self.expenseReportTableFrame.row,
                       sticky=W + N + S + E)

            self.expenseReportTableFrame.addColumn()
            label = Tix.Label(self.expenseReportTableFrame, text='''{0}'''.format(owedTo[i]),
                              justify=CENTER, width=golferOwedtoWidth, relief='ridge')
            label.grid(column=self.expenseReportTableFrame.column, row=self.expenseReportTableFrame.row,
                       sticky=W + N + S + E)

            self.expenseReportTableFrame.stretchSpecifyWeightCurrentColumn(18)

            self.expenseReportTableFrame.resetColumn()


    def _calculateOwedTo(self, selGolfers, golferOwedDueValue):
        #
        # All 4 golfers must be entered before calulation can occur
        #
        positiveList = []
        owedToEntry = []
        for i in range(len(selGolfers)):
            amount1 = golferOwedDueValue[i]
            if amount1 >= float(0):
                positiveList.append(amount1)
            else:
                positiveList.append(float(0))

        for i in range(len(selGolfers)):
            amount1 = float(golferOwedDueValue[i])
            if amount1 >= float(0):
                owedToEntry.append('''Nobody''')
            else:
                entryDone = False
                for j in range(len(positiveList)):
                    if positiveList[j] > float(0):
                        if (amount1 * -1.0) < positiveList[j]:
                            entryDone = True
                            amount2 = float('%.2f' % (positiveList[j] + amount1))
                            positiveList[j] = amount2
                            aString = '''{0} (${1})'''.format(self.golferCBData.getName(selGolfers[j]),
                                                            '%.2f' % (amount1 * -1.0))
                            owedToEntry.append(aString)

                if entryDone == False:
                    owedToEntry.append('''Multiple''')

        for i in range(len(selGolfers)):
            if owedToEntry[i] == 'Multiple':
                line_entry = []
                amountOwed = golferOwedDueValue[i]
                for j in range(len(positiveList)):
                    if positiveList[j] != float(0):
                        if positiveList[j] > float(0):
                            displayAmt = '${0}'.format('%.2f' % (positiveList[j]))
                            amountOwed = float('%.5f' % (amountOwed + positiveList[j]))
                            line_entry.append('{0} ({1})'.format(self.golferCBData.getName(selGolfers[j]), displayAmt))

                for w in range(len(line_entry)):
                    if w == 0:
                        owedToEntry[i] = '''{0}'''.format(line_entry[w])
                    else:
                        owedToEntry[i] = '''{0}\n{1}'''.format(owedToEntry[i], line_entry[w])
        return owedToEntry

    def _calculateOwedDue(self, selGolfers, golferTotals, golferShareValue):
        golferOwedDueValue = []
        for i in range(len(selGolfers)):
            if selGolfers != '' or selGolfers != None:
                amount1 = golferTotals[i]
                amount2 = golferShareValue
                golferOwedDueValue.append(float('%.3f' % (amount1 - amount2)))
        return golferOwedDueValue

    def _calculateGolferTotals(self, selGolfer):
        golferTotals = []
        ordinalStart = convertDateStringToOrdinal(self.selectedStartDate.get())
        ordinalEnd = convertDateStringToOrdinal(self.selectedEndDate.get())
        for i in range(len(selGolfer)):
            resultList = CRUDExpenses.get_MBExpenses_Range_Golfer(ordinalStart, ordinalEnd,
                                                                      int(selGolfer[i]))
            aTotal = float(0)
            if len(resultList) > 0:
                for j in range(len(resultList)):
                    aTotal = aTotal  + float(resultList[j][6])
            golferTotals.append(aTotal)
        return golferTotals

    def populateExpenses(self):

        if self.validateRequiredFields() == False:
            self.fieldsEnable()
            return
        else:
            expenseList = CRUDExpenses.get_MBExpenses_Range(convertDateStringToOrdinal(self.selectedStartDate.get()),
                                                            convertDateStringToOrdinal(self.selectedEndDate.get()))

        if len(expenseList) > 0:
            self._createCanvas()
            col1Width = 3
            dateWidth = 12
            buyerWidth = 15
            descriptionWidth = 23
            amountWidth = 9
            for i in range(len(expenseList)):
                aLine = self.expenseCanvas.addAFrame(self.expenseListingColumnCount)

                Label(aLine, border = AppBorderWidthList, relief='groove', justify=LEFT, width=col1Width,
                      text="{0}".format(i+1)).grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)

                aLine.addColumn()
                Label(aLine, border = AppBorderWidthList, relief='groove', justify=LEFT, width=dateWidth,
                      text="{0}".format(convertOrdinaltoString(expenseList[i][1]))).grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)

                aLine.addColumn()
                Label(aLine, border = AppBorderWidthList, relief='groove', justify=LEFT, width=buyerWidth,
                      text="{0}".format(self.golferCBData.getName(int(expenseList[i][7])))).grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)

                aLine.addColumn()
                Label(aLine, border = AppBorderWidthList, relief='groove', justify=LEFT, width=descriptionWidth,
                      text="{0}".format(expenseList[i][2])).grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)

                aLine.addColumn()
                Label(aLine, border = AppBorderWidthList, relief='groove', justify=LEFT, width=amountWidth,
                      text="$ {0}".format(expenseList[i][3])).grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)

                aLine.addColumn()
                Label(aLine, border = AppBorderWidthList, relief='groove', justify=LEFT, width=amountWidth,
                      text="$ {0}".format(expenseList[i][6])).grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)

                aLine.addColumn()
                aLine.noStretchCurrentColumn()

                aButton = AppButtonForList(aLine, editRecImage, AppBorderWidthList)
                aButton.grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)
                aButton.bind('<Button-1>', lambda mode='edit', expenseID=expenseList[i][0]: self._startDeleteEditExpenseAction('edit', expenseID))

                aLine.addColumn()
                aLine.noStretchCurrentColumn()

                aButton = AppButtonForList(aLine, deleteCommandRecImage, AppBorderWidthList)
                aButton.grid(row=aLine.row, column=aLine.column, sticky=E + W + N + S)
                aButton.bind('<Button-1>', lambda mode='delete', expenseID=expenseList[i][0]: self._startDeleteEditExpenseAction('delete', expenseID))

                self.expenseCanvas.addRow()
            self.expenseCanvas.ResizeScrollBar()
            self._createSummaryFrame(expenseList)
        else:
            self.myMsgBar.noExpensesForPeriodRequested()

    def _startDeleteEditExpenseAction(self, mode, ID, *args):
        if mode == 'edit':
            self.displayEditExpensesWindow(ID)
        else:
            self.deleteActionFromList(self.tableName, ID, self.reloadExpenses)

    def _createCanvas(self):
        theRow = self.expenseListFrame.row
        theColumnSpan = self.expenseListFrame.columnTotal
        self.expenseCanvas = MyScrollFrameVertical(self.expenseListFrame, theColumnSpan,
                                                   self.expenseListingColumnCount, theRow)

    def loadCurrentConfiguration(self):
        if len(self.configDataRead) > 0:
            self.selectedStartDate.load(convertOrdinaltoString(self.configDataRead[0][0]))
            self.selectedEndDate.load(convertOrdinaltoString(self.configDataRead[0][1]))
            self.selectedGolfer1.load(self.golferCBData.getName(int(self.configDataRead[0][2])))
            self.selectedGolfer2.load(self.golferCBData.getName(int(self.configDataRead[0][3])))
            self.selectedGolfer3.load(self.golferCBData.getName(int(self.configDataRead[0][4])))
            self.selectedGolfer4.load(self.golferCBData.getName(int(self.configDataRead[0][5])))

    def fieldsDisable(self):
        pass
        self.selectedStartDate.disable()
        self.selectedEndDate.disable()
        self.selectedGolfer1.disable()
        self.selectedGolfer2.disable()
        self.selectedGolfer3.disable()
        self.selectedGolfer4.disable()


    def fieldsEnable(self):
        pass
        self.selectedStartDate.enable()
        self.selectedEndDate.enable()
        self.selectedGolfer1.enable()
        self.selectedGolfer2.enable()
        self.selectedGolfer3.enable()
        self.selectedGolfer4.enable()


    def fieldsClear(self):
        pass
        self.selectedStartDate.clear()
        self.selectedEndDate.clear()
        self.selectedGolfer1.clear()
        self.selectedGolfer2.clear()
        self.selectedGolfer3.clear()
        self.selectedGolfer4.clear()


    def setRequiredFields(self):
        pass
        self.selectedStartDate.setAsRequiredField()
        self.selectedEndDate.setAsRequiredField()
        self.selectedGolfer1.setAsRequiredField()
        self.selectedGolfer2.setAsRequiredField()
        self.selectedGolfer3.setAsRequiredField()
        self.selectedGolfer4.setAsRequiredField()


    def resetRequiredFields(self):
        pass
        self.selectedStartDate.resetAsRequiredField()
        self.selectedEndDate.resetAsRequiredField()
        self.selectedGolfer1.resetAsRequiredField()
        self.selectedGolfer2.resetAsRequiredField()
        self.selectedGolfer3.resetAsRequiredField()
        self.selectedGolfer4.resetAsRequiredField()


    def validateRequiredFields(self):
        requiredFieldsEntered = True

        if len(self.selectedStartDate.get()) == 0:
            requiredFieldsEntered = False
            self.selectedStartDate.focus()

        elif len(self.selectedEndDate.get()) == 0:
            requiredFieldsEntered = False
            self.selectedEndDate.focus()

        elif len(self.selectedGolfer1.get()) == 0:
            requiredFieldsEntered = False
            self.selectedGolfer1.focus()

        elif len(self.selectedGolfer2.get()) == 0:
            requiredFieldsEntered = False
            self.selectedGolfer2.focus()

        elif len(self.selectedGolfer3.get()) == 0:
            requiredFieldsEntered = False
            self.selectedGolfer3.focus()

        elif len(self.selectedGolfer4.get()) == 0:
            requiredFieldsEntered = False
            self.selectedGolfer4.focus()

        if requiredFieldsEntered == False:
            self.myMsgBar.requiredFieldMessage()
            return False

        startDate = convertDateStringToOrdinal(self.selectedStartDate.get())
        endDate = convertDateStringToOrdinal(self.selectedEndDate.get())

        if startDate > endDate:
            self.myMsgBar.endDateOnOrAfterStartMessage()
            return False
        else:
            return requiredFieldsEntered

    def _dateEnteredAction(self, *args):
        self._aDirtyMethod(None)

    def _anAmountEntered(self, *args):
        pass
        #self._updateRevisedAmount()

    def _updateRevisedAmount(self):
        pass
        #if self.selectedAmount.get() == None or len(self.selectedCurrency.get()) == 0 or self.selectedRate.get() == None:
        #    pass
        #else:
        #    currency = self.selectedCurrency.get()
        #    if currency== 'CDN':
        #        self.selectedAdjustedAmnt.load(self.selectedAmount.get())
        #    elif currency== 'US':
        #        self.selectedAdjustedAmnt.load(self.selectedAmount.get() * self.selectedRate.get())

    def save(self):
        pass
