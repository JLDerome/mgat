'''
Lending library database API

Provides a CRUD interface to item and member entities 
and init and close functions for database control.
'''

import sqlite3 as sql
import os
import csv

db=None
cursor = None
lidGolfers = 0

##### Database init and close #######

def initDB(filename):
    global db, cursor

    try:
        if not os.path.exists(filename):
            #
            #  myGolfDB does not exist, create an empty myGoldDB.db
            #  in the provided filename
            #
            db = sql.connect(filename)
            cursor = db.cursor()
            cursor.execute('PRAGMA Foreign_Keys=True')

            cursor.execute('''CREATE TABLE Golfers(
               golferID INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
               golferFNAME          CHAR(30)    NOT NULL,
               golferLNAME          CHAR(30)    NOT NULL,
               golferPIC_LOC        CHAR(100),
               golferBirthday       INTEGER
               );''')
            
            cursor.execute('''CREATE TABLE Courses(
               courseID INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
               courseNAME          CHAR(60)    NOT NULL,
               courseSNAME         CHAR(30)    NOT NULL,
               courseLogo          CHAR(100)
               );''')
            
            cursor.execute('''CREATE TABLE Tees(
               teeID INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
               teeNAME         CHAR(15)    NOT NULL,
               teeType         CHAR(8)     NOT NULL,
               teeRATING       REAL        NOT NULL,
               teeSLOPE        SMALLINT    NOT NULL,
               courseID        INTEGER     NOT NULL,
               archived        SMALLINT,   
               FOREIGN KEY(courseID) REFERENCES Courses(courseID) ON DELETE CASCADE
               );''')

            cursor.execute('''CREATE TABLE TeeDetails(
               teeDetailID  INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
               teeYardage   SMALLINT    NOT NULL,
               teeHandicap  SMALLINT    NOT NULL,
               teePar       SMALLINT    NOT NULL,
               teeID        INTEGER    NOT NULL,
               FOREIGN KEY(teeID) REFERENCES Tees(teeID) ON DELETE CASCADE
               );''')
 
            # field avgDrv has been changed to total driving yards           
            cursor.execute('''CREATE TABLE Games(
               gameID          INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
               gameDate        CHAR(15)    NOT NULL,
               gameNumber      SMALLINT    NOT NULL,
               teeID           SMALLINT    NOT NULL,
               golferID        SMALLINT    NOT NULL,
               frontScore      SMALLINT,
               backScore       SMALLINT,
               grossScore      SMALLINT,
               hdcpDiff        REAL,
               dEagleCNT       SMALLINT,
               eagleCNT        SMALLINT,
               birdieCNT       SMALLINT,
               parCNT          SMALLINT,
               bogieCNT        SMALLINT,
               dBogieCNT       SMALLINT,
               tBogieCNT       SMALLINT,
               othersCNT       SMALLINT,
               greens          SMALLINT,
               fairways        SMALLINT,
               leftMiss        SMALLINT,
               rightMiss       SMALLINT,
               avgDrive        SMALLINT,    
               totalSands      SMALLINT,
               sandSaves       SMALLINT,
               gameRating      REAL,
               gameSlope       SMALLINT,
               holesPlayed     CHAR(7),
               totalPutts      SMALLINT,
               putts3AndUp     SMALLINT,
               totalYard       SMALLINT,
               courseID        SMALLINT,
               FOREIGN KEY(golferID) REFERENCES Golfers(golferID),
               FOREIGN KEY(teeID) REFERENCES Tees(teeID) ON DELETE CASCADE
               );''')

            cursor.execute('''CREATE TABLE GameDetails(
               gameDetailID  INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
               holeNumber    SMALLINT    NOT NULL,
               holeScore     SMALLINT    NOT NULL,
               holePutts     SMALLINT,
               holeGreens    SMALLINT,
               holeFairways  CHAR(4),
               holeDriveDistance  SMALLINT,
               holeSands     CHAR(4),
               gameID        SMALLINT    NOT NULL,
               FOREIGN KEY(gameID) REFERENCES Games(gameID)
               );''')

            cursor.execute('''CREATE TABLE Addresses(
               AddrID  INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
               ownerTableName CHAR(30),
               town           CHAR(30),
               prov_state     CHAR(30),
               country        CHAR(30),
               ownerID        SMALLINT    NOT NULL,
               pc_zip         CHAR(10),
               phone          CHAR(10),
               website        CHAR(40),
               street_number  CHAR(60)
               );''')

            cursor.execute('''CREATE TABLE MBExpenses(
               expenseID            INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
               expenseDate          INTEGER    NOT NULL,
               expenseItem          CHAR(100)  NOT NULL,
               expenseAmount        REAL       NOT NULL,
               currency             CHAR(5)    NOT NULL,
               USExchangeRate       REAL,
               expenseAdjusted      REAL,
               golferID             INTEGER    NOT NULL,
               FOREIGN KEY(golferID) REFERENCES Golfers(golferID) ON DELETE CASCADE
               );''')
 
            
            
#            cursor.execute('''CREATE TABLE TeeHoles(
#               teeHOLEID       INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
#               teeHoleNUMBER   SMALLINT    NOT NULL,
#               teeHoleHANDICAP SMALLINT    NOT NULL,
#               teeHoleYARDAGE  SMALLINT    NOT NULL,
#               teeHolePar      SMALLINT    NOT NULL
#               );''')
            
        else:
            #
            #  myGolfDB exist, just open it
            #
            db = sql.connect(filename)
            cursor = db.cursor()
            cursor.execute('PRAGMA Foreign_Keys=True')
    except:
        print("Error connecting to", filename)
        cursor = None
        raise

def closeDB():
    #  Check if connection is openned
    if db:
        try:
            cursor.close()
            db.commit()
            db.close()
        except:
            print("problem closing database...")
            raise

def lastRowID():
    return(cursor.lastrowid)