##########################################################
#
#   Departments Table
#
#   This class implements the specific manipulation of the
# data for the ECE Departments Table that the TECH Shop requires.
#

import tkinter.tix as Tix
import tkinter.ttk as ttk
#import ECECRUD as AppDB
#import ECECRUDPerson as AppDBPerson
#from ECECRUDDepartments import getDepartmentsDictionaryInfo, getDepartmentsCode
#from ECEAppDialogClass import AppDuplicatePerson, AppDisplayAbout, AppQuestionRequest
#import tkinter.filedialog as Fd
#import tkinter.messagebox as Mb
import textwrap, re, operator

from tkinter.constants import *

#from ECEAppMyClasses import *
#from ECEAppConstants import *

#from ECEShopProc import *

from PIL import Image

import tkinter.tix as Tix
import tkinter.ttk as ttk
import AppCRUD as AppCRUD
import AppCRUDGolfers as CRUDGolfers
import AppCRUDGames as CRUDGames
import AppCRUDTees as CRUDTees
import AppCRUDCourses as CRUDCourses
import tkinter.filedialog as Fd
import tkinter.messagebox as Mb
import textwrap, re
from dateutil import parser
from datetime import datetime

from tkinter.constants import *

from tableCommonWindowClass import tableCommonWindowClass
from AppClasses import AppFieldsFrame, AppBorderFrame, AppFrame, AppPictureFrame
from AppClasses import AppUserIDFrame, AppLastUpdateFormat, MyScrollFrameVertical, AppFieldEntryDate
from AppClasses import AppCBList, AppSearchLB, AppStdButton, gameCellColumnDataFrame
from AppConstants import golferDefaultPictureName, appGolferPICDir, golferPicWidth, golferPicHeight
from AppConstants import appDateWidth, fontAverageB,AppStdForeground, AppDefaultForeground
from AppConstants import gameListingButtonWidth, strdate, todayDate

from AppProc import convertDateStringToOrdinal, getIndex, convertOrdinaltoString,getDBLocationFullName

courseList = []
golfersList = []
teeList = []

class golfersWindow(tableCommonWindowClass):
    def __init__(self, aWindow, aCloseCommand, loginData):
        #
        #  loginInfo format: Updater Full Name, access_Level, loginID
        #
        self.windowTitle = 'Golfers'
        self.tableName = 'Golfers'
        tableCommonWindowClass.__init__(self, aWindow, aCloseCommand, self.windowTitle, loginData)
        self.loginData = loginData
        self.accessLevel = self.loginData[1]
        self.updaterID = self.loginData[2]
        self.updaterFullname = self.loginData[0]
        self.aCloseCommand = aCloseCommand
        self.aWindow = aWindow
        self.displayStartDateForGame = None
        self.displayEndDateForGame = None
        self.displayGameForCourse = None
        self.display9Holes = True
        self.displayToday = False
        self.gamesListingColumnCount = 14

        self.courseComboBoxData = CRUDCourses.getCourseCB()
        self.courseDictionary = {}
        self.reverseCourseDictionary = {}
        for i in range(len(self.courseComboBoxData)):
            self.courseDictionary.update({self.courseComboBoxData[i][1]: self.courseComboBoxData[i][0]})
            self.reverseCourseDictionary.update({self.courseComboBoxData[i][0]: self.courseComboBoxData[i][1]})

        self.teeComboBoxData = CRUDTees.get_tees()
        self.teeDictionary = {}
        self.reverseTeeDictionary = {}
        self.reverseTeeDictionaryCourseID = {}
        for i in range(len(self.teeComboBoxData)):
            self.teeDictionary.update({self.teeComboBoxData[i][1]: self.teeComboBoxData[i][0]})
            self.reverseTeeDictionary.update({self.teeComboBoxData[i][0]: self.teeComboBoxData[i][1]})
            self.reverseTeeDictionaryCourseID.update({self.teeComboBoxData[i][0]: self.teeComboBoxData[i][5]})

        #
        #  loginInfo format: password, access_Level, loginID, updaterID
        #
        self.updaterFullname = loginData[0]
        self.updaterID = self.loginData[2]
        self.displayCurUser.setUserName(self.updaterFullname)

        #######################################################################################
        # Inherited all from tableCommanWindowClass
        #
        #  This class intents to change the mainArea only.  Everything else must remain common
        #
        self.mainAreaRowCount = 0
        self.mainAreaColumnCount = 0
        self._reDisplayRecordFrame()
        self.mainArea.rowconfigure(self.mainAreaRowCount, weight=1)
        self.windowViewDefault()

    ############################################################ Area to add window specific commands
    #        self.closeButton = ttk.Button(self.commandFrame, text='Test', style='CommandButton.TButton',
    #                                 command= lambda: self.sampleCommand())
    #        self.closeButton.grid(row=0, column=0, sticky=N+S)


    ################################################################# Redefined commands
    #
    #  invoke from the tableCommonWindows.  These commands are specific to each tables
    #  The specific actions are redefinded here.
    #
    #    def displayRec(self, *args):
    #        print("Displaying a Record: ", self.curRecNum)
    #
    #    def save(self):
    #        #
    #        #  Table specific.  Return a True if succesful saved.
    #        #
    #        print("Saving a record (re-defined)")
    #        return True
    def tableSpecificMenu(self):
        pass
        '''
        self.viewsmenu.add_command
        self.viewsmenu.add_command(label="Active Golfers", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                   command=lambda viewID='Active Golfers': self.basicViewsList(viewID))

        sortsmenu = Menu(self.menubar, foreground=AppDefaultForeground, font=AppDefaultMenuFont, tearoff=0)
        sortsmenu.add_command(label="Default", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Default': self.sortingList(sortID))
        sortsmenu.add_command(label="Last Name", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Last Name': self.sortingList(sortID))
        sortsmenu.add_command(label="First Name", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='First Name': self.sortingList(sortID))
        sortsmenu.add_command(label="Birthday", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Birthday': self.sortingList(sortID))
        #
        # Next Command actually puts the menu up
        #
        self.menubar.add_cascade(label="Sorts", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                 menu=sortsmenu)
        '''
    def sort(self):
        pass
        '''
        if self.curOp == 'default':
            currentID = self.recList[self.curRecNumV.get()][0]
            if self.sortID.get() == 'Default':
                self.recList = sorted(self.recList, key=operator.itemgetter(2,1))
            elif self.sortID.get() == 'Last Name':
                self.recList.sort(key=lambda row: row[2])
            elif self.sortID.get() == 'First Name':
                self.recList.sort(key=lambda row: row[1])
            elif self.sortID.get() == 'Birthday':
                self.recList.sort(key=lambda row: row[4])
            else:
                self.recList.sort(key=lambda row: row[0])
            newIdx = getIndex(currentID, self.recList)
            self.curRecNumV.set(newIdx)
            self.displayRec()
        else:
            aMsg = "ERROR: Invalid request. Must be executed in default state only."
            self.myMsgBar.newMessage('error', aMsg)
        '''

    def _enableWindowCommand(self):
        #
        #  Any command specific must added here
        #
        pass

    #        self.closeButton.config(state=NORMAL)

    def _disableWindowCommand(self):
        #
        #  Any command specific must added here
        #
        pass

    #        self.closeButton.config(state=DISABLED)

    def getTableData(self):
        self.recAll = CRUDGolfers.getGolfers()

    def _buildWindowsFields(self, aFrame):

        #        headerList1 = ('BASIC','VALUE','VALUED','MULT','TOLE','SIZE','DESCR','NSN1','NSN2','NSN3','NSN4')
        colTotal = 2
        self.fieldsFrame = AppFieldsFrame(aFrame, colTotal)
        self.fieldsFrame.grid(row=0, column=0, sticky=N + S + E + W)

        ########################################################### Main Identification Section
        self.identificationFrame = AppBorderFrame(self.fieldsFrame, 2)
        self.identificationFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column,
                                      columnspan=1, sticky=E + W + N + S)
        self.identificationFrame.addTitle("Identification Section")

        self.identificationFrame.noStretchColumn(0)

        self.identificationPICFrame = AppFrame(self.identificationFrame, 1)
        self.identificationPICFrame.grid(row=self.identificationFrame.row, column=self.identificationFrame.column,
                                         sticky=N + W + E + S)


        self.selectedPicture = AppPictureFrame(self.identificationPICFrame, golferDefaultPictureName, appGolferPICDir,
                                               golferPicWidth, golferPicHeight, self.myMsgBar)
        self.selectedPicture.grid(row=self.identificationPICFrame.row, column=self.identificationPICFrame.column,
                                  sticky=N + W + E + S)


        self.selectedPicture = AppPictureFrame(self.identificationPICFrame, golferDefaultPictureName, appGolferPICDir,
                                               golferPicWidth, golferPicHeight, self.myMsgBar)
        self.selectedPicture.grid(row=self.identificationPICFrame.row, column=self.identificationPICFrame.column,
                                  sticky=N + W + E + S)

        self.identificationFrame.addColumn()
        self.identificationDATAFrame = AppFrame(self.identificationFrame, 1)
        self.identificationDATAFrame.grid(row=self.identificationFrame.row, column=self.identificationFrame.column,
                                          sticky=N + W + E + S)

        self.selectedUserParticular = AppUserIDFrame(self.identificationDATAFrame, self.myMsgBar)
        self.selectedUserParticular.grid(row=self.identificationDATAFrame.row, column=self.identificationDATAFrame.column,
                                         sticky=N + W + E + S)
        ########################################################### Stats Section
        self.fieldsFrame.addColumn()
        self.statsFrame = AppBorderFrame(self.fieldsFrame, 1)
        self.statsFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column,
                                      columnspan=1, sticky=E + W + N + S)
        self.statsFrame.addTitle("Statistics Section")


        ########################################################### Games Section
        self.fieldsFrame.addRow()
        self.fieldsFrame.stretchCurrentRow()
        self.fieldsFrame.resetColumn()
        self.gamesFrame = AppBorderFrame(self.fieldsFrame, 4)
        self.gamesFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column,
                                      columnspan=colTotal, sticky=E + W + N + S)
        self.gamesFrame.addTitle("My Games")
        self._createHeaderFromCanvasForGame()
        self.gamesFrame.addRow()
        self.gamesFrame.stretchRow(self.gamesFrame.row)
        self._createCanvasForGame()

        ########################################################### Footer Section
        self.fieldsFrame.addRow()
        self.lastUpdate = AppLastUpdateFormat(self.fieldsFrame)
        self.lastUpdate.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, columnspan=colTotal,
                             sticky=E + N + S)

        self.setRequiredFields()
        self.fieldsDisable()

    def _createCanvasForGame(self):
        self.canvas = MyScrollFrameVertical(self.gamesFrame, self.gamesFrame.columnTotal, self.gamesListingColumnCount, self.gamesFrame.row)

    def _reloadGameCanvas(self):
        self.display9Holes = True
        self.displayToday = False
        #            self.idArea.destroy()
        #            self._createIdentificationFrame()
        #            self.golferStatsArea.destroy()
        #            self._createStatsArea()
        #            self.tableHeaderFrame.destroy()
        #            self.canvas.vsbDestroy()
        self.canvas.destroy()
        self._createCanvasForGame()

    def displayRec(self, *args):
        if len(self.recList) > 0:
            self.fieldsClear()
            self.selectedPicture.load(self.recList[self.curRecNumV.get()][3])
            self.selectedUserParticular.load(self.recList[self.curRecNumV.get()])
            self.populateGames()
            self.lastUpdate.load(self.recList[self.curRecNumV.get()][(self.numberOfFields - 2)],
                                 self.recList[self.curRecNumV.get()][(self.numberOfFields - 1)])

        else:
            self._displayRecordFrame()
        '''
        if len(self.recList) > 0:
            self.fieldsClear()
            self.selectedPicture.load(self.recList[self.curRecNumV.get()][1])
            self.selectedTitle_Position.load(self.recList[self.curRecNumV.get()][2])
            self.selectedPHONE_WORK.load(self.recList[self.curRecNumV.get()][4])
            self.selectedPHONE_Extension.load(self.recList[self.curRecNumV.get()][5])
            self.selectedPHONE_Home.load(self.recList[self.curRecNumV.get()][6])
            self.selectedNUMBER.load(self.recList[self.curRecNumV.get()][7])
            self.selectedSIN.load(self.recList[self.curRecNumV.get()][8])
            self.selectedRank.load(self.recList[self.curRecNumV.get()][9])
            self.selectedEmployeeType.load(self.recList[self.curRecNumV.get()][10])
            self.selectedNAME_FIRST.load(self.recList[self.curRecNumV.get()][11])
            self.selectedNAME_INIT.load(self.recList[self.curRecNumV.get()][12])
            self.selectedNAME_LAST.load(self.recList[self.curRecNumV.get()][13])
            self.selectedOFFICE.load(self.recList[self.curRecNumV.get()][14])
            self.selectedDate.load(self.recList[self.curRecNumV.get()][15], self.recList[self.curRecNumV.get()][16],
                                   self.recList[self.curRecNumV.get()][17])
            self.selectedJOB.load(self.recList[self.curRecNumV.get()][18])
            self.selectedID_NUM.load(self.recList[self.curRecNumV.get()][19])
            self.selectedWorkGroup.load(self.recList[self.curRecNumV.get()][46])
            self.selectedCOMP_TIME.load(self.recList[self.curRecNumV.get()][47])
            self.selectedSICK_TIME.load(self.recList[self.curRecNumV.get()][48])
            self.selectedVAC_TIME.load(self.recList[self.curRecNumV.get()][49])

            if self.recList[self.curRecNumV.get()][23] != '' and self.recList[self.curRecNumV.get()][23] != None:
                self.selectedDEPARTMENT.load(self.departmentsCBData.getName(self.recList[self.curRecNumV.get()][23]))
            else:
                self.selectedDEPARTMENT.clear()

            self.selectedREGISTER.load(self.recList[self.curRecNumV.get()][45])
            self.lastUpdate.load(self.recList[self.curRecNumV.get()][(self.numberOfFields - 2)],
                                 self.recList[self.curRecNumV.get()][(self.numberOfFields - 1)])

            self.selectedAccess.load(self.recList[self.curRecNumV.get()][3])
            self.selectedPassword.load(self.recList[self.curRecNumV.get()][20])
            self.selectedLoginName.load(self.recList[self.curRecNumV.get()][21])
            self.selectedAccessLegacy.load(self.recList[self.curRecNumV.get()][22])
            self.selectedSEC_MANAGE.load(self.recList[self.curRecNumV.get()][24])
            self.selectedSEC_LEVEL.load(self.recList[self.curRecNumV.get()][25])
            self.selectedSEC_SECT.load(self.recList[self.curRecNumV.get()][26])
            self.selectedSEC_DIVIS.load(self.recList[self.curRecNumV.get()][27])
            self.selectedSEC_DEPTN.load(self.recList[self.curRecNumV.get()][28])
            self.selectedCOURSE0.load(self.recList[self.curRecNumV.get()][29])
            self.selectedCOURSE1.load(self.recList[self.curRecNumV.get()][30])
            self.selectedCOURSE2.load(self.recList[self.curRecNumV.get()][31])
            self.selectedCOURSE3.load(self.recList[self.curRecNumV.get()][32])
            self.selectedCOURSE4.load(self.recList[self.curRecNumV.get()][33])
            self.selectedCOURSE5.load(self.recList[self.curRecNumV.get()][34])
            self.selectedCOURSE6.load(self.recList[self.curRecNumV.get()][35])
            self.selectedCOURSE7.load(self.recList[self.curRecNumV.get()][36])
            self.selectedCOURSE8.load(self.recList[self.curRecNumV.get()][37])
            self.selectedCOURSE9.load(self.recList[self.curRecNumV.get()][38])
            self.selectedCOURSE10.load(self.recList[self.curRecNumV.get()][39])
            self.selectedCOURSE11.load(self.recList[self.curRecNumV.get()][40])
            self.selectedCOURSE12.load(self.recList[self.curRecNumV.get()][41])
            self.selectedCOURSE13.load(self.recList[self.curRecNumV.get()][42])
            self.selectedCOURSE14.load(self.recList[self.curRecNumV.get()][43])
            self.selectedCOURSE15.load(self.recList[self.curRecNumV.get()][44])
            self.fieldsDisable()
            self.deletedV.set(self.recList[self.curRecNumV.get()][self.numberOfFields - 3])
        else:
            self._displayRecordFrame()
        '''

    def _dateEnteredAction(self, index, value, op):
        pass

    def _createHeaderFromCanvasForGame(self):
        self.parameterGameDataFrame = AppBorderFrame(self.gamesFrame, 5)
        self.parameterGameDataFrame.grid(column=self.gamesFrame.column, row=self.gamesFrame.row,
                                         columnspan=self.gamesFrame.columnTotal+1, sticky=N + S + W + E)
        #   We add a 1 for column span to offset the scroll bar of the canvas.

        self.selectedStartDate = AppFieldEntryDate(self.parameterGameDataFrame, 'Start Date', 'H', appDateWidth,
                                                   self._dateEnteredAction, self.myMsgBar)
        self.selectedStartDate.grid(row=self.parameterGameDataFrame.row, column=self.parameterGameDataFrame.column,
                                    sticky=N + S + E + W)

        self.parameterGameDataFrame.addColumn()
        self.selectedEndDate = AppFieldEntryDate(self.parameterGameDataFrame, 'End Date', 'H', appDateWidth,
                                                 self._dateEnteredAction, self.myMsgBar)
        self.selectedEndDate.grid(row=self.parameterGameDataFrame.row, column=self.parameterGameDataFrame.column,
                                  sticky=N + S + E + W)

        self.parameterGameDataFrame.addColumn()
        self.courseCBData = AppCBList(CRUDCourses.getCoursesDictionaryInfo())
        self.selectingCourse = AppSearchLB(self.parameterGameDataFrame, self, 'Select Golf Course', 'H',
                                           self.courseCBData.getList(), 40, None, self.myMsgBar, None)
        self.selectingCourse.grid(row=self.parameterGameDataFrame.row, column=self.parameterGameDataFrame.column,
                                  sticky=N + S)
        self.selectingCourse.enable()

        self.parameterGameDataFrame.addColumn()
        self.parameterGameDataFrame.noStretchColumn(self.parameterGameDataFrame.column)
        self.applyParameterButton = AppStdButton(self.parameterGameDataFrame, 'Apply', None, self._updateRoundListing)
        self.applyParameterButton.grid(row=self.parameterGameDataFrame.row, column=self.parameterGameDataFrame.column,
                                       sticky=N + E)
        self.applyParameterButton.addToolTip("Apply Game Parameter Listing")

        self.parameterGameDataFrame.addColumn()
        self.parameterGameDataFrame.noStretchColumn(self.parameterGameDataFrame.column)
        self.clearParameterButton = AppStdButton(self.parameterGameDataFrame, 'Clear', None,
                                                 self._clearGameListParameter)
        self.clearParameterButton.grid(row=self.parameterGameDataFrame.row, column=self.parameterGameDataFrame.column,
                                       sticky=N + W)
        self.clearParameterButton.addToolTip("Clear Game Parameter Listing")

        self._createListofGamesHeader(self.gamesFrame)

    def _createListofGamesHeader(self, aFrame):
        aFrame.addRow()
        self.tableHeaderFrame = AppBorderFrame(aFrame, self.gamesListingColumnCount)
        self.tableHeaderFrame.removeBorder()
        self.tableHeaderFrame.grid(row=aFrame.row, column=aFrame.column,
                                   columnspan=aFrame.columnTotal, sticky=N + E + W + S)
        smallWidth = 5
        scoresWidth = 7
        dateWidth = 18
        hdcpWidth = 7
        courseNameWidth = 45

        aRelief = 'flat'

        self.tableHeaderFrame.noStretchColumn(self.gamesListingColumnCount - 1)
        self.tableHeaderFrame.noStretchColumn(self.gamesListingColumnCount - 2)
        Tix.Label(self.tableHeaderFrame, text="#", relief=aRelief, justify=CENTER, font=fontAverageB,
                  disabledforeground=AppStdForeground,
                  fg=AppDefaultForeground, bd=2, width=smallWidth).grid(row=1, column=0, sticky=W + N + E + S)
        Tix.Label(self.tableHeaderFrame, text="Date", relief=aRelief, justify=CENTER, font=fontAverageB,
                  disabledforeground=AppStdForeground,
                  fg=AppDefaultForeground, bd=2, width=dateWidth).grid(row=1, column=1, sticky=W + N + E + S)
        Tix.Label(self.tableHeaderFrame, text="Front\nBack", relief=aRelief, justify=CENTER, font=fontAverageB,
                  disabledforeground=AppStdForeground,
                  fg=AppDefaultForeground, bd=2, width=scoresWidth).grid(row=1, column=2, sticky=W + N + E + S)
        Tix.Label(self.tableHeaderFrame, text="Score", relief=aRelief, justify=CENTER, font=fontAverageB,
                  disabledforeground=AppStdForeground,
                  fg=AppDefaultForeground, bd=2, width=scoresWidth).grid(row=1, column=3, sticky=W + N + E + S)
        Tix.Label(self.tableHeaderFrame, text="Strks", relief=aRelief, justify=CENTER, font=fontAverageB,
                  disabledforeground=AppStdForeground,
                  fg=AppDefaultForeground, bd=2, width=scoresWidth).grid(row=1, column=4, sticky=W + N + E + S)
        Tix.Label(self.tableHeaderFrame, text="Net", relief=aRelief, justify=CENTER, font=fontAverageB,
                  disabledforeground=AppStdForeground,
                  fg=AppDefaultForeground, bd=2, width=scoresWidth).grid(row=1, column=5, sticky=W + N + E + S)
        Tix.Label(self.tableHeaderFrame, text="Hdcp\nDiff", relief=aRelief, justify=CENTER, font=fontAverageB,
                  disabledforeground=AppStdForeground, bd=2,
                  fg=AppDefaultForeground, width=hdcpWidth).grid(row=1, column=6, sticky=W + N + E + S)
        Tix.Label(self.tableHeaderFrame, text="Before\nIndex", relief=aRelief, justify=CENTER,
                  font=fontAverageB, disabledforeground=AppStdForeground,
                  fg=AppDefaultForeground, bd=2, width=hdcpWidth).grid(row=1, column=7, sticky=W + N + E + S)
        Tix.Label(self.tableHeaderFrame, text="After\nIndex", relief=aRelief, justify=CENTER,
                  font=fontAverageB, disabledforeground=AppStdForeground, bd=2,
                  fg=AppDefaultForeground, width=hdcpWidth).grid(row=1, column=8, sticky=W + N + E + S)
        Tix.Label(self.tableHeaderFrame, text="Course/Tee", relief=aRelief, justify=CENTER, font=fontAverageB,
                  disabledforeground=AppStdForeground, bd=2,
                  fg=AppDefaultForeground, width=courseNameWidth).grid(row=1, column=9, sticky=W + N + E + S)
        Tix.Label(self.tableHeaderFrame, text="Rating", relief=aRelief, justify=CENTER, font=fontAverageB,
                  disabledforeground=AppStdForeground, bd=2,
                  fg=AppDefaultForeground, width=hdcpWidth).grid(row=1, column=10, sticky=W + N + E + S)
        Tix.Label(self.tableHeaderFrame, text="Slope", relief=aRelief, justify=CENTER, font=fontAverageB,
                  disabledforeground=AppStdForeground, bd=2,
                  fg=AppDefaultForeground, width=hdcpWidth).grid(row=1, column=11, sticky=W + N + E + S)
        Tix.Label(self.tableHeaderFrame, text=" ", relief=aRelief, justify=CENTER, font=fontAverageB,
                  disabledforeground=AppStdForeground, bd=2,
                  fg=AppDefaultForeground, width=gameListingButtonWidth-4).grid(row=1, column=12, sticky=W)

    def _clearGameListParameter(self, *args):
            self.displayStartDateForGame = None
            self.displayGameForCourse = None
            self.displayEndDateForGame = None

            self.selectedStartDate.clear()
            self.selectedEndDate.clear()
            self.selectingCourse.clear()

#            self._reloadGameCanvas()

    def _verifyFutureDate(self, aDate):
            aDateOrdinal = convertDateStringToOrdinal(aDate)
            todayOrdinal = convertDateStringToOrdinal(strdate)
            if aDateOrdinal > todayOrdinal:
                return False
            else:
                return True

    def _startAddEditGolfer(self, mode, golferData):
        pass

    def OnChildClose(self):
        self.AddEditGolferWindow.destroy()

    def _verifyDateRange(self):
            startOrdinal = convertDateStringToOrdinal(self.selectedStartDate.get())
            endOrdinal = convertDateStringToOrdinal(self.selectedEndDate.get())
            if endOrdinal >= startOrdinal:
                return True
            else:
                return False

    def _updateRoundListing(self, *args):
            aMsg = ''' '''
            self.myMsgBar.clearMessage()
            verified = False
            if len(self.selectedStartDate.get()) > 0 and len(self.selectedEndDate.get()) > 0 and len(
                    self.selectingCourse.get()) > 0:
                # All three field entered
                if self._verifyFutureDate(self.selectedStartDate.get()) == True and self._verifyFutureDate(
                        self.selectedEndDate.get()):
                    if self._verifyDateRange() == True:
                        verified = True
                        self.displayGameForCourse = self.selectingCourse.get()
                        self.displayStartDateForGame = self.selectedStartDate.get()
                        self.displayEndDateForGame = self.selectedEndDate.get()
                    else:
                        self.selectedEndDate.focus()
                        aMsg = "ERROR: End date can not be BEFORE the start date."
                else:
                    if self._verifyFutureDate(self.selectedStartDate.get()) == True:
                        self.selectedEndDate.focus()
                        aMsg = "ERROR: End date can not be in the future."
                    else:
                        self.selectedStartDate.focus()
                        aMsg = "ERROR: Start date can not be in the future."
            elif len(self.selectedStartDate.get()) > 0 and len(self.selectedEndDate.get()) > 0 and len(
                    self.selectingCourse.get()) == 0:
                # Dates entered only
                if self._verifyFutureDate(self.selectedStartDate.get()) == True and self._verifyFutureDate(
                        self.selectedEndDate.get()):
                    if self._verifyDateRange() == True:
                        verified = True
                        self.displayGameForCourse = None
                        self.displayStartDateForGame = self.selectedStartDate.get()
                        self.displayEndDateForGame = self.selectedEndDate.get()
                    else:
                        self.selectedEndDate.focus()
                        aMsg = "ERROR: End date can not be BEFORE the start date."
                else:
                    if self._verifyFutureDate(self.selectedStartDate.get()) == True:
                        self.selectedEndDate.focus()
                        aMsg = "ERROR: End date can not be in the future."
                    else:
                        self.selectedStartDate.focus()
                        aMsg = "ERROR: Start date can not be in the future."

            elif len(self.selectedStartDate.get()) == 0 and len(self.selectedEndDate.get()) == 0 and len(
                    self.selectingCourse.get()) > 0:
                # Course Entered only
                verified = True
                self.displayGameForCourse = self.selectingCourse.get()
                self.displayStartDateForGame = None
                self.displayEndDateForGame = None

            elif len(self.selectedStartDate.get()) > 0 and len(self.selectedEndDate.get()) == 0:
                self.selectedEndDate.focus()
                aMsg = "ERROR: End date is required if a start date is entered."

            elif len(self.selectedStartDate.get()) == 0 and len(self.selectedEndDate.get()) > 0:
                self.selectedStartDate.focus()
                aMsg = "ERROR: Start date is required if an end date is entered."

            else:
                verified = True
                self.displayGameForCourse = None
                self.displayStartDateForGame = None
                self.displayEndDateForGame = None

            if verified == True:
                self.myMsgBar.clearMessage()
                self._reloadGameCanvas()
            else:
                self.myMsgBar.newMessage('error', aMsg)

    def _courseEnteredAction(self, *args):
            self.myMsgBar.clearMessage()

    def populateGames(self):
#        for i_row in range(2):
            #Component I want to take extra space when resizing window
#            for i_column in range(12):
#                self.canvas2.addACellFrame(gameColumnDataFrame, "My New Label", 20, i_row + 1, i_column)
        self._reloadGameCanvas()
        self.btnDel = []
        self.btnEdit = []
        smallWidth = 5
        scoresWidth = 7
        dateWidth = 18
        hdcpWidth = 7
        courseNameWidth = 45

        if len(self.recList) > 0:
            if self.display9Holes == True:
                if self.displayGameForCourse != None and self.displayEndDateForGame != None and self.displayGameForCourse != None:
                    curGolferGameList = CRUDGames.get_games_for_golfer_on_course_dateRange(
                        self.recList[self.curRecNumV.get()][0], self.courseDictionary[self.displayGameForCourse],
                        convertDateStringToOrdinal(self.displayStartDateForGame),
                        convertDateStringToOrdinal(self.displayEndDateForGame))

                elif self.displayGameForCourse != None and self.displayEndDateForGame == None and self.displayStartDateForGame == None:
                    curGolferGameList = CRUDGames.get_games_for_golfer_on_course(self.recList[self.curRecNumV.get()][0],
                                                                                 self.courseDictionary[
                                                                                     self.displayGameForCourse])

                elif self.displayGameForCourse == None and self.displayEndDateForGame != None and self.displayStartDateForGame != None:
                    curGolferGameList = CRUDGames.get_games_for_golfer_dateRange(self.recList[self.curRecNumV.get()][0],
                                                                                 convertDateStringToOrdinal(
                                                                                     self.displayStartDateForGame),
                                                                                 convertDateStringToOrdinal(
                                                                                     self.displayEndDateForGame))

                else:
                    curGolferGameList = CRUDGames.get_games_for_golfer(self.recList[self.curRecNumV.get()][0])

            else:
                if self.displayGameForCourse != None and self.displayEndDateForGame != None and self.displayGameForCourse != None:
                    curGolferGameList = CRUDGames.get_18hole_games_for_golfer_on_course_dateRange(
                        self.recList[self.curRecNumV.get()][0], self.courseDictionary[self.displayGameForCourse],
                        convertDateStringToOrdinal(self.displayStartDateForGame),
                        convertDateStringToOrdinal(self.displayEndDateForGame))

                elif self.displayGameForCourse != None and self.displayEndDateForGame == None and self.displayStartDateForGame == None:
                    curGolferGameList = CRUDGames.get_18hole_games_for_golfer_on_course(
                        self.recList[self.curRecNumV.get()][0], self.courseDictionary[self.displayGameForCourse])

                elif self.displayGameForCourse == None and self.displayEndDateForGame != None and self.displayStartDateForGame != None:
                    curGolferGameList = CRUDGames.get_18hole_games_for_golfer_dateRange(
                        self.recList[self.curRecNumV.get()][0],
                        convertDateStringToOrdinal(self.displayStartDateForGame),
                        convertDateStringToOrdinal(self.displayEndDateForGame))

                else:
                    curGolferGameListpopulateGames = CRUDGames.get_18hole_games_for_golfer(
                        self.recList[self.curRecNumV.get()][0])

        if len(curGolferGameList) > 0:
            '''
            if self.display9Holes == True:
                Button(self.tableHeaderFrame, text="Hide 9s", relief='ridge', justify=CENTER,
                      bg=AppHighlightBackground, fg=AppDefaultForeground, font=AppDefaultFontAvg,
                      border=2, command=self._hide9HoleGames).grid(row=1, column=12, columnspan=2, padx=4)
            else:
                Button(self.tableHeaderFrame, text="Show 9s", relief='ridge', justify=CENTER,
                      bg=AppHighlightBackground, fg=AppDefaultForeground, font=AppDefaultFontAvg,
                      command=self._display9HoleGames).grid(row=1, column=12, columnspan=2, padx=4)

            '''

            AppCRUD.initDB(getDBLocationFullName())
            for row in range(len(curGolferGameList)):
#                print("Time: ", datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3])
                if row != 0:
                    self.canvas.addRow()

                if curGolferGameList[row][
                    26] == 'All':  # calculate indicate only for those game, 9 holes is for stats purposes

                    curGolferHdcpBeforeCurrentGame = CRUDGames.calculateBeforeCurrentGameHdcpMASS(curGolferGameList[row][1],
                                                                                              self.recList[
                                                                                                  self.curRecNumV.get()][
                                                                                                  0],
                                                                                              curGolferGameList[row][0],
                                                                                              curGolferGameList[row][2])
                    curGolferHdcpAfterGame = CRUDGames.calculateAfterCurrentGameHdcpMASS(curGolferGameList[row][1],
                                                                                     self.recList[
                                                                                         self.curRecNumV.get()][0],
                                                                                     curGolferGameList[row][0],
                                                                                     curGolferGameList[row][2])
                    curGolferStrokes = CRUDGames.calculategolferStrokes(curGolferHdcpBeforeCurrentGame,
                                                                        curGolferGameList[row][25])
                else:
                    curGolferHdcpBeforeCurrentGame = ''
                    curGolferHdcpAfterGame = ''
                    curGolferStrokes = ''

                # teeId is index 3
                curGameTeeData = CRUDTees.get_teeName_CourseIDMASS(curGolferGameList[row][3])
                curGameCourseData = CRUDCourses.get_course_snameMASS(curGameTeeData[0][2])
                # build a course and tee name
                #
                # Game Number
                #
                self.canvas.addAGameCellFrame(gameCellColumnDataFrame, str(row + 1), smallWidth, curGolferGameList[row][26],None,None,None,None)
                #
                # Game Date
                #
                self.canvas.addAGameCellFrame(gameCellColumnDataFrame, "{0} ({1})".format(convertOrdinaltoString(int(curGolferGameList[row][1])),
                                               curGolferGameList[row][2]), dateWidth, curGolferGameList[row][26],None,None,None,None)
                #
                # Game Front and Back Score
                #
                if curGolferGameList[row][26] == 'All':
                    aText = "{0}/{1}".format(curGolferGameList[row][5], curGolferGameList[row][6])
                elif curGolferGameList[row][26] == 'Front':
                    aText = "{0} (F)".format(curGolferGameList[row][5])
                else:
                    aText = "{0} (B)".format(curGolferGameList[row][6])
                self.canvas.addAGameCellFrame(gameCellColumnDataFrame, aText, scoresWidth, curGolferGameList[row][26],None,None,None,None)
                #
                # Game Score
                #
                self.canvas.addAGameCellFrame(gameCellColumnDataFrame, curGolferGameList[row][7], scoresWidth, curGolferGameList[row][26],None,None,None,None)
                    #
                # Game Strokes
                #
                if curGolferGameList[row][26] == 'All':
                    aText = curGolferStrokes
                else:
                    aText = " "
                self.canvas.addAGameCellFrame(gameCellColumnDataFrame, aText, scoresWidth, curGolferGameList[row][26],None,None,None,None)
                #
                # Game Net Score
                #
                if curGolferGameList[row][26] == 'All':
                    aText = int(curGolferGameList[row][7] - curGolferStrokes)
                else:
                    aText = " "
                self.canvas.addAGameCellFrame(gameCellColumnDataFrame, aText, scoresWidth, curGolferGameList[row][26],None,None,None,None)
                #
                # Game Hdcp Differential
                #
                self.canvas.addAGameCellFrame(gameCellColumnDataFrame, curGolferGameList[row][8], scoresWidth, curGolferGameList[row][26],None,None,None,None)
                #
                # Current Golfer Index
                #
                if curGolferGameList[row][26] == 'All':
                    aText = curGolferHdcpBeforeCurrentGame
                else:
                    aText = " "
                self.canvas.addAGameCellFrame(gameCellColumnDataFrame, aText, scoresWidth, curGolferGameList[row][26],None,None,None,None)
                #
                # Next Golfer Index
                #
                if curGolferGameList[row][26] == 'All':
                    aText = curGolferHdcpAfterGame
                else:
                    aText = " "
                self.canvas.addAGameCellFrame(gameCellColumnDataFrame, aText, scoresWidth, curGolferGameList[row][26],None,None,None,None)
                #
                # Game Course Name and Tee
                #
                self.canvas.addAGameCellFrame(gameCellColumnDataFrame, "{0}: {1} {2}".format(curGameCourseData[0][0], curGameTeeData[0][1],
                                               curGameTeeData[0][0]), courseNameWidth, curGolferGameList[row][26],None,None,None,None)
                #
                # Game Rating
                #
                if curGolferGameList[row][26] == 'All':
                    rating = float(curGolferGameList[row][24])
                    aText = "%.1f" % rating
                else:
                    rating = float(curGolferGameList[row][24]) / float(2)
                    aText = "%.1f" % rating
                self.canvas.addAGameCellFrame(gameCellColumnDataFrame, aText, scoresWidth, curGolferGameList[row][26],None,None,None,None)
                #
                # Game Slope
                #
                self.canvas.addAGameCellFrame(gameCellColumnDataFrame, curGolferGameList[row][25], scoresWidth, curGolferGameList[row][26],None,None,None,None)
                #
                # Edit Button
                #
                self.canvas.addAGameCellFrame(gameCellColumnDataFrame, "Edt", gameListingButtonWidth, "Button", self._btnGameEdtDetected, curGolferGameList, row, 0, self.theListEditImage)
#                self.canvas.noStrechColumn(int(self.columnCountTotal)-1)
                self.canvas.addAGameCellFrame(gameCellColumnDataFrame, "Del", gameListingButtonWidth, "Button", self._btnGameDelDetected, curGolferGameList, row, 0, self.theListDeleteImage)
#                self.canvas.noStrechColumn(int(self.columnCountTotal)-2)
            AppCRUD.closeDB()
        self.canvas.ResizeScrollBar()


#            if self.display9Holes == True:
#                Tix.Label(self.frame, text='*', relief='groove', justify=CENTER, font=AppDefaultFontAvg,
#                          disabledforeground=AppHighlightBackground, bg=AppHighlightBackground,
#                          fg=AppDefaultForeground, bd=2, width=smallWidth).grid(row=row + 2, column=0,
#                                                                                sticky=W + N + E + S)
#                aMsg = "Indicates 9-hole game (front or back). They are not used for handicap calculations."
#                Tix.Label(self.frame, text=aMsg, relief='groove', justify=LEFT, font=fontBig,
#                          disabledforeground=AppHighlightBackground,
#                          bg=AppHighlightBackground, fg=AppDefaultForeground, bd=2, width=scoresWidth).grid(
#                    row=row + 2, column=1, columnspan=11, sticky=W + N + E + S)
#                Button(self.frame, text="Hide 9s", relief='ridge', justify=CENTER, bg=AppHighlightBackground,
#                       fg=AppDefaultForeground, font=AppDefaultFontAvg,
#                       command=self._hide9HoleGames).grid(row=row + 2, column=12, columnspan=2, padx=4)
#            else:
#                Button(self.frame, text="Show 9s", relief='ridge', justify=CENTER, bg=AppHighlightBackground,
#                       fg=AppDefaultForeground, font=AppDefaultFontAvg,
#                       command=self._display9HoleGames).grid(row=row + 2, column=12, columnspan=2, padx=4)

    def _btnGameDelDetected(self, gameID):
        print("Game ID for deletion received is: ", gameID)
        pass

    #       self._startGameWindow('edit', gameID)
    def _btnGameEdtDetected(self, gameID):
        print("Game ID for editing received is: ", gameID)
        pass

    #       self._startGameWindow('edit', gameID)

    def _hide9HoleGames(self):
            self.display9Holes = False
            self.frame.destroy()
            self._createCanvasForGame()
            self.populateGames()

    def _todayStatsOnly(self):
        self.displayToday = False
        self.golferStatsArea.destroy()
        self._createStatsArea()

    def _display9HoleGames(self):
            self.display9Holes = True
            self.frame.destroy()
            self._createCanvasForGame()
            self.populateGames()

    def _allTimeStatsOnly(self):
            self.displayToday = True
            self.golferStatsArea.destroy()
            self._createStatsArea()

    def fieldsClear(self):
        self.selectedPicture.clear()

    def fieldsDisable(self):
        self.selectedPicture.disable()

    def fieldsEnable(self):
        pass
        self.selectedPicture.enable()

    ################################################################# Redefined commands
    #
    #  invoke from the tableCommonWindows.  These commands are specific to each tables
    #  The specific actions are redefinded here.
    #
    #    def displayRec(self, *args):
    #        print("Displaying a Record: ", self.curRecNumV.get())
    #        self._displayRecordFrame()
    def setRequiredFields(self):
        pass
#        self.selectedEmployeeType.setAsRequiredField()


    def resetRequiredFields(self):
        pass
#        self.selectedEmployeeType.resetAsRequiredField()


    def find(self):
        self.fieldsClear()
        self.resetRequiredFields()
        '''
        if len(self.findValues) > 0:
            self.selectedNAME_FIRST.load(self.findValues[0])
            self.selectedNAME_LAST.load(self.findValues[1])
            self.selectedDEPARTMENT.load(self.findValues[2])
            self.selectedWorkGroup.load(self.findValues[3])
            self.selectedEmployeeType.load(self.findValues[4])
            self.selectedRank.load(self.findValues[5])

        self.selectedNAME_LAST.enable()
        self.selectedNAME_FIRST.enable()
        self.selectedDEPARTMENT.enable()
        self.selectedWorkGroup.find()
        self.selectedEmployeeType.find()
        self.selectedRank.find()
        self.selectedNAME_LAST.focus()
        '''

    '''
    def delete(self):
        if self.recList[self.curRecNumV.get()][0] == 1:
            aMsg = "{0} {1} can not be deleted.".format(self.recList[self.curRecNumV.get()][11], self.recList[self.curRecNumV.get()][13])
            self.myMsgBar.newMessage('error', aMsg)
        else:
            self._disableWindowCommand()
            aMsg = "Are you sure you want to delete the following person:\n{0} {1} {2}?".format(self.recList[self.curRecNumV.get()][9],self.recList[self.curRecNumV.get()][11],
                                                                                self.recList[self.curRecNumV.get()][13])
            ans = AppQuestionRequest(self, "Delete Personnel", aMsg)

            if ans.result == True:
                AppDBPerson.deletePerson(self.recList[self.curRecNumV.get()][0]) # remove from DB
                aMsg = "Record {0} {1} {2} has been removed from the Active Personnel List.".format(self.recList[self.curRecNumV.get()][9],self.recList[self.curRecNumV.get()][11],
                                                                     self.recList[self.curRecNumV.get()][13])
                self.myMsgBar.newMessage('info', aMsg)

                #
                #    Following avoids reloading the whole table after removal field is updated above.
                #    Removes it from current view and the over all list is also updated to reflect
                #  the new deleted status.
                #
                idxRecListAll = getIndex(self.recList[self.curRecNumV.get()][0],self.recAll)
                itemToUpdateDeletedField = self.rBarefoot Resort & Golf, FazioecAll[idxRecListAll]
                itemWithDeletedFieldUpdated = ()
                for i in range(len(itemToUpdateDeletedField)):
                    if i == self.numberOfFields - 3:
                        itemWithDeletedFieldUpdated += ('Y',)
                    else:
                        itemWithDeletedFieldUpdated += (self.recAll[idxRecListAll][i],)
                self.recAll.pop(idxRecListAll)                                # remove the old one
                self.recAll.insert(idxRecListAll,itemWithDeletedFieldUpdated) # add the new one
                if self.viewSet.get() != 'Closed':
                    self.recList.pop(self.curRecNumV.get())                   # remove from current list (only if active view)
                if self.curRecBeforeOp != 0:
                    self.curRecNumV.set(self.curRecBeforeOp - 1)
                else:
                    self.curRecNumV.set(self.curRecBeforeOp)
                self.displayRec()
        self._enableWindowCommand()
        self.myDatabaseBar.defaultState()
        self.navigationEnable(self.accessLevel)
        self.resetCurOp()
    '''

    def windowViewDefault(self):
        self.basicViewsList('All')
        self.sortingList('Default')

    def edit(self):
        self.fieldsEnable()
#        self.selectedEmployeeType.focus()

    #        self.selectedDEPARTMENT.disable()

    def add(self):
        if len(self.recList) == 0:
            self._displayRecordFrame()
        self.fieldsClear()
#        self.selectedDEPARTMENT.load(self.departmentsCBData.getName(DefaultDepartmentID))
#        self.selectedPHONE_WORK.load(43543535)
#        self.selectedAccess.load('View')
#        self.selectedEmployeeType.enable()
#        self.myMsgBar.requiredFieldMessage()
#        self.selectedEmployeeType.focus()

    #        self.selectedDEPARTMENT.disable()
    #        self.fieldsEnable()

    def clearPopups(self):
        # self.selectedDEPARTMENT.clearPopups()  # This is an example of the popups entry (usually a pull down menu)

        pass

    def validateRequiredFields(self):
        #
        #  required Fields:
        #
        #
        requiredFieldsEntered = True
#  Example of a required field.
#        if len(self.selectedSIN.get()) == 0:
#            requiredFieldsEntered = False
#            self.selectedSIN.focus()

        # if len(self.selectedDEPARTMENT.get()) == 0:
        #            requiredFieldsEntered = False
        #            self.selectedDEPARTMENT.focus()

        return requiredFieldsEntered

    def save(self):
        #
        #  Table specific.  Return a True if succesful saved.
        #
        #
        #  House Cleaning in case a popup still exist
        #
        requiredFieldsEntered = self.validateRequiredFields()
        if len(self.selectedDEPARTMENT.get()) > 0:
            departmentsID = self.departmentsCBData.getID(self.selectedDEPARTMENT.get())
        else:
            departmentsID = ''
        if self.curOp == 'add' and requiredFieldsEntered == True:
            pass
            if True:
                pass
        elif self.curOp == 'edit' and requiredFieldsEntered == True:
            pass
            aMsg = "Record {0} {1} has been updated.".format(self.selectedNAME_FIRST.get(),
                                                             self.selectedNAME_LAST.get())
            self.myMsgBar.newMessage('info', aMsg)
            #
            # Add the item again but with modifieBarefoot Resort & Golf, Faziod data.
            #
            ID = self.recList[self.curRecNumV.get()][0]
            anItem = (
                ID,
                self.selectedPicture.get(),
                self.selectedTitle_Position.get(),
                self.selectedAccess.get(),
                self.selectedPHONE_WORK.get(),
                self.selectedPHONE_Extension.get(),
                self.selectedPHONE_Home.get(),
                self.selectedNUMBER.get(),
                self.selectedSIN.get(),
                self.selectedRank.get(),
                self.selectedEmployeeType.get(),
                self.selectedNAME_FIRST.get(),
                self.selectedNAME_INIT.get(),
                self.selectedNAME_LAST.get(),
                self.selectedOFFICE.get(),
                self.recList[self.curRecNumV.get()][15],  # This never changes, keep storing the same value
                self.recList[self.curRecNumV.get()][16],  # Was created when personnal added to the database.
                self.recList[self.curRecNumV.get()][17],
                self.selectedJOB.get(),
                self.selectedID_NUM.get(),
                self.selectedPassword.get(),
                self.selectedLoginName.get(),
                self.selectedAccessLegacy.get(),
                departmentsID,
                self.selectedSEC_MANAGE.get(),
                self.selectedSEC_LEVEL.get(),
                self.selectedSEC_SECT.get(),
                self.selectedSEC_DIVIS.get(),
                self.selectedSEC_DEPTN.get(),
                self.selectedCOURSE0.get(),
                self.selectedCOURSE1.get(),
                self.selectedCOURSE2.get(),
                self.selectedCOURSE3.get(),
                self.selectedCOURSE4.get(),
                self.selectedCOURSE5.get(),
                self.selectedCOURSE6.get(),
                self.selectedCOURSE7.get(),
                self.selectedCOURSE8.get(),
                self.selectedCOURSE9.get(),
                self.selectedCOURSE10.get(),
                self.selectedCOURSE11.get(),
                self.selectedCOURSE12.get(),
                self.selectedCOURSE13.get(),
                self.selectedCOURSE14.get(),
                self.selectedCOURSE15.get(),
                self.selectedREGISTER.get(),
                self.selectedWorkGroup.get(),
                self.selectedSICK_TIME.get(),
                self.selectedCOMP_TIME.get(),
                self.selectedVAC_TIME.get(),
                self.recList[self.curRecNumV.get()][self.numberOfFields - 3],
                convertDateStringToOrdinal(todayDate), self.updaterID
            )

            idxRecListAll = getIndex(self.recList[self.curRecNumV.get()][0], self.recAll)
            self.recAll.pop(idxRecListAll)
            self.recList.pop(self.curRecNumV.get())  # Remove the item that was modified
            self.recAll.insert(idxRecListAll, anItem)
            self.recList.insert(self.curRecNumV.get(), anItem)
            self.curRecNumV.set(len(self.recList) - 1)
            self.resetCurOp()  # Sort has to execute in default mode
            self.sort()
            newIdx = getIndex(ID, self.recList)
            self.curRecNumV.set(newIdx)
            self.fieldsDisable()
            self.displayRec()
            return True

        elif self.curOp == 'find':
            # Important if validation for amount is on.  May create non wanted error
            # Cause by displaying a $ in front of amount and it is a non valid character
            # in an amount field.
            self.fieldsDisable()
            self.findValues = []
            self.findValues.append(self.selectedNAME_FIRST.get())
            self.findValues.append(self.selectedNAME_LAST.get())
            self.findValues.append(self.selectedDEPARTMENT.get())
            self.findValues.append(self.selectedWorkGroup.get())
            self.findValues.append(self.selectedEmployeeType.get())
            self.findValues.append(self.selectedRank.get())

            if len(self.recListTemp) == 0:
                aMsg = "WARNING: No records were found using the given search criterias."
                if self.prevFind == True:
                    self.displayCurFunction.setFindResultMode()
                    self.curFind = True
                else:
                    self.displayCurFunction.setDefaultMode()
                    self.curFind = False
                self.myMsgBar.newMessage('warning', aMsg)
            elif len(self.recList) == 1:
                self.curFind = True
                self.recList = self.recListTemp
                self.displayCurFunction.setFindResultMode()
                aMsg = "One record was found using the given search criterias."
                self.curRecNumV.set(0)
                self.myMsgBar.newMessage('info', aMsg)
            else:
                self.curFind = True
                self.recList = self.recListTemp
                self.displayCurFunction.setFindResultMode()
                aMsg = "{0} records were found using the given search criterias.".format(len(self.recList))
                self.curRecNumV.set(0)
                self.myMsgBar.newMessage('info', aMsg)
            self.displayRec()
            self.clearPopups()
            self.setRequiredFields()
            return True
        else:
            self.clearPopups()
            return False


