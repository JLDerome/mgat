def frac(n): return 360. * n / 500

import tkinter

c = tkinter.Canvas(width=100, height=100); c.pack()

c.create_arc((2,2,98,98), fill='red', start=frac(0), extent = frac(100))
c.create_arc((2,2,98,98), fill='blue', start=frac(100), extent = frac(400))
c.create_arc((2,2,98,98), fill='green', start=frac(400), extent = frac(100))
c.mainloop()