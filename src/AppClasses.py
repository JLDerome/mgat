import tkinter.tix as Tix
import tkinter.filedialog as Fd
import random, shutil
from tkinter import *
import tkinter.ttk as ttk
import textwrap
from tkinter.constants import *
from AppConstants import *
from AppProc import resizeImage, widgetEnter,convertOrdinaltoString, focusNext, convertYardsToKms
from AppProc import getAge, convertDateStringToOrdinal, updateMyAppConstant
from AppProc import stripChar
from PIL import Image, ImageTk
from PIL.Image import NORMAL
import AppCRUDGolfers as CRUDGolfers
import AppCRUDAddresses as CRUDAddresses
import AppCRUDExpenses as CRUDExpenses
import AppCRUDLoginAccess as CRUDLoginAccess
from AppCRUDGames import calculateCurrentHdcp, calculategolferStrokes
import calendar, re
import platform
from dateutil import parser
# from pylab import *

class gameCellColumnDataFrame(Frame):
    def __init__(self, root, aLabel, aWidth, is9Hole, aMethod, aList, aRow, anIndice, imageResized=None):
        Frame.__init__(self, root)
        self.aRow = aRow
        self.configure(highlightcolor = AppDefaultForeground, relief='groove', border = 2,
                       highlightthickness = AppHighlightBorderWidth, takefocus = 0,
                       height =1 )
        if is9Hole == "All":
            Label(self, text=aLabel, width = aWidth, relief='flat', justify=CENTER, takefocus = 0,
                        font=AppDefaultFontAvg, disabledforeground=AppStdForeground, bd=0,
                        fg=AppDefaultForeground).grid(row=0, column=0, sticky="wnes")
        elif is9Hole == "Back" or is9Hole == "Front":
            Label(self, text=aLabel, width = aWidth, relief='flat', justify=CENTER, bd=0,
                        font=AppDefaultFontAvg, disabledforeground=AppHighlightBackground,  takefocus = 0,
                        bg=AppHighlightBackground, fg=AppDefaultForeground).grid(row=0, column=0, sticky="wnes")
        else:
#            imageResized = resizeImage(Image.open(deleteCommandRecImage), buttonImageSizeHeight, buttonImageSizeWidth)
#            self.aButton = Button(self, image=PhotoImage(deleteCommandRecImage), font=AppDefaultFontAvg,relief='flat', border=0, width = aWidth,
#                                      command=lambda ID=aList[aRow][anIndice]: aMethod(ID), takefocus=1).grid(row=0, column=0, sticky="wnes")
            self.bind('<Enter>', self.aFocus)
            self.bind('<Leave>', self.leaving)
            self.bind('<F1>', self.displayToolTip)
            self.bind('<Escape>', self.removeToolTip)
            if aLabel == 'Edt':
 #               imageResized = resizeImage(Image.open(editRecImage), buttonImageSizeHeight,
 #                                          buttonImageSizeWidth)
                self.aButton_ttp = CreateToolTipV2(self, "Edit Game {0}".format(int(self.aRow)+1))
            elif aLabel == 'Del':
 #               imageResized = resizeImage(Image.open(deleteCommandRecImage), buttonImageSizeHeight,
 #                                          buttonImageSizeWidth)
                self.aButton_ttp = CreateToolTipV2(self, "Delete Game {0}".format(int(self.aRow)+1))

            self.aButton = ttk.Button(self, width=aWidth, image=imageResized,
                                      command=lambda ID=aList[aRow][anIndice]: aMethod(ID),
                                      style='GameListingButton.TButton', takefocus=1)
            self.aButton.img = imageResized
            self.aButton.grid(row=0, column=0, sticky="wnes")

        self.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=1)

    def aFocus(self,event):
        self.configure(highlightcolor=AppDefaultForeground,
                       highlightthickness=AppHighlightBorderWidth, takefocus=1)
        widgetEnter(event)

    def leaving(self, event):
        self.configure(takefocus=0)
        self.configure(highlightcolor=AppDefaultBackground,
                       highlightthickness=AppHighlightBorderWidth, takefocus=0)
        if self.aButton_ttp.tw:
            self.aButton_ttp.tw.destroy()

    def displayToolTip(self, event):
        self.aButton_ttp.enter()

    def removeToolTip(self, event):
        self.aButton_ttp.close()

class MyScrollFrameVertical(Frame):
    def __init__(self, root, rootColumnSpan, columnTotal, rootRow):
        Frame.__init__(self, root)
        self.lastWidth = 0
        self.initialFrameRow = rootRow
        self.initialFrameColumnTotal = columnTotal
        self.configure(takefocus=0)
        self.Column = 0
        self.currentRow = 0
        self.currentColumn = 0
        self.columnTotal = columnTotal
        self.noItems = TRUE # no rows added yet (no cell)
        self.canvas = Canvas(root)
        self.vsb = Scrollbar(root, orient="vertical", command=self.canvas.yview)
        self.canvas.configure(yscrollcommand=self.vsb.set, takefocus=0)
        self.canvas.bind("<Configure>", self.OnCanvasConfigure)
        self.vsb.grid(row=self.initialFrameRow, column=rootColumnSpan, sticky="wnes")
        self.canvas.grid(row=self.initialFrameRow, column=0, columnspan=rootColumnSpan, sticky="wnes")

        self.frame = Frame(self.canvas, takefocus=0)
        self.window = self.canvas.create_window((0,0), window=self.frame, anchor="nw",
                          tags="self.frame")
        self.frame.rowconfigure(0, weight=1)  # first row configure

        for i in range(columnTotal-2):
            self.columnconfigure(i, weight=1)
            self.frame.columnconfigure(i, weight=1)
        self.rowconfigure(0, weight=1)

    def destroyAll(self):
        self.canvas.destroy()
        self.vsb.destroy()
        self.frame.destroy()
        self.destroy()

    def addAGameCellFrame(self, aFrame, aText, aWidth, is9Hole, aMethod, aList, aRow, anIndice, anImageForTheListButton=None):
        aFrame(self.frame, aText, aWidth, is9Hole, aMethod, aList, aRow, anIndice, anImageForTheListButton).grid(row=self.currentRow, column=self.currentColumn, sticky="wens")
        self.currentColumn = self.currentColumn + 1

#    def addAGameActionButton(self, aFrame, aText, anImage, gameIDList, indices, aMethod):
#        aFrame(self.frame, aText, anImage, aMethod, gameIDList, indices).grid(row=self.currentRow, column=self.currentColumn, sticky="wens")
#        self.currentColumn = self.currentColumn + 1

    def addAFrame(self, columnCount):
        aFrame = AppBorderFrame(self.frame, columnCount)
        aFrame.grid(row=self.currentRow, column=self.currentColumn, columnspan=self.columnTotal-2, sticky="wens")
        aFrame.removeBorder()
        aFrame.setSelectableFrame()
        return aFrame

    def addRow(self):
        self.currentRow = self.currentRow + 1
        self.frame.rowconfigure(0, weight=1)
        self.resetColumn()

    def resetColumn(self):
        self.currentColumn = 0

    def OnCanvasConfigure(self, event):
        self.lastWidth = event.width
        self.canvas.itemconfigure(self.window, width=event.width)
        self.canvas.configure(scrollregion=self.canvas.bbox("all"))

    def ResizeScrollBar(self):
        self.vsb.update()
        self.canvas.configure(scrollregion=self.canvas.bbox("all"))

def fnCalendarSet(date_var, ayear, amonth, aday):
    parent = ()
    tkCalendar(parent, ayear, amonth, aday, date_var )

def fnCalendar(date_var):
    parent = ()
    tkCalendar(parent, year, month, day, date_var )

class tkCalendar:
    def __init__(self, master, arg_year, arg_month, arg_day, arg_parent_updatable_var):
        self.update_var = arg_parent_updatable_var
        top = self.top = Toplevel(master)
        x, y = top.winfo_pointerxy()
        w = 200  # based on canvas size
        h = 220
        # This technique starts window just below the cursor.
        self.top.geometry('%dx%d+%d+%d' % (w, h, x, y))
        try:
            self.intmonth = int(arg_month)
        except:
            self.intmonth = int(1)
        self.canvas = Canvas(top, width=200, height=220, relief=RIDGE, background="white", borderwidth=1)
        self.canvas.create_rectangle(0, 0, 303, 30, fill="#a4cae8", width=0)
        self.canvas.create_text(100, 17, text=strtitle, font=fntc, fill="#2024d6")
        stryear = str(arg_year)

        self.year_var = StringVar()
        self.year_var.set(stryear)
        self.lblYear = Label(top, textvariable=self.year_var, font=fnta, background="white")
        self.lblYear.place(x=85, y=30)

        self.month_var = StringVar()
        strnummonth = str(self.intmonth)
        strmonth = dictmonths[strnummonth]
        self.month_var.set(strmonth)

        self.lblYear = Label(top, textvariable=self.month_var, font=fnta, background="white")
        self.lblYear.place(x=85, y=50)
        # Variable muy usada
        tagBaseButton = "Arrow"
        self.tagBaseNumber = "DayButton"
        # draw year arrows
        x, y = 40, 43
        tagThisButton = "leftyear"
        tagFinalThisButton = tuple((tagBaseButton, tagThisButton))
        self.fnCreateLeftArrow(self.canvas, x, y, tagFinalThisButton)
        x, y = 150, 43
        tagThisButton = "rightyear"
        tagFinalThisButton = tuple((tagBaseButton, tagThisButton))
        self.fnCreateRightArrow(self.canvas, x, y, tagFinalThisButton)
        # draw month arrows
        x, y = 40, 63
        tagThisButton = "leftmonth"
        tagFinalThisButton = tuple((tagBaseButton, tagThisButton))
        self.fnCreateLeftArrow(self.canvas, x, y, tagFinalThisButton)
        x, y = 150, 63
        tagThisButton = "rightmonth"
        tagFinalThisButton = tuple((tagBaseButton, tagThisButton))
        self.fnCreateRightArrow(self.canvas, x, y, tagFinalThisButton)
        # Print days
        self.canvas.create_text(100, 90, text=strdays, font=fnta)
        self.canvas.pack(expand=1, fill=BOTH)
        self.canvas.tag_bind("Arrow", "<ButtonRelease-1>", self.fnClick)
        self.canvas.tag_bind("Arrow", "<Enter>", self.fnOnMouseOver)
        self.canvas.tag_bind("Arrow", "<Leave>", self.fnOnMouseOut)
        self.fnFillCalendar()

    def fnCreateRightArrow(self, canv, x, y, strtagname):
        canv.create_polygon(x, y, [[x + 0, y - 5], [x + 10, y - 5], [x + 10, y - 10], [x + 20, y + 0], [x + 10, y + 10],
                                   [x + 10, y + 5], [x + 0, y + 5]], tags=strtagname, fill="blue", width=0)

    def fnCreateLeftArrow(self, canv, x, y, strtagname):
        canv.create_polygon(x, y, [[x + 10, y - 10], [x + 10, y - 5], [x + 20, y - 5], [x + 20, y + 5], [x + 10, y + 5],
                                   [x + 10, y + 10]], tags=strtagname, fill="blue", width=0)

    def fnClick(self, event):
        owntags = self.canvas.gettags(CURRENT)
        if "rightyear" in owntags:
            intyear = int(self.year_var.get())
            intyear += 1
            stryear = str(intyear)
            self.year_var.set(stryear)
        if "leftyear" in owntags:
            intyear = int(self.year_var.get())
            intyear -= 1
            stryear = str(intyear)
            self.year_var.set(stryear)
        if "rightmonth" in owntags:
            if self.intmonth < 12:
                self.intmonth += 1
                strnummonth = str(self.intmonth)
                strmonth = dictmonths[strnummonth]
                self.month_var.set(strmonth)
            else:
                self.intmonth = 1
                strnummonth = str(self.intmonth)
                strmonth = dictmonths[strnummonth]
                self.month_var.set(strmonth)
                intyear = int(self.year_var.get())
                intyear += 1
                stryear = str(intyear)
                self.year_var.set(stryear)
        if "leftmonth" in owntags:
            if self.intmonth > 1:
                self.intmonth -= 1
                strnummonth = str(self.intmonth)
                strmonth = dictmonths[strnummonth]
                self.month_var.set(strmonth)
            else:
                self.intmonth = 12
                strnummonth = str(self.intmonth)
                strmonth = dictmonths[strnummonth]
                self.month_var.set(strmonth)
                intyear = int(self.year_var.get())
                intyear -= 1
                stryear = str(intyear)
                self.year_var.set(stryear)
        self.fnFillCalendar()

    def fnFillCalendar(self):
        init_x_pos = 20
        arr_y_pos = [110, 130, 150, 170, 190, 210]
        intposarr = 0
        self.canvas.delete("DayButton")
        self.canvas.update()
        intyear = int(self.year_var.get())
        monthcal = calendar.monthcalendar(intyear, self.intmonth)
        for row in monthcal:
            xpos = init_x_pos
            ypos = arr_y_pos[intposarr]
            for item in row:
                stritem = str(item)
                if stritem == "0":
                    xpos += 27
                else:
                    tagNumber = tuple((self.tagBaseNumber, stritem))
                    self.canvas.create_text(xpos, ypos, text=stritem, font=fnta, tags=tagNumber)
                    xpos += 27
            intposarr += 1
        self.canvas.tag_bind("DayButton", "<ButtonRelease-1>", self.fnClickNumber)
        self.canvas.tag_bind("DayButton", "<Enter>", self.fnOnMouseOver)
        self.canvas.tag_bind("DayButton", "<Leave>", self.fnOnMouseOut)

    def fnClickNumber(self, event):
        owntags = self.canvas.gettags(CURRENT)
        for x in owntags:
            if x not in ("current", "DayButton"):
                strdate = (str(self.year_var.get()) + "-" + str(self.month_var.get()) + "-" + str(x))
                self.update_var.set(strdate)
                self.top.withdraw()
                # event.widget.update_idletasks()

    def fnOnMouseOver(self, event):
        self.canvas.move(CURRENT, 1, 1)
        self.canvas.update()

    def fnOnMouseOut(self, event):
        self.canvas.move(CURRENT, -1, -1)
        self.canvas.update()

class messageBarScore(Frame):
    def __init__(self, parent):
        Frame.__init__(self, parent)
        messageFrame = Frame(self)
        self.myMessage = '''   '''
        self.messageDisplay = Label(messageFrame, text=self.myMessage, relief='flat', font=AppDefaultFontAvg)
        self.messageDisplay.pack(side=LEFT, fill=X)
        messageFrame.pack()

    def clearMessage(self):
        self.myMessage = '''   '''
        self.messageDisplay.config(text=self.myMessage)

    def requiredFieldMessage(self):
        msg = '''PLEASE NOTE: Fields with the {0} background are required.'''.format(AppDefaultRequiredFieldBackground)
        self.newMessage('warning', msg)

    def newMessage(self, msgType, msg):
        if msgType == 'info':
            self.messageDisplay.config(fg=AppDefaultForeground)
        elif msgType == 'error':
            self.bell()
            self.messageDisplay.config(fg='red')
        elif msgType == 'warning':
            self.messageDisplay.config(fg='green')
        self.messageDisplay.config(text=msg)

class messageBar(Frame):
    def __init__(self, parent):
        Frame.__init__(self, parent)
        messageFrame=Frame(self)
        self.myMessage = '''   '''
        self.messageDisplay = Label(messageFrame, text=self.myMessage, relief='flat')
        self.messageDisplay.pack(side=LEFT, fill=X)
        messageFrame.pack()
        self.parent = parent

    def wildcardMessage(self, *args):
        msg = '''PLEASE NOTE: % is the wildcard search symbol ie: %jean%.'''.format(AppDefaultRequiredFieldBackground)
        self.newMessage('warning', msg)
        
    def clearMessage(self, *args):
        self.myMessage = '''   '''
        self.messageDisplay.config(text=self.myMessage)

    def requiredTeeDetailsMessage(self, *args):
        msg = '''PLEASE NOTE: Fields in the Tee Details Table are required.'''.format(AppDefaultRequiredFieldBackground)
        self.newMessage('warning', msg)

    def currentFunctionUnderConstruction(self):
        msg = '''NOTE: Current function is under construction.'''
        self.newMessage('warning', msg)

    def noExpensesForPeriodRequested(self):
        msg = '''NOTE: There are no expenses for the requested period.'''
        self.newMessage('warning', msg)

    def endDateOnOrAfterStartMessage(self):
        msg = '''Error: End date must be the same or after the Start date. DATE MUST BE CHANGED TO CONTINUE.'''
        self.newMessage('error', msg)

    def requiredFieldMessage(self):
        msg = '''PLEASE NOTE: Fields with the {0} background are required.'''.format(AppDefaultRequiredFieldBackground)
        self.newMessage('warning', msg)

    def roundDetailsIncompleteMessage(self):
        msg = '''MISSING DATA: All Fields from your round are required.'''.format(AppDefaultRequiredFieldBackground)
        self.newMessage('error', msg)

    def newMessage(self, msgType, msg):
        if msgType == 'info':
            self.messageDisplay.config(fg=AppDefaultForeground)
        elif msgType == 'error':
            self.bell()
            self.messageDisplay.config(fg='red')
        elif msgType == 'warning':
            self.messageDisplay.config(fg='green')

        self.messageDisplay.config(text=msg)
        self.parent.after(40000, self.clearMessage)  # Leave message for a minute

class AppLastUpdateFormat(Tix.Label):
    def __init__(self, parent):
        Tix.Label.__init__(self, parent)
        aText = " "
        self.config(text=aText, font=fontAverageI)
        self.config(background=AppAccentBackground)

    def load(self, aDate, updaterID):
        updaterFullName = CRUDLoginAccess.getGolferFullNamefromLoginID(updaterID)
        aText = "Last Updated: {0} ({1})".format(convertOrdinaltoString(aDate), updaterFullName)
        self.config(text=aText, font=fontAverageI)


class AppFrame(Frame):
    def __init__(self, parent, columnTotal):
        Frame.__init__(self, parent)
        self.config(border=AppDefaultBorderWidth, takefocus=0)
        self.row = 0
        self.column = 0
        self.columnTotal = columnTotal
        for i in range(self.columnTotal):
            self.columnconfigure(i, weight=1)
        self.rowconfigure(self.row, weight=0)

    def noBorder(self):
        self.config(border=0)

    def aRelief(self):
        self.configure(relief= 'ridge')

    def stretchCurrentRow(self, aWeight):
        self.rowconfigure(self.row, weight=aWeight)

    def addTitle(self, aTitle):
        aLabel = AppLineSectionTitle(self, aTitle)
        aLabel.grid(row=self.row, column=self.column, columnspan=self.columnTotal, sticky=N)
        self.row = self.row + 1

    def defaultBorder(self):
        self.config(border=AppDefaultBorderWidth)

    def stretchCurrentRow(self):
        self.rowconfigure(self.row, weight=1)

    def addRow(self):
        self.row = self.row + 1
        self.rowconfigure(self.row, weight=0)

    def addColumn(self):
        self.column = self.column + 1

    def resetColumn(self):
        self.column = 0

    def addAccentBackground(self):
        self.config(background=AppAccentBackground)

    def addDeletedHighlight(self):
        self.config(highlightcolor=AppDefaultForeground, highlightthickness=AppDeletedHighlightBorderWidth, takefocus=1)


class AppFieldsFrame(Frame):
    def __init__(self, parent, columnTotal):
        Frame.__init__(self, parent)
        self.config(border=AppDefaultBorderWidth)
        self.row=0
        self.column=0
        self.columnTotal=columnTotal
        for i in range(self.columnTotal):
            self.columnconfigure(i, weight = 1)
            
        self.config(background=AppAccentBackground)

    def stretchSpecifyColumnAndWeight(self, aColumn, aWeight):
        self.columnconfigure(aColumn, weight=aWeight)

    def noBorder(self):
        self.config(border=0)
    
    def setBorder(self, aValue):
        self.config(border=aValue)

    def addRow(self):
        self.row = self.row + 1
        self.rowconfigure(self.row, weight=0)

    def stretchCurrentRow(self):
        self.rowconfigure(self.row, weight=1)

    def stretchLargeCurrentColumn(self):
        self.columnconfigure(self.column, weight=4)

    def addColumn(self):
        self.column = self.column + 1
        
    def resetColumn(self):
        self.column = 0

    def setBackgroundDefault(self):
        self.config(background=AppDefaultBackground)


class AppCourseIDFrame(Frame):
    def __init__(self, parent, myMsgBar):
        Frame.__init__(self, parent)
        self.myMsgBar = myMsgBar
        self.config(background=AppDefaultBackground,highlightcolor=AppDefaultForeground,
                    highlightthickness = AppHighlightBorderWidth, takefocus=0)
        self.firstLine = Tix.Label(self, text="  ", font=fontHuge2I, fg=AppDefaultForeground, justify=LEFT)
        self.firstLine.grid(column=0, row=0, sticky=N + W + E + S)

        self.secondLine = Tix.Label(self, text=" ", font=fontSmallB, fg=AppDefaultForeground, justify=CENTER)
        self.secondLine.grid(column=0, row=1, sticky=N + W + E + S)

        self.thirdLine = Tix.Label(self, text=" ", font=fontSmallB,
                                   fg=AppDefaultForeground, justify=CENTER)
        self.thirdLine.grid(column=0, row=2, sticky=N + W + E + S)

    def load(self, UserData):

        #cursor.execute('''CREATE TABLE Courses(
        #   courseID INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
        #   courseNAME          CHAR(60)    NOT NULL,
        #   courseSNAME         CHAR(30)    NOT NULL,
        #   courseLogo          CHAR(100),
        #   established         CHAR(4),
        #   deleted              CHAR(1),
        #   lastModified         INTEGER,
        #   updaterID            INTEGER
        #   );''')
        self.firstLine.destroy()
        courseFull = "{0}".format(UserData[1])
        self.firstLine = Tix.Label(self, text=courseFull, font=fontHuge2I, fg=AppDefaultForeground, justify=LEFT)
        self.firstLine.grid(column=0, row=0, sticky=N + W + E + S)

        courseAddr = CRUDAddresses.get_courseAddresses(UserData[0])
        if len(courseAddr) > 0:
            courseLocation = "{0}, {1}, {2}".format(courseAddr[0][2], courseAddr[0][3], courseAddr[0][4])
        else:
            courseLocation = ""
        self.secondLine.destroy()
        self.secondLine = Tix.Label(self, text=courseLocation, font=fontSmallB, fg=AppDefaultForeground, justify=CENTER)
        self.secondLine.grid(column=0, row=1, sticky=N + W + E + S)

        self.thirdLine.destroy()
        #
        #  MUST CHECK IF NONE TYPE
        #
        if len(UserData[4]) > 0:
            self.thirdLine = Tix.Label(self, text="Established: {0}".format(UserData[4]), font=fontSmallB,
                              fg=AppDefaultForeground, justify=CENTER)
            self.thirdLine.grid(column=0, row=2, sticky=N + W + E + S)
        else:
            self.thirdLine = Tix.Label(self, text=" ", font=fontSmallB,
                                       fg=AppDefaultForeground, justify=CENTER)
            self.thirdLine.grid(column=0, row=2, sticky=N + W + E + S)

class AppUserIDFrame(Frame):
    def __init__(self, parent, myMsgBar):
        Frame.__init__(self, parent)
        self.myMsgBar = myMsgBar
        self.config(background=AppDefaultBackground,highlightcolor=AppDefaultForeground,
                    highlightthickness = AppHighlightBorderWidth, takefocus=0)

    def load(self, UserData):
        refDate = convertDateStringToOrdinal(strdate)  # today in ordinal
        if UserData[4] != '' and UserData[4] != None:
            golferAge = getAge(refDate, int(UserData[4]))  # birthday stored in ordinal
            golferFull = "{0} {1} ({2})".format(UserData[1], UserData[2], golferAge)
        else:
            golferFull = "{0} {1}".format(UserData[1], UserData[2])
        label = Tix.Label(self, text=golferFull, font=fontHuge2I, fg=AppDefaultForeground, justify=LEFT)
        label.grid(column=0, row=0, sticky=N + W + E + S)
        golferAddr = CRUDAddresses.get_golferAddresses(UserData[0])
        if len(golferAddr) > 0:
            golferLocation = "{0}, {1}, {2}".format(golferAddr[0][2], golferAddr[0][3], golferAddr[0][4])
        else:
            golferLocation = ""
        label = Tix.Label(self, text=golferLocation, font=fontSmallB, fg=AppDefaultForeground, justify=CENTER)
        label.grid(column=0, row=1, sticky=N + W + E + S)
        currentIdx = calculateCurrentHdcp(UserData[0])
        label = Tix.Label(self, text=currentIdx, font=fontMonstrousB, fg=AppDefaultForeground, justify=CENTER)
        label.grid(column=0, row=2, sticky=N + W + E + S)

class AppPictureFrame(Frame):
    def __init__(self, parent, defaultIMG, picturePath, aWidth, aHeight, myMsgBar):
        Frame.__init__(self, parent)
        #
        #  A default width is assigned, it can be changed
        #
        self.myMsgBar = myMsgBar
        self.picWidth = aWidth   # Default
        self.picHeight = aHeight # Default
        self.config(width=self.picWidth, height=self.picHeight, background=AppDefaultBackground,
                    highlightcolor=AppDefaultForeground, highlightthickness = AppHighlightBorderWidth, takefocus=0)
#        self.bind('<Enter>', widgetEnter)
        self.pictureDir = "{0}/{1}".format(AppRootDir, picturePath)
        self.defaultIMG = defaultIMG
        self.picturePath = os.path.normpath(picturePath)
        self.loadedImage = None
        self.load("")

    def stretchCurrentRow(self):
        self.rowconfigure(0, weight=1)

    def load(self, IMG): 
        try:
            im1=Image.open('{0}/{1}'.format(self.picturePath,IMG))
            PIC = '{0}/{1}'.format(self.picturePath, IMG)
            self.loadedImage = IMG
        except IOError:
            PIC = '{0}/{1}'.format(self.picturePath, self.defaultIMG)
            self.loadedImage = self.defaultIMG

        original = Image.open(PIC)
        image1 = resizeImage(original,self.picWidth,self.picHeight)                
        self.panel1 = Label(self, image=image1, border=0)
        self.panel1.grid(row = 0, column=0, sticky=N+W)
        self.display = image1
        
    def get(self):
        return self.loadedImage 
        
    def clear(self):
        self.panel1.destroy()
        self.load("")
       
    def disable(self):
        self.config(takefocus=0)
        self.unbind('<Enter>')
        self.panel1.unbind('<Button-1>')
        
    def enable(self):
        self.config(takefocus=1)
        self.bind('<Enter>', widgetEnter)
        self.panel1.bind('<Button-1>', self.getNewImage)
        
    def getNewImage(self, event):
        #
        #  Select existing image in the media image dir or a personal image.
        #  Will verify if image is good and will copy it to the media directory.
        #  If file exist, will change the name to avoid conflicts or losses.
        #

        ftypes = myImageFileFormats
        dlg = Fd.Open(self, initialdir=self.picturePath, filetypes = ftypes)
        imageFileSelectedFullPath = dlg.show()
        imageFileSelectedFullPathNormalized = os.path.normpath(imageFileSelectedFullPath)
        try:
            if os.path.exists(imageFileSelectedFullPath):
                im1=Image.open(imageFileSelectedFullPath) # Verifies we have a good image
                pathToImageFile, imageFileSelected = os.path.split(imageFileSelectedFullPathNormalized)
                if pathToImageFile == self.picturePath:
                    #
                    #  Just use the new image and save the filename
                    #
                    self.loadedImage = imageFileSelected
                    self.load(imageFileSelected)
                else:
                    #
                    #  1. First verify that a file with the same name does not exist in the picture directory
                    #  2. If it does, generate a new filename with a random number
                    #  3. Once a unique filename is acheived, copy it in the picture dictionary
                    #  4. save and load the new picture/
                    #
                    newImageFilename = imageFileSelected
                    futureFullPathImage = "{0}/{1}".format(self.picturePath,imageFileSelected)
                    while (os.path.exists(futureFullPathImage)):
                        # generate a random number to modify file name
                        randomNum = int(random.random()*10000)
                        filename, file_extension = os.path.splitext(newImageFilename)
                        newImageFilename = "{0}{1}{2}".format(filename,str(randomNum),file_extension)
                        futureFullPathImage = "{0}\{1}".format(self.picturePath, newImageFilename)
                    shutil.copy2(imageFileSelectedFullPathNormalized, futureFullPathImage)   # copy the file in the media directory
                    #
                    #  Now, just use the new image and save the filename
                    #
                    self.loadedImage = newImageFilename
                    self.load(newImageFilename)
        except IOError:
            aMsg = 'ERROR: MyGolf Application is unable to display this image. Please select another image.'
            self.myMsgBar.newMessage('error', aMsg) 
 
    def setWidth(self, aWidth):
        self.config(width=aWidth)
        self.picWidth = aWidth
        
    def setHeight(self, aHeight):
        self.config(height=aHeight)
        self.picHeight = aHeight       
        
class AppLineSectionTitle(Label):
    def __init__(self, parent, aLabel):
        Label.__init__(self, parent)
        self.config(text=aLabel, font=fontHugeB)
        
    def justification(self, aValue):
        self.config(justify=aValue)

class AppButtonV1(Label):
    def __init__(self, parent, anImageFile, anImageFileGrey, aBorderWidth, aMethodForButton1=None):
        Label.__init__(self, parent)
        self.parent = parent
        self.anImageFile = anImageFile
        self.anImageFileGrey = anImageFileGrey
        self.aBorderWidth = aBorderWidth
        self.aMethodForButton1 = aMethodForButton1
        self.theImage = Image.open(anImageFile)
        self.theImageGREY = Image.open(anImageFileGrey)
        self.image = resizeImage(self.theImage, AppButtonLargeHeight, AppButtonLargeWidth)
        self.imageGREY = resizeImage(self.theImageGREY, AppButtonLargeHeight, AppButtonLargeWidth)
        self.configure(border=aBorderWidth, relief='groove', image=self.image)
        self.tipTool = False
        self.enable()

    def changeButtonSize(self, aWidth, aHeight):
        self.image = resizeImage(self.theImage, aHeight, aWidth)
        self.imageGREY = resizeImage(self.theImageGREY, aHeight, aWidth)
        self.configure(border=self.aBorderWidth, relief='groove', image=self.image)

    def disable(self):
        self.unbind('<Enter>')
        self.unbind('<Leave>')
        if self.aMethodForButton1 != None:
            self.unbind('<Button-1>')
        self.configure(image=self.imageGREY)

    def enable(self):
        self.bind('<Enter>', lambda event=Event: self._myButtonHighlight(event))
        self.bind('<Leave>', lambda event=Event: self._myButtonReturnFromHighlight(event))
        if self.aMethodForButton1 != None:
            self.bind('<Button-1>', self.aMethodForButton1)
        self.configure(image=self.image)

    def _myButtonHighlight(self, event):
#        event.widget.configure(background=AppHighlightBackground)
        event.widget.configure(relief='ridge')

        if self.tipTool == True:
            self.enter()
            self.after(toolTipTime, self.close)

    def _myButtonReturnFromHighlight(self, event):
        event.widget.configure(relief='groove', background=AppDefaultBackground)
        if self.tipTool == True:
            self.close()

    def setAsListButton(self):
        self.image = resizeImage(Image.open(self.anImageFile), AppButtonSmallHeight, AppButtonSmallWidth)
        self.imageGREY = resizeImage(Image.open(self.anImageFileGrey), AppButtonSmallHeight, AppButtonSmallWidth)
        self.configure(border=self.aBorderWidth, relief='groove', image=self.image)

    def addToolTip(self, tipMsg):
        self.tipTool = True
        self.tipMsg = tipMsg

    def enter(self, event=None):
        x = y = 0
        x, y, cx, cy = self.bbox("insert")
        x += self.winfo_rootx() + 25
        y += self.winfo_rooty() + 20
        # creates a toplevel window
        self.tw = Toplevel(self)
        # Leaves only the label and removes the app window
        self.tw.wm_overrideredirect(True)
        self.tw.wm_geometry("+%d+%d" % (x, y))
        label = ttk.Label(self.tw, text=self.tipMsg, style='toolTip.TLabel')
        label.pack(ipadx=1)

    def close(self, event=None):
        if self.tw:
            self.tw.destroy()

class AppShowHide9Button(Label):
    def __init__(self, parent, aHide9Image, aShow9Image, aBorderWidth, aMethodForButton1=None):
        Label.__init__(self, parent)
        self.parent = parent
        self.aBorderWidth = aBorderWidth
        self.aMethodForButton1 = aMethodForButton1
        self.show9HoleGameImage = aShow9Image
        self.hide9HoleGameImage = aHide9Image
        self.configure(border=aBorderWidth, background = game9HoleBackground, relief='groove', image=self.show9HoleGameImage)
        self.tipTool = False
        self.enable()

    def show9Image(self):
        self.configure(image=self.show9HoleGameImage)

    def hide9Image(self):
        self.configure(image=self.hide9HoleGameImage)

    def disable(self):
        self.unbind('<Enter>')
        self.unbind('<Leave>')
        if self.aMethodForButton1 != None:
            self.unbind('<Button-1>')

    def enable(self):
        self.bind('<Enter>', lambda event=Event: self._myButtonHighlight(event))
        self.bind('<Leave>', lambda event=Event: self._myButtonReturnFromHighlight(event))
        if self.aMethodForButton1 != None:
            self.bind('<Button-1>', self.aMethodForButton1)
        self.hide9Image()

    def _myButtonHighlight(self, event):
        event.widget.configure(relief='ridge', background=game9HoleBackground)

        if self.tipTool == True:
            self.enter()
            self.after(toolTipTime, self.close)

    def _myButtonReturnFromHighlight(self, event):
        event.widget.configure(relief='groove', background=game9HoleBackground)
        if self.tipTool == True:
            self.close()

    def addToolTip(self, tipMsg):
        self.tipTool = True
        self.tipMsg = tipMsg

    def changeTipMsg(self, tipMsg):
        self.tipMsg = tipMsg

    def enter(self, event=None):
        x = y = 0
        x, y, cx, cy = self.bbox("insert")
        x += self.winfo_rootx() + 25
        y += self.winfo_rooty() + 20
        # creates a toplevel window
        self.tw = Toplevel(self)
        # Leaves only the label and removes the app window
        self.tw.wm_overrideredirect(True)
        self.tw.wm_geometry("+%d+%d" % (x, y))
        label = ttk.Label(self.tw, text=self.tipMsg, style='toolTip.TLabel')
        label.pack(ipadx=1)

    def close(self, event=None):
        if self.tw:
            self.tw.destroy()

class AppYesNoBoxScoreCardV1(Label):
    def __init__(self, parent, aFont, aRelief, aBackground, aWidth):
        Label.__init__(self, parent)

        self.yesSet = False
        self.configure(font=aFont, text=" ", relief=aRelief, bg=aBackground, width=aWidth,
                       foreground=AppDefaultForeground, highlightthickness=0,
                       justify=CENTER)
        self.unSet()

    def get(self):
        if self.yesSet == True:
            return 'Y'
        else:
            return 'N'

    def set(self):
        self.yesSet = True
        self.configure(text='Y')

    def unSet(self):
        self.yesSet = False
        self.configure(text='N')

    def invertAction(self, event):
        if self.yesSet == True:
            self.unSet()
        else:
            self.set()

    def enable(self):
        self.bind('<Button-1>', self.invertAction)
        self.bind('<Enter>', self.invertAction)

    def disable(self):
        self.unbind('<Button-1>')
        self.unbind('<Enter>')

class AppYesNoBoxScoreCard(Entry):
    def __init__(self, parent, aFont, aRelief, aBackground, aWidth):
        Entry.__init__(self, parent)

        self.yesSet = False
        self.configure(font=aFont, text=" ", relief=aRelief, bg=aBackground, width=aWidth,
                       foreground=AppDefaultForeground, highlightcolor=roundDetailsScoreColor, highlightthickness=0,
                       justify=CENTER)
        self.unSet()

    def get(self):
        if self.yesSet == True:
            return 'Y'
        else:
            return 'N'

    def set(self):
        self.yesSet = True
        self.configure(state = 'normal')
        self.configure(text='Y')
        self.configure(state='disabled')

    def unSet(self):
        self.yesSet = False
        self.configure(state = 'normal')
        self.configure(text='N')
        self.configure(state='disabled')

    def invertAction(self, event):
        if self.yesSet == True:
            self.unSet()
        else:
            self.set()

    def enable(self):
        self.bind('<Button-1>', self.invertAction)
        self.bind('<Return>', self.invertAction)

    def disable(self):
        self.unbind('<Button-1>')
        self.unbind('<Return>')


class AppCheckMark(Label):
    def __init__(self, parent, aFont, aRelief, aBackground, aWidth):
        Label.__init__(self, parent)

        self.checkMarkSet = False
        self.imageEmpty = resizeImage(Image.open(theEmptyImage), AppButtonLargeHeight, AppButtonLargeWidth)
        self.imageSet = resizeImage(Image.open(theCheckmarkImage), AppButtonLargeHeight, AppButtonLargeWidth)
        self.imageSetWhite = resizeImage(Image.open(theCheckmarkImageWhite), AppButtonLargeHeight, AppButtonLargeWidth)
        self.configure(font=aFont, image=self.imageEmpty, relief=aRelief, bg=aBackground, width=aWidth,
                       foreground=AppDefaultForeground)
        self.unSet()

    def setBorderWidth(self, aWidth):
        self.configure(border=aWidth)

    def get(self):
        if self.checkMarkSet == True:
            return 'Y'
        else:
            return 'N'

    def set(self):
        self.checkMarkSet = True
        self.configure(image=self.imageSet)

    def unSet(self):
        self.checkMarkSet = False
        self.configure(image=self.imageEmpty)

    def viewEnableScorecard(self, aBackground):
        if self.checkMarkSet == True:
            self.configure(image=self.imageSet, bg=aBackground)
        else:
            self.configure(image=self.imageEmpty, bg=aBackground)

    def viewDisableScorecard(self, aBackground):
        if self.checkMarkSet == True:
            self.configure(image=self.imageSetWhite, bg=aBackground)
        else:
            self.configure(image=self.imageEmpty, bg=aBackground)


class AppCheckMarkV2(Label):
    def __init__(self, parent, aBorderWidth, aMethodForButton1=None):
        Label.__init__(self, parent)
        self.parent = parent
        self.aBorderWidth = aBorderWidth
        self.aMethodForButton1 = aMethodForButton1
        self.imageEmpty = resizeImage(Image.open(theEmptyCheckmarkImage), AppButtonLargeHeight, AppButtonLargeWidth)
        self.imageSet = resizeImage(Image.open(theCheckmarkBoxedImage), AppButtonLargeHeight, AppButtonLargeWidth)
        self.imageSetWhite = resizeImage(Image.open(theCheckmarkImageWhite), AppButtonLargeHeight, AppButtonLargeWidth)
        self.configure(border=aBorderWidth, relief='groove', image=self.imageEmpty)
        self.tipTool = False
        self.selectSet = False
        self.enable()

    def get(self):
        if self.selectSet == False:
            return 'N'
        else:
            return 'Y'

    def set(self):
        self.selectSet = True
        self.configure(image=self.imageSet)

    def unSet(self):
        self.selectSet = False
        self.configure(image=self.imageEmpty)

    def clear(self):
        self.selectSet = False
        self.configure(image=self.imageEmpty)

    def disable(self):
        self.unbind('<Enter>')
        self.unbind('<Leave>')
        self.unbind('<Button-1>')

    def enable(self):
        self.bind('<Enter>', lambda event=Event: self._myButtonHighlight(event))
        self.bind('<Leave>', lambda event=Event: self._myButtonReturnFromHighlight(event))
        if self.aMethodForButton1 != None:
            self.bind('<Button-1>', self.aMethodForButton1)
        else:
            self.bind('<Button-1>', self.invertSelection)

    def invertSelection(self, *args):
        if self.selectSet == False:
            self.selectSet = True
            self.configure(image=self.imageSet)
        else:
            self.selectSet = False
            self.configure(image=self.imageEmpty)

    def _myButtonHighlight(self, event):
        event.widget.configure(relief='ridge')

        if self.tipTool == True:
            self.enter()
            self.after(toolTipTime, self.close)

    def _myButtonReturnFromHighlight(self, event):
        event.widget.configure(relief='groove', background=AppDefaultBackground)
        if self.tipTool == True:
            self.close()

    def addToolTip(self, tipMsg):
        self.tipTool = True
        self.tipMsg = tipMsg

    def enter(self, event=None):
        x = y = 0
        x, y, cx, cy = self.bbox("insert")
        x += self.winfo_rootx() + 25
        y += self.winfo_rooty() + 20
        # creates a toplevel window
        self.tw = Toplevel(self)
        # Leaves only the label and removes the app window
        self.tw.wm_overrideredirect(True)
        self.tw.wm_geometry("+%d+%d" % (x, y))
        label = ttk.Label(self.tw, text=self.tipMsg, style='toolTip.TLabel')
        label.pack(ipadx=1)

    def close(self, event=None):
        if self.tw:
            self.tw.destroy()

class AppButton(Label):
    def __init__(self, parent, anImageEnable, anImageDisable, aBorderWidth, aMethodForButton1=None):
        Label.__init__(self, parent)
        self.parent = parent
        self.aBorderWidth = aBorderWidth
        self.aMethodForButton1 = aMethodForButton1
        self.image = anImageEnable
        self.imageGREY = anImageDisable
        self.configure(border=aBorderWidth, relief='groove', image=self.image)
        self.tipTool = False
        self.enable()

    def disable(self):
        self.unbind('<Enter>')
        self.unbind('<Leave>')
        if self.aMethodForButton1 != None:
            self.unbind('<Button-1>')
        self.configure(image=self.imageGREY)

    def enable(self):
        self.bind('<Enter>', lambda event=Event: self._myButtonHighlight(event))
        self.bind('<Leave>', lambda event=Event: self._myButtonReturnFromHighlight(event))
        if self.aMethodForButton1 != None:
            self.bind('<Button-1>', self.aMethodForButton1)
        self.configure(image=self.image)

    def _myButtonHighlight(self, event):
        event.widget.configure(relief='ridge')

        if self.tipTool == True:
            self.enter()
            self.after(toolTipTime, self.close)

    def _myButtonReturnFromHighlight(self, event):
        event.widget.configure(relief='groove', background=AppDefaultBackground)
        if self.tipTool == True:
            self.close()

    def addToolTip(self, tipMsg):
        self.tipTool = True
        self.tipMsg = tipMsg

    def enter(self, event=None):
        x = y = 0
        x, y, cx, cy = self.bbox("insert")
        x += self.winfo_rootx() + 25
        y += self.winfo_rooty() + 20
        # creates a toplevel window
        self.tw = Toplevel(self)
        # Leaves only the label and removes the app window
        self.tw.wm_overrideredirect(True)
        self.tw.wm_geometry("+%d+%d" % (x, y))
        label = ttk.Label(self.tw, text=self.tipMsg, style='toolTip.TLabel')
        label.pack(ipadx=1)

    def close(self, event=None):
        if self.tw:
            self.tw.destroy()


class AppButtonForList(Label):
    def __init__(self, parent, anImageFile, aBorderWidth):
        Label.__init__(self, parent)
        self.parent = parent
        image1 = resizeImage(Image.open(anImageFile), AppButtonSmallHeight, AppButtonSmallWidth)
        self.configure(border=aBorderWidth, relief='groove', image=image1)
        self.image1 = image1
        self.bind('<Enter>', lambda event=Event: self._myButtonHighlight(event))
        self.bind('<Leave>', lambda event=Event: self._myButtonReturnFromHighlight(event))

    def _myButtonHighlight(self, event):
        event.widget.configure(background=AppHighlightBackground)

    def _myButtonReturnFromHighlight(self, event):
        event.widget.configure(background=AppDefaultBackground)

class AppCommandFrame(Frame):
    def __init__(self, parent):
        Frame.__init__(self, parent)
        self.config(border=0)
        self.row = 0
        self.column = None
        self.columnTotal = 0
        self.rowconfigure(self.row, weight=0)


    def addRow(self):
        self.row = self.row + 1
        self.rowconfigure(self.row, weight=0)

    def addColumn(self):
        self.column = self.column + 1

    def noStretchColumn(self, columnNumber):
        self.columnconfigure(columnNumber, weight=0)

    def stretchRow(self, rowNumber):
        self.rowconfigure(rowNumber, weight=10)

    def resetColumn(self):
        self.column = 0

    def addNewCommandButton(self, aButton):
        if self.column == None:
            self.column = 0
            self.columnTotal = 1
        else:
            self.column = self.column + 1
            self.columnTotal = self.columnTotal + 1

        self.columnconfigure(self.column, weight=1)
        aButton.grid(row=self.row, column=self.column, sticky = N + E + S)

class AppTeeDetailsTable(Frame):
    def __init__(self, parent, myMsgBar):
        Frame.__init__(self, parent)
        self.row = 0
        self.column = 0
        self.myMsgBar = myMsgBar
        self.columnTotal = len(teeDetailsColumnHeading)
        self.config(border=AppDefaultBorderWidth, relief='ridge')
        self.defineColumnWeight()
        self.holePar = []
        self.holeHandicap = []
        self.holeYardage = []
        self.createHeaderRow()
        self.createHandicapRow()
        self.createYardageRow()
        self.createParRow()
        self.tableEnabled = False

    def defineColumnWeight(self):
        for i in range(self.columnTotal):
            self.columnconfigure(i, weight= teeDetailsColumnWeight[i])

    def addColumn(self):
        self.column = self.column + 1

    def addRow(self):
        self.row = self.row + 1
        self.rowconfigure(self.row, weight=2)

    def resetColumn(self):
        self.column = 0

    def createHeaderRow(self):
        self.rowconfigure(self.row, weight = 1)
        for i in range(self.columnTotal):
            aLabel = Label(self, font=fontBigB, text=teeDetailsColumnHeading[i], relief = teeDetailsRelief, bg=teeDetailsHeadingColor,
                           width = teeDetailsColumnWidth[i])
            aLabel.grid(row=self.row, column=i, sticky=E+W+S+N)

    def _holeYardage_check(self, d, i, P, s, S, v, V, W):
        valid = False

        if V == 'focusin':
            return True

        if V == 'focusout':
            self._updt_yardage_total()
            return True

        if int(d) == 0:  # Allow a delete
            return True

        if S.isdigit():
            if (int(P) > 0) and (int(P) < 999):
                valid = True
        if not valid:
            self.bell()
            aMsg = '''ERROR: Handicap field must be an integer between 1-999.'''
            self.myMsgBar.newMessage('error', aMsg)
        return valid

    #

    def _holePar_check(self, d, i, P, s, S, v, V, W):
        valid = False

        if V == 'focusin':
            return True

        if V == 'focusout':
            self._updt_par_total()
            return True

        if int(d) == 0:  # Allow a delete
            return True

        if S.isdigit():
            if (int(P) > 2) and (int(P) < 7):
                valid = True

        if not valid:
            self.bell()
            aMsg = '''ERROR: Handicap field must be an integer between 3-6.'''
            self.myMsgBar.newMessage('error', aMsg)
        return valid

    def _updt_par_total(self):
        frontTotal = 0
        backTotal = 0
        for i in range(len(self.holePar)):
            if i < 9:
                new = self.holePar[i].get()
                if len(new) > 0:
                    frontTotal = frontTotal + int(new)

            if i > 8:
                new = self.holePar[i].get()
                if len(new) > 0:
                    backTotal = backTotal + int(new)

            self.frontParTotal.config(state='normal')
            self.frontParTotal.delete(0, END)
            self.frontParTotal.insert(0, frontTotal)
            self.frontParTotal.config(state='disabled')

            self.backParTotal.config(state='normal')
            self.backParTotal.delete(0, END)
            self.backParTotal.insert(0, backTotal)
            self.backParTotal.config(state='disabled')

            self.parTotal.config(state='normal')
            self.parTotal.delete(0, END)
            self.parTotal.insert(0, frontTotal + backTotal)
            self.parTotal.config(state='disabled')

    def _holeHandicap_check(self, d, i, P, s, S, v, V, W):
        valid = False


        if V == 'focusin':
            return True

        if V == 'focusout':
            return True

        if int(d) == 0:  # Allow a delete
            return True

        if S.isdigit():
            if (int(P) > 0) and (int(P) < 19):
                valid = True
        if not valid:
            self.bell()
            aMsg = '''ERROR: Handicap field must be an integer between 1-18.'''
            self.myMsgBar.newMessage('error', aMsg)
        return valid


    def _updt_yardage_total(self):
        frontTotal = 0
        backTotal = 0
        for i in range(len(self.holeYardage)):
            if i < 9:
                new = self.holeYardage[i].get()
                if len(new) > 0:
                    frontTotal = frontTotal + int(new)

            if i > 8:
                new = self.holeYardage[i].get()
                if len(new) > 0:
                    backTotal = backTotal + int(new)

            self.frontYardageTotal.config(state='normal')
            self.frontYardageTotal.delete(0, END)
            self.frontYardageTotal.insert(0, frontTotal)
            self.frontYardageTotal.config(state='disabled')

            self.backYardageTotal.config(state='normal')
            self.backYardageTotal.delete(0, END)
            self.backYardageTotal.insert(0, backTotal)
            self.backYardageTotal.config(state='disabled')

            self.yardageTotal.config(state='normal')
            self.yardageTotal.delete(0, END)
            self.yardageTotal.insert(0, frontTotal + backTotal)
            self.yardageTotal.config(state='disabled')

    def createParRow(self):
        self.addRow()

        for i in range(len(teeDetailsColumnHeading)):
            num = len(self.holePar)
            if teeDetailsColumnHeading[i] == 'Hole':
                self.headingPar = Entry(self, relief=teeDetailsRelief, bg=teeDetailsYardageColor, font=fontAverageB,
                                              highlightcolor=teeDetailsYardageColor, highlightthickness=0, disabledbackground=teeDetailsYardageColor,
                                              background=teeDetailsYardageColor,disabledforeground=AppDefaultForeground,
                                              justify=CENTER, width=teeDetailsColumnWidth[i])
                self.headingPar.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.headingPar.insert(0, "Par")
                self.headingPar.configure(state='disabled')

            elif teeDetailsColumnHeading[i] == 'Out':
                #                self.frontParTotal = Label(self.teeDetailArea, text="0", relief='groove', bd=1, bg=teeDetailsParColor, width=6).grid(row=3, column=i, sticky=W+N)
                self.frontParTotal = Entry(self, relief=teeDetailsRelief, bg=teeDetailsParColor,font=fontAverage,
                                           highlightcolor=teeDetailsParColor, highlightthickness=0,
                                           disabledbackground=teeDetailsParColor,
                                           background=teeDetailsParColor,disabledforeground=AppDefaultForeground,
                                           justify=CENTER, width=teeDetailsColumnWidth[i])

                self.frontParTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.frontParTotal.insert(0, "0")
                self.frontParTotal.configure(state='disabled')

            elif teeDetailsColumnHeading[i] == 'In':
                #                self.backParTotal = Label(self.teeDetailArea, text="0", relief='groove', bd=1, bg=teeDetailsParColor, width=6).grid(row=self.row, column=i, sticky=W+N)
                self.backParTotal = Entry(self, relief=teeDetailsRelief, bg=teeDetailsParColor,font=fontAverage,
                                          highlightcolor=teeDetailsParColor, highlightthickness=0,
                                          disabledbackground=teeDetailsParColor,
                                          background=teeDetailsParColor,disabledforeground=AppDefaultForeground,
                                          justify=CENTER, width=teeDetailsColumnWidth[i])
                self.backParTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.backParTotal.insert(0, "0")
                self.backParTotal.configure(state='disabled')

            elif teeDetailsColumnHeading[i] == 'Total':
                #                self.ParTotal = Label(self.teeDetailArea, text="0", relief='groove', bd=1, bg=teeDetailsParColor, width=8).grid(row=self.row, column=i, sticky=W+N)
                self.parTotal = Entry(self, relief=teeDetailsRelief, bg=teeDetailsParColor, font=fontAverage,
                                      highlightcolor=teeDetailsParColor, highlightthickness=0,
                                      disabledbackground=teeDetailsParColor,
                                      background=teeDetailsParColor,disabledforeground=AppDefaultForeground,
                                      justify=CENTER, width=teeDetailsColumnWidth[i])
                self.parTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.parTotal.insert(0, "0")
                self.parTotal.configure(state='disabled')

            else:
                aValidationCommand = (self.register(self._holePar_check),
                                                    '%d', '%i', '%P', '%s', '%S', '%v', '%V', '%W')
                self.holePar.append(
                                        Entry(self, relief=teeDetailsRelief, bg=teeDetailsParColor,
                                              highlightcolor=teeDetailsParColor, highlightthickness=0, font=fontAverage,
                                              background=teeDetailsParColor, disabledforeground=teeDetailsForegroundDisabledColor,
                                              disabledbackground=teeDetailsBackgroundDisabledColor,justify=CENTER,
                                              width=teeDetailsColumnWidth[i],
                                              validatecommand=aValidationCommand, validate="all")
                                        )
                self.holePar[num].grid(row=self.row, column=i, sticky=E+W+N+S)

    def createHandicapRow(self):
        self.addRow()

        for i in range(len(teeDetailsColumnHeading)):
            num = len(self.holeHandicap)
            if teeDetailsColumnHeading[i] == 'Hole':
                self.handicapHeading = Entry(self, relief=teeDetailsRelief, bg=teeDetailsYardageColor, font=fontAverageB,
                                              highlightcolor=teeDetailsYardageColor, highlightthickness=0, disabledbackground=teeDetailsYardageColor,
                                              background=teeDetailsYardageColor,disabledforeground=AppDefaultForeground,
                                              justify=CENTER, width=teeDetailsColumnWidth[i])
                self.handicapHeading.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.handicapHeading.insert(0, "Handicap")
                self.handicapHeading.configure(state='disabled')

            elif teeDetailsColumnHeading[i] == 'Out':
                #                self.frontHandicapTotal = Label(self.teeDetailArea, text="0", relief='groove', bd=1, bg=teeDetailsHandicapColor, width=6).grid(row=3, column=i, sticky=W+N)
                self.handicapOut = Entry(self, text=' ', relief=teeDetailsRelief, bg=teeDetailsHandicapColor, font=fontAverageB,
                                         disabledforeground=teeDetailsForegroundDisabledColor, highlightthickness=0,
                                         disabledbackground=teeDetailsBackgroundDisabledColor,
                                        width=teeDetailsColumnWidth[i])
                self.handicapOut.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.handicapOut.configure(state='disabled')
            elif teeDetailsColumnHeading[i] == 'In':
                #                self.backHandicapTotal = Label(self.teeDetailArea, text="0", relief='groove', bd=1, bg=teeDetailsHandicapColor, width=6).grid(row=self.row, column=i, sticky=W+N)
                self.handicapIn = Entry(self, text=' ', relief=teeDetailsRelief, bg=teeDetailsHandicapColor, font=fontAverageB,
                                        disabledforeground=teeDetailsForegroundDisabledColor, highlightthickness=0,
                                        disabledbackground=teeDetailsBackgroundDisabledColor,
                                        width=teeDetailsColumnWidth[i])
                self.handicapIn.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.handicapIn.configure(state='disabled')

            elif teeDetailsColumnHeading[i] == 'Total':
                #                self.HandicapTotal = Label(self.teeDetailArea, text="0", relief='groove', bd=1, bg=teeDetailsHandicapColor, width=8).grid(row=self.row, column=i, sticky=W+N)
                self.handicapTotal = Entry(self, text=' ', relief=teeDetailsRelief, bg=teeDetailsHandicapColor, font=fontAverageB,
                                           disabledforeground=teeDetailsForegroundDisabledColor, highlightthickness=0,
                                           disabledbackground=teeDetailsBackgroundDisabledColor,
                                           width=teeDetailsColumnWidth[i])
                self.handicapTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.handicapTotal.configure(state='disabled')
            else:
                aValidationCommand = (self.register(self._holeHandicap_check),
                                  '%d', '%i', '%P', '%s', '%S', '%v', '%V', '%W')
                self.holeHandicap.append(
                                        Entry(self, relief=teeDetailsRelief, bg=teeDetailsHandicapColor, validate="all",
                                              highlightcolor=teeDetailsHandicapColor, highlightthickness=0, font=fontAverage,
                                              background=teeDetailsHandicapColor, disabledforeground=teeDetailsForegroundDisabledColor,
                                              disabledbackground=teeDetailsBackgroundDisabledColor,justify=CENTER, width=teeDetailsColumnWidth[i],
                                              validatecommand=aValidationCommand)
                                        )
                self.holeHandicap[num].grid(row=self.row, column=i, sticky=E+W+N+S)

    def createYardageRow(self):
        self.addRow()

        for i in range(len(teeDetailsColumnHeading)):
            num = len(self.holeYardage)
            if teeDetailsColumnHeading[i] == 'Hole':
                self.headingYardage = Entry(self, relief=teeDetailsRelief, bg=teeDetailsYardageColor, font=fontAverageB,
                                              highlightcolor=teeDetailsYardageColor, highlightthickness=0, disabledbackground=teeDetailsYardageColor,
                                              background=teeDetailsYardageColor,disabledforeground=AppDefaultForeground,
                                              justify=CENTER, width=teeDetailsColumnWidth[i])
                self.headingYardage.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.headingYardage.insert(0, "Yardage")
                self.headingYardage.configure(state='disabled')

            elif teeDetailsColumnHeading[i] == 'Out':
                #                self.frontYardageTotal = Label(self.teeDetailArea, text="0", relief='groove', bd=1, bg=teeDetailsYardageColor, width=6).grid(row=3, column=i, sticky=W+N)
                self.frontYardageTotal = Entry(self, relief=teeDetailsRelief, bg=teeDetailsYardageColor, font=fontAverage,
                                              highlightcolor=teeDetailsYardageColor, highlightthickness=0, disabledbackground=teeDetailsYardageColor,
                                              background=teeDetailsYardageColor,disabledforeground=AppDefaultForeground,
                                              justify=CENTER, width=teeDetailsColumnWidth[i])
                self.frontYardageTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.frontYardageTotal.insert(0, "0")
                self.frontYardageTotal.configure(state='disabled')

            elif teeDetailsColumnHeading[i] == 'In':
                #                self.backYardageTotal = Label(self.teeDetailArea, text="0", relief='groove', bd=1, bg=teeDetailsYardageColor, width=6).grid(row=self.row, column=i, sticky=W+N)
                self.backYardageTotal = Entry(self, relief=teeDetailsRelief, bg=teeDetailsYardageColor, font=fontAverage,
                                              highlightcolor=teeDetailsYardageColor, highlightthickness=0, disabledbackground=teeDetailsYardageColor,
                                              background=teeDetailsYardageColor,disabledforeground=AppDefaultForeground,
                                              justify=CENTER, width=teeDetailsColumnWidth[i])
                self.backYardageTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.backYardageTotal.insert(0, "0")
                self.backYardageTotal.configure(state='disabled')

            elif teeDetailsColumnHeading[i] == 'Total':
                #                self.YardageTotal = Label(self.teeDetailArea, text="0", relief='groove', bd=1, bg=teeDetailsYardageColor, width=8).grid(row=self.row, column=i, sticky=W+N)
                self.yardageTotal = Entry(self, relief=teeDetailsRelief, bg=teeDetailsYardageColor, font=fontAverage,
                                          highlightcolor=teeDetailsYardageColor, highlightthickness=0, disabledbackground=teeDetailsYardageColor,
                                          background=teeDetailsYardageColor,disabledforeground=AppDefaultForeground,
                                          justify=CENTER, width=teeDetailsColumnWidth[i])
                self.yardageTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.yardageTotal.insert(0, "0")
                self.yardageTotal.configure(state='disabled')

            else:
                aValidationCommand = (self.register(self._holeYardage_check),
                                  '%d', '%i', '%P', '%s', '%S', '%v', '%V', '%W')
                self.holeYardage.append(
                                        Entry(self, relief=teeDetailsRelief, bg=teeDetailsYardageColor, validate="all",
                                              highlightcolor=teeDetailsYardageColor, highlightthickness=0, font=fontAverage,
                                              background=teeDetailsYardageColor, disabledforeground=teeDetailsForegroundDisabledColor,
                                              disabledbackground=teeDetailsBackgroundDisabledColor,justify=CENTER,
                                              width=teeDetailsColumnWidth[i],
                                              validatecommand=aValidationCommand)
                                        )
                self.holeYardage[num].grid(row=self.row, column=i, sticky=E+W+N+S)

    def _highlight(self, event):
        event.widget.configure(relief=teeDetailsHighlightRelief, font=fontAverageB)
        event.widget.focus()

    def _removeHighlight(self, event):
        event.widget.configure(relief=teeDetailsRelief, font=fontAverage)

    def enable(self, doNotEnableBinds=False):
        self.tableEnabled = True
        for i in range(18):
            self.widgetEnable(self.holeHandicap[i])
            self.widgetEnable(self.holeYardage[i])
            self.widgetEnable(self.holePar[i])

        self.headingYardage.configure(disabledbackground=teeDetailsYardageColor,
                                       disabledforeground=AppDefaultForeground)

        self.yardageTotal.configure(disabledbackground=teeDetailsYardageColor,
                                    disabledforeground=AppDefaultForeground)
        self.frontYardageTotal.configure(disabledbackground=teeDetailsYardageColor,
                                         disabledforeground=AppDefaultForeground)
        self.backYardageTotal.configure(disabledbackground=teeDetailsYardageColor,
                                         disabledforeground=AppDefaultForeground)

        self.headingPar.configure(disabledbackground=teeDetailsParColor,
                                disabledforeground=AppDefaultForeground)

        self.parTotal.configure(disabledbackground=teeDetailsParColor,
                                disabledforeground=AppDefaultForeground)
        self.frontParTotal.configure(disabledbackground=teeDetailsParColor,
                                     disabledforeground=AppDefaultForeground)
        self.backParTotal.configure(disabledbackground=teeDetailsParColor,
                                    disabledforeground=AppDefaultForeground)

        self.handicapHeading.config(disabledbackground=teeDetailsHandicapColor,
                                    disabledforeground=AppDefaultForeground)
        self.handicapIn.config(disabledbackground=teeDetailsHandicapColor,
                               disabledforeground=AppDefaultForeground)
        self.handicapOut.config(disabledbackground=teeDetailsHandicapColor)
        self.handicapTotal.config(disabledbackground=teeDetailsHandicapColor,
                                  disabledforeground=AppDefaultForeground)

    def verifyRequiredFieldsTeeDetails(self):
        fieldsAllFilled = True
        for i in range(18):
            if len(self.holeHandicap[i].get()) == 0:
                fieldsAllFilled = False
                self.holeHandicap[i].focus()
                return False

        for i in range(18):
            if len(self.holeYardage[i].get()) == 0:
                fieldsAllFilled = False
                self.holeYardage[i].focus()
                return False

        for i in range(18):
            if len(self.holePar[i].get()) == 0:
                fieldsAllFilled = False
                self.holePar[i].focus()
                return False

        return fieldsAllFilled

    def populateTable(self, teeDetailsList):
        disableTable = False
        if self.tableEnabled == False:
            disableTable = True
            self.enable()

        for i in range(len(teeDetailsList)):
            self.holeYardage[i].insert(0, teeDetailsList[i][1])
            self.holeHandicap[i].insert(0,teeDetailsList[i][2])
            self.holePar[i].insert(0, teeDetailsList[i][3])

        self._updt_par_total()
        self._updt_yardage_total()

        if disableTable == True:
            self.disable()

    def disable(self):
        for i in range(18):
            self.widgetDisable(self.holeHandicap[i])
            self.widgetDisable(self.holeYardage[i])
            self.widgetDisable(self.holePar[i])

        self.headingYardage.configure(disabledbackground=teeDetailsBackgroundDisabledColor,
                                       disabledforeground=teeDetailsForegroundDisabledColor)

        self.yardageTotal.configure(disabledbackground=teeDetailsBackgroundDisabledColor,
                                    disabledforeground=teeDetailsForegroundDisabledColor)
        self.frontYardageTotal.configure(disabledbackground=teeDetailsBackgroundDisabledColor,
                                         disabledforeground=teeDetailsForegroundDisabledColor)
        self.backYardageTotal.configure(disabledbackground=teeDetailsBackgroundDisabledColor,
                                         disabledforeground=teeDetailsForegroundDisabledColor)

        self.headingPar.configure(disabledbackground=teeDetailsBackgroundDisabledColor,
                                  disabledforeground=teeDetailsForegroundDisabledColor)
        self.parTotal.configure(disabledbackground=teeDetailsBackgroundDisabledColor,
                                disabledforeground=teeDetailsForegroundDisabledColor)
        self.frontParTotal.configure(disabledbackground=teeDetailsBackgroundDisabledColor,
                                     disabledforeground=teeDetailsForegroundDisabledColor)
        self.backParTotal.configure(disabledbackground=teeDetailsBackgroundDisabledColor,
                                    disabledforeground=teeDetailsForegroundDisabledColor)

        self.handicapHeading.config(disabledbackground=teeDetailsBackgroundDisabledColor,
                                    disabledforeground=teeDetailsForegroundDisabledColor)
        self.handicapIn.config(disabledbackground=teeDetailsBackgroundDisabledColor,
                               disabledforeground=teeDetailsForegroundDisabledColor)
        self.handicapOut.config(disabledbackground=teeDetailsBackgroundDisabledColor)
        self.handicapTotal.config(disabledbackground=teeDetailsBackgroundDisabledColor,
                                 disabledforeground=teeDetailsForegroundDisabledColor)
        self.tableEnabled = False

    def widgetDisable(self,aWidget):
        for i in range(18):
            aWidget.config(state='disabled', relief=teeDetailsRelief, font=fontAverage)
            aWidget.unbind('<Enter>')
            aWidget.unbind('<FocusIn>')
            aWidget.unbind('<Leave>')
            aWidget.unbind('<FocusOut>')

    def widgetEnable(self,aWidget):
        for i in range(18):
            aWidget.config(state='normal')
            aWidget.bind('<Enter>', lambda event=Event: self._highlight(event))
            aWidget.bind('<FocusIn>', lambda event=Event: self._highlight(event))
            aWidget.bind('<Leave>', lambda event=Event: self._removeHighlight(event))
            aWidget.bind('<FocusOut>', lambda event=Event: self._removeHighlight(event))

    def clear(self):
        for i in range(18):
            self.widgetClear(self.holeHandicap[i])
            self.widgetClear(self.holeYardage[i])
            self.widgetClear(self.holePar[i])

        self.widgetClear(self.frontYardageTotal)
        self.widgetClear(self.yardageTotal)
        self.widgetClear(self.backYardageTotal)
        self.widgetClear(self.frontParTotal)
        self.widgetClear(self.parTotal)
        self.widgetClear(self.backParTotal)

    def widgetClear(self, aWidget):
         aState = aWidget.cget('state')
         aWidget.config(state='normal')
         aWidget.delete(0, END)
         if aState != 'normal':
             aWidget.config(state='disabled')

class AppRoundStatsResultTable(Frame):
    def __init__(self, parent, myMsgBar):
        Frame.__init__(self, parent)
        self.parent = parent
        self.tableFont = fontAverage
        self.tableFontB = fontAverageB
        self.row = 0
        self.column = 0
        self.myMsgBar = myMsgBar
        self.columnTotal = len(roundScoringStatsColumnHeading)
        self.columnTotalFielding = len(roundFieldingStatsColumnHeading)
        self.columnOverAll = len(roundOverAllStatsColumnHeading)
        self.config(border=AppDefaultBorderWidth, relief='ridge')
        self.columnconfigure(0, weight=1)

        # roundOverAllStatsColumnHeading = ['Front', 'Back', 'Score', 'Index Before', 'Index After', 'Mileage']
        self.createOverAllFrame()
        self.addEmptyRow()
        self.addRow()
        self.createScoringFrame()
        self.addEmptyRow()
        self.addRow()
        self.createFieldingFrame()
        self.addEmptyRow()
        self.addRow()
        self.createFielding2Frame()


        self.tableEnabled = False
        self.rootWidgetName = self.winfo_name()

    def addEmptyRow(self):
        self.addRow()
        aLabel = Label(self, text="")
        aLabel.grid(row=self.row, column = self.column)

    def defineColumnWeight(self, aFrame, weightList):
        for i in range(len(weightList)):
            aFrame.columnconfigure(i, weight= weightList[i])

    def addColumn(self):
        self.column = self.column + 1

    def addRow(self):
        self.row = self.row + 1
        self.rowconfigure(self.row, weight=2)

    def resetColumn(self):
        self.column = 0

    def createOverAllFrame(self):
        self.OverAllFrame = AppBorderFrame(self, 1)
        # self.OverAllFrame.noBorder()
        self.OverAllFrame.stretchCurrentRow()
        self.OverAllFrame.grid(row=self.row, column=self.column, sticky=E+W+N+S)
        self.defineColumnWeight(self.OverAllFrame, roundOverAllStatsColumnWeight)
        self.createOverAllHeaderRow(self.OverAllFrame)
        self.statsOverAllField = []
        self.createOverAllStatsFieldRow(self.OverAllFrame)

    def createOverAllHeaderRow(self, aFrame):
        self.rowconfigure(self.row, weight = 1)
        for i in range(len(roundOverAllStatsColumnHeading)):
            aLabel = Label(aFrame, font=self.tableFontB, text=roundOverAllStatsColumnHeading[i], relief = roundDetailsRelief, bg=roundDetailsHeadingColor,
                           width = roundDetailsColumnWidth[i])
            aLabel.grid(row=aFrame.row, column=i, sticky=E+W+S+N)

    def createOverAllStatsFieldRow(self, aFrame):
        aFrame.addRow()
        aFrame.rowconfigure(aFrame.row, weight = 1)
        for i in range(len(roundOverAllStatsColumnHeading)):
            num = len(self.statsOverAllField)
            self.statsOverAllField.append(
                Entry(aFrame, relief=roundDetailsRelief, bg=roundDetailsPuttsColor,
                      highlightcolor=roundDetailsPuttsColor, highlightthickness=0, font=self.tableFont,
                      background=roundDetailsPuttsColor, disabledforeground=roundDetailsForegroundDisabledColor,
                      disabledbackground=roundDetailsBackgroundDisabledColor, justify=CENTER,
                      width=roundOverAllStatsColumnWidth[i])
            )
            self.statsOverAllField[num].grid(row=aFrame.row, column=i, sticky=E + W + N + S)


    def createScoringFrame(self):
        self.scoringFrame = AppBorderFrame(self, 1)
        # self.scoringFrame.noBorder()
        self.scoringFrame.stretchCurrentRow()
        self.scoringFrame.grid(row=self.row, column=self.column, sticky=E+W+N+S)
        self.defineColumnWeight(self.scoringFrame, roundScoringStatsColumnWeight)
        self.createScoringHeaderRow(self.scoringFrame)
        self.statsScoringField = []
        self.createScoringStatsFieldRow(self.scoringFrame)

    def createScoringHeaderRow(self, aFrame):
        self.rowconfigure(self.row, weight = 1)
        for i in range(len(roundScoringStatsColumnHeading)):
            aLabel = Label(aFrame, font=self.tableFontB, text=roundScoringStatsColumnHeading[i], relief = roundDetailsRelief, bg=roundDetailsHeadingColor,
                           width = roundDetailsColumnWidth[i])
            aLabel.grid(row=aFrame.row, column=i, sticky=E+W+S+N)

    def createScoringStatsFieldRow(self, aFrame):
        aFrame.addRow()
        aFrame.rowconfigure(aFrame.row, weight = 1)
        for i in range(len(roundScoringStatsColumnHeading)):
            num = len(self.statsScoringField)
            self.statsScoringField.append(
                Entry(aFrame, relief=roundDetailsRelief, bg=roundDetailsPuttsColor, foreground='white',
                      highlightcolor=roundDetailsPuttsColor, highlightthickness=0, font=self.tableFont,
                      background=pieColorsScoring[i], disabledforeground=roundDetailsForegroundDisabledColor,
                      disabledbackground=pieColorsScoring[i], justify=CENTER,
                      width=roundScoringStatsColumnWidth[i])
            )
            self.statsScoringField[num].grid(row=aFrame.row, column=i, sticky=E + W + N + S)


    def createFieldingFrame(self):
        self.fieldingFrame = AppBorderFrame(self, 1)
        # self.fieldingFrame.noBorder()
        self.fieldingFrame.stretchCurrentRow()
        self.fieldingFrame.grid(row=self.row, column=self.column, sticky=E+W+N+S)
        self.defineColumnWeight(self.fieldingFrame, roundFieldingStatsColumnWeight)
        self.createFieldingHeaderRow(self.fieldingFrame)
        self.statsFieldingField = []
        self.createFieldingStatsFieldRow(self.fieldingFrame)

    def createFieldingHeaderRow(self, aFrame):
        self.rowconfigure(self.row, weight = 1)
        for i in range(len(roundFieldingStatsColumnHeading)):
            aLabel = Label(aFrame, font=self.tableFontB, text=roundFieldingStatsColumnHeading[i], relief = roundDetailsRelief, bg=roundDetailsHeadingColor,
                           width = roundDetailsColumnWidth[i])
            aLabel.grid(row=aFrame.row, column=i, sticky=E+W+S+N)

    def createFieldingStatsFieldRow(self, aFrame):
        aFrame.addRow()
        aFrame.rowconfigure(aFrame.row, weight = 1)
        for i in range(len(roundFieldingStatsColumnHeading)):
            num = len(self.statsFieldingField)
            self.statsFieldingField.append(
                Entry(aFrame, relief=roundDetailsRelief, bg=roundDetailsPuttsColor,
                      highlightcolor=roundDetailsPuttsColor, highlightthickness=0, font=self.tableFont,
                      background=roundDetailsPuttsColor, disabledforeground=roundDetailsForegroundDisabledColor,
                      disabledbackground=roundDetailsBackgroundDisabledColor, justify=CENTER,
                      width=roundFieldingStatsColumnWidth[i])
            )
            self.statsFieldingField[num].grid(row=aFrame.row, column=i, sticky=E + W + N + S)

    def createFielding2Frame(self):
        self.Fielding2Frame = AppBorderFrame(self, 1)
        # self.Fielding2Frame.noBorder()
        self.Fielding2Frame.stretchCurrentRow()
        self.Fielding2Frame.grid(row=self.row, column=self.column, sticky=E+W+N+S)
        self.defineColumnWeight(self.Fielding2Frame, roundFielding2StatsColumnWeight)
        self.createFielding2HeaderRow(self.Fielding2Frame)
        self.statsFielding2Field = []
        self.createFielding2StatsFieldRow(self.Fielding2Frame)

    def createFielding2HeaderRow(self, aFrame):
        self.rowconfigure(self.row, weight = 1)
        for i in range(len(roundFielding2StatsColumnHeading)):
            aLabel = Label(aFrame, font=self.tableFontB, text=roundFielding2StatsColumnHeading[i], relief = roundDetailsRelief, bg=roundDetailsHeadingColor,
                           width = roundDetailsColumnWidth[i])
            aLabel.grid(row=aFrame.row, column=i, sticky=E+W+S+N)

    def createFielding2StatsFieldRow(self, aFrame):
        aFrame.addRow()
        aFrame.rowconfigure(aFrame.row, weight = 1)
        for i in range(len(roundFielding2StatsColumnHeading)):
            num = len(self.statsFielding2Field)
            self.statsFielding2Field.append(
                Entry(aFrame, relief=roundDetailsRelief, bg=roundDetailsPuttsColor,
                      highlightcolor=roundDetailsPuttsColor, highlightthickness=0, font=self.tableFont,
                      background=roundDetailsPuttsColor, disabledforeground=roundDetailsForegroundDisabledColor,
                      disabledbackground=roundDetailsBackgroundDisabledColor, justify=CENTER,
                      width=roundFielding2StatsColumnWidth[i])
            )
            self.statsFielding2Field[num].grid(row=aFrame.row, column=i, sticky=E + W + N + S)

    def widgetLoad(self, aWidget, Data):
        aState = aWidget.cget('state')
        aWidget.config(state='normal')
        aWidget.delete(0, END)
        aWidget.insert(0, Data)
        if aState != 'normal':
            aWidget.config(state='disabled')

    def populateStats(self, statsData, holesPlayed, curHdcp, nextHdcp, totalPossibleFairways):
        # gameStats.append(self.frontScoreTotal)        # Indice 0
        # gameStats.append(self.backScoreTotal)         # Indice 1
        # gameStats.append(self.scoreTotal)             # Indice 2
        # gameStats.append(scoringStats[0])  # D Eagle    Indice 3
        # gameStats.append(scoringStats[1])  # Eagle      Indice 4
        # gameStats.append(scoringStats[2])  # Birdie     Indice 5
        # gameStats.append(scoringStats[3])  # Par        Indice 6
        # gameStats.append(scoringStats[4])  # Bogie      Indice 7
        # gameStats.append(scoringStats[5])  # D Bogie    Indice 8
        # gameStats.append(scoringStats[6])  # T Bogie    Indice 9
        # gameStats.append(scoringStats[7])  # Others     Indice 10
        # gameStats.append(self.GreensTotal)            # Indice 11
        # gameStats.append(self.FairwaysTotal)          # Indice 12
        # gameStats.append(drivingStats[0])  # left Miss  Indice 13
        # gameStats.append(drivingStats[1])  # Right Miss Indice 14
        # gameStats.append(drivingStats[2])  # Drv Yards  Indice 15
        # gameStats.append(self.sandTrapTotal)          # Indice 16
        # gameStats.append(self.SandSavesTotal)         # Indice 17
        # gameStats.append(self.puttsTotal)             # Indice 18
        # gameStats.append(self.putts3AndUp)            # Indice 19
        # gameStats.append(self.yardageTotal)           # Indice 20
        for i in range(self.columnTotal):
            self.widgetLoad(self.statsScoringField[i], statsData[i+3])

        # ['Greens', 'Putts', '3 Putts +', 'Sand Traps', 'Sand Saves', 'Sand Save %']
        self.widgetLoad(self.statsFieldingField[0], statsData[11])
        if holesPlayed == 'All':
            totalHoles = 18
        else:
            totalHoles = 9

        aPercentValue = (float(statsData[11])/float(totalHoles)) * float(100)
        aDisplayValue = "{0} %".format(round(aPercentValue, 1))
        self.widgetLoad(self.statsFieldingField[1], aDisplayValue)
        self.widgetLoad(self.statsFieldingField[2], statsData[18])
        self.widgetLoad(self.statsFieldingField[3], statsData[19])
        self.widgetLoad(self.statsFieldingField[4], statsData[16])
        self.widgetLoad(self.statsFieldingField[5], statsData[17])

        if int(statsData[16]) == 0:
            aDisplayValue = "NA"
        else:
            aPercentValue = (float(statsData[17])/float(statsData[16])) * float(100)
            aDisplayValue = "{0} %".format(round(aPercentValue, 1))
        self.widgetLoad(self.statsFieldingField[6], aDisplayValue)

        # ['Front', 'Back', 'Score', 'Index Before', 'Index After', 'Mileage']
        self.widgetLoad(self.statsOverAllField[0], statsData[0])
        self.widgetLoad(self.statsOverAllField[1], statsData[1])
        self.widgetLoad(self.statsOverAllField[2], statsData[2])
        self.widgetLoad(self.statsOverAllField[3], curHdcp)
        self.widgetLoad(self.statsOverAllField[4], nextHdcp)
        kms = convertYardsToKms(statsData[20])
        self.widgetLoad(self.statsOverAllField[5], kms)


        # ['Fairways', 'Left Miss', 'Right Miss', 'Fairway %', 'Average Drives']
        self.widgetLoad(self.statsFielding2Field[0], statsData[12])
        self.widgetLoad(self.statsFielding2Field[1], statsData[13])
        self.widgetLoad(self.statsFielding2Field[2], statsData[14])
        totalPossibleFairways
        aPercentValue = (float(statsData[12]) / float(totalPossibleFairways)) * float(100)
        aDisplayValue = "{0} %".format(round(aPercentValue, 1))
        self.widgetLoad(self.statsFielding2Field[3], aDisplayValue)
        anAverageValue = (float(statsData[15]) / float(statsData[12]))
        aDisplayValue = "{0}".format(round(anAverageValue, 2))
        self.widgetLoad(self.statsFielding2Field[4], aDisplayValue)

class AppRoundDetailsTable(Frame):
    def __init__(self, parent, myMsgBar):
        Frame.__init__(self, parent)
        self.parent = parent
        self.tableFont = fontAverage
        self.tableFontB = fontAverageB
        self.row = 0
        self.column = 0
        self.myMsgBar = myMsgBar
        self.columnTotal = len(roundDetailsColumnHeading)
        self.config(border=AppDefaultBorderWidth, relief='ridge')
        self.defineColumnWeight()
        self.holePar = []
        self.holeScore = []
        self.holePutts = []
        self.holeDrive = []
        self.holeSandTrap = []
        self.holeHandicap = []
        self.holeYardage = []
        self.holeGreens = []
        self.holeFairways = []
        self.holeSandSaves = []
        self.holeWater = []
        self.createHeaderRow()
        self.createHandicapRow()
        self.createYardageRow()
        self.createParRow()
        self.createScoreRow()
        self.createPuttsRow()
        self.createGreensRow()
        self.createDriveRow()
        self.createFairwaysRow()
        self.createSandTrapRow()
        self.createSandSavesRow()
        self.createWaterRow()
        self.tableEnabled = False
        self.rootWidgetName = self.winfo_name()

    def getTotalPossibleDrives(self):
        total = 0
        for i in range(self.startHole, self.endHole):
            if int(self.holePar[i].get()) != 3:
                total = total + 1
        return total

    def defineColumnWeight(self):
        for i in range(self.columnTotal):
            self.columnconfigure(i, weight= roundDetailsColumnWeight[i])

    def addColumn(self):
        self.column = self.column + 1

    def addRow(self):
        self.row = self.row + 1
        self.rowconfigure(self.row, weight=2)

    def resetColumn(self):
        self.column = 0

    def createHeaderRow(self):
        self.rowconfigure(self.row, weight = 1)
        for i in range(self.columnTotal):
            aLabel = Label(self, font=self.tableFontB, text=roundDetailsColumnHeading[i], relief = roundDetailsRelief, bg=roundDetailsHeadingColor,
                           width = roundDetailsColumnWidth[i])
            aLabel.grid(row=self.row, column=i, sticky=E+W+S+N)

    def _updateTotals(self):
        self._updt_score_total()
        self._updt_putt_total()
        self._updt_greens_total()
        self._updt_fairway_total()
        self._updt_driveDistance_total()
        self._updt_sands_and_trap_total()

    def _updt_driveDistance_total(self):
        frontDriveLenghtTotal = 0
        backDriveLenghtTotal = 0
        totalFrontDrives = 0
        totalBackDrives = 0
        for i in range(self.startHole, self.endHole):
            if i < 9:
                rawData = self.holeDrive[i].get()
                try:
                    lenght = int(rawData)
                    totalFrontDrives = totalFrontDrives + 1
                    frontDriveLenghtTotal = frontDriveLenghtTotal + lenght
                except:
                    pass

            if i > 8:
                rawData = self.holeDrive[i].get()
                try:
                    lenght = int(rawData)
                    totalBackDrives = totalBackDrives + 1
                    backDriveLenghtTotal = backDriveLenghtTotal + lenght
                except:
                    pass

            if totalFrontDrives == 0:
                aValue = float(0.0)
            else:
                aValue = "{0}".format(round(float(frontDriveLenghtTotal)/float(totalFrontDrives), 1))
            self.frontDriveTotal.config(state='normal')
            self.frontDriveTotal.delete(0, END)
            self.frontDriveTotal.insert(0, aValue )
            self.frontDriveTotal.config(state='disabled')

            if totalBackDrives == 0:
                aValue = float(0.0)
            else:
                aValue = "{0}".format(round(float(backDriveLenghtTotal)/float(totalBackDrives), 1))
            self.backDriveTotal.config(state='normal')
            self.backDriveTotal.delete(0, END)
            self.backDriveTotal.insert(0,  aValue)
            self.backDriveTotal.config(state='disabled')

            if (totalBackDrives + totalFrontDrives) == 0:
                aValue = float(0.0)
            else:
                totalDrives = totalBackDrives + totalFrontDrives
                totalDriveLength = backDriveLenghtTotal + frontDriveLenghtTotal
                aValue = "{0}".format(round(float(totalDriveLength)/float(totalDrives), 1))
            self.driveTotal.config(state='normal')
            self.driveTotal.delete(0, END)
            self.driveTotal.insert(0, aValue)
            self.driveTotal.config(state='disabled')

    def _updt_sands_and_trap_total(self):
        frontSandSavesTotal = 0
        backSandSavesTotal = 0

        frontSandTrapTotal = 0
        backSandTrapTotal = 0
        for i in range(self.startHole, self.endHole):
            if i < 9:
                new = self.holeSandSaves[i].get()
                if new == 'Y':
                    frontSandSavesTotal = frontSandSavesTotal + 1
                new = self.holeSandTrap[i].get()
                if new == 'Y':
                    frontSandTrapTotal = frontSandTrapTotal + 1

            if i > 8:
                new = self.holeSandSaves[i].get()
                if new == 'Y':
                    backSandSavesTotal = backSandSavesTotal + 1
                new = self.holeSandTrap[i].get()
                if new == 'Y':
                    backSandTrapTotal = backSandTrapTotal + 1

            self.frontSandSavesTotal.config(state='normal')
            self.frontSandSavesTotal.delete(0, END)
            self.frontSandSavesTotal.insert(0, frontSandSavesTotal)
            self.frontSandSavesTotal.config(state='disabled')

            self.backSandSavesTotal.config(state='normal')
            self.backSandSavesTotal.delete(0, END)
            self.backSandSavesTotal.insert(0, backSandSavesTotal)
            self.backSandSavesTotal.config(state='disabled')

            totalSandSaves = frontSandSavesTotal + backSandSavesTotal
            self.SandSavesTotal.config(state='normal')
            self.SandSavesTotal.delete(0, END)
            self.SandSavesTotal.insert(0, totalSandSaves)
            self.SandSavesTotal.config(state='disabled')


            self.frontSandTrapTotal.config(state='normal')
            self.frontSandTrapTotal.delete(0, END)
            self.frontSandTrapTotal.insert(0, frontSandTrapTotal)
            self.frontSandTrapTotal.config(state='disabled')

            self.backSandTrapTotal.config(state='normal')
            self.backSandTrapTotal.delete(0, END)
            self.backSandTrapTotal.insert(0, backSandTrapTotal)
            self.backSandTrapTotal.config(state='disabled')

            totalSandTrap = frontSandTrapTotal + backSandTrapTotal
            self.sandTrapTotal.config(state='normal')
            self.sandTrapTotal.delete(0, END)
            self.sandTrapTotal.insert(0, totalSandTrap)
            self.sandTrapTotal.config(state='disabled')

            if totalSandTrap == 0:
                percentSandSave = ""
            else:
                aPercentValue = (float(totalSandSaves)/float(totalSandTrap)) * float(100)
                percentSandSave = "{0} %".format(round(aPercentValue,1))

            self.SandSavesNetTotal.config(state='normal')
            self.SandSavesNetTotal.delete(0, END)
            self.SandSavesNetTotal.insert(0, percentSandSave)
            self.SandSavesNetTotal.config(state='disabled')

    def _updt_fairway_total(self):
        frontFairwaysTotal = 0
        backFairwaysTotal = 0
        totalFairways = 0
        for i in range(self.startHole, self.endHole):
            if self.holePar[i].get() != "3":
                totalFairways = totalFairways + 1

            if i < 9:
                new = self.holeFairways[i].get()
                if new == 'Y':
                    frontFairwaysTotal = frontFairwaysTotal + 1

            if i > 8:
                new = self.holeFairways[i].get()
                if new == 'Y':
                    backFairwaysTotal = backFairwaysTotal + 1

            self.frontFairwaysTotal.config(state='normal')
            self.frontFairwaysTotal.delete(0, END)
            self.frontFairwaysTotal.insert(0, frontFairwaysTotal)
            self.frontFairwaysTotal.config(state='disabled')

            self.backFairwaysTotal.config(state='normal')
            self.backFairwaysTotal.delete(0, END)
            self.backFairwaysTotal.insert(0, backFairwaysTotal)
            self.backFairwaysTotal.config(state='disabled')

            self.FairwaysTotal.config(state='normal')
            self.FairwaysTotal.delete(0, END)
            self.FairwaysTotal.insert(0, frontFairwaysTotal + backFairwaysTotal)
            self.FairwaysTotal.config(state='disabled')


            aPercentValue = (float(frontFairwaysTotal + backFairwaysTotal)/float(totalFairways)) * float(100)
            percentfairwaySave = "{0} %".format(round(aPercentValue,1))

            self.FairwaysNetTotal.config(state='normal')
            self.FairwaysNetTotal.delete(0, END)
            self.FairwaysNetTotal.insert(0, percentfairwaySave)
            self.FairwaysNetTotal.config(state='disabled')

    #
    #  Field holeDrives contains a distance 3 Char, a R or L
    #  Fairways are deduced from there
    #  Putts deduces the greens and
    #  Sand saves are deduced from sandtraps
    #
    # cursor.execute('''CREATE TABLE GameDetails(
    #    uniqueID       INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
    #    holeNumber     SMALLINT    NOT NULL,
    #    holeScore      SMALLINT    NOT NULL,
    #    holePutts      SMALLINT,
    #    holeDrives     CHAR(4),
    #    holeSandTraps  CHAR(1),
    #    gameID         INTEGER     NOT NULL,
    #    deleted        CHAR(1)     NOT NULL,
    #    lastModified   INTEGER,
    #    updaterID      INTEGER,
    #    FOREIGN KEY(gameID) REFERENCES Games(uniqueID)
    #    );''')

    def getHoleDetails(self, holeNum):
        holeDetails = []

        holeDetails.append(self.holeScore[holeNum].get())
        holeDetails.append(self.holePutts[holeNum].get())
        holeDetails.append(self.holeDrive[holeNum].get())
        holeDetails.append(self.holeSandTrap[holeNum].get())

        return holeDetails

    def _updt_greens_total(self):
        frontGreensTotal = 0
        backGreensTotal = 0
        totalGreensPlayed = 0
        for i in range(self.startHole, self.endHole):
            totalGreensPlayed = totalGreensPlayed + 1
            if i < 9:
                new = self.holeGreens[i].get()
                if new == 'Y':
                    frontGreensTotal = frontGreensTotal + 1

            if i > 8:
                new = self.holeGreens[i].get()
                if new == 'Y':
                    backGreensTotal = backGreensTotal + 1

            self.frontGreensTotal.config(state='normal')
            self.frontGreensTotal.delete(0, END)
            self.frontGreensTotal.insert(0, frontGreensTotal)
            self.frontGreensTotal.config(state='disabled')

            self.backGreensTotal.config(state='normal')
            self.backGreensTotal.delete(0, END)
            self.backGreensTotal.insert(0, backGreensTotal)
            self.backGreensTotal.config(state='disabled')

            self.GreensTotal.config(state='normal')
            self.GreensTotal.delete(0, END)
            self.GreensTotal.insert(0, frontGreensTotal + backGreensTotal)
            self.GreensTotal.config(state='disabled')

            aPercentValue = (float(frontGreensTotal + backGreensTotal)/float(totalGreensPlayed)) * float(100)
            percentGreensSave = "{0} %".format(round(aPercentValue,1))

            self.GreensNetTotal.config(state='normal')
            self.GreensNetTotal.delete(0, END)
            self.GreensNetTotal.insert(0, percentGreensSave)
            self.GreensNetTotal.config(state='disabled')

            self.driveYardsTotal = frontGreensTotal + backGreensTotal

    def _updt_putt_total(self):
        frontPuttTotal = 0
        backPuttTotal = 0
        threePuttandUpTotal = 0
        for i in range(self.startHole, self.endHole):
            new = self.holePutts[i].get()

            if len(new) > 0:
                if int(new) >= 3:
                    threePuttandUpTotal = threePuttandUpTotal + 1

            if i < 9:
                if len(new) > 0:
                    frontPuttTotal = frontPuttTotal + int(new)

            if i > 8:
                if len(new) > 0:
                    backPuttTotal = backPuttTotal + int(new)

            self.frontPuttsTotal.config(state='normal')
            self.frontPuttsTotal.delete(0, END)
            self.frontPuttsTotal.insert(0, frontPuttTotal)
            self.frontPuttsTotal.config(state='disabled')

            self.backPuttsTotal.config(state='normal')
            self.backPuttsTotal.delete(0, END)
            self.backPuttsTotal.insert(0, backPuttTotal)
            self.backPuttsTotal.config(state='disabled')

            self.puttsTotal.config(state='normal')
            self.puttsTotal.delete(0, END)
            self.puttsTotal.insert(0, frontPuttTotal + backPuttTotal)
            self.puttsTotal.config(state='disabled')

        self.putts3AndUp = threePuttandUpTotal

    def _SandTrap_check(self, event, holeNum):
        aMsg = '''INVALID ENTRY: Valid entries are n|N=N , y|Y=Y. <Return> key or <Left-Click> toggles value.'''
        currentWidget = event.widget
        currentContent = currentWidget.get()
        if event.char:
            try:
 #               if event.char != "\b":
                    if event.char == "\t":
                        pass
                    elif (event.char == 'n' or event.char == 'N'):
                        currentWidget.delete(0, END)
                        event.widget.insert(0, 'N')
                        self._focusNext(event.widget)
                        return "break"

                    elif (event.char == 'y' or event.char == 'Y'):
                        currentWidget.delete(0, END)
                        event.widget.insert(0, 'Y')
                        self._focusNext(event.widget)
                        return "break"
                    else:
                        raise Exception
            except:
                self.myMsgBar.newMessage('error', aMsg)
                return "break"

    def _drive_check(self, event, aHoleNum):
        aMsg = '''INVALID ENTRY: Valid entries are r|R|w|W=R, e|E|q|Q|l|L=L, or an integer 1-999.'''
        currentWidget = event.widget
        if currentWidget.select_present() == True:
            if event.char.isdigit() == True:
                currentWidget.delete(0, END)
        currentContent = currentWidget.get()
        if event.char:
            try:
                if event.char != "\b" and event.char != '\x7f':
                    if event.char == "\t":
                        pass
                    elif (event.char == 'r' or event.char == 'R' or event.char == 'w' or event.char == 'W'):
                        if currentWidget.select_present() == True:
                            currentWidget.delete(0, END)
                            currentContent = currentWidget.get()
                        if len(currentContent) == 0:
                            event.widget.insert(0, 'R')
                            self._focusNext(event.widget)
                            return "break"
                        else:
                            raise Exception
                    elif (event.char == 'l' or event.char == 'L' or event.char == 'e' or event.char == 'E' or event.char == 'q' or event.char == 'Q'):
                        #
                        #  Detects the letter and forces a Captial L. reason for the break
                        #
                        if currentWidget.select_present() == True:
                            currentWidget.delete(0, END)
                            currentContent = currentWidget.get()
                        if len(currentContent) == 0:
                            event.widget.insert(0, 'L')
                            self._focusNext(event.widget)
                            return "break"
                        else:
                            raise Exception
                    elif event.char.isdigit() == False:
                        raise Exception
                    else:
                        try:
                            if len(currentContent) > 0:
                                a = int(currentContent)
                            if len(currentContent) == 2:
                                self._focusNext(event.widget)
                            if len(currentContent) > 2:
                                raise Exception
                        except:
                            raise Exception
            except:
                self.myMsgBar.newMessage('error', aMsg)
                return "break"

    def _checkSandTrapResultNoEvent(self, holeNum, NoTotals=False):
        thePar = self.holePar[holeNum].get()
        theScore = self.holeScore[holeNum].get()
        theSandTrap = self.holeSandTrap[holeNum].get()

        if len(thePar) > 0 and len(theScore) > 0:
            if theScore <= thePar and theSandTrap == 'Y':
                self.holeSandSaves[holeNum].set()
            else:
                self.holeSandSaves[holeNum].unSet()
        if NoTotals == False:
            self._updateTotals()

    def _checkSandTrapResult(self, event, holeNum):
        self._removeHighlight(event)
        thePar = self.holePar[holeNum].get()
        theScore = self.holeScore[holeNum].get()
        theSandTrap = self.holeSandTrap[holeNum].get()

        if len(thePar) > 0 and len(theScore) > 0:
            if theScore <= thePar and theSandTrap == 'Y':
                self.holeSandSaves[holeNum].set()
            else:
                self.holeSandSaves[holeNum].unSet()
        self._updateTotals()

    def _checkDriveResultNoEVENT(self, aHoleNum, NoTotals=False):
        try:
            if len(self.holeDrive[aHoleNum].get()) > 0:
                value = int(self.holeDrive[aHoleNum].get())
                self.holeFairways[aHoleNum].set()
        except:
            self.holeFairways[aHoleNum].unSet()
        if NoTotals == False:
            self._updateTotals()

    def _checkDriveResult(self, event, aHoleNum):
        self._removeHighlight(event)
        try:
            if len(event.widget.get()) > 0:
                value = int(event.widget.get())
                self.holeFairways[aHoleNum].set()
        except:
            self.holeFairways[aHoleNum].unSet()
        self._updateTotals()

    def _checkPuttResultNoEvent(self, aHoleNum, NoTotals=False):
        if len(self.holePar[aHoleNum].get()) > 0:
            currentContent = self.holePutts[aHoleNum].get()
            scoreContent = self.holeScore[aHoleNum].get()
            buffer = (int(self.holePar[aHoleNum].get()) - 2)
            if len(currentContent) != 0:
                if (int(scoreContent) - int(currentContent)) <= buffer:
                    self.holeGreens[aHoleNum].set()
                else:
                    self.holeGreens[aHoleNum].unSet()
        if NoTotals == False:
            self._updateTotals()

    def _checkPuttResult(self, event, aHoleNum):
        self._removeHighlight(event)
        if len(self.holePar[aHoleNum].get()) > 0:
            currentContent = event.widget.get()
            scoreContent = self.holeScore[aHoleNum].get()
            buffer = (int(self.holePar[aHoleNum].get()) - 2)
            if len(currentContent) != 0:
                if (int(scoreContent) - int(currentContent)) <= buffer:
                    self.holeGreens[aHoleNum].set()
                else:
                    self.holeGreens[aHoleNum].unSet()
        self._checkSandTrapResultNoEvent(aHoleNum)
        self._updateTotals()

    def _putt_check(self, event, aHoleNum):
        #
        #  Only three digits allowed (integer).  Non character events are ignored.  Many of them are
        #  navigation event such as end, home, pageup etc.
        #
        aMsg = '''ERROR: Integer number required, hopefully less than 10.'''
        currentWidget = event.widget
        if currentWidget.select_present() == True:
            if event.char.isdigit() == True:
                currentWidget.delete(0, END)
        currentContent = currentWidget.get()
        if event.char:
            try:
                #                if event.char == "\t":
                #                   self._focusNext(event.widget)
                if len(self.holeScore[aHoleNum].get()) > 0:
                    if event.char != "\b" :
                        if event.char == "\t":
                            if len(currentContent) == 0:
                                aMsg = '''ERROR: required field, please enter a score between 1-10.'''
                                raise Exception
                            else:
                                aScore = int(currentContent)
                                if len(currentContent) > 0:
                                    if aScore > 9:
                                        raise Exception
                                self._focusNext(event.widget)
                        elif event.char.isdigit() == False:
                            raise Exception
                        elif len(currentContent) > 0:
                            aMsg = '''ERROR: Integer number required, must be less than 10.'''
                            raise Exception
                        else:
                            aScore = int(event.char)
                            self._focusNext(event.widget)
                else:
                    aMsg = '''ERROR: A score for hole number {0} is required before entering the number of putts.'''.format(
                        int(aHoleNum) + 1)
                    raise Exception

            except:
                self.myMsgBar.newMessage('error', aMsg)
                return "break"

    def _holeScore_check(self, event):
        #
        #  Only three digits allowed (integer).  Non character events are ignored.  Many of them are
        #  navigation event such as end, home, pageup etc.
        #
        aMsg = '''ERROR: Integer number required, hopefully less than 19.'''
        currentWidget = event.widget
        if currentWidget.select_present() == True:
            if event.char.isdigit() == True:
                currentWidget.delete(0, END)
        currentContent = currentWidget.get()
        if event.char:
            try:
                #                if event.char == "\t":
                #                   self._focusNext(event.widget)
                if event.char != "\b" and event.char != '\x7f':
                    if event.char == "\t":
                        if len(currentContent) == 0:
                            aMsg = '''ERROR: required field, please enter a score between 1-19.'''
                            raise Exception
                        else:
                            aScore = int(currentContent)
                            if len(currentContent) > 0:
                                if aScore > 19:
                                    raise Exception
                            self._focusNext(event.widget)
                    elif event.char.isdigit() == False:
                        raise Exception
                    elif len(currentContent) > 0:
                        aScore = int("{0}{1}".format(currentContent, event.char))
                        if aScore > 19:
                            aMsg = '''ERROR: Integer number required, hopefully less than 19.'''
                            raise Exception
                        else:
                            self._focusNext(event.widget)
                    elif (int(event.char) == 0):
                        aMsg = '''ERROR: A score of 0 is not valid, please enter a score between 1-19. '''
                        raise Exception
                    elif (int(event.char) != 1):
                        aScore = int(event.char)
                        self._focusNext(event.widget)
            except:
                self.myMsgBar.newMessage('error', aMsg)
                return "break"

    def _focusNext(self, awidget):
        '''Return the next widget in tab order'''
        awidget = self.tk.call('tk_focusNext', awidget._w)
        if not awidget: return None
        #        return self.nametowidget(widget.string)
        nextWidget = self.nametowidget(awidget.string)
        nextWidget.focus()
        return "break"

    def _focusPrevious(self, awidget):
        '''Return the next widget in tab order'''
        awidget = self.tk.call('tk_focusPrev', awidget._w)
        if not awidget: return None
        #        return self.nametowidget(widget.string)
        nextWidget = self.nametowidget(awidget.string)
        nextWidget.focus()
        return "break"

    def _checkHoleResultNoEvent(self, aWidget, aHoleNum, mode=None):
        currentContent = aWidget.get()
        aMsg = " "
        if len(currentContent) != 0:
            if int(currentContent) == int(self.holePar[aHoleNum].get()):
                if mode =='view':
                    aWidget.config(disabledbackground=par_color,
                                   disabledforeground='white')
                else:
                    aWidget.config(bg=par_color, fg='white')
                    aMsg = '''A PAR, always a good score. Well done mate.'''
                try:
                    self.myMsgBar.newMessage('info', aMsg)
                except:
                    pass
            if int(currentContent) == int(self.holePar[aHoleNum].get()) - 1:
                if mode == 'view':
                    aWidget.config(disabledbackground=birdie_color,
                                   disabledforeground='white')
                else:
                    aWidget.config(bg=birdie_color, fg='white')
                    aMsg = '''Youhoo, a birdie.  Keep up the good work.'''
                try:
                    self.myMsgBar.newMessage('info', aMsg)
                except:
                    pass
            if int(currentContent) == int(self.holePar[aHoleNum].get()) - 2:
                if mode == 'view':
                    aWidget.config(disabledbackground=eagle_color,
                                   disabledforeground='white')
                else:
                    aWidget.config(bg=eagle_color, fg='white')
                    if int(self.holePar[aHoleNum].get()) == 3:
                        aMsg = '''Unbelievable, eagle has landed. Even better, IT IS A HOLE IN ONE!!!!!'''
                    else:
                        aMsg = '''Unbelievable, eagle has landed.'''
                try:
                    self.myMsgBar.newMessage('info', aMsg)
                except:
                    pass
            if int(currentContent) == int(self.holePar[aHoleNum].get()) - 3:
                if mode == 'view':
                    aWidget.config(disabledbackground=deagle_color,
                                   disabledforeground='white')
                else:
                    aWidget.config(bg=deagle_color, fg='white')
                    aMsg = '''WOOOOW, I am impress.  I never had a double eagle myself.'''
                try:
                    self.myMsgBar.newMessage('info', aMsg)
                except:
                    pass
            if int(currentContent) == int(self.holePar[aHoleNum].get()) + 1:
                if mode == 'view':
                    aWidget.config(disabledbackground=bogie_color,
                                   disabledforeground='white')
                else:
                    aWidget.config(bg=bogie_color, fg='white')
                    aMsg = '''Oops, be careful.'''
                try:
                    self.myMsgBar.newMessage('info', aMsg)
                except:
                    pass
            if int(currentContent) == int(self.holePar[aHoleNum].get()) + 2:
                if mode == 'view':
                    aWidget.config(disabledbackground=dbogie_color,
                                   disabledforeground='white')
                else:
                    aWidget.config(bg=dbogie_color, fg='white')
                    aMsg = '''Don't let it slip too far...'''
                try:
                    self.myMsgBar.newMessage('info', aMsg)
                except:
                    pass
            if int(currentContent) == int(self.holePar[aHoleNum].get()) + 3:
                if mode == 'view':
                    aWidget.config(disabledbackground=tbogie_color,
                                   disabledforeground='white')
                else:
                    aWidget.config(bg=tbogie_color, fg='white')
                    aMsg = '''HEY HEY, get a hold of it man.'''
                try:
                    self.myMsgBar.newMessage('info', aMsg)
                except:
                    pass
            if int(currentContent) > int(self.holePar[aHoleNum].get()) + 3:
                if mode == 'view':
                    aWidget.config(disabledbackground=others_color,
                                   disabledforeground='white')
                else:
                    aWidget.config(bg=others_color, fg='white')
                    aMsg = '''OMG, you are losing it big times...'''
                try:
                    self.myMsgBar.newMessage('info', aMsg)
                except:
                    pass
            aWidget.select_range(0, 0)
        self._updateTotals()

    def _checkHoleResult(self, event, aHoleNum):
        self._removeHighlight(event)
        currentContent = event.widget.get()
        if len(currentContent) != 0:
            if int(currentContent) == int(self.holePar[aHoleNum].get()):
                event.widget.config(bg=par_color, fg='white')
                aMsg = '''A PAR, always a good score. Well done mate.'''
                self.myMsgBar.newMessage('info', aMsg)
            if int(currentContent) == int(self.holePar[aHoleNum].get()) - 1:
                event.widget.config(bg=birdie_color, fg='white')
                aMsg = '''Youhoo, a birdie.  Keep up the good work.'''
                self.myMsgBar.newMessage('info', aMsg)
            if int(currentContent) == int(self.holePar[aHoleNum].get()) - 2:
                event.widget.config(bg=eagle_color, fg='white')
                if int(self.holePar[aHoleNum].get()) == 3:
                    aMsg = '''Unbelievable, eagle has landed. Even better, IT IS A HOLE IN ONE!!!!!'''
                else:
                    aMsg = '''Unbelievable, eagle has landed.'''
                self.myMsgBar.newMessage('info', aMsg)
            if int(currentContent) == int(self.holePar[aHoleNum].get()) - 3:
                event.widget.config(bg=deagle_color, fg='white')
                aMsg = '''WOOOOW, I am impress.  I never had a double eagle myself.'''
                self.myMsgBar.newMessage('info', aMsg)
            if int(currentContent) == int(self.holePar[aHoleNum].get()) + 1:
                event.widget.config(bg=bogie_color, fg='white')
                aMsg = '''Oops, be careful.'''
                self.myMsgBar.newMessage('info', aMsg)
            if int(currentContent) == int(self.holePar[aHoleNum].get()) + 2:
                event.widget.config(bg=dbogie_color, fg='white')
                aMsg = '''Don't let it slip too far...'''
                self.myMsgBar.newMessage('info', aMsg)
            if int(currentContent) == int(self.holePar[aHoleNum].get()) + 3:
                event.widget.config(bg=tbogie_color, fg='white')
                aMsg = '''HEY HEY, get a hold of it man.'''
                self.myMsgBar.newMessage('info', aMsg)
            if int(currentContent) > int(self.holePar[aHoleNum].get()) + 3:
                event.widget.config(bg=others_color, fg='white')
                aMsg = '''OMG, you are losing it big times...'''
                self.myMsgBar.newMessage('info', aMsg)
            event.widget.select_range(0, 0)
        self._checkPuttResultNoEvent(aHoleNum)
        self._checkSandTrapResultNoEvent(aHoleNum)

    def getScoringStats(self):
        scoringArray = []
        dEagleCNT = 0
        eagleCNT = 0
        birdieCNT = 0
        parCNT = 0
        bogieCNT = 0
        dBogieCNT = 0
        tBogieCNT = 0
        othersCNT = 0

        for i in range(self.startHole, self.endHole):
            if int(self.holeScore[i].get()) == int(self.holePar[i].get()):
                parCNT = parCNT + 1

            if int(self.holeScore[i].get()) == int(self.holePar[i].get()) - 1:
                birdieCNT = birdieCNT + 1

            if int(self.holeScore[i].get()) == int(self.holePar[i].get()) - 2:
                eagleCNT = eagleCNT + 1

            if int(self.holeScore[i].get()) == int(self.holePar[i].get()) - 3:
                dEagleCNT = dEagleCNT + 1

            if int(self.holeScore[i].get()) == int(self.holePar[i].get()) + 1:
                bogieCNT = bogieCNT + 1

            if int(self.holeScore[i].get()) == int(self.holePar[i].get()) + 2:
                dBogieCNT = dBogieCNT + 1

            if int(self.holeScore[i].get()) == int(self.holePar[i].get()) + 3:
                tBogieCNT = tBogieCNT + 1

            if int(self.holeScore[i].get()) > int(self.holePar[i].get()) + 3:
                othersCNT = othersCNT + 1

        scoringArray.append(dEagleCNT)  # Indice 0
        scoringArray.append(eagleCNT)   # Indice 1
        scoringArray.append(birdieCNT)  # Indice 2
        scoringArray.append(parCNT)     # Indice 3
        scoringArray.append(bogieCNT)   # Indice 4
        scoringArray.append(dBogieCNT)  # Indice 5
        scoringArray.append(tBogieCNT)  # Indice 6
        scoringArray.append(othersCNT)  # Indice 7

        return scoringArray

    def getDrivingStats(self):
        drivingArray = []
        rMissCNT = 0
        lMissCNT = 0
        totalDrvYardsCNT = 0

        for i in range(self.startHole, self.endHole):
            if self.holePar[i].get() != "3":
                data = self.holeDrive[i].get()
                if data == "R":
                    rMissCNT = rMissCNT + 1

                elif data == "L":
                    lMissCNT = lMissCNT + 1

                else:
                    totalDrvYardsCNT = totalDrvYardsCNT + int(data)

        drivingArray.append(lMissCNT)
        drivingArray.append(rMissCNT)
        drivingArray.append(totalDrvYardsCNT)

        return drivingArray

    def getGameStats(self):
        gameStats = []
        scoringStats = self.getScoringStats()
        drivingStats = self.getDrivingStats()

        gameStats.append(int(self.frontScoreTotal.get()))       # Indice 0
        gameStats.append(int(self.backScoreTotal.get()))        # Indice 1
        gameStats.append(int(self.scoreTotal.get()))            # Indice 2
        gameStats.append(scoringStats[0])  # D Eagle   Indice 3
        gameStats.append(scoringStats[1])  # Eagle     Indice 4
        gameStats.append(scoringStats[2])  # Birdie    Indice 5
        gameStats.append(scoringStats[3])  # Par       Indice 6
        gameStats.append(scoringStats[4])  # Bogie     Indice 7
        gameStats.append(scoringStats[5])  # D Bogie   Indice 8
        gameStats.append(scoringStats[6])  # T Bogie   Indice 9
        gameStats.append(scoringStats[7])  # Others    Indice 10
        gameStats.append(int(self.GreensTotal.get()))           # Indice 11
        gameStats.append(int(self.FairwaysTotal.get()))         # Indice 12
        gameStats.append(drivingStats[0]) # left Miss  Indice 13
        gameStats.append(drivingStats[1]) # Right Miss Indice 14
        gameStats.append(drivingStats[2]) # Drv Yards  Indice 15
        gameStats.append(int(self.sandTrapTotal.get()))         # Indice 16
        gameStats.append(int(self.SandSavesTotal.get()))        # Indice 17
        gameStats.append(int(self.puttsTotal.get()))            # Indice 18
        gameStats.append(self.putts3AndUp)           # Indice 19
        gameStats.append(int(self.yardageTotal.get()))          # Indice 20

        return gameStats

    def _defineHoleRange(self, size):
        if size == 'Front':
            self.startHole = 0
            self.endHole = 9
        elif size == 'Back':
            self.startHole = 9
            self.endHole = 18
        else:
            self.startHole = 0
            self.endHole = 18


    def _updt_score_total(self):
        frontScoreTotal = 0
        backScoreTotal = 0
        holes_played = 0
        for i in range(self.startHole, self.endHole):
            if i < 9:
                new = self.holeScore[i].get()
                if len(new) > 0:
                    frontScoreTotal = frontScoreTotal + int(new)
                    holes_played = holes_played + 1

            if i > 8:
                new = self.holeScore[i].get()
                if len(new) > 0:
                    backScoreTotal = backScoreTotal + int(new)
                    holes_played = holes_played + 1

            self.frontScoreTotal.config(state='normal')
            self.frontScoreTotal.delete(0, END)
            self.frontScoreTotal.insert(0, frontScoreTotal)
            self.frontScoreTotal.config(state='disabled')

            self.backScoreTotal.config(state='normal')
            self.backScoreTotal.delete(0, END)
            self.backScoreTotal.insert(0, backScoreTotal)
            self.backScoreTotal.config(state='disabled')

            self.scoreTotal.config(state='normal')
            self.scoreTotal.delete(0, END)
            self.scoreTotal.insert(0, frontScoreTotal + backScoreTotal)
            self.scoreTotal.config(state='disabled')

        if holes_played == 18:
            self._updateNet()

    def _updateNet(self):
        curhdcpGolfer = calculateCurrentHdcp(self.golferID)
        curGolferStrokes = calculategolferStrokes(curhdcpGolfer, self.courseSlope)
        # elif self.op == 'edit':
        #     curhdcpGolfer = calculateBeforeCurrentGameHdcp(convertDateStringToOrdinal(self.date_var1.get()),
        #                                                    self.golferDictionary[self.selectingGolfer.get()],
        #                                                    self.gameID, self.selectingGameNumber.get())
        #     curGolferStrokes = calculategolferStrokes(curhdcpGolfer, int(self.teeComboBoxData[0][4]))

        self.scoreNetTotal.config(state='normal')
        self.scoreNetTotal.delete(0, END)
        self.scoreNetTotal.insert(0, int(self.scoreTotal.get()) - curGolferStrokes)
        self.scoreNetTotal.config(state='disabled')

    def _updt_par_total(self):
        frontTotal = 0
        backTotal = 0
        for i in range(len(self.holePar)):
            if i < 9:
                new = self.holePar[i].get()
                if len(new) > 0:
                    frontTotal = frontTotal + int(new)

            if i > 8:
                new = self.holePar[i].get()
                if len(new) > 0:
                    backTotal = backTotal + int(new)

            self.frontParTotal.config(state='normal')
            self.frontParTotal.delete(0, END)
            self.frontParTotal.insert(0, frontTotal)
            self.frontParTotal.config(state='disabled')

            self.backParTotal.config(state='normal')
            self.backParTotal.delete(0, END)
            self.backParTotal.insert(0, backTotal)
            self.backParTotal.config(state='disabled')

            self.parTotal.config(state='normal')
            self.parTotal.delete(0, END)
            self.parTotal.insert(0, frontTotal + backTotal)
            self.parTotal.config(state='disabled')

    def _updt_yardage_total(self):
        frontTotal = 0
        backTotal = 0
        for i in range(len(self.holeYardage)):
            if i < 9:
                new = self.holeYardage[i].get()
                if len(new) > 0:
                    frontTotal = frontTotal + int(new)

            if i > 8:
                new = self.holeYardage[i].get()
                if len(new) > 0:
                    backTotal = backTotal + int(new)

            self.frontYardageTotal.config(state='normal')
            self.frontYardageTotal.delete(0, END)
            self.frontYardageTotal.insert(0, frontTotal)
            self.frontYardageTotal.config(state='disabled')

            self.backYardageTotal.config(state='normal')
            self.backYardageTotal.delete(0, END)
            self.backYardageTotal.insert(0, backTotal)
            self.backYardageTotal.config(state='disabled')

            self.yardageTotal.config(state='normal')
            self.yardageTotal.delete(0, END)
            self.yardageTotal.insert(0, frontTotal + backTotal)
            self.yardageTotal.config(state='disabled')


    def createParRow(self):
        self.addRow()

        for i in range(len(roundDetailsColumnHeading)):
            num = len(self.holePar)
            if roundDetailsColumnHeading[i] == 'Hole':
                self.headingPar = Entry(self, relief=roundDetailsRelief, bg=roundDetailsYardageColor, font=self.tableFontB,
                                              highlightcolor=roundDetailsYardageColor, highlightthickness=0, disabledbackground=roundDetailsYardageColor,
                                              background=roundDetailsYardageColor,disabledforeground=AppDefaultForeground,
                                              justify=CENTER, width=roundDetailsColumnWidth[i])
                self.headingPar.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.headingPar.insert(0, "Par")
                self.headingPar.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'Out':
                #                self.frontParTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsParColor, width=6).grid(row=3, column=i, sticky=W+N)
                self.frontParTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsParColor,font=self.tableFont,
                                           highlightcolor=roundDetailsParColor, highlightthickness=0,
                                           disabledbackground=roundDetailsParColor,
                                           background=roundDetailsParColor,disabledforeground=AppDefaultForeground,
                                           justify=CENTER, width=roundDetailsColumnWidth[i])

                self.frontParTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.frontParTotal.insert(0, "0")
                self.frontParTotal.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'In':
                #                self.backParTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsParColor, width=6).grid(row=self.row, column=i, sticky=W+N)
                self.backParTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsParColor,font=self.tableFont,
                                          highlightcolor=roundDetailsParColor, highlightthickness=0,
                                          disabledbackground=roundDetailsParColor,
                                          background=roundDetailsParColor,disabledforeground=AppDefaultForeground,
                                          justify=CENTER, width=roundDetailsColumnWidth[i])
                self.backParTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.backParTotal.insert(0, "0")
                self.backParTotal.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'Total':
                #                self.ParTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsParColor, width=8).grid(row=self.row, column=i, sticky=W+N)
                self.parTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsParColor, font=self.tableFont,
                                      highlightcolor=roundDetailsParColor, highlightthickness=0,
                                      disabledbackground=roundDetailsParColor,
                                      background=roundDetailsParColor,disabledforeground=AppDefaultForeground,
                                      justify=CENTER, width=roundDetailsColumnWidth[i])
                self.parTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.parTotal.insert(0, "0")
                self.parTotal.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'Net':
                #                self.ParTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsParColor, width=8).grid(row=self.row, column=i, sticky=W+N)
                self.parNetTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsParColor, font=self.tableFont,
                                      highlightcolor=roundDetailsParColor, highlightthickness=0,
                                      disabledbackground=roundDetailsParColor,
                                      background=roundDetailsParColor,disabledforeground=AppDefaultForeground,
                                      justify=CENTER, width=roundDetailsColumnWidth[i])
                self.parNetTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.parNetTotal.insert(0, " ")
                self.parNetTotal.configure(state='disabled')

            else:
                self.holePar.append(
                                        Entry(self, relief=roundDetailsRelief, bg=roundDetailsParColor,
                                              highlightcolor=roundDetailsParColor, highlightthickness=0, font=self.tableFont,
                                              background=roundDetailsParColor, disabledforeground=roundDetailsForegroundDisabledColor,
                                              disabledbackground=roundDetailsBackgroundDisabledColor,justify=CENTER,
                                              width=roundDetailsColumnWidth[i])
                                    )
                self.holePar[num].grid(row=self.row, column=i, sticky=E+W+N+S)

    def createGreensRow(self):
        self.addRow()

        for i in range(len(roundDetailsColumnHeading)):
            num = len(self.holeGreens)
            if roundDetailsColumnHeading[i] == 'Hole':
                self.headingGreens = Entry(self, relief=roundDetailsRelief, bg=roundDetailsGreensColor, font=self.tableFontB,
                                              highlightcolor=roundDetailsGreensColor, highlightthickness=0, disabledbackground=roundDetailsGreensColor,
                                              background=roundDetailsGreensColor,disabledforeground=AppDefaultForeground,
                                              justify=CENTER, width=roundDetailsColumnWidth[i])
                self.headingGreens.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.headingGreens.insert(0, "Greens")
                self.headingGreens.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'Out':
                #                self.frontGreensTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsGreensColor, width=6).grid(row=3, column=i, sticky=W+N)
                self.frontGreensTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsGreensColor,font=self.tableFont,
                                           highlightcolor=roundDetailsGreensColor, highlightthickness=0,
                                           disabledbackground=roundDetailsGreensColor,
                                           background=roundDetailsGreensColor,disabledforeground=AppDefaultForeground,
                                           justify=CENTER, width=roundDetailsColumnWidth[i])

                self.frontGreensTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.frontGreensTotal.insert(0, "0")
                self.frontGreensTotal.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'In':
                #                self.backGreensTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsGreensColor, width=6).grid(row=self.row, column=i, sticky=W+N)
                self.backGreensTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsGreensColor,font=self.tableFont,
                                          highlightcolor=roundDetailsGreensColor, highlightthickness=0,
                                          disabledbackground=roundDetailsGreensColor,
                                          background=roundDetailsGreensColor,disabledforeground=AppDefaultForeground,
                                          justify=CENTER, width=roundDetailsColumnWidth[i])
                self.backGreensTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.backGreensTotal.insert(0, "0")
                self.backGreensTotal.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'Total':
                #                self.GreensTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsGreensColor, width=8).grid(row=self.row, column=i, sticky=W+N)
                self.GreensTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsGreensColor, font=self.tableFont,
                                      highlightcolor=roundDetailsGreensColor, highlightthickness=0,
                                      disabledbackground=roundDetailsGreensColor,
                                      background=roundDetailsGreensColor,disabledforeground=AppDefaultForeground,
                                      justify=CENTER, width=roundDetailsColumnWidth[i])
                self.GreensTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.GreensTotal.insert(0, "0")
                self.GreensTotal.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'Net':
                #                self.GreensTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsGreensColor, width=8).grid(row=self.row, column=i, sticky=W+N)
                self.GreensNetTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsGreensColor, font=self.tableFont,
                                      highlightcolor=roundDetailsGreensColor, highlightthickness=0,
                                      disabledbackground=roundDetailsGreensColor,
                                      background=roundDetailsGreensColor,disabledforeground=AppDefaultForeground,
                                      justify=CENTER, width=roundDetailsColumnWidth[i])
                self.GreensNetTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.GreensNetTotal.insert(0, " ")
                self.GreensNetTotal.configure(state='disabled')

            else:
                self.holeGreens.append(
                                        AppCheckMark(self, self.tableFontB, roundDetailsRelief, roundDetailsGreensColor,
                                                     roundDetailsColumnWidth[i])
                                      )
                self.holeGreens[num].grid(row=self.row, column=i, sticky=E+W+N+S)

    def createSandSavesRow(self):
        self.addRow()

        for i in range(len(roundDetailsColumnHeading)):
            num = len(self.holeSandSaves)
            if roundDetailsColumnHeading[i] == 'Hole':
                self.headingSandSaves = Entry(self, relief=roundDetailsRelief, bg=roundDetailsSandSavesColor, font=self.tableFontB,
                                              highlightcolor=roundDetailsSandSavesColor, highlightthickness=0, disabledbackground=roundDetailsSandSavesColor,
                                              background=roundDetailsSandSavesColor,disabledforeground=AppDefaultForeground,
                                              justify=CENTER, width=roundDetailsColumnWidth[i])
                self.headingSandSaves.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.headingSandSaves.insert(0, "Sand Saves")
                self.headingSandSaves.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'Out':
                #                self.frontSandSavesTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsSandSavesColor, width=6).grid(row=3, column=i, sticky=W+N)
                self.frontSandSavesTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsSandSavesColor,font=self.tableFont,
                                           highlightcolor=roundDetailsSandSavesColor, highlightthickness=0,
                                           disabledbackground=roundDetailsSandSavesColor,
                                           background=roundDetailsSandSavesColor,disabledforeground=AppDefaultForeground,
                                           justify=CENTER, width=roundDetailsColumnWidth[i])

                self.frontSandSavesTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.frontSandSavesTotal.insert(0, "0")
                self.frontSandSavesTotal.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'In':
                #                self.backSandSavesTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsSandSavesColor, width=6).grid(row=self.row, column=i, sticky=W+N)
                self.backSandSavesTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsSandSavesColor,font=self.tableFont,
                                          highlightcolor=roundDetailsSandSavesColor, highlightthickness=0,
                                          disabledbackground=roundDetailsSandSavesColor,
                                          background=roundDetailsSandSavesColor,disabledforeground=AppDefaultForeground,
                                          justify=CENTER, width=roundDetailsColumnWidth[i])
                self.backSandSavesTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.backSandSavesTotal.insert(0, "0")
                self.backSandSavesTotal.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'Total':
                #                self.SandSavesTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsSandSavesColor, width=8).grid(row=self.row, column=i, sticky=W+N)
                self.SandSavesTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsSandSavesColor, font=self.tableFont,
                                      highlightcolor=roundDetailsSandSavesColor, highlightthickness=0,
                                      disabledbackground=roundDetailsSandSavesColor,
                                      background=roundDetailsSandSavesColor,disabledforeground=AppDefaultForeground,
                                      justify=CENTER, width=roundDetailsColumnWidth[i])
                self.SandSavesTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.SandSavesTotal.insert(0, "0")
                self.SandSavesTotal.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'Net':
                #                self.SandSavesTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsSandSavesColor, width=8).grid(row=self.row, column=i, sticky=W+N)
                self.SandSavesNetTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsSandSavesColor, font=self.tableFont,
                                      highlightcolor=roundDetailsSandSavesColor, highlightthickness=0,
                                      disabledbackground=roundDetailsSandSavesColor,
                                      background=roundDetailsSandSavesColor,disabledforeground=AppDefaultForeground,
                                      justify=CENTER, width=roundDetailsColumnWidth[i])
                self.SandSavesNetTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.SandSavesNetTotal.insert(0, " ")
                self.SandSavesNetTotal.configure(state='disabled')

            else:
                self.holeSandSaves.append(
                                         AppCheckMark(self, self.tableFontB, roundDetailsRelief, roundDetailsSandSavesColor,
                                                      roundDetailsColumnWidth[i])
                                        )
                self.holeSandSaves[num].grid(row=self.row, column=i, sticky=E+W+N+S)



    def createFairwaysRow(self):
        self.addRow()

        for i in range(len(roundDetailsColumnHeading)):
            num = len(self.holeFairways)
            if roundDetailsColumnHeading[i] == 'Hole':
                self.headingFairways = Entry(self, relief=roundDetailsRelief, bg=roundDetailsFairwaysColor, font=self.tableFontB,
                                              highlightcolor=roundDetailsFairwaysColor, highlightthickness=0, disabledbackground=roundDetailsFairwaysColor,
                                              background=roundDetailsFairwaysColor,disabledforeground=AppDefaultForeground,
                                              justify=CENTER, width=roundDetailsColumnWidth[i])
                self.headingFairways.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.headingFairways.insert(0, "Fairways")
                self.headingFairways.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'Out':
                #                self.frontFairwaysTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsFairwaysColor, width=6).grid(row=3, column=i, sticky=W+N)
                self.frontFairwaysTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsFairwaysColor,font=self.tableFont,
                                           highlightcolor=roundDetailsFairwaysColor, highlightthickness=0,
                                           disabledbackground=roundDetailsFairwaysColor,
                                           background=roundDetailsFairwaysColor,disabledforeground=AppDefaultForeground,
                                           justify=CENTER, width=roundDetailsColumnWidth[i])

                self.frontFairwaysTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.frontFairwaysTotal.insert(0, "0")
                self.frontFairwaysTotal.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'In':
                #                self.backFairwaysTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsFairwaysColor, width=6).grid(row=self.row, column=i, sticky=W+N)
                self.backFairwaysTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsFairwaysColor,font=self.tableFont,
                                          highlightcolor=roundDetailsFairwaysColor, highlightthickness=0,
                                          disabledbackground=roundDetailsFairwaysColor,
                                          background=roundDetailsFairwaysColor,disabledforeground=AppDefaultForeground,
                                          justify=CENTER, width=roundDetailsColumnWidth[i])
                self.backFairwaysTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.backFairwaysTotal.insert(0, "0")
                self.backFairwaysTotal.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'Total':
                #                self.FairwaysTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsFairwaysColor, width=8).grid(row=self.row, column=i, sticky=W+N)
                self.FairwaysTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsFairwaysColor, font=self.tableFont,
                                      highlightcolor=roundDetailsFairwaysColor, highlightthickness=0,
                                      disabledbackground=roundDetailsFairwaysColor,
                                      background=roundDetailsFairwaysColor,disabledforeground=AppDefaultForeground,
                                      justify=CENTER, width=roundDetailsColumnWidth[i])
                self.FairwaysTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.FairwaysTotal.insert(0, "0")
                self.FairwaysTotal.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'Net':
                #                self.FairwaysTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsFairwaysColor, width=8).grid(row=self.row, column=i, sticky=W+N)
                self.FairwaysNetTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsFairwaysColor, font=self.tableFont,
                                      highlightcolor=roundDetailsFairwaysColor, highlightthickness=0,
                                      disabledbackground=roundDetailsFairwaysColor,
                                      background=roundDetailsFairwaysColor,disabledforeground=AppDefaultForeground,
                                      justify=CENTER, width=roundDetailsColumnWidth[i])
                self.FairwaysNetTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.FairwaysNetTotal.insert(0, " ")
                self.FairwaysNetTotal.configure(state='disabled')

            else:
                self.holeFairways.append(
                                         AppCheckMark(self, self.tableFontB, roundDetailsRelief, roundDetailsFairwaysColor,
                                                      roundDetailsColumnWidth[i])
                                        )
                self.holeFairways[num].grid(row=self.row, column=i, sticky=E+W+N+S)

    def createPuttsRow(self):
        self.addRow()

        for i in range(len(roundDetailsColumnHeading)):
            num = len(self.holePutts)
            if roundDetailsColumnHeading[i] == 'Hole':
                self.headingPutts = Entry(self, relief=roundDetailsRelief, bg=roundDetailsPuttsColor, font=self.tableFontB,
                                              highlightcolor=roundDetailsPuttsColor, highlightthickness=0, disabledbackground=roundDetailsYardageColor,
                                              background=roundDetailsPuttsColor,disabledforeground=AppDefaultForeground,
                                              justify=CENTER, width=roundDetailsColumnWidth[i])
                self.headingPutts.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.headingPutts.insert(0, "Putts")
                self.headingPutts.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'Out':
                #                self.frontPuttsTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsPuttsColor, width=6).grid(row=3, column=i, sticky=W+N)
                self.frontPuttsTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsPuttsColor,font=self.tableFont,
                                           highlightcolor=roundDetailsPuttsColor, highlightthickness=0,
                                           disabledbackground=roundDetailsPuttsColor,
                                           background=roundDetailsPuttsColor,disabledforeground=AppDefaultForeground,
                                           justify=CENTER, width=roundDetailsColumnWidth[i])

                self.frontPuttsTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.frontPuttsTotal.insert(0, "0")
                self.frontPuttsTotal.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'In':
                #                self.backPuttsTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsPuttsColor, width=6).grid(row=self.row, column=i, sticky=W+N)
                self.backPuttsTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsPuttsColor,font=self.tableFont,
                                          highlightcolor=roundDetailsPuttsColor, highlightthickness=0,
                                          disabledbackground=roundDetailsPuttsColor,
                                          background=roundDetailsPuttsColor,disabledforeground=AppDefaultForeground,
                                          justify=CENTER, width=roundDetailsColumnWidth[i])
                self.backPuttsTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.backPuttsTotal.insert(0, "0")
                self.backPuttsTotal.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'Total':
                #                self.PuttsTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsPuttsColor, width=8).grid(row=self.row, column=i, sticky=W+N)
                self.puttsTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsPuttsColor, font=self.tableFont,
                                      highlightcolor=roundDetailsPuttsColor, highlightthickness=0,
                                      disabledbackground=roundDetailsPuttsColor,
                                      background=roundDetailsPuttsColor,disabledforeground=AppDefaultForeground,
                                      justify=CENTER, width=roundDetailsColumnWidth[i])
                self.puttsTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.puttsTotal.insert(0, "0")
                self.puttsTotal.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'Net':
                #                self.puttsTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsputtsColor, width=8).grid(row=self.row, column=i, sticky=W+N)
                self.puttsNetTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsPuttsColor, font=self.tableFont,
                                      highlightcolor=roundDetailsPuttsColor, highlightthickness=0,
                                      disabledbackground=roundDetailsPuttsColor,
                                      background=roundDetailsPuttsColor,disabledforeground=AppDefaultForeground,
                                      justify=CENTER, width=roundDetailsColumnWidth[i])
                self.puttsNetTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.puttsNetTotal.insert(0, " ")
                self.puttsNetTotal.configure(state='disabled')

            else:
                self.holePutts.append(
                                        Entry(self, relief=roundDetailsRelief, bg=roundDetailsPuttsColor,
                                              highlightcolor=roundDetailsPuttsColor, highlightthickness=0, font=self.tableFont,
                                              background=roundDetailsPuttsColor, disabledforeground=roundDetailsForegroundDisabledColor,
                                              disabledbackground=roundDetailsBackgroundDisabledColor,justify=CENTER,
                                              width=roundDetailsColumnWidth[i])
                                        )
                self.holePutts[num].grid(row=self.row, column=i, sticky=E+W+N+S)

                #
                #  Done in widget enable
                #
                # self.holePutts[num].bind('<Key>', lambda event, holeNum=num: self._putt_check(event, holeNum))
                # self.holePutts[num].bind('<FocusOut>', lambda event, holeNum=num: self._checkPuttResult(event, holeNum))
                # self.holePutts[num].bind('<FocusIn>', lambda event, holeNum=num: self._checkHoleEntry(event))
                # self.holePutts[num].bind('<Right>', lambda event: self._goNext(event))
                # self.holePutts[num].bind('<Left>', lambda event: self._goPrevious(event))

    def createDriveRow(self):
        self.addRow()

        for i in range(len(roundDetailsColumnHeading)):
            num = len(self.holeDrive)
            if roundDetailsColumnHeading[i] == 'Hole':
                self.headingDrive = Entry(self, relief=roundDetailsRelief, bg=roundDetailsYardageColor, font=self.tableFontB,
                                              highlightcolor=roundDetailsYardageColor, highlightthickness=0, disabledbackground=roundDetailsYardageColor,
                                              background=roundDetailsYardageColor,disabledforeground=AppDefaultForeground,
                                              justify=CENTER, width=roundDetailsColumnWidth[i])
                self.headingDrive.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.headingDrive.insert(0, "Drive")
                self.headingDrive.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'Out':
                #                self.frontDriveTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsDriveColor, width=6).grid(row=3, column=i, sticky=W+N)
                self.frontDriveTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsDriveColor,font=self.tableFont,
                                           highlightcolor=roundDetailsDriveColor, highlightthickness=0,
                                           disabledbackground=roundDetailsDriveColor,
                                           background=roundDetailsDriveColor,disabledforeground=AppDefaultForeground,
                                           justify=CENTER, width=roundDetailsColumnWidth[i])

                self.frontDriveTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.frontDriveTotal.insert(0, "0")
                self.frontDriveTotal.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'In':
                #                self.backDriveTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsDriveColor, width=6).grid(row=self.row, column=i, sticky=W+N)
                self.backDriveTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsDriveColor,font=self.tableFont,
                                          highlightcolor=roundDetailsDriveColor, highlightthickness=0,
                                          disabledbackground=roundDetailsDriveColor,
                                          background=roundDetailsDriveColor,disabledforeground=AppDefaultForeground,
                                          justify=CENTER, width=roundDetailsColumnWidth[i])
                self.backDriveTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.backDriveTotal.insert(0, "0")
                self.backDriveTotal.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'Total':
                #                self.DriveTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsDriveColor, width=8).grid(row=self.row, column=i, sticky=W+N)
                self.driveTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsDriveColor, font=self.tableFont,
                                      highlightcolor=roundDetailsDriveColor, highlightthickness=0,
                                      disabledbackground=roundDetailsDriveColor,
                                      background=roundDetailsDriveColor,disabledforeground=AppDefaultForeground,
                                      justify=CENTER, width=roundDetailsColumnWidth[i])
                self.driveTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.driveTotal.insert(0, "0")
                self.driveTotal.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'Net':
                #                self.driveTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsdriveColor, width=8).grid(row=self.row, column=i, sticky=W+N)
                self.driveNetTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsDriveColor, font=self.tableFont,
                                      highlightcolor=roundDetailsDriveColor, highlightthickness=0,
                                      disabledbackground=roundDetailsDriveColor,
                                      background=roundDetailsDriveColor,disabledforeground=AppDefaultForeground,
                                      justify=CENTER, width=roundDetailsColumnWidth[i])
                self.driveNetTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.driveNetTotal.insert(0, " ")
                self.driveNetTotal.configure(state='disabled')

            else:
                self.holeDrive.append(
                                        Entry(self, relief=roundDetailsRelief, bg=roundDetailsDriveColor,
                                              highlightcolor=roundDetailsDriveColor, highlightthickness=0, font=self.tableFont,
                                              background=roundDetailsDriveColor, disabledforeground=roundDetailsForegroundDisabledColor,
                                              disabledbackground=roundDetailsBackgroundDisabledColor,justify=CENTER,
                                              width=roundDetailsColumnWidth[i])
                                        )
                self.holeDrive[num].grid(row=self.row, column=i, sticky=E+W+N+S)
                #
                #  Done in widget Enable
                #
                # self.holeDrive[num].bind('<Key>', lambda event, holeNum=num: self._drive_check(event, holeNum))
                # self.holeDrive[num].bind('<FocusOut>', lambda event, holeNum=num: self._checkDriveResult(event, holeNum))
                # self.holeDrive[num].bind('<FocusIn>', lambda event, holeNum=num: self._checkHoleEntry(event))
                # self.holeDrive[num].bind('<Right>', lambda event: self._goNext(event))
                # self.holeDrive[num].bind('<Left>', lambda event: self._goPrevious(event))

    def createSandTrapRow(self):
        self.addRow()

        for i in range(len(roundDetailsColumnHeading)):
            num = len(self.holeSandTrap)
            if roundDetailsColumnHeading[i] == 'Hole':
                self.headingSandTrap = Entry(self, relief=roundDetailsRelief, bg=roundDetailsYardageColor, font=self.tableFontB,
                                              highlightcolor=roundDetailsYardageColor, highlightthickness=0, disabledbackground=roundDetailsYardageColor,
                                              background=roundDetailsYardageColor,disabledforeground=AppDefaultForeground,
                                              justify=CENTER, width=roundDetailsColumnWidth[i])
                self.headingSandTrap.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.headingSandTrap.insert(0, "Sand Trap")
                self.headingSandTrap.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'Out':
                #                self.frontSandTrapTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsSandTrapColor, width=6).grid(row=3, column=i, sticky=W+N)
                self.frontSandTrapTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsSandTrapColor,font=self.tableFont,
                                           highlightcolor=roundDetailsSandTrapColor, highlightthickness=0,
                                           disabledbackground=roundDetailsSandTrapColor,
                                           background=roundDetailsSandTrapColor,disabledforeground=AppDefaultForeground,
                                           justify=CENTER, width=roundDetailsColumnWidth[i])

                self.frontSandTrapTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.frontSandTrapTotal.insert(0, "0")
                self.frontSandTrapTotal.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'In':
                #                self.backSandTrapTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsSandTrapColor, width=6).grid(row=self.row, column=i, sticky=W+N)
                self.backSandTrapTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsSandTrapColor,font=self.tableFont,
                                          highlightcolor=roundDetailsSandTrapColor, highlightthickness=0,
                                          disabledbackground=roundDetailsSandTrapColor,
                                          background=roundDetailsSandTrapColor,disabledforeground=AppDefaultForeground,
                                          justify=CENTER, width=roundDetailsColumnWidth[i])
                self.backSandTrapTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.backSandTrapTotal.insert(0, "0")
                self.backSandTrapTotal.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'Total':
                #                self.SandTrapTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsSandTrapColor, width=8).grid(row=self.row, column=i, sticky=W+N)
                self.sandTrapTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsSandTrapColor, font=self.tableFont,
                                      highlightcolor=roundDetailsSandTrapColor, highlightthickness=0,
                                      disabledbackground=roundDetailsSandTrapColor,
                                      background=roundDetailsSandTrapColor,disabledforeground=AppDefaultForeground,
                                      justify=CENTER, width=roundDetailsColumnWidth[i])
                self.sandTrapTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.sandTrapTotal.insert(0, "0")
                self.sandTrapTotal.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'Net':
                #                self.SandTrapTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsSandTrapColor, width=8).grid(row=self.row, column=i, sticky=W+N)
                self.sandTrapNetTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsSandTrapColor, font=self.tableFont,
                                      highlightcolor=roundDetailsSandTrapColor, highlightthickness=0,
                                      disabledbackground=roundDetailsSandTrapColor,
                                      background=roundDetailsSandTrapColor,disabledforeground=AppDefaultForeground,
                                      justify=CENTER, width=roundDetailsColumnWidth[i])
                self.sandTrapNetTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.sandTrapNetTotal.insert(0, " ")
                self.sandTrapNetTotal.configure(state='disabled')

            else:
                self.holeSandTrap.append(
                                            Entry(self, relief=roundDetailsRelief, bg=roundDetailsDriveColor,
                                                  highlightcolor=roundDetailsDriveColor, highlightthickness=0, font=self.tableFont,
                                                  background=roundDetailsDriveColor, disabledforeground=roundDetailsForegroundDisabledColor,
                                                  disabledbackground=roundDetailsBackgroundDisabledColor, justify=CENTER,
                                                  width=roundDetailsColumnWidth[i])
                                        )
                self.holeSandTrap[num].grid(row=self.row, column=i, sticky=E+W+N+S)
                self.holeSandTrap[num].insert(0, "N")
                self.holeSandTrap[num].configure(state='disabled')
                #
                #  Done in widget enable
                #
                # self.holeSandTrap[num].bind('<Key>', lambda event, holeNum=num: self._SandTrap_check(event, holeNum))
                # self.holeSandTrap[num].bind('<FocusOut>', lambda event, holeNum=num: self._checkSandTrapResult(event, holeNum))
                # self.holeSandTrap[num].bind('<FocusIn>', lambda event, holeNum=num: self._checkHoleEntrySandTrap(event))
                # self.holeSandTrap[num].bind('<Button-1>', lambda event, holeNum=num: self._flipSandTrapValue(event, holeNum))
                # self.holeSandTrap[num].bind('<Return>', lambda event, holeNum=num: self._flipSandTrapValue(event, holeNum))
                # self.holeSandTrap[num].bind('<Right>', lambda event: self._goNext(event))
                # self.holeSandTrap[num].bind('<Left>', lambda event: self._goPrevious(event))

    def createWaterRow(self):
        self.addRow()

        for i in range(len(roundDetailsColumnHeading)):
            num = len(self.holeWater)
            if roundDetailsColumnHeading[i] == 'Hole':
                self.headingWater = Entry(self, relief=roundDetailsRelief, bg=roundDetailsYardageColor, font=self.tableFontB,
                                              highlightcolor=roundDetailsYardageColor, highlightthickness=0, disabledbackground=roundDetailsYardageColor,
                                              background=roundDetailsYardageColor,disabledforeground=AppDefaultForeground,
                                              justify=CENTER, width=roundDetailsColumnWidth[i])
                self.headingWater.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.headingWater.insert(0, "Water")
                self.headingWater.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'Out':
                #                self.frontWaterTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsWaterColor, width=6).grid(row=3, column=i, sticky=W+N)
                self.frontWaterTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsWaterColor,font=self.tableFont,
                                           highlightcolor=roundDetailsWaterColor, highlightthickness=0,
                                           disabledbackground=roundDetailsWaterColor,
                                           background=roundDetailsWaterColor,disabledforeground=AppDefaultForeground,
                                           justify=CENTER, width=roundDetailsColumnWidth[i])

                self.frontWaterTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.frontWaterTotal.insert(0, "0")
                self.frontWaterTotal.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'In':
                #                self.backWaterTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsWaterColor, width=6).grid(row=self.row, column=i, sticky=W+N)
                self.backWaterTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsWaterColor,font=self.tableFont,
                                          highlightcolor=roundDetailsWaterColor, highlightthickness=0,
                                          disabledbackground=roundDetailsWaterColor,
                                          background=roundDetailsWaterColor,disabledforeground=AppDefaultForeground,
                                          justify=CENTER, width=roundDetailsColumnWidth[i])
                self.backWaterTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.backWaterTotal.insert(0, "0")
                self.backWaterTotal.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'Total':
                #                self.WaterTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsWaterColor, width=8).grid(row=self.row, column=i, sticky=W+N)
                self.WaterTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsWaterColor, font=self.tableFont,
                                      highlightcolor=roundDetailsWaterColor, highlightthickness=0,
                                      disabledbackground=roundDetailsWaterColor,
                                      background=roundDetailsWaterColor,disabledforeground=AppDefaultForeground,
                                      justify=CENTER, width=roundDetailsColumnWidth[i])
                self.WaterTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.WaterTotal.insert(0, "0")
                self.WaterTotal.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'Net':
                #                self.WaterTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsWaterColor, width=8).grid(row=self.row, column=i, sticky=W+N)
                self.WaterNetTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsWaterColor, font=self.tableFont,
                                      highlightcolor=roundDetailsWaterColor, highlightthickness=0,
                                      disabledbackground=roundDetailsWaterColor,
                                      background=roundDetailsWaterColor,disabledforeground=AppDefaultForeground,
                                      justify=CENTER, width=roundDetailsColumnWidth[i])
                self.WaterNetTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.WaterNetTotal.insert(0, " ")
                self.WaterNetTotal.configure(state='disabled')

            else:
                self.holeWater.append(
                                            Entry(self, relief=roundDetailsRelief, bg=roundDetailsDriveColor,
                                                  highlightcolor=roundDetailsDriveColor, highlightthickness=0, font=self.tableFont,
                                                  background=roundDetailsDriveColor, disabledforeground=roundDetailsForegroundDisabledColor,
                                                  disabledbackground=roundDetailsBackgroundDisabledColor, justify=CENTER,
                                                  width=roundDetailsColumnWidth[i])
                                        )
                self.holeWater[num].grid(row=self.row, column=i, sticky=E+W+N+S)
                self.holeWater[num].insert(0, "N")
                self.holeWater[num].configure(state='disabled')


    def _flipSandTrapValue(self, event, holeNum, withSandSaveUpdate=False):
        self.holeSandTrap[holeNum].configure(state = 'normal')
        if self.holeSandTrap[holeNum].get() == 'Y':
            self.holeSandTrap[holeNum].delete(0,END)
            self.holeSandTrap[holeNum].insert(0, 'N')
        else:
            self.holeSandTrap[holeNum].delete(0, END)
            self.holeSandTrap[holeNum].insert(0, 'Y')
        if withSandSaveUpdate == False:
            self._focusNext(self.holeSandTrap[holeNum])
        else:
            self._checkSandTrapResultNoEvent(holeNum,True)

    def createScoreRow(self):
        self.addRow()

        for i in range(len(roundDetailsColumnHeading)):
            num = len(self.holeScore)
            if roundDetailsColumnHeading[i] == 'Hole':
                self.headingScore = Entry(self, relief=roundDetailsRelief, bg=roundDetailsYardageColor, font=self.tableFontB,
                                              highlightcolor=roundDetailsYardageColor, highlightthickness=0, disabledbackground=roundDetailsYardageColor,
                                              background=roundDetailsYardageColor,disabledforeground=AppDefaultForeground,
                                              justify=CENTER, width=roundDetailsColumnWidth[i])
                self.headingScore.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.headingScore.insert(0, "Score")
                self.headingScore.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'Out':
                #                self.frontScoreTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsScoreColor, width=6).grid(row=3, column=i, sticky=W+N)
                self.frontScoreTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsScoreColor,font=self.tableFont,
                                           highlightcolor=roundDetailsScoreColor, highlightthickness=0,
                                           disabledbackground=roundDetailsScoreColor,
                                           background=roundDetailsScoreColor,disabledforeground=AppDefaultForeground,
                                           justify=CENTER, width=roundDetailsColumnWidth[i])

                self.frontScoreTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.frontScoreTotal.insert(0, "0")
                self.frontScoreTotal.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'In':
                #                self.backScoreTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsScoreColor, width=6).grid(row=self.row, column=i, sticky=W+N)
                self.backScoreTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsScoreColor,font=self.tableFont,
                                          highlightcolor=roundDetailsScoreColor, highlightthickness=0,
                                          disabledbackground=roundDetailsScoreColor,
                                          background=roundDetailsScoreColor,disabledforeground=AppDefaultForeground,
                                          justify=CENTER, width=roundDetailsColumnWidth[i])
                self.backScoreTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.backScoreTotal.insert(0, "0")
                self.backScoreTotal.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'Total':
                #                self.ScoreTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsScoreColor, width=8).grid(row=self.row, column=i, sticky=W+N)
                self.scoreTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsScoreColor, font=self.tableFont,
                                      highlightcolor=roundDetailsScoreColor, highlightthickness=0,
                                      disabledbackground=roundDetailsScoreColor,
                                      background=roundDetailsScoreColor,disabledforeground=AppDefaultForeground,
                                      justify=CENTER, width=roundDetailsColumnWidth[i])
                self.scoreTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.scoreTotal.insert(0, "0")
                self.scoreTotal.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'Net':
                #                self.ScoreTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsScoreColor, width=8).grid(row=self.row, column=i, sticky=W+N)
                self.scoreNetTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsScoreColor, font=self.tableFont,
                                      highlightcolor=roundDetailsScoreColor, highlightthickness=0,
                                      disabledbackground=roundDetailsScoreColor,
                                      background=roundDetailsScoreColor,disabledforeground=AppDefaultForeground,
                                      justify=CENTER, width=roundDetailsColumnWidth[i])
                self.scoreNetTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.scoreNetTotal.insert(0, "0")
                self.scoreNetTotal.configure(state='disabled')

            else:
                # aValidationCommand = (self.register(self._holeScore_check),
                #                                     '%d', '%i', '%P', '%s', '%S', '%v', '%V', '%W')
                self.holeScore.append(
                                        Entry(self, relief=roundDetailsRelief, bg=roundDetailsScoreColor,
                                              highlightcolor=roundDetailsScoreColor, highlightthickness=0, font=self.tableFont,
                                              background=roundDetailsScoreColor, disabledforeground=roundDetailsForegroundDisabledColor,
                                              disabledbackground=roundDetailsBackgroundDisabledColor,justify=CENTER,
                                              width=roundDetailsColumnWidth[i])
                                        )
                self.holeScore[num].grid(row=self.row, column=i, sticky=E+W+N+S)
                #
                #  Is now done in widgetEnbale
                # self.holeScore[num].bind('<Key>', self._holeScore_check)
                # self.holeScore[num].bind('<FocusOut>', lambda event, holeNum=num: self._checkHoleResult(event, holeNum))
                # # self.holeScore[num].bind('<Right>', lambda event: self._goNext(event))
                # self.holeScore[num].bind('<FocusIn>', lambda event, holeNum=num: self._checkHoleEntry(event))
                # # self.holeScore[num].bind('<Left>', lambda event: self._goPrevious(event))

    def _goNext(self, event):
        self._focusNext(event.widget)

    def _focusNext(self, awidget):
        '''Return the next widget in tab order'''
        awidget = self.tk.call('tk_focusNext', awidget._w)
        if not awidget: return None
        #        return self.nametowidget(widget.string)
        nextWidget = self.nametowidget(awidget.string)
        nextWidget.focus()
        return "break"

    def _goPrevious(self, event):
        self._focusPrevious(event.widget)

    def createHandicapRow(self):
        self.addRow()

        for i in range(len(roundDetailsColumnHeading)):
            num = len(self.holeHandicap)
            if roundDetailsColumnHeading[i] == 'Hole':
                self.handicapHeading = Entry(self, relief=roundDetailsRelief, bg=roundDetailsYardageColor, font=self.tableFontB,
                                              highlightcolor=roundDetailsYardageColor, highlightthickness=0, disabledbackground=roundDetailsYardageColor,
                                              background=roundDetailsYardageColor,disabledforeground=AppDefaultForeground,
                                              justify=CENTER, width=roundDetailsColumnWidth[i])
                self.handicapHeading.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.handicapHeading.insert(0, "Handicap")
                self.handicapHeading.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'Out':
                #                self.frontHandicapTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsHandicapColor, width=6).grid(row=3, column=i, sticky=W+N)
                self.handicapOut = Entry(self, text=' ', relief=roundDetailsRelief, bg=roundDetailsHandicapColor, font=self.tableFontB,
                                         disabledforeground=roundDetailsForegroundDisabledColor, highlightthickness=0,
                                         disabledbackground=roundDetailsBackgroundDisabledColor,
                                        width=roundDetailsColumnWidth[i])
                self.handicapOut.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.handicapOut.configure(state='disabled')
            elif roundDetailsColumnHeading[i] == 'In':
                #                self.backHandicapTotal = Label(self.roundYDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsHandicapColor, width=6).grid(row=self.row, column=i, sticky=W+N)
                self.handicapIn = Entry(self, text=' ', relief=roundDetailsRelief, bg=roundDetailsHandicapColor, font=self.tableFontB,
                                        disabledforeground=roundDetailsForegroundDisabledColor, highlightthickness=0,
                                        disabledbackground=roundDetailsBackgroundDisabledColor,
                                        width=roundDetailsColumnWidth[i])
                self.handicapIn.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.handicapIn.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'Total':
                #                self.HandicapTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsHandicapColor, width=8).grid(row=self.row, column=i, sticky=W+N)
                self.handicapTotal = Entry(self, text=' ', relief=roundDetailsRelief, bg=roundDetailsHandicapColor, font=self.tableFontB,
                                           disabledforeground=roundDetailsForegroundDisabledColor, highlightthickness=0,
                                           disabledbackground=roundDetailsBackgroundDisabledColor,
                                           width=roundDetailsColumnWidth[i])
                self.handicapTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.handicapTotal.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'Net':
                #                self.HandicapTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsHandicapColor, width=8).grid(row=self.row, column=i, sticky=W+N)
                self.handicapNetTotal = Entry(self, text=' ', relief=roundDetailsRelief, bg=roundDetailsHandicapColor, font=self.tableFontB,
                                           disabledforeground=roundDetailsForegroundDisabledColor, highlightthickness=0,
                                           disabledbackground=roundDetailsBackgroundDisabledColor,
                                           width=roundDetailsColumnWidth[i])
                self.handicapNetTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.handicapNetTotal.configure(state='disabled')

            else:

                self.holeHandicap.append(
                                        Entry(self, relief=roundDetailsRelief, bg=roundDetailsHandicapColor,
                                              highlightcolor=roundDetailsHandicapColor, highlightthickness=0, font=self.tableFont,
                                              background=roundDetailsHandicapColor, disabledforeground=roundDetailsForegroundDisabledColor,
                                              disabledbackground=roundDetailsBackgroundDisabledColor,justify=CENTER, width=roundDetailsColumnWidth[i])
                                        )
                self.holeHandicap[num].grid(row=self.row, column=i, sticky=E+W+N+S)

    def createYardageRow(self):
        self.addRow()

        for i in range(len(roundDetailsColumnHeading)):
            num = len(self.holeYardage)
            if roundDetailsColumnHeading[i] == 'Hole':
                self.headingYardage = Entry(self, relief=roundDetailsRelief, bg=roundDetailsYardageColor, font=self.tableFontB,
                                              highlightcolor=roundDetailsYardageColor, highlightthickness=0, disabledbackground=roundDetailsYardageColor,
                                              background=roundDetailsYardageColor,disabledforeground=AppDefaultForeground,
                                              justify=CENTER, width=roundDetailsColumnWidth[i])
                self.headingYardage.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.headingYardage.insert(0, "Yardage")
                self.headingYardage.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'Out':
                #                self.frontYardageTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsYardageColor, width=6).grid(row=3, column=i, sticky=W+N)
                self.frontYardageTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsYardageColor, font=self.tableFont,
                                              highlightcolor=roundDetailsYardageColor, highlightthickness=0, disabledbackground=roundDetailsYardageColor,
                                              background=roundDetailsYardageColor,disabledforeground=AppDefaultForeground,
                                              justify=CENTER, width=roundDetailsColumnWidth[i])
                self.frontYardageTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.frontYardageTotal.insert(0, "0")
                self.frontYardageTotal.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'In':
                #                self.backYardageTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsYardageColor, width=6).grid(row=self.row, column=i, sticky=W+N)
                self.backYardageTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsYardageColor, font=self.tableFont,
                                              highlightcolor=roundDetailsYardageColor, highlightthickness=0, disabledbackground=roundDetailsYardageColor,
                                              background=roundDetailsYardageColor,disabledforeground=AppDefaultForeground,
                                              justify=CENTER, width=roundDetailsColumnWidth[i])
                self.backYardageTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.backYardageTotal.insert(0, "0")
                self.backYardageTotal.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'Total':
                #                self.YardageTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsYardageColor, width=8).grid(row=self.row, column=i, sticky=W+N)
                self.yardageTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsYardageColor, font=self.tableFont,
                                          highlightcolor=roundDetailsYardageColor, highlightthickness=0, disabledbackground=roundDetailsYardageColor,
                                          background=roundDetailsYardageColor,disabledforeground=AppDefaultForeground,
                                          justify=CENTER, width=roundDetailsColumnWidth[i])
                self.yardageTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.yardageTotal.insert(0, "0")
                self.yardageTotal.configure(state='disabled')

            elif roundDetailsColumnHeading[i] == 'Net':
                #                self.YardageTotal = Label(self.roundDetailArea, text="0", relief='groove', bd=1, bg=roundDetailsYardageColor, width=8).grid(row=self.row, column=i, sticky=W+N)
                self.yardageNetTotal = Entry(self, relief=roundDetailsRelief, bg=roundDetailsYardageColor, font=self.tableFont,
                                          highlightcolor=roundDetailsYardageColor, highlightthickness=0, disabledbackground=roundDetailsYardageColor,
                                          background=roundDetailsYardageColor,disabledforeground=AppDefaultForeground,
                                          justify=CENTER, width=roundDetailsColumnWidth[i])
                self.yardageNetTotal.grid(row=self.row, column=i, sticky=E+W+N+S)
                self.yardageNetTotal.insert(0, " ")
                self.yardageNetTotal.configure(state='disabled')

            else:
                self.holeYardage.append(
                                        Entry(self, relief=roundDetailsRelief, bg=roundDetailsYardageColor,
                                              highlightcolor=roundDetailsYardageColor, highlightthickness=0, font=self.tableFont,
                                              background=roundDetailsYardageColor, disabledforeground=roundDetailsForegroundDisabledColor,
                                              disabledbackground=roundDetailsBackgroundDisabledColor,justify=CENTER,
                                              width=roundDetailsColumnWidth[i])
                                        )
                self.holeYardage[num].grid(row=self.row, column=i, sticky=E+W+N+S)

    def validateRequiredFields(self):
#        requiredFieldsEntered = True

        for i in range(self.startHole, self.endHole):
            if len(self.holeScore[i].get()) == 0:
                self.holeScore[i].focus()
                return False

        for i in range(self.startHole, self.endHole):
            if len(self.holePutts[i].get()) == 0:
                self.holePutts[i].focus()
                return False

        for i in range(self.startHole, self.endHole):
            if len(self.holeDrive[i].get()) == 0:
                if self.holePar[i].get() != "3":
                    self.holeDrive[i].focus()
                    return False

        for i in range(self.startHole, self.endHole):
            if len(self.holeSandTrap[i].get()) == 0:
                self.holeSandTrap[i].focus()
                return False

        return True

    def _checkHoleEntrySandTrap(self, event):
        self._focusHighlight(event.widget)
        #self._highlight(event)

    def _checkHoleEntry(self, event):
        self._focusHighlight(event.widget)
        currentWidget = event.widget
        currentContent = currentWidget.get()
        if len(currentContent) > 0:
            currentWidget.select_range(0, END)

    def _highlight(self, event):
        event.widget.configure(relief=roundDetailsHighlightRelief, font=self.tableFontB)
#           event.widget.focus()

    def currentFocusWidgetPartOfRootTable(self):
        aName = self.focus_get()
        pattern = re.compile('.*' + self.rootWidgetName + '*.')
        if re.match(pattern, str(aName)) == None:
            return False
        else:
            return True

    def _focusHighlight(self, aWidget):
        if self.currentFocusWidgetPartOfRootTable():
            aWidget.configure(relief=roundDetailsHighlightRelief, font=self.tableFontB)

    def _removeHighlight(self, event):
        currentFocusWidget= event.widget.focus_get()
        event.widget.configure(relief=roundDetailsRelief, font=self.tableFont)
        self._focusHighlight(currentFocusWidget)

    def enableTotals(self):
        self.headingGreens.configure(disabledbackground=roundDetailsGreensColor,
                                       disabledforeground=AppDefaultForeground)

        self.GreensTotal.configure(disabledbackground=roundDetailsGreensColor,
                                    disabledforeground=AppDefaultForeground)
        self.GreensNetTotal.configure(disabledbackground=roundDetailsGreensColor,
                                    disabledforeground=AppDefaultForeground)
        self.frontGreensTotal.configure(disabledbackground=roundDetailsGreensColor,
                                         disabledforeground=AppDefaultForeground)
        self.backGreensTotal.configure(disabledbackground=roundDetailsGreensColor,
                                         disabledforeground=AppDefaultForeground)

        self.headingFairways.configure(disabledbackground=roundDetailsGreensColor,
                                     disabledforeground=AppDefaultForeground)

        self.FairwaysTotal.configure(disabledbackground=roundDetailsGreensColor,
                                   disabledforeground=AppDefaultForeground)
        self.FairwaysNetTotal.configure(disabledbackground=roundDetailsGreensColor,
                                      disabledforeground=AppDefaultForeground)
        self.frontFairwaysTotal.configure(disabledbackground=roundDetailsGreensColor,
                                        disabledforeground=AppDefaultForeground)
        self.backFairwaysTotal.configure(disabledbackground=roundDetailsGreensColor,
                                       disabledforeground=AppDefaultForeground)

        self.headingSandSaves.configure(disabledbackground=roundDetailsGreensColor,
                                     disabledforeground=AppDefaultForeground)

        self.SandSavesTotal.configure(disabledbackground=roundDetailsGreensColor,
                                   disabledforeground=AppDefaultForeground)
        self.SandSavesNetTotal.configure(disabledbackground=roundDetailsGreensColor,
                                      disabledforeground=AppDefaultForeground)
        self.frontSandSavesTotal.configure(disabledbackground=roundDetailsGreensColor,
                                        disabledforeground=AppDefaultForeground)
        self.backSandSavesTotal.configure(disabledbackground=roundDetailsGreensColor,
                                       disabledforeground=AppDefaultForeground)

        self.headingYardage.configure(disabledbackground=roundDetailsYardageColor,
                                       disabledforeground=AppDefaultForeground)

        self.yardageTotal.configure(disabledbackground=roundDetailsYardageColor,
                                    disabledforeground=AppDefaultForeground)
        self.yardageNetTotal.configure(disabledbackground=roundDetailsYardageColor,
                                    disabledforeground=AppDefaultForeground)
        self.frontYardageTotal.configure(disabledbackground=roundDetailsYardageColor,
                                         disabledforeground=AppDefaultForeground)
        self.backYardageTotal.configure(disabledbackground=roundDetailsYardageColor,
                                         disabledforeground=AppDefaultForeground)

        self.headingPar.configure(disabledbackground=roundDetailsParColor,
                                disabledforeground=AppDefaultForeground)
        self.parTotal.configure(disabledbackground=roundDetailsParColor,
                                disabledforeground=AppDefaultForeground)
        self.parNetTotal.configure(disabledbackground=roundDetailsParColor,
                                disabledforeground=AppDefaultForeground)
        self.frontParTotal.config(disabledbackground=roundDetailsParColor,
                                disabledforeground=AppDefaultForeground)
        self.backParTotal.config(disabledbackground=roundDetailsParColor,
                                disabledforeground=AppDefaultForeground)


        self.headingScore.configure(disabledbackground=roundDetailsScoreColor,
                                disabledforeground=AppDefaultForeground)
        self.scoreTotal.configure(disabledbackground=roundDetailsScoreColor,
                                disabledforeground=AppDefaultForeground)
        self.scoreNetTotal.configure(disabledbackground=roundDetailsScoreColor,
                                disabledforeground=AppDefaultForeground)
        self.frontScoreTotal.configure(disabledbackground=roundDetailsScoreColor,
                                disabledforeground=AppDefaultForeground)
        self.backScoreTotal.configure(disabledbackground=roundDetailsScoreColor,
                                disabledforeground=AppDefaultForeground)


        self.headingPutts.configure(disabledbackground=roundDetailsPuttsColor,
                                disabledforeground=AppDefaultForeground)
        self.puttsTotal.configure(disabledbackground=roundDetailsPuttsColor,
                                disabledforeground=AppDefaultForeground)
        self.puttsNetTotal.configure(disabledbackground=roundDetailsPuttsColor,
                                disabledforeground=AppDefaultForeground)
        self.frontPuttsTotal.configure(disabledbackground=roundDetailsPuttsColor,
                                disabledforeground=AppDefaultForeground)
        self.backPuttsTotal.configure(disabledbackground=roundDetailsPuttsColor,
                                disabledforeground=AppDefaultForeground)

        self.headingSandTrap.configure(disabledbackground=roundDetailsSandTrapColor,
                                disabledforeground=AppDefaultForeground)
        self.sandTrapTotal.configure(disabledbackground=roundDetailsSandTrapColor,
                                disabledforeground=AppDefaultForeground)
        self.sandTrapNetTotal.configure(disabledbackground=roundDetailsSandTrapColor,
                                disabledforeground=AppDefaultForeground)
        self.frontSandTrapTotal.configure(disabledbackground=roundDetailsSandTrapColor,
                                disabledforeground=AppDefaultForeground)
        self.backSandTrapTotal.configure(disabledbackground=roundDetailsSandTrapColor,
                                disabledforeground=AppDefaultForeground)

        self.headingWater.configure(disabledbackground=roundDetailsWaterColor,
                                disabledforeground=AppDefaultForeground)
        self.WaterTotal.configure(disabledbackground=roundDetailsWaterColor,
                                disabledforeground=AppDefaultForeground)
        self.WaterNetTotal.configure(disabledbackground=roundDetailsWaterColor,
                                disabledforeground=AppDefaultForeground)
        self.frontWaterTotal.configure(disabledbackground=roundDetailsWaterColor,
                                disabledforeground=AppDefaultForeground)
        self.backWaterTotal.configure(disabledbackground=roundDetailsWaterColor,
                                disabledforeground=AppDefaultForeground)


        self.headingDrive.configure(disabledbackground=roundDetailsDriveColor,
                                disabledforeground=AppDefaultForeground)
        self.driveTotal.configure(disabledbackground=roundDetailsDriveColor,
                                disabledforeground=AppDefaultForeground)
        self.driveNetTotal.configure(disabledbackground=roundDetailsDriveColor,
                                disabledforeground=AppDefaultForeground)
        self.frontDriveTotal.configure(disabledbackground=roundDetailsDriveColor,
                                disabledforeground=AppDefaultForeground)
        self.backDriveTotal.configure(disabledbackground=roundDetailsDriveColor,
                                disabledforeground=AppDefaultForeground)


        self.handicapHeading.config(disabledbackground=roundDetailsHandicapColor,
                                    disabledforeground=AppDefaultForeground)
        self.handicapIn.config(disabledbackground=roundDetailsHandicapColor,
                               disabledforeground=AppDefaultForeground)
        self.handicapOut.config(disabledbackground=roundDetailsHandicapColor)
        self.handicapTotal.config(disabledbackground=roundDetailsHandicapColor,
                                  disabledforeground=AppDefaultForeground)
        self.handicapNetTotal.config(disabledbackground=roundDetailsHandicapColor,
                                  disabledforeground=AppDefaultForeground)

        for i in range(self.startHole, self.endHole):
            self.holePar[i].configure(disabledbackground=roundDetailsParColor,
                                      disabledforeground=AppDefaultForeground)
            self.holeYardage[i].configure(disabledbackground=roundDetailsYardageColor,
                                          disabledforeground=AppDefaultForeground)
            self.holeHandicap[i].configure(disabledbackground=roundDetailsHandicapColor,
                                           disabledforeground=AppDefaultForeground)
            self._checkHoleResultNoEvent(self.holeScore[i], i, 'view')
            # self.holeScore[i].configure(disabledbackground=roundDetailsScoreColor,
            #                             disabledforeground=AppDefaultForeground)
            self.holePutts[i].configure(disabledbackground=roundDetailsPuttsColor,
                                        disabledforeground=AppDefaultForeground)
            # self.holeDrive[i].configure(disabledbackground=roundDetailsDriveColor,
            #                             disabledforeground=AppDefaultForeground)
            self.holeSandTrap[i].configure(disabledbackground=roundDetailsDriveColor,
                                           disabledforeground=AppDefaultForeground)
            self.holeGreens[i].viewEnableScorecard(roundDetailsGreensColor)
            self.holeSandSaves[i].viewEnableScorecard(roundDetailsGreensColor)

            if self.holePar[i].get() != '3':
                self.holeFairways[i].viewEnableScorecard(roundDetailsGreensColor)
                self.holeDrive[i].configure(disabledbackground=roundDetailsDriveColor,
                                            disabledforeground=AppDefaultForeground)
            else:
                self.holeFairways[i].unSet()
                self.holeDrive[i].configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                            disabledforeground=roundDetailsForegroundDisabledColor)

    def enableView(self):
        self.enableTotals()

    def enable(self):
        self.tableEnabled = True
        for i in range(self.startHole, self.endHole):
            self.holePar[i].configure(disabledbackground=roundDetailsParColor,
                                      disabledforeground=AppDefaultForeground)
            self.holeYardage[i].configure(disabledbackground=roundDetailsYardageColor,
                                          disabledforeground=AppDefaultForeground)
            self.holeHandicap[i].configure(disabledbackground=roundDetailsHandicapColor,
                                           disabledforeground=AppDefaultForeground)
            self.holeGreens[i].viewEnableScorecard(roundDetailsGreensColor)

            if self.holePar[i].get() != '3':
                self.holeFairways[i].viewEnableScorecard(roundDetailsGreensColor)
            else:
                self.holeFairways[i].unSet()

            self.holeSandSaves[i].viewEnableScorecard(roundDetailsGreensColor)
            self.widgetEnable(self.holeScore[i], 'Score', i)
            self.widgetEnable(self.holePutts[i], 'Putts', i)

            if self.holePar[i].get() != '3':
                self.widgetEnable(self.holeDrive[i], 'Drive', i)
            else:
                self.widgetClear(self.holeDrive[i])

            self.widgetEnable(self.holeSandTrap[i], 'SandTrap', i)

        if (self.endHole-self.startHole) != 18:
            if self.startHole == 0:
                for i in range(9, 18):
                    self.widgetClear(self.holeScore[i])
                    self.widgetClear(self.holePutts[i])
                    self.widgetClear(self.holeDrive[i])
                    self.widgetClear(self.holeSandTrap[i])
                    self.holeGreens[i].unSet()
                    self.holeFairways[i].unSet()
                    self.holeSandSaves[i].unSet()
            else:
                for i in range(0, 9):
                    self.widgetClear(self.holeScore[i])
                    self.widgetClear(self.holePutts[i])
                    self.widgetClear(self.holeDrive[i])
                    self.widgetClear(self.holeSandTrap[i])
                    self.holeGreens[i].unSet()
                    self.holeFairways[i].unSet()
                    self.holeSandSaves[i].unSet()

        self.enableTotals()
        # self._updateTotals()

    def verifyRequiredFieldsroundDetails(self):
        fieldsAllFilled = True
        for i in range(18):
            if len(self.holeHandicap[i].get()) == 0:
                fieldsAllFilled = False
                self.holeHandicap[i].focus()
                return False

        for i in range(18):
            if len(self.holeYardage[i].get()) == 0:
                fieldsAllFilled = False
                self.holeYardage[i].focus()
                return False

        for i in range(18):
            if len(self.holePar[i].get()) == 0:
                fieldsAllFilled = False
                self.holePar[i].focus()
                return False

        return fieldsAllFilled

    def populateRoundFromImportData(self, aGameData):
        self._defineHoleRange(aGameData[5])
        for i in range(self.startHole, self.endHole):
            self.widgetLoad(self.holeScore[i], aGameData[0][i])
            if len(self.holeScore[i].get()) > 0:
                self._checkHoleResultNoEvent(self.holeScore[i], i)
            else:
                self.holeScore[i].config(bg=roundDetailsScoreColor, fg=AppDefaultForeground)
            self.widgetLoad(self.holePutts[i], aGameData[1][i])
            self._checkPuttResultNoEvent(i, True)
            if int(self.holePar[i].get()) != 3:
                self.widgetLoad(self.holeDrive[i], aGameData[2][i])
                self._checkDriveResultNoEVENT(i, True)
            self.widgetLoad(self.holeSandTrap[i], aGameData[3][i])
            self._checkSandTrapResultNoEvent(i, True)

        self._updateTotals()

    # cursor.execute('''CREATE TABLE GameDetails(
    #    uniqueID       INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
    #    holeNumber     SMALLINT    NOT NULL,
    #    holeScore      SMALLINT    NOT NULL,
    #    holePutts      SMALLINT,
    #    holeDrives     CHAR(4),
    #    holeSandTraps  CHAR(1),
    #    gameID         INTEGER     NOT NULL,
    #    deleted        CHAR(1)     NOT NULL,
    #    lastModified   INTEGER,
    #    updaterID      INTEGER,
    #    FOREIGN KEY(gameID) REFERENCES Games(uniqueID)
    #    );''')

    def populateRoundInformation(self, aGameDetailsList):
        for i in range(self.startHole, self.endHole):
            self.widgetLoad(self.holeScore[i], aGameDetailsList[i][2])
            if len(self.holeScore[i].get()) > 0:
                self._checkHoleResultNoEvent(self.holeScore[i], i)
            else:
                self.holeScore[i].config(bg=roundDetailsScoreColor, fg=AppDefaultForeground)
            self.widgetLoad(self.holePutts[i], aGameDetailsList[i][3])
            self._checkPuttResultNoEvent(i, True)
            self.widgetLoad(self.holeDrive[i], aGameDetailsList[i][4])
            self._checkDriveResultNoEVENT(i, True)
            self.widgetLoad(self.holeSandTrap[i], aGameDetailsList[i][5])
            self._checkSandTrapResultNoEvent(i, True)

        self._updateTotals()

    def populateTeeInformation(self, roundDetailsList, numberOfHolesPlayed,
                               golferID, courseSlope, NoTotals = False):
        self._defineHoleRange(numberOfHolesPlayed)
        self.golferID=golferID
        self.courseSlope = courseSlope
        disableTable = False
        if self.tableEnabled == False:
            disableTable = True
            self.enable()

        if NoTotals == False:
            for i in range(len(roundDetailsList)):
                self.widgetClear(self.holeHandicap[i])
                self.widgetClear(self.holeYardage[i])
                self.widgetClear(self.holePar[i])

        for i in range(self.startHole, self.endHole):
            self.widgetEnable(self.holeYardage[i])
            self.holeYardage[i].insert(0, roundDetailsList[i][1])
            self.widgetDisable(self.holeYardage[i])

            self.widgetEnable(self.holeHandicap[i])
            self.holeHandicap[i].insert(0,roundDetailsList[i][2])
            self.widgetDisable(self.holeHandicap[i])

            self.widgetEnable(self.holePar[i])
            self.holePar[i].insert(0, roundDetailsList[i][3])
            self.widgetDisable(self.holePar[i])

        self._updt_par_total()
        self._updt_yardage_total()

        for i in range(0,18):
            if len(self.holeScore[i].get()) > 0:
                self._checkHoleResultNoEvent(self.holeScore[i], i)
            else:
                self.holeScore[i].config(bg=roundDetailsScoreColor, fg=AppDefaultForeground)

        self.widgetClear(self.scoreNetTotal)

        self._updt_score_total()

        if disableTable == True:
            self.disable()

    def disable(self):
        for i in range(18):
            self.holePar[i].configure(state='disabled',
                                      disabledbackground=roundDetailsBackgroundDisabledColor,
                                      disabledforeground=roundDetailsForegroundDisabledColor)
            self.holeYardage[i].configure(state='disabled',
                                          disabledbackground=roundDetailsBackgroundDisabledColor,
                                          disabledforeground=roundDetailsForegroundDisabledColor)
            self.holeHandicap[i].configure(state='disabled',
                                           disabledbackground=roundDetailsBackgroundDisabledColor,
                                           disabledforeground=roundDetailsForegroundDisabledColor)
            self.holeGreens[i].viewDisableScorecard(roundDetailsBackgroundDisabledColor)
            self.holeFairways[i].viewDisableScorecard(roundDetailsBackgroundDisabledColor)
            self.holeSandSaves[i].viewDisableScorecard(roundDetailsBackgroundDisabledColor)
            self.widgetDisable(self.holeScore[i], 'Score')
            self.widgetDisable(self.holePutts[i], 'Putts')
            self.widgetDisable(self.holeDrive[i], 'Drive')
            self.widgetDisable(self.holeSandTrap[i], 'SandTrap')

        self.headingGreens.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                      disabledforeground=roundDetailsForegroundDisabledColor)

        self.GreensTotal.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                    disabledforeground=roundDetailsForegroundDisabledColor)
        self.GreensNetTotal.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                    disabledforeground=roundDetailsForegroundDisabledColor)
        self.frontGreensTotal.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                         disabledforeground=roundDetailsForegroundDisabledColor)
        self.backGreensTotal.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                         disabledforeground=roundDetailsForegroundDisabledColor)

        self.headingFairways.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                      disabledforeground=roundDetailsForegroundDisabledColor)

        self.FairwaysTotal.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                    disabledforeground=roundDetailsForegroundDisabledColor)
        self.FairwaysNetTotal.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                    disabledforeground=roundDetailsForegroundDisabledColor)
        self.frontFairwaysTotal.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                         disabledforeground=roundDetailsForegroundDisabledColor)
        self.backFairwaysTotal.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                         disabledforeground=roundDetailsForegroundDisabledColor)

        self.headingSandSaves.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                      disabledforeground=roundDetailsForegroundDisabledColor)

        self.SandSavesTotal.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                    disabledforeground=roundDetailsForegroundDisabledColor)
        self.SandSavesNetTotal.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                    disabledforeground=roundDetailsForegroundDisabledColor)
        self.frontSandSavesTotal.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                         disabledforeground=roundDetailsForegroundDisabledColor)
        self.backSandSavesTotal.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                         disabledforeground=roundDetailsForegroundDisabledColor)

        self.headingYardage.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                      disabledforeground=roundDetailsForegroundDisabledColor)

        self.yardageTotal.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                    disabledforeground=roundDetailsForegroundDisabledColor)
        self.yardageNetTotal.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                    disabledforeground=roundDetailsForegroundDisabledColor)
        self.frontYardageTotal.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                         disabledforeground=roundDetailsForegroundDisabledColor)
        self.backYardageTotal.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                         disabledforeground=roundDetailsForegroundDisabledColor)

        self.headingScore.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                  disabledforeground=roundDetailsForegroundDisabledColor)
        self.scoreTotal.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                disabledforeground=roundDetailsForegroundDisabledColor)
        self.scoreNetTotal.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                disabledforeground=roundDetailsForegroundDisabledColor)
        self.frontScoreTotal.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                     disabledforeground=roundDetailsForegroundDisabledColor)
        self.backScoreTotal.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                    disabledforeground=roundDetailsForegroundDisabledColor)

        self.headingPutts.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                  disabledforeground=roundDetailsForegroundDisabledColor)
        self.puttsTotal.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                disabledforeground=roundDetailsForegroundDisabledColor)
        self.puttsNetTotal.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                disabledforeground=roundDetailsForegroundDisabledColor)
        self.frontPuttsTotal.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                     disabledforeground=roundDetailsForegroundDisabledColor)
        self.backPuttsTotal.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                    disabledforeground=roundDetailsForegroundDisabledColor)

        self.headingDrive.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                  disabledforeground=roundDetailsForegroundDisabledColor)
        self.driveTotal.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                disabledforeground=roundDetailsForegroundDisabledColor)
        self.driveNetTotal.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                disabledforeground=roundDetailsForegroundDisabledColor)
        self.frontDriveTotal.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                     disabledforeground=roundDetailsForegroundDisabledColor)
        self.backDriveTotal.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                    disabledforeground=roundDetailsForegroundDisabledColor)

        self.headingSandTrap.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                  disabledforeground=roundDetailsForegroundDisabledColor)
        self.frontSandTrapTotal.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                     disabledforeground=roundDetailsForegroundDisabledColor)
        self.backSandTrapTotal.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                    disabledforeground=roundDetailsForegroundDisabledColor)
        self.sandTrapTotal.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                disabledforeground=roundDetailsForegroundDisabledColor)
        self.sandTrapNetTotal.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                disabledforeground=roundDetailsForegroundDisabledColor)

        self.headingPar.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                  disabledforeground=roundDetailsForegroundDisabledColor)
        self.parTotal.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                disabledforeground=roundDetailsForegroundDisabledColor)
        self.parNetTotal.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                disabledforeground=roundDetailsForegroundDisabledColor)
        self.frontParTotal.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                     disabledforeground=roundDetailsForegroundDisabledColor)
        self.backParTotal.configure(disabledbackground=roundDetailsBackgroundDisabledColor,
                                    disabledforeground=roundDetailsForegroundDisabledColor)

        self.handicapHeading.config(disabledbackground=roundDetailsBackgroundDisabledColor,
                                    disabledforeground=roundDetailsForegroundDisabledColor)
        self.handicapIn.config(disabledbackground=roundDetailsBackgroundDisabledColor,
                               disabledforeground=roundDetailsForegroundDisabledColor)
        self.handicapOut.config(disabledbackground=roundDetailsBackgroundDisabledColor)
        self.handicapTotal.config(disabledbackground=roundDetailsBackgroundDisabledColor,
                                 disabledforeground=roundDetailsForegroundDisabledColor)
        self.handicapNetTotal.config(disabledbackground=roundDetailsBackgroundDisabledColor,
                                 disabledforeground=roundDetailsForegroundDisabledColor)

        self.tableEnabled = False

    def widgetDisable(self,aWidget, widgetType=None):
        #
        #
        #  This is the last font
        #
        # for i in range(18):
            aWidget.config(state='disabled', relief=roundDetailsRelief, font=self.tableFont,
                           disabledbackground=roundDetailsBackgroundDisabledColor,
                           disabledforeground=roundDetailsForegroundDisabledColor)
            aWidget.unbind('<Enter>')
            aWidget.unbind('<Leave>')
            aWidget.bind('<Right>')
            aWidget.bind('<Left>')
            if widgetType == 'Score':
                aWidget.unbind('<Key>')
                aWidget.unbind('<FocusOut>')
                aWidget.unbind('<FocusIn>')
            if widgetType == 'SandTrap':
                aWidget.unbind('<Key>')
                aWidget.unbind('<FocusOut>')
                aWidget.unbind('<FocusIn>')
                aWidget.unbind('<Button-1>')
                aWidget.unbind('<Return>')
            if widgetType == 'Putts':
                aWidget.unbind('<Key>')
                aWidget.unbind('<FocusOut>')
                aWidget.unbind('<FocusIn>')
            if widgetType == 'Drive':
                aWidget.unbind('<Key>')
                aWidget.unbind('<FocusOut>')
                aWidget.unbind('<FocusIn>')

    def widgetEnable(self,aWidget, widgetType=None, holeNum = None):
        # for i in range(18):
            aWidget.config(state='normal')
            aWidget.bind('<Enter>', lambda event=Event: self._highlight(event))
#            aWidget.bind('<FocusIn>', lambda event=Event: self._highlight(event))
            aWidget.bind('<Leave>', lambda event=Event: self._removeHighlight(event))
#            aWidget.bind('<FocusOut>', lambda event=Event: self._removeHighlight(event))
            aWidget.bind('<Right>', lambda event=Event: self._goNext(event))
            aWidget.bind('<Left>', lambda event=Event: self._goPrevious(event))
            if widgetType == 'Score':
                aWidget.bind('<Key>', self._holeScore_check)
                aWidget.bind('<FocusOut>', lambda event=Event, holeNum=holeNum: self._checkHoleResult(event, holeNum))
                aWidget.bind('<FocusIn>', lambda event=Event: self._checkHoleEntry(event))
            if widgetType == 'SandTrap':
                aWidget.bind('<Key>', lambda event=Event, holeNum=holeNum: self._SandTrap_check(event, holeNum))
                aWidget.bind('<FocusOut>', lambda event=Event, holeNum=holeNum: self._checkSandTrapResult(event, holeNum))
                aWidget.bind('<FocusIn>', lambda event=Event, holeNum=holeNum: self._checkHoleEntrySandTrap(event))
                aWidget.bind('<Button-1>', lambda event=Event, holeNum=holeNum: self._flipSandTrapValue(event, holeNum, True))
                aWidget.bind('<Return>', lambda event=Event, holeNum=holeNum: self._flipSandTrapValue(event, holeNum))
            if widgetType == 'Putts':
                aWidget.bind('<Key>', lambda event=Event, holeNum=holeNum: self._putt_check(event, holeNum))
                aWidget.bind('<FocusOut>', lambda event=Event, holeNum=holeNum: self._checkPuttResult(event, holeNum))
                aWidget.bind('<FocusIn>', lambda event=Event, holeNum=holeNum: self._checkHoleEntry(event))
            if widgetType == 'Drive':
                aWidget.bind('<Key>', lambda event=Event, holeNum=holeNum: self._drive_check(event, holeNum))
                aWidget.bind('<FocusOut>', lambda event=Event, holeNum=holeNum: self._checkDriveResult(event, holeNum))
                aWidget.bind('<FocusIn>', lambda event=Event, holeNum=holeNum: self._checkHoleEntry(event))



    def clearGameDetails(self):
        for i in range(18):
            self.widgetClear(self.holeScore[i])
            self.widgetClear(self.holePutts[i])
            self.widgetClear(self.holeDrive[i])
            self.widgetClear(self.holeSandTrap[i])
            self.holeGreens[i].unSet()
            self.holeSandSaves[i].unSet()
            self.holeFairways[i].unSet()

        self.widgetClear(self.frontYardageTotal)
        self.widgetClear(self.yardageTotal)
        self.widgetClear(self.backYardageTotal)
        self.widgetClear(self.frontParTotal)
        self.widgetClear(self.parTotal)
        self.widgetClear(self.backParTotal)
        self.widgetClear(self.frontScoreTotal)
        self.widgetClear(self.scoreTotal)
        self.widgetClear(self.backScoreTotal)
        self.widgetClear(self.frontPuttsTotal)
        self.widgetClear(self.puttsTotal)
        self.widgetClear(self.backPuttsTotal)
        self.widgetClear(self.frontDriveTotal)
        self.widgetClear(self.backDriveTotal)
        self.widgetClear(self.driveTotal)
        self.widgetClear(self.frontSandTrapTotal)
        self.widgetClear(self.backSandTrapTotal)
        self.widgetClear(self.sandTrapTotal)
        self.widgetClear(self.scoreNetTotal)
        self._updateTotals()

    def clear(self):
        for i in range(18):
            self.widgetClear(self.holeHandicap[i])
            self.widgetClear(self.holeYardage[i])
            self.widgetClear(self.holePar[i])

        self.clearGameDetails()

    def widgetLoad(self, aWidget, Data):
         aState = aWidget.cget('state')
         aWidget.config(state='normal')
         aWidget.delete(0, END)
         aWidget.insert(0, Data)
         if aState != 'normal':
             aWidget.config(state='disabled')

    def widgetClear(self, aWidget):
         aState = aWidget.cget('state')
         aWidget.config(state='normal')
         aWidget.delete(0, END)
         if aState != 'normal':
             aWidget.config(state='disabled')

class AppBorderFrame(Frame):
    def __init__(self, parent, columnTotal):
        Frame.__init__(self, parent)
        self.config(border=AppDefaultBorderWidth, relief='ridge')
        self.row=0
        self.column=0
        self.columnTotal=columnTotal
        self.rowconfigure(self.row, weight=0)
        for i in range(self.columnTotal):
            self.columnconfigure(i, weight = 1)

    def noBorder(self):
        self.config(border=0)

    def addTitle(self, aTitle, aWidget = None, aWidget2=None):
        if aWidget == None and aWidget2 == None:
            self.frameTitle =  AppLineSectionTitle(self, aTitle)
            self.frameTitle.grid(row=self.row, column=self.column, columnspan=self.columnTotal, sticky=N)
            self.row = self.row + 1
        elif aWidget2 == None:
            self.frameTitle =  AppLineSectionTitle(self, aTitle)
            self.frameTitle.grid(row=self.row, column=self.column, columnspan=self.columnTotal, sticky=N)
            aWidget.grid(row=self.row, column=self.column, sticky = N+S+W)
            self.row = self.row + 1
        else:
            self.frameTitle = AppLineSectionTitle(self, aTitle)
            self.frameTitle.grid(row=self.row, column=self.column, columnspan=self.columnTotal, sticky=N)
            aWidget.grid(row=self.row, column=self.column, sticky=N + S + W)
            aWidget2.grid(row=self.row, column=self.column+1, sticky=N + S + W)
            self.row = self.row + 1

    def changeTitle(self, newTitle):
        self.frameTitle.configure(text=newTitle)

    def addRow(self):
        self.row = self.row + 1
        self.rowconfigure(self.row, weight=0)

    def addColumn(self):
        self.column = self.column + 1

    def noStretchColumn(self, columnNumber):
        self.columnconfigure(columnNumber, weight=0)

    def noStretchCurrentColumn(self):
        self.columnconfigure(self.column, weight=0)

    def stretchRow(self, rowNumber):
        self.rowconfigure(rowNumber, weight=10)

    def stretchCurrentRow(self):
        self.rowconfigure(self.row, weight=1)

    def stretchCurrentColumn(self):
        self.columnconfigure(self.column, weight=1)

    def stretchLargeCurrentColumn(self):
        self.columnconfigure(self.column, weight=4)

    def stretchSpecifyWeightCurrentColumn(self, aWeight):
        self.columnconfigure(self.column, weight=aWeight)

    def stretchSpecifyColumnAndWeight(self, aColumn, aWeight):
        self.columnconfigure(aColumn, weight=aWeight)

    def resetColumn(self):
        self.column = 0

    def removeBorder(self):
        self.config(border=0)

    def setBorderHighlight(self):
        self.config(relief = 'flat', border = 6, highlightthickness = 4,highlightbackground = AppDefaultForeground)

    def setSelectableFrame(self):
        self.config(background = AppDefaultBackground, highlightcolor = AppDefaultForeground,
                    highlightthickness = AppHighlightBorderWidth, takefocus = 1)
        self.bind('<Enter>', widgetEnter)

    def deselectSetFocus(self):
        self.config(takefocus=0)


class AppMENUListFrame(Frame):
    def __init__(self, parent, columnTotal):
        Frame.__init__(self, parent)
        self.config(border=AppDefaultBorderWidth, relief='ridge', highlightbackground='red')
        self.row=0
        self.column=0
        self.columnTotal=columnTotal
        for i in range(self.columnTotal):
            self.columnconfigure(i, weight = 1)

    def addRow(self):
        self.row = self.row + 1
        self.rowconfigure(self.row,  weight = 0)

    def addColumn(self):
        self.column = self.column + 1

    def resetColumn(self):
        self.column = 0

    def addMenuItem(self, CommandText, selectedMethod):
        self.addRow()
        anItem = AppMENUFrame(self, CommandText, selectedMethod)
        anItem.grid(row=self.row, column=self.column, sticky=N+E+W)

class AppMENUFrame(Frame):
    def __init__(self, parent, CommandText, selectedMethod):
        Frame.__init__(self, parent)
        original = Image.open(MenuImg)
        image1 = resizeImage(original, 30, 30)

        self.config(border=3, relief='flat', width=golferPicWidth, height=golferPicHeight,
                    highlightcolor=AppDefaultForeground, highlightthickness=AppHighlightBorderWidth, takefocus=1)
        self.bind('<Enter>', widgetEnter)

        self.bind('<Button-1>', selectedMethod)
        self.bind('<Enter>', widgetEnter)
        self.bind('<Return>', selectedMethod)
        #
        panel1 = ttk.Label(self, image=image1)
        panel1.bind('<Button-1>', selectedMethod)
        panel1.bind('<Enter>', self.focus())
        self.display = image1
        panel1.grid(row=0, column=0)
        aMsg = ttk.Label(self, text=CommandText, style='MenuItems.TLabel')
        aMsg.grid(row=0, column=1, sticky=N + W)
        aMsg.bind('<Button-1>', selectedMethod)
        aMsg.bind('<Enter>', self.focus())


class AppCmdLineButton(Frame):
    def __init__(self, parent, aText, anImage, aProc):
        Frame.__init__(self, parent)
        self.config(takefocus=0)
        self.aProc = aProc

        self.aFrame = Frame(self, highlightcolor=AppDefaultForeground,
                            highlightthickness=AppHighlightBorderWidth, takefocus=1)
        self.aFrame.grid(row=0, column=0, sticky=W + N)

        if aText != None:
            self.aButton = ttk.Button(self.aFrame, text=aText, style='CommandLineButton.TButton', command=self.aProc, takefocus=0)
            self.aButton.grid(row=0, column=0, sticky=N)
        elif anImage != None:
#            imageResized = resizeImage(Image.open(anImage), iconWidth, iconHeight)
            imageResized = resizeImage(Image.open(anImage), buttonImageSizeHeight, buttonImageSizeWidth)
            self.aButton = ttk.Button(self.aFrame, image=imageResized, command=self.aProc, style='CommandLineButton.TButton',takefocus=0)
            self.aButton.img = imageResized
            self.aButton.grid(row=0, column=0, sticky=N+S)
        self.enable()

    def focus(self):
        self.aFrame.focus()

    def enable(self):
        self.aButton.config(state='normal', takefocus=0)
        self.aFrame.config(takefocus=1)
        self.aFrame.bind('<Enter>', widgetEnter)
        self.aFrame.bind('<Return>', self.aProc)

    def disable(self):
        self.aButton.config(state='disabled', takefocus=0)
        self.aFrame.config(takefocus=0)
        self.aFrame.unbind('<Enter>')
        self.aFrame.unbind('<Return>')

    def addToolTip(self, aText):
        self.aButton_ttp = CreateToolTip(self.aButton, aText)

class AppStdButton(Frame):
    def __init__(self, parent, aText, anImage, aProc):
        Frame.__init__(self, parent)
        self.config(takefocus=0)
        self.aProc = aProc

        self.aFrame = Frame(self, highlightcolor=AppDefaultForeground,
                            highlightthickness=AppHighlightBorderWidth, takefocus=1)
        self.aFrame.grid(row=0, column=0, sticky=W + N)

        if aText != None:
            self.aButton = ttk.Button(self.aFrame, text=aText, style='CommandButton.TButton', command=self.aProc, takefocus=0)
            self.aButton.grid(row=0, column=0, sticky=N)
        elif anImage != None:
#            imageResized = resizeImage(Image.open(anImage), iconWidth, iconHeight)
            imageResized = resizeImage(Image.open(anImage), buttonImageSizeHeight, buttonImageSizeWidth)
            self.aButton = ttk.Button(self.aFrame, image=imageResized, command=self.aProc, style='CommandButton.TButton',takefocus=0)
            self.aButton.img = imageResized
            self.aButton.grid(row=0, column=0, sticky=N+S)
        self.enable()


    def focus(self):
        self.aFrame.focus()

    def enable(self):
        self.aButton.config(state='normal', takefocus=0)
        self.aFrame.config(takefocus=1)
        self.aFrame.bind('<Enter>', widgetEnter)
        self.aFrame.bind('<Return>', self.aProc)

    def disable(self):
        self.aButton.config(state='disabled', takefocus=0)
        self.aFrame.config(takefocus=0)
        self.aFrame.unbind('<Enter>')
        self.aFrame.unbind('<Return>')

    def addToolTip(self, aText):
        self.aButton_ttp = CreateToolTip(self.aButton, aText)

class GameEditButton(Frame):
    def __init__(self, parent, aText, anImage, aProc, aParameterList, anIndice):
        Frame.__init__(self, parent)
        self.config(takefocus=0)
        self.aProc = aProc

        if aText != None:
            # Button(self.frame, text="Show 9s", relief='ridge', justify=CENTER, bg=AppHighlightBackground,
            #                       fg=AppDefaultForeground, font=AppDefaultFontAvg,
            #                       command=self._display9HoleGames).grid(row=row + 2, column=12, columnspan=2, padx=4)

            self.aButton = Button(self, text=aText, justify=CENTER, font=AppDefaultFontAvg,relief='groove',
                                      command= lambda ID=aParameterList[anIndice]: self.aProc(ID), takefocus=0)
            self.aButton.grid(row=0, column=0, sticky=N+E+S+W)
        elif anImage != None:
            imageResized = resizeImage(Image.open(anImage), buttonImageSizeHeight, buttonImageSizeWidth)
            self.aButton = ttk.Button(self, image=imageResized,
                                      command= lambda ID=aParameterList[anIndice]: self.aProc(ID), takefocus=0)
            self.aButton.img = imageResized
            self.aButton.grid(row=0, column=0, sticky=N+S)
        self.enable()

    def enable(self):
        self.aButton.config(state='normal', takefocus=0)
        self.config(takefocus=1)
        self.bind('<Enter>', widgetEnter)
        self.bind('<Return>', self.aProc)

    def disable(self):
        self.aButton.config(state='disabled', takefocus=0)
        self.config(takefocus=0)
        self.unbind('<Enter>')
        self.unbind('<Return>')

    def addToolTip(self, aText):
        self.aButton_ttp = CreateToolTip(self.aButton, aText)


class AppFieldEntryInteger(Tix.Frame):
    def __init__(self, parent, aLabel, orientation, aWidth, aDirtyMethod, aFocusOutMethod=None):
        self.myMsgBar = None
        self.aFocusOutMethod = aFocusOutMethod
        self.aWidth = aWidth
        Tix.Frame.__init__(self, parent)
        self.parent = parent
        self.aDirtyMethod = aDirtyMethod
        self.currency = True
        self.config(takefocus=0)
        myRow = 0
        myColumn = 0
        self.totalDigits = AppDefaultAmountTotalDigit  # default Value

        if aLabel != None:
            if orientation == 'H':
                aLabel = ttk.Label(self, text='{0}: '.format(aLabel), takefocus=0, style='RegularFieldTitle.TLabel')
                aLabel.grid(row=myRow, column=myColumn, sticky=E + N)
                myRow = 0
                myColumn = 1
            elif orientation == 'V':
                aLabel = ttk.Label(self, text='{0}'.format(aLabel), takefocus=0, style='RegularFieldTitle.TLabel')
                aLabel.grid(row=myRow, column=myColumn, sticky=N)
                myRow = 1
                myColumn = 0

        self.anEntry = Tix.Entry(self, highlightcolor=AppDefaultForeground, highlightthickness=AppHighlightBorderWidth,
                                 width=aWidth, font=fontBig,
                                 background=AppDefaultEditBackground, disabledforeground=AppDefaultForeground)
        self.anEntry.grid(row=myRow, column=myColumn, sticky=N + S)

    def removeFocusOutMethod(self):
        self.aFocusOutMethod = None

    def setAsDeleted(self):
        self.anEntry.config(disabledforeground=AppDefaultDisabledForeground)

    def resetAsDeleted(self):
        self.anEntry.config(disabledforeground=AppDefaultForeground)

    def setAsRequiredField(self):
        self.anEntry.config(background=AppDefaultRequiredFieldBackground)

    def resetAsRequiredField(self):
        self.anEntry.config(background=AppDefaultEditBackground)

    def justification(self, aValue):
        self.anEntry.config(justify=aValue)

    def focus(self):
        self.anEntry.focus()

    def disable(self):
        if self.anEntry.cget('state') != 'disabled':
            theCurrentValue = self.anEntry.get()
            self.anEntry.config(state='disabled', validate="none")
            self.anEntry.unbind('<Enter>')
            self.anEntry.unbind('<Key>')
            if self.aFocusOutMethod != None:
                self.anEntry.unbind('<FocusOut>')
            self.load(theCurrentValue)

    def clear(self):
        aState = self.anEntry.cget('state')
        self.anEntry.config(state='normal')
        self.anEntry.delete(0, END)
        if aState != 'normal':
            self.anEntry.config(state='disabled')

    def getAsString(self):
        # used for searches
        return self.anEntry.get()

    def get(self):
        amnt = self.anEntry.get()
        try:
            amnt2 = amnt.strip('$ ')
            if len(amnt2) > 0:
                return int(amnt2)
            else:
                return None
        except:
            return 'ERROR'

    def load(self, aValue):
        aState = self.anEntry.cget('state')
        # Ensure that the next operation is done in Disabled mode.
        if aState != 'disabled':
            self.disable()
        try:
            int(aValue)
            amount = int(aValue)
        except:
            amount=''

        self.anEntry.configure(state='normal')
        self.anEntry.delete(0, END)
        self.anEntry.insert(0, "{0}".format(amount))

        self.anEntry.configure(state='disabled')

        # return to the entry state when entering the load
        if aState != 'disabled':
            self.enable()

    def enable(self):
        if self.anEntry.cget('state') != 'normal':
            amnt = self.anEntry.get()
            self.anEntry.config(state='normal')
            self.anEntry.delete(0, END)
            self.anEntry.insert(0, amnt)
            self.anEntry.config(validate="all")
            self.anEntry.bind('<Enter>', widgetEnter)
            self.anEntry.bind('<Key>', self.aDirtyMethod)
            if self.aFocusOutMethod != None:
                self.anEntry.bind('<FocusOut>', self.aFocusOutMethod)

    def addVariable(self, aVar):
        self.anEntry.config(textvariable=aVar)

    def setAsFloat(self):
        self.currency = False

    def _validateAmount(self, action, index, valueIfAllowed, valueBeforeEntry, textInvolved, validateOption,
                        callbackMethod, widgetName):
        pattern = "{0}".format("^[1-9]")

        num_format = re.compile(pattern)
        isnumber = True
        if callbackMethod == 'focusout':
            if len(valueIfAllowed) > 0:
                isnumber = re.match(num_format, valueIfAllowed)
                if len(valueIfAllowed) > 3:
                    isnumber = False
            else:
                return True
        elif callbackMethod == 'key' and action == '1':
            if textInvolved.isdigit() == True:
                isnumber = re.match(num_format, valueIfAllowed)
                if isnumber:
                    if len(valueIfAllowed) > 3:
                        aMsg = 'ERROR: Only {0} digits allowed (1-999).'.format(self.totalDigits)
                        self.myMsgBar.newMessage('error', aMsg)
                        return False
                    else:
                        return True
                else:
                    aMsg = 'ERROR: Invalid amount format, only {0} digits allowed (1-999).'.format(self.totalDigits)
                    self.myMsgBar.newMessage('error', aMsg)
                    return False
            else:
                aMsg = 'ERROR: Only digits are allowed (integer numbers).'
                self.myMsgBar.newMessage('error', aMsg)
                return False

        if isnumber:
            return True
        else:
            aMsg = 'ERROR: Invalid amount format, only {0} digits (1-999).'.format(self.totalDigits)
            self.myMsgBar.newMessage('error', aMsg)
            return False

    def addValidationAmount(self, totalDigits, myMsgBar):
        self.totalDigits = totalDigits
        self.myMsgBar = myMsgBar
        vcmd = (self.parent.register(self._validateAmount),
                '%d', '%i', '%P', '%s', '%S', '%v', '%V', '%W')
        self.anEntry.config(validate="none", validatecommand=vcmd)

class AppFieldEntryFloat(Tix.Frame):
    def __init__(self, parent, aLabel, orientation, aWidth, aDirtyMethod, aFocusOutMethod=None):
        self.myMsgBar = None
        self.aFocusOutMethod = aFocusOutMethod
        self.aWidth = aWidth
        Tix.Frame.__init__(self, parent)
        self.parent = parent
        self.aDirtyMethod = aDirtyMethod
        self.currency = True
        self.config(takefocus=0)
        myRow = 0
        myColumn = 0
        self.decimal = AppDefaultAmountDecimal     # default Value
        self.totalDigits = AppDefaultAmountTotalDigit  # default Value

        if aLabel != None:
            if orientation == 'H':
                aLabel = ttk.Label(self, text='{0}: '.format(aLabel), takefocus=0, style='RegularFieldTitle.TLabel')
                aLabel.grid(row=myRow, column=myColumn, sticky=E + N)
                myRow = 0
                myColumn = 1
            elif orientation == 'V':
                aLabel = ttk.Label(self, text='{0}'.format(aLabel), takefocus=0, style='RegularFieldTitle.TLabel')
                aLabel.grid(row=myRow, column=myColumn, sticky=N)
                myRow = 1
                myColumn = 0

        self.anEntry = Tix.Entry(self, highlightcolor=AppDefaultForeground, highlightthickness=AppHighlightBorderWidth,
                                 width=aWidth, font=fontBig,
                                 background=AppDefaultEditBackground, disabledforeground=AppDefaultForeground)
        self.anEntry.grid(row=myRow, column=myColumn, sticky=N + S)

    def removeFocusOutMethod(self):
        self.aFocusOutMethod = None

    def setAsDeleted(self):
        self.anEntry.config(disabledforeground=AppDefaultDisabledForeground)

    def resetAsDeleted(self):
        self.anEntry.config(disabledforeground=AppDefaultForeground)

    def setAsRequiredField(self):
        self.anEntry.config(background=AppDefaultRequiredFieldBackground)

    def resetAsRequiredField(self):
        self.anEntry.config(background=AppDefaultEditBackground)

    def justification(self, aValue):
        self.anEntry.config(justify=aValue)

    def focus(self):
        self.anEntry.focus()

    def disable(self):
        theCurrentValue = self.anEntry.get()
        self.anEntry.config(state='disabled', validate="none")
        self.anEntry.unbind('<Enter>')
        self.anEntry.unbind('<Key>')
        if self.aFocusOutMethod != None:
            self.anEntry.unbind('<FocusOut>')
        self.load(theCurrentValue)

    def clear(self):
        aState = self.anEntry.cget('state')
        self.anEntry.config(state='normal')
        self.anEntry.delete(0, END)
        if aState != 'normal':
            self.anEntry.config(state='disabled')

    def getAsString(self):
        # used for searches
        amnt = self.anEntry.get()
        amnt2 = amnt.strip('$ ')
        return amnt2

    def get(self):
        amnt = self.anEntry.get()
        try:
            amnt2 = amnt.strip('$ ')
            if len(amnt2) > 0:
                return float(amnt2)
            else:
                return None
        except:
            return 'ERROR'

    def load(self, aValue):
        aState = self.anEntry.cget('state')
        # Ensure that the next operation is done in Disabled mode.
        if aState != 'disabled':
            self.disable()
        try:
            float(aValue)
            amount = float(aValue)
        except:
            amount = None

        self.anEntry.configure(state='normal')
        self.anEntry.delete(0, END)
        if amount != None:
            if self.currency==True:
                self.anEntry.insert(0, "$ {0}".format(round(amount, self.decimal)))
            else:
                self.anEntry.insert(0, "{0}".format(round(amount, self.decimal)))

        self.anEntry.configure(state='disabled')

        # return to the entry state when entering the load
        if aState != 'disabled':
            self.enable()

    def enable(self):
        if self.anEntry.cget('state') != 'normal':
            amnt = self.anEntry.get()
            if self.currency == True:
                amnt2 = amnt.strip('$ ')
            else:
                amnt2 = amnt

            self.anEntry.config(state='normal')
            self.anEntry.delete(0, END)
            self.anEntry.insert(0, amnt2)
            self.anEntry.config(validate="all")
            self.anEntry.bind('<Enter>', widgetEnter)
            self.anEntry.bind('<Key>', self.aDirtyMethod)
            if self.aFocusOutMethod != None:
                self.anEntry.bind('<FocusOut>', self.aFocusOutMethod)

    def addVariable(self, aVar):
        self.anEntry.config(textvariable=aVar)

    def setAsFloat(self):
        self.currency = False

    def _validateAmount(self, action, index, valueIfAllowed, valueBeforeEntry, textInvolved, validateOption,
                        callbackMethod, widgetName):
        pattern = "{0}{1}{2}{3}{4}".format("^[\-]?[0-9]{,", str(int(self.totalDigits) - int(self.decimal)), "}\.?[0-9]{,",
                                           self.decimal, "}$")
        num_format = re.compile(pattern)
        isnumber = True
        if callbackMethod == 'focusout':
            if len(valueIfAllowed) > 0:
                isnumber = re.match(num_format, valueIfAllowed)
            else:
                return True
        elif callbackMethod == 'key' and action == '1':
            if textInvolved == '-':
                num_format = re.compile("^[\-]$")
                isnumber = re.match(num_format, valueIfAllowed)
                if isnumber:
                    return True
                else:
                    aMsg = 'ERROR: Minus sign valid only in first position.'
                    self.myMsgBar.newMessage('error', aMsg)
                    return False
            elif textInvolved == '.':
                isnumber = re.match(num_format, valueIfAllowed)
                if isnumber:
                    return True
                else:
                    aMsg = 'ERROR: Invalid amount format after dot is entered.'
                    self.myMsgBar.newMessage('error', aMsg)
                    return False
            elif textInvolved.isdigit() == True:
                isnumber = re.match(num_format, valueIfAllowed)
                if isnumber:
                    return True
                else:
                    aMsg = 'ERROR: Invalid amount format ({0} digits after the decimal point, {1} total digits).'.format(
                        self.decimal, self.totalDigits)
                    self.myMsgBar.newMessage('error', aMsg)
                    return False
            else:
                aMsg = 'ERROR: Only digits, a minus sign or a dot are allowed.'
                self.myMsgBar.newMessage('error', aMsg)
                return False

        if isnumber:
            return True
        else:
            aMsg = 'ERROR: Invalid amount format ({0} digits after the decimal point, {1} total digits).'.format(self.decimal, self.totalDigits)
            self.myMsgBar.newMessage('error', aMsg)
            return False

    def addValidationAmount(self, decimal, totalDigits, myMsgBar):
        self.decimal = decimal
        self.totalDigits = totalDigits
        self.myMsgBar = myMsgBar
        vcmd = (self.parent.register(self._validateAmount),
                '%d', '%i', '%P', '%s', '%S', '%v', '%V', '%W')
        self.anEntry.config(validate="none", validatecommand=vcmd)

class AppFieldEntryDateV2(Tix.Frame):
    def __init__(self, parent, aLabel, orientation, aWidth, aProc, myMsgBar, aDirtyMethod=None):
        #
        #  aProc is a procedure to execute if item is not in the list.  If no
        # procedure is passed to the class, no action is taken.
        #
        Tix.Frame.__init__(self, parent)
        self.myMsgBar = myMsgBar
        self.aDirtyMethod = aDirtyMethod
        self.config(pady=offsetYFieldEntry,takefocus=0)
        myRow = 0
        myColumn = 0
        self.dateVar = Tix.StringVar()
        self._dateEnteredActionProc = aProc

        if aLabel != None:
            if orientation == 'H':
                self.aLabel = ttk.Label(self, text='{0}: '.format(aLabel), takefocus=0, style='RegularFieldTitle.TLabel')
                self.aLabel.grid(row=myRow, column=myColumn, sticky=E + N + S)
                myRow = 0
                myColumn = 1
            elif orientation == 'V':
                self.aLabel = ttk.Label(self, text='{0}'.format(aLabel), takefocus=0, style='RegularFieldTitle.TLabel')
                self.aLabel.grid(row=myRow, column=myColumn, sticky=N + S)
                myRow = 1
                myColumn = 0
            else:
                self.aLabel = ttk.Label(self, text='{0}'.format(aLabel), takefocus=0, style='RegularFieldTitle.TLabel')


        self.aFrame = Frame(self, highlightcolor=AppDefaultForeground,
                            highlightthickness=AppHighlightBorderWidth, takefocus=0)
#        self.aFrame = Frame(self, border=0, highlightcolor=AppDefaultForeground,
#                            highlightthickness=AppHighlightBorderWidth, takefocus=1)
        #        aFrame=Frame(self, takefocus=0)
        if orientation == 'H':
            self.aFrame.grid(row=myRow, column=myColumn, sticky=W + N)
        else:
            self.aFrame.grid(row=myRow, column=myColumn, sticky=N)

#        self.anEntry = AppACListEntry(self.aFrame, topFrame, aList, aWidth, aProc, self.myMsgBar)

        self.anEntry = Entry(self.aFrame, takefocus=1, font=fontBig, background=AppDefaultEditBackground,
                             disabledforeground=AppDefaultForeground, width=aWidth, textvariable=self.dateVar,
                             justify=CENTER)

        self.anEntry.grid(row=0, column=0, sticky=N)

        parseDate = parser.parse(self.dateVar.get())
        editImg = resizeImage(Image.open(editRecImage), iconWidth, iconHeight)
        self.editProcButton = ttk.Button(self.aFrame, image=editImg, style='ComboBoxButton.TButton', takefocus=0,
                                 command=lambda: fnCalendarSet(self.dateVar, parseDate.year,
                                                               parseDate.month, parseDate.day))
        self.editProcButton.img = editImg
        self.editProcButton.grid(row=0, column=1, sticky=N + S)
        self.editProcButton_ttp = CreateToolTip(self.editProcButton, "Change Date")
        if aProc != None:
            self.setVarAction()
        self.enable()

    def setSmallFont(self):
        self.aLabel.configure(style='SmallFieldTitle.TLabel')
        self.editProcButton.configure(style='smallButton.TButton')
        self.anEntry.configure(font=fontAverage)

    def stretchColumn(self):
        self.columnconfigure(0, weight=1)

    def setAsRequiredField(self):
        self.anEntry.config(background=AppDefaultRequiredFieldBackground)

    def resetAsRequiredField(self):
        self.anEntry.config(background=AppDefaultEditBackground)

    def setVarAction(self):
        self.dateVar.trace('w', self._dateEnteredActionProc)

    def focus(self):
        self.anEntry.focus()

    def disable(self):
        self.anEntry.config(state='disabled', takefocus=0)
        self.editProcButton.configure(state='disabled')
        self.anEntry.unbind('<Enter>')
        self.anEntry.unbind('<FocusOut>')
        self.anEntry.unbind('<Key>')

    def clear(self):
        aState = self.anEntry.cget('state')
        self.anEntry.config(state='normal')
        self.anEntry.delete(0, END)
        if aState != 'normal':
            self.anEntry.config(state='disabled')

    def get(self):
            return self.anEntry.get()

    def load(self, aValue):
        if aValue == None or aValue == 'N':
            pass
        else:
            aState = self.anEntry.cget('state')
            self.anEntry.config(state='normal')
            self.anEntry.delete(0, END)
            self.anEntry.insert(0, aValue)
            if aState != 'normal':
                self.anEntry.config(state='disabled')
            self.aDirtyMethod(None)

    def enable(self):
        self.anEntry.config(state='normal', takefocus=1)
        self.editProcButton.configure(state='normal')
        self.anEntry.bind('<Enter>', widgetEnter)
        self.anEntry.bind('<FocusOut>', self._check)
        self.anEntry.bind('<Key>', self.aDirtyMethod)

    def _check(self, event):
        try:
            parsed = parser.parse(self.get())
        except:
            aMsg = "ERROR: Invalid date format."
            self.myMsgBar.newMessage('error', aMsg)
            self.focus()

class AppFieldEntryDate(Tix.Frame):
    def __init__(self, parent, aLabel, orientation, aWidth, aProc, myMsgBar, aDirtyMethod=None):
        #
        #  aProc is a procedure to execute if item is not in the list.  If no
        # procedure is passed to the class, no action is taken.
        #
        Tix.Frame.__init__(self, parent)
        self.myMsgBar = myMsgBar
        self.aDirtyMethod = aDirtyMethod
        self.config(pady=offsetYFieldEntry,takefocus=0)
        myRow = 0
        myColumn = 0
        self.dateVar = Tix.StringVar()
        self._dateEnteredActionProc = aProc

        if aLabel != None:
            if orientation == 'H':
                aLabel = ttk.Label(self, text='{0}: '.format(aLabel), takefocus=0, style='RegularFieldTitle.TLabel')
                aLabel.grid(row=myRow, column=myColumn, sticky=E + N + S)
                myRow = 0
                myColumn = 1
            else:
                aLabel = ttk.Label(self, text='{0}'.format(aLabel), takefocus=0, style='RegularFieldTitle.TLabel')
                aLabel.grid(row=myRow, column=myColumn, sticky=N + S)
                myRow = 1
                myColumn = 0

        self.aFrame = Frame(self, highlightcolor=AppDefaultForeground,
                            highlightthickness=AppHighlightBorderWidth, takefocus=0)
#        self.aFrame = Frame(self, border=0, highlightcolor=AppDefaultForeground,
#                            highlightthickness=AppHighlightBorderWidth, takefocus=1)
        #        aFrame=Frame(self, takefocus=0)
        if orientation == 'H':
            self.aFrame.grid(row=myRow, column=myColumn, sticky=W + N)
        else:
            self.aFrame.grid(row=myRow, column=myColumn, sticky=N)

#        self.anEntry = AppACListEntry(self.aFrame, topFrame, aList, aWidth, aProc, self.myMsgBar)

        self.anEntry = Entry(self.aFrame, takefocus=1, font=fontBig, background=AppDefaultEditBackground,
                             disabledforeground=AppDefaultForeground, width=aWidth, textvariable=self.dateVar,
                             justify=CENTER)

        self.anEntry.grid(row=0, column=0, sticky=N)

        parseDate = parser.parse(self.dateVar.get())
        editImg = resizeImage(Image.open(editRecImage), iconWidth, iconHeight)
        self.editProcButton = ttk.Button(self.aFrame, image=editImg, style='ComboBoxButton.TButton', takefocus=0,
                                 command=lambda: fnCalendarSet(self.dateVar, parseDate.year,
                                                               parseDate.month, parseDate.day))
        self.editProcButton.img = editImg
        self.editProcButton.grid(row=0, column=1, sticky=N + S)
        self.editProcButton_ttp = CreateToolTip(self.editProcButton, "Change Date")
        if aProc != None:
            self.setVarAction()
        self.enable()

    def justifyLeft(self):
        self.anEntry.config(justify=LEFT)

    def setAsRequiredField(self):
        self.anEntry.config(background=AppDefaultRequiredFieldBackground)

    def resetAsRequiredField(self):
        self.anEntry.config(background=AppDefaultEditBackground)

    def setVarAction(self):
        self.dateVar.trace('w', self._dateEnteredActionProc)

    def focus(self):
        self.anEntry.focus()

    def disable(self):
        self.anEntry.config(state='disabled')
        self.editProcButton.configure(state='disabled')
        self.anEntry.unbind('<Enter>')
        self.anEntry.unbind('<FocusOut>')
        self.anEntry.unbind('<Key>')

    def clear(self):
        aState = self.anEntry.cget('state')
        self.anEntry.config(state='normal')
        self.anEntry.delete(0, END)
        if aState != 'normal':
            self.anEntry.config(state='disabled')

    def get(self):
            return self.anEntry.get()

    def load(self, aValue):
        if aValue == None or aValue == 'N':
            pass
        else:
            aState = self.anEntry.cget('state')
            self.anEntry.config(state='normal')
            self.anEntry.delete(0, END)
            self.anEntry.insert(0, aValue)
            if aState != 'normal':
                self.anEntry.config(state='disabled')
            self.aDirtyMethod(None)

    def enable(self):
        self.anEntry.config(state='normal', takefocus=1)
        self.editProcButton.configure(state='normal')
        self.anEntry.bind('<Enter>', widgetEnter)
        self.anEntry.bind('<FocusOut>', self._check)
        self.anEntry.bind('<Key>', self.aDirtyMethod)

    def _check(self, event):
        try:
            parsed = parser.parse(self.get())
        except:
            aMsg = "ERROR: Invalid date format."
            self.myMsgBar.newMessage('error', aMsg)
            self.focus()

class AppFieldEntryPhone(Tix.Frame):
    def __init__(self, parent, aLabel, orientation, aWidth, aDirtyMethod=None):
        self.myMsgBar = None
        self.aWidth = aWidth
        self.aDirtyMethod = aDirtyMethod
        Tix.Frame.__init__(self, parent)
        self.parent = parent
        self.config(takefocus=0)
        myRow = 0
        myColumn = 0

        if aLabel != None:
            if orientation == 'H':
                aLabel = ttk.Label(self, text='{0}: '.format(aLabel), takefocus=0, style='RegularFieldTitle.TLabel')
                aLabel.grid(row=myRow, column=myColumn, sticky=E + N)
                myRow = 0
                myColumn = 1
            elif orientation == 'V':
                aLabel = ttk.Label(self, text='{0}'.format(aLabel), takefocus=0, style='RegularFieldTitle.TLabel')
                aLabel.grid(row=myRow, column=myColumn, sticky=N)
                myRow = 1
                myColumn = 0

        self.anEntry = Tix.Entry(self, highlightcolor=AppDefaultForeground, highlightthickness=AppHighlightBorderWidth,
                                 width=aWidth, font=fontBig,
                                 background=AppDefaultEditBackground, disabledforeground=AppDefaultForeground)
        self.anEntry.grid(row=myRow, column=myColumn, sticky=N + S)

        self.fullVerification = True
        self.extensionVerification = False  # default is full verification

    def setAsDeleted(self):
        self.anEntry.config(disabledforeground=AppDefaultDisabledForeground)

    def resetAsDeleted(self):
        self.anEntry.config(disabledforeground=AppDefaultForeground)

    def setAsRequiredField(self):
        self.anEntry.config(background=AppDefaultRequiredFieldBackground)

    def resetAsRequiredField(self):
        self.anEntry.config(background=AppDefaultEditBackground)

    def justification(self, aValue):
        self.anEntry.config(justify=aValue)

    def focus(self):
        self.anEntry.focus()

    def disable(self):
        self.anEntry.config(state='disabled', validate="none")
        self.anEntry.unbind('<Enter>')
        self.anEntry.unbind('<Key>')

    def clear(self):
        self.anEntry.config(state='normal')
        self.anEntry.delete(0, END)
        self.anEntry.config(state='disabled')

    def removeFormatting(self, aPhoneStringFormatted):

        charToStrip = "( )-.#"
        for i in range(0, len(charToStrip)):
            aPhoneStringFormatted = aPhoneStringFormatted.replace(charToStrip[i], "")
        return aPhoneStringFormatted

    def get(self):
        aPhoneString = self.removeFormatting(self.anEntry.get())
        return aPhoneString

    def formatForDisplay(self, aValue):
        if self.fullVerification:
            displayNumber = "({0}) {1}-{2}".format(aValue[:3], aValue[3:6], aValue[-4:])
        else:
            displayNumber = aValue
        return displayNumber

    def load(self, aValue):
        try:
            if len(aValue) > 0:
                displayNumber = self.formatForDisplay(aValue)
                self.anEntry.config(state='normal')
                self.anEntry.delete(0, END)
                self.anEntry.insert(0, displayNumber)
                self.anEntry.config(state='disabled')
                self.aDirtyMethod(None)
        except:
            pass  # error in phone format. stored as a integer number

    def enable(self):
        self.anEntry.config(state='normal')
        self.anEntry.config(validate="focusout")
        self.anEntry.bind('<Enter>', widgetEnter)
        self.anEntry.bind('<Key>', self.aDirtyMethod)

    def addVariable(self, aVar):
        self.anEntry.config(textvariable=aVar)

    def _validateAmount(self, action, index, valueIfAllowed, valueBeforeEntry, textInvolved, validateOption,
                        callbackMethod, widgetName):
        if len(valueIfAllowed) > 0:  # Allow an empty field
            if callbackMethod == 'focusout':
                #
                #  Next three line is in case user return to
                #  change.  Must override the formatting that
                #  is being forced.
                #
                aStr = valueIfAllowed
                #
                #  Remove any formating characters
                #
                aStr = self.removeFormatting(aStr)
                #                charToStrip = "( )-"
                #                for i in range(0,len(charToStrip)):
                #                    aStr = aStr.replace(charToStrip[i],"")
                if self.fullVerification:
                    phonePattern = re.compile(r'''
                                                ^           # Start of string
                                                (\d{3})     # area code is 3 digits (e.g. '800')
                                                \D*         # optional separator is any number of non-digits
                                                (\d{3})     # trunk is 3 digits (e.g. '555')
                                                \D*         # optional separator
                                                (\d{4})     # rest of number is 4 digits (e.g. '1212')
                                                $           # end of string
                                                ''', re.VERBOSE)
                else:
                    phonePattern = re.compile(r'''
                                                ^           # Start of string
                                                (\d{4})     # 4 digits (e.g. '1212')
                                                $           # end of string
                                                ''', re.VERBOSE)

                # phonePattern = re.compile(r'''
                #                                                        # don't match beginning of string, number can start anywhere
                #                                            (\d{3})     # area code is 3 digits (e.g. '800')
                #                                            \D*         # optional separator is any number of non-digits
                #                                            (\d{3})     # trunk is 3 digits (e.g. '555')
                #                                            \D*         # optional separator
                #                                            (\d{4})     # rest of number is 4 digits (e.g. '1212')
                #                                            $           # end of string
                #                                            \D*         # optional separator
                #                                            (\d*)       # extension is optional and can be any number of digits
                #                                            ''', re.VERBOSE)
                result = phonePattern.search(aStr)
                if result:
                    showThis = self.formatForDisplay(result.group())
                    self.anEntry.delete(0, END)
                    self.anEntry.insert(0, showThis)
                    return True
                else:
                    if self.fullVerification:
                        aMsg = 'ERROR: Invalid phone number format <(ddd) ddd-dddd>.'
                    else:
                        aMsg = 'ERROR: Invalid phone number format <dddd>.'
                    self.myMsgBar.newMessage('error', aMsg)
                    self.anEntry.focus()
                    return False
            return True  # no action while entering. focus verifies the format
        else:
            return True

    def setFullPhoneVerification(self):
        self.fullVerification = True
        self.extensionVerification = False

    def setExtensionVerification(self):
        self.fullVerification = False
        self.extensionVerification = True

    def addValidation(self, myMsgBar):
        self.myMsgBar = myMsgBar
        vcmd = (self.parent.register(self._validateAmount),
                '%d', '%i', '%P', '%s', '%S', '%v', '%V', '%W')
        self.anEntry.config(validate="none", validatecommand=vcmd)

class AppFieldEntry(Tix.Frame):
    def __init__(self, parent, aLabel, orientation, aWidth, aDirtyMethod=None):
        self.myMsgBar = None
        self.aWidth = aWidth
        Tix.Frame.__init__(self, parent)
        self.parent = parent
        self.config(takefocus=0)
        myRow = 0
        myColumn = 0
        self.isScaEntry = False
        self.aDirtyMethod = aDirtyMethod

        if aLabel != None:
            if orientation == 'H':
                aLabel = ttk.Label(self, text='{0}: '.format(aLabel), takefocus=0, style='RegularFieldTitle.TLabel')
                aLabel.grid(row=myRow, column=myColumn, sticky=E + N)
                myRow = 0
                myColumn = 1
            elif orientation == 'V':
                aLabel = ttk.Label(self, text='{0}'.format(aLabel), takefocus=0, style='RegularFieldTitle.TLabel')
                aLabel.grid(row=myRow, column=myColumn, sticky=N)
                myRow = 1
                myColumn = 0

        self.anEntry = Tix.Entry(self, highlightcolor=AppDefaultForeground, highlightthickness=AppHighlightBorderWidth,
                                 width=aWidth, font=fontBig,
                                 background=AppDefaultEditBackground, disabledforeground=AppDefaultForeground)
        self.anEntry.grid(row=myRow, column=myColumn, sticky=N + S)

    def setAsScaEntry(self):
        self.isScaEntry = True

    def setAsRequiredField(self):
        self.anEntry.config(background=AppDefaultRequiredFieldBackground)

    def resetAsRequiredField(self):
        self.anEntry.config(background=AppDefaultEditBackground)

    def setAsDeleted(self):
        self.anEntry.config(disabledforeground=AppDefaultDisabledForeground)

    def resetAsDeleted(self):
        self.anEntry.config(disabledforeground=AppDefaultForeground)

    def justification(self, aValue):
        self.anEntry.config(justify=aValue)

    def focus(self):
        self.anEntry.focus()

    def disable(self):
        self.anEntry.config(state='disabled')
        self.anEntry.unbind('<Enter>')
        self.anEntry.unbind('<Key>')
        self.anEntry.config(takefocus=0)

    def clear(self):
        aState = self.anEntry.cget('state')
        self.anEntry.config(state='normal')
        self.anEntry.delete(0, END)
        if aState != 'normal':
            self.anEntry.config(state='disabled')

    def get(self):
        if self.isScaEntry == True:
            aStr = self.anEntry.get()
            return aStr[2:4]
        else:
            return self.anEntry.get()

    def load(self, aValue):
        aState = self.anEntry.cget('state')
        if aValue == None:
            aValue = ''
        elif self.isScaEntry == True:
            aValue = "A0{0} 3600".format(aValue)

        self.anEntry.config(state='normal')
        self.anEntry.delete(0, END)
        self.anEntry.insert(0, aValue)
        if aState != 'normal':
            self.anEntry.config(state='disabled')

    def enable(self):
        self.anEntry.config(state='normal')
        self.anEntry.bind('<Enter>', widgetEnter)
        self.anEntry.bind('<Key>', self.aDirtyMethod)
        self.anEntry.config(takefocus=1)

    def addVariable(self, aVar):
        self.anEntry.config(textvariable=aVar)

class AutocompleteCombobox(ttk.Combobox):
    def set_completion_list(self, completion_list):
        """Use our completion list as our drop down selection menu, arrows move through menu."""
        self._completion_list = sorted(completion_list, key=str.lower)  # Work with a sorted list
        self._hits = []
        self._hit_index = 0
        self.position = 0
        self.bind('<KeyRelease>', self.handle_keyrelease)
        self['values'] = self._completion_list  # Setup our popup menu

    def autocomplete(self, delta=0):
        """autocomplete the Combobox, delta may be 0/1/-1 to cycle through possible hits"""
        if delta:  # need to delete selection otherwise we would fix the current position
            self.delete(self.position, END)
        else:  # set position to end so selection starts where textentry ended
            self.position = len(self.get())
        # collect hits
        _hits = []
        for element in self._completion_list:
            if element.lower().startswith(self.get().lower()):  # Match case insensitively
                _hits.append(element)
        # if we have a new hit list, keep this in mind
        if _hits != self._hits:
            self._hit_index = 0
            self._hits = _hits
        # only allow cycling if we are in a known hit list
        if _hits == self._hits and self._hits:
            self._hit_index = (self._hit_index + delta) % len(self._hits)
        # now finally perform the auto completion
        if self._hits:
            self.delete(0, END)
            self.insert(0, self._hits[self._hit_index])
            self.select_range(self.position, END)

    def handle_keyrelease(self, event):
        """event handler for the keyrelease event on this widget"""
        if event.keysym == "BackSpace":
            self.delete(self.index(INSERT), END)
            self.position = self.index(END)
        if event.keysym == "Left":
            if self.position < self.index(END):  # delete the selection
                self.delete(self.position, END)
            else:
                self.position = self.position - 1  # delete one character
                self.delete(self.position, END)
        if event.keysym == "Right":
            self.position = self.index(END)  # go to end (no selection)
        if len(event.keysym) == 1:
            self.autocomplete()
            # No need for up/down, we'll jump to the popup
            # list at the position of the autocompletion


class AppACListEntry(Entry):
    #
    #   Special List Box Entry
    #      1. present a listbox with choices
    #      2. can be configure to execute an add script if choice is not present in list
    #      3. It will force to select from list otherwise
    #      4. In order to open listbox at correct place, parent's parent must be provided
    #      5. Popup list removes unmatched items (anywhere, not case sensitive)
    #
    #
    def __init__(self, parent, parentof, lista, fieldWidth, addItemProc, myMsgBar, aDirtyMethod, focusOutActionRequired, *args, **kwargs):
        self.fieldWidth = fieldWidth
        self.addItemProc = addItemProc
        self.focusOutActionRequired = focusOutActionRequired
        self.aDirtyMethod = aDirtyMethod
        Entry.__init__(self, parent, *args, **kwargs)
        self.parent = parent
        self.parentof = parentof
        self.myMsgBar = myMsgBar
        self.search = True
        self.traceProc = None
        self.trace = False
        self.smallFont=False

        self.config(takefocus=1, font=fontBig, background=AppDefaultEditBackground,
                    disabledforeground=AppDefaultForeground, width=self.fieldWidth,
                    justify=LEFT)

        #        self.config(highlightcolor=AppDefaultForeground, highlightthickness = AppHighlightBorderWidth, width=self.fieldWidth,
        #                    justify=LEFT, font=fontBig, disabledforeground=AppDefaultForeground)
        #        self.bind('<Enter>', self.enter)
        #        self.bind('<Return>', self.enterKeyEntered)
        #        self.bind('<FocusOut>', self.check)
        self.lista = lista
        self.var = self["textvariable"]
        if self.var == '':
            self.var = self["textvariable"] = StringVar()
        self.var.trace("w", self.changed)
        self.config(state='disabled')
        #        self.bind("<ButtonRelease-1>", self.selectItem)
        #        self.bind("<Right>", self.enterKeyEntered)
        #        self.bind("<Return>", self.selection)
        #        self.bind("<Up>", self.up)
        #        self.bind("<Down>", self.down)
        #        self.bind("<MouseWheel>", self._on_mousewheel)
        #        self.bind("<Key>", self.cancel)
        self.lb_up = False
        self.lbName = None
        self.enableEntry = False
        self.disableVerificationAgainstList = True

    def setSmallFont(self):
        self.config(font=fontAverage)
        self.smallFont=True

    def clearPopups(self):
        if self.lb_up == True:
            self.lb.destroy()
            self.lb_up = False

    def disableCheck(self):
        self.disableVerificationAgainstList = True

    def enableCheck(self):
        self.disableVerificationAgainstList = False

    def setAsDeleted(self):
        self.config(disabledforeground=AppDefaultDisabledForeground)

    def resetAsDeleted(self):
        self.config(disabledforeground=AppDefaultForeground)

    def setAsRequiredField(self):
        self.config(background=AppDefaultRequiredFieldBackground)

    def resetAsRequiredField(self):
        self.config(background=AppDefaultEditBackground)

    def addTrace(self, aTraceProc):
        self.trace = True
        self.traceProc = aTraceProc

    def enable(self):
        self.enableEntry = True
        #        self.bind('<Enter>', self.enter)
        self.bind('<FocusOut>', self.check)
        self.bind("<ButtonRelease-1>", self.selectItem)
        self.bind("<Right>", self.enterKeyEntered)
        self.bind("<Return>", self.selection)
        self.bind("<Up>", self.up)
        self.bind("<Down>", self.down)
        self.bind("<MouseWheel>", self._on_mousewheel)
        self.bind("<Key>", self.cancel)

    #        self.config(background=AppDefaultEditBackground)

    def disable(self):
        self.enableEntry = False
        #        self.unbind('<Enter>')
        self.unbind('<FocusOut>')
        self.unbind("<ButtonRelease-1>")
        self.unbind("<Right>")
        self.unbind("<Return>")
        self.unbind("<Up>")
        self.unbind("<Down>")
        self.unbind("<MouseWheel>")
        self.unbind("<Key>")

    def cancel(self, event, *args):
        self.aDirtyMethod(None)
        if event.char == chr(27):
            if self.lb_up:
                self.lb.destroy()
                self.lb_up = False
        else:
            self.enableCheck()

    def enterKeyEntered(self, *args):
        self.aDirtyMethod(None)
        self.enableCheck()
        if not self.lb_up:
            self.lb = self.createLB()
            for w in self.lista:
                self.lb1.insert(END, w)
            index = 0
            self.lb1.selection_set(first=index)
            self.lb1.activate(index)
        if len(self.var.get()) > 0:
            self.changed(None, None, None)

    def createLB(self):
        self.enableCheck()
        if len(self.lista) > 9:
            height = 10
        else:
            height = len(self.lista)

        lb = AppFrame(self.parentof, 2)
        lb.noBorder()

        lb.place(in_=self.parentof, relx=0, rely=0, anchor=NW,
                 x=self.parent.winfo_rootx() - self.parentof.winfo_rootx(),
                 y=self.parent.winfo_rooty() - self.parentof.winfo_rooty() + self.winfo_height() + AppHighlightBorderWidth)

        if len(self.lista) + 1 > 10:
            yScroll = Scrollbar(lb, orient=VERTICAL)
            yScroll.grid(row=0, column=1, sticky=N + S)

            self.lb1 = Listbox(lb, width=self.fieldWidth + pullDownAdjustmentWidth, font=fontBig, height=height,
                               border=AppDefaultBorderWidth, yscrollcommand=yScroll.set, takefocus=0)
            if self.smallFont == True:
                self.lb1.config(font=fontAverage)
            self.lb1.grid(row=0, column=0, sticky=N + S + W + E)
            yScroll['command'] = self.lb1.yview
        else:
            self.lb1 = Listbox(lb, width=self.fieldWidth + pullDownAdjustmentWidth, font=fontBig, height=height,
                               border=AppDefaultBorderWidth, takefocus=0)
            if self.smallFont == True:
                self.lb1.config(font=fontAverage)
            self.lb1.grid(row=0, column=0, sticky=N + S + W + E)

        # lb = Listbox(self.parentof, width=self.fieldWidth, font=fontBig, height=height, border=2, takefocus=0)
        #        lb.place(in_=self.parentof, relx=0, rely=0, anchor=NW, x= self.parent.winfo_rootx()- self.parentof.winfo_rootx(),
        #                  y= self.parent.winfo_rooty() - self.parentof.winfo_rooty() + self.winfo_height())
        self.lb1.bind("<ButtonRelease-1>", self.selectItem)
        self.lb1.bind("<Double-Button-1>", self.selection)
        self.lb1.bind("<Right>", self.selection)
        self.lb1.bind("<Up>", self.up)
        self.lb1.bind("<Down>", self.down)
        self.lb1.bind("<MouseWheel>", self._on_mousewheel)
        self.lbName = self.lb1.winfo_name()
        self.lb_up = True
        return lb

    def _on_mousewheel(self, event):
        if event.delta > 0:
            self.lb1.yview_scroll(-2, 'units')
        else:
            self.lb1.yview_scroll(2, 'units')

    def enter(self, event):
        if self.enableEntry:
            if len(self.var.get()) > 0:
                self.changed(None, None, None)
            else:
                self.enterKeyEntered(None)
            self.focus()

    def check(self, event):
        if self.focusOutActionRequired != None:
            self.focusOutActionRequired()
        if self.disableVerificationAgainstList == False:
            if self.lbName != None:
                aName = event.widget.focus_lastfor()
                pattern = re.compile('.*' + self.lbName + '*.')
                if re.match(pattern, str(aName)) == None:
                    if not self.findInList():
                        if len(self.get()) > 0:
                            if self.addItemProc == None:  # Is there a proc
                                aMsg = "ERROR: Item must be selected from the list."
                                self.myMsgBar.newMessage('error', aMsg)
                                self.focus()
                            else:
                                self.addItemProc()
                                self.disableCheck()
                        else:
                            #
                            #  This is a catch in case noSupplierID not provided
                            #  Most cases will point to an ID which is a no supplier (empty fields)
                            #
                            aMsg = "INFO: No items selected."
                            self.myMsgBar.newMessage('info', aMsg)
                            self.clearPopups()
                    else:
                        self.disableCheck()
                        if self.lb_up:
                            self.lb.destroy()
                            self.lb_up = False
                        if self.trace == True:
                            if self.traceProc != None:
                                self.traceProc(event)
            else:
                if not self.findInList():
                    if len(self.get()) > 0:
                        if self.addItemProc == None:  # Is there a proc
                            aMsg = "ERROR: Item must be selected from the list."
                            self.myMsgBar.newMessage('error', aMsg)
                            self.focus()
                        else:
                            self.addItemProc()
                            self.disableCheck()
                    else:
                        #
                        #  This is a catch in case noSupplierID not provided
                        #  Most cases will point to an ID which is a no supplier (empty fields)
                        #
                        aMsg = "INFO: No items selected."
                        self.myMsgBar.newMessage('info', aMsg)
                        self.clearPopups()
                else:

                    self.disableCheck()
                    if self.lb_up:
                        self.lb.destroy()
                        self.lb_up = False
                    if self.trace == True:
                        if self.traceProc != None:
                            self.traceProc(event)

    def changed(self, name, index, mode):
        if self.enableEntry:
            if self.var.get() == '':
                if self.lb_up:
                    self.lb.destroy()
                    self.lb_up = False
                    self.enterKeyEntered(None)
            else:
                words = self.comparison()
                if words:
                    if not self.lb_up:
                        self.lb = self.createLB()

                    self.lb1.delete(0, END)
                    for w in words:
                        self.lb1.insert(END, w)
                    index = 0
                    self.lb1.selection_set(first=index)
                    self.lb1.activate(index)
                else:
                    if self.lb_up:
                        self.lb.destroy()
                        self.lb_up = False

    def selectItem(self, event):
        self.aDirtyMethod(None)
        if self.lb_up:
            if self.lb1.curselection() == ():
                index = '0'
            else:
                index = self.lb1.curselection()[0]
            self.lb1.selection_clear(first=index)
            self.lb1.selection_set(first=index)
            self.lb1.activate(index)
        # self.reDisplayList(index)
        self.focus()

    def selection(self, event):
        if self.lb_up:
            self.var.set(self.lb1.get(ACTIVE))
            self.aDirtyMethod(None)
            self.lb.destroy()
            self.lb_up = False
            self.icursor(END)
            focusNext(self, self)
            #            self.focus()

    def up(self, event):
        if self.lb_up:
            if self.lb1.curselection() == ():
                index = '0'
            else:
                index = self.lb1.curselection()[0]
            if index != '0':
                self.lb1.selection_clear(first=index)
                index = str(int(index) - 1)
                self.lb1.selection_set(first=index)
                self.lb1.activate(index)
            self.lb1.see(index)
            self.focus()

    def down(self, event):
        if self.lb_up:
            if self.lb1.curselection() == ():
                index = '0'
            else:
                index = self.lb1.curselection()[0]
            if index != END:
                self.lb1.selection_clear(first=index)
                index = str(int(index) + 1)
                self.lb1.selection_set(first=index)
                self.lb1.activate(index)
            self.lb1.see(index)
            self.focus()

    def disableSearch(self):
        self.search = False

    def enableSearch(self):
        self.search = True

    def comparison(self):
        #        pattern = re.escape('.*' + self.var.get() + '.*', re.IGNORECASE)
        if self.search == True:
            pattern1 = re.escape(self.var.get())
            pattern2 = re.compile('.*' + pattern1 + '.*', re.IGNORECASE)
            return [w for w in self.lista if re.match(pattern2, w)]
            self.focus()
        else:
            return self.lista
            self.focus()

    def findInList(self, *args):
        try:
            self.lista.index(self.var.get())
            return True
        except:
            return False

            #    def autowidth(self,maxwidth):
            #        f = font.Font(font=self.cget("font"))
            #        pixels = 0
            #        for item in self.get(0, "end"):
            #            pixels = max(pixels, f.measure(item))
            # bump listbox size until all entries fit


# pixels = pixels + 10
#        width = int(self.cget("width"))
#        for w in range(0, maxwidth+1, 5):
#            if self.winfo_reqwidth() >= pixels:
#                break
#            self.config(width=width+w)

class AppSearchLB(Tix.Frame):
    def __init__(self, parent, topFrame, aLabel, orientation, aList, aWidth, aProc, myMsgBar, aDirtyMethod=None, aFocusOutActionRequired=None):
        #
        #  aProc is a procedure to execute if item is not in the list.  If no
        # procedure is passed to the class, no action is taken.
        #
        Tix.Frame.__init__(self, parent)
        self.topFrame = topFrame
        self.myMsgBar = myMsgBar
        self.aFocusOutActionRequired = aFocusOutActionRequired
        self.aDirtyMethod = aDirtyMethod

        self.aList = aList
        self.config(pady=offsetYFieldEntry,takefocus=0)
        myRow = 0
        myColumn = 0

        if aLabel != None:
            if orientation == 'H':
                self.aLabel = ttk.Label(self, text='{0}: '.format(aLabel), takefocus=0, style='RegularFieldTitle.TLabel')
                self.aLabel.grid(row=myRow, column=myColumn, sticky=E + N + S)
                myRow = 0
                myColumn = 1
            elif orientation == 'V':
                self.aLabel = ttk.Label(self, text='{0}'.format(aLabel), takefocus=0, style='RegularFieldTitle.TLabel')
                self.aLabel.grid(row=myRow, column=myColumn, sticky=N + S)
                myRow = 1
                myColumn = 0
            else:
                self.aLabel = ttk.Label(self, text='{0}'.format(aLabel), takefocus=0, style='RegularFieldTitle.TLabel')

        self.aFrame = Frame(self, highlightcolor=AppDefaultForeground,
                            highlightthickness=AppHighlightBorderWidth, takefocus=0)
        #        aFrame=Frame(self, takefocus=0)
        if orientation == 'H':
            self.aFrame.grid(row=myRow, column=myColumn, sticky=W + N)
        else:
            self.aFrame.grid(row=myRow, column=myColumn, sticky=N)

        self.anEntry = AppACListEntry(self.aFrame, topFrame, aList, aWidth, aProc, self.myMsgBar, self.aDirtyMethod, self.aFocusOutActionRequired)
        self.anEntry.grid(row=0, column=0, sticky=N)

        pullDownImg = resizeImage(Image.open(pullDownImage), iconWidth, iconHeight)
        self.buttonPullDown = ttk.Button(self.aFrame, image=pullDownImg, style='ComboBoxButton.TButton', takefocus=0)
        self.buttonPullDown.img = pullDownImg
        self.buttonPullDown.grid(row=0, column=1, sticky=N+S)

        addImg = resizeImage(Image.open(addCommandRecImage), iconWidth, iconHeight)
        self.addProcButton = ttk.Button(self.aFrame, image=addImg, style='ComboBoxButton.TButton', takefocus=0)
        self.addProcButton.img = addImg
        self.addProcButton.grid(row=0, column=4, sticky=N+S)
        self.addProcButton_ttp = CreateToolTip(self.addProcButton, "Add a New Record")

        editImg = resizeImage(Image.open(editRecImage), iconWidth, iconHeight)
        self.editProcButton = ttk.Button(self.aFrame, image=editImg, style='ComboBoxButton.TButton', takefocus=0)
        self.editProcButton.img = editImg
        self.editProcButton.grid(row=0, column=5, sticky=N+S)
        self.editProcButton_ttp = CreateToolTip(self.editProcButton, "Edit Current Record")

        viewImg = resizeImage(Image.open(viewRecImage), iconWidth, iconHeight)
        self.viewProcButton = ttk.Button(self.aFrame, image=viewImg, style='ComboBoxButton.TButton', takefocus=0)
        self.viewProcButton.img = viewImg
        self.viewProcButton.grid(row=0, column=2, sticky=N+S)
        self.viewProcButton_ttp = CreateToolTip(self.viewProcButton, "View Record Details")

        changeImg = resizeImage(Image.open(changeRecImage), iconWidth, iconHeight)
        self.changeProcButton = ttk.Button(self.aFrame, image=changeImg, style='ComboBoxButton.TButton', takefocus=0)
        self.changeProcButton.img = changeImg
        self.changeProcButton.grid(row=0, column=3, sticky=N+S)
        self.changeProcButton_ttp = CreateToolTip(self.changeProcButton, "Turn on Record Update")

        cancelImg = resizeImage(Image.open(cancelCommandRecImage), iconWidth, iconHeight)
        self.cancelProcButton = ttk.Button(self.aFrame, image=cancelImg, style='ToolTipButton.TButton', takefocus=0)
        self.cancelProcButton.img = cancelImg
        self.cancelProcButton.grid(row=0, column=6, sticky=N+S)
        self.cancelProcButton_ttp = CreateToolTip(self.cancelProcButton, "Cancel Record Update")

        self.editProcButton.grid_forget()
        self.addProcButton.grid_forget()
        self.viewProcButton.grid_forget()
        self.changeProcButton.grid_forget()
        self.cancelProcButton.grid_forget()
        self.editCurrentItemActive = False
        self.addCurrentItemActive = False
        self.viewCurrentItemActive = False
        self.changeCurrentItemActive = False
        self.cancelCurrentItemActive = False

        #        self.anEntry.config(state='disabled')
        self.updateValuesList(aList)

    def setSmallFont(self):
        self.aLabel.configure(style='SmallFieldTitle.TLabel')
        self.anEntry.setSmallFont()

    def disableCheck(self):
        self.anEntry.disableCheck()

    def addCancelProc(self, aProc):
        self.cancelProcButton.grid(row=0, column=6, sticky=N)
        self.cancelItem = aProc
        self.cancelCurrentItemActive = True

    def addChangeProc(self, aProc):
        self.changeProcButton.grid(row=0, column=3, sticky=N)
        self.changeItem = aProc
        self.changeCurrentItemActive = True

    def addEditProc(self, aProc):
        self.editProcButton.grid(row=0, column=5, sticky=N)
        self.editCurrentItem = aProc
        self.editCurrentItemActive = True

    def addAddProc(self, aProc):
        self.addProcButton.grid(row=0, column=4, sticky=N)
        self.addNewItem = aProc
        self.addCurrentItemActive = True

    def addViewProc(self, aProc):
        self.viewProcButton.grid(row=0, column=2, sticky=N)
        self.viewCurrentItem = aProc
        self.viewProcButton.config(state='normal')
        self.viewProcButton.bind('<Button>', self.viewCurrentItem)
        self.viewCurrentItemActive = True

    def setAsDeleted(self):
        self.anEntry.setAsDeleted()

    def resetAsDeleted(self):
        self.anEntry.resetAsDeleted()

    def setAsRequiredField(self):
        self.anEntry.setAsRequiredField()

    def resetAsRequiredField(self):
        self.anEntry.resetAsRequiredField()

    def addTrace(self, aTraceProc):
        self.anEntry.addTrace(aTraceProc)

    def justification(self, aValue):
        self.anEntry.config(justify=aValue)

    def clearPopups(self):
        if self.anEntry.lb_up == True:
            self.anEntry.lb.destroy()
            self.anEntry.lb_up = False

    def enter(self, event):
        self.anEntry.focus()

    #        self.aFrame.focus()

    def focus(self):
        self.anEntry.focus()

    #        self.aFrame.focus()

    def get(self):
        return (self.anEntry.get())

    def clear(self):
        aState = self.anEntry.cget('state')
        self.anEntry.config(state='normal')
        self.anEntry.delete(0, END)
        if aState != 'normal':
            self.anEntry.config(state='disabled')

    def load(self, aValue):
        aState = self.anEntry.cget('state')
        if aValue == None:
            aValue = ''
        self.anEntry.config(state='normal')
        self.anEntry.delete(0, END)
        self.anEntry.insert(0, aValue)
        if aState != 'normal':
            self.anEntry.config(state='disabled')
        self.clearPopups()

    def find(self):
        self.anEntry.config(state='normal', takefocus=1)
        self.aFrame.config(takefocus=1)

        #        self.buttonPullDown.config(state='normal')
        #        self.buttonPullDown.bind('<Button>', self.anEntry.enter)
        if self.viewCurrentItemActive:
            self.viewProcButton.config(state='disabled')
            self.viewProcButton.unbind('<Button>')

            #        self.bind('<Enter>', self.enter)

    def enableCancelButton(self):
        self.cancelProcButton.config(state='normal')
        self.cancelProcButton.bind('<Button>', self.cancelItem)

    def enableChangeButton(self):
        self.changeProcButton.config(state='normal')
        self.changeProcButton.bind('<Button>', self.changeItem)

    def focusout(self, *args):
        self.clearPopups()

    def enable(self):
        self.anEntry.config(state='normal', takefocus=1)
        self.aFrame.config(takefocus=0)

        #        self.bind('<FocusOut>', self.focusout)

        if self.editCurrentItemActive:
            self.editProcButton.config(state='normal')
            self.editProcButton.bind('<Button>', self.editCurrentItem)

        # if self.viewCurrentItemActive:
        #            self.viewProcButton.config(state='normal')
        #            self.viewProcButton.bind('<Button>', self.viewCurrentItem)

        if self.addCurrentItemActive:
            self.addProcButton.config(state='normal')
            self.addProcButton.bind('<Button>', self.addNewItem)

        self.buttonPullDown.config(state='normal')
        self.buttonPullDown.bind('<Button>', self.anEntry.enter)

        self.bind('<Enter>', self.enter)
        #        self.bind("<Right>", self.anEntry.enterKeyEntered)

        self.anEntry.enable()

    def disable(self):
        self.anEntry.config(state='disabled', takefocus=0)
        self.aFrame.config(takefocus=0)
        self.unbind('<Enter>')
        #        self.bind('<FocusOut>')
        self.anEntry.unbind('<Enter>')
        self.anEntry.disable()

        self.buttonPullDown.config(state='disabled')
        self.buttonPullDown.unbind('<Button>')

        if self.editCurrentItemActive:
            self.editProcButton.config(state='disabled')
            self.editProcButton.unbind('<Button>')

        if self.cancelCurrentItemActive:
            self.cancelProcButton.config(state='disabled')
            self.cancelProcButton.unbind('<Button>')

        if self.changeCurrentItemActive:
            self.changeProcButton.config(state='disabled')
            self.changeProcButton.unbind('<Button>')

        if self.viewCurrentItemActive:
            self.viewProcButton.config(state='normal')
            self.viewProcButton.bind('<Button>', self.viewCurrentItem)
        # self.viewProcButton.config(state='disabled')
        #        self.viewProcButton.unbind('<Button>')

        if self.addCurrentItemActive:
            self.addProcButton.config(state='disabled')
            self.addProcButton.unbind('<Button>')

        self.clearPopups()

    def updateValuesList(self, aList):
        self.aList = aList
        self.anEntry.lista = aList

    def disableSearch(self):
        self.anEntry.disableSearch()

    def enableSearch(self):
        self.anEntry.enableSearch()

class AppColumnAlignFieldFrame(Tix.Frame):
    def __init__(self, parent):
        Tix.Frame.__init__(self, parent)
        self.config(border=AppDefaultBorderWidth, relief='ridge')
        self.row = 0
        self.column = 0
        self.columnTotal = 2
        self.itemNumber = 0
        self.vertical = False
        self.rowconfigure(self.row, weight=0)
        for i in range(self.columnTotal):
            self.columnconfigure(i, weight=1)

    def noBorder(self):
        self.config(border=0)

    def defaultBorder(self):
        self.config(border=AppDefaultBorderWidth)

    def addTitle(self, aTitle):
        aLabel = ttk.Label(self, text='{0}'.format(aTitle), takefocus=0, style='RegularFieldTitle.TLabel')
        aLabel.grid(row=self.row, column=self.column, columnspan=self.columnTotal, sticky=N)
        self.itemNumber = self.itemNumber + 1
        self.vertical = True

    def addFrame(self, aFrame):
        self.itemNumber = self.itemNumber + 1
        self.row = self.itemNumber - 1
        aFrame.grid(row=self.row, column=0, columnspan=self.columnTotal, sticky=N)

    def addFrameStrech(self, aFrame):
        self.itemNumber = self.itemNumber + 1
        self.row = self.itemNumber - 1
        aFrame.grid(row=self.row, column=0, columnspan=self.columnTotal, sticky=N + W + E)

    def addFrameLeftAlign(self, aFrame):
        self.itemNumber = self.itemNumber + 1
        self.row = self.itemNumber - 1
        aFrame.grid(row=self.row, column=0, columnspan=self.columnTotal, sticky=N + W)

    def addFrameRightAlign(self, aFrame):
        self.itemNumber = self.itemNumber + 1
        self.row = self.itemNumber - 1
        aFrame.grid(row=self.row, column=0, columnspan=self.columnTotal, sticky=N + E)

    def addEmptyRow(self):
        self.itemNumber = self.itemNumber + 1
        self.row = self.itemNumber - 1
        aFrame = AppBorderFrame(self, 1)
        aFrame.noBorder()
        aFrame.grid(row=self.row, column=0, columnspan=self.columnTotal, sticky=N + E)

        anEntry = Tix.Entry(aFrame, highlightcolor=AppDefaultForeground, highlightthickness=AppHighlightBorderWidth,
                            width=10, font=fontBig, relief=FLAT,
                            background=AppDefaultEditBackground, disabledforeground=AppDefaultForeground)
        anEntry.grid(row=0, column=0, sticky=E + N)
        anEntry.config(state='disabled')

    def addTextCenter(self, aText, alignment):
        self.itemNumber = self.itemNumber + 1
        self.row = self.itemNumber - 1
        aFrame = AppBorderFrame(self, 1)
        aFrame.noBorder()
        aFrame.grid(row=self.row, column=0, columnspan=self.columnTotal, sticky=N + E + W)

        anEntry = Tix.Entry(aFrame, highlightcolor=AppDefaultForeground, highlightthickness=AppHighlightBorderWidth,
                            font=fontBigI, relief=FLAT,
                            background=AppDefaultEditBackground, disabledforeground=AppDefaultForeground)
        anEntry.grid(row=0, column=0, sticky=E + N + W + S)
        anEntry.insert(0, aText)
        anEntry.config(justify=alignment)
        anEntry.config(state='disabled')

    def addNewField(self, aTitle, anEntry):
        self.itemNumber = self.itemNumber + 1
        self.row = self.itemNumber - 1
        if self.vertical == False:
            if aTitle == None:
                aLabel = ttk.Label(self, text=''.format(aTitle), takefocus=0, style='RegularFieldTitle.TLabel')
            else:
                aLabel = ttk.Label(self, text='{0}: '.format(aTitle), takefocus=0, style='RegularFieldTitle.TLabel')
        else:
            aLabel = ttk.Label(self, text=''.format(aTitle), takefocus=0, style='RegularFieldTitle.TLabel')
        aLabel.grid(row=self.row, column=0, sticky=E + N + S)
        anEntry.grid(row=self.row, column=1, sticky=W + N + S)
        self.rowconfigure(self.row, weight = 1)

    def  noStretchCurrentRow(self):
        self.rowconfigure(self.row, weight=0)

class AppCBList():
    def __init__(self, aComboBoxData):
        self.updateDictionaries(aComboBoxData)

    def getList(self):
        aList = []
        for i in range(len(self.aComboBoxData)):
            aList.append(self.aComboBoxData[i][1])
        return aList

    def getName(self, ID):
        return self.aReverseDictionary[ID]

    def getID(self, Name):
        if len(Name) > 0:
            return (self.aDictionary[Name])
        else:
            return None

    def updateDictionaries(self, aComboBoxData):
        self.aComboBoxData = aComboBoxData
        self.aDictionary = {}
        self.aReverseDictionary = {}
        for i in range(len(aComboBoxData)):
            self.aDictionary.update({aComboBoxData[i][1]: aComboBoxData[i][0]})
            self.aReverseDictionary.update({aComboBoxData[i][0]: aComboBoxData[i][1]})

class AppCB_AC(Tix.Frame):
    def __init__(self, parent, aList, aWidth, addItemProc, msgWidget):
        self.addItemProc = addItemProc
        self.msgWidget = msgWidget
        Tix.Frame.__init__(self, parent)

        self.config(border=2, highlightcolor=AppDefaultForeground, highlightthickness=AppHighlightBorderWidth,
                    takefocus=0)
        self.bind('<Enter>', widgetEnter)
        self.bind('<FocusOut>', self.check)
        self.Name = self.winfo_name()
        self.CB = AutocompleteCombobox(self, width=aWidth, font=fontBig, justify=LEFT, background=AppDefaultBackground)
        self.CB.grid(row=0, column=0, sticky=N + S + E + W)
        self.CB.set_completion_list(aList)
        self.CB.bind("<ComboboxSelected>")
        self.CB.config(state='disabled')
        self.CBName = self.CB.winfo_name()
        self.updateValuesList(aList)

    def check(self, event):
        aName = event.widget.focus_lastfor()
        pattern = re.compile('.*' + self.CBName + '*.', re.IGNORECASE)
        if re.match(pattern, str(aName)):
            pass
        else:
            if not self.findInList():
                if len(self.CB.get()) > 0:
                    if self.addItemProc == None:
                        aMsg = "ERROR: Items must be in the list."
                        self.msgWidget.newMessage('error', aMsg)
                        self.focus()
                    else:
                        self.addItemProc()

    def focus(self):
        self.CB.focus()

    def get(self):
        return (self.CB.get())

    def clear(self):
        aState = self.CB.cget('state')
        self.CB.config(state='normal')
        self.CB.delete(0, END)
        if aState != 'normal':
            self.CB.config(state='disabled')


    def load(self, aValue):
        if aValue == None:
            aValue = ''
        self.CB.config(state='normal')
        self.CB.delete(0, END)
        self.CB.insert(0, aValue)
        self.CB.config(state='disabled')

    def enable(self):
        self.CB.config(state='normal', takefocus=1)
        self.bind('<Enter>', widgetEnter)

    def disable(self):
        self.CB.config(state='disabled', takefocus=0)
        self.unbind('<Enter>')

    def updateValuesList(self, aList):
        self.aList = aList
        self.CB.config(state='normal')
        self.CB.delete(0, END)
        self.CB.set_completion_list(aList)

    def findInList(self, *args):
        try:
            self.aList.index(self.CB.get())
            return True
        except:
            return False

class AppCB(Frame):
    def __init__(self, parent, topFrame, aLabel, orientation, aList, aWidth, myMsgBar, aDirtyMethod):
        Frame.__init__(self, parent)
        self.topFrame = topFrame
        self.myMsgBar = myMsgBar
        self.aWidth = aWidth


        # self.aList = aList
        # self.config(pady=offsetYFieldEntry,takefocus=0)
        # myRow = 0
        # myColumn = 0
        #
        # if aLabel != None:
        #     if orientation == 'H':
        #         self.aLabel = ttk.Label(self, text='{0}: '.format(aLabel), takefocus=0, style='RegularFieldTitle.TLabel')
        #         self.aLabel.grid(row=myRow, column=myColumn, sticky=E + N + S)
        #         myRow = 0
        #         myColumn = 1
        #     elif orientation == 'V':
        #         self.aLabel = ttk.Label(self, text='{0}'.format(aLabel), takefocus=0, style='RegularFieldTitle.TLabel')
        #         self.aLabel.grid(row=myRow, column=myColumn, sticky=N + S)
        #         myRow = 1
        #         myColumn = 0
        #     else:
        #         self.aLabel = ttk.Label(self, text='{0}'.format(aLabel), takefocus=0, style='RegularFieldTitle.TLabel')
        #
        # self.aFrame = Frame(self, highlightcolor=AppDefaultForeground,
        #                     highlightthickness=AppHighlightBorderWidth, takefocus=0)
        # #        aFrame=Frame(self, takefocus=0)
        # if orientation == 'H':
        #     self.aFrame.grid(row=myRow, column=myColumn, sticky=W + N)
        # else:
        #     self.aFrame.grid(row=myRow, column=myColumn, sticky=N)
        #
        # self.anEntry = AppACListEntry(self.aFrame, topFrame, aList, aWidth, aProc, self.myMsgBar, self.aDirtyMethod, self.aFocusOutActionRequired)
        # self.anEntry.grid(row=0, column=0, sticky=N)


        self.aList = aList
        self.config(pady=offsetYFieldEntry,takefocus=0)
        myRow = 0
        myColumn = 0

        if aLabel != None:       
            if orientation == 'H':
                self.aLabel = ttk.Label(self, text='{0}: '.format(aLabel), takefocus=0, border=0, style='RegularFieldTitle.TLabel')
                self.aLabel.grid(row=myRow, column=myColumn, sticky=E+N+S)
                myRow = 0
                myColumn = 1
            elif orientation == 'V':
                self.aLabel = ttk.Label(self, text='{0}'.format(aLabel), takefocus=0, border=0, style='RegularFieldTitle.TLabel')
                self.aLabel.grid(row=myRow, column=myColumn, sticky=N+S)
                myRow = 1
                myColumn = 0
            else:
                self.aLabel = ttk.Label(self, text='{0}'.format(aLabel), takefocus=0,
                                        style='RegularFieldTitle.TLabel')


#        self.aFrame=Frame(self, border=0, highlightcolor=AppDefaultForeground, highlightthickness = AppHighlightBorderWidth, takefocus=1)
        self.aFrame=Frame(self, border=0)
        if orientation == 'H':
            self.aFrame.grid(row=myRow, column=myColumn, sticky=W+N)
        else:
            self.aFrame.grid(row=myRow, column=myColumn, sticky=S)

        self.anEntry = AppLBforCB(self.aFrame, self.topFrame, self.aList, self.aWidth, self.myMsgBar, aDirtyMethod)
        self.anEntry.grid(row=0, column=0, sticky= N)

        pullDownImg = resizeImage(Image.open(pullDownImage),iconWidth,iconHeight)                     
        self.buttonPullDown = ttk.Button(self.aFrame, image=pullDownImg, style='ComboBoxButton.TButton', takefocus=0)
        self.buttonPullDown.img = pullDownImg
        self.buttonPullDown.grid(row=0, column=1, sticky=N+S)
#        self.buttonFind_ttp = CreateToolTip(self.buttonFind, "Find Record(s)")
#        self.anEntry.config(state='disabled')
#        self.updateValuesList(aList)

    def setSmallFont(self):
        self.aLabel.configure(style='SmallFieldTitle.TLabel')
        self.anEntry.configure(font=fontAverage)
        self.smallFont=True

    def setAsRequiredField(self):
        self.anEntry.config(background=AppDefaultRequiredFieldBackground)

    def resetAsRequiredField(self):
        self.anEntry.config(background=AppDefaultEditBackground)

    def clearPopups(self):
        if self.anEntry.lb_up == True:
            self.anEntry.lb.destroy()
            self.anEntry.lb_up = False
                   
    def justification(self, aValue):
        self.anEntry.config(justify=aValue)
        
    def enter(self,event):
        self.aFrame.focus()

    def focus(self):
        self.aFrame.focus()
    
    def enable(self):
        self.anEntry.enable() 
        self.aFrame.config(takefocus=1)    
        self.buttonPullDown.config(state='normal')
        self.bind('<Enter>', self.enter)
        self.buttonPullDown.bind('<Button>', self.anEntry.enter)
                  
    def disable(self):
        self.anEntry.disable()
        self.buttonPullDown.config(state='disabled')
        self.aFrame.config(takefocus=0)            
        self.buttonPullDown.unbind('<Button>')
        self.clearPopups()
        self.unbind('<Enter>')
        
    def addTrace(self, aTraceProc):
        self.anEntry.addTrace(aTraceProc)

    def clear(self):
        aState = self.anEntry.cget('state')
        self.anEntry.config(state='normal')
        self.anEntry.delete(0, END)
        if aState != 'normal':
            self.anEntry.config(state='disabled')
                  
    def load(self, aValue):
        self.anEntry.config(state='normal')
        self.anEntry.delete(0, END)
        self.anEntry.insert(0, aValue)
        self.anEntry.config(state='disabled')
        
    def updateList(self, aList): 
        self.anEntry.updateList(aList)

class AppLBforCB(Entry):
    #
    #   Special List Box Entry
    #      1. present a listbox with choices
    #      2. can be configure to execute an add script if choice is not present in list
    #      3. It will force to select from list otherwise
    #      4. In order to open listbox at correct place, parent's parent must be provided
    #      5. Popup list removes unmatched items (anywhere, not case sensitive)
    #      
    #
    def __init__(self, parent, parentof, lista, fieldWidth, myMsgBar, aDirtyMethod):
        self.fieldWidth = fieldWidth
        Entry.__init__(self, parent)
        self.parent=parent
        self.parentof=parentof
        self.myMsgBar = myMsgBar
        self.search = False
        self.traceProc = None
        self.trace = False
        self.aDirtyMethod = aDirtyMethod
        
        self.config(takefocus = 0, font=fontBig, disabledforeground=AppDefaultForeground, width=self.fieldWidth,
                    justify=LEFT)
#        self.bind('<Enter>', self.enter)
#        self.bind('<Return>', self.enterKeyEntered)
#        self.bind('<FocusOut>', self.check)
        self.lista = lista        
        self.var = self["textvariable"]
        if self.var == '':
            self.var = self["textvariable"] = StringVar()
        self.var.trace("w", self.changed)
        self.config(state='disabled')
#        self.bind("<ButtonRelease-1>", self.selectItem)
#        self.bind("<Right>", self.enterKeyEntered)
#        self.bind("<Return>", self.selection)
#        self.bind("<Up>", self.up)
#        self.bind("<Down>", self.down)
#        self.bind("<MouseWheel>", self._on_mousewheel) 
#        self.bind("<Key>", self.cancel)             
        self.lb_up = False
        self.lbName = None
        self.enableEntry = False

    def addTrace(self, aTraceProc):
        self.trace = True
        self.traceProc = aTraceProc
        
    def enable(self):
        self.enableEntry = True
#        self.bind('<Enter>', self.enter)
        self.bind('<FocusOut>', self.check)
        self.bind("<ButtonRelease-1>", self.selectItem)
        self.bind("<Right>", self.enterKeyEntered)
        self.bind("<Return>", self.selection)
        self.bind("<Up>", self.up)
        self.bind("<Down>", self.down)
        self.bind("<MouseWheel>", self._on_mousewheel) 
        self.bind("<Key>", self.cancel)
        self.config(background=AppDefaultEditBackground, disabledbackground=AppDefaultEditBackground)             
        
    def disable(self):
        self.enableEntry = False
#        self.unbind('<Enter>')
        self.unbind('<FocusOut>')
        self.unbind("<ButtonRelease-1>")
        self.unbind("<Right>")
        self.unbind("<Return>")
        self.unbind("<Up>")
        self.unbind("<Down>")
        self.unbind("<MouseWheel>") 
        self.unbind("<Key>")             
        self.config(background=AppDefaultBackground, disabledbackground=AppDefaultBackground)
        
    def cancel(self, event):
        self.aDirtyMethod(None)
        if event.char == chr(27):
            if self.lb_up:
                self.lb.destroy()
                self.lb_up = False
       
    def enterKeyEntered(self, *args):
        self.aDirtyMethod(None)
        if not self.lb_up:
            self.lb = self.createLB()
            for w in self.lista:
                self.lb.insert(END,w)
            index = 0
            self.lb.selection_set(first=index)
            self.lb.activate(index)
        if len(self.var.get()) > 0:
            self.changed(None,None,None) 
                           
    def createLB(self):
        if len(self.lista)+1 > 9:
            height = 10
        else:
            height = len(self.lista)+1
        lb = Listbox(self.parentof, width=self.fieldWidth, font=fontBig, height=height, border=AppDefaultBorderWidth, takefocus=0)
        lb.place(in_=self.parentof, relx=0, rely=0, anchor=NW, x= self.parent.winfo_rootx()- self.parentof.winfo_rootx(),
                  y= self.parent.winfo_rooty() - self.parentof.winfo_rooty() + self.winfo_height() + AppHighlightBorderWidth)
        lb.bind("<ButtonRelease-1>", self.selectItem)
        lb.bind("<Double-Button-1>", self.selection)
        lb.bind("<Right>", self.selection)
        lb.bind("<Up>", self.up)
        lb.bind("<Down>", self.down)
        lb.bind("<MouseWheel>", self._on_mousewheel)       
        lb.insert(END,"")
        self.lbName = lb.winfo_name()
        self.lb_up = True
        return lb
    
    def _on_mousewheel(self, event):
        if event.delta > 0:
            self.lb.yview_scroll(-2, 'units')
        else:
            self.lb.yview_scroll(2, 'units')
                  
    def enter(self,event):
        if self.enableEntry:
            if len(self.get()) > 0:
                self.changed(None,None,None)
            else:
                self.enterKeyEntered(None)
            self.focus()

    def check(self, event):
        if self.lbName != None:
            aName = event.widget.focus_lastfor()
            pattern = re.compile('.*' + self.lbName + '*.')
            if re.match(pattern, str(aName)) == None:
                if not self.findInList():
                    if len(self.get()) == 0:
                        aMsg = "INFO: No items selected."
                        self.myMsgBar.newMessage('info', aMsg)
                        
                if self.lb_up:
                    self.lb.destroy()
                    self.lb_up = False
                    
                if self.trace == True:
                    if self.traceProc != None:
                        self.traceProc(event)

    def changed(self, name, index, mode):
        if self.enableEntry:
            if self.var.get() == '':
                if self.lb_up:
                    self.lb.destroy()
                    self.lb_up = False
                    self.enterKeyEntered(None)
            else:
                words = self.comparison()
                if words:            
                    if not self.lb_up:
                        self.lb = self.createLB()
                    
                    self.lb.delete(0, END)
                    self.lb.insert(END,"")
                    for w in words:
                        self.lb.insert(END,w)
                    index = 0
                    self.lb.selection_set(first=index)
                    self.lb.activate(index)
                else:
                    if self.lb_up:
                        self.lb.destroy()
                        self.lb_up = False

    def selectItem(self, event):
        if self.lb_up:
            if self.lb.curselection() == ():
                index = '0'
            else:
                index = self.lb.curselection()[0]
            self.lb.selection_clear(first=index)
            self.lb.selection_set(first=index)
            self.lb.activate(index)
#        self.reDisplayList(index)
        self.focus()
               
    def selection(self, event):
        if self.lb_up:
            self.var.set(self.lb.get(ACTIVE))
            self.lb.destroy()
            self.lb_up = False
            self.icursor(END)            
            focusNext(self, self)
#            self.focus()

    def up(self, event):
        if self.lb_up:
            if self.lb.curselection() == ():
                index = '0'
            else:
                index = self.lb.curselection()[0]
            if index != '0':                
                self.lb.selection_clear(first=index)
                index = str(int(index)-1)                
                self.lb.selection_set(first=index)
                self.lb.activate(index)               
            self.lb.see(index)
            self.focus()

    def down(self, event):
        if self.lb_up:
            if self.lb.curselection() == ():
                index = '0'
            else:
                index = self.lb.curselection()[0]
            if index != END:                        
                self.lb.selection_clear(first=index)
                index = str(int(index)+1)        
                self.lb.selection_set(first=index)
                self.lb.activate(index) 
            self.lb.see(index)
            self.focus()

    def disableSearch(self):
        self.search = False
        
    def enableSearch(self):
        self.search = True

    def comparison(self):
#        pattern = re.escape('.*' + self.var.get() + '.*', re.IGNORECASE)
        if self.search == True:
            pattern1 = re.escape(self.var.get())
            pattern2 = re.compile('.*' + pattern1 + '.*', re.IGNORECASE)
            return [w for w in self.lista if re.match(pattern2, w)]
            self.focus()
        else:
            return self.lista
            self.focus()
            
    def updateList(self, aList):
        self.lista = aList

    def findInList(self, *args):
        try:
            self.lista.index(self.var.get())
            return True
        except:
            return False
        
#    def autowidth(self,maxwidth):
#        f = font.Font(font=self.cget("font"))
#        pixels = 0
#        for item in self.get(0, "end"):
#            pixels = max(pixels, f.measure(item))
        # bump listbox size until all entries fit
#        pixels = pixels + 10
#        width = int(self.cget("width"))
#        for w in range(0, maxwidth+1, 5):
#            if self.winfo_reqwidth() >= pixels:
#                break
#            self.config(width=width+w)

class AppUserDisplayLabel(Label):         
    def __init__(self, parent, userName):
        Label.__init__(self, parent)
#        self.aFunction = Label(self, text='Default view', font=fontAverageI, justify=RIGHT)
#        self.aFunction.grid(row=0, column=0, sticky=E+S)
#        self.rowconfigure(0, weight=0)
        self.userName = userName
        self.config(text=userName, font=fontAverageI, justify=RIGHT)
        
    def setUserName(self, aName):
        self.config(text=aName)

class AppFunctionDisplayLabel(Label):         
    def __init__(self, parent):
        Label.__init__(self, parent)
#        self.aFunction = Label(self, text='Default view', font=fontAverageI, justify=RIGHT)
#        self.aFunction.grid(row=0, column=0, sticky=E+S)
#        self.rowconfigure(0, weight=0)
        self.config(text='Default view', font=fontAverageI, justify=RIGHT)

    def setViewMode(self):
        self.config(text='View mode', font=fontAverageI, justify=RIGHT, foreground='red')

    def setAddMode(self):
        self.config(text='Add mode', font=fontAverageI, justify=RIGHT, foreground='red')

    def setFindMode(self):
        self.config(text='Find mode', font=fontAverageI, justify=RIGHT, foreground='green')

    def setFindResultMode(self):
        self.config(text='Find Result View', font=fontAverageI, justify=RIGHT, foreground='green')

    def setEditMode(self):
        self.config(text='Edit mode', font=fontAverageI, justify=RIGHT, foreground='red')

    def setDuplicateMode(self):
        self.config(text='Duplicate Record mode', font=fontAverageI, justify=RIGHT, foreground='red')

    def setDeleteMode(self):
        self.config(text='Delete Record mode', font=fontAverageI, justify=RIGHT, foreground='red')

    def setDeleteSetMode(self):
        self.config(text='Delete Set mode', font=fontAverageI, justify=RIGHT, foreground='red')

    def setUnDeleteSetMode(self):
        self.config(text='UnDelete Set mode', font=fontAverageI, justify=RIGHT, foreground='red')

    def setDefaultMode(self):
        self.config(text='Default view', font=fontAverageI, justify=RIGHT, foreground=AppDefaultForeground)


class AppViewSortFormat(Tix.Label):
    def __init__(self, parent):
        Tix.Label.__init__(self, parent)
        aText = " "
        self.config(text=aText, font=fontBigI)
        self.config(background=AppDefaultBackground)

    def load(self, aView, aSort):
        aText = "View: {0}, Sort: {1}".format(aView, aSort)
        self.config(text=aText, font=fontAverageI)


class CreateToolTipV2(object):
    '''
    create a tooltip for a given widget
    '''

    def __init__(self, widget, text='widget info'):
        self.widget = widget
        self.text = text
        self.widget.bind("<Button-2>", self.enter)
        self.widget.bind("<Leave>", self.close)

    def enter(self, event=None):
        x = y = 0
        x, y, cx, cy = self.widget.bbox("insert")
        x += self.widget.winfo_rootx() + 25
        y += self.widget.winfo_rooty() + 20
        # creates a toplevel window
        self.tw = Toplevel(self.widget)
        # Leaves only the label and removes the app window
        self.tw.wm_overrideredirect(True)
        self.tw.wm_geometry("+%d+%d" % (x, y))
        label = ttk.Label(self.tw, text=self.text, style='toolTip.TLabel')
        label.pack(ipadx=1)

    def close(self, event=None):
        try:
            if self.tw:
                self.tw.destroy()
        except:
            pass

class CreateToolTip(object):
    '''
    create a tooltip for a given widget
    '''
    def __init__(self, widget, text='widget info'):
        self.widget = widget
        self.text = text
        self.widget.bind("<Enter>", self.enter)
        self.widget.bind("<Leave>", self.close)
        
    def enter(self, event=None):
        x = y = 0
        x, y, cx, cy = self.widget.bbox("insert")
        x += self.widget.winfo_rootx() + 25
        y += self.widget.winfo_rooty() + 20
        # creates a toplevel window
        self.tw = Toplevel(self.widget)
        # Leaves only the label and removes the app window
        self.tw.wm_overrideredirect(True)
        self.tw.wm_geometry("+%d+%d" % (x, y))
        label = ttk.Label(self.tw, text=self.text, style='toolTip.TLabel')
        label.pack(ipadx=1)
        
    def close(self, event=None):
        if self.tw:
            self.tw.destroy()

class databaseCommands(Tix.Frame):
    def __init__(self, parent, access):
        Tix.Frame.__init__(self, parent)

        self.access = access

        databaseCommandsDisplay = Tix.Frame(self)
        databaseCommandsDisplay.grid(column=0, row=0)

        self.rowCount = 0
        self.columnCount = 0
        btnImage = resizeImage(Image.open(findRecImage), iconWidth, iconHeight)
        self.buttonFind = ttk.Button(databaseCommandsDisplay, image=btnImage, command=parent.findCommand,
                                     style='CommandButton.TButton', takefocus=0)
        self.buttonFind.grid(column=self.columnCount, row=0, sticky=W)
        self.buttonFind.img = btnImage
        self.buttonFind_ttp = CreateToolTip(self.buttonFind, "Find Record(s)")

        self.columnCount = self.columnCount + 1
        btnImage = resizeImage(Image.open(addCommandRecImage), iconWidth, iconHeight)
        self.addCommand = ttk.Button(databaseCommandsDisplay, image=btnImage, command=parent.addCommand,
                                     style='CommandButton.TButton', takefocus=0)
        self.addCommand.grid(column=self.columnCount, row=0, sticky=W)
        self.addCommand.img = btnImage
        self.addCommand_ttp = CreateToolTip(self.addCommand, "Add a New Record")

        self.columnCount = self.columnCount + 1
        btnImage = resizeImage(Image.open(editRecImage), iconWidth, iconHeight)
        self.editCommand = ttk.Button(databaseCommandsDisplay, image=btnImage, command=parent.editCommand,
                                      style='CommandButton.TButton', takefocus=0)
        self.editCommand.grid(column=self.columnCount, row=0, sticky=W)
        self.editCommand.img = btnImage
        self.editCommand_ttp = CreateToolTip(self.editCommand, "Edit Current Record")

        self.columnCount = self.columnCount + 1
        btnImage = resizeImage(Image.open(duplicateRecImage), iconWidth, iconHeight)
        self.duplicateCommand = ttk.Button(databaseCommandsDisplay, image=btnImage, command=parent.duplicateCommand,
                                           style='CommandButton.TButton', takefocus=0)
        self.duplicateCommand.grid(column=self.columnCount, row=0, sticky=W)
        self.duplicateCommand.img = btnImage
        self.duplicateCommand_ttp = CreateToolTip(self.duplicateCommand, "Create Duplicate Record")

        self.columnCount = self.columnCount + 1
        btnImage = resizeImage(Image.open(saveRecImage), iconWidth, iconHeight)
        self.saveCommand = ttk.Button(databaseCommandsDisplay, image=btnImage, command=parent.saveCommand,
                                      style='CommandButton.TButton', takefocus=0)
        self.saveCommand.grid(column=self.columnCount, row=0, sticky=W)
        self.saveCommand.img = btnImage
        self.saveCommand_ttp = CreateToolTip(self.saveCommand, "Save Current Record, or\nExecute Find Command")
        self.saveCommand.bind('<Return>', parent.saveCommand)
        self.saveCommand.config(state='disabled')

        self.columnCount = self.columnCount + 1
        btnImage = resizeImage(Image.open(deleteCommandRecImage), iconWidth, iconHeight)
        self.deleteButton = ttk.Button(databaseCommandsDisplay, image=btnImage,
                                       command= parent.deleteCommand,
                                       style='CommandButton.TButton', takefocus=0)
        self.deleteButton.grid(column=self.columnCount, row=0, sticky=W)
        self.deleteButton.img = btnImage
        self.deleteButton_ttp = CreateToolTip(self.deleteButton, "Delete Current Record")

        self.columnCount = self.columnCount + 1
        btnImage = resizeImage(Image.open(removeFromSetCommandImage), iconWidth, iconHeight)
        self.removeFromSetButton = ttk.Button(databaseCommandsDisplay, image=btnImage,
                                              command=parent.removeFromSetCommand, style='CommandButton.TButton',
                                              takefocus=0)
        self.removeFromSetButton.grid(column=self.columnCount, row=0, sticky=W)
        self.removeFromSetButton.img = btnImage
        self.removeFromSetButton_ttp = CreateToolTip(self.removeFromSetButton, "Remove Record From Current Set")

        self.columnCount = self.columnCount + 1
        btnImage = resizeImage(Image.open(cancelCommandRecImage), iconWidth, iconHeight)
        self.cancelButton = ttk.Button(databaseCommandsDisplay, image=btnImage, command=parent.cancelCommand,
                                       style='CommandButton.TButton', takefocus=0)
        self.cancelButton.grid(column=self.columnCount, row=0, sticky=W)
        self.cancelButton.img = btnImage
        self.cancelButton_ttp = CreateToolTip(self.cancelButton, "Cancel Current Command")

        self.columnCount = self.columnCount + 1
        btnImage = resizeImage(Image.open(refreshRecImage), iconWidth, iconHeight)
        self.buttonRefresh = ttk.Button(databaseCommandsDisplay, image=btnImage, command=parent.refreshWindow,
                                        style='CommandButton.TButton', takefocus=0)
        self.buttonRefresh.grid(column=self.columnCount, row=0, sticky=W)
        self.buttonRefresh.img = btnImage
        self.buttonRefresh_ttp = CreateToolTip(self.buttonRefresh, "Reload Default View\nRefreshes DB Records")

        self.columnCount = self.columnCount + 1
        btnImage = resizeImage(Image.open(undeleteCommandImage), iconWidth, iconHeight)
        self.buttonUndelete = ttk.Button(databaseCommandsDisplay, image=btnImage,
                                         command=parent.unDeleteCommand,
                                         style='CommandButton.TButton', takefocus=0)
        self.buttonUndelete.grid(column=self.columnCount, row=0, sticky=W)
        self.buttonUndelete.img = btnImage
        self.buttonUndelete_ttp = CreateToolTip(self.buttonUndelete, "Undelete DB Records")

    def _setAccessRestriction(self):
        if self.access == 'View':
            self.duplicateCommand.config(state='disabled')
            self.addCommand.config(state='disabled')
            self.editCommand.config(state='disabled')
            self.deleteButton.config(state='disabled')
            self.buttonUndelete.config(state='disabled')

    def emptyDBState(self):
        self.duplicateCommand.config(state='disabled')
        self.buttonFind.config(state='normal')
        self.addCommand.config(state='normal')
        self.editCommand.config(state='disabled')
        self.saveCommand.config(state='disabled')
        self.deleteButton.config(state='disabled')
        self.removeFromSetButton.config(state='disabled')
        self.cancelButton.config(state='normal')
        self.buttonRefresh.config(state='normal')
        self.buttonUndelete.config(state='disabled')
        self._setAccessRestriction()

    def findState(self):
        self.focus()
        self.duplicateCommand.config(state='disabled')
        self.buttonFind.config(state='normal')
        self.addCommand.config(state='disabled')
        self.editCommand.config(state='disabled')
        self.saveCommand.config(state='normal')
        self.deleteButton.config(state='disabled')
        self.removeFromSetButton.config(state='disabled')
        self.cancelButton.config(state='normal')
        self.buttonRefresh.config(state='disabled')
        self.buttonUndelete.config(state='disabled')
        self._setAccessRestriction()

    def addState(self):
        self.focus()
        self.duplicateCommand.config(state='disabled')
        self.buttonFind.config(state='disabled')
        self.addCommand.config(state='disabled')
        self.editCommand.config(state='disabled')
        self.saveCommand.config(state='normal')
        self.deleteButton.config(state='disabled')
        self.removeFromSetButton.config(state='disabled')
        self.cancelButton.config(state='normal')
        self.buttonRefresh.config(state='disabled')
        self.buttonUndelete.config(state='disabled')
        self._setAccessRestriction()

    def editState(self):
        self.focus()
        self.duplicateCommand.config(state='disabled')
        self.buttonFind.config(state='disabled')
        self.addCommand.config(state='disabled')
        self.editCommand.config(state='disabled')
        self.saveCommand.config(state='normal')
        self.deleteButton.config(state='disabled')
        self.removeFromSetButton.config(state='disabled')
        self.cancelButton.config(state='normal')
        self.buttonRefresh.config(state='disabled')
        self.buttonUndelete.config(state='disabled')
        self._setAccessRestriction()

    def deleteState(self):
        self.focus()
        self.duplicateCommand.config(state='disabled')
        self.buttonFind.config(state='normal')
        self.addCommand.config(state='disabled')
        self.editCommand.config(state='disabled')
        self.saveCommand.config(state='disabled')
        self.deleteButton.config(state='disabled')
        self.removeFromSetButton.config(state='normal')
        self.cancelButton.config(state='disabled')
        self.buttonRefresh.config(state='normal')
        self.buttonUndelete.config(state='normal')
        self._setAccessRestriction()

    def defaultState(self):
        self.focus()
        self.duplicateCommand.config(state='normal')
        self.buttonFind.config(state='normal')
        self.addCommand.config(state='normal')
        self.editCommand.config(state='normal')
        self.saveCommand.config(state='disabled')
        self.deleteButton.config(state='normal')
        self.removeFromSetButton.config(state='normal')
        self.cancelButton.config(state='normal')
        self.buttonRefresh.config(state='normal')
        self.buttonUndelete.config(state='disabled')
        self._setAccessRestriction()

    def cancelState(self):
        self.focus()
        self.duplicateCommand.config(state='normal')
        self.buttonFind.config(state='normal')
        self.addCommand.config(state='normal')
        self.editCommand.config(state='normal')
        self.saveCommand.config(state='disabled')
        self.deleteButton.config(state='normal')
        self.removeFromSetButton.config(state='normal')
        self.cancelButton.config(state='normal')
        self.buttonRefresh.config(state='normal')
        self.buttonUndelete.config(state='disabled')
        self._setAccessRestriction()

    def saveState(self):
        self.focus()
        self.duplicateCommand.config(state='normal')
        self.buttonFind.config(state='normal')
        self.addCommand.config(state='normal')
        self.editCommand.config(state='normal')
        self.saveCommand.config(state='disabled')
        self.deleteButton.config(state='normal')
        self.removeFromSetButton.config(state='normal')
        self.cancelButton.config(state='normal')
        self.buttonRefresh.config(state='normal')
        self.buttonUndelete.config(state='disabled')
        self._setAccessRestriction()

    def refreshState(self):
        self.focus()
        self.duplicateCommand.config(state='normal')
        self.buttonFind.config(state='normal')
        self.addCommand.config(state='normal')
        self.editCommand.config(state='normal')
        self.saveCommand.config(state='disabled')
        self.deleteButton.config(state='normal')
        self.removeFromSetButton.config(state='normal')
        self.cancelButton.config(state='normal')
        self.buttonRefresh.config(state='normal')
        self.buttonUndelete.config(state='disabled')
        self._setAccessRestriction()


#        self.buttonAdd = tk.Button(databaseCommandsDisplay, text='Add',  font=buttonFont,
#                                    fg=MyGolfDefaultFGColor,command= lambda: parent.addRecord()) 
#        self.buttonAdd.grid(column=1, row=0, sticky=W)
#        self.buttonAdd.pack(side=LEFT)       
#        new_order = (self.buttonFind, self.buttonAdd, self.buttonEdit, self.buttonDelete, self.buttonSave, self.buttonCancel, self.buttonRefresh)
#        for widget in new_order:
#            widget.lift()

#        courseCommandsDisplay.pack(side = TOP)

#    def disableDatabaseCommands(self, op):
#        if (op == 'add') or (op == 'edit') or (op== 'find'):
#            self.buttonFind.config(state='disabled')
#            self.buttonAdd.config(state='disabled')
#            self.buttonEdit.config(state='disabled')
#            self.buttonDelete.config(state='disabled')
#            self.buttonSave.config(state='normal')
#            self.buttonCancel.config(state='normal')
#        elif op=='del':
#            self.buttonFind.config(state='disabled')
#            self.buttonAdd.config(state='disabled')
#            self.buttonEdit.config(state='disabled')
#            self.buttonDelete.config(state='disabled')
#            self.buttonSave.config(state='disabled')
#            self.buttonCancel.config(state='disabled')

#    def enableDatabaseCommands(self, op):
#        if op == 'default':
#            self.buttonFind.config(state='normal')
#            self.buttonAdd.config(state='normal')
#            self.buttonEdit.config(state='normal')
#            self.buttonDelete.config(state='normal')
#            self.buttonSave.config(state='normal')
#            self.buttonCancel.config(state='normal')

class navDatabaseTool(Frame):
    def __init__(self, parent):
        self.parent=parent
        Frame.__init__(self, parent)
        navDisplay=Frame(self)
        
        self.recNumDisplay = Entry(navDisplay, relief='flat', width=recordNumWidth, disabledforeground=AppDefaultForeground,
                                   background=AppDefaultBackground, disabledbackground=AppDefaultBackground)
        if len(parent.recList) > 0:
            self.myString = '''  Record {0} of {1}'''.format(parent.curRecNumV.get()+1, len(parent.recList))
        else:
            self.myString = '''  Record {0} of {1}'''.format(parent.curRecNumV.get(), len(parent.recList))
        self.recNumDisplay.insert(0, self.myString)
        self.recNumDisplay.pack(side=RIGHT)
        self.recNumDisplay.config(state='disabled')


#        b = tk.Button(self.middleButton, text="Add Addr", font=fontAverage, fg=MyGolfDefaultFGColor,
#                      command=self._startAddrWindow)
#        b.grid(row=0, column=1,  sticky=N+S)        
        nxtRecImage = resizeImage(Image.open(nextRecImage),iconWidth,iconHeight)
        prvRecImage = resizeImage(Image.open(previousRecImage),iconWidth,iconHeight)
        lstRecImage = resizeImage(Image.open(lastRecImage),iconWidth,iconHeight)
        fstRecImage = resizeImage(Image.open(firstRecImage),iconWidth,iconHeight)
                     
        self.buttonLast = ttk.Button(navDisplay, image=lstRecImage, command  = parent.lastRecord, style='CommandButton.TButton', takefocus=0)
        self.buttonLast.img = lstRecImage
        self.buttonLast.pack(side=RIGHT)
        self.buttonLast_ttp = CreateToolTip(self.buttonLast, "Display Last Record")
        
        self.buttonNext = ttk.Button(navDisplay, image=nxtRecImage, command  = parent.nextRecord, style='CommandButton.TButton', takefocus=0)
        self.buttonNext.img = nxtRecImage
        self.buttonNext.pack(side=RIGHT)  
        self.buttonNext_ttp = CreateToolTip(self.buttonNext, "Display Next Record")
        
        self.buttonPrevious = ttk.Button(navDisplay, image=prvRecImage, command  = parent.prvRecord, style='CommandButton.TButton', takefocus=0)
        self.buttonPrevious.img = prvRecImage
        self.buttonPrevious.pack(side=RIGHT)
        self.buttonPrevious_ttp = CreateToolTip(self.buttonPrevious, "Display Previous Record")
        
        self.buttonFirst = ttk.Button(navDisplay, image=fstRecImage, command  = parent.firstRecord, style='CommandButton.TButton', takefocus=0)
        self.buttonFirst.img = fstRecImage
        self.buttonFirst_ttp = CreateToolTip(self.buttonFirst, "Display First Record")
        self.buttonFirst.pack(side=RIGHT)        

        navDisplay.pack()
        
    def updateRecordNumber(self, *args):
        self.recNumDisplay.config(state='normal')
        self.recNumDisplay.delete(0, END)
        if len(self.parent.recList) > 0:
            self.myString = '''  Record {0} of {1}'''.format(self.parent.curRecNumV.get()+1, len(self.parent.recList))
        else:  
            self.myString = '''  Record {0} of {1}'''.format(self.parent.curRecNumV.get(), len(self.parent.recList))
        self.recNumDisplay.insert(0, self.myString)
        self.recNumDisplay.pack(side=RIGHT)
        self.recNumDisplay.config(state='disabled')
                      
    def enableNavigation(self):
        self.buttonFirst.config(state='normal')
        self.buttonLast.config(state='normal')
        self.buttonNext.config(state='normal')
        self.buttonPrevious.config(state='normal')

    def disableNavigation(self):
        self.buttonFirst.config(state='disabled')
        self.buttonLast.config(state='disabled')
        self.buttonNext.config(state='disabled')
        self.buttonPrevious.config(state='disabled')

class PopUpWindow(Toplevel):
    def __init__(self, parent, Mode=None, uniqueID=None, updaterID=None):

        Toplevel.__init__(self, parent)
        self.transient(parent)
        self.uniqueID = uniqueID
        self.mode = Mode
        self.parent = parent
        self.recList = []
        self.updaterID = updaterID

        self.row = 0
        self.column = 0
        self.rowconfigure(self.row, weight=1)
        self.columnconfigure(self.column, weight=1)

        self.result = False
        self.anID = None

        body = AppFrame(self, 1)
        body.noBorder()
        body.grid(row=self.row, column=self.column, sticky=N + S + E + W)

        #        self.mainArea = Tix.Frame(self, border=3, relief='groove', takefocus=0)
        #        self.mainArea.grid(row=self.rowCount, column=self.columnCount, columnspan=self.columnCountTotal, sticky=N+S+E+W)

        self.buttonbox()
        self.msgbox()

        self.initial_focus = self.body(body)

        self.grab_set()

        if not self.initial_focus:
            self.initial_focus = self

        self.protocol("WM_DELETE_WINDOW", self.no)

        self.geometry("+%d+%d" % (parent.winfo_rootx() + 50,
                                  parent.winfo_rooty() + 50))

        self.initial_focus.focus_set()

        self.wait_window(self)

    def _setAsDeleted(self, *args):
        #
        #  At a minimun, record action are disabled.
        #  Additionnal commands can be added, see person class
        #  for override example for this method.
        #
        self.recordFrame.config(highlightcolor=AppDefaultForeground, takefocus=1)
        self.recordFrame.focus()
        if len(self.recList) > 0:
            if self.recList[0][self.numberOfFields - 3] == 'Y':
                self.recordFrame.config(highlightcolor=AppDeletedHighlightColor, takefocus=1)
                self.recordFrame.focus()

    def _displayRecordFrame(self, aFrame):
        #        if len(self.recList) > 0:
        self.recordFrame = AppFrame(aFrame, 1)
        self.recordFrame.grid(row=aFrame.row, column=aFrame.column, columnspan=aFrame.columnTotal, sticky=N + S + E + W)
        self.recordFrame.addAccentBackground()
        self.recordFrame.addDeletedHighlight()
        self.recordFrame.noBorder()

        self._buildWindowsFields(self.recordFrame)
        self.recordFrame.rowconfigure(self.recordFrame.row, weight=0)
        self.displayRec()
        #       else:
        #           aMsg = 'ERROR: Record not found.'
        #           self.myMsgBar.newMessage('error', aMsg)

    def _buildWindowsFields(self, aFrame):
        pass

    def displayRec(self):
        self._setAsDeleted(None)
        pass

    def addRow(self):
        self.row = self.row + 1
        self.rowconfigure(self.row, weight=1)

    def addColumn(self):
        self.column = self.column + 1
        self.columnconfigure(self.column, weight=1)

    def body(self, master):
        # create dialog body.  return widget that should have
        # initial focus.  this method should be overridden

        pass

    def buttonbox(self):
        # add standard button box. override if you don't want the
        # standard buttons

        box = AppFrame(self, 2)
        box.noBorder()

        if self.mode == 'view':
            w = Button(box, text="Exit", style='CommandButton.TButton', command=self.no)
            w.grid(row=0, column=0, columnspan=box.columnTotal, sticky=N)
        else:
            self.saveButton = Button(box, text="Save", style='CommandButton.TButton', command=self.yes,
                                         default=ACTIVE)
            self.saveButton.grid(row=0, column=0, sticky=E + N)
            w = Button(box, text="Cancel", style='CommandButton.TButton', command=self.no)
            w.grid(row=0, column=1, sticky=W + N)

        self.bind("<Return>", self.yes)
        self.bind("<Escape>", self.no)

        self.addRow()
        box.grid(row=self.row, column=self.column, sticky=N + S + E + W)

    def msgbox(self):
        # add standard button box. override if you don't want the
        # standard buttons

        box = AppFrame(self, 1)
        box.noBorder()

        self.myMsgBar = messageBar(box)
        self.myMsgBar.grid(row=0, column=0, sticky=W + S, padx=5)

        self.addRow()
        box.grid(row=self.row, column=self.column, sticky=N + S + E + W)

    def yes(self, event=None):

        if not self.validate():
            self.initial_focus.focus_set()  # put focus back
            return

        self.withdraw()
        self.update_idletasks()

        self.apply()

        self.no()

    def no(self, event=None):

        # put focus back to the parent window
        self.parent.focus_set()
        self.destroy()

    #
    # command hooks

    def validate(self):
        #
        #  If any form validation is required, put them here
        #

        return 1  # override

    def apply(self):

        pass  # override

