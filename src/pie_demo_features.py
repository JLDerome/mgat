from aggdraw import *
from PIL import Image

def drawchart(size, data, range):

    draw = Draw("RGB", size, "white")

    bbox = 10, 10, size[0]-10, size[1]-10

    for lo, hi, ink in data:
        lo = 360.0 * lo / range
        hi = 360.0 * hi / range
        draw.pieslice(bbox, lo, hi, Brush(ink))

    return Image.fromstring(draw.mode, draw.size, draw.tostring())


data = (
    (0, 100, "red"),
    (100, 400, "blue"),
    (400, 500, "green")
    )

im = drawchart((250, 250), data, 500)

im.save("chart.png")