##########################################################
#
#   Personnel Table
#
#   This class implements the specific manipulation of the
# data for the ECE Personnel that the TECH Shop requires.
#

import tkinter.tix as Tix
import tkinter.ttk as ttk
from MyGolfApp2016 import AppCRUD_FROMECECRUD as AppDB
import tkinter.filedialog as Fd
import tkinter.messagebox as Mb
import os, shutil
import random

from tkinter.constants import *

from MyGolfApp2016.AppMyClasses import *

from MyGolfApp2016.AppConstants import *

from MyGolfApp2016.AppProcFROMECE import resizeImage, comparePathEquality, focusNext

from PIL import Image
from ECEShopApp.ECEShopProc import convertDateStringToOrdinal

salutationList = []

class personnelWindow(tableCommonWindowClass):
    def __init__(self, aWindow, aCloseCommand, loginData):
        self.windowTitle = 'Personnel'
        tableCommonWindowClass.__init__(self, aWindow, aCloseCommand, self.windowTitle, loginData[1])
        
        self.loginData = loginData
        self.accessLevel = self.loginData[1]
        self.updaterID = self.loginData[2]
        self.selectedPIC = ''
        rankFullName=AppDB.getRankFullName(self.updaterID)
        self.displayCurUser.setUserName(rankFullName)
#######################################################################################
# Inherited all from tableCommanWindowClass
#  
#  This class intents to change the mainArea only.  Everything else must remain common
#       
        self.mainAreaRowCount = 0
        self.mainAreaColumnCount = 0
        self._getNewRecList()

        self._reDisplayRecordFrame()
        self.mainArea.rowconfigure(self.mainAreaRowCount, weight=1)
       
#        if len(self.recList) > 0:
#            self.displayRec()
#            self.myNavBar.enableNavigation()
#        self._displayPersonnelImage('') 
#        self.mainAreaColumnCount = self.mainAreaColumnCount + 1
#        self._displayPersonnelRecord()       
#        self.mainArea.rowconfigure(self.mainAreaRowCount, weight=1)
               
############################################################ Area to add window specific commands        
        self.closeButton = ttk.Button(self.commandFrame, text='Test', style='CommandButton.TButton', 
                                 command= lambda: self.sampleCommand())
        self.closeButton.grid(row=0, column=0, sticky=N+S)
        
################################################################# Window Specific Methods

    def _enablePersonnelWindowCommand(self):
        self.closeButton.config(state=NORMAL)

    def _disablePersonnelWindowCommand(self):
        self.closeButton.config(state=DISABLED)
               
    def sampleCommand(self):
        self.myMsgBar.clearMessage()
        aMsg = 'INFO: Command area, specific to current window (Parts)'
        self.myMsgBar.newMessage('info', aMsg)

    def _getNewRecList(self):
        if self.accessLevel == 'Root': # Access level
            self.recList = AppDB.getPersonnelROOT()
        else:    
            self.recList = AppDB.getPersonnel()
        self.curRecNumV.set(self.curRecNumV.get())

    def _reDisplayRecordFrame(self):
        self.recordFrame.destroy()
        self._getNewRecList()
        self._displayRecordFrame()
        
    def _displayRecordFrame(self):
        colTotal = 5
        self.recordFrame = AppFrame(self.mainArea, colTotal) 
        self.recordFrame.grid(row=self.mainAreaRowCount, column=self.mainAreaColumnCount, columnspan= self.mainAreaColumnTotal, sticky=N+S+E+W)
        
        if len(self.recList) > 0:
            self._displayPersonnelImage('')  # Will need to be check when records are available (place holder)
            self.recordFrame.column = self.recordFrame.column + 1 
            self._displayPersonnelRecord()
            if self.curOp != 'add':        
                self.displayRec()
                self.navigationEnable(self.accessLevel)
        else:
            wMsg = Tix.Label(self.recordFrame, text='No Personnel Records Found', font=fontMonstrousB)
            wMsg.grid(row=0, column=0, columnspan=self.recordFrame.columnTotal)

    def _newPICEntered(self, *args):
#        PIC = '{0}/{1}'.format(personnelPICPath, self.PIC_var.get())     
#        self.picArea = AppPICFrame(self.recordFrame)        
#        self.picArea.grid(row=0, column= 0)
#        original = Image.open(PIC)
#        self.panel1.destroy()
#        image1 = resizeImage(original,personnelPicWidth,personnelPicHeight)                
#        self.panel1 = Tix.Label(self.picArea, image=image1 )
#        self.panel1.grid(row = 0, column=0, sticky=N+W)
#        self.display = image1
#        self.panel1.bind('<Button-1>', self._findPictureFile)
#        self.pic_ttp = CreateToolTip(self.panel1, "Click to select a new picture")
#        self.picArea.enable()
#        self.selectedEmployeeType.focus()
        self.selectedPIC = self.PIC_var.get()
        self.picArea.destroy()
        self._displayPersonnelImage(self.selectedPIC)
        self.picArea.enable()
        
    def _reActivatePersonnelImage(self, IMG):
        self.selectedPIC = IMG
        self.picArea.destroy()
        self._displayPersonnelImage(self.selectedPIC)
        self.picArea.enable()
       
    def _displayPersonnelImage(self, IMG):
        try:
            im1=Image.open('{0}/{1}'.format(appPICDir,IMG))
            PIC = '{0}/{1}'.format(personnelPICPath, IMG)
        except IOError:
            PIC = personnelDefaultPic

#        self.picArea = Tix.Frame(self.recordFrame, border=3, relief='ridge', width=personnelPicWidth, height=personnelPicHeight)        
        self.picArea = AppPICFrame(self.recordFrame)        
        self.picArea.grid(row=0, column= 0, sticky=N)
        original = Image.open(PIC)
        image1 = resizeImage(original,personnelPicWidth,personnelPicHeight)                
        self.panel1 = Tix.Label(self.picArea, image=image1 )
        self.panel1.grid(row = 0, column=0, sticky=N+W)
        self.display = image1
        self.picArea.disable()
        
        if self.curOp == 'add' or self.curOp == 'edit':
            self.PIC_var = Tix.StringVar()
            self.PIC_var.set('')
            self.PIC_var.trace('w', self._newPICEntered)
            self.panel1.bind('<Button-1>', self._findPictureFile)
            self.pic_ttp = CreateToolTip(self.panel1, "Click to select a new picture")
            self.picArea.enable()
            
    def displayRec(self, *args):
        #   IDX CREATE TABLE Personnel(
        #    0  personnelID             INTEGER PRIMARY KEY AUTOINCREMENT  NOT NULL,
        #    1  personnelFNAME          CHAR(30)  NOT NULL,
        #    2  personnelLNAME          CHAR(30)  NOT NULL,
        #    3  personnelType           CHAR(4),
        #    4  personnelSalutation     CHAR(6),
        #    5  personnelPIC_LOC        CHAR(100),
        #    6  pri_SN                  CHAR(11),
        #    7  officeNumber            CHAR(9),
        #    8  extension               CHAR(5),
        #    9  ECEWorkGroup            CHAR(30),
        #   10  title_Position          CHAR(30),
        #   11  loginName               CHAR(20),
        #   12  password                CHAR(30),
        #   13  access_Level            CHAR(15),
        #   14  lastModified            INTEGER,
        #   15  updaterID               INTEGER
        #      );''')
        self.selectedFname.load(self.recList[self.curRecNumV.get()][1])
        self.selectedLname.load(self.recList[self.curRecNumV.get()][2])
        self.selectedEmployeeType.load(self.recList[self.curRecNumV.get()][3])
        self.selectedRank.load(self.recList[self.curRecNumV.get()][4])       
        self._displayPersonnelImage(self.recList[self.curRecNumV.get()][5])
        self.selectedPRI.load(self.recList[self.curRecNumV.get()][6])
        self.selectedOffice.load(self.recList[self.curRecNumV.get()][7])
        self.selectedExtension.load(self.recList[self.curRecNumV.get()][8])
        self.selectedEmployment.load(self.recList[self.curRecNumV.get()][9])
        self.selectedPosition.load(self.recList[self.curRecNumV.get()][10])
        self.lastUpdate.load(self.recList[self.curRecNumV.get()][14], self.recList[self.curRecNumV.get()][15])
        
        if self.accessLevel == 'Root':
            self.selectedLoginName.load(self.recList[self.curRecNumV.get()][11])
            self.selectedPassword.load(self.recList[self.curRecNumV.get()][12])
            self.selectedAccess.load(self.recList[self.curRecNumV.get()][13])
                  
        self.fieldsDisable()
            
            
    def _displayPersonnelRecord(self):
        self.recordDetailsAreaRowCount = 0
        self.recordDetailsAreaColumnCount = 0
        self.recordDetailsArea = Tix.Frame(self.recordFrame)        
        self.recordDetailsArea.grid(row=0, column= 1, columnspan= self.recordFrame.columnTotal-1, sticky=N+S+E+W)
        self.recordDetailsAreaColumnTotal = 5
        for i in range( self.recordDetailsAreaColumnTotal):
            self.recordDetailsArea.columnconfigure(i, weight=1)
            
############################################################################## Field Headers Row 1   
        self.recordDetailsArea.rowconfigure(self.recordDetailsAreaRowCount, weight=0)

        headerList=['Type', 'Rank', 'First Name', 'Group', 'Office']
        
        for i in range(len(headerList)):       
            aLabel = ttk.Label(self.recordDetailsArea, text=headerList[i], style='RegularFieldTitle.TLabel')
            aLabel.grid(row=self.recordDetailsAreaRowCount, column=self.recordDetailsAreaColumnCount, sticky=N+S)
            self.recordDetailsAreaColumnCount = self.recordDetailsAreaColumnCount + 1
        
############################################################################## Fields            
        self.recordDetailsAreaRowCount = self.recordDetailsAreaRowCount + 1
        self.recordDetailsArea.rowconfigure(self.recordDetailsAreaRowCount, weight=0)
        self.recordDetailsAreaColumnCount = 0

        employeeTypeV = Tix.StringVar()
        employeeTypeV.trace('w',self._employeeTypeEntered)
        self.selectedEmployeeType = AppCBVar(self.recordDetailsArea, employeeTypeV, employeeTypeList, employeeTypeWidth)
        self.selectedEmployeeType.grid(row=self.recordDetailsAreaRowCount, column=self.recordDetailsAreaColumnCount, sticky=N+S)
        self.selectedEmployeeType.disable()
        
        self.recordDetailsAreaColumnCount = self.recordDetailsAreaColumnCount + 1
#        rankV = Tix.StringVar()
#        rankV.trace('w',self._rankEntered)
        self.selectedRank = AppCB(self.recordDetailsArea, [], rankWidth)
        self.selectedRank.grid(row=self.recordDetailsAreaRowCount, column=self.recordDetailsAreaColumnCount, sticky=N+S)
        self.selectedRank.disable()
        
        self.recordDetailsAreaColumnCount = self.recordDetailsAreaColumnCount + 1
        self.selectedFname = AppEntry(self.recordDetailsArea, nameWidth)
        self.selectedFname.grid(row=self.recordDetailsAreaRowCount, column=self.recordDetailsAreaColumnCount, sticky=N+S)
        self.selectedFname.disable()

        self.recordDetailsAreaColumnCount = self.recordDetailsAreaColumnCount + 1
        employmentV = Tix.StringVar()
        employmentV.trace('w',self._employentEntered)
        self.selectedEmployment = AppCBVar(self.recordDetailsArea, employmentV, employeeEmploymentList, employmentWidth)
        self.selectedEmployment.grid(row=self.recordDetailsAreaRowCount, column=self.recordDetailsAreaColumnCount, sticky=N+S)
        self.selectedEmployment.disable()
        
        self.recordDetailsAreaColumnCount = self.recordDetailsAreaColumnCount + 1
        self.selectedOffice = AppEntryCentered(self.recordDetailsArea, officeWidth)
        self.selectedOffice.grid(row=self.recordDetailsAreaRowCount, column=self.recordDetailsAreaColumnCount, sticky=N+S)
        self.selectedOffice.disable()           
                
############################################################################## Field Headers Row 2   
        self.recordDetailsAreaRowCount = self.recordDetailsAreaRowCount + 1
        self.recordDetailsAreaColumnCount = 0
        
        headerList=['','PRI/SN/Col #', 'Last Name', 'Position', 'Extension'] 
        
        for i in range(len(headerList)):       
            aLabel = ttk.Label(self.recordDetailsArea, text=headerList[i], style='RegularFieldTitle.TLabel')
            if headerList[i] == 'PRI/SN/Col #':
                aLabel.grid(row=self.recordDetailsAreaRowCount, column=self.recordDetailsAreaColumnCount, 
                            columnspan = 2, sticky=N+S)
                self.recordDetailsAreaColumnCount = self.recordDetailsAreaColumnCount + 1
            elif headerList[i] != '':
                aLabel.grid(row=self.recordDetailsAreaRowCount, column=self.recordDetailsAreaColumnCount, sticky=N+S)
            if headerList[i] != '':               
                self.recordDetailsAreaColumnCount = self.recordDetailsAreaColumnCount + 1
        self.recordDetailsArea.rowconfigure(self.recordDetailsAreaRowCount, weight=0)
           
############################################################################## Fields            
        self.recordDetailsAreaRowCount = self.recordDetailsAreaRowCount + 1
        self.recordDetailsArea.rowconfigure(self.recordDetailsAreaRowCount, weight=0)
        self.recordDetailsAreaColumnCount = 0
        
#        for i in range(len(headerList)):
#            if headerList[i] == '':
#                self.recordDetailsAreaColumnCount = self.recordDetailsAreaColumnCount + 1
                
        self.selectedPRI= AppEntryCentered(self.recordDetailsArea, priWidth)
        self.selectedPRI.grid(row=self.recordDetailsAreaRowCount, column=self.recordDetailsAreaColumnCount, 
                                columnspan = 2, sticky=N+S)
        self.selectedPRI.disable()

        self.recordDetailsAreaColumnCount = self.recordDetailsAreaColumnCount + 2        
        self.selectedLname= AppEntry(self.recordDetailsArea, nameWidth)
        self.selectedLname.grid(row=self.recordDetailsAreaRowCount, column=self.recordDetailsAreaColumnCount, sticky=N+S)
        self.selectedLname.disable()

        self.recordDetailsAreaColumnCount = self.recordDetailsAreaColumnCount + 1
#        positionV = Tix.StringVar()
#        positionV.trace('w',self._positionEntered)
        self.selectedPosition = AppCB(self.recordDetailsArea, positionList, positionWidth)
        self.selectedPosition.grid(row=self.recordDetailsAreaRowCount, column=self.recordDetailsAreaColumnCount, sticky=N+S)
        self.selectedPosition.disable()
        
        self.recordDetailsAreaColumnCount = self.recordDetailsAreaColumnCount + 1
        self.selectedExtension = AppEntryCentered(self.recordDetailsArea, extensionWidth)
        self.selectedExtension.grid(row=self.recordDetailsAreaRowCount, column=self.recordDetailsAreaColumnCount, sticky=N+S)
        self.selectedExtension.disable()
                    
############################################################################## Field Headers Row 3
#  Display only if user has root access
        if self.accessLevel == 'Root':
            self.recordDetailsAreaRowCount = self.recordDetailsAreaRowCount + 1
            self.recordDetailsArea.rowconfigure(self.recordDetailsAreaRowCount, weight=0)
            self.recordDetailsAreaColumnCount = 0
            
            myHeaderList=['Login Name', 'Password', 'Password']
            self.rootDataFrame = AppRootFrame(self.recordDetailsArea, len(myHeaderList))
            self.rootDataFrame.grid(row=self.recordDetailsAreaRowCount, column=self.recordDetailsAreaColumnCount, columnspan=len(headerList), sticky=E+W+N+S)
            
            #################################################################### Display Header
            for i in range(len(myHeaderList)):       
                aLabel = ttk.Label(self.rootDataFrame, text=myHeaderList[i], style='RegularFieldTitle.TLabel')
                aLabel.grid(row=self.rootDataFrame.row, column=self.rootDataFrame.column, sticky=N+S)
                self.rootDataFrame.column = self.rootDataFrame.column + 1
                
            #################################################################### Display Fields
                
            self.rootDataFrame.row = self.rootDataFrame.row + 1
            self.rootDataFrame.rowconfigure(self.rootDataFrame.row, weight = 1)

            self.rootDataFrame.column = 0
            self.selectedLoginName = AppEntry(self.rootDataFrame, loginNameWidth)
            self.selectedLoginName.grid(row=self.rootDataFrame.row, column=self.rootDataFrame.column, sticky=N+S)
            self.selectedLoginName.disable()
            
            self.rootDataFrame.column = self.rootDataFrame.column + 1
#            accessV = Tix.StringVar()
#            accessV.trace('w',self._accessEntered)
            self.selectedAccess = AppCB(self.rootDataFrame, accessLevelList, accessWidth)
            self.selectedAccess.grid(row=self.rootDataFrame.row, column=self.rootDataFrame.column, sticky=N+S)
            self.selectedAccess.disable()
            
            self.rootDataFrame.column = self.rootDataFrame.column + 1
            self.selectedPassword = AppEntry(self.rootDataFrame, passwordWidth)
            self.selectedPassword.grid(row=self.rootDataFrame.row, column=self.rootDataFrame.column, sticky=N+S)
            self.selectedPassword.disable()
            
######################################################## Last Update Row
        self.recordDetailsAreaRowCount = self.recordDetailsAreaRowCount + 1
        self.recordDetailsArea.rowconfigure(self.recordDetailsAreaRowCount, weight=0)
        self.recordDetailsAreaColumnCount = 0
            
        self.lastUpdate = AppLastUpdateFormat(self.recordDetailsArea, self.recList[self.curRecNumV.get()][14], self.recList[self.curRecNumV.get()][15])
        self.lastUpdate.grid(row=self.recordDetailsAreaRowCount,column=self.recordDetailsAreaColumnCount, columnspan = self.recordDetailsAreaColumnTotal, sticky=E+N+S)

                                   
        if self.accessLevel == 'Root':
            new_order = (self.picArea, self.selectedEmployeeType, self.selectedRank, self.selectedPRI, self.selectedFname, self.selectedLname, self.selectedEmployment, 
                         self.selectedPosition, self.selectedOffice, self.selectedExtension, self.selectedLoginName, self.selectedPassword, self.selectedAccess)
        else:
            new_order = (self.picArea, self.selectedEmployeeType, self.selectedRank, self.selectedPRI, self.selectedFname, self.selectedLname, self.selectedEmployment, 
                         self.selectedPosition, self.selectedOffice, self.selectedExtension)
        
        for widget in new_order:
            widget.lift()

    def _employeeTypeEntered(self, *args):
        self._updateRankList()
               
    def _employentEntered(self, *args):
        pass
        
    def _accessEntered(self, *args):
        pass
        
    def _positionEntered(self, *args):
        pass
    
    def fieldsEnable(self):
        if self.curOp == 'edit':
            self._reActivatePersonnelImage(self.recList[self.curRecNumV.get()][5])
        self.selectedEmployeeType.enable()
        self.selectedRank.enable()
        self.selectedFname.enable()
        self.selectedLname.enable()
        self.selectedEmployment.enable()
        self.selectedPosition.enable()
        self.selectedOffice.enable()
        self.selectedExtension.enable()
        self.selectedPRI.enable()
        if self.accessLevel == 'Root':
            self.selectedPassword.enable()
            self.selectedLoginName.enable()
            self.selectedAccess.enable()
       
    def fieldsDisable(self):
        self.picArea.disable()
        self.selectedEmployeeType.disable()
        self.selectedRank.disable()
        self.selectedFname.disable()
        self.selectedLname.disable()
        self.selectedEmployment.disable()
        self.selectedPosition.disable()
        self.selectedOffice.disable()
        self.selectedExtension.disable()
        self.selectedPRI.disable()
        if self.accessLevel == 'Root':
            self.selectedPassword.disable()
            self.selectedLoginName.disable()
            self.selectedAccess.disable()
            
    def fieldsClear(self):
#        self.picArea.disable()
        self.picArea.destroy()
        self._displayPersonnelImage(self.selectedPIC)
        self.picArea.enable()
        self.selectedEmployeeType.clear()
        self.selectedRank.clear()
        self.selectedFname.clear()
        self.selectedLname.clear()
        self.selectedEmployment.clear()
        self.selectedPosition.clear()
        self.selectedOffice.clear()
        self.selectedExtension.clear()
        self.selectedPRI.clear()
        if self.accessLevel == 'Root':
            self.selectedPassword.clear()
            self.selectedLoginName.clear()
            self.selectedAccess.clear()
              
    def _updateRankList(self):
        self.selectedRank.disable()
        if self.selectedEmployeeType.get() == 'Civ':
            self.selectedRank.updateValuesList(rankCivilianList)
            self.fieldsEnable()
        elif self.selectedEmployeeType.get() == 'Mil-Army': # 'Mil-Air', 'Mil-Army', 'Mil-Navy'
            self.selectedRank.updateValuesList(rankArmyList)
            self.fieldsEnable()
        elif self.selectedEmployeeType.get() == 'Mil-Air': # 'Mil-Air', 'Mil-Army', 'Mil-Navy'
            self.selectedRank.updateValuesList(rankAirList)
            self.fieldsEnable()
        elif self.selectedEmployeeType.get() == 'Mil-Navy': # 'Mil-Air', 'Mil-Army', 'Mil-Navy'
            self.selectedRank.updateValuesList(rankNavyList)
            self.fieldsEnable()

    def _rankEntered(self, *args):
        pass
                   
    def _findPictureFile(self, *args):
#        #
#        #  Select existing image in the media image dir or a personal image.
#        #  Will verify if image is good and will copy it to the media directory.
#        #  If file exist, will change the name to avoid conflicts or losses.
#        #
        self.myMsgBar.clearMessage()
#        fileExist=False  # will be use if a filename is the same
        ftypes = myImageFileFormats
        dlg = Fd.Open(self, initialdir=appPICDir, filetypes = ftypes)
        imageFileSelectedFullPath = dlg.show()
        try:
            if os.path.exists(imageFileSelectedFullPath):
                im1=Image.open(imageFileSelectedFullPath) # Verifies we have a good image
                pathToImageFile, imageFileSelected = os.path.split(imageFileSelectedFullPath)
                #
                # If image comes from the default App Image folder
                # just save the image filename. Otherwise, copy the image in the
                # App Image folder and save file name.
                #
                equality = comparePathEquality(appPICDir, pathToImageFile)
                if equality == True:
                    self.PIC_var.set(imageFileSelected)
                else:
                    #
                    #  If not, does the filename exist in the 
                    #  target directory
                    #
                    normalizedPath = os.path.normpath(appPICDir)  # uses all backslashes for consistency
                    fullImagePathName = "{0}\{1}".format(normalizedPath, imageFileSelected)
                    while (os.path.exists(fullImagePathName)):
                        # generate a random number to modify file name
                        randomNum = int(random.random()*10000)
                        filename, file_extension = os.path.splitext(imageFileSelected)
                        imageFileSelected = "{0}{1}{2}".format(filename,str(randomNum),file_extension)
                        fullImagePathName = "{0}\{1}".format(normalizedPath, imageFileSelected)
#                    filePath = "{0}/{1}".format(appPICDir,imageFileSelected)
                    newFullImagePathName = "{0}\{1}".format(normalizedPath, imageFileSelected)
                    shutil.copy2(imageFileSelectedFullPath, newFullImagePathName)
                    self.PIC_var.set(imageFileSelected)
            else:
                self.PIC_var.set('')
        except IOError:
            aMsg = '''ERROR: Unable to display this image. Please select another image.'''
            self.myMsgBar.newMessage('error', aMsg)
            self.PIC_var.set('')

################################################################# Redefined commands
#
#  invoke from the tableCommonWindows.  These commands are specific to each tables
#  The specific actions are redefinded here.
#       
#    def displayRec(self, *args):
#        print("Displaying a Record: ", self.curRecNumV.get())
#        self._displayRecordFrame()
    def duplicate(self):
        self.fieldsEnable()
    
    def refresh(self):
        self._reDisplayRecordFrame()

    def delete(self):
        self._disablePersonnelWindowCommand()
        if self.recList[self.curRecNumV.get()][0] == 1: # Root can not deleted
            aMsg = "WARNING: System Master User can not be deleted."
            self.myMsgBar.newMessage('warning', aMsg)
        else:
            aMsg = "Are you sure you want to delete record {0} {1}?".format(self.recList[self.curRecNumV.get()][1],self.recList[self.curRecNumV.get()][2])
            ans = Mb.askyesno('Delete Record', aMsg)
            if ans == True:
                AppDB.deletePersonnel(self.recList[self.curRecNumV.get()][0])
                aMsg = "Record {0} {1} has been deleted.".format(self.recList[self.curRecNumV.get()][1],self.recList[self.curRecNumV.get()][2])
                self.myMsgBar.newMessage('info', aMsg)
                self._getNewRecList()
                if self.curRecBeforeOp != 0:
                    self.curRecNumV.set(self.curRecBeforeOp - 1)
                self._reDisplayRecordFrame()
        self._enablePersonnelWindowCommand()     
        self.navigationEnable(self.accessLevel)
        self.myDatabaseBar.defaultState()    
        self.curOp = 'default'
                 
    def edit(self):
        self.fieldsEnable()
                 
    def add(self):
#        self._reDisplayRecordFrame() # Display the record area in Add operation instead
        self.fieldsClear()
        self.selectedEmployeeType.enable()
        self.selectedPIC = ''
     
    def save(self):
        #
        #  Table specific.  Return a True if succesful saved.
        #
        if self.curOp == 'add':
            
            if self.accessLevel == 'Root': 
                AppDB.insertPersonnelRoot(self.selectedFname.get(), self.selectedLname.get(), self.selectedEmployeeType.get(), self.selectedRank.get(), self.selectedPIC,
                                          self.selectedPRI.get(), self.selectedOffice.get(), self.selectedExtension.get(), self.selectedEmployment.get(), self.selectedPosition.get(),  
                                          self.selectedLoginName.get(), self.selectedPassword.get(), self.selectedAccess.get(), convertDateStringToOrdinal(todayDate), self.updaterID)
            else:
                AppDB.insertPersonnel(self.selectedFname.get(), self.selectedLname.get(), self.selectedEmployeeType.get(), self.selectedRank.get(), self.selectedPIC,
                                          self.selectedPRI.get(), self.selectedOffice.get(), self.selectedExtension.get(), self.selectedEmployment.get(), self.selectedPosition.get(),  
                                          '', '', '', convertDateStringToOrdinal(todayDate), self.updaterID)
                
            self._getNewRecList()
            self._reDisplayRecordFrame()
            
        elif self.curOp == 'edit': 
            if self.accessLevel == 'Root': 
                AppDB.updatePersonnel(self.recList[self.curRecNumV.get()][0], self.selectedFname.get(), self.selectedLname.get(), self.selectedEmployeeType.get(), self.selectedRank.get(),
                                      self.selectedPIC, self.selectedPRI.get(), self.selectedOffice.get(), self.selectedExtension.get(), self.selectedEmployment.get(),
                                      self.selectedPosition.get(), self.selectedLoginName.get(), self.selectedPassword.get(), self.selectedAccess.get(), 
                                      convertDateStringToOrdinal(todayDate), self.updaterID
                                     )
            else:
                AppDB.updatePersonnel(self.recList[self.curRecNumV.get()][0], self.selectedFname.get(), self.selectedLname.get(), self.selectedEmployeeType.get(), self.selectedRank.get(),
                                      self.selectedPIC, self.selectedPRI.get(), self.selectedOffice.get(), self.selectedExtension.get(), self.selectedEmployment.get(),
                                      self.selectedPosition.get(), self.recList[self.curRecNumV.get()][11], self.recList[self.curRecNumV.get()][12], self.recList[self.curRecNumV.get()][13], 
                                      convertDateStringToOrdinal(todayDate), self.updaterID
                                     )
                                
            self._getNewRecList()
            self._reDisplayRecordFrame()
        return True
            