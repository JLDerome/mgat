import tkinter.tix as Tix
import tkinter.filedialog as Fd
import random, shutil
from tkinter import *
import tkinter.ttk as ttk
import textwrap
from tkinter.constants import *
from AppConstants import *
from AppProc import resizeImage, widgetEnter,convertOrdinaltoString, focusNext
from AppProc import getAge, convertDateStringToOrdinal, updateMyAppConstant
from AppProc import stripChar
from PIL import Image, ImageTk
from PIL.Image import NORMAL
import AppCRUDGolfers as CRUDGolfers
import AppCRUDAddresses as CRUDAddresses
import AppCRUDExpenses as CRUDExpenses
from AppCRUDGames import calculateCurrentHdcp
from ECEDialogClass import AppDisplayAbout, AppRecordDuplication, AppQuestionRequest
import calendar, re
import platform
from dateutil import parser


class popUpWindowClass(Tix.Frame):
    def __init__(self, parent, aCloseCommand, windowTitle, loginData):
        Tix.Frame.__init__(self, parent)
        self.accessLevel = loginData[1]
        self.CloseMe = aCloseCommand
        self.parent = parent
        self.curRecNumV = Tix.IntVar()
        self.curRecNumV.set(0)
        self.curRecBeforeOp = self.curRecNumV.get()
        self.prevFind = False
        self.curFind = False
        self.curOp = 'default'
        self.recList = []
        self.accessLevel = ''
        self.updaterID = 0
        self.updaterID = ''
        self.updaterFullname = ''
        self.mainAreaRowCount = 0
        self.mainAreaColumnCount = 0
        self.findValues = []
        self.viewSet = Tix.StringVar()
        self.viewSet.set("Active")  # Active, All, or Closed
        self.sortFieldID = 0  # Last Name
        self.sortID = Tix.StringVar()
        self.sortID.set('Default')  # Department, Workgroup, Last Name
        self.deletedV = Tix.StringVar()
        self.deletedV.trace('w', self._setAsDeleted)
        ############################################################ Window Common Configuration
        self.dirty = False
        mainWInWidth = 400
        mainWinHeight = 400
        self.configure(width=mainWInWidth, height=mainWinHeight)
        self.columnCountTotal = 7
        for i in range(self.columnCountTotal):
            self.columnconfigure(i, weight=1)

        ############################################################ Window Title Area
        self.rowCount = 0
        self.columnCount = 0
        wMsg = Tix.Label(self, text=windowTitle, font=fontMonstrousB)
        wMsg.grid(row=self.rowCount, column=self.columnCount, columnspan=self.columnCountTotal, sticky=E + W)
        self.rowconfigure(self.rowCount, weight=0)

        self.displayCurFunction = AppFunctionDisplayLabel(self)
        self.displayCurFunction.grid(row=self.rowCount, column=self.columnCount, columnspan=self.columnCountTotal,
                                     sticky=E + S)

        self.displayCurUser = AppUserDisplayLabel(self, " ")
        self.displayCurUser.grid(row=self.rowCount, column=self.columnCount, columnspan=self.columnCountTotal,
                                 sticky=W + S)

        ############################################################ Main Frame Area
        self.rowCount = self.rowCount + 1
        self.columnCount = 0
        self.mainArea = Tix.Frame(self, border=3, relief='groove', takefocus=0)
        self.mainArea.grid(row=self.rowCount, column=self.columnCount, columnspan=self.columnCountTotal,
                           sticky=N + S + E + W)
        self.mainAreaColumnTotal = 21
        for i in range(self.mainAreaColumnTotal):
            self.mainArea.columnconfigure(i, weight=1)
        self.rowconfigure(self.rowCount, weight=1)

        ############################################################ Command Frame Area
        self.rowCount = self.rowCount + 1
        self.columnCount = 0

        #  This frame is used for commands specific to current class
        self.columnCount = self.columnCount + 1
        self.commandFrame = Tix.Frame(self)
        self.commandFrame.grid(row=self.rowCount, column=self.columnCount, columnspan=self.columnCountTotal - 2,
                               sticky=N + S)
        self.viewSortUpdate = AppViewSortFormat(self.commandFrame)
        self.viewSortUpdate.grid(row=0, column=0, sticky=E + N + S)
#        self.viewSet.trace('w', self.updateViewSortDisplay)
#        self.sortID.trace('w', self.updateViewSortDisplay)

        self.rowconfigure(self.rowCount, weight=0)

        ############################################################ Message Bar Area
        self.rowCount = self.rowCount + 1
        self.columnCount = 0
        self.myMsgBar = messageBar(self)
        self.myMsgBar.grid(row=self.rowCount, column=self.columnCount, columnspan=self.columnCountTotal, sticky=W + S,
                           padx=5)
        self.rowconfigure(self.rowCount, weight=0)

        ########################################################### Display records
        self.recordFrame = Tix.Frame(self.mainArea, takefocus=0)
        self.recordFrame.grid(row=0, column=0, columnspan=self.mainAreaColumnTotal, sticky=N + S + E + W)


        wMsg = Tix.Label(self.recordFrame, text='No Records Found', font=fontMonstrousB)
        wMsg.grid(row=0, column=0)


        ############################################################ Common Window Binding
        self.parent.bind('<End>', self.endKeyPressed)
        self.createMenu()

    #        self.loginWindow.bind('<Home>', self.focusLoginName)

    ############################################################ Table Specific Commands
    #
    #   These commands will be redefined for each table that invokes this class
    #   Manipulation of data is specific to each tables.
    #
    def createMenu(self):
        self.menubar = Menu(self.parent, foreground=AppDefaultForeground, font=AppDefaultMenuFont)
        self.filemenu = Menu(self.menubar, foreground=AppDefaultForeground, font=AppDefaultMenuFont, tearoff=0)
        #        filemenu.add_command(label="Close", command=self.donothing)
        self.filemenu.add_separator()
        self.filemenu.add_command(label="Exit", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                  command=self.CloseMe)
        self.menubar.add_cascade(label="File", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                 menu=self.filemenu)

        self.tableSpecificMenu()

        self.helpmenu = Menu(self.menubar, tearoff=0)
        self.helpmenu.add_command(label="About...", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                  command=lambda: self.displayAbout())
        self.menubar.add_cascade(label="Help", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                 menu=self.helpmenu)

        self.parent.config(menu=self.menubar)

    def displayAbout(self):
        # AppDisplayAbout(self)
        print("Working on it.....")

    def _setAsDeleted(self, *args):
        pass
        #
        #  At a minimun, record action are disabled.
        #  Additionnal commands can be added, see person class
        #  for override example for this method.
        #
 #       if self.recList[self.curRecNumV.get()][self.numberOfFields - 3] == 'Y':
 #           self.myDatabaseBar.deleteState()
 #           self.recordFrame.config(highlightcolor=AppDeletedHighlightColor, takefocus=0)
 #           self.recordFrame.focus()
 #       else:
 #           self.myDatabaseBar.defaultState()
 #           self.recordFrame.config(highlightcolor=AppDefaultForeground, takefocus=0)
 #           self.recordFrame.focus()

    def endKeyPressed(self, Event):
        pass
#        self.myDatabaseBar.saveCommand.focus()

    def sort(self):
        pass

    def getTableData(self):
        pass  # Table specific

    def _getNewRecList(self):
        self.getTableData()
        self.curRecNumV.set(self.curRecNumV.get())

        if len(self.recAll) > 0:
            self.numberOfFields = len(self.recAll[0])
        else:
            self.numberOfFields = 0
        self.recList = self.recAll
        self.curRecNumV.set(self.curRecNumV.get())  # forces display of record number

    def _reDisplayRecordFrame(self):
        self.recordFrame.destroy()
        self._displayRecordFrame()

    def _buildWindowsFields(self, aFrame):
        pass

    def _displayRecordFrame(self):
        self.recordFrame = AppFrame(self.mainArea, 1)
        self.recordFrame.grid(row=self.mainAreaRowCount, column=self.mainAreaColumnCount,
                              columnspan=self.mainAreaColumnTotal, sticky=N + S + E + W)
        self.recordFrame.addAccentBackground()
        self.recordFrame.addDeletedHighlight()
        self.recordFrame.noBorder()
        self.recordFrame.stretchCurrentRow()

        self._buildWindowsFields(self.recordFrame)
        self.recordFrame.rowconfigure(self.recordFrame.row, weight=1)
#            self.navigationEnable(self.accessLevel)
#            self.basicViewsList('Active')
#        self.displayRec()

#        elif self.curOp == 'add':
#            self._buildWindowsFields(self.recordFrame)
#            self.recordFrame.rowconfigure(self.recordFrame.row, weight=1)#

#        else:
#            wMsg = Tix.Label(self.recordFrame, text='No Records Found in Current List.', font=fontMonstrousB)
#            wMsg.grid(row=0, column=0, columnspan=self.recordFrame.columnTotal)
#
#   MUST BE REVIEWED
#
    def basicViewsList(self, viewID):
        if self.curOp == 'add' or self.curOp == 'edit':
            aMsg = "ERROR: Invalid request. Compete add/edit operation before changing views."
            self.myMsgBar.newMessage('error', aMsg)
        else:
            pass

class addEditGamesPopUp(popUpWindowClass):
    def __init__(self, aWindow, aCloseCommand, mode, expenseID, updateExpenseList, loginData):
        #
        #  loginInfo format: password, access_Level, loginID, updaterID
        #
        if mode == 'add':
            self.windowTitle = 'Add A Game'
        elif mode == 'edit':
            self.windowTitle = 'Edit A Game'
        else:
            self.windowTitle = 'An Error Occur in the Call'
        popUpWindowClass.__init__(self, aWindow, aCloseCommand, self.windowTitle, loginData)
        self.tableName = 'PopupExpenses'
        self.aCloseCommand = aCloseCommand
        self.aWindow = aWindow
        self.loginData = loginData

        self.expenseID = expenseID
        self.updateExpenseList = updateExpenseList
        self.thisExpense = []

        self.configDataRead = updateMyAppConstant(AppConfigFile)
        self.golferComboBoxData = CRUDGolfers.getGolferCB()
        self.golferDictionary = {}
        self.reverseGolferDictionary = {}
        for i in range(len(self.golferComboBoxData)):
            golfer_full = '''{0} {1}'''.format(self.golferComboBoxData[i][1], self.golferComboBoxData[i][2])
            self.golferDictionary.update({golfer_full: self.golferComboBoxData[i][0]})
            self.reverseGolferDictionary.update({self.golferComboBoxData[i][0]: golfer_full})

        #
        #  loginInfo format: password, access_Level, loginID, updaterID
        #
        if loginData[2] == 1:
            self.updaterFullname = "Super User"
        else:
            self.updaterFullname = CRUDGolfers.getGolferFullname(loginData[3])
        self.updaterID = self.loginData[2]
        self.displayCurUser.setUserName(self.updaterFullname)

        #######################################################################################
        # Inherited all from tableCommanWindowClass
        #
        #  This class intents to change the mainArea only.  Everything else must remain common
        #
        self.mainAreaRowCount = 0
        self.mainAreaColumnCount = 0
        self._reDisplayRecordFrame()
        self.mainArea.rowconfigure(self.mainAreaRowCount, weight=1)
 #       self.windowViewDefault()

    ############################################################ Area to add window specific commands
    #        self.closeButton = ttk.Button(self.commandFrame, text='Test', style='CommandButton.TButton',
    #                                 command= lambda: self.sampleCommand())
    #        self.closeButton.grid(row=0, column=0, sticky=N+S)


    ################################################################# Redefined commands
    #
    #  invoke from the tableCommonWindows.  These commands are specific to each tables
    #  The specific actions are redefinded here.
    #
    #    def displayRec(self, *args):
    #        print("Displaying a Record: ", self.curRecNum)
    #
    #    def save(self):
    #        #
    #        #  Table specific.  Return a True if succesful saved.
    #        #
    #        print("Saving a record (re-defined)")
    #        return True
    def tableSpecificMenu(self):
        pass
        '''
        self.viewsmenu.add_command
        self.viewsmenu.add_command(label="Active Golfers", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                   command=lambda viewID='Active Golfers': self.basicViewsList(viewID))

        sortsmenu = Menu(self.menubar, foreground=AppDefaultForeground, font=AppDefaultMenuFont, tearoff=0)
        sortsmenu.add_command(label="Default", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Default': self.sortingList(sortID))
        sortsmenu.add_command(label="Last Name", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Last Name': self.sortingList(sortID))
        sortsmenu.add_command(label="First Name", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='First Name': self.sortingList(sortID))
        sortsmenu.add_command(label="Birthday", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Birthday': self.sortingList(sortID))
        #
        # Next Command actually puts the menu up
        #
        self.menubar.add_cascade(label="Sorts", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                 menu=sortsmenu)
        '''

    def sort(self):
        pass
        '''
        if self.curOp == 'default':
            currentID = self.recList[self.curRecNumV.get()][0]
            if self.sortID.get() == 'Default':
                self.recList = sorted(self.recList, key=operator.itemgetter(2,1))
            elif self.sortID.get() == 'Last Name':
                self.recList.sort(key=lambda row: row[2])
            elif self.sortID.get() == 'First Name':
                self.recList.sort(key=lambda row: row[1])
            elif self.sortID.get() == 'Birthday':
                self.recList.sort(key=lambda row: row[4])
            else:
                self.recList.sort(key=lambda row: row[0])
            newIdx = getIndex(currentID, self.recList)
            self.curRecNumV.set(newIdx)
            self.displayRec()
        else:
            aMsg = "ERROR: Invalid request. Must be executed in default state only."
            self.myMsgBar.newMessage('error', aMsg)
        '''

    def displayAbout(self):
        # AppDisplayAbout(self)
        print("Working on it.....")

    def _buildWindowsFields(self, aFrame):
        pass

class addEditExpensePopUp(popUpWindowClass):
    def __init__(self, aWindow, aCloseCommand, mode, expenseID, updateExpenseList, loginData):
        #
        #  loginInfo format: password, access_Level, loginID, updaterID
        #
        if mode == 'add':
            self.windowTitle = 'Add Trip Expenses'
        elif mode == 'edit':
            self.windowTitle = 'Edit A Trip Expenses'
        else:
            self.windowTitle = 'An Error Occur in the Call'
        popUpWindowClass.__init__(self, aWindow, aCloseCommand, self.windowTitle, loginData)
        self.tableName = 'PopupExpenses'
        self.aCloseCommand = aCloseCommand
        self.aWindow = aWindow
        self.loginData = loginData

        self.expenseID = expenseID
        self.updateExpenseList = updateExpenseList
        self.thisExpense = []

        self.configDataRead = updateMyAppConstant(AppConfigFile)
        self.golferComboBoxData = CRUDGolfers.getGolferCB()
        self.golferDictionary = {}
        self.reverseGolferDictionary = {}
        for i in range(len(self.golferComboBoxData)):
            golfer_full = '''{0} {1}'''.format(self.golferComboBoxData[i][1], self.golferComboBoxData[i][2])
            self.golferDictionary.update({golfer_full: self.golferComboBoxData[i][0]})
            self.reverseGolferDictionary.update({self.golferComboBoxData[i][0]: golfer_full})

        #
        #  loginInfo format: password, access_Level, loginID, updaterID
        #
        if loginData[2] == 1:
            self.updaterFullname = "Super User"
        else:
            self.updaterFullname = CRUDGolfers.getGolferFullname(loginData[3])
        self.updaterID = self.loginData[2]
        self.displayCurUser.setUserName(self.updaterFullname)

        #######################################################################################
        # Inherited all from tableCommanWindowClass
        #
        #  This class intents to change the mainArea only.  Everything else must remain common
        #
        self.mainAreaRowCount = 0
        self.mainAreaColumnCount = 0
        self._reDisplayRecordFrame()
        self.mainArea.rowconfigure(self.mainAreaRowCount, weight=1)
 #       self.windowViewDefault()

    ############################################################ Area to add window specific commands
    #        self.closeButton = ttk.Button(self.commandFrame, text='Test', style='CommandButton.TButton',
    #                                 command= lambda: self.sampleCommand())
    #        self.closeButton.grid(row=0, column=0, sticky=N+S)


    ################################################################# Redefined commands
    #
    #  invoke from the tableCommonWindows.  These commands are specific to each tables
    #  The specific actions are redefinded here.
    #
    #    def displayRec(self, *args):
    #        print("Displaying a Record: ", self.curRecNum)
    #
    #    def save(self):
    #        #
    #        #  Table specific.  Return a True if succesful saved.
    #        #
    #        print("Saving a record (re-defined)")
    #        return True
    def tableSpecificMenu(self):
        pass
        '''
        self.viewsmenu.add_command
        self.viewsmenu.add_command(label="Active Golfers", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                   command=lambda viewID='Active Golfers': self.basicViewsList(viewID))

        sortsmenu = Menu(self.menubar, foreground=AppDefaultForeground, font=AppDefaultMenuFont, tearoff=0)
        sortsmenu.add_command(label="Default", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Default': self.sortingList(sortID))
        sortsmenu.add_command(label="Last Name", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Last Name': self.sortingList(sortID))
        sortsmenu.add_command(label="First Name", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='First Name': self.sortingList(sortID))
        sortsmenu.add_command(label="Birthday", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Birthday': self.sortingList(sortID))
        #
        # Next Command actually puts the menu up
        #
        self.menubar.add_cascade(label="Sorts", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                 menu=sortsmenu)
        '''

    def sort(self):
        pass
        '''
        if self.curOp == 'default':
            currentID = self.recList[self.curRecNumV.get()][0]
            if self.sortID.get() == 'Default':
                self.recList = sorted(self.recList, key=operator.itemgetter(2,1))
            elif self.sortID.get() == 'Last Name':
                self.recList.sort(key=lambda row: row[2])
            elif self.sortID.get() == 'First Name':
                self.recList.sort(key=lambda row: row[1])
            elif self.sortID.get() == 'Birthday':
                self.recList.sort(key=lambda row: row[4])
            else:
                self.recList.sort(key=lambda row: row[0])
            newIdx = getIndex(currentID, self.recList)
            self.curRecNumV.set(newIdx)
            self.displayRec()
        else:
            aMsg = "ERROR: Invalid request. Must be executed in default state only."
            self.myMsgBar.newMessage('error', aMsg)
        '''

    def displayAbout(self):
        # AppDisplayAbout(self)
        print("Working on it.....")

    def _descriptionEnteredAction(self):
        pass

    def _buildWindowsFields(self, aFrame):
        #        headerList1 = ('BASIC','VALUE','VALUED','MULT','TOLE','SIZE','DESCR','NSN1','NSN2','NSN3','NSN4')
        colTotal = 2
        self.fieldsFrame = AppFieldsFrame(aFrame, colTotal)
        self.fieldsFrame.grid(row=0, column=0, sticky=N + S + E + W)
        self.fieldColList = ['Date', 'Buyer', 'Item Description', 'Amount', 'Currency', 'Rate', 'Adjusted Amount']
        self.fieldsFrame.stretchCurrentRow()

        ########################################################### Main Identification Section
        self.expenseFieldFrame = AppColumnAlignFieldFrame(self.fieldsFrame)
#        self.expenseFieldFrame.noBorder()
        self.expenseFieldFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column,
                                    columnspan=colTotal, sticky=E + W + N + S)
#        self.expenseFieldFrame.addTitle("Expense Data")

#        aFrame = AppBorderFrame(self.expenseFieldFrame, 1)
#        aFrame.noBorder()
#        self.selectedSUBSYS = AppFieldEntry(aFrame, 'Incorporated', 'H', itemsSUBSYSLenght)
#        self.selectedSUBSYS.grid(row=aFrame.row, column=aFrame.column, sticky=N)
#        self.column3Row1ForMIUandSCADetailsFrame.addFrame(aFrame)

        self.selectedExpenseDate = AppFieldEntryDate(self.expenseFieldFrame, None, None, appDateWidth,
                                                     self._dateEnteredAction, self.myMsgBar)
        self.expenseFieldFrame.addNewField('Expense Date', self.selectedExpenseDate)

        self.selectedExpenseDescription = AppFieldEntry(self.expenseFieldFrame, None, None,
                                                        appDescriptionWidth)
        self.expenseFieldFrame.addNewField('Item Description', self.selectedExpenseDescription)

        self.golferCBData = AppCBList(CRUDGolfers.getGolfersDictionaryInfo())
        self.selectedGolfer = AppSearchLB(self.expenseFieldFrame, self, None, None,
                                           self.golferCBData.getList(), AppFullNameWidth, None, self.myMsgBar)
        self.expenseFieldFrame.addNewField('Expense Paid By', self.selectedGolfer)

        self.selectedAmount = AppFieldEntryFloat(self.expenseFieldFrame, None, None, AppAmountWidth)
        self.selectedAmount.addValidationAmount(2,6, self.myMsgBar)
        self.expenseFieldFrame.addNewField('Expense Amount', self.selectedAmount)

        self.selectedCurrency = AppSearchLB(self.expenseFieldFrame, self, None, None, AppCurrencyList,
                                                   AppCurrencyWidth, None, self.myMsgBar)
        self.expenseFieldFrame.addNewField('Expense Currency', self.selectedCurrency)

        self.selectedRate = AppFieldEntryFloat(self.expenseFieldFrame, None, None, AppAmountWidth)
        self.selectedRate.addValidationAmount(5,7, self.myMsgBar)
        self.expenseFieldFrame.addNewField('Exchange Rate', self.selectedRate)

        self.selectedAdjustedAmnt = AppFieldEntryFloat(self.expenseFieldFrame, None, None, AppAmountWidth)
        self.selectedAdjustedAmnt.addValidationAmount(5,7, self.myMsgBar)
        self.expenseFieldFrame.addNewField('Expense Adjusted Amount', self.selectedAdjustedAmnt)

        ########################################################### Footer Section
        self.fieldsFrame.addRow()
        self.lastUpdate = AppLastUpdateFormat(self.fieldsFrame)
        self.lastUpdate.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, columnspan=colTotal,
                             sticky=E + N + S)

        self.setRequiredFields()
        self.fieldsDisable()

    def fieldsDisable(self):
        self.selectedExpenseDescription.disable()
        self.selectedGolfer.disable()
        self.selectedExpenseDate.disable()
        self.selectedAmount.disable()
        self.selectedCurrency.disable()
        self.selectedRate.disable()
        self.selectedAdjustedAmnt.disable()

    def fieldsEnable(self):
        self.selectedExpenseDescription.enable()
        self.selectedGolfer.enable()
        self.selectedExpenseDate.enable()
        self.selectedAmount.enable()
        self.selectedCurrency.enable()
        self.selectedRate.enable()
        self.selectedAdjustedAmnt.enable()

    def setRequiredFields(self):
        # Table specific.  Will be defined in calling class
        self.selectedExpenseDescription.setAsRequiredField()
        self.selectedGolfer.setAsRequiredField()
        self.selectedExpenseDate.setAsRequiredField()
        self.selectedAmount.setAsRequiredField()
        self.selectedCurrency.setAsRequiredField()
        self.selectedRate.setAsRequiredField()


    def resetRequiredFields(self):
        # Table specific.  Will be defined in calling class
        self.selectedExpenseDescription.resetAsRequiredField()
        self.selectedGolfer.resetAsRequiredField()
        self.selectedExpenseDate.resetAsRequiredField()
        self.selectedAmount.resetAsRequiredField()
        self.selectedCurrency.resetAsRequiredField()
        self.selectedRate.resetAsRequiredField()

    def validateRequiredFields(self):
        requiredFieldsEntered = True

        if len(self.selectedExpenseDescription.get()) == 0:
            requiredFieldsEntered = False
            self.self.selectedExpenseDescription.focus()

        if len(self.selectedGolfer.get()) == 0:
            requiredFieldsEntered = False
            self.selectedGolfer.focus()

        if len(self.selectedExpenseDate.get()) == 0:
            requiredFieldsEntered = False
            self.selectedExpenseDate.focus()

        if len(self.selectedAmount.get()) == 0:
            requiredFieldsEntered = False
            self.selectedAmount.focus()

        if len(self.selectedCurrency.get()) == 0:
            requiredFieldsEntered = False
            self.selectedCurrency.focus()

        if len(self.selectedRate.get()) == 0:
            requiredFieldsEntered = False
            self.selectedRate.focus()

        if requiredFieldsEntered == False:
            self.myMsgBar.requiredFieldMessage()
        return requiredFieldsEntered

    def _dateEnteredAction(self, *args):
        pass

    def _currencyEnteredAction(self, *args):
        self._updateRevisedAmount()


class addEditExpenseV2(Tix.Frame):
    def __init__(self, parent, aCloseCommand, mode, expenseID, updateExpenseList):
        Tix.Frame.__init__(self, parent)
        self.CloseMe = aCloseCommand
        self.mode = mode
        self.expenseID = expenseID
        self.updateExpenseList = updateExpenseList
        self.thisExpense = []

        self.configDataRead = updateMyAppConstant(AppConfigFile)
        self.golferComboBoxData = CRUDGolfers.getGolferCB()
        self.golferDictionary = {}
        self.reverseGolferDictionary = {}
        for i in range(len(self.golferComboBoxData)):
            golfer_full = '''{0} {1}'''.format(self.golferComboBoxData[i][1], self.golferComboBoxData[i][2])
            self.golferDictionary.update({golfer_full: self.golferComboBoxData[i][0]})
            self.reverseGolferDictionary.update({self.golferComboBoxData[i][0]: golfer_full})

        ################################################### Frame general Configuration
        self.columnCount = 6
        for i in range(self.columnCount):
            self.columnconfigure(i, weight=1)  # Allow all column to strech

        ################################################### Page Title area
        self.row = 0
        self.rowconfigure(self.row, weight=0)
        if self.mode == 'add':
            label = Tix.Label(self, text="Adding Expenses", font=fontMonstrousB, fg=AppDefaultForeground)
        else:
            label = Tix.Label(self, text="Editing Expenses", font=fontMonstrousB, fg=AppDefaultForeground)
            self.thisExpense = CRUDExpenses.get_MBExpensesForExpenseID(expenseID)
        label.grid(column=0, row=self.row, columnspan=self.columnCount, sticky=W + N + E)
        self.rowconfigure(self.row, weight=0)  # no streching

        ################################################### Fields Frames

        self.row = self.row + 1
        self._displayFields()

        ################################################### Command Frame (action buttons)
        self.row = self.row + 1
        self.commandLine = Tix.Frame(self, border=3, relief='flat')
        self.commandLine.grid(column=0, row=self.row, columnspan=self.columnCount, sticky=N + S)
        self.rowconfigure(self.row, weight=0)  # no streching
        #
        self.commandLineButtonWidth = 8
        self.commandLineRowCount = 0
        self.commandLineaColumnCount = 0
        self.aSubmitBtn = Tix.Button(self.commandLine, text="Submit", width=self.commandLineButtonWidth,
                                     font=fontAverage,
                                     fg=AppDefaultForeground, command=lambda: self._submitButtonPress())
        self.aSubmitBtn.grid(column=self.commandLineaColumnCount, row=self.commandLineRowCount, sticky=N + S + W)
        self.aSubmitBtn.config(state='normal')

        self.commandLineaColumnCount = self.commandLineaColumnCount + 1
        self.aCloseBtn = Tix.Button(self.commandLine, text="Close", width=self.commandLineButtonWidth, font=fontAverage,
                                    fg=AppDefaultForeground, command=lambda: self._closeButtonPress())
        self.aCloseBtn.grid(column=self.commandLineaColumnCount, row=self.commandLineRowCount, sticky=N + S + W)
        self.aCloseBtn.config(state='normal')

        ################################################### Message Frame (row to display informational messages to user)

        self.row = self.row + 1
        self.myMsgBar = messageBarScore(self)
        self.myMsgBar.grid(column=0, row=self.row, columnspan=self.columnCount, sticky=W + S, pady=2)
        self.rowconfigure(self.row, weight=0)  # no streching


    def _validateExpenseForm(self):
        for i in range(len(self.dataEntry)):
            if len(self.dataEntry[i].get()) == 0:
                aMsg = '''ERROR: Required field, please enter the required information.'''
                self.myMsgBar.newMessage('error', aMsg)
                self.dataEntry[i].focus()
                return False

        if self._validateAmount() == False:
            self.dataEntry[3].focus()
            return False

        if self._validateRate() == False:
            self.dataEntry[5].focus()
            return False

        if self._validateTotal() == False:
            self.dataEntry[3].focus()
            return False

        return True

    def _validateTotal(self):
        if len(stripChar((' ', '$'), self.dataEntry[6].get())) > 0:
            amount = stripChar((' ', '$'), self.dataEntry[6].get())
            try:
                float(amount)
                self.dataEntry[6].delete(0, END)
                self.dataEntry[6].insert(0, "$ {0}".format("%.2f" % float(amount)))
            except:
                aMsg = '''ERROR: Invalid total amount, please verify expense and rate amounts.'''
                self.myMsgBar.newMessage('error', aMsg)
                return False
        else:
            return False

    def _validateRate(self):
        if len(stripChar((' ', '$'), self.dataEntry[5].get())) > 0:
            amount = stripChar((' ', '$'), self.dataEntry[5].get())
            try:
                float(amount)
                self.dataEntry[5].delete(0, END)
                self.dataEntry[5].insert(0, "$ {0}".format("%.5f" % float(amount)))
            except:
                aMsg = '''ERROR: Invalid amount entered'''
                self.myMsgBar.newMessage('error', aMsg)
                return False
        else:
            return False

    def _validateAmount(self):
        if len(stripChar((' ', '$'), self.dataEntry[3].get())) > 0:
            amount = stripChar((' ', '$'), self.dataEntry[3].get())
            try:
                float(amount)
                self.dataEntry[3].delete(0, END)
                self.dataEntry[3].insert(0, "$ {0}".format("%.2f" % float(amount)))
            except:
                aMsg = '''ERROR: Invalid amount entered'''
                self.myMsgBar.newMessage('error', aMsg)
                return False
        else:
            return False

    def _clearExpenseForm(self):

        self.dataEntry[3].delete(0, END)
        self.dataEntry[3].insert(0, '$ ')

        self.dataEntry[6].config(state='normal')
        self.dataEntry[6].delete(0, END)
        self.dataEntry[6].config(state=DISABLED)

        self.dataEntry[0].focus()

    def _submitButtonPress(self):
        self._updateRevisedAmount()
        if self._validateExpenseForm() == True:
            if self.mode == 'add':
                anOrdinalDate = convertDateStringToOrdinal(self.dataEntry[0].get())
                anAmount = float(stripChar((' ', '$'), self.dataEntry[3].get()))
                aRate = float(stripChar((' ', '$'), self.dataEntry[5].get()))
                adjustedAmount = float(stripChar((' ', '$'), self.dataEntry[6].get()))
                golferID = self.golferDictionary[self.dataEntry[1].get()]
                CRUDExpenses.insert_MBExpenses(anOrdinalDate, self.dataEntry[2].get(), anAmount, self.dataEntry[4].get(),
                                             aRate, adjustedAmount, golferID)
                self.updateExpenseList()
                aMsg = '''An expense of {0} by {1} has been added for your {2} MB trip.'''.format(
                    self.dataEntry[6].get(), self.dataEntry[1].get(), year)
                self.myMsgBar.newMessage('info', aMsg)
                self._clearExpenseForm()
            elif self.mode == 'edit':
                anOrdinalDate = convertDateStringToOrdinal(self.dataEntry[0].get())
                anAmount = float(stripChar((' ', '$'), self.dataEntry[3].get()))
                aRate = float(stripChar((' ', '$'), self.dataEntry[5].get()))
                adjustedAmount = float(stripChar((' ', '$'), self.dataEntry[6].get()))
                CRUDExpenses.update_MBExpenses(self.thisExpense[0][0], anOrdinalDate, self.dataEntry[2].get(), anAmount,
                                             self.dataEntry[4].get(), aRate, adjustedAmount, self.thisExpense[0][7])
                self.updateExpenseList()
                self.CloseMe()

    def _closeButtonPress(self):
        self.CloseMe()

    def _updateGolferList(self):
        golfersList = []
        for i in range(len(self.golferComboBoxData)):
            golfer_full = '''{0} {1}'''.format(self.golferComboBoxData[i][1], self.golferComboBoxData[i][2])
            golfersList.append(golfer_full)
        return golfersList

    def _amount_check(self, event):
        self.myMsgBar.clearMessage()
        aMsg = '''ERROR: Invalid amount entered'''
        currentContent = event.widget.get()
        futureContent = currentContent + event.char
        if event.char:
            try:
                if event.char != "\b":
                    float(futureContent)
            except:
                self.myMsgBar.newMessage('error', aMsg)
                event.widget.focus()
                return "break"

    def _updateRevisedAmount(self):
        # Clear any amount in the total area
        self.dataEntry[6].config(state='normal')
        if len(self.dataEntry[6].get()) > 0:
            self.dataEntry[6].delete(0, END)
        # Get the amount entered
        if len(stripChar((' ', '$'), self.dataEntry[3].get())):
            amount = stripChar((' ', '$'), self.dataEntry[3].get())
        else:
            amount = 0.00
        # Verify currency
        if self.dataEntry[4].get() == 'US':
            rate = stripChar((' ', '$'), self.dataEntry[5].get())
            amount = float(amount) * float(rate)
        else:
            amount = float(amount)

        self.dataEntry[6].insert(0, "$ {0}".format("%.2f" % amount))
        self.dataEntry[6].config(state=DISABLED)

    def _amount_finalize(self, event, decimalAmount):
        self.myMsgBar.clearMessage()
        aMsg = '''ERROR: Invalid amount entered YYY'''
        currentContent = stripChar((' ', '$'), event.widget.get())
        try:
            if len(currentContent) > 0:
                amount = float(currentContent)
                event.widget.delete(0, END)
                if decimalAmount == 6:
                    event.widget.insert(0, "$ {0}".format("%.6f" % amount))
                    self._updateRevisedAmount()
                elif decimalAmount == 5:
                    event.widget.insert(0, "$ {0}".format("%.5f" % amount))
                    self._updateRevisedAmount()
                elif decimalAmount == 4:
                    event.widget.insert(0, "$ {0}".format("%.4f" % amount))
                    self._updateRevisedAmount()
                elif decimalAmount == 3:
                    event.widget.insert(0, "$ {0}".format("%.3f" % amount))
                    self._updateRevisedAmount()
                else:
                    # Default is two
                    event.widget.insert(0, "$ {0}".format("%.2f" % amount))
                    self._updateRevisedAmount()

        except:
            self.myMsgBar.newMessage('error', aMsg)
            event.widget.focus()
            return "break"

    def _amount_prepare(self, event):
        # Remove all spaces and dollar signs
        # Ready for user to enter the amount
        currentWidget = event.widget
        currentContent = currentWidget.get()
        stripped = stripChar((' ', '$'), currentContent)
        if len(currentContent) > 0:
            currentWidget.delete(0, END)
            currentWidget.insert(0, stripped)

    def _golferEnteredAction(self, *args):
        pass

    def _dateEnteredAction(self, *args):
        pass

    def _currencyEnteredAction(self, *args):
        self._updateRevisedAmount()

    def _displayFields(self):
        self.fieldFrame = Tix.Frame(self, border=3, relief='groove')
        self.fieldFrame.grid(column=0, row=self.row, columnspan=self.columnCount, pady=3, sticky=N + S + E + W)
        self.rowconfigure(self.row, weight=1)

        self.fieldColList = ['Date', 'Buyer', 'Item Description', 'Amount', 'Currency', 'Rate', 'Adjusted Amount']

        # Field entry widget
        self.fieldFrameRowCount = 0
        self.fieldFrame.rowconfigure(self.fieldFrameRowCount, weight=1)
        self.dataEntry = []
        dateWidth = 15
        buyerWidth = 30
        itemWidth = 60
        amountWidth = 10
        currencyWidth = 6
        rateWidth = 12
        adjustedWidth = 10

        for items in self.fieldColList:
            colCount = 0
            num = len(self.dataEntry)
            if items == 'Date':
                '''
                label = Tix.Label(self.fieldFrame, text="{0}: ".format(items), font=fontBigI, fg=AppDefaultForeground)
                label.grid(column=colCount, row=self.fieldFrameRowCount, padx=2, sticky=W + N)

                colCount = colCount + 1
                onlyHereFrame = Tix.Frame(self.fieldFrame, relief='flat')
                onlyHereFrame.grid(column=colCount, row=self.fieldFrameRowCount, sticky=N + W)

                self.date_var1 = Tix.StringVar()
                if self.mode == 'edit':
                    self.date_var1.set(convertOrdinaltoString(self.thisExpense[0][1]))
                self.date_var1.trace('w', self._dateEnteredAction)

                self.dataEntry.append(
                    Tix.Entry(onlyHereFrame, justify='left', width=dateWidth, textvariable=self.date_var1,
                              highlightcolor=AppDefaultForeground, highlightthickness=2,
                              font=fontBig, fg=AppDefaultForeground))
                self.dataEntry[num].config(state="readonly")
                self.dataEntry[num].grid(row=0, column=0, sticky=N + W)

                self.datePicker = Tix.Button(onlyHereFrame, relief='flat', text='...',
                                             command=lambda: fnCalendar(self.date_var1))
                self.datePicker.grid(row=0, column=1, sticky=N + W)
                '''
                #parent, aLabel, orientation, aWidth
                self.selectedExpenseDate = AppFieldEntryDate(self.fieldFrame, 'Start Date', 'H', appDateWidth)
#                                                            self._dateEnteredAction, self.myMsgBar)
                self.selectedExpenseDate.grid(column=colCount, row=self.fieldFrameRowCount,
                                            sticky=N + S + E + W)

            if items == 'Buyer':
                label = Tix.Label(self.fieldFrame, text="{0}: ".format(items), font=fontBigI, fg=AppDefaultForeground)
                label.grid(column=colCount, row=self.fieldFrameRowCount, padx=2, sticky=W + N)

                colCount = colCount + 1
                vGolfer = Tix.StringVar()
                if self.mode == 'edit':
                    vGolfer.set(self.reverseGolferDictionary[self.thisExpense[0][7]])
                vGolfer.trace('w', self._golferEnteredAction)
                aList = self._updateGolferList()
                self.dataEntry.append(ttk.Combobox(self.fieldFrame, text=vGolfer, values=aList, font=fontBig,
                                                   foreground=AppDefaultForeground,
                                                   justify=LEFT))
                self.dataEntry[num].bind("<ComboboxSelected>")
                self.dataEntry[num].grid(row=self.fieldFrameRowCount, column=colCount, sticky=N + W)
                self.dataEntry[num].config(state='readonly')

            if items == 'Item Description':
                label = Tix.Label(self.fieldFrame, text="{0}: ".format(items), font=fontBigI, fg=AppDefaultForeground)
                label.grid(column=colCount, row=self.fieldFrameRowCount, padx=2, sticky=W + N)

                colCount = colCount + 1
                self.dataEntry.append(
                    Tix.Entry(self.fieldFrame, justify='left', width=itemWidth, highlightcolor=AppDefaultForeground,
                              highlightthickness=2,
                              font=fontBig, fg=AppDefaultForeground))
                self.dataEntry[num].config(state='normal')
                if self.mode == 'edit':
                    self.dataEntry[num].insert(0, self.thisExpense[0][2])
                self.dataEntry[num].grid(row=self.fieldFrameRowCount, column=colCount, sticky=N + W)

            if items == 'Amount':
                label = Tix.Label(self.fieldFrame, text="{0}: ".format(items), font=fontBigI, fg=AppDefaultForeground)
                label.grid(column=colCount, row=self.fieldFrameRowCount, padx=2, sticky=W + N)

                colCount = colCount + 1
                self.dataEntry.append(
                    Tix.Entry(self.fieldFrame, justify='left', width=amountWidth, highlightcolor=AppDefaultForeground,
                              highlightthickness=2,
                              font=fontBig, fg=AppDefaultForeground))
                self.dataEntry[num].config(state='normal')
                self.dataEntry[num].config(state='normal')
                if self.mode == 'edit':
                    self.dataEntry[num].insert(0, "$ {0}".format('%.2f' % self.thisExpense[0][3]))
                else:
                    self.dataEntry[num].insert(0, '$')
                self.dataEntry[num].bind('<Key>', lambda event: self._amount_check(event))
                self.dataEntry[num].bind('<FocusOut>',
                                         lambda event, decimalAmount=2: self._amount_finalize(event, decimalAmount))
                self.dataEntry[num].bind('<FocusIn>', lambda event: self._amount_prepare(event))
                self.dataEntry[num].grid(row=self.fieldFrameRowCount, column=colCount, sticky=N + W)

            if items == 'Currency':
                label = Tix.Label(self.fieldFrame, text="{0}: ".format(items), font=fontBigI, fg=AppDefaultForeground)
                label.grid(column=colCount, row=self.fieldFrameRowCount, padx=2, sticky=W + N)

                colCount = colCount + 1
                self.dataEntry.append(
                    Tix.Spinbox(self.fieldFrame, justify='left', width=currencyWidth, values=('CDN', 'US'),
                                highlightcolor=AppDefaultForeground, highlightthickness=2,
                                font=fontBig, fg=AppDefaultForeground, command=self._currencyEnteredAction))
                self.dataEntry[num].grid(row=self.fieldFrameRowCount, column=colCount, sticky=N + W)
                if self.mode == 'edit':
                    self.dataEntry[num].delete(0, END)
                    self.dataEntry[num].insert(0, self.thisExpense[0][4])
                self.dataEntry[num].config(state='readonly')

            if items == 'Rate':
                label = Tix.Label(self.fieldFrame, text="{0}: ".format(items), font=fontBigI, fg=AppDefaultForeground)
                label.grid(column=colCount, row=self.fieldFrameRowCount, padx=2, sticky=W + N)

                colCount = colCount + 1
                self.dataEntry.append(
                    Tix.Entry(self.fieldFrame, justify='left', width=rateWidth, highlightcolor=AppDefaultForeground,
                              highlightthickness=2,
                              font=fontBig, fg=AppDefaultForeground))
                self.dataEntry[num].config(state='normal')
                if self.mode == 'edit':
                    self.dataEntry[num].delete(0, END)
                    self.dataEntry[num].insert(0, "$ {0}".format('%.6f' % self.thisExpense[0][5]))
                else:
                    self.dataEntry[num].insert(0, "$ {0}".format("%.6f" % MyGolfCurrentUSRate))
                self.dataEntry[num].bind('<Key>', lambda event: self._amount_check(event))
                self.dataEntry[num].bind('<FocusOut>',
                                         lambda event, decimalAmount=6: self._amount_finalize(event, decimalAmount))
                self.dataEntry[num].bind('<FocusIn>', lambda event: self._amount_prepare(event))
                self.dataEntry[num].grid(row=self.fieldFrameRowCount, column=colCount, sticky=N + W)

            if items == 'Adjusted Amount':
                label = Tix.Label(self.fieldFrame, text="{0}: ".format(items), font=fontBigI, fg=AppDefaultForeground)
                label.grid(column=colCount, row=self.fieldFrameRowCount, padx=2, sticky=W + N)

                colCount = colCount + 1
                self.dataEntry.append(Tix.Entry(self.fieldFrame, justify='left', width=adjustedWidth, relief='flat',
                                                disabledforeground=AppDefaultForeground,
                                                font=fontBig, fg=AppDefaultForeground))
                if self.mode == 'edit':
                    self.dataEntry[num].insert(0, "$ {0}".format('%.2f' % self.thisExpense[0][6]))
                self.dataEntry[num].config(state=DISABLED)
                self.dataEntry[num].grid(row=self.fieldFrameRowCount, column=colCount, sticky=N + W)

            self.fieldFrameRowCount = self.fieldFrameRowCount + 1
            self.fieldFrame.rowconfigure(self.fieldFrameRowCount, weight=1)

class gameCellColumnDataFrame(Frame):
    def __init__(self, root, aLabel, aWidth, is9Hole, aMethod, aList, aRow, anIndice):
        Frame.__init__(self, root)
        self.aRow = aRow
        self.configure(highlightcolor = AppDefaultForeground, relief='groove', border = 2,
                       highlightthickness = AppHighlightBorderWidth, takefocus = 0,
                       height =1 )
        if is9Hole == "All":
            Label(self, text=aLabel, width = aWidth, relief='flat', justify=CENTER, takefocus = 0,
                        font=AppDefaultFontAvg, disabledforeground=AppStdForeground, bd=0,
                        fg=AppDefaultForeground).grid(row=0, column=0, sticky="wnes")
        elif is9Hole == "Back" or is9Hole == "Front":
            Label(self, text=aLabel, width = aWidth, relief='flat', justify=CENTER, bd=0,
                        font=AppDefaultFontAvg, disabledforeground=AppHighlightBackground,  takefocus = 0,
                        bg=AppHighlightBackground, fg=AppDefaultForeground).grid(row=0, column=0, sticky="wnes")
        else:
            imageResized = resizeImage(Image.open(deleteCommandRecImage), buttonImageSizeHeight, buttonImageSizeWidth)
#            self.aButton = Button(self, image=PhotoImage(deleteCommandRecImage), font=AppDefaultFontAvg,relief='flat', border=0, width = aWidth,
#                                      command=lambda ID=aList[aRow][anIndice]: aMethod(ID), takefocus=1).grid(row=0, column=0, sticky="wnes")
            self.bind('<Enter>', self.aFocus)
            self.bind('<Leave>', self.leaving)
            self.bind('<F1>', self.displayToolTip)
            self.bind('<Escape>', self.removeToolTip)
            if aLabel == 'Edt':
                imageResized = resizeImage(Image.open(editRecImage), buttonImageSizeHeight,
                                           buttonImageSizeWidth)
                self.aButton_ttp = CreateToolTipV2(self, "Edit Game {0}".format(int(self.aRow)+1))
            elif aLabel == 'Del':
                imageResized = resizeImage(Image.open(deleteCommandRecImage), buttonImageSizeHeight,
                                           buttonImageSizeWidth)
                self.aButton_ttp = CreateToolTipV2(self, "Delete Game {0}".format(int(self.aRow)+1))

            self.aButton = ttk.Button(self, width=aWidth, image=imageResized,
                                      command=lambda ID=aList[aRow][anIndice]: aMethod(ID),
                                      style='GameListingButton.TButton', takefocus=1)
            self.aButton.img = imageResized
            self.aButton.grid(row=0, column=0, sticky="wnes")

        self.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=1)

    def aFocus(self,event):
        self.configure(highlightcolor=AppDefaultForeground,
                       highlightthickness=AppHighlightBorderWidth, takefocus=1)
        widgetEnter(event)

    def leaving(self, event):
        self.configure(takefocus=0)
        self.configure(highlightcolor=AppDefaultBackground,
                       highlightthickness=AppHighlightBorderWidth, takefocus=0)
        if self.aButton_ttp.tw:
            self.aButton_ttp.tw.destroy()

    def displayToolTip(self, event):
        self.aButton_ttp.enter()

    def removeToolTip(self, event):
        self.aButton_ttp.close()

class MyScrollFrameVertical(Frame):
    def __init__(self, root, rootColumnSpan, columnTotal, rootRow):
        Frame.__init__(self, root)
        self.lastWidth = 0
        self.frameRow = rootRow
        self.Column = 0
        self.currentRow = 0
        self.currentColumn = 0
        self.noItems = TRUE # no rows added yet (no cell)
        self.canvas = Canvas(root)
        self.vsb = Scrollbar(root, orient="vertical", command=self.canvas.yview)
        self.canvas.configure(yscrollcommand=self.vsb.set, takefocus=0)
        self.canvas.bind("<Configure>", self.OnCanvasConfigure)
        self.vsb.grid(row=self.frameRow, column=rootColumnSpan, sticky="wnes")
        self.canvas.grid(row=self.frameRow, column=0, columnspan=rootColumnSpan, sticky="wnes")

        self.frame = Frame(self.canvas)
        self.window = self.canvas.create_window((0,0), window=self.frame, anchor="nw",
                          tags="self.frame")
        self.frame.rowconfigure(0, weight=1)  # first row configure

        for i in range(columnTotal-2):
            self.columnconfigure(i, weight=1)
            self.frame.columnconfigure(i, weight=1)
        self.rowconfigure(0, weight=1)


    def addAGameCellFrame(self, aFrame, aText, aWidth, is9Hole, aMethod, aList, aRow, anIndice):
        aFrame(self.frame, aText, aWidth, is9Hole, aMethod, aList, aRow, anIndice).grid(row=self.currentRow, column=self.currentColumn, sticky="wens")
        self.currentColumn = self.currentColumn + 1

#    def addAGameActionButton(self, aFrame, aText, anImage, gameIDList, indices, aMethod):
#        aFrame(self.frame, aText, anImage, aMethod, gameIDList, indices).grid(row=self.currentRow, column=self.currentColumn, sticky="wens")
#        self.currentColumn = self.currentColumn + 1

    def addRow(self):
        self.currentRow = self.currentRow + 1
        self.frame.rowconfigure(0, weight=1)
        self.resetColumn()

    def resetColumn(self):
        self.currentColumn = 0

    def OnCanvasConfigure(self, event):
        self.lastWidth = event.width
        self.canvas.itemconfigure(self.window, width=event.width)
        self.canvas.configure(scrollregion=self.canvas.bbox("all"))

    def ResizeScrollBar(self):
        self.vsb.update()
        self.canvas.configure(scrollregion=self.canvas.bbox("all"))

def fnCalendarSet(date_var, ayear, amonth, aday):
    parent = ()
    tkCalendar(parent, ayear, amonth, aday, date_var )

def fnCalendar(date_var):
    parent = ()
    tkCalendar(parent, year, month, day, date_var )

class tkCalendar:
    def __init__(self, master, arg_year, arg_month, arg_day, arg_parent_updatable_var):
        self.update_var = arg_parent_updatable_var
        top = self.top = Toplevel(master)
        x, y = top.winfo_pointerxy()
        w = 200  # based on canvas size
        h = 220
        # This technique starts window just below the cursor.
        self.top.geometry('%dx%d+%d+%d' % (w, h, x, y))
        try:
            self.intmonth = int(arg_month)
        except:
            self.intmonth = int(1)
        self.canvas = Canvas(top, width=200, height=220, relief=RIDGE, background="white", borderwidth=1)
        self.canvas.create_rectangle(0, 0, 303, 30, fill="#a4cae8", width=0)
        self.canvas.create_text(100, 17, text=strtitle, font=fntc, fill="#2024d6")
        stryear = str(arg_year)

        self.year_var = StringVar()
        self.year_var.set(stryear)
        self.lblYear = Label(top, textvariable=self.year_var, font=fnta, background="white")
        self.lblYear.place(x=85, y=30)

        self.month_var = StringVar()
        strnummonth = str(self.intmonth)
        strmonth = dictmonths[strnummonth]
        self.month_var.set(strmonth)

        self.lblYear = Label(top, textvariable=self.month_var, font=fnta, background="white")
        self.lblYear.place(x=85, y=50)
        # Variable muy usada
        tagBaseButton = "Arrow"
        self.tagBaseNumber = "DayButton"
        # draw year arrows
        x, y = 40, 43
        tagThisButton = "leftyear"
        tagFinalThisButton = tuple((tagBaseButton, tagThisButton))
        self.fnCreateLeftArrow(self.canvas, x, y, tagFinalThisButton)
        x, y = 150, 43
        tagThisButton = "rightyear"
        tagFinalThisButton = tuple((tagBaseButton, tagThisButton))
        self.fnCreateRightArrow(self.canvas, x, y, tagFinalThisButton)
        # draw month arrows
        x, y = 40, 63
        tagThisButton = "leftmonth"
        tagFinalThisButton = tuple((tagBaseButton, tagThisButton))
        self.fnCreateLeftArrow(self.canvas, x, y, tagFinalThisButton)
        x, y = 150, 63
        tagThisButton = "rightmonth"
        tagFinalThisButton = tuple((tagBaseButton, tagThisButton))
        self.fnCreateRightArrow(self.canvas, x, y, tagFinalThisButton)
        # Print days
        self.canvas.create_text(100, 90, text=strdays, font=fnta)
        self.canvas.pack(expand=1, fill=BOTH)
        self.canvas.tag_bind("Arrow", "<ButtonRelease-1>", self.fnClick)
        self.canvas.tag_bind("Arrow", "<Enter>", self.fnOnMouseOver)
        self.canvas.tag_bind("Arrow", "<Leave>", self.fnOnMouseOut)
        self.fnFillCalendar()

    def fnCreateRightArrow(self, canv, x, y, strtagname):
        canv.create_polygon(x, y, [[x + 0, y - 5], [x + 10, y - 5], [x + 10, y - 10], [x + 20, y + 0], [x + 10, y + 10],
                                   [x + 10, y + 5], [x + 0, y + 5]], tags=strtagname, fill="blue", width=0)

    def fnCreateLeftArrow(self, canv, x, y, strtagname):
        canv.create_polygon(x, y, [[x + 10, y - 10], [x + 10, y - 5], [x + 20, y - 5], [x + 20, y + 5], [x + 10, y + 5],
                                   [x + 10, y + 10]], tags=strtagname, fill="blue", width=0)

    def fnClick(self, event):
        owntags = self.canvas.gettags(CURRENT)
        if "rightyear" in owntags:
            intyear = int(self.year_var.get())
            intyear += 1
            stryear = str(intyear)
            self.year_var.set(stryear)
        if "leftyear" in owntags:
            intyear = int(self.year_var.get())
            intyear -= 1
            stryear = str(intyear)
            self.year_var.set(stryear)
        if "rightmonth" in owntags:
            if self.intmonth < 12:
                self.intmonth += 1
                strnummonth = str(self.intmonth)
                strmonth = dictmonths[strnummonth]
                self.month_var.set(strmonth)
            else:
                self.intmonth = 1
                strnummonth = str(self.intmonth)
                strmonth = dictmonths[strnummonth]
                self.month_var.set(strmonth)
                intyear = int(self.year_var.get())
                intyear += 1
                stryear = str(intyear)
                self.year_var.set(stryear)
        if "leftmonth" in owntags:
            if self.intmonth > 1:
                self.intmonth -= 1
                strnummonth = str(self.intmonth)
                strmonth = dictmonths[strnummonth]
                self.month_var.set(strmonth)
            else:
                self.intmonth = 12
                strnummonth = str(self.intmonth)
                strmonth = dictmonths[strnummonth]
                self.month_var.set(strmonth)
                intyear = int(self.year_var.get())
                intyear -= 1
                stryear = str(intyear)
                self.year_var.set(stryear)
        self.fnFillCalendar()

    def fnFillCalendar(self):
        init_x_pos = 20
        arr_y_pos = [110, 130, 150, 170, 190, 210]
        intposarr = 0
        self.canvas.delete("DayButton")
        self.canvas.update()
        intyear = int(self.year_var.get())
        monthcal = calendar.monthcalendar(intyear, self.intmonth)
        for row in monthcal:
            xpos = init_x_pos
            ypos = arr_y_pos[intposarr]
            for item in row:
                stritem = str(item)
                if stritem == "0":
                    xpos += 27
                else:
                    tagNumber = tuple((self.tagBaseNumber, stritem))
                    self.canvas.create_text(xpos, ypos, text=stritem, font=fnta, tags=tagNumber)
                    xpos += 27
            intposarr += 1
        self.canvas.tag_bind("DayButton", "<ButtonRelease-1>", self.fnClickNumber)
        self.canvas.tag_bind("DayButton", "<Enter>", self.fnOnMouseOver)
        self.canvas.tag_bind("DayButton", "<Leave>", self.fnOnMouseOut)

    def fnClickNumber(self, event):
        owntags = self.canvas.gettags(CURRENT)
        for x in owntags:
            if x not in ("current", "DayButton"):
                strdate = (str(self.year_var.get()) + "-" + str(self.month_var.get()) + "-" + str(x))
                self.update_var.set(strdate)
                self.top.withdraw()
                # event.widget.update_idletasks()

    def fnOnMouseOver(self, event):
        self.canvas.move(CURRENT, 1, 1)
        self.canvas.update()

    def fnOnMouseOut(self, event):
        self.canvas.move(CURRENT, -1, -1)
        self.canvas.update()

class messageBarScore(Frame):
    def __init__(self, parent):
        Frame.__init__(self, parent)
        messageFrame = Frame(self)
        self.myMessage = '''   '''
        self.messageDisplay = Label(messageFrame, text=self.myMessage, relief='flat', font=AppDefaultFontAvg)
        self.messageDisplay.pack(side=LEFT, fill=X)
        messageFrame.pack()

    def clearMessage(self):
        self.myMessage = '''   '''
        self.messageDisplay.config(text=self.myMessage)

    def newMessage(self, msgType, msg):
        if msgType == 'info':
            self.messageDisplay.config(fg=AppDefaultForeground)
        elif msgType == 'error':
            self.bell()
            self.messageDisplay.config(fg='red')
        elif msgType == 'warning':
            self.bell()
            self.messageDisplay.config(fg='green')
        self.messageDisplay.config(text=msg)

class messageBar(Frame):
    def __init__(self, parent):
        Frame.__init__(self, parent)
        messageFrame=Frame(self)
        self.myMessage = '''   '''
        self.messageDisplay = Label(messageFrame, text=self.myMessage, relief='flat')
        self.messageDisplay.pack(side=LEFT, fill=X)
        messageFrame.pack()
        
    def clearMessage(self):
        self.myMessage = '''   '''
        self.messageDisplay.config(text=self.myMessage)
            
    def newMessage(self, msgType, msg):
        if msgType == 'info':
            self.messageDisplay.config(fg=AppDefaultForeground)
        elif msgType == 'error':
            self.bell()
            self.messageDisplay.config(fg='red')
        elif msgType == 'warning':
            self.bell()
            self.messageDisplay.config(fg='green')
        self.messageDisplay.config(text=msg)


class AppLastUpdateFormat(Tix.Label):
    def __init__(self, parent):
        Tix.Label.__init__(self, parent)
        aText = " "
        self.config(text=aText, font=fontAverageI)
        self.config(background=AppAccentBackground)

    def load(self, aDate, updaterFullName):
        aText = "Last Updated: {0} ({1})".format(convertOrdinaltoString(aDate), updaterFullName)
        self.config(text=aText, font=fontAverageI)


class AppFrame(Frame):
    def __init__(self, parent, columnTotal):
        Frame.__init__(self, parent)
        self.config(border=AppDefaultBorderWidth, takefocus=0)
        self.row = 0
        self.column = 0
        self.columnTotal = columnTotal
        for i in range(self.columnTotal):
            self.columnconfigure(i, weight=1)
        self.rowconfigure(self.row, weight=0)

    def noBorder(self):
        self.config(border=0)

    def addTitle(self, aTitle):
        aLabel = AppLineSectionTitle(self, aTitle)
        aLabel.grid(row=self.row, column=self.column, columnspan=self.columnTotal, sticky=N)
        self.row = self.row + 1

    def defaultBorder(self):
        self.config(border=AppDefaultBorderWidth)

    def stretchCurrentRow(self):
        self.rowconfigure(self.row, weight=1)

    def addRow(self):
        self.row = self.row + 1
        self.rowconfigure(self.row, weight=0)

    def addColumn(self):
        self.column = self.column + 1

    def resetColumn(self):
        self.column = 0

    def addAccentBackground(self):
        self.config(background=AppAccentBackground)

    def addDeletedHighlight(self):
        self.config(highlightcolor=AppDefaultForeground, highlightthickness=AppDeletedHighlightBorderWidth, takefocus=1)


class AppFieldsFrame(Frame):
    def __init__(self, parent, columnTotal):
        Frame.__init__(self, parent)
        self.config(border=AppDefaultBorderWidth)
        self.row=0
        self.column=0
        self.columnTotal=columnTotal
        for i in range(self.columnTotal):
            self.columnconfigure(i, weight = 1)
            
        self.config(background=AppAccentBackground)
            
    def noBorder(self):
        self.config(border=0)
    
    def setBorder(self, aValue):
        self.config(border=aValue)

    def addRow(self):
        self.row = self.row + 1
        self.rowconfigure(self.row, weight=0)

    def stretchCurrentRow(self):
        self.rowconfigure(self.row, weight=1)

    def addColumn(self):
        self.column = self.column + 1
        
    def resetColumn(self):
        self.column = 0

class AppUserIDFrame(Frame):
    def __init__(self, parent, myMsgBar):
        Frame.__init__(self, parent)
        self.myMsgBar = myMsgBar
        self.config(background=AppDefaultBackground,highlightcolor=AppDefaultForeground,
                    highlightthickness = AppHighlightBorderWidth, takefocus=0)

    def load(self, UserData):
        refDate = convertDateStringToOrdinal(strdate)  # today in ordinal
        if UserData[4] != '' and UserData[4] != None:
            golferAge = getAge(refDate, int(UserData[4]))  # birthday stored in ordinal
            golferFull = "{0} {1} ({2})".format(UserData[1], UserData[2], golferAge)
        else:
            golferFull = "{0} {1}".format(UserData[1], UserData[2])
        label = Tix.Label(self, text=golferFull, font=fontHuge2I, fg=AppDefaultForeground, justify=LEFT)
        label.grid(column=0, row=0, sticky=N + W + E + S)
        golferAddr = CRUDAddresses.get_golferAddresses(UserData[0])
        if len(golferAddr) > 0:
            golferLocation = "{0}, {1}, {2}".format(golferAddr[0][2], golferAddr[0][3], golferAddr[0][4])
        else:
            golferLocation = ""
        label = Tix.Label(self, text=golferLocation, font=fontSmallB, fg=AppDefaultForeground, justify=CENTER)
        label.grid(column=0, row=1, sticky=N + W + E + S)
        currentIdx = calculateCurrentHdcp(UserData[0])
        label = Tix.Label(self, text=currentIdx, font=fontMonstrousB, fg=AppDefaultForeground, justify=CENTER)
        label.grid(column=0, row=2, sticky=N + W + E + S)

class AppPictureFrame(Frame):
    def __init__(self, parent, defaultIMG, picturePath, aWidth, aHeight, myMsgBar):
        Frame.__init__(self, parent)
        #
        #  A default width is assigned, it can be changed
        #
        self.myMsgBar = myMsgBar
        self.picWidth = aWidth   # Default
        self.picHeight = aHeight # Default
        self.config(width=self.picWidth, height=self.picHeight, background=AppDefaultBackground,
                    highlightcolor=AppDefaultForeground, highlightthickness = AppHighlightBorderWidth, takefocus=0)
#        self.bind('<Enter>', widgetEnter)
        self.pictureDir = "{0}/{1}".format(AppRootDir, picturePath)
        self.defaultIMG = defaultIMG
        self.picturePath = os.path.normpath(picturePath)
        self.loadedImage = None
        self.load("")

    def load(self, IMG): 
        try:
            im1=Image.open('{0}/{1}'.format(self.picturePath,IMG))
            PIC = '{0}/{1}'.format(self.picturePath, IMG)
            self.loadedImage = IMG
        except IOError:
            PIC = '{0}/{1}'.format(self.picturePath, self.defaultIMG)
            self.loadedImage = self.defaultIMG

        original = Image.open(PIC)
        image1 = resizeImage(original,self.picWidth,self.picHeight)                
        self.panel1 = Label(self, image=image1, border=0)
        self.panel1.grid(row = 0, column=0, sticky=N+W)
        self.display = image1
        
    def get(self):
        return self.loadedImage 
        
    def clear(self):
        self.panel1.destroy()
        self.load("")
       
    def disable(self):
        self.config(takefocus=0)
        self.unbind('<Enter>')
        self.panel1.unbind('<Button-1>')
        
    def enable(self):
        self.config(takefocus=1)
        self.bind('<Enter>', widgetEnter)
        self.panel1.bind('<Button-1>', self.getNewImage)
        
    def getNewImage(self, event):
        #
        #  Select existing image in the media image dir or a personal image.
        #  Will verify if image is good and will copy it to the media directory.
        #  If file exist, will change the name to avoid conflicts or losses.
        #
        self.myMsgBar.clearMessage()
        ftypes = myImageFileFormats
        dlg = Fd.Open(self, initialdir=self.picturePath, filetypes = ftypes)
        imageFileSelectedFullPath = dlg.show()
        imageFileSelectedFullPathNormalized = os.path.normpath(imageFileSelectedFullPath)
        try:
            if os.path.exists(imageFileSelectedFullPath):
                im1=Image.open(imageFileSelectedFullPath) # Verifies we have a good image
                pathToImageFile, imageFileSelected = os.path.split(imageFileSelectedFullPathNormalized)
                if pathToImageFile == self.picturePath:
                    #
                    #  Just use the new image and save the filename
                    #
                    self.loadedImage = imageFileSelected
                    self.load(imageFileSelected)
                else:
                    #
                    #  1. First verify that a file with the same name does not exist in the picture directory
                    #  2. If it does, generate a new filename with a random number
                    #  3. Once a unique filename is acheived, copy it in the picture dictionary
                    #  4. save and load the new picture/
                    #
                    newImageFilename = imageFileSelected
                    futureFullPathImage = "{0}/{1}".format(self.picturePath,imageFileSelected)
                    while (os.path.exists(futureFullPathImage)):
                        # generate a random number to modify file name
                        randomNum = int(random.random()*10000)
                        filename, file_extension = os.path.splitext(newImageFilename)
                        newImageFilename = "{0}{1}{2}".format(filename,str(randomNum),file_extension)
                        futureFullPathImage = "{0}\{1}".format(self.picturePath, newImageFilename)
                    shutil.copy2(imageFileSelectedFullPathNormalized, futureFullPathImage)   # copy the file in the media directory
                    #
                    #  Now, just use the new image and save the filename
                    #
                    self.loadedImage = newImageFilename
                    self.load(newImageFilename)
        except IOError:
            aMsg = 'ERROR: MyGolf Application is unable to display this image. Please select another image.'
            self.myMsgBar.newMessage('error', aMsg) 
 
    def setWidth(self, aWidth):
        self.config(width=aWidth)
        self.picWidth = aWidth
        
    def setHeight(self, aHeight):
        self.config(height=aHeight)
        self.picHeight = aHeight       
        
class AppLineSectionTitle(Label):
    def __init__(self, parent, aLabel):
        Label.__init__(self, parent)
        self.config(text=aLabel, font=fontHugeB)
        
    def justification(self, aValue):
        self.config(justify=aValue)
        
class AppBorderFrame(Frame):
    def __init__(self, parent, columnTotal):
        Frame.__init__(self, parent)
        self.config(border=AppDefaultBorderWidth, relief='ridge')
        self.row=0
        self.column=0
        self.columnTotal=columnTotal
        self.rowconfigure(self.row, weight=0)
        for i in range(self.columnTotal):
            self.columnconfigure(i, weight = 1)
 
    def addTitle(self, aTitle):
        aLabel =  AppLineSectionTitle(self, aTitle)
        aLabel.grid(row=self.row, column=self.column, columnspan=self.columnTotal, sticky=N)
        self.row = self.row + 1
               
    def addRow(self):
        self.row = self.row + 1
        self.rowconfigure(self.row, weight=0)
        
    def addColumn(self):
        self.column = self.column + 1

    def noStretchColumn(self, columnNumber):
        self.columnconfigure(columnNumber, weight=0)

    def stretchRow(self, rowNumber):
        self.rowconfigure(rowNumber, weight=10)

    def resetColumn(self):
        self.column = 0   
        
    def removeBorder(self):     
        self.config(border=0)

    def setBorderHighlight(self):
        self.config(relief = 'flat', border = 6, highlightthickness = 4,highlightbackground = AppDefaultForeground)

class AppMENUListFrame(Frame):
    def __init__(self, parent, columnTotal):
        Frame.__init__(self, parent)
        self.config(border=AppDefaultBorderWidth, relief='ridge', highlightbackground='red')
        self.row=0
        self.column=0
        self.columnTotal=columnTotal
        for i in range(self.columnTotal):
            self.columnconfigure(i, weight = 1)

    def addRow(self):
        self.row = self.row + 1
        self.rowconfigure(self.row,  weight = 0)

    def addColumn(self):
        self.column = self.column + 1

    def resetColumn(self):
        self.column = 0

    def addMenuItem(self, CommandText, selectedMethod):
        self.addRow()
        anItem = AppMENUFrame(self, CommandText, selectedMethod)
        anItem.grid(row=self.row, column=self.column, sticky=N+E+W)

class AppMENUFrame(Frame):
    def __init__(self, parent, CommandText, selectedMethod):
        Frame.__init__(self, parent)
        original = Image.open(MenuImg)
        image1 = resizeImage(original, 30, 30)

        self.config(border=3, relief='flat', width=golferPicWidth, height=golferPicHeight,
                    highlightcolor=AppDefaultForeground, highlightthickness=AppHighlightBorderWidth, takefocus=1)
        self.bind('<Enter>', widgetEnter)

        self.bind('<Button-1>', selectedMethod)
        self.bind('<Enter>', widgetEnter)
        self.bind('<Return>', selectedMethod)
        #
        panel1 = ttk.Label(self, image=image1)
        panel1.bind('<Button-1>', selectedMethod)
        panel1.bind('<Enter>', self.focus())
        self.display = image1
        panel1.grid(row=0, column=0)
        aMsg = ttk.Label(self, text=CommandText, style='MenuItems.TLabel')
        aMsg.grid(row=0, column=1, sticky=N + W)
        aMsg.bind('<Button-1>', selectedMethod)
        aMsg.bind('<Enter>', self.focus())


class AppStdButton(Frame):
    def __init__(self, parent, aText, anImage, aProc):
        Frame.__init__(self, parent)
        self.config(takefocus=0)
        self.aProc = aProc

        self.aFrame = Frame(self, highlightcolor=AppDefaultForeground,
                            highlightthickness=AppHighlightBorderWidth, takefocus=1)
        self.aFrame.grid(row=0, column=0, sticky=W + N)

        if aText != None:
            self.aButton = ttk.Button(self.aFrame, text=aText, style='CommandButton.TButton', command=self.aProc, takefocus=0)
            self.aButton.grid(row=0, column=0, sticky=N)
        elif anImage != None:
#            imageResized = resizeImage(Image.open(anImage), iconWidth, iconHeight)
            imageResized = resizeImage(Image.open(anImage), buttonImageSizeHeight, buttonImageSizeWidth)
            self.aButton = ttk.Button(self.aFrame, image=imageResized, command=self.aProc, style='CommandButton.TButton',takefocus=0)
            self.aButton.img = imageResized
            self.aButton.grid(row=0, column=0, sticky=N+S)
        self.enable()

    def enable(self):
        self.aButton.config(state='normal', takefocus=0)
        self.aFrame.config(takefocus=1)
        self.aFrame.bind('<Enter>', widgetEnter)
        self.aFrame.bind('<Return>', self.aProc)

    def disable(self):
        self.aButton.config(state=DISABLED, takefocus=0)
        self.aFrame.config(takefocus=0)
        self.aFrame.unbind('<Enter>')
        self.aFrame.unbind('<Return>')

    def addToolTip(self, aText):
        self.aButton_ttp = CreateToolTip(self.aButton, aText)

class GameEditButton(Frame):
    def __init__(self, parent, aText, anImage, aProc, aParameterList, anIndice):
        Frame.__init__(self, parent)
        self.config(takefocus=0)
        self.aProc = aProc

        if aText != None:
            # Button(self.frame, text="Show 9s", relief='ridge', justify=CENTER, bg=AppHighlightBackground,
            #                       fg=AppDefaultForeground, font=AppDefaultFontAvg,
            #                       command=self._display9HoleGames).grid(row=row + 2, column=12, columnspan=2, padx=4)

            self.aButton = Button(self, text=aText, justify=CENTER, font=AppDefaultFontAvg,relief='groove',
                                      command= lambda ID=aParameterList[anIndice]: self.aProc(ID), takefocus=0)
            self.aButton.grid(row=0, column=0, sticky=N+E+S+W)
        elif anImage != None:
            imageResized = resizeImage(Image.open(anImage), buttonImageSizeHeight, buttonImageSizeWidth)
            self.aButton = ttk.Button(self, image=imageResized,
                                      command= lambda ID=aParameterList[anIndice]: self.aProc(ID), takefocus=0)
            self.aButton.img = imageResized
            self.aButton.grid(row=0, column=0, sticky=N+S)
        self.enable()

    def enable(self):
        self.aButton.config(state='normal', takefocus=0)
        self.config(takefocus=1)
        self.bind('<Enter>', widgetEnter)
        self.bind('<Return>', self.aProc)

    def disable(self):
        self.aButton.config(state=DISABLED, takefocus=0)
        self.config(takefocus=0)
        self.unbind('<Enter>')
        self.unbind('<Return>')

    def addToolTip(self, aText):
        self.aButton_ttp = CreateToolTip(self.aButton, aText)


class AppFieldEntryFloat(Tix.Frame):
    def __init__(self, parent, aLabel, orientation, aWidth):
        self.myMsgBar = None
        self.aWidth = aWidth
        Tix.Frame.__init__(self, parent)
        self.parent = parent
        self.config(takefocus=0)
        myRow = 0
        myColumn = 0

        if aLabel != None:
            if orientation == 'H':
                aLabel = ttk.Label(self, text='{0}: '.format(aLabel), takefocus=0, style='RegularFieldTitle.TLabel')
                aLabel.grid(row=myRow, column=myColumn, sticky=E + N)
                myRow = 0
                myColumn = 1
            elif orientation == 'V':
                aLabel = ttk.Label(self, text='{0}'.format(aLabel), takefocus=0, style='RegularFieldTitle.TLabel')
                aLabel.grid(row=myRow, column=myColumn, sticky=N)
                myRow = 1
                myColumn = 0

        self.anEntry = Tix.Entry(self, highlightcolor=AppDefaultForeground, highlightthickness=AppHighlightBorderWidth,
                                 width=aWidth, font=fontBig,
                                 background=AppDefaultEditBackground, disabledforeground=AppDefaultForeground)
        self.anEntry.grid(row=myRow, column=myColumn, sticky=N + S)

    def setAsDeleted(self):
        self.anEntry.config(disabledforeground=AppDefaultDisabledForeground)

    def resetAsDeleted(self):
        self.anEntry.config(disabledforeground=AppDefaultForeground)

    def setAsRequiredField(self):
        self.anEntry.config(background=AppDefaultRequiredFieldBackground)

    def resetAsRequiredField(self):
        self.anEntry.config(background=AppDefaultEditBackground)

    def justification(self, aValue):
        self.anEntry.config(justify=aValue)

    def focus(self):
        self.anEntry.focus()

    def disable(self):
        self.anEntry.config(state=DISABLED, validate="none")
        self.anEntry.unbind('<Enter>')

    def clear(self):
        self.anEntry.config(state='normal')
        self.anEntry.delete(0, END)
        self.anEntry.config(state=DISABLED)

    def get(self):
        amnt = self.anEntry.get()
        try:
            amnt2 = amnt.strip('$ ')
            if len(amnt2) > 0:
                return float(amnt2)
            else:
                return float(0)
        except:
            return 'ERROR'

    def load(self, aValue):
        try:
            float(aValue)
            #            displayFormat = "{0}.{1}".format(totalDigits,decimal)
            #            amount2 ="$ {:{0}f}".format(displayFormat,aValue)
            amount = "$ {:7.2f}".format(aValue)
        except:
            amount = "$ 0.00"

        self.anEntry.config(state='normal')
        self.anEntry.delete(0, END)
        self.anEntry.insert(0, amount)
        self.anEntry.config(state=DISABLED)

    def enable(self):
        amnt = self.anEntry.get()
        amnt2 = amnt.strip('$ ')

        self.anEntry.config(state='normal')
        self.anEntry.delete(0, END)
        self.anEntry.insert(0, amnt2)
        self.anEntry.config(validate="all")
        self.anEntry.bind('<Enter>', widgetEnter)

    def addVariable(self, aVar):
        self.anEntry.config(textvariable=aVar)

    def _validateAmount(self, action, index, valueIfAllowed, valueBeforeEntry, textInvolved, validateOption,
                        callbackMethod, widgetName, decimal, totalDigits):
        #        self.myMsgBar.clearMessage()
        pattern = "{0}{1}{2}{3}{4}".format("^[\-]?[0-9]{,", str(int(totalDigits) - int(decimal)), "}\.?[0-9]{,",
                                           decimal, "}$")
        num_format = re.compile(pattern)
        isnumber = True
        if callbackMethod == 'focusout':
            if len(valueIfAllowed) > 0:
                isnumber = re.match(num_format, valueIfAllowed)
            else:
                return True
        elif callbackMethod == 'key' and action == '1':
            if textInvolved == '-':
                num_format = re.compile("^[\-]$")
                isnumber = re.match(num_format, valueIfAllowed)
                if isnumber:
                    return True
                else:
                    aMsg = 'ERROR: Minus sign valid only in first position.'
                    self.myMsgBar.newMessage('error', aMsg)
                    return False
            elif textInvolved == '.':
                isnumber = re.match(num_format, valueIfAllowed)
                if isnumber:
                    return True
                else:
                    aMsg = 'ERROR: Invalid amount format after dot is entered.'
                    self.myMsgBar.newMessage('error', aMsg)
                    return False
            elif textInvolved.isdigit() == True:
                isnumber = re.match(num_format, valueIfAllowed)
                if isnumber:
                    return True
                else:
                    aMsg = 'ERROR: Invalid amount format ({0} digits after the decimal point, 9 total digits).'.format(
                        decimal)
                    self.myMsgBar.newMessage('error', aMsg)
                    return False
            else:
                aMsg = 'ERROR: Only digits, a minus sign or a dot are allowed.'
                self.myMsgBar.newMessage('error', aMsg)
                return False

        if isnumber:
            return True
        else:
            aMsg = 'ERROR: Invalid amount format ({0} digits after the decimal point, 9 total digits).'.format(decimal)
            self.myMsgBar.newMessage('error', aMsg)
            return False

    def addValidationAmount(self, decimal, totalDigits, myMsgBar):
        self.myMsgBar = myMsgBar
        vcmd = (self.parent.register(self._validateAmount),
                '%d', '%i', '%P', '%s', '%S', '%v', '%V', '%W', decimal, totalDigits)
        self.anEntry.config(validate="none", validatecommand=vcmd)

class AppFieldEntryDate(Tix.Frame):
    def __init__(self, parent, aLabel, orientation, aWidth, aProc, myMsgBar):
        #
        #  aProc is a procedure to execute if item is not in the list.  If no
        # procedure is passed to the class, no action is taken.
        #
        Tix.Frame.__init__(self, parent)
        self.myMsgBar = myMsgBar

        self.config(pady=offsetYFieldEntry,takefocus=0)
        myRow = 0
        myColumn = 0
        self.dateVar = Tix.StringVar()
        self._dateEnteredActionProc = aProc

        if aLabel != None:
            if orientation == 'H':
                aLabel = ttk.Label(self, text='{0}: '.format(aLabel), takefocus=0, style='RegularFieldTitle.TLabel')
                aLabel.grid(row=myRow, column=myColumn, sticky=E + N + S)
                myRow = 0
                myColumn = 1
            else:
                aLabel = ttk.Label(self, text='{0}'.format(aLabel), takefocus=0, style='RegularFieldTitle.TLabel')
                aLabel.grid(row=myRow, column=myColumn, sticky=N + S)
                myRow = 1
                myColumn = 0

        self.aFrame = Frame(self, highlightcolor=AppDefaultForeground,
                            highlightthickness=AppHighlightBorderWidth, takefocus=0)
#        self.aFrame = Frame(self, border=0, highlightcolor=AppDefaultForeground,
#                            highlightthickness=AppHighlightBorderWidth, takefocus=1)
        #        aFrame=Frame(self, takefocus=0)
        if orientation == 'H':
            self.aFrame.grid(row=myRow, column=myColumn, sticky=W + N)
        else:
            self.aFrame.grid(row=myRow, column=myColumn, sticky=N)

#        self.anEntry = AppACListEntry(self.aFrame, topFrame, aList, aWidth, aProc, self.myMsgBar)

        self.anEntry = Entry(self.aFrame, takefocus=1, font=fontBig, background=AppDefaultEditBackground,
                             disabledforeground=AppDefaultForeground, width=aWidth, textvariable=self.dateVar,
                             justify=CENTER)

        self.anEntry.grid(row=0, column=0, sticky=N)

        parseDate = parser.parse(self.dateVar.get())
        editImg = resizeImage(Image.open(editRecImage), iconWidth, iconHeight)
        self.editProcButton = ttk.Button(self.aFrame, image=editImg, style='ComboBoxButton.TButton', takefocus=0,
                                 command=lambda: fnCalendarSet(self.dateVar, parseDate.year,
                                                               parseDate.month, parseDate.day))
        self.editProcButton.img = editImg
        self.editProcButton.grid(row=0, column=1, sticky=N + S)
        self.editProcButton_ttp = CreateToolTip(self.editProcButton, "Change Date")
        if aProc != None:
            self.setVarAction()
        self.enable()

    def setAsRequiredField(self):
        self.anEntry.config(background=AppDefaultRequiredFieldBackground)

    def resetAsRequiredField(self):
        self.anEntry.config(background=AppDefaultEditBackground)

    def setVarAction(self):
            self.dateVar.trace('w', self._dateEnteredActionProc)

    def focus(self):
            self.anEntry.focus()

    def disable(self):
            self.anEntry.config(state=DISABLED)
            self.editProcButton.configure(state=DISABLED)
            self.anEntry.unbind('<Enter>')
            self.anEntry.unbind('<FocusOut>')

    def clear(self):
            self.anEntry.config(state='normal')
            self.anEntry.delete(0, END)
            self.anEntry.config(state=DISABLED)

    def get(self):
            return self.anEntry.get()

    def load(self, aValue):
            self.anEntry.config(state='normal')
            self.anEntry.delete(0, END)
            self.anEntry.insert(0, aValue)
            self.anEntry.config(state=DISABLED)

    def enable(self):
            self.anEntry.config(state='normal', takefocus=1)
            self.editProcButton.configure(state=NORMAL)
            self.anEntry.bind('<Enter>', widgetEnter)
            self.anEntry.bind('<FocusOut>', self._check)

    def _check(self, event):
        try:
            parsed = parser.parse(self.get())
            self.myMsgBar.clearMessage()
        except:
            aMsg = "ERROR: Invalid date format."
            self.myMsgBar.newMessage('error', aMsg)
            self.focus()

class AppFieldEntry(Tix.Frame):
    def __init__(self, parent, aLabel, orientation, aWidth):
        self.myMsgBar = None
        self.aWidth = aWidth
        Tix.Frame.__init__(self, parent)
        self.parent = parent
        self.config(takefocus=0)
        myRow = 0
        myColumn = 0
        self.isScaEntry = False

        if aLabel != None:
            if orientation == 'H':
                aLabel = ttk.Label(self, text='{0}: '.format(aLabel), takefocus=0, style='RegularFieldTitle.TLabel')
                aLabel.grid(row=myRow, column=myColumn, sticky=E + N)
                myRow = 0
                myColumn = 1
            elif orientation == 'V':
                aLabel = ttk.Label(self, text='{0}'.format(aLabel), takefocus=0, style='RegularFieldTitle.TLabel')
                aLabel.grid(row=myRow, column=myColumn, sticky=N)
                myRow = 1
                myColumn = 0

        self.anEntry = Tix.Entry(self, highlightcolor=AppDefaultForeground, highlightthickness=AppHighlightBorderWidth,
                                 width=aWidth, font=fontBig,
                                 background=AppDefaultEditBackground, disabledforeground=AppDefaultForeground)
        self.anEntry.grid(row=myRow, column=myColumn, sticky=N + S)

    def setAsScaEntry(self):
        self.isScaEntry = True

    def setAsRequiredField(self):
        self.anEntry.config(background=AppDefaultRequiredFieldBackground)

    def resetAsRequiredField(self):
        self.anEntry.config(background=AppDefaultEditBackground)

    def setAsDeleted(self):
        self.anEntry.config(disabledforeground=AppDefaultDisabledForeground)

    def resetAsDeleted(self):
        self.anEntry.config(disabledforeground=AppDefaultForeground)

    def justification(self, aValue):
        self.anEntry.config(justify=aValue)

    def focus(self):
        self.anEntry.focus()

    def disable(self):
        self.anEntry.config(state=DISABLED)
        self.anEntry.unbind('<Enter>')
        self.anEntry.config(takefocus=0)

    def clear(self):
        self.anEntry.config(state='normal')
        self.anEntry.delete(0, END)
        self.anEntry.config(state=DISABLED)

    def get(self):
        if self.isScaEntry == True:
            aStr = self.anEntry.get()
            return aStr[2:4]
        else:
            return self.anEntry.get()

    def load(self, aValue):
        if aValue == None:
            aValue = ''
        elif self.isScaEntry == True:
            aValue = "A0{0} 3600".format(aValue)

        self.anEntry.config(state='normal')
        self.anEntry.delete(0, END)
        self.anEntry.insert(0, aValue)
        self.anEntry.config(state=DISABLED)

    def enable(self):
        self.anEntry.config(state='normal')
        self.anEntry.bind('<Enter>', widgetEnter)
        self.anEntry.config(takefocus=1)

    def addVariable(self, aVar):
        self.anEntry.config(textvariable=aVar)

class AutocompleteCombobox(ttk.Combobox):
    def set_completion_list(self, completion_list):
        """Use our completion list as our drop down selection menu, arrows move through menu."""
        self._completion_list = sorted(completion_list, key=str.lower)  # Work with a sorted list
        self._hits = []
        self._hit_index = 0
        self.position = 0
        self.bind('<KeyRelease>', self.handle_keyrelease)
        self['values'] = self._completion_list  # Setup our popup menu

    def autocomplete(self, delta=0):
        """autocomplete the Combobox, delta may be 0/1/-1 to cycle through possible hits"""
        if delta:  # need to delete selection otherwise we would fix the current position
            self.delete(self.position, END)
        else:  # set position to end so selection starts where textentry ended
            self.position = len(self.get())
        # collect hits
        _hits = []
        for element in self._completion_list:
            if element.lower().startswith(self.get().lower()):  # Match case insensitively
                _hits.append(element)
        # if we have a new hit list, keep this in mind
        if _hits != self._hits:
            self._hit_index = 0
            self._hits = _hits
        # only allow cycling if we are in a known hit list
        if _hits == self._hits and self._hits:
            self._hit_index = (self._hit_index + delta) % len(self._hits)
        # now finally perform the auto completion
        if self._hits:
            self.delete(0, END)
            self.insert(0, self._hits[self._hit_index])
            self.select_range(self.position, END)

    def handle_keyrelease(self, event):
        """event handler for the keyrelease event on this widget"""
        if event.keysym == "BackSpace":
            self.delete(self.index(INSERT), END)
            self.position = self.index(END)
        if event.keysym == "Left":
            if self.position < self.index(END):  # delete the selection
                self.delete(self.position, END)
            else:
                self.position = self.position - 1  # delete one character
                self.delete(self.position, END)
        if event.keysym == "Right":
            self.position = self.index(END)  # go to end (no selection)
        if len(event.keysym) == 1:
            self.autocomplete()
            # No need for up/down, we'll jump to the popup
            # list at the position of the autocompletion


class AppACListEntry(Entry):
    #
    #   Special List Box Entry
    #      1. present a listbox with choices
    #      2. can be configure to execute an add script if choice is not present in list
    #      3. It will force to select from list otherwise
    #      4. In order to open listbox at correct place, parent's parent must be provided
    #      5. Popup list removes unmatched items (anywhere, not case sensitive)
    #
    #
    def __init__(self, parent, parentof, lista, fieldWidth, addItemProc, myMsgBar, *args, **kwargs):
        self.fieldWidth = fieldWidth
        self.addItemProc = addItemProc
        Entry.__init__(self, parent, *args, **kwargs)
        self.parent = parent
        self.parentof = parentof
        self.myMsgBar = myMsgBar
        self.search = True
        self.traceProc = None
        self.trace = False

        self.config(takefocus=1, font=fontBig, background=AppDefaultEditBackground,
                    disabledforeground=AppDefaultForeground, width=self.fieldWidth,
                    justify=LEFT)

        #        self.config(highlightcolor=AppDefaultForeground, highlightthickness = AppHighlightBorderWidth, width=self.fieldWidth,
        #                    justify=LEFT, font=fontBig, disabledforeground=AppDefaultForeground)
        #        self.bind('<Enter>', self.enter)
        #        self.bind('<Return>', self.enterKeyEntered)
        #        self.bind('<FocusOut>', self.check)
        self.lista = lista
        self.var = self["textvariable"]
        if self.var == '':
            self.var = self["textvariable"] = StringVar()
        self.var.trace("w", self.changed)
        self.config(state=DISABLED)
        #        self.bind("<ButtonRelease-1>", self.selectItem)
        #        self.bind("<Right>", self.enterKeyEntered)
        #        self.bind("<Return>", self.selection)
        #        self.bind("<Up>", self.up)
        #        self.bind("<Down>", self.down)
        #        self.bind("<MouseWheel>", self._on_mousewheel)
        #        self.bind("<Key>", self.cancel)
        self.lb_up = False
        self.lbName = None
        self.enableEntry = False
        self.disableVerificationAgainstList = True

    def clearPopups(self):
        if self.lb_up == True:
            self.lb.destroy()
            self.lb_up = False

    def disableCheck(self):
        self.disableVerificationAgainstList = True

    def enableCheck(self):
        self.disableVerificationAgainstList = False

    def setAsDeleted(self):
        self.config(disabledforeground=AppDefaultDisabledForeground)

    def resetAsDeleted(self):
        self.config(disabledforeground=AppDefaultForeground)

    def setAsRequiredField(self):
        self.config(background=AppDefaultRequiredFieldBackground)

    def resetAsRequiredField(self):
        self.config(background=AppDefaultEditBackground)

    def addTrace(self, aTraceProc):
        self.trace = True
        self.traceProc = aTraceProc

    def enable(self):
        self.enableEntry = True
        #        self.bind('<Enter>', self.enter)
        self.bind('<FocusOut>', self.check)
        self.bind("<ButtonRelease-1>", self.selectItem)
        self.bind("<Right>", self.enterKeyEntered)
        self.bind("<Return>", self.selection)
        self.bind("<Up>", self.up)
        self.bind("<Down>", self.down)
        self.bind("<MouseWheel>", self._on_mousewheel)
        self.bind("<Key>", self.cancel)

    #        self.config(background=AppDefaultEditBackground)

    def disable(self):
        self.enableEntry = False
        #        self.unbind('<Enter>')
        self.unbind('<FocusOut>')
        self.unbind("<ButtonRelease-1>")
        self.unbind("<Right>")
        self.unbind("<Return>")
        self.unbind("<Up>")
        self.unbind("<Down>")
        self.unbind("<MouseWheel>")
        self.unbind("<Key>")

    def cancel(self, event):
        if event.char == chr(27):
            if self.lb_up:
                self.lb.destroy()
                self.lb_up = False
        else:
            self.enableCheck()

    def enterKeyEntered(self, *args):
        self.enableCheck()
        if not self.lb_up:
            self.lb = self.createLB()
            for w in self.lista:
                self.lb1.insert(END, w)
            index = 0
            self.lb1.selection_set(first=index)
            self.lb1.activate(index)
        if len(self.var.get()) > 0:
            self.changed(None, None, None)

    def createLB(self):
        self.enableCheck()
        if len(self.lista) > 9:
            height = 10
        else:
            height = len(self.lista)

        lb = AppFrame(self.parentof, 2)
        lb.noBorder()

        lb.place(in_=self.parentof, relx=0, rely=0, anchor=NW,
                 x=self.parent.winfo_rootx() - self.parentof.winfo_rootx(),
                 y=self.parent.winfo_rooty() - self.parentof.winfo_rooty() + self.winfo_height() + AppHighlightBorderWidth)

        if len(self.lista) + 1 > 10:
            yScroll = Scrollbar(lb, orient=VERTICAL)
            yScroll.grid(row=0, column=1, sticky=N + S)

            self.lb1 = Listbox(lb, width=self.fieldWidth + pullDownAdjustmentWidth, font=fontBig, height=height,
                               border=AppDefaultBorderWidth, yscrollcommand=yScroll.set, takefocus=0)
            self.lb1.grid(row=0, column=0, sticky=N + S + W + E)
            yScroll['command'] = self.lb1.yview
        else:
            self.lb1 = Listbox(lb, width=self.fieldWidth + pullDownAdjustmentWidth, font=fontBig, height=height,
                               border=AppDefaultBorderWidth, takefocus=0)
            self.lb1.grid(row=0, column=0, sticky=N + S + W + E)

        # lb = Listbox(self.parentof, width=self.fieldWidth, font=fontBig, height=height, border=2, takefocus=0)
        #        lb.place(in_=self.parentof, relx=0, rely=0, anchor=NW, x= self.parent.winfo_rootx()- self.parentof.winfo_rootx(),
        #                  y= self.parent.winfo_rooty() - self.parentof.winfo_rooty() + self.winfo_height())
        self.lb1.bind("<ButtonRelease-1>", self.selectItem)
        self.lb1.bind("<Double-Button-1>", self.selection)
        self.lb1.bind("<Right>", self.selection)
        self.lb1.bind("<Up>", self.up)
        self.lb1.bind("<Down>", self.down)
        self.lb1.bind("<MouseWheel>", self._on_mousewheel)
        self.lbName = self.lb1.winfo_name()
        self.lb_up = True
        return lb

    def _on_mousewheel(self, event):
        if event.delta > 0:
            self.lb1.yview_scroll(-2, 'units')
        else:
            self.lb1.yview_scroll(2, 'units')

    def enter(self, event):
        if self.enableEntry:
            if len(self.var.get()) > 0:
                self.changed(None, None, None)
            else:
                self.enterKeyEntered(None)
            self.focus()

    def check(self, event):
        #        self.myMsgBar.clearMessage()
        if self.disableVerificationAgainstList == False:
            if self.lbName != None:
                aName = event.widget.focus_lastfor()
                pattern = re.compile('.*' + self.lbName + '*.')
                if re.match(pattern, str(aName)) == None:
                    if not self.findInList():
                        if len(self.get()) > 0:
                            if self.addItemProc == None:  # Is there a proc
                                aMsg = "ERROR: Item must be selected from the list."
                                self.myMsgBar.newMessage('error', aMsg)
                                self.focus()
                            else:
                                self.addItemProc()
                                self.disableCheck()

                        else:
                            #
                            #  This is a catch in case noSupplierID not provided
                            #  Most cases will point to an ID which is a no supplier (empty fields)
                            #
                            aMsg = "INFO: No items selected."
                            self.myMsgBar.newMessage('info', aMsg)
                            self.clearPopups()
                    else:
                        self.myMsgBar.clearMessage()
                        self.disableCheck()
                        if self.lb_up:
                            self.lb.destroy()
                            self.lb_up = False
                        if self.trace == True:
                            if self.traceProc != None:
                                self.traceProc(event)
            else:
                if not self.findInList():
                    if len(self.get()) > 0:
                        if self.addItemProc == None:  # Is there a proc
                            aMsg = "ERROR: Item must be selected from the list."
                            self.myMsgBar.newMessage('error', aMsg)
                            self.focus()
                        else:
                            self.addItemProc()
                            self.disableCheck()

                    else:
                        #
                        #  This is a catch in case noSupplierID not provided
                        #  Most cases will point to an ID which is a no supplier (empty fields)
                        #
                        aMsg = "INFO: No items selected."
                        self.myMsgBar.newMessage('info', aMsg)
                        self.clearPopups()
                else:
                    self.myMsgBar.clearMessage()
                    self.disableCheck()
                    if self.lb_up:
                        self.lb.destroy()
                        self.lb_up = False
                    if self.trace == True:
                        if self.traceProc != None:
                            self.traceProc(event)

    def changed(self, name, index, mode):
        if self.enableEntry:
            if self.var.get() == '':
                if self.lb_up:
                    self.lb.destroy()
                    self.lb_up = False
                    self.enterKeyEntered(None)
            else:
                words = self.comparison()
                if words:
                    if not self.lb_up:
                        self.lb = self.createLB()

                    self.lb1.delete(0, END)
                    for w in words:
                        self.lb1.insert(END, w)
                    index = 0
                    self.lb1.selection_set(first=index)
                    self.lb1.activate(index)
                else:
                    if self.lb_up:
                        self.lb.destroy()
                        self.lb_up = False

    def selectItem(self, event):
        if self.lb_up:
            if self.lb1.curselection() == ():
                index = '0'
            else:
                index = self.lb1.curselection()[0]
            self.lb1.selection_clear(first=index)
            self.lb1.selection_set(first=index)
            self.lb1.activate(index)
        # self.reDisplayList(index)
        self.focus()

    def selection(self, event):
        if self.lb_up:
            self.var.set(self.lb1.get(ACTIVE))
            self.lb.destroy()
            self.lb_up = False
            self.icursor(END)
            focusNext(self, self)
            #            self.focus()

    def up(self, event):
        if self.lb_up:
            if self.lb1.curselection() == ():
                index = '0'
            else:
                index = self.lb1.curselection()[0]
            if index != '0':
                self.lb1.selection_clear(first=index)
                index = str(int(index) - 1)
                self.lb1.selection_set(first=index)
                self.lb1.activate(index)
            self.lb1.see(index)
            self.focus()

    def down(self, event):
        if self.lb_up:
            if self.lb1.curselection() == ():
                index = '0'
            else:
                index = self.lb1.curselection()[0]
            if index != END:
                self.lb1.selection_clear(first=index)
                index = str(int(index) + 1)
                self.lb1.selection_set(first=index)
                self.lb1.activate(index)
            self.lb1.see(index)
            self.focus()

    def disableSearch(self):
        self.search = False

    def enableSearch(self):
        self.search = True

    def comparison(self):
        #        pattern = re.escape('.*' + self.var.get() + '.*', re.IGNORECASE)
        if self.search == True:
            pattern1 = re.escape(self.var.get())
            pattern2 = re.compile('.*' + pattern1 + '.*', re.IGNORECASE)
            return [w for w in self.lista if re.match(pattern2, w)]
            self.focus()
        else:
            return self.lista
            self.focus()

    def findInList(self, *args):
        try:
            self.lista.index(self.var.get())
            return True
        except:
            return False

            #    def autowidth(self,maxwidth):
            #        f = font.Font(font=self.cget("font"))
            #        pixels = 0
            #        for item in self.get(0, "end"):
            #            pixels = max(pixels, f.measure(item))
            # bump listbox size until all entries fit


# pixels = pixels + 10
#        width = int(self.cget("width"))
#        for w in range(0, maxwidth+1, 5):
#            if self.winfo_reqwidth() >= pixels:
#                break
#            self.config(width=width+w)

class AppSearchLB(Tix.Frame):
    def __init__(self, parent, topFrame, aLabel, orientation, aList, aWidth, aProc, myMsgBar):
        #
        #  aProc is a procedure to execute if item is not in the list.  If no
        # procedure is passed to the class, no action is taken.
        #
        Tix.Frame.__init__(self, parent)
        self.topFrame = topFrame
        self.myMsgBar = myMsgBar

        self.aList = aList
        self.config(pady=offsetYFieldEntry,takefocus=0)
        myRow = 0
        myColumn = 0

        if aLabel != None:
            if orientation == 'H':
                aLabel = ttk.Label(self, text='{0}: '.format(aLabel), takefocus=0, style='RegularFieldTitle.TLabel')
                aLabel.grid(row=myRow, column=myColumn, sticky=E + N + S)
                myRow = 0
                myColumn = 1
            else:
                aLabel = ttk.Label(self, text='{0}'.format(aLabel), takefocus=0, style='RegularFieldTitle.TLabel')
                aLabel.grid(row=myRow, column=myColumn, sticky=N + S)
                myRow = 1
                myColumn = 0

        self.aFrame = Frame(self, highlightcolor=AppDefaultForeground,
                            highlightthickness=AppHighlightBorderWidth, takefocus=0)
        #        aFrame=Frame(self, takefocus=0)
        if orientation == 'H':
            self.aFrame.grid(row=myRow, column=myColumn, sticky=W + N)
        else:
            self.aFrame.grid(row=myRow, column=myColumn, sticky=N)

        self.anEntry = AppACListEntry(self.aFrame, topFrame, aList, aWidth, aProc, self.myMsgBar)
        self.anEntry.grid(row=0, column=0, sticky=N)

        pullDownImg = resizeImage(Image.open(pullDownImage), iconWidth, iconHeight)
        self.buttonPullDown = ttk.Button(self.aFrame, image=pullDownImg, style='ComboBoxButton.TButton', takefocus=0)
        self.buttonPullDown.img = pullDownImg
        self.buttonPullDown.grid(row=0, column=1, sticky=N+S)

        addImg = resizeImage(Image.open(addCommandRecImage), iconWidth, iconHeight)
        self.addProcButton = ttk.Button(self.aFrame, image=addImg, style='ComboBoxButton.TButton', takefocus=0)
        self.addProcButton.img = addImg
        self.addProcButton.grid(row=0, column=4, sticky=N+S)
        self.addProcButton_ttp = CreateToolTip(self.addProcButton, "Add a New Record")

        editImg = resizeImage(Image.open(editRecImage), iconWidth, iconHeight)
        self.editProcButton = ttk.Button(self.aFrame, image=editImg, style='ComboBoxButton.TButton', takefocus=0)
        self.editProcButton.img = editImg
        self.editProcButton.grid(row=0, column=5, sticky=N+S)
        self.editProcButton_ttp = CreateToolTip(self.editProcButton, "Edit Current Record")

        viewImg = resizeImage(Image.open(viewRecImage), iconWidth, iconHeight)
        self.viewProcButton = ttk.Button(self.aFrame, image=viewImg, style='ComboBoxButton.TButton', takefocus=0)
        self.viewProcButton.img = viewImg
        self.viewProcButton.grid(row=0, column=2, sticky=N+S)
        self.viewProcButton_ttp = CreateToolTip(self.viewProcButton, "View Record Details")

        changeImg = resizeImage(Image.open(changeRecImage), iconWidth, iconHeight)
        self.changeProcButton = ttk.Button(self.aFrame, image=changeImg, style='ComboBoxButton.TButton', takefocus=0)
        self.changeProcButton.img = changeImg
        self.changeProcButton.grid(row=0, column=3, sticky=N+S)
        self.changeProcButton_ttp = CreateToolTip(self.changeProcButton, "Turn on Record Update")

        cancelImg = resizeImage(Image.open(cancelCommandRecImage), iconWidth, iconHeight)
        self.cancelProcButton = ttk.Button(self.aFrame, image=cancelImg, style='ToolTipButton.TButton', takefocus=0)
        self.cancelProcButton.img = cancelImg
        self.cancelProcButton.grid(row=0, column=6, sticky=N+S)
        self.cancelProcButton_ttp = CreateToolTip(self.cancelProcButton, "Cancel Record Update")

        self.editProcButton.grid_forget()
        self.addProcButton.grid_forget()
        self.viewProcButton.grid_forget()
        self.changeProcButton.grid_forget()
        self.cancelProcButton.grid_forget()
        self.editCurrentItemActive = False
        self.addCurrentItemActive = False
        self.viewCurrentItemActive = False
        self.changeCurrentItemActive = False
        self.cancelCurrentItemActive = False

        #        self.anEntry.config(state=DISABLED)
        self.updateValuesList(aList)

    def disableCheck(self):
        self.anEntry.disableCheck()

    def addCancelProc(self, aProc):
        self.cancelProcButton.grid(row=0, column=6, sticky=N)
        self.cancelItem = aProc
        self.cancelCurrentItemActive = True

    def addChangeProc(self, aProc):
        self.changeProcButton.grid(row=0, column=3, sticky=N)
        self.changeItem = aProc
        self.changeCurrentItemActive = True

    def addEditProc(self, aProc):
        self.editProcButton.grid(row=0, column=5, sticky=N)
        self.editCurrentItem = aProc
        self.editCurrentItemActive = True

    def addAddProc(self, aProc):
        self.addProcButton.grid(row=0, column=4, sticky=N)
        self.addNewItem = aProc
        self.addCurrentItemActive = True

    def addViewProc(self, aProc):
        self.viewProcButton.grid(row=0, column=2, sticky=N)
        self.viewCurrentItem = aProc
        self.viewProcButton.config(state=NORMAL)
        self.viewProcButton.bind('<Button>', self.viewCurrentItem)
        self.viewCurrentItemActive = True

    def setAsDeleted(self):
        self.anEntry.setAsDeleted()

    def resetAsDeleted(self):
        self.anEntry.resetAsDeleted()

    def setAsRequiredField(self):
        self.anEntry.setAsRequiredField()

    def resetAsRequiredField(self):
        self.anEntry.resetAsRequiredField()

    def addTrace(self, aTraceProc):
        self.anEntry.addTrace(aTraceProc)

    def justification(self, aValue):
        self.anEntry.config(justify=aValue)

    def clearPopups(self):
        if self.anEntry.lb_up == True:
            self.anEntry.lb.destroy()
            self.anEntry.lb_up = False

    def enter(self, event):
        self.anEntry.focus()

    #        self.aFrame.focus()

    def focus(self):
        self.anEntry.focus()

    #        self.aFrame.focus()

    def get(self):
        return (self.anEntry.get())

    def clear(self):
        self.anEntry.config(state='normal')
        self.anEntry.delete(0, END)
        self.anEntry.config(state=DISABLED)

    def load(self, aValue):
        if aValue == None:
            aValue = ''
        self.anEntry.config(state='normal')
        self.anEntry.delete(0, END)
        self.anEntry.insert(0, aValue)
        self.anEntry.config(state=DISABLED)
        self.clearPopups()

    def find(self):
        self.anEntry.config(state='normal', takefocus=1)
        self.aFrame.config(takefocus=1)

        #        self.buttonPullDown.config(state=NORMAL)
        #        self.buttonPullDown.bind('<Button>', self.anEntry.enter)
        if self.viewCurrentItemActive:
            self.viewProcButton.config(state=DISABLED)
            self.viewProcButton.unbind('<Button>')

            #        self.bind('<Enter>', self.enter)

    def enableCancelButton(self):
        self.cancelProcButton.config(state=NORMAL)
        self.cancelProcButton.bind('<Button>', self.cancelItem)

    def enableChangeButton(self):
        self.changeProcButton.config(state=NORMAL)
        self.changeProcButton.bind('<Button>', self.changeItem)

    def focusout(self, *args):
        self.clearPopups()

    def enable(self):
        self.anEntry.config(state='normal', takefocus=1)
        self.aFrame.config(takefocus=0)

        #        self.bind('<FocusOut>', self.focusout)

        if self.editCurrentItemActive:
            self.editProcButton.config(state=NORMAL)
            self.editProcButton.bind('<Button>', self.editCurrentItem)

        # if self.viewCurrentItemActive:
        #            self.viewProcButton.config(state=NORMAL)
        #            self.viewProcButton.bind('<Button>', self.viewCurrentItem)

        if self.addCurrentItemActive:
            self.addProcButton.config(state=NORMAL)
            self.addProcButton.bind('<Button>', self.addNewItem)

        self.buttonPullDown.config(state=NORMAL)
        self.buttonPullDown.bind('<Button>', self.anEntry.enter)

        self.bind('<Enter>', self.enter)
        #        self.bind("<Right>", self.anEntry.enterKeyEntered)
        self.anEntry.enable()

    def testing(self, *args):
        print("Received")

    def disable(self):
        self.anEntry.config(state=DISABLED, takefocus=0)
        self.aFrame.config(takefocus=0)
        self.unbind('<Enter>')
        #        self.bind('<FocusOut>')
        self.anEntry.unbind('<Enter>')
        self.anEntry.disable()

        self.buttonPullDown.config(state=DISABLED)
        self.buttonPullDown.unbind('<Button>')

        if self.editCurrentItemActive:
            self.editProcButton.config(state=DISABLED)
            self.editProcButton.unbind('<Button>')

        if self.cancelCurrentItemActive:
            self.cancelProcButton.config(state=DISABLED)
            self.cancelProcButton.unbind('<Button>')

        if self.changeCurrentItemActive:
            self.changeProcButton.config(state=DISABLED)
            self.changeProcButton.unbind('<Button>')

        if self.viewCurrentItemActive:
            self.viewProcButton.config(state=NORMAL)
            self.viewProcButton.bind('<Button>', self.viewCurrentItem)
        # self.viewProcButton.config(state=DISABLED)
        #        self.viewProcButton.unbind('<Button>')

        if self.addCurrentItemActive:
            self.addProcButton.config(state=DISABLED)
            self.addProcButton.unbind('<Button>')

        self.clearPopups()

    def updateValuesList(self, aList):
        self.aList = aList
        self.anEntry.lista = aList

    def disableSearch(self):
        self.anEntry.disableSearch()

    def enableSearch(self):
        self.anEntry.enableSearch()

class AppColumnAlignFieldFrame(Tix.Frame):
    def __init__(self, parent):
        Tix.Frame.__init__(self, parent)
        self.config(border=AppDefaultBorderWidth, relief='ridge')
        self.row = 0
        self.column = 0
        self.columnTotal = 2
        self.itemNumber = 0
        self.vertical = False
        self.rowconfigure(self.row, weight=0)
        for i in range(self.columnTotal):
            self.columnconfigure(i, weight=1)

    def noBorder(self):
        self.config(border=0)

    def defaultBorder(self):
        self.config(border=AppDefaultBorderWidth)

    def addTitle(self, aTitle):
        aLabel = ttk.Label(self, text='{0}'.format(aTitle), takefocus=0, style='RegularFieldTitle.TLabel')
        aLabel.grid(row=self.row, column=self.column, columnspan=self.columnTotal, sticky=N)
        self.itemNumber = self.itemNumber + 1
        self.vertical = True

    def addFrame(self, aFrame):
        self.itemNumber = self.itemNumber + 1
        self.row = self.itemNumber - 1
        aFrame.grid(row=self.row, column=0, columnspan=self.columnTotal, sticky=N)

    def addFrameStrech(self, aFrame):
        self.itemNumber = self.itemNumber + 1
        self.row = self.itemNumber - 1
        aFrame.grid(row=self.row, column=0, columnspan=self.columnTotal, sticky=N + W + E)

    def addFrameLeftAlign(self, aFrame):
        self.itemNumber = self.itemNumber + 1
        self.row = self.itemNumber - 1
        aFrame.grid(row=self.row, column=0, columnspan=self.columnTotal, sticky=N + W)

    def addFrameRightAlign(self, aFrame):
        self.itemNumber = self.itemNumber + 1
        self.row = self.itemNumber - 1
        aFrame.grid(row=self.row, column=0, columnspan=self.columnTotal, sticky=N + E)

    def addEmptyRow(self):
        self.itemNumber = self.itemNumber + 1
        self.row = self.itemNumber - 1
        aFrame = AppBorderFrame(self, 1)
        aFrame.noBorder()
        aFrame.grid(row=self.row, column=0, columnspan=self.columnTotal, sticky=N + E)

        anEntry = Tix.Entry(aFrame, highlightcolor=AppDefaultForeground, highlightthickness=AppHighlightBorderWidth,
                            width=10, font=fontBig, relief=FLAT,
                            background=AppDefaultEditBackground, disabledforeground=AppDefaultForeground)
        anEntry.grid(row=0, column=0, sticky=E + N)
        anEntry.config(state=DISABLED)

    def addTextCenter(self, aText, alignment):
        self.itemNumber = self.itemNumber + 1
        self.row = self.itemNumber - 1
        aFrame = AppBorderFrame(self, 1)
        aFrame.noBorder()
        aFrame.grid(row=self.row, column=0, columnspan=self.columnTotal, sticky=N + E + W)

        anEntry = Tix.Entry(aFrame, highlightcolor=AppDefaultForeground, highlightthickness=AppHighlightBorderWidth,
                            font=fontBigI, relief=FLAT,
                            background=AppDefaultEditBackground, disabledforeground=AppDefaultForeground)
        anEntry.grid(row=0, column=0, sticky=E + N + W)
        anEntry.insert(0, aText)
        anEntry.config(justify=alignment)
        anEntry.config(state=DISABLED)

    def addNewField(self, aTitle, anEntry):
        self.itemNumber = self.itemNumber + 1
        self.row = self.itemNumber - 1
        if self.vertical == False:
            if aTitle == None:
                aLabel = ttk.Label(self, text=''.format(aTitle), takefocus=0, style='RegularFieldTitle.TLabel')
            else:
                aLabel = ttk.Label(self, text='{0}: '.format(aTitle), takefocus=0, style='RegularFieldTitle.TLabel')
        else:
            aLabel = ttk.Label(self, text=''.format(aTitle), takefocus=0, style='RegularFieldTitle.TLabel')
        aLabel.grid(row=self.row, column=0, sticky=E + N)
        anEntry.grid(row=self.row, column=1, sticky=W + N)

class AppCBList():
    def __init__(self, aComboBoxData):
        self.updateDictionaries(aComboBoxData)

    def getList(self):
        aList = []
        for i in range(len(self.aComboBoxData)):
            aList.append(self.aComboBoxData[i][1])
        return aList

    def getName(self, ID):
        return self.aReverseDictionary[ID]

    def getID(self, Name):
        return (self.aDictionary[Name])

    def updateDictionaries(self, aComboBoxData):
        self.aComboBoxData = aComboBoxData
        self.aDictionary = {}
        self.aReverseDictionary = {}
        for i in range(len(aComboBoxData)):
            self.aDictionary.update({aComboBoxData[i][1]: aComboBoxData[i][0]})
            self.aReverseDictionary.update({aComboBoxData[i][0]: aComboBoxData[i][1]})

class AppCB_AC(Tix.Frame):
    def __init__(self, parent, aList, aWidth, addItemProc, msgWidget):
        self.addItemProc = addItemProc
        self.msgWidget = msgWidget
        Tix.Frame.__init__(self, parent)

        self.config(border=2, highlightcolor=AppDefaultForeground, highlightthickness=AppHighlightBorderWidth,
                    takefocus=0)
        self.bind('<Enter>', widgetEnter)
        self.bind('<FocusOut>', self.check)
        self.Name = self.winfo_name()
        self.CB = AutocompleteCombobox(self, width=aWidth, font=fontBig, justify=LEFT, background=AppDefaultBackground)
        self.CB.grid(row=0, column=0, sticky=N + S + E + W)
        self.CB.set_completion_list(aList)
        self.CB.bind("<ComboboxSelected>")
        self.CB.config(state=DISABLED)
        self.CBName = self.CB.winfo_name()
        self.updateValuesList(aList)

    def check(self, event):
        #        self.msgWidget.clearMessage()
        aName = event.widget.focus_lastfor()
        pattern = re.compile('.*' + self.CBName + '*.', re.IGNORECASE)
        if re.match(pattern, str(aName)):
            pass
        else:
            if not self.findInList():
                if len(self.CB.get()) > 0:
                    if self.addItemProc == None:
                        aMsg = "ERROR: Items must be in the list."
                        self.msgWidget.newMessage('error', aMsg)
                        self.focus()
                    else:
                        self.addItemProc()

    def focus(self):
        self.CB.focus()

    def get(self):
        return (self.CB.get())

    def clear(self):
        self.CB.config(state='normal')
        self.CB.delete(0, END)
        self.CB.config(state=DISABLED)

    def load(self, aValue):
        if aValue == None:
            aValue = ''
        self.CB.config(state=NORMAL)
        self.CB.delete(0, END)
        self.CB.insert(0, aValue)
        self.CB.config(state=DISABLED)

    def enable(self):
        self.CB.config(state=NORMAL, takefocus=1)
        self.bind('<Enter>', widgetEnter)

    def disable(self):
        self.CB.config(state=DISABLED, takefocus=0)
        self.unbind('<Enter>')

    def updateValuesList(self, aList):
        self.aList = aList
        self.CB.config(state=NORMAL)
        self.CB.delete(0, END)
        self.CB.set_completion_list(aList)

    def findInList(self, *args):
        try:
            self.aList.index(self.CB.get())
            return True
        except:
            return False

class AppCB(Frame):
    def __init__(self, parent, topFrame, aLabel, orientation, aList, aWidth, myMsgBar):
        Frame.__init__(self, parent)
        self.topFrame = topFrame
        self.myMsgBar = myMsgBar
        self.aWidth = aWidth
        
        self.aList = aList
        self.config(border=2, takefocus=0)
        myRow = 0
        myColumn = 0

        if aLabel != None:       
            if orientation == 'H':
                aLabel = ttk.Label(self, text='{0}: '.format(aLabel), takefocus=0, border=0, style='RegularFieldTitle.TLabel')
                aLabel.grid(row=myRow, column=myColumn, sticky=E+N+S)
                myRow = 0
                myColumn = 1
            else:
                aLabel = ttk.Label(self, text='{0}'.format(aLabel), takefocus=0, border=0, style='RegularFieldTitle.TLabel')
                aLabel.grid(row=myRow, column=myColumn, sticky=N+S)
                myRow = 1
                myColumn = 0

#        self.aFrame=Frame(self, border=0, highlightcolor=AppDefaultForeground, highlightthickness = AppHighlightBorderWidth, takefocus=1)
        self.aFrame=Frame(self, border=0)
        if orientation == 'H':
            self.aFrame.grid(row=myRow, column=myColumn, sticky=N+S)
        else:
            self.aFrame.grid(row=myRow, column=myColumn, sticky=N+S)

        self.anEntry = AppLBforCB(self.aFrame, self.topFrame, self.aList, self.aWidth, self.myMsgBar)
        self.anEntry.grid(row=0, column=0, sticky= N+S)

        pullDownImg = resizeImage(Image.open(pullDownImage),iconWidth,iconHeight)                     
        self.buttonPullDown = ttk.Button(self.aFrame, image=pullDownImg, style='ComboBoxButton.TButton', takefocus=0)
        self.buttonPullDown.img = pullDownImg
        self.buttonPullDown.grid(row=0, column=1, sticky=N+S)
#        self.buttonFind_ttp = CreateToolTip(self.buttonFind, "Find Record(s)")
#        self.anEntry.config(state=DISABLED)
#        self.updateValuesList(aList)
        
    def clearPopups(self):
        if self.anEntry.lb_up == True:
            self.anEntry.lb.destroy()
            self.anEntry.lb_up = False
                   
    def justification(self, aValue):
        self.anEntry.config(justify=aValue)
        
    def enter(self,event):
        self.aFrame.focus()

    def focus(self):
        self.aFrame.focus()
    
    def enable(self):
        self.anEntry.enable() 
        self.aFrame.config(takefocus=1)    
        self.buttonPullDown.config(state=NORMAL) 
        self.bind('<Enter>', self.enter)
        self.buttonPullDown.bind('<Button>', self.anEntry.enter)
                  
    def disable(self):
        self.anEntry.disable()
        self.buttonPullDown.config(state=DISABLED) 
        self.aFrame.config(takefocus=0)            
        self.buttonPullDown.unbind('<Button>')
        self.clearPopups()
        self.unbind('<Enter>')
        
    def addTrace(self, aTraceProc):
        self.anEntry.addTrace(aTraceProc)

    def clear(self):
        self.anEntry.config(state='normal')
        self.anEntry.delete(0, END)
        self.anEntry.config(state=DISABLED)
                  
    def load(self, aValue):
        self.anEntry.config(state='normal')
        self.anEntry.delete(0, END)
        self.anEntry.insert(0, aValue)
        self.anEntry.config(state=DISABLED)   
        
    def updateList(self, aList): 
        self.anEntry.updateList(aList)


class AppCBV0(Tix.Frame):
    def __init__(self, parent, topFrame, aLabel, orientation, aList, aWidth, myMsgBar):
        Tix.Frame.__init__(self, parent)
        self.topFrame = topFrame
        self.myMsgBar = myMsgBar
        self.aWidth = aWidth

        self.aList = aList
        self.config(takefocus=0)
        myRow = 0
        myColumn = 0

        if aLabel != None:
            if orientation == 'H':
                aLabel = ttk.Label(self, text='{0}: '.format(aLabel), takefocus=0, border=0,
                                   style='RegularFieldTitle.TLabel')
                aLabel.grid(row=myRow, column=myColumn, sticky=E + N + S)
                myRow = 0
                myColumn = 1
            else:
                aLabel = ttk.Label(self, text='{0}'.format(aLabel), takefocus=0, border=0,
                                   style='RegularFieldTitle.TLabel')
                aLabel.grid(row=myRow, column=myColumn, sticky=N+S)
                myRow = 1
                myColumn = 0

        self.aFrame = Frame(self, border=0, highlightcolor=AppDefaultForeground,
                            highlightthickness=AppHighlightBorderWidth, takefocus=1)
        if orientation == 'H':
            self.aFrame.grid(row=myRow, column=myColumn, sticky=N)
        else:
            self.aFrame.grid(row=myRow, column=myColumn, sticky=N)

        self.anEntry = AppLBforCB(self.aFrame, self.topFrame, self.aList, self.aWidth, self.myMsgBar)
        self.anEntry.grid(row=0, column=0, sticky=N+S)

        pullDownImg = resizeImage(Image.open(pullDownImage), iconWidth, iconHeight)
        self.buttonPullDown = ttk.Button(self.aFrame, image=pullDownImg, style='ComboBoxButton.TButton', takefocus=0)
        self.buttonPullDown.img = pullDownImg
        self.buttonPullDown.grid(row=0, column=1, sticky=N+S)

    #        self.buttonFind_ttp = CreateToolTip(self.buttonFind, "Find Record(s)")
    #        self.anEntry.config(state=DISABLED)
    #        self.updateValuesList(aList)

    def setAsDeleted(self):
        self.anEntry.setAsDeleted()

    def resetAsDeleted(self):
        self.anEntry.resetAsDeleted()

    def setAsRequiredField(self):
        self.anEntry.setAsRequiredField()

    def resetAsRequiredField(self):
        self.anEntry.resetAsRequiredField()

    def clearPopups(self):
        if self.anEntry.lb_up == True:
            self.anEntry.lb.destroy()
            self.anEntry.lb_up = False

    def justification(self, aValue):
        self.anEntry.config(justify=aValue)

    def get(self):
        return self.anEntry.get()

    def enter(self, event):
        self.aFrame.focus()

    def focus(self):
        self.aFrame.focus()

    def find(self):
        self.anEntry.enable()
        self.aFrame.config(takefocus=1)
        self.buttonPullDown.config(state=NORMAL)
        self.activateBindings()
        self.anEntry.disableTrace()

    def activateBindings(self):
        self.aFrame.bind('<Right>', self.anEntry.enter)
        self.aFrame.bind('<Down>', self.anEntry.enter)
        self.bind('<Enter>', self.enter)
        self.buttonPullDown.bind('<Button>', self.anEntry.enter)

    def deActivateBindings(self):
        self.aFrame.unbind('<Right>')
        self.aFrame.unbind('<Down>')
        self.unbind('<Enter>')
        self.buttonPullDown.unbind('<Button>')

    def enable(self):
        self.anEntry.enable()
        self.aFrame.config(takefocus=1)
        self.buttonPullDown.config(state=NORMAL)
        self.activateBindings()
        self.anEntry.enableTrace()

    def disable(self):
        self.anEntry.disable()
        self.buttonPullDown.config(state=DISABLED)
        self.aFrame.config(takefocus=0)
        self.deActivateBindings()
        self.clearPopups()

    def addTrace(self, aTraceProc):
        self.anEntry.addTrace(aTraceProc)

    def clear(self):
        self.anEntry.config(state='normal')
        self.anEntry.delete(0, END)
        self.anEntry.config(state=DISABLED)

    def load(self, aValue):
        if aValue == None:
            aValue = ''
        self.anEntry.config(state='normal')
        self.anEntry.delete(0, END)
        self.anEntry.insert(0, aValue)
        self.anEntry.config(state=DISABLED)

    def updateList(self, aList):
        self.anEntry.updateList(aList)


class AppLBforCB(Entry):
    #
    #   Special List Box Entry
    #      1. present a listbox with choices
    #      2. can be configure to execute an add script if choice is not present in list
    #      3. It will force to select from list otherwise
    #      4. In order to open listbox at correct place, parent's parent must be provided
    #      5. Popup list removes unmatched items (anywhere, not case sensitive)
    #      
    #
    def __init__(self, parent, parentof, lista, fieldWidth, myMsgBar):
        self.fieldWidth = fieldWidth
        Entry.__init__(self, parent)
        self.parent=parent
        self.parentof=parentof
        self.myMsgBar = myMsgBar
        self.search = False
        self.traceProc = None
        self.trace = False
        
        self.config(takefocus = 0, font=fontBig, disabledforeground=AppDefaultForeground, width=self.fieldWidth,
                    justify=LEFT)
#        self.bind('<Enter>', self.enter)
#        self.bind('<Return>', self.enterKeyEntered)
#        self.bind('<FocusOut>', self.check)
        self.lista = lista        
        self.var = self["textvariable"]
        if self.var == '':
            self.var = self["textvariable"] = StringVar()
        self.var.trace("w", self.changed)
        self.config(state=DISABLED)
#        self.bind("<ButtonRelease-1>", self.selectItem)
#        self.bind("<Right>", self.enterKeyEntered)
#        self.bind("<Return>", self.selection)
#        self.bind("<Up>", self.up)
#        self.bind("<Down>", self.down)
#        self.bind("<MouseWheel>", self._on_mousewheel) 
#        self.bind("<Key>", self.cancel)             
        self.lb_up = False
        self.lbName = None
        self.enableEntry = False
        
    def addTrace(self, aTraceProc):
        self.trace = True
        self.traceProc = aTraceProc
        
    def enable(self):
        self.enableEntry = True
#        self.bind('<Enter>', self.enter)
        self.bind('<FocusOut>', self.check)
        self.bind("<ButtonRelease-1>", self.selectItem)
        self.bind("<Right>", self.enterKeyEntered)
        self.bind("<Return>", self.selection)
        self.bind("<Up>", self.up)
        self.bind("<Down>", self.down)
        self.bind("<MouseWheel>", self._on_mousewheel) 
        self.bind("<Key>", self.cancel)
        self.config(background=AppDefaultEditBackground, disabledbackground=AppDefaultEditBackground)             
        
    def disable(self):
        self.enableEntry = False
#        self.unbind('<Enter>')
        self.unbind('<FocusOut>')
        self.unbind("<ButtonRelease-1>")
        self.unbind("<Right>")
        self.unbind("<Return>")
        self.unbind("<Up>")
        self.unbind("<Down>")
        self.unbind("<MouseWheel>") 
        self.unbind("<Key>")             
        self.config(background=AppDefaultBackground, disabledbackground=AppDefaultBackground)
        
    def cancel(self, event):
        if event.char == chr(27):
            if self.lb_up:
                self.lb.destroy()
                self.lb_up = False
       
    def enterKeyEntered(self, *args):
        if not self.lb_up:
            self.lb = self.createLB()
            for w in self.lista:
                self.lb.insert(END,w)
            index = 0
            self.lb.selection_set(first=index)
            self.lb.activate(index)
        if len(self.var.get()) > 0:
            self.changed(None,None,None) 
                           
    def createLB(self):
        if len(self.lista)+1 > 9:
            height = 10
        else:
            height = len(self.lista)+1
        lb = Listbox(self.parentof, width=self.fieldWidth, font=fontBig, height=height, border=AppDefaultBorderWidth, takefocus=0)
        lb.place(in_=self.parentof, relx=0, rely=0, anchor=NW, x= self.parent.winfo_rootx()- self.parentof.winfo_rootx(),
                  y= self.parent.winfo_rooty() - self.parentof.winfo_rooty() + self.winfo_height() + AppHighlightBorderWidth)
        lb.bind("<ButtonRelease-1>", self.selectItem)
        lb.bind("<Double-Button-1>", self.selection)
        lb.bind("<Right>", self.selection)
        lb.bind("<Up>", self.up)
        lb.bind("<Down>", self.down)
        lb.bind("<MouseWheel>", self._on_mousewheel)       
        lb.insert(END,"")
        self.lbName = lb.winfo_name()
        self.lb_up = True
        return lb
    
    def _on_mousewheel(self, event):
        if event.delta > 0:
            self.lb.yview_scroll(-2, 'units')
        else:
            self.lb.yview_scroll(2, 'units')
                  
    def enter(self,event):
        if self.enableEntry:
            if len(self.get()) > 0:
                self.changed(None,None,None)
            else:
                self.enterKeyEntered(None)
            self.focus()

    def check(self, event):
        if self.lbName != None:
            self.myMsgBar.clearMessage()
            aName = event.widget.focus_lastfor()
            pattern = re.compile('.*' + self.lbName + '*.')
            if re.match(pattern, str(aName)) == None:
                if not self.findInList():
                    if len(self.get()) == 0:
                        aMsg = "INFO: No items selected."
                        self.myMsgBar.newMessage('info', aMsg)
                        
                if self.lb_up:
                    self.lb.destroy()
                    self.lb_up = False
                    
                if self.trace == True:
                    if self.traceProc != None:
                        self.traceProc(event)

    def changed(self, name, index, mode):
        if self.enableEntry:
            if self.var.get() == '':
                if self.lb_up:
                    self.lb.destroy()
                    self.lb_up = False
                    self.enterKeyEntered(None)
            else:
                words = self.comparison()
                if words:            
                    if not self.lb_up:
                        self.lb = self.createLB()
                    
                    self.lb.delete(0, END)
                    self.lb.insert(END,"")
                    for w in words:
                        self.lb.insert(END,w)
                    index = 0
                    self.lb.selection_set(first=index)
                    self.lb.activate(index)
                else:
                    if self.lb_up:
                        self.lb.destroy()
                        self.lb_up = False

    def selectItem(self, event):
        if self.lb_up:
            if self.lb.curselection() == ():
                index = '0'
            else:
                index = self.lb.curselection()[0]
            self.lb.selection_clear(first=index)
            self.lb.selection_set(first=index)
            self.lb.activate(index)
#        self.reDisplayList(index)
        self.focus()
               
    def selection(self, event):
        if self.lb_up:
            self.var.set(self.lb.get(ACTIVE))
            self.lb.destroy()
            self.lb_up = False
            self.icursor(END)            
            focusNext(self, self)
#            self.focus()

    def up(self, event):
        if self.lb_up:
            if self.lb.curselection() == ():
                index = '0'
            else:
                index = self.lb.curselection()[0]
            if index != '0':                
                self.lb.selection_clear(first=index)
                index = str(int(index)-1)                
                self.lb.selection_set(first=index)
                self.lb.activate(index)               
            self.lb.see(index)
            self.focus()

    def down(self, event):
        if self.lb_up:
            if self.lb.curselection() == ():
                index = '0'
            else:
                index = self.lb.curselection()[0]
            if index != END:                        
                self.lb.selection_clear(first=index)
                index = str(int(index)+1)        
                self.lb.selection_set(first=index)
                self.lb.activate(index) 
            self.lb.see(index)
            self.focus()

    def disableSearch(self):
        self.search = False
        
    def enableSearch(self):
        self.search = True

    def comparison(self):
#        pattern = re.escape('.*' + self.var.get() + '.*', re.IGNORECASE)
        if self.search == True:
            pattern1 = re.escape(self.var.get())
            pattern2 = re.compile('.*' + pattern1 + '.*', re.IGNORECASE)
            return [w for w in self.lista if re.match(pattern2, w)]
            self.focus()
        else:
            return self.lista
            self.focus()
            
    def updateList(self, aList):
        self.lista = aList

    def findInList(self, *args):
        try:
            self.lista.index(self.var.get())
            return True
        except:
            return False
        
#    def autowidth(self,maxwidth):
#        f = font.Font(font=self.cget("font"))
#        pixels = 0
#        for item in self.get(0, "end"):
#            pixels = max(pixels, f.measure(item))
        # bump listbox size until all entries fit
#        pixels = pixels + 10
#        width = int(self.cget("width"))
#        for w in range(0, maxwidth+1, 5):
#            if self.winfo_reqwidth() >= pixels:
#                break
#            self.config(width=width+w)

class AppUserDisplayLabel(Label):         
    def __init__(self, parent, userName):
        Label.__init__(self, parent)
#        self.aFunction = Label(self, text='Default view', font=fontAverageI, justify=RIGHT)
#        self.aFunction.grid(row=0, column=0, sticky=E+S)
#        self.rowconfigure(0, weight=0)
        self.userName = userName
        self.config(text=userName, font=fontAverageI, justify=RIGHT)
        
    def setUserName(self, aName):
        self.config(text=aName)

class AppFunctionDisplayLabel(Label):         
    def __init__(self, parent):
        Label.__init__(self, parent)
#        self.aFunction = Label(self, text='Default view', font=fontAverageI, justify=RIGHT)
#        self.aFunction.grid(row=0, column=0, sticky=E+S)
#        self.rowconfigure(0, weight=0)
        self.config(text='Default view', font=fontAverageI, justify=RIGHT)
        
    def setAddMode(self):
        self.config(text='Add mode', font=fontAverageI, justify=RIGHT, foreground='red')
            
    def setFindMode(self):
        self.config(text='Find mode', font=fontAverageI, justify=RIGHT, foreground='green')
            
    def setFindResultMode(self):
        self.config(text='Find Result View', font=fontAverageI, justify=RIGHT, foreground='green')
            
    def setEditMode(self):
        self.config(text='Edit mode', font=fontAverageI, justify=RIGHT, foreground='red')
        
    def setDuplicateMode(self):
        self.config(text='Duplicate Record mode', font=fontAverageI, justify=RIGHT, foreground='red')
            
    def setDefaultMode(self):
        self.config(text='Default view', font=fontAverageI, justify=RIGHT, foreground=AppDefaultForeground)


class AppViewSortFormat(Tix.Label):
    def __init__(self, parent):
        Tix.Label.__init__(self, parent)
        aText = " "
        self.config(text=aText, font=fontBigI)
        self.config(background=AppDefaultBackground)

    def load(self, aView, aSort):
        aText = "View: {0}, Sort: {1}".format(aView, aSort)
        self.config(text=aText, font=fontAverageI)


class CreateToolTipV2(object):
    '''
    create a tooltip for a given widget
    '''

    def __init__(self, widget, text='widget info'):
        self.widget = widget
        self.text = text
        self.widget.bind("<Button-2>", self.enter)
        self.widget.bind("<Leave>", self.close)

    def enter(self, event=None):
        x = y = 0
        x, y, cx, cy = self.widget.bbox("insert")
        x += self.widget.winfo_rootx() + 25
        y += self.widget.winfo_rooty() + 20
        # creates a toplevel window
        self.tw = Toplevel(self.widget)
        # Leaves only the label and removes the app window
        self.tw.wm_overrideredirect(True)
        self.tw.wm_geometry("+%d+%d" % (x, y))
        label = ttk.Label(self.tw, text=self.text, style='toolTip.TLabel')
        label.pack(ipadx=1)

    def close(self, event=None):
        try:
            if self.tw:
                self.tw.destroy()
        except:
            pass

class CreateToolTip(object):
    '''
    create a tooltip for a given widget
    '''
    def __init__(self, widget, text='widget info'):
        self.widget = widget
        self.text = text
        self.widget.bind("<Enter>", self.enter)
        self.widget.bind("<Leave>", self.close)
        
    def enter(self, event=None):
        x = y = 0
        x, y, cx, cy = self.widget.bbox("insert")
        x += self.widget.winfo_rootx() + 25
        y += self.widget.winfo_rooty() + 20
        # creates a toplevel window
        self.tw = Toplevel(self.widget)
        # Leaves only the label and removes the app window
        self.tw.wm_overrideredirect(True)
        self.tw.wm_geometry("+%d+%d" % (x, y))
        label = ttk.Label(self.tw, text=self.text, style='toolTip.TLabel')
        label.pack(ipadx=1)
        
    def close(self, event=None):
        if self.tw:
            self.tw.destroy()


class databaseCommands(Tix.Frame):
    def __init__(self, parent, access):
        Tix.Frame.__init__(self, parent)

        self.access = access

        databaseCommandsDisplay = Tix.Frame(self)
        databaseCommandsDisplay.grid(column=0, row=0)

        self.rowCount = 0
        self.columnCount = 0
        btnImage = resizeImage(Image.open(findRecImage), iconWidth, iconHeight)
        self.buttonFind = ttk.Button(databaseCommandsDisplay, image=btnImage, command=parent.findCommand,
                                     style='CommandButton.TButton', takefocus=0)
        self.buttonFind.grid(column=self.columnCount, row=0, sticky=W)
        self.buttonFind.img = btnImage
        self.buttonFind_ttp = CreateToolTip(self.buttonFind, "Find Record(s)")

        self.columnCount = self.columnCount + 1
        btnImage = resizeImage(Image.open(addCommandRecImage), iconWidth, iconHeight)
        self.addCommand = ttk.Button(databaseCommandsDisplay, image=btnImage, command=parent.addCommand,
                                     style='CommandButton.TButton', takefocus=0)
        self.addCommand.grid(column=self.columnCount, row=0, sticky=W)
        self.addCommand.img = btnImage
        self.addCommand_ttp = CreateToolTip(self.addCommand, "Add a New Record")

        self.columnCount = self.columnCount + 1
        btnImage = resizeImage(Image.open(editRecImage), iconWidth, iconHeight)
        self.editCommand = ttk.Button(databaseCommandsDisplay, image=btnImage, command=parent.editCommand,
                                      style='CommandButton.TButton', takefocus=0)
        self.editCommand.grid(column=self.columnCount, row=0, sticky=W)
        self.editCommand.img = btnImage
        self.editCommand_ttp = CreateToolTip(self.editCommand, "Edit Current Record")

        self.columnCount = self.columnCount + 1
        btnImage = resizeImage(Image.open(duplicateRecImage), iconWidth, iconHeight)
        self.duplicateCommand = ttk.Button(databaseCommandsDisplay, image=btnImage, command=parent.duplicateCommand,
                                           style='CommandButton.TButton', takefocus=0)
        self.duplicateCommand.grid(column=self.columnCount, row=0, sticky=W)
        self.duplicateCommand.img = btnImage
        self.duplicateCommand_ttp = CreateToolTip(self.duplicateCommand, "Create Duplicate Record")

        self.columnCount = self.columnCount + 1
        btnImage = resizeImage(Image.open(saveRecImage), iconWidth, iconHeight)
        self.saveCommand = ttk.Button(databaseCommandsDisplay, image=btnImage, command=parent.saveCommand,
                                      style='CommandButton.TButton', takefocus=0)
        self.saveCommand.grid(column=self.columnCount, row=0, sticky=W)
        self.saveCommand.img = btnImage
        self.saveCommand_ttp = CreateToolTip(self.saveCommand, "Save Current Record, or\nExecute Find Command")
        self.saveCommand.bind('<Return>', parent.saveCommand)
        self.saveCommand.config(state=DISABLED)

        self.columnCount = self.columnCount + 1
        btnImage = resizeImage(Image.open(deleteCommandRecImage), iconWidth, iconHeight)
        self.deleteButton = ttk.Button(databaseCommandsDisplay, image=btnImage,
                                       command=lambda tableName=parent.tableName: parent.deleteCommand(tableName),
                                       style='CommandButton.TButton', takefocus=0)
        self.deleteButton.grid(column=self.columnCount, row=0, sticky=W)
        self.deleteButton.img = btnImage
        self.deleteButton_ttp = CreateToolTip(self.deleteButton, "Delete Current Record")

        self.columnCount = self.columnCount + 1
        btnImage = resizeImage(Image.open(removeFromSetCommandImage), iconWidth, iconHeight)
        self.removeFromSetButton = ttk.Button(databaseCommandsDisplay, image=btnImage,
                                              command=parent.removeFromSetCommand, style='CommandButton.TButton',
                                              takefocus=0)
        self.removeFromSetButton.grid(column=self.columnCount, row=0, sticky=W)
        self.removeFromSetButton.img = btnImage
        self.removeFromSetButton_ttp = CreateToolTip(self.removeFromSetButton, "Remove Record From Current Set")

        self.columnCount = self.columnCount + 1
        btnImage = resizeImage(Image.open(cancelCommandRecImage), iconWidth, iconHeight)
        self.cancelButton = ttk.Button(databaseCommandsDisplay, image=btnImage, command=parent.cancelCommand,
                                       style='CommandButton.TButton', takefocus=0)
        self.cancelButton.grid(column=self.columnCount, row=0, sticky=W)
        self.cancelButton.img = btnImage
        self.cancelButton_ttp = CreateToolTip(self.cancelButton, "Cancel Current Command")

        self.columnCount = self.columnCount + 1
        btnImage = resizeImage(Image.open(refreshRecImage), iconWidth, iconHeight)
        self.buttonRefresh = ttk.Button(databaseCommandsDisplay, image=btnImage, command=parent.refreshWindow,
                                        style='CommandButton.TButton', takefocus=0)
        self.buttonRefresh.grid(column=self.columnCount, row=0, sticky=W)
        self.buttonRefresh.img = btnImage
        self.buttonRefresh_ttp = CreateToolTip(self.buttonRefresh, "Reload Default View\nRefreshes DB Records")

        self.columnCount = self.columnCount + 1
        btnImage = resizeImage(Image.open(undeleteCommandImage), iconWidth, iconHeight)
        self.buttonUndelete = ttk.Button(databaseCommandsDisplay, image=btnImage,
                                         command=lambda tableName=parent.tableName: parent.undeleteCommand(tableName),
                                         style='CommandButton.TButton', takefocus=0)
        self.buttonUndelete.grid(column=self.columnCount, row=0, sticky=W)
        self.buttonUndelete.img = btnImage
        self.buttonUndelete_ttp = CreateToolTip(self.buttonUndelete, "Undelete DB Records")

    def _setAccessRestriction(self):
        if self.access == 'View':
            self.duplicateCommand.config(state=DISABLED)
            self.addCommand.config(state=DISABLED)
            self.editCommand.config(state=DISABLED)
            self.deleteButton.config(state=DISABLED)
            self.buttonUndelete.config(state=DISABLED)

    def emptyDBState(self):
        self.duplicateCommand.config(state=DISABLED)
        self.buttonFind.config(state=NORMAL)
        self.addCommand.config(state=NORMAL)
        self.editCommand.config(state=DISABLED)
        self.saveCommand.config(state=DISABLED)
        self.deleteButton.config(state=DISABLED)
        self.removeFromSetButton.config(state=DISABLED)
        self.cancelButton.config(state=NORMAL)
        self.buttonRefresh.config(state=NORMAL)
        self.buttonUndelete.config(state=DISABLED)
        self._setAccessRestriction()

    def findState(self):
        self.focus()
        self.duplicateCommand.config(state=DISABLED)
        self.buttonFind.config(state=NORMAL)
        self.addCommand.config(state=DISABLED)
        self.editCommand.config(state=DISABLED)
        self.saveCommand.config(state=NORMAL)
        self.deleteButton.config(state=DISABLED)
        self.removeFromSetButton.config(state=DISABLED)
        self.cancelButton.config(state=NORMAL)
        self.buttonRefresh.config(state=DISABLED)
        self.buttonUndelete.config(state=DISABLED)
        self._setAccessRestriction()

    def addState(self):
        self.focus()
        self.duplicateCommand.config(state=DISABLED)
        self.buttonFind.config(state=DISABLED)
        self.addCommand.config(state=DISABLED)
        self.editCommand.config(state=DISABLED)
        self.saveCommand.config(state=NORMAL)
        self.deleteButton.config(state=DISABLED)
        self.removeFromSetButton.config(state=DISABLED)
        self.cancelButton.config(state=NORMAL)
        self.buttonRefresh.config(state=DISABLED)
        self.buttonUndelete.config(state=DISABLED)
        self._setAccessRestriction()

    def editState(self):
        self.focus()
        self.duplicateCommand.config(state=DISABLED)
        self.buttonFind.config(state=DISABLED)
        self.addCommand.config(state=DISABLED)
        self.editCommand.config(state=DISABLED)
        self.saveCommand.config(state=NORMAL)
        self.deleteButton.config(state=DISABLED)
        self.removeFromSetButton.config(state=DISABLED)
        self.cancelButton.config(state=NORMAL)
        self.buttonRefresh.config(state=DISABLED)
        self.buttonUndelete.config(state=DISABLED)
        self._setAccessRestriction()

    def deleteState(self):
        self.focus()
        self.duplicateCommand.config(state=DISABLED)
        self.buttonFind.config(state=NORMAL)
        self.addCommand.config(state=DISABLED)
        self.editCommand.config(state=DISABLED)
        self.saveCommand.config(state=DISABLED)
        self.deleteButton.config(state=DISABLED)
        self.removeFromSetButton.config(state=NORMAL)
        self.cancelButton.config(state=DISABLED)
        self.buttonRefresh.config(state=NORMAL)
        self.buttonUndelete.config(state=NORMAL)
        self._setAccessRestriction()

    def defaultState(self):
        self.focus()
        self.duplicateCommand.config(state=NORMAL)
        self.buttonFind.config(state=NORMAL)
        self.addCommand.config(state=NORMAL)
        self.editCommand.config(state=NORMAL)
        self.saveCommand.config(state=DISABLED)
        self.deleteButton.config(state=NORMAL)
        self.removeFromSetButton.config(state=NORMAL)
        self.cancelButton.config(state=NORMAL)
        self.buttonRefresh.config(state=NORMAL)
        self.buttonUndelete.config(state=DISABLED)
        self._setAccessRestriction()

    def cancelState(self):
        self.focus()
        self.duplicateCommand.config(state=NORMAL)
        self.buttonFind.config(state=NORMAL)
        self.addCommand.config(state=NORMAL)
        self.editCommand.config(state=NORMAL)
        self.saveCommand.config(state=DISABLED)
        self.deleteButton.config(state=NORMAL)
        self.removeFromSetButton.config(state=NORMAL)
        self.cancelButton.config(state=NORMAL)
        self.buttonRefresh.config(state=NORMAL)
        self.buttonUndelete.config(state=DISABLED)
        self._setAccessRestriction()

    def saveState(self):
        self.focus()
        self.duplicateCommand.config(state=NORMAL)
        self.buttonFind.config(state=NORMAL)
        self.addCommand.config(state=NORMAL)
        self.editCommand.config(state=NORMAL)
        self.saveCommand.config(state=DISABLED)
        self.deleteButton.config(state=NORMAL)
        self.removeFromSetButton.config(state=NORMAL)
        self.cancelButton.config(state=NORMAL)
        self.buttonRefresh.config(state=NORMAL)
        self.buttonUndelete.config(state=DISABLED)
        self._setAccessRestriction()

    def refreshState(self):
        self.focus()
        self.duplicateCommand.config(state=NORMAL)
        self.buttonFind.config(state=NORMAL)
        self.addCommand.config(state=NORMAL)
        self.editCommand.config(state=NORMAL)
        self.saveCommand.config(state=DISABLED)
        self.deleteButton.config(state=NORMAL)
        self.removeFromSetButton.config(state=NORMAL)
        self.cancelButton.config(state=NORMAL)
        self.buttonRefresh.config(state=NORMAL)
        self.buttonUndelete.config(state=DISABLED)
        self._setAccessRestriction()


#        self.buttonAdd = tk.Button(databaseCommandsDisplay, text='Add',  font=buttonFont,
#                                    fg=MyGolfDefaultFGColor,command= lambda: parent.addRecord()) 
#        self.buttonAdd.grid(column=1, row=0, sticky=W)
#        self.buttonAdd.pack(side=LEFT)       
#        new_order = (self.buttonFind, self.buttonAdd, self.buttonEdit, self.buttonDelete, self.buttonSave, self.buttonCancel, self.buttonRefresh)
#        for widget in new_order:
#            widget.lift()

#        courseCommandsDisplay.pack(side = TOP)

#    def disableDatabaseCommands(self, op):
#        if (op == 'add') or (op == 'edit') or (op== 'find'):
#            self.buttonFind.config(state=DISABLED)
#            self.buttonAdd.config(state=DISABLED)
#            self.buttonEdit.config(state=DISABLED)
#            self.buttonDelete.config(state=DISABLED)
#            self.buttonSave.config(state=NORMAL)
#            self.buttonCancel.config(state=NORMAL)
#        elif op=='del':
#            self.buttonFind.config(state=DISABLED)
#            self.buttonAdd.config(state=DISABLED)
#            self.buttonEdit.config(state=DISABLED)
#            self.buttonDelete.config(state=DISABLED)
#            self.buttonSave.config(state=DISABLED)
#            self.buttonCancel.config(state=DISABLED)

#    def enableDatabaseCommands(self, op):
#        if op == 'default':
#            self.buttonFind.config(state=NORMAL)
#            self.buttonAdd.config(state=NORMAL)
#            self.buttonEdit.config(state=NORMAL)
#            self.buttonDelete.config(state=NORMAL)
#            self.buttonSave.config(state=NORMAL)
#            self.buttonCancel.config(state=NORMAL)

class navDatabaseTool(Frame):
    def __init__(self, parent):
        self.parent=parent
        Frame.__init__(self, parent)
        navDisplay=Frame(self)
        
        self.recNumDisplay = Entry(navDisplay, relief='flat', width=recordNumWidth, disabledforeground=AppDefaultForeground,
                                   background=AppDefaultBackground, disabledbackground=AppDefaultBackground)
        if len(parent.recList) > 0:
            self.myString = '''  Record {0} of {1}'''.format(parent.curRecNumV.get()+1, len(parent.recList))
        else:
            self.myString = '''  Record {0} of {1}'''.format(parent.curRecNumV.get(), len(parent.recList))
        self.recNumDisplay.insert(0, self.myString)
        self.recNumDisplay.pack(side=RIGHT)
        self.recNumDisplay.config(state=DISABLED)


#        b = tk.Button(self.middleButton, text="Add Addr", font=fontAverage, fg=MyGolfDefaultFGColor,
#                      command=self._startAddrWindow)
#        b.grid(row=0, column=1,  sticky=N+S)        
        nxtRecImage = resizeImage(Image.open(nextRecImage),iconWidth,iconHeight)
        prvRecImage = resizeImage(Image.open(previousRecImage),iconWidth,iconHeight)
        lstRecImage = resizeImage(Image.open(lastRecImage),iconWidth,iconHeight)
        fstRecImage = resizeImage(Image.open(firstRecImage),iconWidth,iconHeight)
                     
        self.buttonLast = ttk.Button(navDisplay, image=lstRecImage, command  = parent.lastRecord, style='CommandButton.TButton', takefocus=0)
        self.buttonLast.img = lstRecImage
        self.buttonLast.pack(side=RIGHT)
        self.buttonLast_ttp = CreateToolTip(self.buttonLast, "Display Last Record")
        
        self.buttonNext = ttk.Button(navDisplay, image=nxtRecImage, command  = parent.nextRecord, style='CommandButton.TButton', takefocus=0)
        self.buttonNext.img = nxtRecImage
        self.buttonNext.pack(side=RIGHT)  
        self.buttonNext_ttp = CreateToolTip(self.buttonNext, "Display Next Record")
        
        self.buttonPrevious = ttk.Button(navDisplay, image=prvRecImage, command  = parent.prvRecord, style='CommandButton.TButton', takefocus=0)
        self.buttonPrevious.img = prvRecImage
        self.buttonPrevious.pack(side=RIGHT)
        self.buttonPrevious_ttp = CreateToolTip(self.buttonPrevious, "Display Previous Record")
        
        self.buttonFirst = ttk.Button(navDisplay, image=fstRecImage, command  = parent.firstRecord, style='CommandButton.TButton', takefocus=0)
        self.buttonFirst.img = fstRecImage
        self.buttonFirst_ttp = CreateToolTip(self.buttonFirst, "Display First Record")
        self.buttonFirst.pack(side=RIGHT)        

        navDisplay.pack()
        
    def updateRecordNumber(self, *args):
        self.recNumDisplay.config(state='normal')
        self.recNumDisplay.delete(0, END)
        if len(self.parent.recList) > 0:
            self.myString = '''  Record {0} of {1}'''.format(self.parent.curRecNumV.get()+1, len(self.parent.recList))
        else:  
            self.myString = '''  Record {0} of {1}'''.format(self.parent.curRecNumV.get(), len(self.parent.recList))
        self.recNumDisplay.insert(0, self.myString)
        self.recNumDisplay.pack(side=RIGHT)
        self.recNumDisplay.config(state=DISABLED)
                      
    def enableNavigation(self):
        self.buttonFirst.config(state=NORMAL)
        self.buttonLast.config(state=NORMAL)
        self.buttonNext.config(state=NORMAL)
        self.buttonPrevious.config(state=NORMAL)

    def disableNavigation(self):
        self.buttonFirst.config(state=DISABLED)
        self.buttonLast.config(state=DISABLED)
        self.buttonNext.config(state=DISABLED)
        self.buttonPrevious.config(state=DISABLED)


class tableCommonWindowClass(Tix.Frame):
    def __init__(self, parent, aCloseCommand, windowTitle, loginData):
        Tix.Frame.__init__(self, parent)
        self.accessLevel = loginData[1]
        self.loginData = loginData
        self.CloseMe = aCloseCommand
        self.parent = parent
        self.curRecNumV = Tix.IntVar()
        self.curRecNumV.set(0)
        self.curRecBeforeOp = self.curRecNumV.get()
        self.prevFind = False
        self.curFind = False
        self.curOp = 'default'
        self.recList = []
        self.accessLevel = ''
        self.updaterID = 0
        self.mainAreaRowCount = 0
        self.mainAreaColumnCount = 0
        self.findValues = []
        self.viewSet = Tix.StringVar()
        self.viewSet.set("Active")  # Active, All, or Closed
        self.sortFieldID = 0  # Last Name
        self.sortID = Tix.StringVar()
        self.sortID.set('Default')  # Department, Workgroup, Last Name
        self.deletedV = Tix.StringVar()
        self.deletedV.trace('w', self._setAsDeleted)
        ############################################################ Window Common Configuration
        self.dirty = False
        mainWInWidth = 400
        mainWinHeight = 400
        self.configure(width=mainWInWidth, height=mainWinHeight)
        self.columnCountTotal = 7
        for i in range(self.columnCountTotal):
            self.columnconfigure(i, weight=1)

        ############################################################ Window Title Area
        self.rowCount = 0
        self.columnCount = 0
        wMsg = Tix.Label(self, text=windowTitle, font=fontMonstrousB)
        wMsg.grid(row=self.rowCount, column=self.columnCount, columnspan=self.columnCountTotal, sticky=E + W)
        self.rowconfigure(self.rowCount, weight=0)

        self.displayCurFunction = AppFunctionDisplayLabel(self)
        self.displayCurFunction.grid(row=self.rowCount, column=self.columnCount, columnspan=self.columnCountTotal,
                                     sticky=E + S)

        self.displayCurUser = AppUserDisplayLabel(self, " ")
        self.displayCurUser.grid(row=self.rowCount, column=self.columnCount, columnspan=self.columnCountTotal,
                                 sticky=W + S)

        ############################################################ Main Frame Area
        self.rowCount = self.rowCount + 1
        self.columnCount = 0
        self.mainArea = Tix.Frame(self, border=3, relief='groove', takefocus=0)
        self.mainArea.grid(row=self.rowCount, column=self.columnCount, columnspan=self.columnCountTotal,
                           sticky=N + S + E + W)
        self.mainAreaColumnTotal = 21
        for i in range(self.mainAreaColumnTotal):
            self.mainArea.columnconfigure(i, weight=1)
        self.rowconfigure(self.rowCount, weight=1)

        ############################################################ Command Frame Area
        self.rowCount = self.rowCount + 1
        self.columnCount = 0

        self.myDatabaseBar = databaseCommands(self, self.accessLevel)
        self.myDatabaseBar.grid(row=self.rowCount, column=self.columnCount, sticky=N + W + S)

        #  This frame is used for commands specific to current class
        self.columnCount = self.columnCount + 1
        self.commandFrame = Tix.Frame(self)
        self.commandFrame.grid(row=self.rowCount, column=self.columnCount, columnspan=self.columnCountTotal - 2,
                               sticky=N + S)
        self.viewSortUpdate = AppViewSortFormat(self.commandFrame)
        self.viewSortUpdate.grid(row=0, column=0, sticky=E + N + S)
        self.viewSet.trace('w', self.updateViewSortDisplay)
        self.sortID.trace('w', self.updateViewSortDisplay)

        self.myNavBar = navDatabaseTool(self)
        self.myNavBar.grid(row=self.rowCount, column=self.columnCountTotal - 1, sticky=N + E + S)
        self.curRecNumV.trace('w', self.myNavBar.updateRecordNumber)

        self.rowconfigure(self.rowCount, weight=0)

        ############################################################ Message Bar Area
        self.rowCount = self.rowCount + 1
        self.columnCount = 0
        self.myMsgBar = messageBar(self)
        self.myMsgBar.grid(row=self.rowCount, column=self.columnCount, columnspan=self.columnCountTotal, sticky=W + S,
                           padx=5)
        self.rowconfigure(self.rowCount, weight=0)

        ########################################################### Display records
        self.recordFrame = Tix.Frame(self.mainArea, takefocus=0)
        self.recordFrame.grid(row=0, column=0, columnspan=self.mainAreaColumnTotal, sticky=N + S + E + W)


        wMsg = Tix.Label(self.recordFrame, text='No Records Found', font=fontMonstrousB)
        wMsg.grid(row=0, column=0)

        ############################################################ Initial Navigation State
        if len(self.recList) == 0:
            self.navigationDisable('')

        ############################################################ Common Window Binding
        self.parent.bind('<End>', self.endKeyPressed)
        self.createMenu()

    #        self.loginWindow.bind('<Home>', self.focusLoginName)

    ############################################################ Table Specific Commands
    #
    #   These commands will be redefined for each table that invokes this class
    #   Manipulation of data is specific to each tables.
    #
    def _setAsDeleted(self, *args):
        #
        #  At a minimun, record action are disabled.
        #  Additionnal commands can be added, see person class
        #  for override example for this method.
        #
        if self.recList[self.curRecNumV.get()][self.numberOfFields - 3] == 'Y':
            self.myDatabaseBar.deleteState()
            self.recordFrame.config(highlightcolor=AppDeletedHighlightColor, takefocus=0)
            self.recordFrame.focus()
        else:
            self.myDatabaseBar.defaultState()
            self.recordFrame.config(highlightcolor=AppDefaultForeground, takefocus=0)
            self.recordFrame.focus()

    def updateViewSortDisplay(self, *args):
        self.viewSortUpdate.load(self.viewSet.get(), self.sortID.get())

    def createMenu(self):
        self.menubar = Menu(self.parent, foreground=AppDefaultForeground, font=AppDefaultMenuFont)
        self.filemenu = Menu(self.menubar, foreground=AppDefaultForeground, font=AppDefaultMenuFont, tearoff=0)
        #        filemenu.add_command(label="Close", command=self.donothing)
        self.filemenu.add_separator()
        self.filemenu.add_command(label="Exit", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                  command=self.CloseMe)
        self.menubar.add_cascade(label="File", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                 menu=self.filemenu)

        self.gamesmenu = Menu(self.menubar, foreground=AppDefaultForeground, font=AppDefaultMenuFont, tearoff=0)
        self.gamesmenu.add_command
        self.gamesmenu.add_command(label="Add a Game", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                   command= self.displayAddGameWindow)
        self.menubar.add_cascade(label="Games", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                 menu=self.gamesmenu)


        self.expensesmenu = Menu(self.menubar, foreground=AppDefaultForeground, font=AppDefaultMenuFont, tearoff=0)
        self.expensesmenu.add_command
        self.expensesmenu.add_command(label="Add Expenses", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                   command= self.displayAddExpensesWindow)
        self.menubar.add_cascade(label="Expenses", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                 menu=self.expensesmenu)

        '''
        self.viewsmenu = Menu(self.menubar, foreground=AppDefaultForeground, font=AppDefaultMenuFont, tearoff=0)
        self.viewsmenu.add_command(label="All", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                   command=lambda viewID='All': self.basicViewsList(viewID))
        self.viewsmenu.add_command(label="Active", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                   command=lambda viewID='Active': self.basicViewsList(viewID))
        self.viewsmenu.add_command(label="Closed", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                   command=lambda viewID='Closed': self.basicViewsList(viewID))
        self.viewsmenu.add_separator()
        self.menubar.add_cascade(label="Views", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                 menu=self.viewsmenu)
        '''
        #        self.viewsPersonnelmenu = Menu(self.menubar, foreground=AppDefaultForeground, font=AppDefaultMenuFont, tearoff=0)
        #        self.menubar.add_cascade(label="More Views", foreground=AppDefaultForeground, font=AppDefaultMenuFont, menu=self.viewsPersonnelmenu)
        self.tableSpecificMenu()
        '''
        self.setCommandmenu = Menu(self.menubar, tearoff=0)
        self.setCommandmenu.add_command(label="Delete Set", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                        command=lambda tableName=self.tableName: self.deleteSetCommand(tableName))
        self.setCommandmenu.add_command(label="Undelete Set", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                        command=lambda tableName=self.tableName: self.unDeleteSetCommand(tableName))
        self.setCommandmenu.add_separator()
        self.menubar.add_cascade(label="Set Operations", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                 menu=self.setCommandmenu)
        '''

        self.helpmenu = Menu(self.menubar, tearoff=0)
        self.helpmenu.add_command(label="About...", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                  command=lambda: self.displayAbout())
        self.menubar.add_cascade(label="Help", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                 menu=self.helpmenu)

        self.parent.config(menu=self.menubar)

    def displayAbout(self):
        pass  # Was creating a loop, must be declared in the table specific

    def displayAddGameWindow(self):
        pass
        self.AddEditGamesWindow = Tix.Toplevel()
        self.AddEditGamesWindow.transient(self)

        myGeo = self.winfo_geometry()
        myNewGeo = '+{0}+{1}'.format((int(myGeo.split("+")[1]) + 40), (int(myGeo.split("+")[2]) + 40))
        self.AddEditGamesWindow.geometry(myNewGeo)
        # golferData should replace the 3
        addEditGamesPopUp(self.AddEditGamesWindow, self.OnAddEditGamesWindowClose, 'add', None, None, self.loginData).pack(fill=BOTH, expand=1)

        self.wait_window(self.AddEditGamesWindow) # addEditExpenses has to terminate for current window to resume

    def OnAddEditGamesWindowClose(self):
        self.AddEditGamesWindow.destroy()

    def displayAddExpensesWindow(self):
        pass
        self.AddEditExpensesWindow = Tix.Toplevel()
        self.AddEditExpensesWindow.transient(self)

        myGeo = self.winfo_geometry()
        myNewGeo = '+{0}+{1}'.format((int(myGeo.split("+")[1]) + 40), (int(myGeo.split("+")[2]) + 40))
        self.AddEditExpensesWindow.geometry(myNewGeo)
        # golferData should replace the 3
        addEditExpensePopUp(self.AddEditExpensesWindow, self.OnAddEditExpensesWindowClose, 'add', None, None, self.loginData).pack(fill=BOTH, expand=1)

        # aWindow, aCloseCommand, mode, expenseID, updateExpenseList, loginData
        self.wait_window(self.AddEditExpensesWindow) # addEditExpenses has to terminate for current window to resume

    def OnAddEditExpensesWindowClose(self):
        self.AddEditExpensesWindow.destroy()

    def tableSpecificMenu(self):
        pass

    def sortingList(self, sortID):
        self.sortID.set(sortID)  # set the type
        self.sort()  # execute the sort

    def sort(self):
        pass

    def getTableData(self):
        pass  # Table specific

    def _getNewRecList(self):
        self.getTableData()
        self.curRecNumV.set(self.curRecNumV.get())

        if len(self.recAll) > 0:
            self.numberOfFields = len(self.recAll[0])
        else:
            self.numberOfFields = 0
        self.recList = self.recAll
        self.curRecNumV.set(self.curRecNumV.get())  # forces display of record number

    def _reDisplayRecordFrame(self):
        self.recordFrame.destroy()
        self._getNewRecList()
        self._displayRecordFrame()

    def _buildWindowsFields(self, aFrame):
        pass

    def _displayRecordFrame(self):
        self.recordFrame = AppFrame(self.mainArea, 1)
        self.recordFrame.grid(row=self.mainAreaRowCount, column=self.mainAreaColumnCount,
                              columnspan=self.mainAreaColumnTotal, sticky=N + S + E + W)
        self.recordFrame.addAccentBackground()
        self.recordFrame.addDeletedHighlight()
        self.recordFrame.noBorder()
        self.recordFrame.stretchCurrentRow()

        if len(self.recList) > 0:
            self._buildWindowsFields(self.recordFrame)
            self.recordFrame.rowconfigure(self.recordFrame.row, weight=1)
            self.navigationEnable(self.accessLevel)
#            self.basicViewsList('Active')
            self.displayRec()

        elif self.curOp == 'add':
            self._buildWindowsFields(self.recordFrame)
            self.recordFrame.rowconfigure(self.recordFrame.row, weight=1)

        else:
            wMsg = Tix.Label(self.recordFrame, text='No Records Found in Current List.', font=fontMonstrousB)
            wMsg.grid(row=0, column=0, columnspan=self.recordFrame.columnTotal)
#
#   MUST BE REVIEWED
#
    def basicViewsList(self, viewID):
        if self.curOp == 'add' or self.curOp == 'edit':
            aMsg = "ERROR: Invalid request. Compete add/edit operation before changing views."
            self.myMsgBar.newMessage('error', aMsg)
        else:
            pass
            '''
            self.setViewDefault()
            if len(self.recList) == 0:
                self._buildWindowsFields(self.recordFrame)

            self.deletedFieldIdx = len(self.recAll[0]) - 3
            if viewID == 'All':
                self.viewSet.set("All")
                temp = []
                for line in range(len(self.recAll)):
                    temp.append(self.recAll[line])
                self.recList = temp
                self.curRecNumV.set(0)  # reset the record being viewed to 0
            elif viewID == 'Active':
                temp = []
                for line in range(len(self.recAll)):
                    if self.recAll[line][self.deletedFieldIdx] == 'N':  # is the item active (not deleted)
                        temp.append(self.recAll[line])
                if len(temp) > 0:
                    self.viewSet.set('Active')
                    self.recList = temp
                    self.curRecNumV.set(0)  # reset the record being viewed to 0
                else:
                    aMsg = "WARNING: There are no closed/deleted records in this table."
                    self.myMsgBar.newMessage('warning', aMsg)
            elif viewID == 'Closed':
                temp = []
                for line in range(len(self.recAll)):
                    if self.recAll[line][self.deletedFieldIdx] == 'Y':  # is the item closed (deleted)
                        temp.append(self.recAll[line])
                if len(temp) > 0:
                    self.viewSet.set('Closed')
                    self.recList = temp
                    self.curRecNumV.set(0)  # reset the record being viewed to 0
                else:
                    aMsg = "WARNING: There are no closed/deleted records in this table."
                    self.myMsgBar.newMessage('warning', aMsg)

            elif viewID == 'Active Golfers':
                temp = []
                for line in range(len(self.recAll)):
                    if self.recAll[line][23] == DefaultDepartmentID and self.recAll[line][
                        self.deletedFieldIdx] == 'N':  # is the item closed (deleted)
                        temp.append(self.recAll[line])
                if len(temp) > 0:
                    self.viewSet.set('Active ECE')
                    self.recList = temp
                    self.curRecNumV.set(0)  # reset the record being viewed to 0
                else:
                    aMsg = "WARNING: There are no closed/deleted records in this table."
                    self.myMsgBar.newMessage('warning', aMsg)

            elif viewID == 'Active Non-ECE':
                temp = []
                for line in range(len(self.recAll)):
                    if self.recAll[line][23] != DefaultDepartmentID and self.recAll[line][
                        self.deletedFieldIdx] == 'N':  # is the item closed (deleted)
                        temp.append(self.recAll[line])
                if len(temp) > 0:
                    self.viewSet.set('Active Non-ECE')
                    self.recList = temp
                    self.curRecNumV.set(0)  # reset the record being viewed to 0
                else:
                    aMsg = "WARNING: There are no closed/deleted records in this table."
                    self.myMsgBar.newMessage('warning', aMsg)
            elif viewID == 'Active No Department':
                temp = []
                for line in range(len(self.recAll)):
                    if self.recAll[line][23] == 1 and self.recAll[line][
                        self.deletedFieldIdx] == 'N':  # is the item closed (deleted)
                        temp.append(self.recAll[line])
                if len(temp) > 0:
                    self.viewSet.set('Active No Department')
                    self.recList = temp
                    self.curRecNumV.set(0)  # reset the record being viewed to 0
                else:
                    aMsg = "WARNING: There are no closed/deleted records in this table."
                    self.myMsgBar.newMessage('warning', aMsg)

            self.navigationEnable(self.accessLevel)
            self.myNavBar.enableNavigation()
            self.displayRec()  # display current record
            '''

    def endKeyPressed(self, Event):
        self.myDatabaseBar.saveCommand.focus()

    def nextRecord(self, *args):
        self.myMsgBar.clearMessage()
        if len(self.recList) > 0:
            if self.curRecNumV.get() < (len(self.recList) - 1):  # Don't go past last record
                self.curRecNumV.set(self.curRecNumV.get() + 1)
                self.displayRec()

    def prvRecord(self, *args):
        self.myMsgBar.clearMessage()
        if len(self.recList) > 0:
            if self.curRecNumV.get() > 0:  # Don't go past first record
                self.curRecNumV.set(self.curRecNumV.get() - 1)
                self.displayRec()

    def lastRecord(self, *args):
        self.myMsgBar.clearMessage()
        if len(self.recList) > 0:
            self.curRecNumV.set((len(self.recList) - 1))
            self.displayRec()

    def firstRecord(self, *args):
        self.myMsgBar.clearMessage()
        if len(self.recList) > 0:
            self.curRecNumV.set(0)
            self.displayRec()

    def displayRec(self, *args):
        # Table Specific
        pass

    def refresh(self, *args):
        self.clearPopups()
        self._reDisplayRecordFrame()

    def resetCurOp(self):
        if self.curFind == True:
            self.curOp = 'find'
            self.displayCurFunction.setFindResultMode()
        else:
            self.curOp = 'default'
            self.displayCurFunction.setDefaultMode()

    def setViewDefault(self):
        self.myMsgBar.clearMessage()
        self.myDatabaseBar.refreshState()
        self.prevFind = False
        self.curFind = False
        self.curOp = 'default'
        self.displayCurFunction.setDefaultMode()
        self.myNavBar.updateRecordNumber()
        self.fieldsDisable()
        self.displayRec()

    def windowViewDefault(self):
        self.basicViewsList('Active')
        self.sortingList('Default')

    def refreshWindow(self, *args):
        #
        #  Refresh all DB records
        #  (Sync DB with main ECE Data)
        #
        self.myMsgBar.clearMessage()
        self.myDatabaseBar.refreshState()
        self.refresh()  # reloads from Database
        self.prevFind = False
        self.curFind = False
        self.findValues = []
        self.curOp = 'default'
        self.displayCurFunction.setDefaultMode()
        self.windowViewDefault()
        self.myNavBar.updateRecordNumber()
        self.fieldsDisable()
        self.displayRec()

    def findCommand(self, *args):
        self.myMsgBar.clearMessage()
        self.myDatabaseBar.findState()
        self.myNavBar.disableNavigation()
        self.prevFind = self.curFind
        self.curOp = 'find'
        self.displayCurFunction.setFindMode()
        self.find()

    def editCommand(self, *args):
        self.myMsgBar.clearMessage()
        self.myDatabaseBar.editState()
        self.myNavBar.disableNavigation()
        self.curRecBeforeOp = self.curRecNumV.get()
        self.curOp = 'edit'
        self.displayCurFunction.setEditMode()
        self.edit()

    def duplicateCommand(self, *args):
        self.myMsgBar.clearMessage()
        self.myDatabaseBar.addState()
        self.myNavBar.disableNavigation()
        self.curRecBeforeOp = self.curRecNumV.get()
        self.curOp = 'add'
        self.displayCurFunction.setDuplicateMode()
        self.clearPopups()
        self.fieldsEnable()
        self.duplicate()

    def duplicate(self):
        # Table specific.  Will be defined in calling class
        pass

    def edit(self):
        # Table specific.  Will be defined in calling class
        pass

    def find(self):
        # Table specific.  Will be defined in calling class
        pass

    def addCommand(self, *args):
        self.myMsgBar.clearMessage()
        self.myDatabaseBar.addState()
        self.myNavBar.disableNavigation()
        self.curOp = 'add'
        self.curRecBeforeOp = self.curRecNumV.get()
        self.displayCurFunction.setAddMode()
        if len(self.recList) == 0:
            self._displayRecordFrame()
        self.add()

    def add(self):
        # Table specific.  Will be defined in calling class
        pass

    def delete(self, tableName):
        self._disableWindowCommand()
        aMsg = "Are you sure you want to delete the current record?"
        ans = AppQuestionRequest(self, "Delete Record - {0}".format(tableName), aMsg)
        if ans.result == True:
            pass
            '''
            AppDB.delete(tableName, self.recList[self.curRecNumV.get()][0], convertDateStringToOrdinal(todayDate),
                         self.updaterID)  # remove from DB
            aMsg = "Current Record from table {0} has been deleted.".format(tableName)
            self.myMsgBar.newMessage('info', aMsg)
            idxRecListAll = getIndex(self.recList[self.curRecNumV.get()][0], self.recAll)
            itemToUpdateDeletedField = self.recAll[idxRecListAll]
            itemWithDeletedFieldUpdated = ()
            for i in range(len(itemToUpdateDeletedField)):
                if i == self.numberOfFields - 3:
                    itemWithDeletedFieldUpdated += ('Y',)
                else:
                    itemWithDeletedFieldUpdated += (self.recAll[idxRecListAll][i],)
            self.recAll.pop(idxRecListAll)  # remove the old one
            self.recAll.insert(idxRecListAll, itemWithDeletedFieldUpdated)  # add the new one

            if self.viewSet.get() != 'Closed':
                if self.viewSet.get() == 'All':
                    self.recList.pop(self.curRecNumV.get())  # remove from current list (only if active view)
                    self.recList.insert(self.curRecNumV.get(), itemWithDeletedFieldUpdated)
                else:
                    self.recList.pop(self.curRecNumV.get())  # remove from current list (only if active view)

            if self.curRecBeforeOp == 0 or self.viewSet.get() == 'All':
                self.curRecNumV.set(self.curRecBeforeOp)
            else:
                self.curRecNumV.set(self.curRecBeforeOp - 1)
            '''
        self._enableWindowCommand()
        self.myDatabaseBar.defaultState()
        self.navigationEnable(self.accessLevel)
        self.resetCurOp()

    def saveCommand(self, *args):
        self.myMsgBar.clearMessage()
        if self.save() == True:
            self.myDatabaseBar.saveState()
            if len(self.recList) == 0:
                self.navigationDisable(self.accessLevel)
            else:
                self.myNavBar.enableNavigation()
            self.curOp = 'default'
            self.displayRec()

    def save(self):
        # Table specific.  Will be defined in calling class
        pass

    def navEmptyList(self):
        self.navigationDisable(self.accessLevel)

    def clearPopups(self):
        # Table specific.  Will be defined in calling class
        pass

    def undeleteCommand(self, tableName, *args):
        self.curRecBeforeOp = self.curRecNumV.get()
        self._disableWindowCommand()
        aMsg = "Are you sure you want to undelete the current record?"
        ans = AppQuestionRequest(self, "Undelete Record - {0}".format(tableName), aMsg)

        if ans.result == True:
            pass
            '''
            AppDB.unDelete(tableName, self.recList[self.curRecNumV.get()][0], convertDateStringToOrdinal(todayDate),
                           self.updaterID)
            aMsg = "Record has been added to the Active {0} List.".format(tableName)
            self.myMsgBar.newMessage('info', aMsg)

            idxRecListAll = getIndex(self.recList[self.curRecNumV.get()][0], self.recAll)
            itemToUpdateDeletedField = self.recAll[idxRecListAll]
            itemWithDeletedFieldUpdated = ()

            for i in range(len(itemToUpdateDeletedField)):
                if i == self.numberOfFields - 3:
                    itemWithDeletedFieldUpdated += ('N',)
                else:
                    itemWithDeletedFieldUpdated += (self.recAll[idxRecListAll][i],)

            self.recAll.pop(idxRecListAll)  # remove the old one
            self.recAll.insert(idxRecListAll, itemWithDeletedFieldUpdated)  # add the new one

            if self.viewSet.get() == 'Closed':
                self.recList.pop(self.curRecNumV.get())  # remove from current list (only if active view)
                if self.curRecBeforeOp > 0:
                    self.curRecNumV.set(self.curRecBeforeOp - 1)
            elif self.viewSet.get() == 'All':
                self.recList.pop(self.curRecBeforeOp)
                self.recList.insert(self.curRecBeforeOp, itemWithDeletedFieldUpdated)  # add the new one
                self.curRecNumV.set(self.curRecBeforeOp)
            else:
                self.curRecNumV.set(self.curRecBeforeOp)
            '''
        self._enableWindowCommand()
        self.myDatabaseBar.defaultState()
        self.navigationEnable(self.accessLevel)
        self.resetCurOp()
        self.displayRec()

    def cancelCommand(self, *args):
        self.clearPopups()
        self.myMsgBar.clearMessage()
        self.myDatabaseBar.cancelState()
        if self.prevFind == True or self.curFind == True:
            self.displayCurFunction.setFindResultMode()
        else:
            self.displayCurFunction.setDefaultMode()
        self.curOp = 'default'
        self.navigationEnable(self.accessLevel)
        self.setRequiredFields()
        self.fieldsDisable()
        if len(self.recList) != 0:
            self.displayRec()
        else:
            self._displayRecordFrame()
        self._setAsDeleted()

    def deleteCommand(self, tableName, *args):
        self.myMsgBar.clearMessage()
        self.myDatabaseBar.deleteState()
        self.myNavBar.disableNavigation()
        if len(self.recList) == 0:
            self.navigationDisable(self.accessLevel)
        self.curOp = 'delete'
        self.displayCurFunction.setDeleteMode()
        self.curRecBeforeOp = self.curRecNumV.get()
        self.delete(tableName)
        self.displayRec()

    def deleteSetCommand(self, tableName, *args):
        self.myMsgBar.clearMessage()
        self.myDatabaseBar.deleteState()
        self.myNavBar.disableNavigation()
        if len(self.recList) == 0:
            self.navigationDisable(self.accessLevel)
        self.curOp = 'deleteSet'
        self.displayCurFunction.setDeleteSetMode()
        self.curRecBeforeOp = self.curRecNumV.get()
        self.deleteSet(tableName)

    def deleteSet(self, tableName):
        self._disableWindowCommand()
        TotalRecordsDeleted = len(self.recList)
        aMsg = "Are you sure you want to delete All {0} Records in the current Set?".format(TotalRecordsDeleted)
        ans = AppQuestionRequest(self, "Delete Set - {0}".format(tableName), aMsg)
        if ans.result == True:
            pass
            '''
           AppDB.deleteSet(tableName, self.recList, convertDateStringToOrdinal(todayDate),
                            self.updaterID)  # remove from DB
            #
            #    Following avoids reloading the whole table after removal field is updated above.
            #    Removes it from current view and the over all list is also updated to reflect
            #  the new deleted status.
            #
            for i in range(len(self.recList)):
                idxRecListAll = getIndex(self.recList[i][0], self.recAll)
                itemToUpdateDeletedField = self.recAll[idxRecListAll]
                itemWithDeletedFieldUpdated = ()
                for i in range(len(itemToUpdateDeletedField)):
                    if i == self.numberOfFields - 3:
                        itemWithDeletedFieldUpdated += ('Y',)
                    else:
                        itemWithDeletedFieldUpdated += (self.recAll[idxRecListAll][i],)
                self.recAll.pop(idxRecListAll)  # remove the old one
                self.recAll.insert(idxRecListAll, itemWithDeletedFieldUpdated)  # add the new one
            aMsg = "{0} records were removed from the {0} Active List.".format(TotalRecordsDeleted, self.tableName)
            self.myMsgBar.newMessage('info', aMsg)

            if self.viewSet.get() != 'Closed':
                for i in range(len(self.recList)):
                    self.recList.pop(0)
            else:
                self.basicViewsList('Closed')
            '''
        self.curRecNumV.set(
            0)  # Only if root was in the list, self.displayRec will detect if there is a record in the list
        self.displayRec()
        self._enableWindowCommand()
        self.myDatabaseBar.defaultState()
        self.navigationEnable(self.accessLevel)
        self.resetCurOp()

    def unDeleteSetCommand(self, tableName, *args):
        self.myMsgBar.clearMessage()
        self.myDatabaseBar.deleteState()
        self.myNavBar.disableNavigation()
        if len(self.recList) == 0:
            self.navigationDisable(self.accessLevel)
        self.curOp = 'unDeleteSet'
        self.displayCurFunction.setUnDeleteSetMode()
        self.curRecBeforeOp = self.curRecNumV.get()
        self.unDeleteSet(tableName)

    def unDeleteSet(self, tableName):
        self._disableWindowCommand()
        TotalRecordsUnDeleted = len(self.recList)
        aMsg = "Are you sure you want to undelete All {0} records in the current Set?".format(TotalRecordsUnDeleted)
        ans = AppQuestionRequest(self, "UnDelete Set - {0}".format(tableName), aMsg)
        if ans.result == True:
            pass
            '''
            temp = []
            AppDB.unDeleteSet(tableName, self.recList, convertDateStringToOrdinal(todayDate), self.updaterID)  #
            for i in range(len(self.recList)):
                idxRecListAll = getIndex(self.recList[i][0], self.recAll)
                itemToUpdateDeletedField = self.recAll[idxRecListAll]
                itemWithDeletedFieldUpdated = ()
                for i in range(len(itemToUpdateDeletedField)):
                    if i == self.numberOfFields - 3:
                        itemWithDeletedFieldUpdated += ('N',)
                    else:
                        itemWithDeletedFieldUpdated += (self.recAll[idxRecListAll][i],)
                self.recAll.pop(idxRecListAll)  # remove the old one
                self.recAll.insert(idxRecListAll, itemWithDeletedFieldUpdated)  # add the new one
                if self.viewSet.get() != 'Closed':
                    temp.append(itemWithDeletedFieldUpdated)

            aMsg = "{0} records were added from the {1} Active List.".format(TotalRecordsUnDeleted, tableName)
            self.myMsgBar.newMessage('info', aMsg)

            if self.viewSet.get() == 'Closed':
                for i in range(len(self.recList)):
                    self.recList.pop(0)
            else:
                self.recList = temp
            '''
        self.curRecNumV.set(self.curRecBeforeOp)
        self._enableWindowCommand()
        self.myDatabaseBar.defaultState()
        self.navigationEnable(self.accessLevel)
        self.resetCurOp()
        self.displayRec()

    def removeFromSetCommand(self, *args):
        if len(self.recList) > 0:
            self.recList.pop(self.curRecNumV.get())  # Remove the item that was modified
            if self.curRecNumV.get() > (len(self.recList) - 1):
                self.curRecNumV.set(len(self.recList) - 1)
            self.curRecNumV.set(self.curRecNumV.get())
            self.displayRec()
            aMsg = 'WARNING: Record was removed from the current view.'
            self.myMsgBar.newMessage('warning', aMsg)

    def setRequiredFields(self):
        pass

    def resetRequiredFields(self):
        pass

    def fieldsClear(self):
        # Table Specific
        pass

    def fieldsEnable(self):
        # Table Specific
        pass

    def fieldsDisable(self):
        # Table Specific
        pass

    def navigationDisable(self, accessLevel):
        self.myDatabaseBar.duplicateCommand.config(state=DISABLED)
        self.myDatabaseBar.buttonFind.config(state=DISABLED)
        self.myDatabaseBar.editCommand.config(state=DISABLED)
        self.myDatabaseBar.deleteButton.config(state=DISABLED)
        self.myDatabaseBar.removeFromSetButton.config(state=DISABLED)
        self.myDatabaseBar.buttonRefresh.config(state=DISABLED)
        if accessLevel == 'View':
            self.myDatabaseBar.buttonFind.config(state=NORMAL)
        self.myNavBar.disableNavigation()

    def navigationEnable(self, accessLevel):
        if self.viewSet.get() == 'Closed':  # All, Active, Closed
            if accessLevel == 'Root':
                self.myDatabaseBar.duplicateCommand.config(state=NORMAL)
                self.myDatabaseBar.buttonUndelete.config(state=NORMAL)
            else:
                self.myDatabaseBar.duplicateCommand.config(state=DISABLED)
            self.myDatabaseBar.buttonFind.config(state=DISABLED)
            self.myDatabaseBar.editCommand.config(state=DISABLED)
            self.myDatabaseBar.deleteButton.config(state=DISABLED)
            self.myDatabaseBar.removeFromSetButton.config(state=NORMAL)
            self.myDatabaseBar.addCommand.config(state=DISABLED)
            self.myDatabaseBar.buttonRefresh.config(state=NORMAL)
        elif accessLevel == 'Root':
            self.myDatabaseBar.duplicateCommand.config(state=NORMAL)
            self.myDatabaseBar.buttonFind.config(state=NORMAL)
            self.myDatabaseBar.editCommand.config(state=NORMAL)
            self.myDatabaseBar.deleteButton.config(state=NORMAL)
            self.myDatabaseBar.removeFromSetButton.config(state=NORMAL)
            self.myDatabaseBar.addCommand.config(state=NORMAL)
            self.myDatabaseBar.buttonRefresh.config(state=NORMAL)
        elif accessLevel == 'Maintenance':
            self.myDatabaseBar.duplicateCommand.config(state=NORMAL)
            self.myDatabaseBar.buttonFind.config(state=NORMAL)
            self.myDatabaseBar.editCommand.config(state=NORMAL)
            self.myDatabaseBar.deleteButton.config(state=NORMAL)
            self.myDatabaseBar.removeFromSetButton.config(state=NORMAL)
            self.myDatabaseBar.addCommand.config(state=NORMAL)
            self.myDatabaseBar.buttonRefresh.config(state=NORMAL)
        elif accessLevel == 'Update':
            self.myDatabaseBar.duplicateCommand.config(state=DISABLED)
            self.myDatabaseBar.buttonFind.config(state=NORMAL)
            self.myDatabaseBar.editCommand.config(state=NORMAL)
            self.myDatabaseBar.deleteButton.config(state=DISABLED)
            self.myDatabaseBar.removeFromSetButton.config(state=NORMAL)
            self.myDatabaseBar.addCommand.config(state=DISABLED)
            self.myDatabaseBar.buttonRefresh.config(state=NORMAL)
        elif accessLevel == 'View':
            self.myDatabaseBar.duplicateCommand.config(state=DISABLED)
            self.myDatabaseBar.buttonFind.config(state=NORMAL)
            self.myDatabaseBar.editCommand.config(state=DISABLED)
            self.myDatabaseBar.deleteButton.config(state=DISABLED)
            self.myDatabaseBar.removeFromSetButton.config(state=NORMAL)
            self.myDatabaseBar.addCommand.config(state=DISABLED)
            self.myDatabaseBar.buttonRefresh.config(state=NORMAL)

        if len(self.recList) == 0:
            self.myDatabaseBar.emptyDBState()
        else:
            self.myNavBar.enableNavigation()


class PopUpWindow(Toplevel):
    def __init__(self, parent, Mode=None, uniqueID=None, updaterID=None):

        Toplevel.__init__(self, parent)
        self.transient(parent)
        self.uniqueID = uniqueID
        self.mode = Mode
        self.parent = parent
        self.recList = []
        self.updaterID = updaterID

        self.row = 0
        self.column = 0
        self.rowconfigure(self.row, weight=1)
        self.columnconfigure(self.column, weight=1)

        self.result = False
        self.anID = None

        body = AppFrame(self, 1)
        body.noBorder()
        body.grid(row=self.row, column=self.column, sticky=N + S + E + W)

        #        self.mainArea = Tix.Frame(self, border=3, relief='groove', takefocus=0)
        #        self.mainArea.grid(row=self.rowCount, column=self.columnCount, columnspan=self.columnCountTotal, sticky=N+S+E+W)

        self.buttonbox()
        self.msgbox()

        self.initial_focus = self.body(body)

        self.grab_set()

        if not self.initial_focus:
            self.initial_focus = self

        self.protocol("WM_DELETE_WINDOW", self.no)

        self.geometry("+%d+%d" % (parent.winfo_rootx() + 50,
                                  parent.winfo_rooty() + 50))

        self.initial_focus.focus_set()

        self.wait_window(self)

    def _setAsDeleted(self, *args):
        #
        #  At a minimun, record action are disabled.
        #  Additionnal commands can be added, see person class
        #  for override example for this method.
        #
        self.recordFrame.config(highlightcolor=AppDefaultForeground, takefocus=1)
        self.recordFrame.focus()
        if len(self.recList) > 0:
            if self.recList[0][self.numberOfFields - 3] == 'Y':
                self.recordFrame.config(highlightcolor=AppDeletedHighlightColor, takefocus=1)
                self.recordFrame.focus()

    def _displayRecordFrame(self, aFrame):
        #        if len(self.recList) > 0:
        self.recordFrame = AppFrame(aFrame, 1)
        self.recordFrame.grid(row=aFrame.row, column=aFrame.column, columnspan=aFrame.columnTotal, sticky=N + S + E + W)
        self.recordFrame.addAccentBackground()
        self.recordFrame.addDeletedHighlight()
        self.recordFrame.noBorder()

        self._buildWindowsFields(self.recordFrame)
        self.recordFrame.rowconfigure(self.recordFrame.row, weight=0)
        self.displayRec()
        #       else:
        #           aMsg = 'ERROR: Record not found.'
        #           self.myMsgBar.newMessage('error', aMsg)

    def _buildWindowsFields(self, aFrame):
        pass

    def displayRec(self):
        self._setAsDeleted(None)
        pass

    def addRow(self):
        self.row = self.row + 1
        self.rowconfigure(self.row, weight=1)

    def addColumn(self):
        self.column = self.column + 1
        self.columnconfigure(self.column, weight=1)

    def body(self, master):
        # create dialog body.  return widget that should have
        # initial focus.  this method should be overridden

        pass

    def buttonbox(self):
        # add standard button box. override if you don't want the
        # standard buttons

        box = AppFrame(self, 2)
        box.noBorder()

        if self.mode == 'view':
            w = Button(box, text="Exit", style='CommandButton.TButton', command=self.no)
            w.grid(row=0, column=0, columnspan=box.columnTotal, sticky=N)
        else:
            self.saveButton = Button(box, text="Save", style='CommandButton.TButton', command=self.yes,
                                         default=ACTIVE)
            self.saveButton.grid(row=0, column=0, sticky=E + N)
            w = Button(box, text="Cancel", style='CommandButton.TButton', command=self.no)
            w.grid(row=0, column=1, sticky=W + N)

        self.bind("<Return>", self.yes)
        self.bind("<Escape>", self.no)

        self.addRow()
        box.grid(row=self.row, column=self.column, sticky=N + S + E + W)

    def msgbox(self):
        # add standard button box. override if you don't want the
        # standard buttons

        box = AppFrame(self, 1)
        box.noBorder()

        self.myMsgBar = messageBar(box)
        self.myMsgBar.grid(row=0, column=0, sticky=W + S, padx=5)

        self.addRow()
        box.grid(row=self.row, column=self.column, sticky=N + S + E + W)

    def yes(self, event=None):

        if not self.validate():
            self.initial_focus.focus_set()  # put focus back
            return

        self.withdraw()
        self.update_idletasks()

        self.apply()

        self.no()

    def no(self, event=None):

        # put focus back to the parent window
        self.parent.focus_set()
        self.destroy()

    #
    # command hooks

    def validate(self):
        #
        #  If any form validation is required, put them here
        #

        return 1  # override

    def apply(self):

        pass  # override

