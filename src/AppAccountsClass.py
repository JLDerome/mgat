##########################################################
#
#   Departments Table
#
#   This class implements the specific manipulation of the
# data for the ECE Departments Table that the TECH Shop requires.
#

import tkinter.tix as Tix
import tkinter.ttk as ttk
from MyGolfApp2016 import AppCRUD_FROMECECRUD as AppDB
import tkinter.filedialog as Fd
import tkinter.messagebox as Mb
import textwrap, re

from tkinter.constants import *

from MyGolfApp2016.AppMyClasses import *
from MyGolfApp2016.AppConstants import *

from MyGolfApp2016.AppProcFROMECE import *

from PIL import Image
#
# Starter frame for a new table
#
class accountsWindow(tableCommonWindowClass):
    def __init__(self, aWindow, aCloseCommand, loginData):
        self.windowTitle = 'Accounts'
        tableCommonWindowClass.__init__(self, aWindow, aCloseCommand, self.windowTitle, loginData[1])
        self.loginData = loginData
        self.accessLevel = self.loginData[1]
        self.updaterID = self.loginData[2]
        rankFullName=AppDB.getRankFullName(self.updaterID)
        self.displayCurUser.setUserName(rankFullName)
#######################################################################################
# Inherited all from tableCommanWindowClass
#  
#  This class intents to change the mainArea only.  Everything else must remain common
#       
        self.mainAreaRowCount = 0
        self.mainAreaColumnCount = 0       
        self._getNewRecList()
        self._reDisplayRecordFrame()
        self.mainArea.rowconfigure(self.mainAreaRowCount, weight=1)
        
############################################################ Area to add window specific commands        
#        self.closeButton = ttk.Button(self.commandFrame, text='Test', style='CommandButton.TButton', 
#                                 command= lambda: self.sampleCommand())
#        self.closeButton.grid(row=0, column=0, sticky=N+S)
        

################################################################# Redefined commands
#
#  invoke from the tableCommonWindows.  These commands are specific to each tables
#  The specific actions are redefinded here.
#       
#    def displayRec(self, *args):
#        print("Displaying a Record: ", self.curRecNum)
#        
#    def save(self):
#        #
#        #  Table specific.  Return a True if succesful saved.
#        #
#        print("Saving a record (re-defined)")
#        return True

    def _departmentEntered(self, *args):
        pass

    def _enableWindowCommand(self):
        #
        #  Any command specific must added here 
        #
        pass
#        self.closeButton.config(state=NORMAL)

    def _disableWindowCommand(self):
        #
        #  Any command specific must added here 
        #
        pass
#        self.closeButton.config(state=DISABLED)
               
    def sampleCommand(self):
        self.myMsgBar.clearMessage()
        aMsg = 'INFO: Command area, specific to current window ({0})'.format(self.windowTitle)
        self.myMsgBar.newMessage('info', aMsg)
        AppDB.importCSV_Parts(importTUTPartsFilename)

    def _getNewRecList(self):
        self.recList = AppDB.getAccounts()
        self.curRecNumV.set(self.curRecNumV.get()) # forces display of record number
        
    def _reDisplayRecordFrame(self):
        self.recordFrame.destroy()
        self._getNewRecList()
        self._displayRecordFrame()
        if self.curOp == 'add':
            self.fieldsEnable()
        
    def _displayRecordFrame(self):
        self.recordFrame = AppFrame(self.mainArea, 1) 
        self.recordFrame.grid(row=self.mainAreaRowCount, column=self.mainAreaColumnCount, columnspan= self.mainAreaColumnTotal, sticky=N+S+E+W)
        
        if len(self.recList) > 0:
            self._buildWindowsFields(self.recordFrame)
            self.recordFrame.rowconfigure(self.recordFrame.row, weight=0)
            if self.curOp != 'add':        
                self.displayRec()
                self.navigationEnable(self.accessLevel)
            new_order = (
                            )
            
            for widget in new_order:
                widget.lift()
        else:
            wMsg = Tix.Label(self.recordFrame, text='No Records Found in Current List.', font=fontMonstrousB)
            wMsg.grid(row=0, column=0, columnspan=self.recordFrame.columnTotal)
            
    def _buildWindowsFields(self, aFrame):
        ######################################################
        # '''CREATE TABLE Accounts(
        #            0                 accountsID      INTEGER PRIMARY KEY AUTOINCREMENT  NOT NULL,
        #            1                 FIN            CHAR(20),
        #            2                 SOURCE         CHAR(20),
        #            3                 FUNDS          CHAR(7),
        #            4                 APPROV         CHAR(30),
        #            5                 A_TITLE        CHAR(60)
        #            6                 ACTIVE         CHAR(1),
        #            7                 DEPARTMENT     CHAR(2),
        #            8                 SEC_MANAGE     CHAR(1),
        #            9                 SEC_LEVEL      CHAR(2),
        #            10                SEC_SECT       CHAR(1),
        #            11                SEC_DIVIS      CHAR(1),
        #            12                SEC_DEPTN      CHAR(2),
        #            13                DAY            CHAR(2),
        #            14                MONTH          CHAR(3),
        #            15                YEAR           CHAR(4),
        #            16                BUDGET_AMNT    REAL,
        #            17                COMMIT_AMNT    REAL,
        #            18                EXPEND_AMNT    REAL,
        #            19                BALANCE_AMNT   REAL,
        #            20                DESCR          CHAR(40),
        #            21                D_NAME         CHAR(40),
        #            22                RESOURCE       CHAR(15),
        #            23                SPECIAL        CHAR(1),
        #            24                FMAS_CODE      CHAR(2),
        #            25                DOC            CHAR(3),
        #            26                IO_NUMBER      CHAR(20),
        #            27                CO_NUMBER      CHAR(20),
        #            28                FCODE          CHAR(6),
        #            29                CCENTER        CHAR(6),
        #            30                GLEDGER        CHAR(5),
        #            31                FUNDC          CHAR(5),
        #            32                R_TYPE         CHAR(3),
        #            33                lastModified   INTEGER,
        #            34                updaterID      INTEGER,
        #            35                departmentsID  INTEGER
        #             );''')
        #
        colTotal = 2
        self.fieldsFrame = AppFrame(aFrame, colTotal)
        self.fieldsFrame.grid(row=0, column=0, sticky=N+S+E+W)

        self.departmentsCBData = AppCBList(AppDB.getDepartmentsDictionaryInfo())
#        self.selectedDepartment = AppHorizontalCB3(self.fieldsFrame, 'Department', departmentV, self.departmentsCBData.getList(), partsDepartmentLenght)
        self.selectedDepartment = AppHorizontalCB(self.fieldsFrame, 'Department', self.departmentsCBData.getList(), partsDepartmentLenght)
        self.selectedDepartment.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, columnspan=colTotal, sticky=N+S)
        self.selectedDepartment.bind('<FocusOut>', lambda event: self._departmentEntered(event))
        self.selectedDepartment.disable()

        # Empty row for separation
#        empty = AppSpaceRow(self.fieldsFrame)
#        empty.grid(row=self.fieldsFrame.row, column=0, sticky=N+S)

############################################# Last Update Row
        self.fieldsFrame.row = self.fieldsFrame.row + 1
        self.fieldsFrame.column = 0
        self.fieldsFrame.rowconfigure(self.fieldsFrame.row, weight=0)
        
        self.lastUpdate = AppLastUpdateFormat(self.fieldsFrame, self.recList[self.curRecNumV.get()][33], self.recList[self.curRecNumV.get()][34])
        self.lastUpdate.grid(row=self.fieldsFrame.row,column=self.fieldsFrame.column, columnspan = colTotal, sticky=E+N+S)
              
        self.fieldsDisable()
#        self.fieldsFrame.rowconfigure(self.fieldsFrame.row, weight=0)
#        self.headerList = ['Code', 'Name']
#        for i in range(len(self.headerList)):       
#            aLabel = ttk.Label(self.fieldsFrame, text=self.headerList[i], style='RegularFieldTitle.TLabel')
#            aLabel.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, sticky=N+S)
#            self.fieldsFrame.column = self.fieldsFrame.column + 1

    def displayRec(self, *args):
        ######################################################
        # '''CREATE TABLE Accounts(
        #            0                 accountsID      INTEGER PRIMARY KEY AUTOINCREMENT  NOT NULL,
        #            1                 FIN            CHAR(20),
        #            2                 SOURCE         CHAR(20),
        #            3                 FUNDS          CHAR(7),
        #            4                 APPROV         CHAR(30),
        #            5                 A_TITLE        CHAR(60)
        #            6                 ACTIVE         CHAR(1),
        #            7                 DEPARTMENT     CHAR(2),
        #            8                 SEC_MANAGE     CHAR(1),
        #            9                 SEC_LEVEL      CHAR(2),
        #            10                SEC_SECT       CHAR(1),
        #            11                SEC_DIVIS      CHAR(1),
        #            12                SEC_DEPTN      CHAR(2),
        #            13                DAY            CHAR(2),
        #            14                MONTH          CHAR(3),
        #            15                YEAR           CHAR(4),
        #            16                BUDGET_AMNT    REAL,
        #            17                COMMIT_AMNT    REAL,
        #            18                EXPEND_AMNT    REAL,
        #            19                BALANCE_AMNT   REAL,
        #            20                DESCR          CHAR(40),
        #            21                D_NAME         CHAR(40),
        #            22                RESOURCE       CHAR(15),
        #            23                SPECIAL        CHAR(1),
        #            24                FMAS_CODE      CHAR(2),
        #            25                DOC            CHAR(3),
        #            26                IO_NUMBER      CHAR(20),
        #            27                CO_NUMBER      CHAR(20),
        #            28                FCODE          CHAR(6),
        #            29                CCENTER        CHAR(6),
        #            30                GLEDGER        CHAR(5),
        #            31                FUNDC          CHAR(5),
        #            32                R_TYPE         CHAR(3),
        #            33                lastModified   INTEGER,
        #            34                updaterID      INTEGER,
        #            35                departmentsID  INTEGER
        #             );''')
        # Example
        # self.selectedCode.load(self.recList[self.curRecNumV.get()][1])
        if self.recList[self.curRecNumV.get()][35] != '':
            self.selectedDepartment.load(self.departmentsCBData.getName(self.recList[self.curRecNumV.get()][35]))
        else:
            self.selectedDepartment.clear()
#        self.selectecSupplier.load(self.recList[self.curRecNumV.get()][36])
        self.lastUpdate.load(self.recList[self.curRecNumV.get()][33], self.recList[self.curRecNumV.get()][34])        
                              
        if self.accessLevel == 'Root':
            pass
            
        self.fieldsDisable()
        
              
    def fieldsClear(self):
        # Table Specific
        self.selectedDepartment.clear()
                      
    def fieldsDisable(self):
        # Table Specific
        self.selectedDepartment.disable()
                               
    def fieldsEnable(self):
        # Table Specific
        self.selectedDepartment.enable()
                
    ################################################################# Redefined commands
    #
    #  invoke from the tableCommonWindows.  These commands are specific to each tables
    #  The specific actions are redefinded here.
    #       
    #    def displayRec(self, *args):
    #        print("Displaying a Record: ", self.curRecNumV.get())
    #        self._displayRecordFrame()
    def duplicate(self):
        self.fieldsEnable()
    
    def find(self):
        #Table specific.  Will be defined in calling class
        self.fieldsClear()

    def refresh(self):
        self._reDisplayRecordFrame()

    def delete(self):
        self._disableWindowCommand()
        aMsg = "Are you sure you want to delete record {0}?".format(self.recList[self.curRecNumV.get()][1])
        ans = Mb.askyesno('Delete Record', aMsg)
        if ans == True:
#            AppDB.deleteParts(self.recList[self.curRecNumV.get()][0]) # remove from DB
            aMsg = "Record {0} has been deleted.".format(self.recList[self.curRecNumV.get()][1])
            self.myMsgBar.newMessage('info', aMsg)
            self.recList.pop(self.curRecNumV.get())  # remove from current list
            if self.curRecBeforeOp != 0:
                self.curRecNumV.set(self.curRecBeforeOp - 1)
            else:
                self.curRecNumV.set(self.curRecBeforeOp)
            self._displayRecordFrame()
        self._enableWindowCommand()     
        self.navigationEnable(self.accessLevel)
        self.myDatabaseBar.defaultState()
        self.resetCurOp()
        
    def edit(self):
        self.fieldsEnable()
                 
    def add(self):
        self.fieldsClear()
        self.fieldsEnable()
#        self.selectedBasic.focus()     # Table Specific
    
    def save(self):
        #
        #  Table specific.  Return a True if succesful saved.
        #
        if self.curOp == 'add':
            
            myValue = self._parseSelectedValue(self.selectedValue.get())
            myPrice = self._parseSelectedPrice(self.selectedPrice.get())
#            AppDB.insertParts(self.selectedBasic.get(),myValue[0],myValue[1],self.selectedMultiplier.get(),self.selectedTolerance.get(),self.selectedSize.get(),self.selectedDescription.get(),
#                        self.selectecNSN1.get(),self.selectecNSN2.get(),self.selectecNSN3.get(),self.selectecNSN4.get(),self.selectecCatNum.get(),
#                        self.selectecSupplierNum.get(),'',self.selectedUI.get(),self.selectecSNC.get(),self.selectedClass.get(),
#                        myPrice[0],myPrice[1],'',self.selectecSSI.get(), '',
#                        self.selectedStock.get(),self.selectedMin.get(),self.selectedOnOrder.get(),self.selectedNeeded.get(),self.selectedToday.get(),
#                        self.selectedFunction.get(),self.selectedDescription2.get(),self.selectedNomenclature.get(),self.selectedRow.get(),
#                        self.selectedSide.get(),self.selectedShelf.get(),self.selectedSection.get(),self.selectedDepartment.get(),self.selectecSupplier.get(),
#                        convertDateStringToOrdinal(todayDate), self.updaterID)
#            
#            ID = AppDB.getPartsID(self.selectedBasic.get(), myValue[0], myValue[1], self.selectedDescription.get(), self.selectedDescription2.get(), 
#                                  self.selectedFunction.get(), self.selectedNomenclature.get(), convertDateStringToOrdinal(todayDate),  self.updaterID)
            ID = [(34),]
            
            aMsg = "Record {0} has been added.".format(self.selectedBasic.get())
            self.myMsgBar.newMessage('info', aMsg)
            
            
            
            if self.curFind == True:
                anItem=()
#                anItem = (ID[0][0],self.selectedBasic.get(),myValue[0],myValue[1],self.selectedMultiplier.get(),self.selectedTolerance.get(),self.selectedSize.get(),self.selectedDescription.get(),
#                        self.selectecNSN1.get(),self.selectecNSN2.get(),self.selectecNSN3.get(),self.selectecNSN4.get(),self.selectecCatNum.get(),
#                        self.selectecSupplierNum.get(),'',self.selectedUI.get(),self.selectecSNC.get(),self.selectedClass.get(),
#                        myPrice[0],myPrice[1],'',self.selectecSSI.get(), '',
#                        self.selectedStock.get(),self.selectedMin.get(),self.selectedOnOrder.get(),self.selectedNeeded.get(),self.selectedToday.get(),
#                        self.selectedFunction.get(),self.selectedDescription2.get(),self.selectedNomenclature.get(),self.selectedRow.get(),
#                        self.selectedSide.get(),self.selectedShelf.get(),self.selectedSection.get(),self.selectedDepartment.get(),self.selectecSupplier.get(),
#                        convertDateStringToOrdinal(todayDate), self.updaterID)
                self.recList.append(anItem)
                self.curRecNumV.set(len(self.recList)-1)               
                aNewList = sorted(self.recList, key=getKey)
                self.recList = aNewList
                newIdx = getIndex(ID[0][0], aNewList)
                self.curRecNumV.set(newIdx)
                self.displayRec()
            else:
                #
                #  If not in find mode, re_display the list and point
                # to the newly added item.
                #           
                self._getNewRecList()
                newIdx = getIndex(ID[0][0], self.recList)
                if newIdx != -1:
                    self.curRecNumV.set(newIdx)                
                self._reDisplayRecordFrame()
            self.resetCurOp()
           
        elif self.curOp == 'edit':
            pass 
            myValue = self._parseSelectedValue(self.selectedValue.get())
            myPrice = self._parseSelectedPrice(self.selectedPrice.get())
#            AppDB.updateParts(self.recList[self.curRecNumV.get()][0], self.selectedBasic.get(),myValue[0],myValue[1],self.selectedMultiplier.get(),self.selectedTolerance.get(),
#                        self.selectedSize.get(),self.selectedDescription.get(),self.selectecNSN1.get(),self.selectecNSN2.get(),self.selectecNSN3.get(),self.selectecNSN4.get(),
#                        self.selectecCatNum.get(),self.selectecSupplierNum.get(),self.recList[self.curRecNumV.get()][14],self.selectedUI.get(),self.selectecSNC.get(),self.selectedClass.get(),
#                        myPrice[0],myPrice[1],self.recList[self.curRecNumV.get()][20],self.selectecSSI.get(), self.recList[self.curRecNumV.get()][22],
#                        self.selectedStock.get(),self.selectedMin.get(),self.selectedOnOrder.get(),self.selectedNeeded.get(),self.selectedToday.get(),
#                        self.selectedFunction.get(),self.selectedDescription2.get(),self.selectedNomenclature.get(),self.selectedRow.get(),
#                        self.selectedSide.get(),self.selectedShelf.get(),self.selectedSection.get(),self.selectedDepartment.get(),self.selectecSupplier.get(),
#                        convertDateStringToOrdinal(todayDate), self.updaterID)
            aMsg = "Record {0} has been updated.".format(self.selectedBasic.get())
            self.myMsgBar.newMessage('info', aMsg)
            #
            # Add the item again but with modified data.            
            #
            anItem=()
#            anItem=(self.recList[self.curRecNumV.get()][0], self.selectedBasic.get(),myValue[0],myValue[1],self.selectedMultiplier.get(),self.selectedTolerance.get(),
#                        self.selectedSize.get(),self.selectedDescription.get(),self.selectecNSN1.get(),self.selectecNSN2.get(),self.selectecNSN3.get(),self.selectecNSN4.get(),
#                        self.selectecCatNum.get(),self.selectecSupplierNum.get(),self.recList[self.curRecNumV.get()][14],self.selectedUI.get(),self.selectecSNC.get(),self.selectedClass.get(),
#                        myPrice[0],myPrice[1],self.recList[self.curRecNumV.get()][20],self.selectecSSI.get(), self.recList[self.curRecNumV.get()][22],
#                        self.selectedStock.get(),self.selectedMin.get(),self.selectedOnOrder.get(),self.selectedNeeded.get(),self.selectedToday.get(),
#                        self.selectedFunction.get(),self.selectedDescription2.get(),self.selectedNomenclature.get(),self.selectedRow.get(),
#                        self.selectedSide.get(),self.selectedShelf.get(),self.selectedSection.get(),self.selectedDepartment.get(),self.selectecSupplier.get(),
#                        convertDateStringToOrdinal(todayDate), self.updaterID)
            aRemovedItem = self.recList.pop(self.curRecNumV.get())  # Remove the item that was modified
            self.recList.insert(self.curRecNumV.get(),anItem)
            self.displayRec()
            self.resetCurOp()
            
        elif self.curOp == 'find':
#            self.recListTemp = AppDB.findParts(self.selectedBasic.get(), self.selectedDescription.get(), self.selectedDescription2.get(), self.selectedNomenclature.get(), 
#                                           self.selectedFunction.get())
            self.recListTemp=[]
            if len(self.recListTemp) == 0:
                aMsg = "WARNING: No records were found using the given search criterias."
#                self._reDisplayRecordFrame()
                self.curFind = False
                self.displayCurFunction.setDefaultMode()
                self.myMsgBar.newMessage('warning', aMsg)
            elif len(self.recList) == 1:
                self.curFind = True
                self.recList = self.recListTemp
                self.displayCurFunction.setFindResultMode()
                aMsg = "One record was found using the given search criterias."
                self.curRecNumV.set(0)
                self.myMsgBar.newMessage('info', aMsg)
            else:
                self.curFind = True
                self.recList = self.recListTemp
                self.displayCurFunction.setFindResultMode()
                aMsg = "{0} records were found using the given search criterias.".format(len(self.recList))
                self.curRecNumV.set(0)
                self.myMsgBar.newMessage('info', aMsg)
            self.displayRec()
        return True
    