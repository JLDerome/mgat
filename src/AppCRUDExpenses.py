import sqlite3 as sql
import os
import csv
import AppCRUD as AppCRUD
from AppProc import getDBLocationFullName, convertDateStringToOrdinal
from AppConstants import *

db=None
cursor = None
lidGolfers = 0

#cursor.execute('''CREATE TABLE MBExpenses(
#   uniqueID            INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
#   expenseDate          INTEGER    NOT NULL,
#   expenseItem          CHAR(100)  NOT NULL,
#   expenseAmount        REAL       NOT NULL,
#   currency             CHAR(5)    NOT NULL,
#   USExchangeRate       REAL,
#   expenseAdjusted      REAL,
#   golferID             INTEGER    NOT NULL,
#   deleted              CHAR(1)    NOT NULL,
#   lastModified         INTEGER,
#   updaterID            INTEGER,
#   FOREIGN KEY(golferID) REFERENCES Golfers(golferID) ON DELETE CASCADE
#   );''')

def find_MBExpenses(ExpenseDescription,GolferID,ExpenseDate,Amount,
                    Currency,Rate,AdjustedAmnt):

    if ExpenseDescription == None or len(ExpenseDescription) == 0:
        field0 = "expenseItem like '%%' "
    else:
        field0 = "expenseItem like '%{0}%' ".format(ExpenseDescription)

    if GolferID == None or GolferID == '':
        field1 = ''
    else:
        field1 = 'AND golferID = {0}'.format(GolferID)

    if ExpenseDate == None or len(ExpenseDate) == 0:
        field2 = ""
    else:
        field2 = "AND expenseDate = {0} ".format(convertDateStringToOrdinal(ExpenseDate))

    if Amount == None or Amount == float(0):
        field3 = ''
    else:
        field3 = "AND expenseAmount = {0} ".format(Amount)

    if Currency == None or len(Currency) == 0:
        field4 = ''
    else:
        field4 = "AND currency like '%{0}%' ".format(Currency)

    if Rate == None or Rate == float(0):
        field5 = ''
    else:
        field5 = "AND USExchangeRate = {0} ".format(Rate)

    if AdjustedAmnt == None or AdjustedAmnt == float(0):
        field6 = ''
    else:
        field6 = "AND expenseAdjusted = {0} ".format(AdjustedAmnt)

    AppCRUD.initDB(getDBLocationFullName())

    query = '''select * from MBExpenses where {0} {1} {2} {3} {4} {5} {6}
                                        order by golferID, expenseDate
            '''.format(field0, field1, field2, field3, field4, field5, field6)

    print("The query is: ", query)

    temp = AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()
    return temp

def get_MBExpenses():
    AppCRUD.initDB(getDBLocationFullName())
    query = '''select * from MBExpenses order by golferID, expenseDate'''
    temp =  AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()
    return temp

def get_MBExpensesLastUniqueID():
    AppCRUD.initDB(getDBLocationFullName())
    query = '''select uniqueID from MBExpenses order by uniqueID'''
    temp =  AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()
    size = len(temp)
    return temp[size-1][0]

def get_MBExpensesForExpenseID(uniqueID):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''select * from MBExpenses where uniqueID = {0}'''.format(uniqueID)
    temp = AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()
    return temp

def get_MBExpenses_Range(startDateOrd, endDateOrd):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''select * from MBExpenses where expenseDate >= {0} and expenseDate <= {1} and deleted = 'N' order by expenseDate, golferID '''.format(startDateOrd, endDateOrd)
    temp =  AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()
    return temp

def get_MBExpenses_Range_Golfer(startDateOrd, endDateOrd, golferID):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''select * from MBExpenses where expenseDate >= {0} and expenseDate <= {1} and golferID = {2} and deleted = 'N' order by golferID, expenseDate'''.format(startDateOrd, endDateOrd, golferID)
    temp =  AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()
    return temp

def insert_MBExpenses(expenseDate, expenseItem, expenseAmount, currency,
                           USExchangeRate, expenseAdjusted, golferID, updaterID):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''
    insert into MBExpenses (expenseDate, expenseItem, expenseAmount, currency,
                            USExchangeRate, expenseAdjusted, golferID, deleted,
                            lastModified, updaterID)
                values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'''
    AppCRUD.cursor.execute(query, ( expenseDate, expenseItem, expenseAmount, currency,
                                    USExchangeRate, expenseAdjusted, golferID,
                                   'N', convertDateStringToOrdinal(todayDate), updaterID))
    AppCRUD.closeDB()

def update_MBExpenses(uniqueID, expenseDate, expenseItem, expenseAmount, currency,
                      USExchangeRate, expenseAdjusted, golferID, updaterID):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''
    update MBExpenses 
    set expenseDate=?, expenseItem=?, expenseAmount=?, currency=?,
        USExchangeRate=?, expenseAdjusted=?, golferID=?, updaterID=?,
        lastModified=?
    where uniqueID == ?'''
    AppCRUD.cursor.execute(query, (expenseDate, expenseItem, expenseAmount, currency,
                                   USExchangeRate, expenseAdjusted, golferID, updaterID,
                                   convertDateStringToOrdinal(todayDate), uniqueID))
    AppCRUD.closeDB()

def delete_MBExpense(uniqueID):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''
    delete from MBExpenses
    where uniqueID = ?'''
    AppCRUD.cursor.execute(query,(uniqueID,))
    AppCRUD.closeDB()

def exportCSV_MBExpenses(fileName):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''select * from MBExpenses'''
    data = AppCRUD.cursor.execute(query).fetchall()
    file = AppDefaultLocation + '/' + BackupDirDefaultName +'/'+ fileName
    with open(file, 'w', newline='', encoding='ISO-8859-1') as fout:
        for i in range(len(data)):
            csv.writer(fout).writerow(data[i])
    AppCRUD.closeDB()
    return len(data)
            
def importCSV_MBExpenses(fileName):
    AppCRUD.initDB(getDBLocationFullName())
    file = AppDefaultLocation + '/' + BackupDirDefaultName + '/' + fileName
    with open(file, 'r', encoding='ISO-8859-1') as fin:
        reader = csv.reader(fin)
        for row in reader:
            # row[5] must be a valid courseID
            query = '''
            insert into MBExpenses (uniqueID, expenseDate, expenseItem, expenseAmount, currency,
                                    USExchangeRate, expenseAdjusted, golferID,
                                    deleted, lastModified,updaterID)
            values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'''
            AppCRUD.cursor.execute(query, (row[0], row[1], row[2], row[3], row[4],
                                           row[5], row[6], row[7], row[8], row[9], row[10]))
    AppCRUD.closeDB()
    return len(get_MBExpenses())

def importExpensesCSV_MyGolf2015(fileName):
    AppCRUD.initDB(getDBLocationFullName())
    with open(fileName, newline='', encoding='ISO-8859-1') as fin:
        reader = csv.reader(fin)
        for row in reader:
            query = '''
               insert into MBExpenses (uniqueID,expenseDate,expenseItem,expenseAmount,
                                       currency,USExchangeRate,expenseAdjusted,golferID,
                                       deleted, lastModified,updaterID)
                           values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'''
            AppCRUD.cursor.execute(query, (row[0], row[1], row[2], row[3], row[4],
                                           row[5], row[6], row[7], 'N', convertDateStringToOrdinal(todayDate), '1'))
    AppCRUD.closeDB()
    return len(get_MBExpenses())


#cursor.execute('''CREATE TABLE MBExpenses(
#   uniqueID            INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
#   expenseDate          INTEGER    NOT NULL,
#   expenseItem          CHAR(100)  NOT NULL,
#   expenseAmount        REAL       NOT NULL,
#   currency             CHAR(5)    NOT NULL,
#   USExchangeRate       REAL,
#   expenseAdjusted      REAL,
#   golferID             INTEGER    NOT NULL,
#   deleted              CHAR(1)    NOT NULL,
#   lastModified         INTEGER,
#   updaterID            INTEGER,
#   FOREIGN KEY(golferID) REFERENCES Golfers(golferID) ON DELETE CASCADE
#   );''')

