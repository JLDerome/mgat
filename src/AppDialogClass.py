import tkinter.ttk as ttk
import tkinter.tix as Tix

from tkinter import *
import tkinter.filedialog as Fd
import random, shutil

from AppConstants import *
from AppClasses import *

import os
#from ECEShopApp.ECEAppConstants import fontHugeB
from AppProc import *



from PIL import Image
from PIL.Image import NORMAL

class AppDialogYesNo(Toplevel):
    def __init__(self, parent, title = None, data = None):
        Toplevel.__init__(self, parent)
        self.transient(parent)
        
        self.data = data  # Possible Data to display
        if title:
            self.title(title)
            self.aTitle = title
            
        self.row = 0
        self.column = 0
        self.rowconfigure(self.row, weight = 0)
        self.columnconfigure(self.column, weight = 0)

        self.parent = parent

        self.result = False

        body = AppFrame(self,1)
        body.noBorder()
        self.initial_focus = self.body(body)
        body.grid(row=self.row, column=self.column, sticky=N+S+E+W)

        self.buttonbox()

        try:
            self.grab_set()
        except:
            pass

        if not self.initial_focus:
            self.initial_focus = self

        self.protocol("WM_DELETE_WINDOW", self.no)

        self.geometry("+%d+%d" % (parent.winfo_rootx()+50,
                                  parent.winfo_rooty()+50))

        self.initial_focus.focus_set()

        self.wait_window(self)

    #
    # construction hooks
    def addRow(self):
        self.row = self.row + 1
        self.rowconfigure(self.row, weight = 0)

    def addColumn(self):
        self.column = self.column + 1
        self.columnconfigure(self.column, weight = 0)

    def body(self, master):
        # create dialog body.  return widget that should have
        # initial focus.  this method should be overridden
        pass

    def buttonbox(self):
        # add standard button box. override if you don't want the
        # standard buttons

        box = AppFrame(self, 2)
        box.noBorder()

        w = ttk.Button(box, text="Yes", style='CommandButton.TButton', command=self.yes, default=ACTIVE)
        w.grid(row=0, column = 0, sticky = E+N)
        w = ttk.Button(box, text="No", style='CommandButton.TButton', command=self.no)
        w.grid(row=0, column = 1, sticky=W+N)

        self.bind("<Return>", self.yes)
        self.bind("<Escape>", self.no)

        self.addRow()
        box.grid(row=self.row, column=self.column, sticky=N+S+E+W)

    #
    # standard button semantics

    def yes(self, event=None):

        if not self.validate():
            self.initial_focus.focus_set() # put focus back
            return

        self.withdraw()
        self.update_idletasks()

        self.apply()

        self.no()

    def no(self, event=None):

        # put focus back to the parent window
        self.parent.focus_set()
        self.destroy()

    #
    # command hooks

    def validate(self):
        #
        #  If any form validation is required, put them here
        #

        return 1 # override

    def apply(self):

        pass # override
    
class AppDialogOk(Toplevel):

    def __init__(self, parent, title = None, data = None, image = None, removeTransient = False):

        Toplevel.__init__(self, parent)
        if removeTransient == False:
            self.transient(parent)

        self.data = data  # Possible Data to display
        self.aTitle = title
        if title:
            self.title(title)
            
        self.row = 0
        self.column = 0
        self.rowconfigure(self.row, weight = 1)
        self.columnconfigure(self.column, weight = 1)

        self.parent = parent

        self.result = False
        
        self.image = image

        body = AppFrame(self,1)
        # body.noBorder()
        self.initial_focus = self.body(body)
        body.grid(row=self.row, column=self.column, sticky=N+S+E+W)
        body.stretchCurrentRow()


        self.buttonbox()

        try:
            self.grab_set()
        except:
            pass

        if not self.initial_focus:
            self.initial_focus = self

        self.protocol("WM_DELETE_WINDOW", self.ok)

        self.geometry("+%d+%d" % (parent.winfo_rootx()+50,
                                  parent.winfo_rooty()+50))

        self.initial_focus.focus_set()

        self.wait_window(self)

    #
    # construction hooks
    def addRow(self):
        self.row = self.row + 1
        self.rowconfigure(self.row, weight = 0)

    def addColumn(self):
        self.column = self.column + 1
        self.columnconfigure(self.column, weight = 0)

    def body(self, master):
        # create dialog body.  return widget that should have
        # initial focus.  this method should be overridden

        pass

    def buttonbox(self):
        # add standard button box. override if you don't want the
        # standard buttons

        box = AppFrame(self, 2)
        box.noBorder()

        w = ttk.Button(box, text="Ok", style='CommandButton.TButton', command=self.ok, default=ACTIVE)
        w.grid(row=0, column = 0, sticky = E+N)

        self.bind("<Return>", self.ok)
        self.bind("<Escape>", self.ok)

        self.addRow()
        box.grid(row=self.row, column=self.column, sticky=N+S+E+W)

    #
    # standard button semantics

    def ok(self, event=None):

        self.result = True
        self.withdraw()
        self.update_idletasks()
        self.parent.focus_set()
        self.destroy()
        
class AppDisplayUnderConstruction(AppDialogOk):

    def body(self, master):

        bodyFrame = AppBorderFrame(master, 2)
        bodyFrame.grid(row=0, column=0, sticky=N+S+E+W)
        
        PICFrame = AppFrame(bodyFrame, 1)
        PICFrame.noBorder()
        PICFrame.grid(row=bodyFrame.row, column=bodyFrame.column, sticky=N+W+E+S)
        appUnderConstructionPicWidth = 120
        appUnderConstructionPicHeight = 138

        Picture = AppPictureFrame(PICFrame, underConstructionImage, AppmediaImagesPath,
                                  appUnderConstructionPicWidth, appUnderConstructionPicWidth, None)
        Picture.grid(row=PICFrame.row, column=PICFrame.column, sticky=N+W+E+S)
        Picture.disable()
        

        bodyFrame.addColumn()
        messageFrame = AppFrame(bodyFrame, 1)
        messageFrame.noBorder()
        messageFrame.grid(row=bodyFrame.row, column=bodyFrame.column, sticky=N+W+E+S)

        aLabel =  AppLineSectionTitle(messageFrame, 'Function Under Construction')
        aLabel.grid(row=messageFrame.row, column=messageFrame.column, columnspan=messageFrame.columnTotal, sticky=N)
        
        messageFrame.addRow()
        aLabel =  Label(messageFrame, text='We are sorry that this portion of the application\nis not available yet. Stay tune for future upgrades.', font=fontBig)
        aLabel.grid(row=messageFrame.row, column=messageFrame.column, columnspan=messageFrame.columnTotal, sticky=N)


class AppDisplayRecordNotFound(AppDialogOk):
    def body(self, master):
        bodyFrame = AppBorderFrame(master, 2)
        bodyFrame.grid(row=0, column=0, sticky=N + S + E + W)

        PICFrame = AppFrame(bodyFrame, 1)
        PICFrame.noBorder()
        PICFrame.grid(row=bodyFrame.row, column=bodyFrame.column, sticky=N + W + E + S)

        Picture = AppPictureFrame(PICFrame, warningImage, AppmediaImagesPath,
                                  eceLogoPicWidth, eceLogoPicHeight, None)
        Picture.grid(row=PICFrame.row, column=PICFrame.column, sticky=N + W + E + S)
        Picture.disable()

        bodyFrame.addColumn()
        messageFrame = AppFrame(bodyFrame, 1)
        messageFrame.noBorder()
        messageFrame.grid(row=bodyFrame.row, column=bodyFrame.column, sticky=N + W + E + S)

        aLabel = AppLineSectionTitle(messageFrame, self.aTitle)
        aLabel.grid(row=messageFrame.row, column=messageFrame.column, columnspan=messageFrame.columnTotal, sticky=N)

        messageFrame.addRow()
        aLabel = Label(messageFrame, text=self.data, font=fontBig)
        aLabel.grid(row=messageFrame.row, column=messageFrame.column, columnspan=messageFrame.columnTotal, sticky=N)
        
class AppDisplayAbout(AppDialogOk):

    def body(self, master):

        bodyFrame = AppBorderFrame(master, 2)
        bodyFrame.grid(row=0, column=0, sticky=N+S+E+W)
        
        PICFrame = AppFrame(bodyFrame, 1)
        PICFrame.noBorder()
        PICFrame.grid(row=bodyFrame.row, column=bodyFrame.column, sticky=N+W+E+S)

        Picture = AppPictureFrame(PICFrame, AppMainLogo, AppmediaImagesPath,
                                            appDialogPicWidth, appDialogPicHeight, None)
        Picture.grid(row=PICFrame.row, column=PICFrame.column, sticky=N+W+E+S)
        Picture.disable()
        

        bodyFrame.addColumn()
        messageFrame = AppFrame(bodyFrame, 1)
        messageFrame.noBorder()
        messageFrame.grid(row=bodyFrame.row, column=bodyFrame.column, sticky=N+W+E+S)

        aLabel =  AppLineSectionTitle(messageFrame, 'My Myrtle Beach Golf App')
        aLabel.grid(row=messageFrame.row, column=messageFrame.column, columnspan=messageFrame.columnTotal, sticky=N)
        
        messageFrame.addRow()
        aMsg = 'Golf Application to track and maintain golf statistics during\nyour Myrtle Beach travels. In addition, it can ' \
               'keep\ntrack of your expenses. Build for 4 golfers.\n\n Author: Jean-Luc Derome\n' \
               'Version {0} (2016)'.format(AppVersionNumber)
        aLabel =  Label(messageFrame, text=aMsg, font=fontBig)
        aLabel.grid(row=messageFrame.row, column=messageFrame.column, columnspan=messageFrame.columnTotal, sticky=N)


class AppRecordDuplication(AppDialogOk):
    def body(self, master):
        bodyFrame = AppBorderFrame(master, 2)
        bodyFrame.grid(row=0, column=0, sticky=N + S + E + W)

        PICFrame = AppFrame(bodyFrame, 1)
        PICFrame.noBorder()
        PICFrame.grid(row=bodyFrame.row, column=bodyFrame.column, sticky=N + W + E + S)

        Picture = AppPictureFrame(PICFrame, errorimage, AppmediaImagesPath,
                                  duplicationPicWidth, duplicationPicHeight, None)
        Picture.grid(row=PICFrame.row, column=PICFrame.column, sticky=N + W + E + S)
        Picture.disable()

        bodyFrame.addColumn()
        messageFrame = AppFrame(bodyFrame, 1)
        messageFrame.noBorder()
        messageFrame.grid(row=bodyFrame.row, column=bodyFrame.column, sticky=N + W + E + S)

        aLabel = AppLineSectionTitle(messageFrame, self.aTitle)
        aLabel.grid(row=messageFrame.row, column=messageFrame.column, columnspan=messageFrame.columnTotal, sticky=N)

        messageFrame.addRow()
        aLabel = Label(messageFrame,
                       text='{0}'.format(self.data),
                       font=fontBig)
        aLabel.grid(row=messageFrame.row, column=messageFrame.column, columnspan=messageFrame.columnTotal, sticky=N)

class AppDisplayErrorMessage(AppDialogOk):
    def body(self, master):
        bodyFrame = AppBorderFrame(master, 2)
        bodyFrame.grid(row=0, column=0, sticky=N + S + E + W)

        PICFrame = AppFrame(bodyFrame, 1)
        PICFrame.noBorder()
        PICFrame.grid(row=bodyFrame.row, column=bodyFrame.column, sticky=N + W + E + S)

        Picture = AppPictureFrame(PICFrame, errorimage, AppmediaImagesPath,
                                  duplicationPicWidth, duplicationPicHeight, None)
        Picture.grid(row=PICFrame.row, column=PICFrame.column, sticky=N + W + E + S)
        Picture.disable()

        bodyFrame.addColumn()
        messageFrame = AppFrame(bodyFrame, 1)
        messageFrame.noBorder()
        messageFrame.grid(row=bodyFrame.row, column=bodyFrame.column, sticky=N + W + E + S)

        aLabel = AppLineSectionTitle(messageFrame, self.aTitle)
        aLabel.grid(row=messageFrame.row, column=messageFrame.column, columnspan=messageFrame.columnTotal, sticky=N)

        messageFrame.addRow()
        aLabel = Label(messageFrame,
                       text='{0}'.format(self.data),
                       font=fontBig)
        aLabel.grid(row=messageFrame.row, column=messageFrame.column, columnspan=messageFrame.columnTotal, sticky=N)

class AppDisplayMessage(AppDialogOk):
    def body(self, master):
        bodyFrame = AppBorderFrame(master, 2)
        bodyFrame.grid(row=0, column=0, sticky=N + S + E + W)

        PICFrame = AppFrame(bodyFrame, 1)
        PICFrame.noBorder()
        PICFrame.grid(row=bodyFrame.row, column=bodyFrame.column, sticky=N + W + E + S)

        Picture = AppPictureFrame(PICFrame, AppMainLogo, AppmediaImagesPath,
                                  duplicationPicWidth, duplicationPicHeight, None)
        Picture.grid(row=PICFrame.row, column=PICFrame.column, sticky=N + W + E + S)
        Picture.disable()

        bodyFrame.addColumn()
        messageFrame = AppFrame(bodyFrame, 1)
        messageFrame.noBorder()
        messageFrame.grid(row=bodyFrame.row, column=bodyFrame.column, sticky=N + W + E + S)

        aLabel = AppLineSectionTitle(messageFrame, self.aTitle)
        aLabel.grid(row=messageFrame.row, column=messageFrame.column, columnspan=messageFrame.columnTotal, sticky=N)

        messageFrame.addRow()
        aLabel = Label(messageFrame,
                       text='{0}'.format(self.data),
                       font=fontBig)
        aLabel.grid(row=messageFrame.row, column=messageFrame.column, columnspan=messageFrame.columnTotal, sticky=N)


class AppRecordDuplicationV2(AppDialogOk):

    def body(self, master):

        bodyFrame = AppBorderFrame(master, 2)
        bodyFrame.grid(row=0, column=0, sticky=N+S+E+W)
        
        PICFrame = AppFrame(bodyFrame, 1)
        PICFrame.noBorder()
        PICFrame.grid(row=bodyFrame.row, column=bodyFrame.column, sticky=N+W+E+S)

        Picture = AppPictureFrame(PICFrame, errorimage, AppmediaImagesPath, 
                                               duplicationPicWidth,  duplicationPicHeight, None)
        Picture.grid(row=PICFrame.row, column=PICFrame.column, sticky=N+W+E+S)
        Picture.disable()
        

        bodyFrame.addColumn()
        messageFrame = AppFrame(bodyFrame, 1)
        messageFrame.noBorder()
        messageFrame.grid(row=bodyFrame.row, column=bodyFrame.column, sticky=N+W+E+S)

        aLabel =  AppLineSectionTitle(messageFrame, 'Record Duplication Error')
        aLabel.grid(row=messageFrame.row, column=messageFrame.column, columnspan=messageFrame.columnTotal, sticky=N)
        
        messageFrame.addRow()
        aLabel =  Label(messageFrame, text='Record {0} can not be added as\n it already exist in the database.'.format(self.data), font=fontBig)
        aLabel.grid(row=messageFrame.row, column=messageFrame.column, columnspan=messageFrame.columnTotal, sticky=N)  
              
class AppPossibleRecordDuplication(AppDialogYesNo):

    def body(self, master):

        bodyFrame = AppBorderFrame(master, 2)
        
        questionPICFrame = AppFrame(bodyFrame, 1)
        questionPICFrame.noBorder()
        questionPICFrame.grid(row=bodyFrame.row, column=bodyFrame.column, sticky=N+W+E+S)

        questionPicture = AppPictureFrame(questionPICFrame,  warningImage, AppmediaImagesPath, 
                                               warningPicWidth, warningPicHeight, None)
        questionPicture.grid(row=questionPICFrame.row, column=questionPICFrame.column, sticky=N+W+E+S)
        questionPicture.disable()
        
        bodyFrame.addColumn()
        messageFrame = AppFrame(bodyFrame, 3)
        messageFrame.noBorder()
        messageFrame.grid(row=bodyFrame.row, column=bodyFrame.column, sticky=N+W+E+S)

        aLabel =  Label(messageFrame, text="   ", font=fontBig)
        aLabel.grid(row=messageFrame.row, column=messageFrame.column, sticky=N)
        
        messageFrame.addColumn()
        aLabel =  Label(messageFrame, text="{0}".format(self.aTitle), font=fontHugeB)
        aLabel.grid(row=messageFrame.row, column=messageFrame.column, sticky=N)
        bodyFrame.grid(row=0, column=0, columnspan = messageFrame.columnTotal, sticky=N)
        
        messageFrame.addRow()
        messageFrame.resetColumn()
        aLabel =  Label(messageFrame, text="   ", font=fontBig)
        aLabel.grid(row=messageFrame.row, column=messageFrame.column, sticky=N+W)
        
        messageFrame.addColumn()
        aLabel =  Label(messageFrame, text="The following records are possible matches (%): ", font=fontBig)
        aLabel.grid(row=messageFrame.row, column=messageFrame.column, sticky=N+W)

        for i in range(len(self.data)):           
            messageFrame.addRow()
            messageFrame.resetColumn()
            aLabel =  Label(messageFrame, text="   ", font=fontBig)
            aLabel.grid(row=messageFrame.row, column=messageFrame.column, sticky=N+W)
            
            messageFrame.addColumn()
            aLabel =  Label(messageFrame, text="       {2}. {0} ({1}%)".format(self.data[i][1],self.data[i][2], i+1), font=fontBig)
            aLabel.grid(row=messageFrame.row, column=messageFrame.column, sticky=N+W)
            bodyFrame.grid(row=0, column=0, sticky=N)
    
            messageFrame.addColumn()
            aLabel =  Label(messageFrame, text="   ", font=fontBig)
            aLabel.grid(row=messageFrame.row, column=messageFrame.column, sticky=N+W)
              
        messageFrame.addRow()
        messageFrame.resetColumn()
        aLabel =  Label(messageFrame, text="   ", font=fontBig)
        aLabel.grid(row=messageFrame.row, column=messageFrame.column, sticky=N+W)
        
        messageFrame.addColumn()
        aLabel =  Label(messageFrame, text="Do you wish to continue the operation?", font=fontBig)
        aLabel.grid(row=messageFrame.row, column=messageFrame.column, sticky=N+W)
        
    def apply(self):
        self.result = True

class AppQuestionRequest(AppDialogYesNo):

    def body(self, master):

        bodyFrame = AppBorderFrame(master, 2)
        
        questionPICFrame = AppFrame(bodyFrame, 1)
        questionPICFrame.noBorder()
        questionPICFrame.grid(row=bodyFrame.row, column=bodyFrame.column, sticky=N+W+E+S)

        questionPicture = AppPictureFrame(questionPICFrame, questionImage, AppmediaImagesPath, 
                                               questionPicWidth, questionPicHeight, None)
        questionPicture.grid(row=questionPICFrame.row, column=questionPICFrame.column, sticky=N+W+E+S)
        questionPicture.disable()
        
        bodyFrame.addColumn()
        messageFrame = AppFrame(bodyFrame, 3)
        messageFrame.noBorder()
        messageFrame.grid(row=bodyFrame.row, column=bodyFrame.column, sticky=N+W+E+S)

        aLabel =  Label(messageFrame, text="   ", font=fontBig)
        aLabel.grid(row=messageFrame.row, column=messageFrame.column, sticky=N)
        
        messageFrame.addColumn()
        aLabel =  Label(messageFrame, text="{0}".format(self.data), font=fontBig)
        aLabel.grid(row=messageFrame.row, column=messageFrame.column, sticky=N)
        bodyFrame.grid(row=0, column=0, sticky=N+S+E+W)

        messageFrame.addColumn()
        aLabel =  Label(messageFrame, text="   ", font=fontBig)
        aLabel.grid(row=messageFrame.row, column=messageFrame.column, sticky=N)
              
    def apply(self):
        self.result = True
                   
class AppDuplicatePerson(AppDialogYesNo):

    def body(self, master):

        bodyFrame = AppBorderFrame(master, 2)
        
        warningPICFrame = AppFrame(bodyFrame, 1)
        warningPICFrame.noBorder()
        warningPICFrame.grid(row=bodyFrame.row, column=bodyFrame.column, sticky=N+W+E+S)

        warningPicture = AppPictureFrame(warningPICFrame, warningImage, AppmediaImagesPath, 
                                               warningPicWidth, warningPicHeight, None)
        warningPicture.grid(row=warningPICFrame.row, column=warningPICFrame.column, sticky=N+W+E+S)
        warningPicture.disable()
        
        bodyFrame.addColumn()
        messageFrame = AppFrame(bodyFrame, 1)
        messageFrame.noBorder()
        messageFrame.grid(row=bodyFrame.row, column=bodyFrame.column, sticky=N+W+E+S)

        aLabel =  AppLineSectionTitle(messageFrame, 'Possible Record Duplication')
        aLabel.grid(row=messageFrame.row, column=messageFrame.column, columnspan=messageFrame.columnTotal, sticky=N)
        
        messageFrame.addRow()
        aLabel =  Label(messageFrame, text='The following user(s) with the same last name aready exist in your database:', font=fontBig)
        aLabel.grid(row=messageFrame.row, column=messageFrame.column, columnspan=messageFrame.columnTotal, sticky=N)
       
        for i in range(len(self.data)):
            messageFrame.addRow()
            deptName = "FINDING"
            aUser="{0} {1} {2} ({3}) from {4}".format(self.data[i][0],self.data[i][1],self.data[i][2],self.data[i][3], deptName)
            Label(messageFrame, text='{0}. {1} '.format(i+1, aUser), font=fontBig).grid(row=messageFrame.row, column=messageFrame.column, columnspan=messageFrame.columnTotal, sticky=N)     
            
        messageFrame.addRow()
        aLabel =  Label(messageFrame, text='Do you wish to proceed with the Add new Personnel?', font=fontBig)
        aLabel.grid(row=messageFrame.row, column=messageFrame.column, columnspan=messageFrame.columnTotal, sticky=N)
        bodyFrame.grid(row=0, column=0, sticky=N+S+E+W)
       
    def apply(self):
        self.result = True
