from popUpWindowClass import popUpWindowClass
from AppClasses import *
from AppConstants import *
from AppProc import convertDateStringToOrdinal, updateMyAppConstant, exportAppConfig
from AppProc import getAYearFromOrdinal
import AppCRUDGolfers as CRUDGolfers
import AppCRUDExpenses as CRUDExpenses
import AppCRUDLoginAccess as CRUDLoginAccess
import tkinter.messagebox as Mb

class addEditTripConfigurationPopUp(popUpWindowClass):
    def __init__(self, aWindow, aCloseCommand, loginData):
        #
        #  loginInfo format: Updater Full Name, Login ID, updaterID
        #
        self.windowTitle = 'Myrtle Beach Trip Configuration'
        popUpWindowClass.__init__(self, aWindow, aCloseCommand, self.windowTitle, loginData, None)
        self.tableName = 'TripConfiguration'
        self.aCloseCommand = aCloseCommand
        self.aWindow = aWindow
        self.loginData = loginData

        self.configDataRead = []
        self.golferComboBoxData = CRUDGolfers.getGolferCB()
        self.golferDictionary = {}
        self.reverseGolferDictionary = {}
        for i in range(len(self.golferComboBoxData)):
            golfer_full = '''{0} {1}'''.format(self.golferComboBoxData[i][1], self.golferComboBoxData[i][2])
            self.golferDictionary.update({golfer_full: self.golferComboBoxData[i][0]})
            self.reverseGolferDictionary.update({self.golferComboBoxData[i][0]: golfer_full})
        self.bigSpender = None
        #
        #  loginInfo format: Updater Full Name, access_Level, loginID
        #
        self.updaterFullname = loginData[0]
        self.updaterLoginID = self.loginData[2]
        self.displayCurUser.setUserName(self.updaterFullname)


        #######################################################################################
        # Inherited all from tableCommanWindowClass
        #
        #  This class intents to change the mainArea only.  Everything else must remain common
        #
        self.mainAreaRowCount = 0
        self.mainAreaColumnCount = 0
        self._reDisplayRecordFrame()
        self.mainArea.rowconfigure(self.mainAreaRowCount, weight=1)
        self.setDefaultState()

    def setDefaultState(self):
        self.curOp='default'
        self.fieldsDisable()
        self.displayCurFunction.setDefaultMode()
        self.submitButton.disable()
        self.editButton.enable()
        self.cancelButton.disable()
        self.loadCurrentConfiguration()
        self.lastUpdate.load(self.configDataRead[0][11],int(self.configDataRead[0][10]))
        self.dirty = False

    def loadCurrentConfiguration(self):
        self.configDataRead = updateMyAppConstant(AppConfigFile)
        self.selectedCurrency.load(self.configDataRead[0][8])
        self.selectedRate.load(self.configDataRead[0][7])
        self.selectedEndDate.load(convertOrdinaltoString(self.configDataRead[0][1]))
        self.selectedStartDate.load(convertOrdinaltoString(self.configDataRead[0][0]))
        self.selectedGolfer1.load(self.golferCBData.getName(int(self.configDataRead[0][2])))
        self.selectedGolfer2.load(self.golferCBData.getName(int(self.configDataRead[0][3])))
        self.selectedGolfer3.load(self.golferCBData.getName(int(self.configDataRead[0][4])))
        self.selectedGolfer4.load(self.golferCBData.getName(int(self.configDataRead[0][5])))
        self.selectedMarker.load(self.golferCBData.getName(int(self.configDataRead[0][6])))
        self.selectedSpender.load(self.golferCBData.getName(int(self.configDataRead[0][9])))
        self.bigSpender = self.reverseGolferDictionary[int(self.configDataRead[0][9])]

    def add(self, *args):
        pass
        self.dirty = False

    def edit(self, *args):
        pass
        self.curOp='edit'
        self.displayCurFunction.setEditMode()
        self.editButton.disable()
        self.submitButton.enable()
        self.cancelButton.enable()
        self.fieldsEnable()
        self.dirty = False

    def cancel(self, *args):
        pass
        ans = False
        if self.dirty == True:
            aMsg = "The record currently in {0} mode has not been saved.\n" \
                   "Cancelling now would result in lost of data.\n" \
                   "Do you wish to continue with the cancel current command?".format(self.curOp)
            ans = Mb.askyesno('Cancel Current Command', aMsg)
            if ans == True:
                if self.validateRequiredFields() == True:
                    self.setDefaultState()
        else:
            if self.validateRequiredFields() == True:
                self.setDefaultState()

    def _submitForm(self, *args):
        if self.validateRequiredFields() == True:
            self.save()
        # Must be defined in the popUp window.

    def _clearForm(self, *args):
        self.fieldsClear()
        # Must be defined in the popUp window.

    def tableSpecificMenu(self):
        pass
        '''
        self.viewsmenu.add_command
        self.viewsmenu.add_command(label="Active Golfers", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                   command=lambda viewID='Active Golfers': self.basicViewsList(viewID))

        sortsmenu = Menu(self.menubar, foreground=AppDefaultForeground, font=AppDefaultMenuFont, tearoff=0)
        sortsmenu.add_command(label="Default", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Default': self.sortingList(sortID))
        sortsmenu.add_command(label="Last Name", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Last Name': self.sortingList(sortID))
        sortsmenu.add_command(label="First Name", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='First Name': self.sortingList(sortID))
        sortsmenu.add_command(label="Birthday", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Birthday': self.sortingList(sortID))
        #
        # Next Command actually puts the menu up
        #
        self.menubar.add_cascade(label="Sorts", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                 menu=sortsmenu)
        '''

    def _exitForm(self, *args):
        if self.dirty == True:
            aMsg = "The record currently in {0} mode has not been saved.\n" \
                   "Exiting now would result in lost of data.\n" \
                   "Do you wish to continue with the exit command?".format(self.curOp)
            ans = Mb.askyesno('Restore My Golf App 2016 Database', aMsg)
            if ans == True:
                self.aCloseCommand()
        else:
            self.aCloseCommand()

    def sort(self):
        pass
        '''
        if self.curOp == 'default':
            currentID = self.recList[self.curRecNumV.get()][0]
            if self.sortID.get() == 'Default':
                self.recList = sorted(self.recList, key=operator.itemgetter(2,1))
            elif self.sortID.get() == 'Last Name':
                self.recList.sort(key=lambda row: row[2])
            elif self.sortID.get() == 'First Name':
                self.recList.sort(key=lambda row: row[1])
            elif self.sortID.get() == 'Birthday':
                self.recList.sort(key=lambda row: row[4])
            else:
                self.recList.sort(key=lambda row: row[0])
            newIdx = getIndex(currentID, self.recList)
            self.curRecNumV.set(newIdx)
            self.displayRec()
        else:
            aMsg = "ERROR: Invalid request. Must be executed in default state only."
            self.myMsgBar.newMessage('error', aMsg)
        '''

    def _descriptionEnteredAction(self):
        pass

    def _buildWindowsFields(self, aFrame):
        #        headerList1 = ('BASIC','VALUE','VALUED','MULT','TOLE','SIZE','DESCR','NSN1','NSN2','NSN3','NSN4')
        colTotal = 2
        self.fieldsFrame = AppFieldsFrame(aFrame, colTotal)
        self.fieldsFrame.grid(row=0, column=0, sticky=N + S + E + W)
        self.fieldsFrame.stretchCurrentRow()

        ########################################################### Main Identification Section
        self.dateFrame = AppBorderFrame(self.fieldsFrame, 1)
        self.dateFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column,
                            sticky=E + W + N + S)
        self.dateFrame.addTitle("Trip Dates")

        self.tripDateFrame = AppColumnAlignFieldFrame(self.dateFrame)
        self.tripDateFrame.noBorder()
        self.tripDateFrame.grid(row=self.dateFrame.row, column=self.dateFrame.column,
                                sticky=E + W + N + S)

        self.selectedStartDate = AppFieldEntryDate(self.tripDateFrame, None, None, appDateWidth,
                                                     self._dateEnteredAction, self.myMsgBar, self._aDirtyMethod)
        self.tripDateFrame.addNewField('Trip Start Date', self.selectedStartDate)

        self.selectedEndDate = AppFieldEntryDate(self.tripDateFrame, None, None, appDateWidth,
                                                     self._dateEnteredAction, self.myMsgBar, self._aDirtyMethod)
        self.tripDateFrame.addNewField('Trip End Date', self.selectedEndDate)

        self.fieldsFrame.addColumn()

        self.golferCBData = AppCBList(CRUDGolfers.getGolfersDictionaryInfo())

        self.golfersFrame = AppBorderFrame(self.fieldsFrame, 1)
        self.golfersFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column,
                               sticky=E + W + N + S)
        self.golfersFrame.addTitle("Golfers Participating in Trip")

        self.tripGolferFrame = AppColumnAlignFieldFrame(self.golfersFrame)
        self.tripGolferFrame.noBorder()
        self.tripGolferFrame.grid(row=self.golfersFrame.row, column=self.golfersFrame.column,
                                sticky=E + W + N + S)

        self.selectedGolfer1 = AppSearchLB(self.tripGolferFrame, self, None, None,
                                           self.golferCBData.getList(), AppFullNameWidth, None, self.myMsgBar, self._aDirtyMethod, None)
        self.tripGolferFrame.addNewField('Golfer #1', self.selectedGolfer1)
        self.selectedGolfer2 = AppSearchLB(self.tripGolferFrame, self, None, None,
                                           self.golferCBData.getList(), AppFullNameWidth, None, self.myMsgBar, self._aDirtyMethod, None)


        self.tripGolferFrame.addNewField('Golfer #2', self.selectedGolfer2)
        self.selectedGolfer3 = AppSearchLB(self.tripGolferFrame, self, None, None,
                                           self.golferCBData.getList(), AppFullNameWidth, None, self.myMsgBar, self._aDirtyMethod, None)
        self.tripGolferFrame.addNewField('Golfer #3', self.selectedGolfer3)
        self.selectedGolfer4 = AppSearchLB(self.tripGolferFrame, self, None, None,
                                           self.golferCBData.getList(), AppFullNameWidth, None, self.myMsgBar, self._aDirtyMethod, None)
        self.tripGolferFrame.addNewField('Golfer #4', self.selectedGolfer4)

        self.fieldsFrame.addRow()
        self.fieldsFrame.resetColumn()
        self.fieldsFrame.stretchCurrentRow()
        self.markerFrame = AppBorderFrame(self.fieldsFrame, 1)
        self.markerFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column,
                               sticky=E + W + N + S)
        self.markerFrame.addTitle("Official Game Marker during Trip")

        self.tripMarkerFrame = AppColumnAlignFieldFrame(self.markerFrame)
        self.tripMarkerFrame.noBorder()
        self.tripMarkerFrame.grid(row=self.markerFrame.row, column=self.markerFrame.column,
                                sticky=E + W + N + S)
        self.selectedMarker = AppSearchLB(self.tripMarkerFrame, self, None, None,
                                          self.golferCBData.getList(), AppFullNameWidth, None, self.myMsgBar, self._aDirtyMethod, None)
        self.tripMarkerFrame.addNewField('Official Trip Marker', self.selectedMarker)

        self.fieldsFrame.addColumn()
        self.expenseFrame = AppBorderFrame(self.fieldsFrame, 1)
        self.expenseFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column,
                               sticky=E + W + N + S)
        self.expenseFrame.addTitle("Expenses Related Configuration")

        self.tripExpenseFrame = AppColumnAlignFieldFrame(self.expenseFrame)
        self.tripExpenseFrame.noBorder()
        self.tripExpenseFrame.grid(row=self.expenseFrame.row, column=self.expenseFrame.column,
                                sticky=E + W + N + S)

        self.selectedSpender = AppSearchLB(self.tripExpenseFrame, self, None, None,
                                          self.golferCBData.getList(), AppFullNameWidth, None, self.myMsgBar, self._aDirtyMethod, None)
        self.tripExpenseFrame.addNewField('Main Spending Golfer', self.selectedSpender)

        self.selectedCurrency = AppSearchLB(self.tripExpenseFrame, self, None, None, AppCurrencyList,
                                                   AppCurrencyWidth, None, self.myMsgBar, self._aDirtyMethod, None)
        self.tripExpenseFrame.addNewField('Expense Currency', self.selectedCurrency)

        self.selectedRate = AppFieldEntryFloat(self.tripExpenseFrame, None, None, AppAmountWidth, self._aDirtyMethod, None)
        self.selectedRate.addValidationAmount(5,7, self.myMsgBar)
        self.tripExpenseFrame.addNewField('Exchange Rate', self.selectedRate)


        ########################################################### Footer Section
        self.fieldsFrame.addRow()
        self.lastUpdate = AppLastUpdateFormat(self.fieldsFrame)
        self.lastUpdate.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, columnspan=colTotal,
                             sticky=E + N + S)

        self.setRequiredFields()
        self.fieldsDisable()
        self.clearButton.destroy()  # Not required for this application

    def addWindowSpecificCommand(self):


        self.editButton = AppButton(self.commandFrame, self.theCommandEditImage, self.theCommandEditImageGrey, AppBorderWidthList,
                                    self.edit)
        self.commandFrame.addNewCommandButton(self.editButton)
        self.editButton.addToolTip("Edit Form")

        self.cancelButton = AppButton(self.commandFrame, self.theCommandCancelImage, self.theCommandCancelImageGrey,
                                      AppBorderWidthList, self.cancel)
        self.commandFrame.addNewCommandButton(self.cancelButton)
        self.cancelButton.addToolTip("Cancel Current Operation")


    def fieldsDisable(self):
        pass
        self.selectedCurrency.disable()
        self.selectedRate.disable()
        self.selectedEndDate.disable()
        self.selectedStartDate.disable()
        self.selectedGolfer1.disable()
        self.selectedGolfer2.disable()
        self.selectedGolfer3.disable()
        self.selectedGolfer4.disable()
        self.selectedMarker.disable()
        self.selectedSpender.disable()


    def fieldsEnable(self):
        pass
        self.selectedCurrency.enable()
        self.selectedRate.enable()
        self.selectedEndDate.enable()
        self.selectedStartDate.enable()
        self.selectedGolfer1.enable()
        self.selectedGolfer2.enable()
        self.selectedGolfer3.enable()
        self.selectedGolfer4.enable()
        self.selectedMarker.enable()
        self.selectedSpender.enable()


    def fieldsClear(self):
        pass
        self.selectedCurrency.clear()
        self.selectedRate.clear()
        self.selectedEndDate.clear()
        self.selectedStartDate.clear()
        self.selectedGolfer1.clear()
        self.selectedGolfer2.clear()
        self.selectedGolfer3.clear()
        self.selectedGolfer4.clear()
        self.selectedMarker.clear()
        self.selectedSpender.clear()

    def setRequiredFields(self):
        # Table specific.  Will be defined in calling class
        pass
        self.selectedCurrency.setAsRequiredField()
        self.selectedRate.setAsRequiredField()
        self.selectedEndDate.setAsRequiredField()
        self.selectedStartDate.setAsRequiredField()
        self.selectedGolfer1.setAsRequiredField()
        self.selectedGolfer2.setAsRequiredField()
#        self.selectedGolfer3.setAsRequiredField()
#        self.selectedGolfer4.setAsRequiredField()
        self.selectedMarker.setAsRequiredField()
        self.selectedSpender.setAsRequiredField()

    def resetRequiredFields(self):
        pass
        self.selectedCurrency.resetAsRequiredField()
        self.selectedRate.resetAsRequiredField()
        self.selectedEndDate.resetAsRequiredField()
        self.selectedStartDate.resetAsRequiredField()
        self.selectedGolfer1.resetAsRequiredField()
        self.selectedGolfer2.resetAsRequiredField()
        self.selectedGolfer3.resetAsRequiredField()
        self.selectedGolfer4.resetAsRequiredField()
        self.selectedMarker.resetAsRequiredField()
        self.selectedSpender.resetAsRequiredField()


    def validateRequiredFields(self):
        requiredFieldsEntered = True

        if len(self.selectedStartDate.get()) == 0:
            requiredFieldsEntered = False
            self.self.selectedStartDate.focus()

        elif len(self.selectedEndDate.get()) == 0:
            requiredFieldsEntered = False
            self.selectedEndDate.focus()

        elif len(self.selectedGolfer1.get()) == 0:
            requiredFieldsEntered = False
            self.selectedGolfer1.focus()

        elif len(self.selectedGolfer2.get()) == 0:
            requiredFieldsEntered = False
            self.selectedGolfer2.focus()

        elif self.selectedMarker.get() == None:
            requiredFieldsEntered = False
            self.selectedMarker.focus()

        elif self.selectedSpender.get() == None:
            requiredFieldsEntered = False
            self.selectedSpender.focus()

        elif len(self.selectedCurrency.get()) == 0:
            requiredFieldsEntered = False
            self.selectedCurrency.focus()

        elif self.selectedRate.get() == None:
            requiredFieldsEntered = False
            self.selectedRate.focus()

        elif requiredFieldsEntered == False:
            self.myMsgBar.requiredFieldMessage()

        if requiredFieldsEntered == False:
            self.myMsgBar.requiredFieldMessage()
        return requiredFieldsEntered

    def _dateEnteredAction(self, *args):
        self._aDirtyMethod(None)

    def save(self):
        #exportAppConfig
        anOrdinalStartDate = convertDateStringToOrdinal(self.selectedStartDate.get())
        anOrdinalEndDate = convertDateStringToOrdinal(self.selectedEndDate.get())
        aRate = self.selectedRate.get()
        aCurrency = self.selectedCurrency.get()
        aTripMakerID = self.golferCBData.getID(self.selectedMarker.get())
        aTripSpenderID = self.golferCBData.getID(self.selectedSpender.get())
        aGolferID1 = self.golferCBData.getID(self.selectedGolfer1.get())
        aGolferID2 = self.golferCBData.getID(self.selectedGolfer2.get())
        aGolferID3 = self.golferCBData.getID(self.selectedGolfer3.get())
        aGolferID4 = self.golferCBData.getID(self.selectedGolfer4.get())
        ordinalTodayDate = convertDateStringToOrdinal(todayDate)
        updaterFullName = self.updaterFullname
        aRowOfData = []
        theData = (anOrdinalStartDate, anOrdinalEndDate,aGolferID1, aGolferID2, aGolferID3, aGolferID4, aTripMakerID,
                   aRate, aCurrency, aTripSpenderID, self.updaterLoginID, ordinalTodayDate)
        data = aRowOfData.append(theData)
        ans = exportAppConfig(AppConfigFile,theData)
        if ans == True:
            self.setDefaultState()
            aMsg = '''Trip Configuration file was written to file: {0}'''.format(AppConfigFile)
            self.myMsgBar.newMessage('info', aMsg)
        else:
            aMsg = '''ERROR: An error has occurred while writing to the Trip Configuration to file:  {0}'''.format(AppConfigFile)
            self.myMsgBar.newMessage('error', aMsg)


