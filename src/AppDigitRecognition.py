from sklearn.externals import joblib
import argparse
import mahotas
import cv2
import numpy as np
from skimage import feature
from sklearn.svm import LinearSVC

class HOG:
    def __init__(self, orientations=9, pixelsPerCell=(8, 8),
                 cellsPerBlock=(3, 3), transform=False):
        # store the number of orientations, pixels per cell,
        # cells per block, and whether or not power law
        # compression should be applied
        self.orienations = orientations
        self.pixelsPerCell = pixelsPerCell
        self.cellsPerBlock = cellsPerBlock
        self.transform = transform

    def describe(self, image):
        # compute HOG for the image
        hist = feature.hog(image, orientations=self.orienations,
                           pixels_per_cell=self.pixelsPerCell,
                           cells_per_block=self.cellsPerBlock,
                           transform_sqrt=self.transform)

        # return the HOG features
        return hist

def identifyDigit(grayImage):
    fn_model = "models/svm.cpickle"
    # load the model
    # model = joblib.load(args["model"])
    model = joblib.load(fn_model)

    # initialize the HOG descriptor
    hog = HOG(orientations=18, pixelsPerCell=(10, 10),
              cellsPerBlock=(1, 1), transform=True)

    # # load the image and convert it to grayscale
    # # image = cv2.imread(args["image"])
    # image = cv2.imread(fn_image)
    # gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    #
    # # blur the image, find edges, and then find contours along
    # # the edged regions
    # cv2.imshow("image", grayImage)
    # while (1):
    #     k = cv2.waitKey(33)
    #     if k == 27:
    #         break
    # blurred = cv2.GaussianBlur(grayImage, (7, 7), 0)
    # blurred = cv2.GaussianBlur(grayImage, (7, 7), 0)
    blurred = np.hstack([cv2.bilateralFilter(grayImage, 5,200,20)])
    edged = cv2.Canny(blurred, 140, 230)
    (_, cnts, _) = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    # cv2.imshow("image", blurred)
    # while (1):
    #     k = cv2.waitKey(33)
    #     if k == 27:
    #         break
    #
    # cv2.imshow("image", edged)
    # while (1):
    #     k = cv2.waitKey(33)
    #     if k == 27:
    #         break

    # sort the contours by their x-axis position, ensuring
    # that we read the numbers from left to right
    cnts = sorted([(c, cv2.boundingRect(c)[0]) for c in cnts], key=lambda x: x[1])

    # loop over the contours
    list = []
    # lastX = -1
    # print("Number of rectangles: ", len(cnts))
    for (c, _) in cnts:
        # compute the bounding box for the rectangle
        (x, y, w, h) = cv2.boundingRect(c)

        # if the width is at least 7 pixels and the height
        # is at least 20 pixels, the contour is likely a digit
        # if w >= 5 and w <= 45 and h >= 20 and h <= 60 and x > lastX + 5:
        # aString = "({0},{1},{2},{3})".format(x, y, w, h)
        # print("size of rectangles (x,y,w,h): ", aString)
        if w >= 5 and w <= 55 and h >= 29 and h <= 65:
            # crop the ROI and then threshold the grayscale
            # ROI to reveal the digit
            roi = blurred[y:y + h, x:x + w]
            # cv2.imshow("image", roi)
            # while (1):
            #     k = cv2.waitKey(33)
            #     if k == 27:
            #         break
            thresh = roi.copy()
            T = mahotas.thresholding.otsu(roi)
            thresh[thresh > T] = 255
            thresh = cv2.bitwise_not(thresh)

            # deskew the image center its extent
            thresh = deskew(thresh, 20)
            thresh = center_extent(thresh, (20, 20))

            # extract features from the image and classify it
            hist = hog.describe(thresh)
            digit = model.predict([hist])[0]
            list.append(digit)
            # lastX = x

    return list

def trainDigits():
    (digits, target) = load_digits("data/digits.csv")
    data = []

    # initialize the HOG descriptor
    hog = HOG(orientations=18, pixelsPerCell=(10, 10),
              cellsPerBlock=(1, 1), transform=True)

    # loop over the images
    for image in digits:
        # deskew the image, center it
        image = deskew(image, 20)
        image = center_extent(image, (20, 20))

        # describe the image and update the data matrix
        hist = hog.describe(image)
        data.append(hist)

    # train the model
    model = LinearSVC(random_state=42)
    model.fit(data, target)

    # dump the model to file
    # joblib.dump(model, args["model"])
    joblib.dump(model, "models/svm.cpickle")

def load_digits(datasetPath):
    # build the dataset and then split it into data
    # and labels
    data = np.genfromtxt(datasetPath, delimiter=",", dtype="uint8")
    target = data[:, 0]
    data = data[:, 1:].reshape(data.shape[0], 28, 28)

    # return a tuple of the data and targets
    return (data, target)


def deskew(image, width):
    # grab the width and height of the image and compute
    # moments for the image
    (h, w) = image.shape[:2]
    moments = cv2.moments(image)

    # deskew the image by applying an affine transformation
    skew = moments["mu11"] / moments["mu02"]
    M = np.float32([
        [1, skew, -0.5 * w * skew],
        [0, 1, 0]])
    image = cv2.warpAffine(image, M, (w, h),
                           flags=cv2.WARP_INVERSE_MAP | cv2.INTER_LINEAR)

    # resize the image to have a constant width
    image = resize(image, width=width)

    # return the deskewed image
    return image


def center_extent(image, size):
    # grab the extent width and height
    (eW, eH) = size

    # handle when the width is greater than the height
    if image.shape[1] > image.shape[0]:
        image = resize(image, width=eW)

    # otherwise, the height is greater than the width
    else:
        image = resize(image, height=eH)

    # allocate memory for the extent of the image and
    # grab it
    extent = np.zeros((eH, eW), dtype="uint8")
    offsetX = (eW - image.shape[1]) // 2
    offsetY = (eH - image.shape[0]) // 2
    extent[offsetY:offsetY + image.shape[0], offsetX:offsetX + image.shape[1]] = image

    # compute the center of mass of the image and then
    # move the center of mass to the center of the image
    (cY, cX) = np.round(mahotas.center_of_mass(extent)).astype("int32")
    (dX, dY) = ((size[0] // 2) - cX, (size[1] // 2) - cY)
    M = np.float32([[1, 0, dX], [0, 1, dY]])
    extent = cv2.warpAffine(extent, M, size)

    # return the extent of the image
    return extent


def translate(image, x, y):
    # Define the translation matrix and perform the translation
    M = np.float32([[1, 0, x], [0, 1, y]])
    shifted = cv2.warpAffine(image, M, (image.shape[1], image.shape[0]))

    # Return the translated image
    return shifted

def rotate(image, angle, center=None, scale=1.0):
    # Grab the dimensions of the image
    (h, w) = image.shape[:2]

    # If the center is None, initialize it as the center of
    # the image
    if center is None:
        center = (w / 2, h / 2)

    # Perform the rotation
    M = cv2.getRotationMatrix2D(center, angle, scale)
    rotated = cv2.warpAffine(image, M, (w, h))

    # Return the rotated image
    return rotated

def resize(image, width=None, height=None, inter=cv2.INTER_AREA):
    # initialize the dimensions of the image to be resized and
    # grab the image size
    dim = None
    (h, w) = image.shape[:2]

    # if both the width and height are None, then return the
    # original image
    if width is None and height is None:
        return image

    # check to see if the width is None
    if width is None:
        # calculate the ratio of the height and construct the
        # dimensions
        r = height / float(h)
        dim = (int(w * r), height)

    # otherwise, the height is None
    else:
        # calculate the ratio of the width and construct the
        # dimensions
        r = width / float(w)
        dim = (width, int(h * r))

    # resize the image
    resized = cv2.resize(image, dim, interpolation=inter)

    # return the resized image
    return resized

# def identifyDigit(grayImage):
#     fn_model = "models/svm.cpickle"
#     # load the model
#     # model = joblib.load(args["model"])
#     model = joblib.load(fn_model)
#
#     # initialize the HOG descriptor
#     hog = HOG(orientations=18, pixelsPerCell=(10, 10),
#               cellsPerBlock=(1, 1), transform=True)
#
#     # # load the image and convert it to grayscale
#     # # image = cv2.imread(args["image"])
#     # image = cv2.imread(fn_image)
#     # gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
#     #
#     # # blur the image, find edges, and then find contours along
#     # # the edged regions
#     blurred = cv2.GaussianBlur(grayImage, (5, 5), 0)
#     edged = cv2.Canny(blurred, 30, 150)
#     (_, cnts, _) = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
#
#     # sort the contours by their x-axis position, ensuring
#     # that we read the numbers from left to right
#     cnts = sorted([(c, cv2.boundingRect(c)[0]) for c in cnts], key=lambda x: x[1])
#
#     # loop over the contours
#     list = []
#     for (c, _) in cnts:
#         # compute the bounding box for the rectangle
#         (x, y, w, h) = cv2.boundingRect(c)
#
#         # if the width is at least 7 pixels and the height
#         # is at least 20 pixels, the contour is likely a digit
#         if w >= 5 and w <= 45 and h >= 20 and h <= 60:
#             # crop the ROI and then threshold the grayscale
#             # ROI to reveal the digit
#             roi = grayImage[y:y + h, x:x + w]
#             # cv2.imshow("image", roi)
#             # while (1):
#             #     k = cv2.waitKey(33)
#             #     if k == 27:
#             #         break
#             thresh = roi.copy()
#             T = mahotas.thresholding.otsu(roi)
#             thresh[thresh > T] = 255
#             thresh = cv2.bitwise_not(thresh)
#
#             # deskew the image center its extent
#             thresh = deskew(thresh, 20)
#             thresh = center_extent(thresh, (20, 20))
#
#             # extract features from the image and classify it
#             hist = hog.describe(thresh)
#             digit = model.predict([hist])[0]
#             list.append(digit)
#
#     return list
