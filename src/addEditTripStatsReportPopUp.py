from popUpWindowClass import popUpWindowClass
from AppClasses import *
from AppConstants import *
from AppProc import convertDateStringToOrdinal, updateMyAppConstant
from AppProc import getDBLocationFullName
import AppCRUDGolfers as CRUDGolfers
import AppCRUDGames as CRUDGames
import AppCRUD as CRUD
from AppDialogClass import AppDisplayUnderConstruction
import tkinter.messagebox as Mb


class compareStatsTable(Frame):
    def __init__(self, parent, tableColumnHeading, tableColumnWidth, tableColumnWeight, myMsgBar):
        Frame.__init__(self, parent)
        self.parent = parent
        self.tableFont = fontAverage
        self.tableFontB = fontAverageB
        self.tableRelief = 'ridge'
        self.tableHeadingColor = AppDefaultBackground
        self.row = 0
        self.column = 0
        self.myMsgBar = myMsgBar
        self.tableColumnHeading = tableColumnHeading
        self.tableColumnWeight = tableColumnWeight
        self.tableColumnWidth = tableColumnWidth
        self.columnTotal = len(self.tableColumnHeading)
        self.config(border=0, relief='ridge')
        self.defineColumnWeight()
        self.createHeaderRow()

        self.rootWidgetName = self.winfo_name()

    def addRow(self, stretch=False):
        self.row = self.row + 1
        if stretch == True:
            self.rowconfigure(self.row, weight=1)

    def addColumn(self):
        self.column = self.column + 1

    def resetColumn(self):
        self.column = 0

    def defineColumnWeight(self):
        for i in range(self.columnTotal):
            self.columnconfigure(i, weight=self.tableColumnWeight[i])

    def createHeaderRow(self):
        self.rowconfigure(self.row, weight=1)
        for i in range(self.columnTotal):
            if i != 0:
                aLabel = Label(self, font=self.tableFontB, text=self.tableColumnHeading[i], relief=self.tableRelief,
                               bg=self.tableHeadingColor, border = 3,
                               width=self.tableColumnWidth[i])
                aLabel.grid(row=self.row, column=i, sticky=E + W + S + N)

    def populateTable(self, rowList, columnData, leaderList, leaderDirection, roundsPlayed):
        if len(rowList) > 0:
            for row in range(len(rowList)):
                self.addRow(True)
                self.resetColumn()
                if row%2 != 0:
                    background = defaultBgColorReportsAlternate
                else:
                    background = defaultBgColorReports

                Label(self, text=rowList[row], border=3, relief=self.tableRelief, justify=LEFT,
                      width=self.tableColumnWidth[self.column], font=self.tableFontB,
                      background=background).grid(row=self.row, column=self.column, sticky=E + W + S + N)

                for col in range(len(columnData[row])):
                    self.addColumn()
                    if float(roundsPlayed[row] > 0):  # do not identify him in leaders if no games have been played.
                        if (float(columnData[row][col]) >= float(leaderList[col])) and (leaderDirection[col] == 'H'):
                            background = defaultBgColorReportsLeader
                        elif (float(columnData[row][col]) <= float(leaderList[col])) and (leaderDirection[col] == 'L'):
                            background = defaultBgColorReportsLeader
                        elif row % 2 != 0:
                            background = defaultBgColorReportsAlternate
                        else:
                            background = defaultBgColorReports

                    Tix.Label(self, text=columnData[row][col], border=3, relief=self.tableRelief,
                              font=fontBig, width=self.tableColumnWidth[self.column],
                              background=background).grid(row=self.row, column=self.column, sticky=N + S + E + W)

class addEditTripStatsReportPopUp(popUpWindowClass):
    def __init__(self, aWindow, aCloseCommand, loginData):
        #
        #  loginInfo format: Updater Full Name, Login ID, updaterID
        #
        self.windowTitle = 'Myrtle Beach Trip Stats Comparison'
        popUpWindowClass.__init__(self, aWindow, aCloseCommand, self.windowTitle, loginData, None)
        self.tableName = 'StatsCompare'
        self.aCloseCommand = aCloseCommand
        self.aWindow = aWindow
        self.loginData = loginData

        self.configDataRead = []
        self.golferComboBoxData = CRUDGolfers.getGolferCB()
        self.golferDictionary = {}
        self.reverseGolferDictionary = {}
        for i in range(len(self.golferComboBoxData)):
            golfer_full = '''{0} {1}'''.format(self.golferComboBoxData[i][1], self.golferComboBoxData[i][2])
            self.golferDictionary.update({golfer_full: self.golferComboBoxData[i][0]})
            self.reverseGolferDictionary.update({self.golferComboBoxData[i][0]: golfer_full})
        self.bigSpender = None
        #
        #  loginInfo format: Updater Full Name, access_Level, loginID
        #
        self.updaterFullname = loginData[0]
        self.updaterLoginID = self.loginData[2]
        self.displayCurUser.setUserName(self.updaterFullname)


        #######################################################################################
        # Inherited all from tableCommanWindowClass
        #
        #  This class intents to change the mainArea only.  Everything else must remain common
        #
        self.mainAreaRowCount = 0
        self.mainAreaColumnCount = 0
        self._reDisplayRecordFrame()
        self.mainArea.rowconfigure(self.mainAreaRowCount, weight=1)
        self.setDefaultState()
        self.populateStatsTables()
        self.deletedV.set('N') # In the absence of a table with the field delete, creates the green outline of the main window

    def setDefaultState(self):
        self.curOp='default'
        self.fieldsDisable()
        self.displayCurFunction.setDefaultMode()
        self.submitButton.disable()
        self.editButton.enable()
        self.cancelButton.disable()
        self.loadCurrentConfiguration()
        self.lastUpdate.load(self.configDataRead[0][11],int(self.configDataRead[0][10]))
        self.dirty = False

    def loadCurrentConfiguration(self):
        self.configDataRead = updateMyAppConstant(AppConfigFile)
        # self.selectedCurrency.load(self.configDataRead[0][8])
        # self.selectedRate.load(self.configDataRead[0][7])
        self.selectedEndDate.load(convertOrdinaltoString(self.configDataRead[0][1]))
        self.selectedStartDate.load(convertOrdinaltoString(self.configDataRead[0][0]))
        self.selectedGolfer1.load(self.golferCBData.getName(int(self.configDataRead[0][2])))
        self.selectedGolfer2.load(self.golferCBData.getName(int(self.configDataRead[0][3])))
        self.selectedGolfer3.load(self.golferCBData.getName(int(self.configDataRead[0][4])))
        self.selectedGolfer4.load(self.golferCBData.getName(int(self.configDataRead[0][5])))
        # self.selectedMarker.load(self.golferCBData.getName(int(self.configDataRead[0][6])))
        # self.selectedSpender.load(self.golferCBData.getName(int(self.configDataRead[0][9])))
        # self.bigSpender = self.reverseGolferDictionary[int(self.configDataRead[0][9])]

    def add(self, *args):
        pass
        self.dirty = False

    def edit(self, *args):
        pass
        self.curOp='edit'
        self.displayCurFunction.setEditMode()
        self.editButton.disable()
        self.submitButton.enable()
        self.cancelButton.enable()
        self.fieldsEnable()
        self.dirty = False

    def cancel(self, *args):
        if self.dirty == True:
            aMsg = "The record currently in {0} mode has not been saved.\n" \
                   "Cancelling now would result in lost of data.\n" \
                   "Do you wish to continue with the cancel current command?".format(self.curOp)
            ans = Mb.askyesno('Cancel Current Command', aMsg)
            if ans == True:
                if self.validateRequiredFields() == True:
                    self.setDefaultState()
        else:
            if self.validateRequiredFields() == True:
                self.setDefaultState()

    def _submitForm(self, *args):
        if self.validateRequiredFields() == True:
            self.save()
        # Must be defined in the popUp window.

    def _clearForm(self, *args):
        self.fieldsClear()
        # Must be defined in the popUp window.

    def tableSpecificMenu(self):
        pass
        '''
        self.viewsmenu.add_command
        self.viewsmenu.add_command(label="Active Golfers", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                   command=lambda viewID='Active Golfers': self.basicViewsList(viewID))

        sortsmenu = Menu(self.menubar, foreground=AppDefaultForeground, font=AppDefaultMenuFont, tearoff=0)
        sortsmenu.add_command(label="Default", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Default': self.sortingList(sortID))
        sortsmenu.add_command(label="Last Name", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Last Name': self.sortingList(sortID))
        sortsmenu.add_command(label="First Name", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='First Name': self.sortingList(sortID))
        sortsmenu.add_command(label="Birthday", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Birthday': self.sortingList(sortID))
        #
        # Next Command actually puts the menu up
        #
        self.menubar.add_cascade(label="Sorts", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                 menu=sortsmenu)
        '''

    def _exitForm(self, *args):
        if self.dirty == True:
            aMsg = "The record currently in {0} mode has not been saved.\n" \
                   "Exiting now would result in lost of data.\n" \
                   "Do you wish to continue with the exit command?".format(self.curOp)
            ans = Mb.askyesno('Restore My Golf App 2016 Database', aMsg)
            if ans == True:
                self.aCloseCommand()
        else:
            self.aCloseCommand()

    def sort(self):
        pass
        '''
        if self.curOp == 'default':
            currentID = self.recList[self.curRecNumV.get()][0]
            if self.sortID.get() == 'Default':
                self.recList = sorted(self.recList, key=operator.itemgetter(2,1))
            elif self.sortID.get() == 'Last Name':
                self.recList.sort(key=lambda row: row[2])
            elif self.sortID.get() == 'First Name':
                self.recList.sort(key=lambda row: row[1])
            elif self.sortID.get() == 'Birthday':
                self.recList.sort(key=lambda row: row[4])
            else:
                self.recList.sort(key=lambda row: row[0])
            newIdx = getIndex(currentID, self.recList)
            self.curRecNumV.set(newIdx)
            self.displayRec()
        else:
            aMsg = "ERROR: Invalid request. Must be executed in default state only."
            self.myMsgBar.newMessage('error', aMsg)
        '''

    def _descriptionEnteredAction(self):
        pass

    def _buildWindowsFields(self, aFrame):
        #        headerList1 = ('BASIC','VALUE','VALUED','MULT','TOLE','SIZE','DESCR','NSN1','NSN2','NSN3','NSN4')
        colTotal = 2
        self.fieldsFrame = AppFieldsFrame(aFrame, colTotal)
        self.fieldsFrame.grid(row=0, column=0, sticky=N + S + E + W)
        # self.fieldsFrame.stretchCurrentRow()

        ########################################################### Parameter Section
        self.parameterFrame = AppBorderFrame(self.fieldsFrame, 1)
        self.parameterFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column,
                                 columnspan=self.fieldsFrame.columnTotal,sticky=E + W + N + S)

        self.selectedStartDate = AppFieldEntryDateV2(self.parameterFrame, 'Start Date', 'V', appDateWidth,
                                                   self._dateEnteredAction, self.myMsgBar, self._aDirtyMethod)
        self.selectedStartDate.stretchColumn()
        self.selectedStartDate.grid(row=self.parameterFrame.row, column=self.parameterFrame.column,
                                    sticky=E + W + N + S)

        self.parameterFrame.addColumn()
        self.parameterFrame.stretchCurrentColumn()
        self.selectedEndDate = AppFieldEntryDateV2(self.parameterFrame, 'End Date', 'V', appDateWidth,
                                                   self._dateEnteredAction, self.myMsgBar, self._aDirtyMethod)
        self.selectedEndDate.stretchColumn()
        self.selectedEndDate.grid(row=self.parameterFrame.row, column=self.parameterFrame.column,
                                    sticky=E + W + N + S)

        self.parameterFrame.addColumn()
        self.parameterFrame.stretchCurrentColumn()
        self.golferCBData = AppCBList(CRUDGolfers.getGolfersDictionaryInfo())
        self.selectedGolfer1 = AppSearchLB(self.parameterFrame, self, 'Golfer #1', 'V',
                                           self.golferCBData.getList(), AppFullNameWidth, None, self.myMsgBar,
                                           self._aDirtyMethod, None)
        self.selectedGolfer1.grid(row=self.parameterFrame.row, column=self.parameterFrame.column,
                                  sticky=E + W + N + S)

        self.parameterFrame.addColumn()
        self.parameterFrame.stretchCurrentColumn()
        self.selectedGolfer2 = AppSearchLB(self.parameterFrame, self, 'Golfer #2', 'V',
                                           self.golferCBData.getList(), AppFullNameWidth, None, self.myMsgBar,
                                           self._aDirtyMethod, None)
        self.selectedGolfer2.grid(row=self.parameterFrame.row, column=self.parameterFrame.column,
                                  sticky=E + W + N + S)

        self.parameterFrame.addColumn()
        self.parameterFrame.stretchCurrentColumn()
        self.selectedGolfer3 = AppSearchLB(self.parameterFrame, self, 'Golfer #3', 'V',
                                           self.golferCBData.getList(), AppFullNameWidth, None, self.myMsgBar,
                                           self._aDirtyMethod, None)
        self.selectedGolfer3.grid(row=self.parameterFrame.row, column=self.parameterFrame.column,
                                  sticky=E + W + N + S)

        self.parameterFrame.addColumn()
        self.parameterFrame.stretchCurrentColumn()
        self.selectedGolfer4 = AppSearchLB(self.parameterFrame, self, 'Golfer #4', 'V',
                                           self.golferCBData.getList(), AppFullNameWidth, None, self.myMsgBar,
                                           self._aDirtyMethod, None)
        self.selectedGolfer4.grid(row=self.parameterFrame.row, column=self.parameterFrame.column,
                                  sticky=E + W + N + S)

        self.fieldsFrame.addRow()
        self.fieldsFrame.stretchCurrentRow()

        self.reportFrame = AppBorderFrame(self.fieldsFrame, 1)
        self.reportFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column,
                                 columnspan=self.fieldsFrame.columnTotal, sticky=E + W + N + S)
        self.reportFrame.stretchCurrentRow()
        self.reportFrame.stretchCurrentColumn()

        self.reportPg1Frame = AppBorderFrame(self.reportFrame, 1)
        self.reportPg1Frame.grid(row=self.reportFrame.row, column=self.reportFrame.column,
                                 columnspan=self.reportFrame.columnTotal, sticky=E + W + N + S)
        self.reportPg1Frame.stretchCurrentRow()
        self.reportPg1Frame.stretchCurrentColumn()
        self.reportPg1Frame.noBorder()

        self.roundRelatedFrame = AppBorderFrame(self.reportPg1Frame, 1)
        self.roundRelatedFrame.grid(row = self.reportPg1Frame.row, column = self.reportPg1Frame.column,
                                    columnspan=self.reportPg1Frame.columnTotal,
                                    sticky=E+W+S+N)
        self.roundRelatedFrame.addTitle("Round Related Statistics")
        self.roundRelatedFrame.stretchCurrentColumn()
        self.roundRelatedFrame.stretchCurrentRow()

        self.roundStatsColumnHeading = ("","Total Rounds", "Best 18", "Average 18", "Best 9", "Average 9", "Golfer Index")
        self.roundStatsColumnWidth = (golferNameWidth, 10, 10, 10, 10, 10, 10)
        self.roundStatsColumnWeight = (golferNameWeight, 6, 6, 6, 6, 6, 6)
        # self.roundStatsTable = compareStatsTable(self.roundRelatedFrame, self.roundStatsColumnHeading,
        #                                          self.roundStatsColumnWidth, self.roundStatsColumnWeight,
        #                                          self.myMsgBar)
        # self.roundStatsTable.grid(row=self.roundRelatedFrame.row, column=self.roundRelatedFrame.column,
        #                             sticky=E + N + W + S)

        self.reportPg1Frame.addRow()
        self.reportPg1Frame.stretchCurrentRow()
        self.reportPg1Frame.stretchCurrentColumn()
        self.scoringRelatedFrame = AppBorderFrame(self.reportPg1Frame, 1)
        self.scoringRelatedFrame.grid(row = self.reportPg1Frame.row, column = self.reportPg1Frame.column,
                                      columnspan=self.reportPg1Frame.columnTotal,
                                      sticky=E+W+S+N)
        self.scoringRelatedFrame.addTitle("Scoring Related Statistics")
        self.scoringRelatedFrame.stretchCurrentColumn()
        self.scoringRelatedFrame.stretchCurrentRow()

        self.scoringStatsColumnHeading = ("","Double Eagles", "Eagles", "Birdies", "Pars", "Bogies", "Double Bogies", "Triple Bogies", "Others")
        self.scoringStatsColumnWidth = (golferNameWidth, 8, 8, 8, 8, 8, 8, 8, 8)
        self.scoringStatsColumnWeight = (golferNameWeight+2, 4, 4, 4, 4, 4, 4, 4, 4)
        # self.scoringStatsTable = compareStatsTable(self.scoringRelatedFrame, self.scoringStatsColumnHeading,
        #                                          self.scoringStatsColumnWidth, self.scoringStatsColumnWeight,
        #                                          self.myMsgBar)
        # self.scoringStatsTable.grid(row=self.scoringRelatedFrame.row, column=self.scoringRelatedFrame.column,
        #                             sticky=E + N + W + S)

        self.reportPg1Frame.addRow()
        self.goToPage2Button = AppButton(self.reportPg1Frame, self.theNextPageImage, self.theNextPageImage,
                                           AppBorderWidthList, self._goToPage2)
        self.goToPage2Button.grid(row=self.reportPg1Frame.row, column=self.reportPg1Frame.column,
                                  sticky=N+S+E)
        self.goToPage2Button.addToolTip("Next Page")

        self.reportPg2Frame = AppBorderFrame(self.reportFrame, 1)
        self.reportPg2Frame.grid(row=self.reportFrame.row, column=self.reportFrame.column,
                                 columnspan=self.reportFrame.columnTotal, sticky=E + W + N + S)
        self.reportPg2Frame.stretchCurrentRow()
        self.reportPg2Frame.stretchCurrentColumn()
        self.reportPg2Frame.noBorder()

        self.fairwayRelatedFrame = AppBorderFrame(self.reportPg2Frame, 1)
        self.fairwayRelatedFrame.grid(row = self.reportPg2Frame.row, column = self.reportPg2Frame.column,
                                    columnspan=self.reportPg2Frame.columnTotal,
                                    sticky=E+W+S+N)
        self.fairwayRelatedFrame.addTitle("Fairway Related Statistics")
        self.fairwayRelatedFrame.stretchCurrentColumn()
        self.fairwayRelatedFrame.stretchCurrentRow()

        self.fairwayStatsColumnHeading = ("","Fairways", "Average\nFairways", "Driving\nAccuracy", "Average\nDrives",
                                          "Total Miss\nRight", "Right Miss\nPercentage", "Total Miss\n Left", "Left Miss\nPercentage")
        self.fairwayStatsColumnWidth = (golferNameWidth, 8, 8, 8, 8, 8, 8, 8, 8)
        self.fairwayStatsColumnWeight = (golferNameWeight, 4, 4, 4, 4, 4, 4, 4, 4)
        # self.fairwayStatsTable = compareStatsTable(self.fairwayRelatedFrame, self.fairwayStatsColumnHeading,
        #                                          self.fairwayStatsColumnWidth, self.fairwayStatsColumnWeight,
        #                                          self.myMsgBar)
        # self.fairwayStatsTable.grid(row=self.fairwayRelatedFrame.row, column=self.fairwayRelatedFrame.column,
        #                             sticky=E + N + W + S)

        self.reportPg2Frame.addRow()
        self.reportPg2Frame.stretchCurrentRow()
        self.reportPg2Frame.stretchCurrentColumn()
        self.greenRelatedFrame = AppBorderFrame(self.reportPg2Frame, 1)
        self.greenRelatedFrame.grid(row = self.reportPg2Frame.row, column = self.reportPg2Frame.column,
                                      columnspan=self.reportPg2Frame.columnTotal,
                                      sticky=E+W+S+N)
        self.greenRelatedFrame.addTitle("Green Related Statistics")
        self.greenRelatedFrame.stretchCurrentColumn()
        self.greenRelatedFrame.stretchCurrentRow()

        self.greenStatsColumnHeading = ("","GIRs", "Average\nGIRs", "Total\nPutts", "Putts per\nRound",
                                          "3-Putts +", "Total Sands", "Sand Saves", "Sand Save\nPercentage")
        self.greenStatsColumnWidth = (golferNameWidth, 8, 8, 8, 8, 8, 8, 8, 8)
        self.greenStatsColumnWeight = (golferNameWeight, 4, 4, 4, 4, 4, 4, 4, 4)
        # self.greenStatsTable = compareStatsTable(self.greenRelatedFrame, self.greenStatsColumnHeading,
        #                                          self.greenStatsColumnWidth, self.greenStatsColumnWeight,
        #                                          self.myMsgBar)
        # self.greenStatsTable.grid(row=self.greenRelatedFrame.row, column=self.greenRelatedFrame.column,
        #                             sticky=E + N + W + S)

        self.reportPg2Frame.addRow()
        self.goToPage1Button = AppButton(self.reportPg2Frame, self.thePreviousPageImage, self.thePreviousPageImage,
                                         AppBorderWidthList, self._goToPage1)
        self.goToPage1Button.grid(row=self.reportPg2Frame.row, column=self.reportPg2Frame.column,
                                  sticky=N + S + E)
        self.goToPage1Button.addToolTip("Previous Page")
        self.reportPg2Frame.grid_remove()
        # self.goToPage2Button.changeButtonSize(AppPageNavButtonWidth, AppPageNavButtonHeight)

        ########################################################### Footer Section
        self.fieldsFrame.addRow()
        self.lastUpdate = AppLastUpdateFormat(self.fieldsFrame)
        self.lastUpdate.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, columnspan=colTotal,
                             sticky=E + N + S)

        self.setRequiredFields()
        self.fieldsDisable()
        self.clearButton.destroy()  # Not required for this application

    def createStatsTables(self):
        self.roundStatsTable = compareStatsTable(self.roundRelatedFrame, self.roundStatsColumnHeading,
                                                 self.roundStatsColumnWidth, self.roundStatsColumnWeight,
                                                 self.myMsgBar)
        self.roundStatsTable.grid(row=self.roundRelatedFrame.row, column=self.roundRelatedFrame.column,
                                  sticky=E + N + W + S)

        self.scoringStatsTable = compareStatsTable(self.scoringRelatedFrame, self.scoringStatsColumnHeading,
                                                 self.scoringStatsColumnWidth, self.scoringStatsColumnWeight,
                                                 self.myMsgBar)
        self.scoringStatsTable.grid(row=self.scoringRelatedFrame.row, column=self.scoringRelatedFrame.column,
                                    sticky=E + N + W + S)

        self.fairwayStatsTable = compareStatsTable(self.fairwayRelatedFrame, self.fairwayStatsColumnHeading,
                                                 self.fairwayStatsColumnWidth, self.fairwayStatsColumnWeight,
                                                 self.myMsgBar)
        self.fairwayStatsTable.grid(row=self.fairwayRelatedFrame.row, column=self.fairwayRelatedFrame.column,
                                    sticky=E + N + W + S)

        self.greenStatsTable = compareStatsTable(self.greenRelatedFrame, self.greenStatsColumnHeading,
                                                 self.greenStatsColumnWidth, self.greenStatsColumnWeight,
                                                 self.myMsgBar)
        self.greenStatsTable.grid(row=self.greenRelatedFrame.row, column=self.greenRelatedFrame.column,
                                    sticky=E + N + W + S)

    def obtainGameRelatedStatsFromList(self, aList, golferID, startDate, endDate):
        # (Number of Games,Best 18,Average 18, Best 9, Average 9, Current Index)
        # (10.5,84,89.4,42,44.5,9.4)
        #    gamesStats=obtainGameRelatedStatsFromList(aList, golferID, startDate, endDate)
        best18 = 0
        first18 = False
        total18 = 0
        best9 = 0
        first9 = False
        total9 = 0
        numOf18 = 0
        numOf9 = 0
        numOfGames = float(0)
        for i in range(len(aList)):
            if aList[i][26] == 'Back':
                numOfGames = numOfGames + 0.5
                total9 = aList[i][6] + total9
                numOf9 = 1 + numOf9
                if first9 == False:
                    first9 = True
                    best9 = int(aList[i][6])
                elif int(aList[i][6]) < best9:
                    best9 = int(aList[i][6])

            elif aList[i][26] == 'Front':
                if first9 == False:
                    first9 = True
                    best9 = int(aList[i][5])
                elif int(aList[i][5]) < best9:
                    best9 = int(aList[i][5])
                numOfGames = numOfGames + 0.5
                total9 = aList[i][5] + total9
                numOf9 = 1 + numOf9
            else:
                if first18 == False:
                    first18 = True
                    best18 = int(aList[i][7])
                elif int(aList[i][7]) < best18:
                    best18 = int(aList[i][7])
                # check front 9
                if first9 == False:
                    first9 = True
                    best9 = int(aList[i][5])
                elif int(aList[i][5]) < best9:
                    best9 = int(aList[i][5])
                # check back9
                if first9 == False:
                    first9 = True
                    best9 = int(aList[i][6])
                elif int(aList[i][6]) < best9:
                    best9 = int(aList[i][6])

                numOfGames = numOfGames + float(1)
                # add front 9
                total9 = aList[i][5] + total9
                numOf9 = 1 + numOf9
                # add back 9
                total9 = aList[i][6] + total9
                numOf9 = 1 + numOf9

                total18 = aList[i][7] + total18
                numOf18 = 1 + numOf18

                # Uses only 18 to calculate the average
                #    if numOf18 == 0:
                #        listNum18=(0.0)
                #    else:
                #        listNum18=("%.1f" % (float(total18)/float(numOf18))) # Uses only 18 to calculate the average

                # Uses all games (9's and 18's) to calculate the average
        if numOf9 == 0:
            listNum18 = (0.0)
        else:
            listNum18 = ("%.1f" % (float(total9) / float(numOf9 / 2)))  # Uses all the scores

        if numOf9 == 0:
            listNumof9 = 0.0
        else:
            listNumof9 = ("%.1f" % (float(total9) / float(numOf9)))

        idx = self.calculateIdxForDateRange(golferID, startDate, endDate)
        # (Number of Games,Best 18,Average 18, Best 9, Average 9, Current Index)
        returnList = (numOfGames, best18, listNum18, best9, listNumof9, idx)
        return returnList

    def calculateIdxForDateRange(self, golferID, startDate, endDate):
        # use ordinal dates
        curGameList = CRUDGames.get_games_forIdx_RangeDate_for_golfer(golferID, startDate,
                                                                       endDate)  # organized, latest first
        numberOfGames = len(curGameList)
        if numberOfGames == 0:
            return 0.0
        else:
            newGameList = []
            for i in range(numberOfGames):
                newGameList.append(curGameList[i][1])

            index = self.calculateHdcpList(newGameList)
            return index

    def calculateHdcpList(self, hdcpGameList):
            hdcpGameList.sort()
            if len(hdcpGameList) >= 20:
                newHdcp = float(0.0)
                for i in range(10):
                    newHdcp = newHdcp + float(hdcpGameList[i])
                newHdcp = newHdcp / float(10)
                #            return"%.1f" % newHdcp
            elif len(hdcpGameList) == 19:
                newHdcp = float(0.0)
                for i in range(9):
                    newHdcp = newHdcp + float(hdcpGameList[i])
                newHdcp = newHdcp / float(9)
                #            return"%.1f" % newHdcp
            elif len(hdcpGameList) == 18:
                newHdcp = float(0.0)
                for i in range(8):
                    newHdcp = newHdcp + float(hdcpGameList[i])
                newHdcp = newHdcp / float(8)
                #            return"%.1f" % newHdcp
            elif len(hdcpGameList) == 17:
                newHdcp = float(0.0)
                for i in range(7):
                    newHdcp = newHdcp + float(hdcpGameList[i])
                newHdcp = newHdcp / float(7)
                #            return"%.1f" % newHdcp
            elif (len(hdcpGameList) == 15) or (len(hdcpGameList) == 16):
                newHdcp = float(0.0)
                for i in range(6):
                    newHdcp = newHdcp + float(hdcpGameList[i])
                newHdcp = newHdcp / float(6)
                #            return"%.1f" % newHdcp
            elif (len(hdcpGameList) == 13) or (len(hdcpGameList) == 14):
                newHdcp = float(0.0)
                for i in range(5):
                    newHdcp = newHdcp + float(hdcpGameList[i])
                newHdcp = newHdcp / float(5)
                #            return"%.1f" % newHdcp
            elif (len(hdcpGameList) == 11) or (len(hdcpGameList) == 12):
                newHdcp = float(0.0)
                for i in range(4):
                    newHdcp = newHdcp + float(hdcpGameList[i])
                newHdcp = newHdcp / float(4)
                #            return"%.1f" % newHdcp
            elif (len(hdcpGameList) == 9) or (len(hdcpGameList) == 10):
                newHdcp = float(0.0)
                for i in range(3):
                    newHdcp = newHdcp + float(hdcpGameList[i])
                newHdcp = newHdcp / float(3)
                #            return"%.1f" % newHdcp
            elif (len(hdcpGameList) == 7) or (len(hdcpGameList) == 8):
                newHdcp = float(0.0)
                for i in range(2):
                    newHdcp = newHdcp + float(hdcpGameList[i])
                newHdcp = newHdcp / float(2)
            else:
                newHdcp = float(hdcpGameList[0])

            newHdcp = newHdcp * float(0.98)
            return "%.1f" % newHdcp

    def _calculateGameStats(self, golfers, golfersGameList, startDate, endDate):
        #(Number of Games,Best 18,Average 18, Best 9, Average 9, Current Index)
        #(10.5,84,89.4,42,44.5,9.4)
        allGolfersList = []
        for i in range(len(golfers)):
            allGolfersList.append(self.obtainGameRelatedStatsFromList(golfersGameList[i], self.golferCBData.getID(golfers[i]), startDate, endDate))
        return allGolfersList

    def _getGolfersList(self, golfers, startDate, endDate):
        CRUD.initDB(getDBLocationFullName())

        golfersGameList = []
        for i in range(len(golfers)):
            golfersGameList.append(CRUDGames.get_games_for_period_golfer(self.golferCBData.getID(golfers[i]), startDate, endDate, True))

        CRUD.closeDB()

        return golfersGameList

    def findColLeader(self,aList,dirList):
        returnList = []
        if len(aList) > 0:
            for col in range(len(dirList)):
                leaderIdx = 0
                if dirList[col] == 'H':
                    for row in range(1,len(aList)):
                        if float(aList[row][col]) > float(aList[leaderIdx][col]):
                            leaderIdx = row
                    returnList.append(aList[leaderIdx][col])
                elif dirList[col] == 'L':
                    for row in range(1,len(aList)):
                        if float(aList[row][col]) > 0:
                            if float(aList[row][col]) < float(aList[leaderIdx][col]):
                                leaderIdx = row
                    returnList.append(aList[leaderIdx][col])
        return returnList

    def obtainScoringRelatedStatsFromList(self, aList, golferID):
        # (Deagles,Eagles,Birdies,Pars,Bogies,DBogies,TBogies,Others)
        Deagles = 0
        Eagles = 0
        Birdies = 0
        Pars = 0
        Bogies = 0
        DBogies = 0
        TBogies = 0
        Others = 0
        for i in range(len(aList)):
            Deagles = Deagles + aList[i][9]
            Eagles = Eagles + aList[i][10]
            Birdies = Birdies + aList[i][11]
            Pars = Pars + aList[i][12]
            Bogies = Bogies + aList[i][13]
            DBogies = DBogies + aList[i][14]
            TBogies = TBogies + aList[i][15]
            Others = Others + aList[i][16]
        returnList = (Deagles, Eagles, Birdies, Pars, Bogies, DBogies, TBogies, Others)
        return returnList

    def _calculateScoringStats(self, golfers, golfersGameList):
        #(Deagles,Eagles,Birdies,Pars,Bogies,DBogies,TBogies,Others)
        allGolfersList = []
        for i in range(len(golfers)):
            allGolfersList.append(self.obtainScoringRelatedStatsFromList(golfersGameList[i],
                                                                    self.golferCBData.getID(golfers[i])))
        return allGolfersList

    def populateStatsTables(self):
        try:
            self.roundStatsTable.grid_info()
            self.gamesCanvas.destroyAll()
        except:
            pass

        try:
            self.scoringStatsTable.grid_info()
            self.gamesCanvas.destroyAll()
        except:
            pass

        try:
            self.fairwayStatsTable.grid_info()
            self.gamesCanvas.destroyAll()
        except:
            pass

        try:
            self.greenStatsTable.grid_info()
            self.gamesCanvas.destroyAll()
        except:
            pass

        self.createStatsTables()

        startDate = convertDateStringToOrdinal(self.selectedStartDate.get())
        endDate = convertDateStringToOrdinal(self.selectedEndDate.get())

        golfers = []
        golfers.append(self.selectedGolfer1.get())
        golfers.append(self.selectedGolfer2.get())
        golfers.append(self.selectedGolfer3.get())
        golfers.append(self.selectedGolfer4.get())

        golfersGameList = self._getGolfersList(golfers, startDate, endDate)


        golfersGameStats = self._calculateGameStats(golfers, golfersGameList, startDate, endDate)
        golfersGamesPlayed = []
        for i in range(len(golfersGameList)):
            golfersGamesPlayed.append(golfersGameStats[i][0])
        gameStatsDirectionList = ('H', 'L', 'L', 'L', 'L', 'L')
        gameStatsLeadingValues = self.findColLeader(golfersGameStats, gameStatsDirectionList)
        self.roundStatsTable.populateTable(golfers, golfersGameStats, gameStatsLeadingValues,
                                           gameStatsDirectionList, golfersGamesPlayed)

        golfersScoringStats = self._calculateScoringStats(golfers, golfersGameList)
        scoringStatsDirectionList = ('H', 'H', 'H', 'H', 'H', 'H', 'H', 'H')
        scoringStatsLeadingValues = self.findColLeader(golfersScoringStats, scoringStatsDirectionList)
        self.scoringStatsTable.populateTable(golfers, golfersScoringStats, scoringStatsLeadingValues,
                                             scoringStatsDirectionList, golfersGamesPlayed)

        golfersFairwayStats = self._calculateFairwayStats(golfers, golfersGameList, golfersGamesPlayed)
        fairwayDirectionList = ('H', 'H', 'H', 'H', 'H', 'H', 'H', 'H')
        fairwayStatsLeadingValues = self.findColLeader(golfersFairwayStats, fairwayDirectionList)
        self.fairwayStatsTable.populateTable(golfers, golfersFairwayStats, fairwayStatsLeadingValues,
                                             fairwayDirectionList, golfersGamesPlayed)

        golfersGreensStats = self._calculateGreensStats(golfers, golfersGameList, golfersGamesPlayed)
        greensDirectionList = ('H', 'H', 'L', 'L', 'L', 'H', 'H', 'H')
        greensStatsLeadingValues = self.findColLeader(golfersGreensStats, greensDirectionList)
        self.greenStatsTable.populateTable(golfers, golfersGreensStats, greensStatsLeadingValues,
                                           greensDirectionList, golfersGamesPlayed)

    def _calculateGreensStats(self, golfers, golfersGameList, gamesPlayed):
        #(Deagles,Eagles,Birdies,Pars,Bogies,DBogies,TBogies,Others)
        allGolfersList = []
        for i in range(len(golfers)):
            allGolfersList.append(self.obtainGreensRelatedStatsFromList(golfersGameList[i],
                                                                        self.golferCBData.getID(golfers[i]),
                                                                        gamesPlayed[i]))
        return allGolfersList

    def _calculateFairwayStats(self, golfers, golfersGameList, gamesPlayed):
        #(Deagles,Eagles,Birdies,Pars,Bogies,DBogies,TBogies,Others)
        allGolfersList = []
        for i in range(len(golfers)):
            allGolfersList.append(self.obtainFairwayRelatedStatsFromList(golfersGameList[i],
                                                                         self.golferCBData.getID(golfers[i]),
                                                                         gamesPlayed[i]))
        return allGolfersList

    def obtainGreensRelatedStatsFromList(self, aList, golferID,gamesPlayed):
        # (Girs, AvgGIRs, TotalPutts, puttsPerRound,putts3AndUp,totalSands, sandSaves,sandSavesPercent)
        Girs = 0
        TotalPutts = 0
        puttsPerRound = 0
        putts3AndUp = 0
        totalSands = 0
        sandSaves = 0
        for i in range(len(aList)):
            totalSands = totalSands + aList[i][22]
            sandSaves = sandSaves + aList[i][23]
            Girs = Girs + aList[i][17]
            TotalPutts = TotalPutts + aList[i][27]
            putts3AndUp = putts3AndUp + aList[i][28]

        if totalSands > 0:
            sandSavesPercent = (float(sandSaves) / float(totalSands)) * float(100)
        else:
            sandSavesPercent = 0.0

        if gamesPlayed > 0:
            AvgGIRs = float(Girs) / gamesPlayed
        else:
            AvgGIRs = 0.0

        if gamesPlayed > 0:
            AvgGIRs = float(Girs) / gamesPlayed
            puttsPerRound = float(TotalPutts) / gamesPlayed
        else:
            AvgGIRs = 0.0
            puttsPerRound = 0.0

        returnList = (Girs, "%.1f" % AvgGIRs, TotalPutts, "%.1f" % puttsPerRound, putts3AndUp, totalSands, sandSaves,
                      "%.1f" % sandSavesPercent)
        return returnList

    def obtainFairwayRelatedStatsFromList(self, aList, golferID, gamesPlayed):
        # (Fairways, AvgFairways, DrvAccuracy, AvgDrives,TotalMissR,RMPercent,TotalMissL,LMPercent)
        Fairways = 0
        DrvAccuracy = 0
        TotalYardsDrives = 0.0
        TotalMissR = 0
        RMPercent = 0.0
        TotalMissL = 0
        LMPercent = 0.0
        for i in range(len(aList)):
            Fairways = Fairways + aList[i][18]
            TotalMissR = TotalMissR + aList[i][20]
            TotalMissL = TotalMissL + aList[i][19]
            TotalYardsDrives = TotalYardsDrives + aList[i][21]

        TotalMiss = TotalMissL + TotalMissR
        allFairways = TotalMiss + Fairways

        if TotalMiss > 0:
            RMPercent = (float(TotalMissR) / float(allFairways)) * float(100)
            LMPercent = (float(TotalMissL) / float(allFairways)) * float(100)
        else:
            RMPercent = 0.0
            LMPercent = 0.0

        if gamesPlayed > 0:
            AvgFairways = float(Fairways) / gamesPlayed
        else:
            AvgFairways = 0.0

        if allFairways > 0:
            DrvAccuracy = (float(Fairways) / float(allFairways)) * float(100)
            AvgDrives = float(TotalYardsDrives) / float(Fairways)
        else:
            DrvAccuracy = 0.0
            AvgDrives = 0.0

        returnList = (
        Fairways, "%.1f" % AvgFairways, "%.1f" % DrvAccuracy, "%.1f" % AvgDrives, TotalMissR, "%.1f" % RMPercent,
        TotalMissL, "%.1f" % LMPercent)
        return returnList

    def underConstruction(self, *args):
        AppDisplayUnderConstruction(self)

    def _goToPage2(self, *args):
        self.reportPg1Frame.grid_remove()
        self.reportPg2Frame.grid()

    def _goToPage1(self, *args):
        self.reportPg2Frame.grid_remove()
        self.reportPg1Frame.grid()

    def addWindowSpecificCommand(self):
        self.editButton = AppButton(self.commandFrame, self.theCommandEditImage, self.theCommandEditImageGrey, AppBorderWidthList,
                                    self.edit)
        self.commandFrame.addNewCommandButton(self.editButton)
        self.editButton.addToolTip("Edit Form")

        self.cancelButton = AppButton(self.commandFrame, self.theCommandCancelImage, self.theCommandCancelImageGrey,
                                      AppBorderWidthList, self.cancel)
        self.commandFrame.addNewCommandButton(self.cancelButton)
        self.cancelButton.addToolTip("Cancel Current Operation")

    def fieldsDisable(self):
        pass
        self.selectedEndDate.disable()
        self.selectedStartDate.disable()
        self.selectedGolfer1.disable()
        self.selectedGolfer2.disable()
        self.selectedGolfer3.disable()
        self.selectedGolfer4.disable()

    def fieldsEnable(self):
        pass
        self.selectedEndDate.enable()
        self.selectedStartDate.enable()
        self.selectedGolfer1.enable()
        self.selectedGolfer2.enable()
        self.selectedGolfer3.enable()
        self.selectedGolfer4.enable()

    def fieldsClear(self):
        pass
        self.selectedEndDate.clear()
        self.selectedStartDate.clear()
        self.selectedGolfer1.clear()
        self.selectedGolfer2.clear()
        self.selectedGolfer3.clear()
        self.selectedGolfer4.clear()


    def setRequiredFields(self):
        # Table specific.  Will be defined in calling class
        pass
        self.selectedEndDate.setAsRequiredField()
        self.selectedStartDate.setAsRequiredField()
        self.selectedGolfer1.setAsRequiredField()
        self.selectedGolfer2.setAsRequiredField()
        self.selectedGolfer3.setAsRequiredField()
        self.selectedGolfer4.setAsRequiredField()


    def resetRequiredFields(self):
        pass
        self.selectedEndDate.resetAsRequiredField()
        self.selectedStartDate.resetAsRequiredField()
        self.selectedGolfer1.resetAsRequiredField()
        self.selectedGolfer2.resetAsRequiredField()
        self.selectedGolfer3.resetAsRequiredField()
        self.selectedGolfer4.resetAsRequiredField()

    def validateRequiredFields(self, NoMessageCheck = True):
        requiredFieldsEntered = True

        if len(self.selectedStartDate.get()) == 0:
            requiredFieldsEntered = False
            self.self.selectedStartDate.focus()

        elif len(self.selectedEndDate.get()) == 0:
            requiredFieldsEntered = False
            self.selectedEndDate.focus()

        elif len(self.selectedGolfer1.get()) == 0:
            requiredFieldsEntered = False
            self.selectedGolfer1.focus()

        elif len(self.selectedGolfer2.get()) == 0:
            requiredFieldsEntered = False
            self.selectedGolfer2.focus()

        elif len(self.selectedGolfer3.get()) == 0:
            requiredFieldsEntered = False
            self.selectedGolfer3.focus()

        elif len(self.selectedGolfer4.get()) == 0:
            requiredFieldsEntered = False
            self.selectedGolfer4.focus()

        if requiredFieldsEntered == False and NoMessageCheck == True:
            self.myMsgBar.requiredFieldMessage()

        return requiredFieldsEntered

    def _dateEnteredAction(self, *args):
        self._aDirtyMethod(None)

    def save(self):
        self.populateStatsTables()
        self.fieldsDisable()
        self.submitButton.disable()
        self.editButton.enable()
        self.cancelButton.disable()
        self.dirty = False


