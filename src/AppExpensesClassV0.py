##########################################################
#
#   Departments Table
#
#   This class implements the specific manipulation of the
# data for the ECE Departments Table that the TECH Shop requires.
#

import tkinter.tix as Tix
import tkinter.ttk as ttk
import AppCRUD as AppCRUD
import AppCRUDGolfers as CRUDGolfers
import tkinter.filedialog as Fd
import tkinter.messagebox as Mb
import textwrap, re

from tkinter.constants import *

from AppClasses import *
from AppConstants import *

from AppProc import *

from PIL import Image

#cursor.execute('''CREATE TABLE MBExpenses(
#   expenseID            INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
#   expenseDate          INTEGER    NOT NULL,
#   expenseItem          CHAR(100)  NOT NULL,
#   expenseAmount        REAL       NOT NULL,
#   currency             CHAR(5)    NOT NULL,
#   USExchangeRate       REAL,
#   expenseAdjusted      REAL,
#   golferID             INTEGER    NOT NULL,
#   lastModified INTEGER,
#   updaterID INTEGER,
#   FOREIGN KEY(golferID) REFERENCES Golfers(golferID) ON DELETE CASCADE
#   );''')


class expensesWindow(tableCommonWindowClass):
    def __init__(self, aWindow, aCloseCommand, loginData):
        self.windowTitle = 'Expenses'
        tableCommonWindowClass.__init__(self, aWindow, aCloseCommand, self.windowTitle, loginData[1])
        self.loginData = loginData
        self.accessLevel = self.loginData[1]
        #
        #  loginInfo format: password, access_Level, loginID, updaterID
        #
        if loginData[2] == 1:
            self.updaterFullname = "Super User"
        else:
            self.updaterFullname = CRUDGolfers.getGolferFullname(loginData[3])
        self.updaterID = self.loginData[2]
        self.displayCurUser.setUserName('Root')
        self.numFields = 0
        #######################################################################################
        # Inherited all from tableCommanWindowClass
        #
        #  This class intents to change the mainArea only.  Everything else must remain common
        #
        self.mainAreaRowCount = 0
        self.mainAreaColumnCount = 0
        self._getNewRecList()
        self._reDisplayRecordFrame()
        self.mainArea.rowconfigure(self.mainAreaRowCount, weight=1)

    ############################################################ Area to add window specific commands
    #        self.closeButton = ttk.Button(self.commandFrame, text='Test', style='CommandButton.TButton',
    #                                 command= lambda: self.sampleCommand())
    #        self.closeButton.grid(row=0, column=0, sticky=N+S)


    ################################################################# Redefined commands
    #
    #  invoke from the tableCommonWindows.  These commands are specific to each tables
    #  The specific actions are redefinded here.
    #
    #    def displayRec(self, *args):
    #        print("Displaying a Record: ", self.curRecNum)
    #
    #    def save(self):
    #        #
    #        #  Table specific.  Return a True if succesful saved.
    #        #
    #        print("Saving a record (re-defined)")
    #        return True

    def _enableWindowCommand(self):
        #
        #  Any command specific must added here
        #
        pass

    #        self.closeButton.config(state=NORMAL)

    def _disableWindowCommand(self):
        #
        #  Any command specific must added here
        #
        pass

    #        self.closeButton.config(state=DISABLED)

    def sampleCommand(self):
        self.myMsgBar.clearMessage()
        aMsg = 'INFO: Command area, specific to current window (Parts)'
        self.myMsgBar.newMessage('info', aMsg)

    def _getNewRecList(self):
        self.recList = AppCRUDGolfers.getGolfers()
        self.curRecNumV.set(self.curRecNumV.get())  # forces display of record number

    def _reDisplayRecordFrame(self):
        self.recordFrame.destroy()
        self._getNewRecList()
        if len(self.recList) > 0:
            self.numFields = len(self.recList[0])
            self._displayRecordFrame()

    def _displayRecordFrame(self):
        self.recordFrame = AppFrame(self.mainArea, 1)
        self.recordFrame.grid(row=self.mainAreaRowCount, column=self.mainAreaColumnCount,
                              columnspan=self.mainAreaColumnTotal, sticky=N + S + E + W)
        self.recordFrame.addAccentBackground()
        self.recordFrame.noBorder()

        if len(self.recList) > 0:
            self._buildWindowsFields(self.recordFrame)
            self.recordFrame.rowconfigure(self.recordFrame.row, weight=0)
            if self.curOp != 'add':
                self.displayRec()
                self.navigationEnable(self.accessLevel)
            new_order = (

            )

            for widget in new_order:
                widget.lift()
        else:
            wMsg = Tix.Label(self.recordFrame, text='No Records Found in Current List.', font=fontMonstrousB)
            wMsg.grid(row=0, column=0, columnspan=self.recordFrame.columnTotal)

    def _buildWindowsFields(self, aFrame):
        #
        colTotal = 2
        self.fieldsFrame = AppFieldsFrame(aFrame, colTotal)
        self.fieldsFrame.grid(row=0, column=0, sticky=N + S + E + W)
        self.fieldsFrame.noBorder()

        self.identificationFrame = AppBorderFrame(self.fieldsFrame, 2)
        self.identificationFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column,
                                      columnspan=self.fieldsFrame.columnTotal, sticky=N + S + E + W)
        self.identificationFrame.addTitle("Identification Section")

        self.identificationFrame.noStretchColumn(self.identificationFrame.column)
        self.identificationPICFrame = AppBorderFrame(self.identificationFrame, 5)
        self.identificationPICFrame.grid(row=self.identificationFrame.row, column=self.identificationFrame.column,
                                         sticky=N + W + E + S)
        self.identificationPICFrame.removeBorder()

        self.selectedPicture = AppPictureFrame(self.identificationPICFrame, golferDefaultPictureName, appGolferPICDir,
                                               golferPicWidth, golferPicHeight, self.myMsgBar)
        self.selectedPicture.grid(row=self.identificationPICFrame.row, column=self.identificationPICFrame.column,
                                  sticky=N + W + E + S)

        self.identificationFrame.addColumn()
        self.identificationDATAFrame = AppBorderFrame(self.identificationFrame, 2)
        self.identificationDATAFrame.grid(row=self.identificationFrame.row, column=self.identificationFrame.column,
                                          sticky=N + W + E + S)
        self.identificationDATAFrame.removeBorder()

        self.selectedFName = AppFieldEntry(self.identificationDATAFrame, 'First Name', 'V', 25)
        self.selectedFName.grid(row=self.identificationDATAFrame.row, column=self.identificationDATAFrame.column,
                                sticky=N)

        self.identificationDATAFrame.addColumn()
        self.selectedLName = AppFieldEntry(self.identificationDATAFrame, 'Last Name', 'V', 25)
        self.selectedLName.grid(row=self.identificationDATAFrame.row, column=self.identificationDATAFrame.column,
                                sticky=N)

        self.identificationDATAFrame.addRow()
        self.identificationDATAFrame.resetColumn()
        self.selectedBDay = AppFieldEntry(self.identificationDATAFrame, 'Birthday', 'V', 25)
        self.selectedBDay.grid(row=self.identificationDATAFrame.row, column=self.identificationDATAFrame.column,
                               sticky=N)
        # Empty row for separation
        #        empty = AppSpaceRow(self.fieldsFrame)
        #        empty.grid(row=self.fieldsFrame.row, column=0, sticky=N+S)

        self.fieldsFrame.addRow()
        self.lastUpdate = AppLastUpdateFormat(self.fieldsFrame, self.recList[self.curRecNumV.get()][self.numFields - 2],
                                              self.updaterFullname)
        self.lastUpdate.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, columnspan=colTotal,
                             sticky=E + N + S)

        self.fieldsDisable()

    #        self.fieldsFrame.rowconfigure(self.fieldsFrame.row, weight=0)
    #        self.headerList = ['Code', 'Name']
    #        for i in range(len(self.headerList)):
    #            aLabel = ttk.Label(self.fieldsFrame, text=self.headerList[i], style='RegularFieldTitle.TLabel')
    #            aLabel.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, sticky=N+S)
    #            self.fieldsFrame.column = self.fieldsFrame.column + 1

    def displayRec(self, *args):
        # Example
        # self.selectedCode.load(self.recList[self.curRecNumV.get()][1])
        self.selectedFName.load(self.recList[self.curRecNumV.get()][1])
        self.selectedLName.load(self.recList[self.curRecNumV.get()][2])
        self.selectedBDay.load(convertOrdinaltoString(self.recList[self.curRecNumV.get()][4]))
        self.selectedPicture.load(self.recList[self.curRecNumV.get()][3])

        if self.accessLevel == 'Root':
            pass

        self.fieldsDisable()

    def fieldsClear(self):
        self.selectedFName.clear()
        self.selectedLName.clear()
        self.selectedBDay.clear()
        self.selectedPicture.clear()

    def fieldsDisable(self):
        self.selectedFName.disable()
        self.selectedLName.disable()
        self.selectedBDay.disable()
        self.selectedPicture.disable()

    def fieldsEnable(self):
        self.selectedFName.enable()
        self.selectedLName.enable()
        self.selectedBDay.enable()
        self.selectedPicture.enable()

    ################################################################# Redefined commands
    #
    #  invoke from the tableCommonWindows.  These commands are specific to each tables
    #  The specific actions are redefinded here.
    #
    #    def displayRec(self, *args):
    #        print("Displaying a Record: ", self.curRecNumV.get())
    #        self._displayRecordFrame()
    def duplicate(self):
        self.cancelPopups()
        self.fieldsEnable()

    def find(self):
        self.clearPopups()
        # Table specific.  Will be defined in calling class
        self.fieldsClear()
        if len(self.findValues) > 0:
            pass
        self.fieldsEnable()

    def refresh(self):
        self.clearPopups()
        self._reDisplayRecordFrame()

    def delete(self):
        self._disableWindowCommand()
        aMsg = "Are you sure you want to delete record {0}?".format(self.recList[self.curRecNumV.get()][1])
        ans = Mb.askyesno('Delete Record', aMsg)
        if ans == True:
            #            AppDB.deleteParts(self.recList[self.curRecNumV.get()][0]) # remove from DB
            aMsg = "Record {0} has been deleted.".format(self.recList[self.curRecNumV.get()][1])
            self.myMsgBar.newMessage('info', aMsg)
            self.recList.pop(self.curRecNumV.get())  # remove from current list
            if self.curRecBeforeOp != 0:
                self.curRecNumV.set(self.curRecBeforeOp - 1)
            else:
                self.curRecNumV.set(self.curRecBeforeOp)
            self._displayRecordFrame()
        self._enableWindowCommand()
        self.myDatabaseBar.defaultState()
        self.navigationEnable(self.accessLevel)
        self.resetCurOp()

    def edit(self):
        self.fieldsEnable()

    #        self.selectedBasic.focus()

    def add(self):
        self.fieldsClear()
        self.fieldsEnable()

    def clearPopups(self):
        #        self.selectedDepartment.clearPopups()  # This is an example of the popups entry
        pass

    def save(self):
        #
        #  Table specific.  Return a True if succesful saved.
        #
        #
        #  House Cleaning in case a popup still exist
        #
        if self.curOp == 'add':

            AppCRUDGolfers.insertGolfers(
            )

            ID = []

            aMsg = "Record {0} has been added.".format(self.selectedBasic.get())
            self.myMsgBar.newMessage('info', aMsg)

            if self.curFind == True:
                anItem = (
                )
                self.recList.append(anItem)
                self.curRecNumV.set(len(self.recList) - 1)
                aNewList = sorted(self.recList, key=getKey)
                self.recList = aNewList
                newIdx = getIndex(ID[0][0], aNewList)
                self.curRecNumV.set(newIdx)
                self.displayRec()
            else:
                #
                #  If not in find mode, re_display the list and point
                # to the newly added item.
                #
                self._getNewRecList()
                newIdx = getIndex(ID[0][0], self.recList)
                if newIdx != -1:
                    self.curRecNumV.set(newIdx)
                self._reDisplayRecordFrame()
            self.resetCurOp()

        elif self.curOp == 'edit':
            #            AppCRUDGolfers.update_Golfer(self.recList[self.curRecNumV.get()][0], self., LName, golferPIC_LOC, golferBirthday)
            aMsg = "???????????????"
            self.myMsgBar.newMessage('info', aMsg)
            #
            # Add the item again but with modified data.
            #
            anItem = (
            )  # Remove the item that was modifiedse
            self.recList.insert(self.curRecNumV.get(), anItem)
            self.fieldsDisable()
            self.displayRec()
            self.resetCurOp()

        elif self.curOp == 'find':
            #            self.recListTemp = AppDB.findParts(self.selectedBasic.get(), self.selectedDescription.get(), self.selectedDescription2.get(), self.selectedNomenclature.get(),
            #                                           self.selectedFunction.get(), theSupplierID, departmentsID)
            self.recList = {}
            self.findValues = []
            self.findValues.append(self.selectedS_NUM.get())

            if len(self.recListTemp) == 0:
                aMsg = "WARNING: No records were found using the given search criterias."
                if self.prevFind == True:
                    self.displayCurFunction.setFindResultMode()
                    self.curFind = True
                else:
                    self.displayCurFunction.setDefaultMode()
                    self.curFind = False
                self.myMsgBar.newMessage('warning', aMsg)
            elif len(self.recList) == 1:
                self.curFind = True
                self.recList = self.recListTemp
                self.displayCurFunction.setFindResultMode()
                aMsg = "One record was found using the given search criterias."
                self.curRecNumV.set(0)
                self.myMsgBar.newMessage('info', aMsg)
            else:
                self.curFind = True
                self.recList = self.recListTemp
                self.displayCurFunction.setFindResultMode()
                aMsg = "{0} records were found using the given search criterias.".format(len(self.recList))
                self.curRecNumV.set(0)
                self.myMsgBar.newMessage('info', aMsg)
            self.displayRec()
        self.clearPopups()
        return True
