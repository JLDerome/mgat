import tkinter.tix as Tix
import tkinter.filedialog as Fd
import random, shutil
from tkinter import *
import tkinter.ttk as ttk
import textwrap
from tkinter.constants import *
from AppConstants import *
from AppClasses import *
import tkinter.messagebox as Mb
from AppProc import resizeImage, widgetEnter,convertOrdinaltoString, focusNext
from AppProc import getAge, convertDateStringToOrdinal, updateMyAppConstant
from AppProc import stripChar
from PIL import Image, ImageTk
from PIL.Image import NORMAL
import AppCRUDGolfers as CRUDGolfers
import AppCRUDAddresses as CRUDAddresses
import AppCRUDExpenses as CRUDExpenses
import AppCRUD as CRUD
from AppCRUDGames import calculateCurrentHdcp
from AppDialogClass import AppDisplayAbout, AppRecordDuplication, AppQuestionRequest
import calendar, re
import platform
from dateutil import parser

class popUpWindowClass(Tix.Frame):
    def __init__(self, parent, aMethod_FutureUse, windowTitle, loginData, mode):
        Tix.Frame.__init__(self, parent)
        parent.protocol("WM_DELETE_WINDOW", self._exitForm)
        self.accessLevel = loginData[1]
        self.aMethod_FutureUse = aMethod_FutureUse
        self.parent = parent
        self.curRecNumV = Tix.IntVar()
        self.curRecNumV.set(0)
        self.curRecBeforeOp = self.curRecNumV.get()
        self.prevFind = False
        self.curFind = False
        self.curOp = 'default'
        self.openningMode = mode
        self.recList = []
        self.accessLevel = ''
        self.updaterID = loginData[2]
        self.updaterFullname = ''
        self.mainAreaRowCount = 0
        self.mainAreaColumnCount = 0
        self.findValues = []
        self.viewSet = Tix.StringVar()
        self.viewSet.set("Active")  # Active, All, or Closed
        self.sortFieldID = 0  # Last Name
        self.sortID = Tix.StringVar()
        self.sortID.set('Default')  # Department, Workgroup, Last Name
        self.deletedV = Tix.StringVar()
        self.deletedV.trace('w', self._setAsDeleted)
        self.openRequiredImages()
        #################################################################### App Configuration data
        self.configDataRead = updateMyAppConstant(AppConfigFile)
        if self.configDataRead != None:
            if self.configDataRead[0][1] != None:
                self.startDate = convertOrdinaltoString(int(self.configDataRead[0][0]))
            else:
                self.startDate = "{0}-Feb-1".format(year)

            if self.configDataRead[0][1] != None:
                self.endDate = convertOrdinaltoString(int(self.configDataRead[0][1]))
            else:
                self.endDate = "{0}-Mar-1".format(year)
        else:
            self.startDate = "{0}-Feb-1".format(year)
            self.endDate = "{0}-Mar-1".format(year)
        ############################################################ Window Common Configuration
        self.dirty = False
        mainWInWidth = 400
        mainWinHeight = 400
        self.configure(width=mainWInWidth, height=mainWinHeight)
        self.columnCountTotal = 7
        for i in range(self.columnCountTotal):
            self.columnconfigure(i, weight=1)

        ############################################################ Window Title Area
        self.rowCount = 0
        self.columnCount = 0
        self.windowTitleLabel = Tix.Label(self, text=windowTitle, font=fontMonstrousB)
        self.windowTitleLabel.grid(row=self.rowCount, column=self.columnCount, columnspan=self.columnCountTotal, sticky=E + W)
        self.rowconfigure(self.rowCount, weight=0)

        self.displayCurFunction = AppFunctionDisplayLabel(self)
        self.displayCurFunction.grid(row=self.rowCount, column=self.columnCount, columnspan=self.columnCountTotal,
                                     sticky=E + S)

        self.displayCurUser = AppUserDisplayLabel(self, " ")
        self.displayCurUser.grid(row=self.rowCount, column=self.columnCount, columnspan=self.columnCountTotal,
                                 sticky=W + S)

        ############################################################ Main Frame Area
        self.rowCount = self.rowCount + 1
        self.columnCount = 0
        self.mainArea = Tix.Frame(self, border=3, relief='groove', takefocus=0)
        self.mainArea.grid(row=self.rowCount, column=self.columnCount, columnspan=self.columnCountTotal,
                           sticky=N + S + E + W)
        self.mainAreaColumnTotal = 21
        for i in range(self.mainAreaColumnTotal):
            self.mainArea.columnconfigure(i, weight=1)
        self.rowconfigure(self.rowCount, weight=1)


        ############################################################ Command Frame Area
        self.rowCount = self.rowCount + 1
        self.columnCount = 0


        #  This frame is used for commands specific to current class
        self.columnCount = self.columnCount + 1
        self.commandFrame = AppCommandFrame(self)
        self.commandFrame.grid(row=self.rowCount, column=self.columnCount, columnspan=self.columnCountTotal - 2,
                               sticky=N + S)

        self.submitButton = AppButton(self.commandFrame, self.theCommandSaveImage, self.theCommandSaveImageGrey,
                                      AppBorderWidthList, self._submitForm)
        self.commandFrame.addNewCommandButton(self.submitButton)
        self.submitButton.addToolTip("Submit/Save")

        self.addWindowSpecificCommand()


        self.clearButton = AppButton(self.commandFrame, self.theCommandClearImage, self.theCommandClearImageGrey, AppBorderWidthList,
                                     self._clearForm)
        self.commandFrame.addNewCommandButton(self.clearButton)
        self.clearButton.addToolTip("Clear Fields")


        self.exitButton = AppButton(self.commandFrame, self.theCommandExitImage, self.theCommandExitImageGrey, AppBorderWidthList,
                                    self._exitForm)
        self.commandFrame.addNewCommandButton(self.exitButton)
        self.exitButton.addToolTip("Exit Form")

        self.rowconfigure(self.rowCount, weight=0)

        ############################################################ Message Bar Area
        self.rowCount = self.rowCount + 1
        self.columnCount = 0
        self.myMsgBar = messageBar(self)
        self.myMsgBar.grid(row=self.rowCount, column=self.columnCount, columnspan=self.columnCountTotal, sticky=W + S,
                           padx=5)
        self.rowconfigure(self.rowCount, weight=0)

        ########################################################### Display records
        self.recordFrame = Tix.Frame(self.mainArea, takefocus=0)
        self.recordFrame.grid(row=0, column=0, columnspan=self.mainAreaColumnTotal, sticky=N + S + E + W)


        wMsg = Tix.Label(self.recordFrame, text='No Records Found', font=fontMonstrousB)
        wMsg.grid(row=0, column=0)


        ############################################################ Common Window Binding
        self.parent.bind('<End>', self.endKeyPressed)
        self.createMenu()
        self.bindAllSpecialKeys()

    #        self.loginWindow.bind('<Home>', self.focusLoginName)

    ############################################################ Table Specific Commands
    #
    #   These commands will be redefined for each table that invokes this class
    #   Manipulation of data is specific to each tables.
    #
    def changeFormTitle(self, aTitle):
        self.windowTitleLabel.config(text=aTitle)

    def openRequiredImages(self):
        self.theListAddImage = resizeImage(Image.open(addCommandRecImage), AppButtonSmallHeight, AppButtonSmallWidth)
        self.theListAddImageGREY = resizeImage(Image.open(addCommandRecImageDisable), AppButtonSmallHeight, AppButtonSmallWidth)
        self.theCommandAddImage = resizeImage(Image.open(addCommandRecImage), AppButtonLargeHeight, AppButtonLargeWidth)
        self.theCommandAddImageGrey = resizeImage(Image.open(addCommandRecImageDisable), AppButtonLargeHeight, AppButtonLargeWidth)

        self.theListViewImage = resizeImage(Image.open(viewCommandImage), AppButtonSmallHeight, AppButtonSmallWidth)
        self.theListViewImageGREY = resizeImage(Image.open(viewCommandImageDisable), AppButtonSmallHeight, AppButtonSmallWidth)
        self.theCommandViewImage = resizeImage(Image.open(viewCommandImage), AppButtonLargeHeight, AppButtonLargeWidth)
        self.theCommandViewImageGrey = resizeImage(Image.open(viewCommandImageDisable), AppButtonLargeHeight, AppButtonLargeWidth)

        self.theListArchiveImage = resizeImage(Image.open(archiveCommandImage), AppButtonSmallHeight, AppButtonSmallWidth)
        self.theListArchiveImageGREY = resizeImage(Image.open(archiveCommandImageDisable), AppButtonSmallHeight, AppButtonSmallWidth)

        self.theListEditImage = resizeImage(Image.open(editRecImage), AppButtonLargeWidth,
                                                                AppButtonLargeHeight)
        self.theListEditImageGREY = resizeImage(Image.open(editRecImageDisable), AppButtonLargeWidth,
                                                                AppButtonLargeHeight)
        self.theCommandEditImage = resizeImage(Image.open(editRecImage), AppButtonLargeWidth,
                                                                AppButtonLargeHeight)
        self.theCommandEditImageGrey = resizeImage(Image.open(editRecImageDisable), AppButtonLargeWidth,
                                                                AppButtonLargeHeight)

        self.theListDeleteImage = resizeImage(Image.open(deleteCommandRecImage), AppButtonLargeWidth,
                                                                AppButtonLargeHeight)
        self.theListDeleteImageGREY = resizeImage(Image.open(deleteCommandRecImageDisable), AppButtonLargeWidth,
                                                                AppButtonLargeHeight)
        self.theCommandDeleteImage = resizeImage(Image.open(deleteCommandRecImage), AppButtonLargeWidth,
                                                                AppButtonLargeHeight)
        self.theCommandDeleteImageGrey = resizeImage(Image.open(deleteCommandRecImageDisable), AppButtonLargeWidth,
                                                                AppButtonLargeHeight)

        self.theCommandSaveImage = resizeImage(Image.open(saveRecImage), AppButtonLargeWidth,
                                                                AppButtonLargeHeight)
        self.theCommandSaveImageGrey = resizeImage(Image.open(saveRecImageDisable), AppButtonLargeWidth,
                                                                AppButtonLargeHeight)

        self.theCommandClearImage = resizeImage(Image.open(clearCommandImage), AppButtonLargeWidth,
                                                                AppButtonLargeHeight)
        self.theCommandClearImageGrey = resizeImage(Image.open(clearCommandImageDisable),AppButtonLargeWidth,
                                                                AppButtonLargeHeight)

        self.theCommandExitImage = resizeImage(Image.open(exitCommandImage), AppButtonLargeWidth,
                                                                AppButtonLargeHeight)
        self.theCommandExitImageGrey = resizeImage(Image.open(exitCommandImageDisable), AppButtonLargeWidth,
                                                                AppButtonLargeHeight)

        self.theCommandCancelImage = resizeImage(Image.open(cancelCommandRecImage), AppButtonLargeWidth,
                                                                AppButtonLargeHeight)
        self.theCommandCancelImageGrey = resizeImage(Image.open(cancelCommandRecImageDisable), AppButtonLargeWidth,
                                                                AppButtonLargeHeight)

        self.theCommandReloadImage = resizeImage(Image.open(refreshRecImage), AppButtonLargeWidth,
                                                                AppButtonLargeHeight)
        self.theCommandReloadImageGrey = resizeImage(Image.open(refreshRecImageDisable), AppButtonLargeWidth,
                                                                AppButtonLargeHeight)

        self.theCommandActivateScorecardImage = resizeImage(Image.open(scorecardActivateImage), AppButtonLargeWidth,
                                                                AppButtonLargeHeight)
        self.theCommandActivateScorecardImageGrey = resizeImage(Image.open(scorecardActivateImageDisable), AppButtonLargeWidth,
                                                                AppButtonLargeHeight)

        self.theNextPageImage = resizeImage(Image.open(nextPageImage), AppPageNavButtonWidth, AppPageNavButtonHeight)
        self.thePreviousPageImage = resizeImage(Image.open(previousPageImage), AppPageNavButtonWidth, AppPageNavButtonHeight)

        self.theListAddRoundImage = resizeImage(Image.open(addRoundCommandImage), AppButtonSmallHeight, AppButtonSmallWidth)
        self.theListAddRoundImageGREY = resizeImage(Image.open(addRoundCommandImageDisable), AppButtonSmallHeight,
                                                    AppButtonSmallWidth)

        self.theListDuplicateImage = resizeImage(Image.open(duplicateRecImage), AppButtonSmallHeight, AppButtonSmallWidth)
        self.theListDuplicateImageGREY = resizeImage(Image.open(duplicateRecImageDisable), AppButtonSmallHeight, AppButtonSmallWidth)

    def bindAllSpecialKeys(self):
        self.parent.bind('<Alt-E>', self._exitForm)
        self.parent.bind('<Alt-e>', self._exitForm)

    def unBindAllSpecialKeys(self):
        self.parent.unbind('<Alt-e>')
        self.parent.unbind('<Alt-E>')

    def addWindowSpecificCommand(self):
        pass

    def fieldsDisable(self):
        # Table specific
        pass


    def fieldsEnable(self):
        # Table specific
        pass

    def fieldsClear(self):
        # Table specific
        pass

    def _submitForm(self, *args):
        pass
        # Must be defined in the popUp window.

    def _clearForm(self, *args):
        pass
        # Must be defined in the popUp window.

    def _exitForm(self, *args):
        if self.dirty == True:
            aMsg = "The record currently in {0} mode has not been saved.\n" \
                   "Exiting now would result in lost of data.\n" \
                   "Do you wish to continue with the exit command?".format(self.openningMode)
            ans = AppQuestionRequest(self, "WARNING, Data Not Saved", aMsg)
            if ans.result == True:
                self.parent.destroy()
        else:
            self.parent.destroy()

    def addCommand(self, *args):
        self.curOp = 'add'
        self.fieldsClear()
#        self.curRecBeforeOp = self.curRecNumV.get()
        self.displayCurFunction.setAddMode()
#        if len(self.recList) == 0:
#            self._displayRecordFrame()
        self.fieldsEnable()
        self.add()

    def editCommand(self, *args):
        self.curOp = 'edit'
        self.displayCurFunction.setEditMode()
        self.fieldsEnable()
        self.edit()

    def viewCommand(self, *args):
        self.curOp = 'view'
        self.displayCurFunction.setViewMode()
        self.fieldsEnable()
        self.view()

    def duplicateCommand(self, args):
        self.curOp = 'duplicate'
        self.displayCurFunction.setDuplicateMode()
        self.fieldsEnable()
        self.duplicate()

    def add(self, *args):
        pass

    def edit(self, *args):
        pass

    def createMenu(self):
        self.menubar = Menu(self.parent, foreground=AppDefaultForeground, font=AppDefaultMenuFont)
        self.filemenu = Menu(self.menubar, foreground=AppDefaultForeground, font=AppDefaultMenuFont, tearoff=0)
        #        filemenu.add_command(label="Close", command=self.donothing)
        self.filemenu.add_separator()
        self.filemenu.add_command(label="Exit", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                  command=self._exitForm)
        self.menubar.add_cascade(label="File", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                 menu=self.filemenu)

        self.tableSpecificMenu()

        self.helpmenu = Menu(self.menubar, tearoff=0)
        self.helpmenu.add_command(label="About...", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                  command=lambda: self.displayAbout())
        self.menubar.add_cascade(label="Help", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                 menu=self.helpmenu)

        self.parent.config(menu=self.menubar)

    def deleteActionFromList(self, tableName, anID, anUpdateMethod=None):
        self.unBindAllSpecialKeys()
        aMsg = "Are you sure you want to delete the current record?"
        ans = AppQuestionRequest(self, "Delete Record - {0}".format(tableName), aMsg)
        if ans.result == True:
            CRUD.delete(tableName, anID, convertDateStringToOrdinal(todayDate),
                         self.updaterID)  # remove from DB
            aMsg = "Record ID {1} from table {0} has been deleted.".format(tableName, anID)
            self.myMsgBar.newMessage('info', aMsg)

        if anUpdateMethod != None:
            anUpdateMethod()
        self.dirty = False
        self.bindAllSpecialKeys()

    def displayAbout(self, *args):
        AppDisplayAbout(self)

    def _aDirtyMethod(self, *args):
        self.dirty = True

    def _setAsDeleted(self, *args):
        pass
        #
        #  At a minimun, record action are disabled.
        #  Additionnal commands can be added, see person class
        #  for override example for this method.
        #
        if self.deletedV.get() == 'Y':
            self.recordFrame.config(highlightcolor=AppDeletedHighlightColor, takefocus=0)
            self.recordFrame.focus()
        else:
            self.recordFrame.config(highlightcolor=AppDefaultForeground, takefocus=0)
            self.recordFrame.focus()

    def endKeyPressed(self, Event):
        pass
        self.submitButton.focus()

    def sort(self):
        pass

    def getTableData(self):
        pass  # Table specific

    def _getNewRecList(self):
        self.getTableData()
        self.curRecNumV.set(self.curRecNumV.get())

        if len(self.recAll) > 0:
            self.numberOfFields = len(self.recAll[0])
        else:
            self.numberOfFields = 0
        self.recList = self.recAll
        self.curRecNumV.set(self.curRecNumV.get())  # forces display of record number

    def _reDisplayRecordFrame(self):
        self.recordFrame.destroy()
        self._displayRecordFrame()

    def _buildWindowsFields(self, aFrame):
        pass

    def _displayRecordFrame(self):
        self.recordFrame = AppFrame(self.mainArea, 1)
        self.recordFrame.grid(row=self.mainAreaRowCount, column=self.mainAreaColumnCount,
                              columnspan=self.mainAreaColumnTotal, sticky=N + S + E + W)
        self.recordFrame.addAccentBackground()
        self.recordFrame.addDeletedHighlight()
        self.recordFrame.noBorder()
        self.recordFrame.stretchCurrentRow()

        self._buildWindowsFields(self.recordFrame)
        self.recordFrame.rowconfigure(self.recordFrame.row, weight=1)
#            self.navigationEnable(self.accessLevel)
#            self.basicViewsList('Active')
#        self.displayRec()

#        elif self.curOp == 'add':
#            self._buildWindowsFields(self.recordFrame)
#            self.recordFrame.rowconfigure(self.recordFrame.row, weight=1)#

#        else:
#            wMsg = Tix.Label(self.recordFrame, text='No Records Found in Current List.', font=fontMonstrousB)
#            wMsg.grid(row=0, column=0, columnspan=self.recordFrame.columnTotal)
#
#   MUST BE REVIEWED
#
    def basicViewsList(self, viewID):
        if self.curOp == 'add' or self.curOp == 'edit':
            aMsg = "ERROR: Invalid request. Compete add/edit operation before changing views."
            self.myMsgBar.newMessage('error', aMsg)
        else:
            pass