'''
Created on Feb 29, 2016

@author: derome
'''
import tkinter.ttk as ttk
from MyGolfApp2016.AppConstants import fontAverage, AppDefaultForeground, fontMonstrousB, fontBigB, fontHuge2I, commandButtonWidth, commandButtonHeight

style=ttk.Style()
################################ General Styles
style.configure('.', font=fontAverage , foreground=AppDefaultForeground)
style.configure('WindowTitle.TLabel', font=fontMonstrousB)
################################ Login Styles
style.configure('Login.TButton', font=fontBigB)
style.configure('LoginFieldTitle.TLabel', font=fontBigB)
################################ Menu Style
style.configure('MenuItems.TLabel', font=fontHuge2I)
################################ Command Styles (buttons, etc)
style.configure('CommandButton.TButton', font=fontAverage, width=commandButtonWidth, height=commandButtonHeight)
################################ TooTip Styles
style.configure('toolTip.TLabel', justify='left', foreground=AppDefaultForeground, relief='solid', borderwidth=1,
                    font=fontAverage)
