from AppConstants import exportGolfersFilename, exportCourseFilename, exportTeeFilename, exportMBExpensesFilename
from AppConstants import exportTeeDetailsFilename,exportGamesFilename,exportGameDetailsFilename,exportAddressFilename
from AppConstants import exportLoginAccessFilename, AppDefaultLocation, BackupDirDefaultName
from AppConstants import import2015GolferDataFilename, import2015CoursesDataFilename, import2015TeesDataFilename
from AppConstants import import2015TeeDetailsDataFilename, import2015ExpensesDataFilename,import2015GamesDataFilename
from AppConstants import import2015GameDetailsDataFilename, import2015AddressesDataFilename, DBYearRevision
import AppCRUDGolfers as CRUDGolfers
import AppCRUDCourses as CRUDCourses
import AppCRUDTees as CRUDTees
import AppCRUDExpenses as CRUDExpenses
import AppCRUDGames as CRUDGames
import AppCRUDAddresses as CRUDAddresses
import AppCRUDLoginAccess as AppCRUDLoginAccess
import tkinter.messagebox as Mb

def backupMyGolfDB():
    aMsg = "My Golf App " + DBYearRevision  + " Backup Database initiated.\nOld files will be deleted and replaced with current data.\nDo you still want to proceed with the backup command?".format(
        AppDefaultLocation + BackupDirDefaultName)
    ans = Mb.askyesno('Backup My Golf App ' + DBYearRevision  + ' Database', aMsg)
    if ans == True:
        amtGolfers = CRUDGolfers.exportCSV_golfers(exportGolfersFilename)
        amtCourses = CRUDCourses.exportCSV_courses(exportCourseFilename)
        amtTees = CRUDTees.exportCSV_tees(exportTeeFilename)
        amtTeeDetails = CRUDTees.exportCSV_teeDetails(exportTeeDetailsFilename)
        amtMBExpenses = CRUDExpenses.exportCSV_MBExpenses(exportMBExpensesFilename)
        amtGames = CRUDGames.exportCSV_games(exportGamesFilename)
        amtGameDetails = CRUDGames.exportCSV_gameDetails(exportGameDetailsFilename)
        amtAddresses = CRUDAddresses.exportCSV_addresses(exportAddressFilename)
        amtLoginAccess = AppCRUDLoginAccess.exportCSV_LoginAccess(exportLoginAccessFilename)

        aMsg = "My Golf App " + DBYearRevision  + " Backup Database was completed.\n  1. Source directory used {0}/{1}.\n" \
               "  2. {2} Golfers exported.\n  3. {3} Courses exported\n  4. {4} Tees exported.\n" \
               "  5. {5} Tee Details exported.\n  6. {6} Expenses exported.\n  7. {7} Games exported.\n" \
               "  8. {8} Game Details exported.\n  9. {9} Addresses exported.\n  10. {10} Login Access Records exported." \
            .format(AppDefaultLocation, BackupDirDefaultName, amtGolfers, amtCourses, amtTees,
                    amtTeeDetails, amtMBExpenses, amtGames, amtGameDetails, amtAddresses, amtLoginAccess)
        Mb.showinfo('Backup My Golf App ' + DBYearRevision  + ' Database', aMsg)
    else:
        aMsg = "My Golf App " + DBYearRevision  + " Backup Database command was cancelled."
        Mb.showinfo('Backup My Golf App ' + DBYearRevision  + ' Database', aMsg)

def restoreMyGolfDB():
    aMsg = "My Golf App " + DBYearRevision  + " Restore Database initiated.\nA fresh My Golf Database must be installed (no data)\n" \
           "NON Empty database will cause SERIOUS corruptions.\nDo you still want to proceed with the backup command?".format(
        AppDefaultLocation + BackupDirDefaultName)
    ans = Mb.askyesno('Restore My Golf App ' + DBYearRevision  + ' Database', aMsg)
    if ans == True:
        amtGolfers = CRUDGolfers.importCSV_golfers(exportGolfersFilename)
        amtCourses = CRUDCourses.importCSV_courses(exportCourseFilename)
        amtTees = CRUDTees.importCSV_tees(exportTeeFilename)
        amtTeeDetails = CRUDTees.importCSV_teeDetails(exportTeeDetailsFilename)
        amtMBExpenses = CRUDExpenses.importCSV_MBExpenses(exportMBExpensesFilename)
        amtGames = CRUDGames.importCSV_games(exportGamesFilename)
        amtGameDetails = CRUDGames.importCSV_gameDetails(exportGameDetailsFilename)
        amtAddresses = CRUDAddresses.importCSV_addresses(exportAddressFilename)
        amtLoginAccess = AppCRUDLoginAccess.importCSV_LoginAccess(exportLoginAccessFilename)

        aMsg = "My Golf App " + DBYearRevision  + " Backup Database was completed.\n  1. Source directory used {0}/{1}.\n" \
               "  2. {2} Golfers restored.\n  3. {3} Courses restored\n  4. {4} Tees restored.\n" \
               "  5. {5} Tee Details restored.\n  6. {6} Expenses restored.\n  7. {7} Games restored.\n" \
               "  8. {8} Game Details restored.\n  9. {9} Addresses restored. \n {10} Login Access restored." \
            .format(AppDefaultLocation, BackupDirDefaultName, amtGolfers, amtCourses, amtTees,
                    amtTeeDetails, amtMBExpenses, amtGames, amtGameDetails, amtAddresses, amtLoginAccess)
        Mb.showinfo('Restore My Golf App ' + DBYearRevision  + ' Database', aMsg)
    else:
        aMsg = "My Golf App " + DBYearRevision  + " Restore Database command was cancelled."
        Mb.showinfo('Restore My Golf App ' + DBYearRevision  + ' Database', aMsg)

def convert2015MyGolfData():
    aMsg = "My Golf App 2015 Convert Database initiated.\nCAN BE PERFORMED ONLY ONCE.\nDo you still want to proceed with the conversion command?".format(
        AppDefaultLocation + BackupDirDefaultName)
    ans = Mb.askyesno('Converting My Golf 2015 Database', aMsg)
    if ans == True:
        amtGolfers = CRUDGolfers.importGolfersCSV_MyGolf2015(import2015GolferDataFilename)
        amtCourses = CRUDCourses.importCoursesCSV_MyGolf2015(import2015CoursesDataFilename)
        amtTees = CRUDTees.importTeesCSV_MyGolf2015(import2015TeesDataFilename)
        amtTeeDetails = CRUDTees.importTeeDetailsCSV_MyGolf2015(import2015TeeDetailsDataFilename)
        amtMBExpenses = CRUDExpenses.importExpensesCSV_MyGolf2015(import2015ExpensesDataFilename)
        amtGames = CRUDGames.importGamesCSV_MyGolf2015(import2015GamesDataFilename)
        amtGameDetails = CRUDGames.importGameDetailsCSV_MyGolf2015(import2015GameDetailsDataFilename)
        amtAddresses = CRUDAddresses.importAddressesCSV_MyGolf2015(import2015AddressesDataFilename)

        aMsg = "My Golf App 2015 Convert Database was completed.\n  1. Source directory used {0}/{1}.\n" \
               "  2. {2} Golfers imported.\n  3. {3} Courses imported.\n  4. {4} Tees imported.\n" \
               "  5. {5} Tee Details imported.\n  6. {6} Expenses imported.\n  7. {7} Games imported.\n" \
               "  8. {8} Game Details imported.\n  9. {9} Addresses imported." \
            .format(AppDefaultLocation, BackupDirDefaultName, amtGolfers, amtCourses,
                    amtTees, amtTeeDetails, amtMBExpenses, amtGames, amtGameDetails, amtAddresses)
        Mb.showinfo('Converting My Golf 2015 Data', aMsg)
    else:
        aMsg = "My Golf App 2015 Convert Database command was cancelled."
        Mb.showinfo('Converting My Golf 2015 Data', aMsg)

def restoreGameDetailsUpgrade():
    aMsg = "My Golf App " + DBYearRevision  + " Restore Database with New Game Details initiated.\nA fresh My Golf Database must be installed (no data)\n" \
           "NON Empty database will cause SERIOUS corruptions.\nDo you still want to proceed with the backup command?".format(
        AppDefaultLocation + BackupDirDefaultName)
    ans = Mb.askyesno('Restore My Golf App ' + DBYearRevision  + ' Database', aMsg)
    if ans == True:
        amtGolfers = CRUDGolfers.importCSV_golfers(exportGolfersFilename)
        amtCourses = CRUDCourses.importCSV_courses(exportCourseFilename)
        amtTees = CRUDTees.importCSV_tees(exportTeeFilename)
        amtTeeDetails = CRUDTees.importCSV_teeDetails(exportTeeDetailsFilename)
        amtMBExpenses = CRUDExpenses.importCSV_MBExpenses(exportMBExpensesFilename)
        amtGames = CRUDGames.importCSV_games(exportGamesFilename)
        amtGameDetails = CRUDGames.importCSV_gameDetails(exportGameDetailsFilename)
#        amtGameDetails = 0
        amtAddresses = CRUDAddresses.importCSV_addresses(exportAddressFilename)
        amtLoginAccess = AppCRUDLoginAccess.importCSV_LoginAccess(exportLoginAccessFilename)

        aMsg = "My Golf App " + DBYearRevision  + " Backup Database with Game Details Upgrade was completed.\n  1. Source directory used {0}/{1}.\n" \
               "  2. {2} Golfers restored.\n  3. {3} Courses restored\n  4. {4} Tees restored.\n" \
               "  5. {5} Tee Details restored.\n  6. {6} Expenses restored.\n  7. {7} Games restored.\n" \
               "  8. {8} Game Details restored.\n  9. {9} Addresses restored. \n {10} Login Access restored." \
            .format(AppDefaultLocation, BackupDirDefaultName, amtGolfers, amtCourses, amtTees,
                    amtTeeDetails, amtMBExpenses, amtGames, amtGameDetails, amtAddresses, amtLoginAccess)
        Mb.showinfo('Restore My Golf App ' + DBYearRevision  + ' Database - Game Details', aMsg)
    else:
        aMsg = "My Golf App " + DBYearRevision  + " Restore Database  with Game Details Upgrade command was cancelled."
        Mb.showinfo('Restore My Golf App ' + DBYearRevision  + ' Database with Game Details Upgrade', aMsg)