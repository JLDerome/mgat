import matplotlib, numpy, sys
import matplotlib.pyplot as plt
matplotlib.use('TkAgg')
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
if sys.version_info[0] < 3:
    import Tkinter as Tk
else:
    import tkinter as Tk

root = Tk.Tk()

f = Figure(figsize=(5,4), dpi=100)
ax = f.add_subplot(111)

data = (20, 35, 30, 35, 27)

ind = numpy.arange(5)  # the x locations for the groups
width = .5

rects1 = ax.bar(ind, data, width)

canvas = FigureCanvasTkAgg(f, master=root)
canvas.show()
canvas.get_tk_widget().pack(side=Tk.TOP, fill=Tk.BOTH, expand=1)

# labels = ["Oranges", "Bananas", "Apples", "Kiwis", "Grapes", "Pears"]
# values = [0.1, 0.4, 0.1, 0.2, 0.1, 0.1]
#
#  # now to get the total number of failed in each section
# actualFigure = plt.figure(figsize = (8,8))
# actualFigure.suptitle("Fruit Stats", fontsize = 22)
#
# explode = list()
# for k in labels:
#     explode.append(0.1)
#
# # pie= plt.pie(values, labels=labels, explode = explode, shadow=True)
# pie= plt.pie(values, labels=labels, shadow=False)
#
# canvas = FigureCanvasTkAgg(actualFigure, master=root)
# canvas.get_tk_widget().pack()
# canvas.show()

# Website example
#https://pythonspot.com/en/matplotlib-pie-chart/
#https://pythonspot.com/en/download-matplotlib-examples/

labels = ['Cookies', 'Jellybean', 'Milkshake', 'Cheesecake']
sizes = [38.4, 40.6, 20.7, 10.3]
colors = ['yellowgreen', 'gold', 'lightskyblue', 'lightcoral']
 # now to get the total number of failed in each section
actualFigure = plt.figure(figsize = (8,8))
actualFigure.suptitle("Items", fontsize = 22)

patches, texts, autopct = plt.pie(sizes, colors=colors, autopct='%1.1f%%', shadow=False, startangle=90)
plt.legend(patches, labels, loc="best")
plt.axis('equal')
# plt.tight_layout()

canvas = FigureCanvasTkAgg(actualFigure, master=root)
canvas.get_tk_widget().pack()
canvas.show()

Tk.mainloop()

# labels = ["Oranges", "Bananas", "Apples", "Kiwis", "Grapes", "Pears"]
# values = [0.1, 0.4, 0.1, 0.2, 0.1, 0.1]
#
#  # now to get the total number of failed in each section
# actualFigure = plt.figure(figsize = (8,8))
# actualFigure.suptitle("Fruit Stats", fontsize = 22)
#
# #explode=(0, 0.05, 0, 0)
# # as explode needs to contain numerical values for each "slice" of the pie chart (i.e. every group needs to have an associated explode value)
# explode = list()
# for k in labels:
#     explode.append(0.1)
#
# pie= plt.pie(values, labels=labels, explode = explode, shadow=True)
#
# canvas = FigureCanvasTkAgg(actualFigure, self)
# canvas.get_tk_widget().pack()
# canvas.show()