##########################################################
#
#   Personnel Table
#
#   This class implements the specific manipulation of the
# data for the ECE Personnel that the TECH Shop requires.
#

import tkinter.tix as Tix
import tkinter.ttk as ttk

from tkinter.constants import BOTH, N, W, S, E

from MyGolfApp2016.AppMyClasses import tableCommonWindowClass

from MyGolfApp2016.AppConstants import personnelDefaultPic, personnelPicWidth, personnelPicHeight
from MyGolfApp2016.AppConstants import AppDefaultForeground, fontMonstrousB

from MyGolfApp2016.AppProcFROMECE import resizeImage

from PIL import Image

class NFLWindow(tableCommonWindowClass):
    def __init__(self, aWindow, aCloseCommand, loginData):
        tableCommonWindowClass.__init__(self, aWindow, aCloseCommand, 'ECE Tech Shop NFL Window', loginData[1])
        self.loginData = loginData
        