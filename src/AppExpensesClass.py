##########################################################
#
#   Departments Table
#
#   This class implements the specific manipulation of the
# data for the ECE Departments Table that the TECH Shop requires.
#

import tkinter.tix as Tix
import tkinter.ttk as ttk
#import ECECRUD as AppDB
#import ECECRUDPerson as AppDBPerson
#from ECECRUDDepartments import getDepartmentsDictionaryInfo, getDepartmentsCode
#from ECEAppDialogClass import AppDuplicatePerson, AppDisplayAbout, AppQuestionRequest
#import tkinter.filedialog as Fd
#import tkinter.messagebox as Mb
import textwrap, re, operator

from tkinter.constants import *

#from ECEAppMyClasses import *
#from ECEAppConstants import *

#from ECEShopProc import *

from PIL import Image

import tkinter.tix as Tix
import tkinter.ttk as ttk
import AppCRUD as CRUD
import AppCRUDGolfers as CRUDGolfers
import AppCRUDGames as CRUDGames
import AppCRUDTees as CRUDTees
import AppCRUDExpenses as CRUDExpenses
import AppCRUDCourses as CRUDCourses
import tkinter.filedialog as Fd
import tkinter.messagebox as Mb
import textwrap, re
from dateutil import parser

from tkinter.constants import *

from tableCommonWindowClass import tableCommonWindowClass
from AppClasses import *
from AppConstants import *
from AppProc import *

courseList = []
golfersList = []
teeList = []

class expensesWindow(tableCommonWindowClass):
    def __init__(self, aWindow, aCloseCommand, loginData):
        #
        #  loginInfo format: Updater Full Name, access_Level, loginID
        #
        self.windowTitle = 'Expenses'
        self.tableName = 'MBExpenses'
        tableCommonWindowClass.__init__(self, aWindow, aCloseCommand, self.windowTitle, loginData, self.tableName)
        self.loginData = loginData
        self.accessLevel = self.loginData[1]
        self.updaterID = self.loginData[2]
        self.updaterFullname = self.loginData[0]
        self.aCloseCommand = aCloseCommand
        self.aWindow = aWindow

        #
        #  loginInfo format: password, access_Level, loginID, updaterID
        #
        self.updaterFullname = loginData[0]
        self.updaterID = self.loginData[2]
        self.displayCurUser.setUserName(self.updaterFullname)

        self.configDataRead = updateMyAppConstant(AppConfigFile)
        self.golferComboBoxData = CRUDGolfers.getGolferCB()
        self.golferDictionary = {}
        self.reverseGolferDictionary = {}
        for i in range(len(self.golferComboBoxData)):
            golfer_full = '''{0} {1}'''.format(self.golferComboBoxData[i][1], self.golferComboBoxData[i][2])
            self.golferDictionary.update({golfer_full: self.golferComboBoxData[i][0]})
            self.reverseGolferDictionary.update({self.golferComboBoxData[i][0]: golfer_full})
        self.bigSpender = self.reverseGolferDictionary[int(self.configDataRead[0][9])]


        #######################################################################################
        # Inherited all from tableCommanWindowClass
        #
        #  This class intents to change the mainArea only.  Everything else must remain common
        #
        self.mainAreaRowCount = 0
        self.mainAreaColumnCount = 0
        self._reDisplayRecordFrame()
        self.mainArea.rowconfigure(self.mainAreaRowCount, weight=1)
        self.windowViewDefault()

    def tableSpecificMenu(self):
        pass

#        self.viewsmenu.add_command
#        self.viewsmenu.add_command(label="Active Golfers", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
#                                   command=lambda viewID='Active Golfers': self.basicViewsList(viewID))

        sortsmenu = Menu(self.menubar, foreground=AppDefaultForeground, font=AppDefaultMenuFont, tearoff=0)
        sortsmenu.add_command(label="Date (Default)", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Default': self.sortingList(sortID))
        sortsmenu.add_command(label="Golfers", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Golfers': self.sortingList(sortID))
        sortsmenu.add_command(label="Description", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Description': self.sortingList(sortID))
        sortsmenu.add_command(label="Currency", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Currency': self.sortingList(sortID))
        #
        # Next Command actually puts the menu up
        #
        self.menubar.add_cascade(label="Sorts", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                 menu=sortsmenu)
    def sort(self):
        pass
        if self.curOp == 'default':
            currentID = self.recList[self.curRecNumV.get()][0]
            if self.sortID.get() == 'Default':
                # Sorting on Date
                self.recList = sorted(self.recList, key=operator.itemgetter(1))
            elif self.sortID.get() == 'Description':
                self.recList.sort(key=lambda row: row[2])
            elif self.sortID.get() == 'Golfers':
                self.recList.sort(key=lambda row: row[7])
            elif self.sortID.get() == 'Currency':
                self.recList.sort(key=lambda row: row[4])
            else:
                # Based on uniqueID
                self.recList.sort(key=lambda row: row[0])
            newIdx = getIndex(currentID, self.recList)
            self.curRecNumV.set(newIdx)
            self.displayRec()
        else:
            aMsg = "ERROR: Invalid request. Must be executed in default state only."
            self.myMsgBar.newMessage('error', aMsg)

    def getTableData(self):
        self.recAll = CRUDExpenses.get_MBExpenses()

    def _buildWindowsFields(self, aFrame):
        #cursor.execute('''CREATE TABLE MBExpenses(
        #   uniqueID            INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
        #   expenseDate          INTEGER    NOT NULL,
        #   expenseItem          CHAR(100)  NOT NULL,
        #   expenseAmount        REAL       NOT NULL,
        #   currency             CHAR(5)    NOT NULL,
        #   USExchangeRate       REAL,
        #   expenseAdjusted      REAL,
        #   golferID             INTEGER    NOT NULL,
        #   deleted              CHAR(1)    NOT NULL,
        #   lastModified         INTEGER,
        #   updaterID            INTEGER,
        #   FOREIGN KEY(golferID) REFERENCES Golfers(uniqueID) ON DELETE CASCADE
        #   );''')
        colTotal = 1
        self.fieldsFrame = AppFieldsFrame(aFrame, colTotal)
        self.fieldsFrame.grid(row=0, column=0, sticky=N + S + E + W)

        ########################################################### Main Identification Section
        self.expenseFieldFrame = AppColumnAlignFieldFrame(self.fieldsFrame)
        self.expenseFieldFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column,
                                    columnspan=colTotal, sticky=E + W + N + S)

        self.selectedExpenseDate = AppFieldEntryDate(self.expenseFieldFrame, None, None, appDateWidth,
                                                     self._dateEnteredAction, self.myMsgBar, self._aDirtyMethod)
        self.expenseFieldFrame.addNewField('Expense Date', self.selectedExpenseDate)

        self.selectedExpenseDescription = AppFieldEntry(self.expenseFieldFrame, None, None,
                                                        appDescriptionWidth, self._aDirtyMethod)
        self.expenseFieldFrame.addNewField('Item Description', self.selectedExpenseDescription)

        self.golferCBData = AppCBList(CRUDGolfers.getGolfersDictionaryInfo())
        self.selectedGolfer = AppSearchLB(self.expenseFieldFrame, self, None, None,
                                           self.golferCBData.getList(), AppFullNameWidth, None, self.myMsgBar, self._aDirtyMethod, None)
        self.expenseFieldFrame.addNewField('Expense Paid By', self.selectedGolfer)

        self.selectedAmount = AppFieldEntryFloat(self.expenseFieldFrame, None, None, AppAmountWidth, self._aDirtyMethod, self._anAmountEntered)
        self.selectedAmount.addValidationAmount(2,6, self.myMsgBar)
        self.expenseFieldFrame.addNewField('Expense Amount', self.selectedAmount)

        self.selectedCurrency = AppSearchLB(self.expenseFieldFrame, self, None, None, AppCurrencyList,
                                                   AppCurrencyWidth, None, self.myMsgBar, self._aDirtyMethod, self._anAmountEntered)
        self.expenseFieldFrame.addNewField('Expense Currency', self.selectedCurrency)

        self.selectedRate = AppFieldEntryFloat(self.expenseFieldFrame, None, None, AppAmountWidth, self._aDirtyMethod, self._anAmountEntered)
        self.selectedRate.addValidationAmount(5,7, self.myMsgBar)
        self.expenseFieldFrame.addNewField('Exchange Rate', self.selectedRate)

        self.selectedAdjustedAmnt = AppFieldEntryFloat(self.expenseFieldFrame, None, None, AppAmountWidth, None, None)
        self.selectedAdjustedAmnt.addValidationAmount(2,7, self.myMsgBar)
        self.expenseFieldFrame.addNewField('Expense Adjusted Amount', self.selectedAdjustedAmnt)

        #       self.identificationFrame.noStretchColumn(0)
        ########################################################### Footer Section
        self.fieldsFrame.addRow()
        self.lastUpdate = AppLastUpdateFormat(self.fieldsFrame)
        self.lastUpdate.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, columnspan=colTotal,
                             sticky=E + N + S)

        self.setRequiredFields()
        self.fieldsDisable()

    # cursor.execute('''CREATE TABLE MBExpenses(
    #   uniqueID            INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
    #   expenseDate          INTEGER    NOT NULL,
    #   expenseItem          CHAR(100)  NOT NULL,
    #   expenseAmount        REAL       NOT NULL,
    #   currency             CHAR(5)    NOT NULL,
    #   USExchangeRate       REAL,
    #   expenseAdjusted      REAL,
    #   golferID             INTEGER    NOT NULL,
    #   deleted              CHAR(1)    NOT NULL,
    #   lastModified         INTEGER,
    #   updaterID            INTEGER,
    #   FOREIGN KEY(golferID) REFERENCES Golfers(uniqueID) ON DELETE CASCADE
    #   );''')

    def _anAmountEntered(self, *args):
        self._updateRevisedAmount()

    def _updateRevisedAmount(self):
        if self.selectedAmount.get() == None or len(self.selectedCurrency.get()) == 0 or self.selectedRate.get() == None:
            pass
        else:
            currency = self.selectedCurrency.get()
            if currency== 'CDN':
                self.selectedAdjustedAmnt.load(self.selectedAmount.get())
            elif currency== 'US':
                self.selectedAdjustedAmnt.load(self.selectedAmount.get() * self.selectedRate.get())

    def _aCountryEntered(self):
        pass

    def displayAddressesFrame(self):
        pass

    def displayCoursesFieldsFrame(self):
        self.identificationDATAAddressFrame.grid_remove()
        self.identificationDATAEditAddFrame.grid()

    def deleteDependencies(self, ID):
        pass
        # Need to delete the dependencies (ie: tees and teeDetails associated with course).

    def unDeleteDependencies(self, ID):
        pass
        # Need to unDelete the dependencies (ie: tees and teeDetails associated with course).


    # cursor.execute('''CREATE TABLE MBExpenses(
    # 0  uniqueID            INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
    # 1  expenseDate          INTEGER    NOT NULL,
    # 2  expenseItem          CHAR(100)  NOT NULL,
    # 3  expenseAmount        REAL       NOT NULL,
    # 4  currency             CHAR(5)    NOT NULL,
    # 5  USExchangeRate       REAL,
    # 6  expenseAdjusted      REAL,
    # 7  golferID             INTEGER    NOT NULL,
    #   deleted              CHAR(1)    NOT NULL,
    #   lastModified         INTEGER,
    #   updaterID            INTEGER,
    #   FOREIGN KEY(golferID) REFERENCES Golfers(uniqueID) ON DELETE CASCADE
    #   );''')

    def displayRec(self, *args):
        if len(self.recList) > 0:
            self.fieldsClear()

            self.selectedExpenseDescription.load(self.recList[self.curRecNumV.get()][2])
            self.selectedGolfer.load(self.golferCBData.getName(self.recList[self.curRecNumV.get()][7]))
            self.selectedExpenseDate.load(convertOrdinaltoString(self.recList[self.curRecNumV.get()][1]))
            self.selectedAmount.load(self.recList[self.curRecNumV.get()][3])
            self.selectedCurrency.load(self.recList[self.curRecNumV.get()][4])
            self.selectedRate.load(self.recList[self.curRecNumV.get()][5])
            self.selectedAdjustedAmnt.load(self.recList[self.curRecNumV.get()][6])

            self.lastUpdate.load(self.recList[self.curRecNumV.get()][(self.numberOfFields - 2)],
                                 self.recList[self.curRecNumV.get()][(self.numberOfFields - 1)])
            self.deletedV.set(self.recList[self.curRecNumV.get()][self.numberOfFields - 3])
            self.dirty = False
        else:
            self._displayRecordFrame()

    def _dateEnteredAction(self, *args):
        self._aDirtyMethod(None)

    def _verifyFutureDate(self, aDate):
            aDateOrdinal = convertDateStringToOrdinal(aDate)
            todayOrdinal = convertDateStringToOrdinal(strdate)
            if aDateOrdinal > todayOrdinal:
                return False
            else:
                return True

    def _startAddEditGolfer(self, mode, golferData):
        pass

    def OnChildClose(self):
        self.AddEditGolferWindow.destroy()

    def fieldsDisable(self):
        self.selectedExpenseDescription.disable()
        self.selectedGolfer.disable()
        self.selectedExpenseDate.disable()
        self.selectedAmount.disable()
        self.selectedCurrency.disable()
        self.selectedRate.disable()
        self.selectedAdjustedAmnt.disable()

    def fieldsEnable(self):
        self.selectedExpenseDescription.enable()
        self.selectedGolfer.enable()
        self.selectedExpenseDate.enable()
        self.selectedAmount.enable()
        self.selectedCurrency.enable()
        self.selectedRate.enable()

    def fieldsClear(self):
        self.selectedExpenseDescription.clear()
        self.selectedGolfer.clear()
        self.selectedExpenseDate.clear()
        self.selectedAmount.clear()
        self.selectedCurrency.clear()
        self.selectedRate.clear()
        self.selectedAdjustedAmnt.clear()

    def setRequiredFields(self):
        # Table specific.  Will be defined in calling class
        self.selectedExpenseDescription.setAsRequiredField()
        self.selectedGolfer.setAsRequiredField()
        self.selectedExpenseDate.setAsRequiredField()
        self.selectedAmount.setAsRequiredField()
        self.selectedCurrency.setAsRequiredField()
        self.selectedRate.setAsRequiredField()

    def resetRequiredFields(self):
        # Table specific.  Will be defined in calling class
        self.selectedExpenseDescription.resetAsRequiredField()
        self.selectedGolfer.resetAsRequiredField()
        self.selectedExpenseDate.resetAsRequiredField()
        self.selectedAmount.resetAsRequiredField()
        self.selectedCurrency.resetAsRequiredField()
        self.selectedRate.resetAsRequiredField()

    def validateRequiredFields(self):
        requiredFieldsEntered = True

        if len(self.selectedExpenseDescription.get()) == 0:
            requiredFieldsEntered = False
            self.selectedExpenseDescription.focus()

        elif len(self.selectedGolfer.get()) == 0:
            requiredFieldsEntered = False
            self.selectedGolfer.focus()

        elif len(self.selectedExpenseDate.get()) == 0:
            requiredFieldsEntered = False
            self.selectedExpenseDate.focus()

        elif self.selectedAmount.get() == None:
            requiredFieldsEntered = False
            self.selectedAmount.focus()

        elif len(self.selectedCurrency.get()) == 0:
            requiredFieldsEntered = False
            self.selectedCurrency.focus()

        elif self.selectedRate.get() == None:
            requiredFieldsEntered = False
            self.selectedRate.focus()

        elif requiredFieldsEntered == False:
            self.myMsgBar.requiredFieldMessage()

        if requiredFieldsEntered == False:
            self.myMsgBar.requiredFieldMessage()
        return requiredFieldsEntered

    def find(self):
        self.fieldsClear()
        self.resetRequiredFields()
        if len(self.findValues) > 0:
            self.selectedExpenseDescription.load(self.findValues[0])
            self.selectedGolfer.load(self.findValues[1])
            self.selectedExpenseDate.load(self.findValues[2])
            self.selectedAmount.load(self.findValues[3])
            self.selectedCurrency.load(self.findValues[4])
            self.selectedRate.load(self.findValues[5])
            self.selectedAdjustedAmnt.load(self.findValues[6])
        self.fieldsEnable()
        self.selectedAdjustedAmnt.enable()
        self.selectedExpenseDate.focus()
        self.dirty = False

    def edit(self):
        self.fieldsEnable()
        self.dirty = False

    def add(self):
        if len(self.recList) == 0:
            self._displayRecordFrame()
        self.fieldsClear()
        #
        #  Time saving input from the configuration
        #  trip data.
        #
        self.selectedExpenseDate.load(todayDate)
        self.selectedGolfer.load(self.bigSpender)
        if self.configDataRead[0][7] == None:
            self.selectedRate.load(1.0)
        else:
            self.selectedRate.load(self.configDataRead[0][7]) # Default currency rate of exchange
            self.selectedCurrency.load(self.configDataRead[0][8]) # Default Currency


        self.fieldsEnable()
        self.dirty = False

    def clearPopups(self):
        pass
#        self.selectedCountry.clearPopups()

    def validateRequiredFields(self):
        #
        #  required Fields:
        #
        #
        requiredFieldsEntered = True

#        if len(self.selectedLongCourseName.get()) == 0:
#            if len(self.identificationDATADisplayFrame.grid_info()) == 0:
#                self.displayCoursesFieldsFrame()
#            requiredFieldsEntered = False
#            self.selectedLongCourseName.focus()

#        elif len(self.selectedShortCourseName.get()) == 0:
#            if len(self.identificationDATADisplayFrame.grid_info()) == 0:
#                self.displayCoursesFieldsFrame()
#            requiredFieldsEntered = False
#            self.selectedShortCourseName.focus()


        if requiredFieldsEntered == False:
            self.myMsgBar.requiredFieldMessage()
        return requiredFieldsEntered

    def save(self):
        #
        #  Table specific.  Return a True if succesful saved.
        #
        #
        #  House Cleaning in case a popup still exist
        #
        requiredFieldsEntered = self.validateRequiredFields()

        if self.curOp == 'add' and requiredFieldsEntered == True:
            pass
            try:
                CRUDExpenses.insert_MBExpenses(
                                               convertDateStringToOrdinal(self.selectedExpenseDate.get()),
                                               self.selectedExpenseDescription.get(),
                                               self.selectedAmount.get(),
                                               self.selectedCurrency.get(),
                                               self.selectedRate.get(),
                                               self.selectedAdjustedAmnt.get(),
                                               self.golferCBData.getID(self.selectedGolfer.get()),
                                               self.updaterID
                                              )

                ID = CRUDExpenses.get_MBExpensesLastUniqueID()

                anItem = (
                          ID,
                          convertDateStringToOrdinal(self.selectedExpenseDate.get()),
                          self.selectedExpenseDescription.get(),
                          self.selectedAmount.get(),
                          self.selectedCurrency.get(),
                          self.selectedRate.get(),
                          self.selectedAdjustedAmnt.get(),
                          self.golferCBData.getID(self.selectedGolfer.get()),
                          'N',
                          convertDateStringToOrdinal(todayDate), self.updaterID
                         )
                self._updateRecList(ID, anItem)
                aMsg = "A {3} expense of $ {0} on {1} by {2} has been added to the Trip Expenses.".format( self.selectedAmount.get(),
                                                                                                         self.selectedExpenseDate.get(),
                                                                                                         self.selectedGolfer.get(),
                                                                                                         self.selectedCurrency.get()
                                                                                                        )
                self.myMsgBar.newMessage('info', aMsg)
                return True

            except:
                aMsg = "ERROR: An error occurred while trying to create a new Trip Expense."
                self.myMsgBar.newMessage('error', aMsg)
                return False

        elif self.curOp == 'edit' and requiredFieldsEntered == True:
            pass
            ID = self.recList[self.curRecNumV.get()][0]
            anItem = (
                ID,
                convertDateStringToOrdinal(self.selectedExpenseDate.get()),
                self.selectedExpenseDescription.get(),
                self.selectedAmount.get(),
                self.selectedCurrency.get(),
                self.selectedRate.get(),
                self.selectedAdjustedAmnt.get(),
                self.golferCBData.getID(self.selectedGolfer.get()),
                'N',
                convertDateStringToOrdinal(todayDate), self.updaterID
            )


            try:
                CRUDExpenses.update_MBExpenses( ID, convertDateStringToOrdinal(self.selectedExpenseDate.get()),
                                                self.selectedExpenseDescription.get(), self.selectedAmount.get(),
                                                self.selectedCurrency.get(), self.selectedRate.get(),
                                                self.selectedAdjustedAmnt.get(),
                                                self.golferCBData.getID(self.selectedGolfer.get()),
                                                self.updaterID
                                              )

                idxRecListAll = getIndex(self.recList[self.curRecNumV.get()][0], self.recAll)
                self._updateRecListAll(idxRecListAll, ID, anItem)
                aMsg = "A {3} expense of $ {0} on {1} by {2} has been updated.".format( self.selectedAmount.get(),
                                                                                        self.selectedExpenseDate.get(),
                                                                                        self.selectedGolfer.get(),
                                                                                        self.selectedCurrency.get()
                                                                                       )
                self.myMsgBar.newMessage('info', aMsg)
                return True
            except:
                aMsg = "ERROR: An error occurred while editing an Expense."
                self.myMsgBar.newMessage('error', aMsg)
                return False
                #                if addrID != None:


        elif self.curOp == 'find':
            # Important if validation for amount is on.  May create non wanted error
            # Cause by displaying a $ in front of amount and it is a non valid character
            # in an amount field.
            self.fieldsDisable()
            print("A date: ", self.selectedExpenseDate.get())
            self.recListTemp = CRUDExpenses.find_MBExpenses(
                                                        self.selectedExpenseDescription.get(),
                                                        self.golferCBData.getID(self.selectedGolfer.get()),
                                                        self.selectedExpenseDate.get(),
                                                        self.selectedAmount.get(),
                                                        self.selectedCurrency.get(),
                                                        self.selectedRate.get(),
                                                        self.selectedAdjustedAmnt.get()
                                                      )
            self.findValues = []
            self.findValues.append(self.selectedExpenseDescription.get())
            self.findValues.append(self.selectedGolfer.get())
            self.findValues.append(self.selectedExpenseDate.get())
            self.findValues.append(self.selectedAmount.get())
            self.findValues.append(self.selectedCurrency.get())
            self.findValues.append(self.selectedRate.get())
            self.findValues.append(self.selectedAdjustedAmnt.get())

            if len(self.recListTemp) == 0:
                aMsg = "WARNING: No records were found using the given search criterias."
                if self.prevFind == True:
                    self.displayCurFunction.setFindResultMode()
                    self.curFind = True
                else:
                    self.displayCurFunction.setDefaultMode()
                    self.curFind = False
                self.myMsgBar.newMessage('warning', aMsg)
            elif len(self.recList) == 1:
                self.curFind = True
                self.recList = self.recListTemp
                self.displayCurFunction.setFindResultMode()
                aMsg = "One record was found using the given search criterias."
                self.curRecNumV.set(0)
                self.myMsgBar.newMessage('info', aMsg)
            else:
                self.curFind = True
                self.recList = self.recListTemp
                self.displayCurFunction.setFindResultMode()
                aMsg = "{0} records were found using the given search criterias.".format(len(self.recList))
                self.curRecNumV.set(0)
                self.myMsgBar.newMessage('info', aMsg)
            self.displayRec()
            self.clearPopups()
            self.setRequiredFields()
            return True
        else:
            self.clearPopups()
            return False


