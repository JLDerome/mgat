import sqlite3 as sql
import os
import csv
import AppCRUD as AppCRUD
from AppProc import getDBLocationFullName, convertDateStringToOrdinal
from AppConstants import *

db=None
cursor = None
lidGolfers = 0

#CREATE TABLE Tees(
#        uniqueID INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
#        teeNAME         CHAR(15)    NOT NULL,
#        teeType         CHAR(8)    NOT NULL,
#        teeRATING       REAL        NOT NULL,
#        teeSLOPE        SMALLINT    NOT NULL,
#        courseID        INTEGER     NOT NULL,
#        archived        CHAR(1),
#        deleted         CHAR(1),
#        lastModified INTEGER,
#        updaterID INTEGER
#        FOREIGN KEY(courseID) REFERENCES Courses(courseID) ON DELETE CASCADE
#       );

def getTeesDictionaryInfo(courseID, NoClose=False):
    #
    #
    #
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())
    query = '''
               select uniqueID, teeName, teeType, teeRATING from Tees
               where courseID = {0} and archived != 'Y' and deleted != 'Y'
               ORDER by teeRATING DESC, teeType ASC
            '''.format(courseID)

    temp = AppCRUD.cursor.execute(query).fetchall()
    returnList = []

    for i in range(len(temp)):
        newItem = (temp[i][0], "{0} {1}".format(temp[i][2], temp[i][1]))
        returnList.append(newItem)

    if NoClose == False:
        AppCRUD.closeDB()

    return returnList

def findIfCreatingADuplicateTeeForCourse(NoClose, courseID, teeName, teeType, teeID):
    #
    # NoClose: When True, the DB remain opens after execution of the method.
    #                     It also do not open the DB as it implied that it is
    #                     already open.
    #
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                select * from Tees where courseID = {0} and teeName = "{1}" and teeType = "{2}" and
                                         archived = 'N' and uniqueID != {3}
            '''.format(courseID, teeName, teeType, teeID)

    ans = AppCRUD.cursor.execute(query).fetchall()

    if NoClose == False:
        AppCRUD.closeDB()

    if len(ans) > 0:
        return True
    else:
        return False

def findDuplicateTeeForCourse(NoClose, courseID, teeName, teeType):
    #
    # NoClose: When True, the DB remain opens after execution of the method.
    #                     It also do not open the DB as it implied that it is
    #                     already open.
    #
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                select * from Tees where courseID = {0} and teeName = "{1}" and teeType = "{2}" and
                                         archived = 'N'
            '''.format(courseID, teeName, teeType)

    ans = AppCRUD.cursor.execute(query).fetchall()

    if NoClose == False:
        AppCRUD.closeDB()

    if len(ans) > 0:
        return True
    else:
        return False

def get_tees(NoClose=False):
    #
    # NoClose: When True, the DB remain opens after execution of the method.
    #                     It also do not open the DB as it implied that it is
    #                     already open.
    #
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''select * from Tees where archived != 'Y' ORDER BY courseID '''
    ans =  AppCRUD.cursor.execute(query).fetchall()

    if NoClose == False:
        AppCRUD.closeDB()

    return ans

def get_tee_info_for_teeID(uniqueID):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''select * from Tees where uniqueID = {0} and archived = 'N' '''.format(uniqueID)
    temp =  AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()
    return temp

def get_CourseID_From_TeeID(uniqueID, NoClose):
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                select courseID from Tees where uniqueID = {0}
            '''.format(uniqueID)

    ans = AppCRUD.cursor.execute(query).fetchall()

    if NoClose == False:
        AppCRUD.closeDB()

    if len(ans) > 0:
        return ans[0][0]
    else:
        return None

def get_teeName_CourseID(uniqueID):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''select teeName, teeType, courseID from Tees where uniqueID = {0}'''.format(uniqueID)
    return AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()

def get_teeName_CourseIDMASS(uniqueID):

    query = '''select teeName, teeType, courseID from Tees where uniqueID = {0}'''.format(uniqueID)
    return AppCRUD.cursor.execute(query).fetchall()

def get_teeList_for_Course_WindowMASS(courseID):

    query = '''select * from Tees where courseID = {0} order by teeType, teeSLOPE DESC'''.format(courseID)
    temp =  AppCRUD.cursor.execute(query).fetchall()

    return temp

def get_teeList_for_Course_Window(courseID):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''select * from Tees where courseID = {0} order by teeType, teeSLOPE DESC'''.format(courseID)
    temp =  AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()
    return temp

def get_teeIDs_for_Course(courseID):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''select uniqueID from Tees where courseID = {0}'''.format(courseID)
    temp = AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()
    return temp

def get_tee_rating(uniqueID, NoClose=False):
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                select teeRATING from Tees where uniqueID = {0}
            '''.format(uniqueID)

    temp = AppCRUD.cursor.execute(query).fetchall()

    if NoClose == False:
        AppCRUD.closeDB()

    if len(temp) > 0:
        return temp[0][0]
    else:
        return None

def get_tee_slope(uniqueID, NoClose=False):
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                select teeSLOPE from Tees where uniqueID = {0}
            '''.format(uniqueID)

    temp =  AppCRUD.cursor.execute(query).fetchall()

    if NoClose == False:
        AppCRUD.closeDB()

    if len(temp) > 0:
        return temp[0][0]
    else:
        return None

def get_tee_name_from_teeID(uniqueID, NoClose = False):
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())
    query = '''
                select teeNAME from Tees where uniqueID = {0}
            '''.format(uniqueID)
    temp =  AppCRUD.cursor.execute(query).fetchall()

    if NoClose == False:
        AppCRUD.closeDB()

    if len(temp) > 0:
        return temp[0][0]
    else:
        return None

def get_full_tee_name_from_teeID(uniqueID, NoClose=False):
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''select teeNAME, teeType from Tees where uniqueID = {0}'''.format(uniqueID)
    temp =  AppCRUD.cursor.execute(query).fetchall()

    if NoClose == False:
        return "{0} - {1}".format(temp[0][1], temp[0][0])
        AppCRUD.closeDB()
    else:
        return "{0} - {1}".format(temp[0][1],temp[0][0])

def get_teeListCB_ForACourse(courseID):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''select uniqueID, teeNAME, teeType, teeRATING, teeSLOPE from Tees where courseID = {0}'''.format(courseID)
    temp =  AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()
    return temp
   
def exportCSV_tees(fileName):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''select * from Tees'''
    data = AppCRUD.cursor.execute(query).fetchall()
    file = AppDefaultLocation + '/' + BackupDirDefaultName +'/'+ fileName
    with open(file, 'w', newline='', encoding='ISO-8859-1') as fout:
        for i in range(len(data)):
            csv.writer(fout).writerow(data[i])
    AppCRUD.closeDB()
    return len(data)


def importCSV_tees(fileName):
    AppCRUD.initDB(getDBLocationFullName())
    file = AppDefaultLocation + '/' + BackupDirDefaultName + '/' + fileName
    with open(file, 'r', encoding='ISO-8859-1') as fin:
        reader = csv.reader(fin)
        for row in reader:
            # row[5] must be a valid courseID
            query = '''
            insert into Tees (uniqueID, teeNAME,teeType,teeRATING,teeSLOPE, courseID, archived,
                              deleted, lastModified,updaterID)
            values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'''
            AppCRUD.cursor.execute(query, (row[0],row[1],row[2],row[3],row[4], row[5], row[6],
                                           row[7], row[8], row[9]))
    AppCRUD.closeDB()
    return len(get_tees(False))

def insert_tees(NoClose, teeNAME,teeType,teeRATING,teeSLOPE,courseID,updaterID):
    #
    # NoClose: When True, the DB remain opens after execution of the method.
    #                     It also do not open the DB as it implied that it is
    #                     already open.
    #
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    todayOrdinal = convertDateStringToOrdinal(todayDate)
    #
    #  Problems with single quote within the field
    #
    newTeeName = "{0}".format(teeNAME)
    NewType = "{0}".format(teeType)
    query = '''
                insert into Tees (teeNAME, teeType, teeRATING,teeSLOPE,courseID, archived,
                                 deleted, lastModified,updaterID)
                values (?,?,?,?,?,?,?,?,?)
            '''

    AppCRUD.cursor.execute(query,(newTeeName, NewType, teeRATING,teeSLOPE, courseID,
                                  'N','N', convertDateStringToOrdinal(todayDate), updaterID))

    ID = get_TeeIDFromRecentAdd(NoClose,teeNAME,NewType,teeRATING, teeSLOPE,
                                courseID, 'N',updaterID)

    if NoClose == False:
        AppCRUD.closeDB()
        return ID
    else:
        return ID

def get_TeeIDFromRecentAdd(NoClose, teeNAME,teeType,teeRATING,teeSLOPE,
                           courseID, archived,updaterID):
    #
    # NoClose: When True, the DB remain opens after execution of the method.
    #                     It also do not open the DB as it implied that it is
    #                     already open.
    #
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                select uniqueID from Tees where courseID = {0} and teeNAME = "{1}" and teeType ="{2}" and
                                                teeRATING = {3} and teeSLOPE = {4} and archived = "{5}" and
                                                updaterID = {6}
            '''.format(courseID, teeNAME,teeType,teeRATING,teeSLOPE,archived,updaterID)
    temp = AppCRUD.cursor.execute(query).fetchall()

    if len(temp) > 0:
        ID = temp[0][0]
    else:
        ID = None

    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())
        return ID
    else:
        return ID


def update_tee_archive(archived, idTee):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''
    update Tees set archived=? 
    where uniqueID = ?'''
    AppCRUD.cursor.execute(query, (archived, idTee))
    AppCRUD.closeDB()



#CREATE TABLE Tees(
#        uniqueID INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
#        teeNAME         CHAR(15)    NOT NULL,
#        teeType         CHAR(8)    NOT NULL,
#        teeRATING       REAL        NOT NULL,
#        teeSLOPE        SMALLINT    NOT NULL,
#        courseID        INTEGER     NOT NULL,
#        archived        CHAR(1),
#        deleted         CHAR(1),
#        lastModified INTEGER,
#        updaterID INTEGER
#        FOREIGN KEY(courseID) REFERENCES Courses(courseID) ON DELETE CASCADE
#       );

def update_tees(NoClose, idTee, TeeName, Type, Rating, Slope, CourseID, updaterID):
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                update Tees
                set teeNAME="{0}", teeType="{1}", teeRATING={2}, teeSLOPE={3}, courseID={4},
                    lastModified={5}, updaterID=?
                where uniqueID = ?
            '''.format(TeeName, Type, Rating, Slope, CourseID, convertDateStringToOrdinal(todayDate))

    AppCRUD.cursor.execute(query, (updaterID, idTee))
    if NoClose == False:
        AppCRUD.closeDB()

def delete_tee(idTee):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''
    delete from Tees
    where uniqueID = ?'''
    AppCRUD.cursor.execute(query,(idTee,))
    AppCRUD.closeDB()

def importTeesCSV_MyGolf2015(fileName):
    AppCRUD.initDB(getDBLocationFullName())
    with open(fileName, newline='', encoding='ISO-8859-1') as fin:
        reader = csv.reader(fin)
        for row in reader:
            query = '''
               insert into Tees (uniqueID,teeNAME, teeType, teeRating, teeSLOPE, courseID,
                                 archived,deleted, lastModified,updaterID)
                           values (?, ?, ?, ?, ?, ?, ?, ? , ?, ?)'''
            AppCRUD.cursor.execute(query, (row[0], row[1], row[2], row[3], row[4],
                                           row[5], row[6], 'N', convertDateStringToOrdinal(todayDate), '1'))
    AppCRUD.closeDB()
    return len(get_tees())


#CREATE TABLE Tees(
#        uniqueID INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
#        teeNAME         CHAR(15)    NOT NULL,
#        teeType         CHAR(8)    NOT NULL,
#        teeRATING       REAL        NOT NULL,
#        teeSLOPE        SMALLINT    NOT NULL,
#        courseID        INTEGER     NOT NULL,
#        archived        CHAR(1),
#        deleted         CHAR(1),
#        lastModified INTEGER,
#        updaterID INTEGER
#        FOREIGN KEY(courseID) REFERENCES Courses(courseID) ON DELETE CASCADE
#       );

#cursor.execute('''CREATE TABLE TeeDetails(
#   uniqueID  INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
#   teeYardage   SMALLINT    NOT NULL,
#   teeHandicap  SMALLINT    NOT NULL,
#   teePar       SMALLINT    NOT NULL,
#   teeID        INTEGER    NOT NULL,
#   archived        CHAR(1),
#   deleted              CHAR(1),
#   lastModified         INTEGER,
#   updaterID            INTEGER,
#   FOREIGN KEY(teeID) REFERENCES Tees(uniqueID) ON DELETE CASCADE
#   );''')
def get_teeDetails():
    AppCRUD.initDB(getDBLocationFullName())
    query = '''select * from TeeDetails'''
    temp =  AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()
    return temp

def get_teeDetailsIDs_for_Tee(NoClose, teeID):
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''select * from TeeDetails where teeID = {0} order by uniqueID'''.format(teeID)
    temp = AppCRUD.cursor.execute(query).fetchall()

    if NoClose == False:
        AppCRUD.closeDB()
    return temp

def get_teeDetailsIDs_for_Tee_OPEN(teeID):
    # Action is performed on an open DB, usually for a Mass operation
    query = '''select uniqueID from TeeDetails where teeID = {0}'''.format(teeID)
    temp = AppCRUD.cursor.execute(query).fetchall()
    return temp

def get_teeDetails_for_curTeeMASS(curTeeID):

    query = '''select * from TeeDetails where teeID == {0}'''.format(curTeeID)
    temp = AppCRUD.cursor.execute(query).fetchall()

    return temp

def get_teeDetails_for_curTee(curTeeID):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''select * from TeeDetails where teeID == {0}'''.format(curTeeID)
    temp = AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()
    return temp

def get_teePar_for_curTee(curTeeID):
    AppCRUD.initDB(getDBLocationFullName())

def insert_teeDetails(NoClose, teeYardage, teeHandicap ,teePar, teeID, updaterID):
    #
    # NoClose: When True, the DB remain opens after execution of the method.
    #                     It also do not open the DB as it implied that it is
    #                     already open.
    #
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                insert into TeeDetails (teeYardage, teeHandicap ,teePar, teeID,
                                        archived, deleted, lastModified, updaterID)
                values (?, ?, ?, ?, ?, ?, ?, ?)
            '''
    AppCRUD.cursor.execute(query, (teeYardage, teeHandicap ,teePar, teeID, 'N',
                                   'N', convertDateStringToOrdinal(todayDate), updaterID))

    if NoClose == False:
         AppCRUD.closeDB()
#cursor.execute('''CREATE TABLE TeeDetails(
#   uniqueID  INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
#   teeYardage   SMALLINT    NOT NULL,
#   teeHandicap  SMALLINT    NOT NULL,
#   teePar       SMALLINT    NOT NULL,
#   teeID        INTEGER    NOT NULL,
#   archived        CHAR(1),
#   deleted              CHAR(1),
#   lastModified         INTEGER,
#   updaterID            INTEGER,
#   FOREIGN KEY(teeID) REFERENCES Tees(uniqueID) ON DELETE CASCADE
#   );''')

def update_teeDetails(NoClose, uniqueID, teeYardage, teeHandicap ,teePar,
                      teeID, updaterID):
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                update TeeDetails
                set teeYardage={0}, teeHandicap={1}, teePar={2}, teeID={3}, lastModified = {4},
                    updaterID = {5}
                where uniqueID == {6}
            '''.format(teeYardage, teeHandicap ,teePar, teeID, convertDateStringToOrdinal(todayDate),
                       updaterID, uniqueID)

    AppCRUD.cursor.execute(query, )

    if NoClose == False:
        AppCRUD.closeDB()

def delete_teeDetails(idTee):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''
    delete from TeeDetails
    where teeID = ?'''    
    AppCRUD.cursor.execute(query,(idTee,))
    AppCRUD.closeDB()

def exportCSV_teeDetails(fileName):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''select * from TeeDetails'''
    data = AppCRUD.cursor.execute(query).fetchall()
    file = AppDefaultLocation + '/' + BackupDirDefaultName +'/'+ fileName
    with open(file, 'w', newline='', encoding='ISO-8859-1') as fout:
        for i in range(len(data)):
            csv.writer(fout).writerow(data[i])
    AppCRUD.closeDB()
    return len(data)
            
def importCSV_teeDetails(fileName):
    AppCRUD.initDB(getDBLocationFullName())
    file = AppDefaultLocation + '/' + BackupDirDefaultName + '/' + fileName
    with open(file, 'r', encoding='ISO-8859-1') as fin:
        reader = csv.reader(fin)
        for row in reader:
            # row[5] must be a valid courseID
            query = '''
            insert into TeeDetails (uniqueID, teeYardage, teeHandicap ,teePar, teeID,
                                    archived, deleted, lastModified,updaterID)
            values (?, ?, ?, ?, ?, ?, ?, ?, ?)'''
            AppCRUD.cursor.execute(query, (row[0],row[1],row[2],row[3],row[4],
                                           row[5], row[6], row[7], row[8]))
    AppCRUD.closeDB()
    return len(get_teeDetails())

def importTeeDetailsCSV_MyGolf2015(fileName):
    AppCRUD.initDB(getDBLocationFullName())
    with open(fileName, newline='', encoding='ISO-8859-1') as fin:
        reader = csv.reader(fin)
        for row in reader:
            query = '''
               insert into TeeDetails (uniqueID, teeYardage, teeHandicap, teePar, teeID,
                                 deleted, lastModified,updaterID)
                           values (?, ?, ?, ?, ?, ?, ?, ?)'''
            AppCRUD.cursor.execute(query, (row[0], row[1], row[2], row[3], row[4], 'N',
                                           convertDateStringToOrdinal(todayDate), '1'))
    AppCRUD.closeDB()
    return len(get_teeDetails())

#CREATE TABLE TeeDetails(
#              uniqueID INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
#              teeYardage  SMALLINT    NOT NULL,
#              teeHandicap SMALLINT    NOT NULL,
#              teePar      SMALLINT    NOT NULL,
#              teeID       INTEGER    NOT NULL,
#              archived     CHAR(1),
#              deleted      CHAR(1),
#              lastModified INTEGER,
#              updaterID INTEGER,
#              FOREIGN KEY(teeID) REFERENCES Tees(teeID) ON DELETE CASCADE
#   );''')

