from tkinter import *
import tkinter.ttk as ttk
from AppConstants import *
from AppClasses import AppFrame, AppMENUListFrame
from AppClasses import messageBar
from PIL import Image, ImageTk
from AppBackupRestoreProc import backupMyGolfDB, restoreMyGolfDB
from AppProc import resizeImage
from AppCRUD import createOpenAppDB
import AppCRUDGolfers as CRUDGolfers
import AppCRUDLoginAccess as AppCRUDLoginAccess
from AppGolfersClass import golfersWindow
from AppCoursesClass import coursesWindow
from AppExpensesClass import expensesWindow
from addEditTripConfigurationPopUp import addEditTripConfigurationPopUp
from addEditLoginAccessPopUp import addEditLoginAccessPopUp
from expensesTripReportPopUp import expenseTripReportPopUp


class App(Tk):
    def __init__(self, parent):
        Tk.__init__(self, parent)

        self.parent = parent
        Tk.protocol(self, 'WM_DELETE_WINDOW', self.evQuit)
        Tk.option_add(self, "*foreground",
                      AppDefaultForeground)  # Would have help before, global setting for the app width =200, height =220
        Tk.option_add(self, "*background",
                      AppDefaultBackground)  # Would have help before, global setting for the app width =200, height =220

        self.style = ttk.Style()
        self.option_add("*Dialog.msg.wrapLength", "12i")
        ################################ General Styles
        self.style.configure('.', font=fontBig, foreground=AppDefaultForeground, background=AppDefaultBackground)
        self.style.configure('WindowTitle.TLabel', font=fontMonstrousB)
        self.style.configure('RegularFieldTitle.TLabel', font=fontBigB)
        self.style.configure('SmallFieldTitle.TLabel', font=fontAverageB)
        self.style.configure('LastUpdateDisplay.TLabel', font=fontBigI)
        self.style.configure('RegularFields.TEntry', font=fontBig, highlightcolor=AppDefaultForeground,
                             highlightthickness=2)
        ################################ Login Styles
        self.style.configure('Login.TButton', font=fontBigB)
        self.style.configure('LoginFieldTitle.TLabel', font=fontBigB)
        ################################ Menu Style
        self.style.configure('MenuItems.TLabel', font=fontHuge2I)
        ################################ Command Styles (buttons, etc)
        self.style.configure('CommandLineButton.TButton', font=fontAverage, width=commandLineButtonWidth,
                             height=commandLineButtonHeight)
        ################################ Command Styles (buttons, etc)
        self.style.configure('CommandButton.TButton', font=fontBig, width=commandButtonWidth,
                             height=commandButtonHeight)
        self.style.configure('smallButton.TButton', font=fontAverage, width=commandButtonWidth,
                             height=commandButtonHeight)
        ################################ Command Styles (buttons, etc)
        self.style.configure('GameListingButton.TButton', font=AppDefaultFontAvg, width=gameListingButtonWidth,
                             height=gameListingButtonHeight, relief='flat')


        ################################ TooTip StylesCourses

        self.style.configure('toolTip.TLabel', justify='left', foreground=AppDefaultForeground, relief='solid',
                             borderwidth=1,
                             font=fontBig)

        self.loginData = []
        self.loginID = ''  # ID number of person logged In
        self.accessLevel = 'TESTING'  # Login person access level
        self.row = 0
        self.column = 0
        self.bindAllSpecialKeys()

    def bindAllSpecialKeys(self):
        self.bind('<Alt-E>', self.evQuit)
        self.bind('<Alt-e>', self.evQuit)

    def unBindAllSpecialKeys(self):
        self.unbind('<Alt-e>')
        self.unbind('<Alt-E>')

    def display(self):
        self.resizable(False, False)
        self.title(AppTitle)
        self.loginWindow.protocol('WM_DELETE_WINDOW', self.evQuit)

    def redrawMenu(self):
        self.accessLevel = self.loginData[1]  # Login person access level
        if self.accessLevel == 'Root':
            self.mainDisplayColumnTotal = 3
        elif self.accessLevel == 'Update' or self.accessLevel == 'Maintenance':
            self.mainDisplayColumnTotal = 2
        else:
            self.mainDisplayColumnTotal = 1
        self.mainDisplay = AppFrame(self, self.mainDisplayColumnTotal)
        self.mainDisplay.grid(row=self.row, column=self.column, sticky=E + W + S + N)
        wMsg = ttk.Label(self.mainDisplay, text=AppTitleFull, style='WindowTitle.TLabel')
        wMsg.grid(row=self.mainDisplay.row, column=self.mainDisplay.column, columnspan=self.mainDisplayColumnTotal)
        self.mainDisplay.addRow()
        #########################################################################################
        # Main Menu Frame
        #
        self.mainMenuFrame = AppMENUListFrame(self.mainDisplay, 1)
        self.mainMenuFrame.grid(row=self.mainDisplay.row, column=self.mainDisplay.column, sticky=N + E + W + S)

        wMsg = ttk.Label(self.mainMenuFrame, text='Main Menu', style='WindowTitle.TLabel')
        wMsg.grid(row=self.mainMenuFrame.row, column=self.mainMenuFrame.column, sticky=N)

        self.mainMenuFrame.addMenuItem('Golfers', self._golfersSelected)
        self.mainMenuFrame.addMenuItem('Courses', self._coursesSelected)
        self.mainMenuFrame.addMenuItem('Expenses', self._expensesSelected)
        self.mainMenuFrame.addMenuItem('MB Trip Configuration', self._tripConfigurationSelected)
        self.mainMenuFrame.addMenuItem('Expense Trip Report', self._tripExpenseReportSelected)
        self.mainMenuFrame.addMenuItem('Logout', self._logOutSelected)

        #########################################################################################
        # Maintenance Menu Frame
        #
        if self.accessLevel == 'Root' or self.accessLevel == 'Update' or self.accessLevel == 'Maintenance':
            self.mainDisplay.addColumn()
            self.maintenanceMenuFrame = AppMENUListFrame(self.mainDisplay, 1)
            self.maintenanceMenuFrame.grid(row=self.mainDisplay.row, column=self.mainDisplay.column,
                                           sticky=N + E + W + S)

            wMsg = ttk.Label(self.maintenanceMenuFrame, text='Maintenance Menu', style='WindowTitle.TLabel')
            wMsg.grid(row=self.maintenanceMenuFrame.row, column=self.maintenanceMenuFrame.column, sticky=N)

            self.maintenanceMenuFrame.addMenuItem('My Golf Data Backup', self._backupSelected)
            self.maintenanceMenuFrame.addMenuItem('My Golf Data Restore', self._restoreSelected)
            # self.maintenanceMenuFrame.addMenuItem('Data Restore Game Details Upgrade', self._convertGameDetailsUpgrade)

        #########################################################################################
        # Root Menu Frame
        #
        if self.accessLevel == 'Root':
            self.mainDisplay.addColumn()
            self.rootMenuFrame = AppMENUListFrame(self.mainDisplay, 1)
            self.rootMenuFrame.grid(row=self.mainDisplay.row, column=self.mainDisplay.column, sticky=N + E + W + S)  #

            wMsg = ttk.Label(self.rootMenuFrame, text='Root Menu', style='WindowTitle.TLabel')
            wMsg.grid(row=self.rootMenuFrame.row, column=self.rootMenuFrame.column, sticky=N)

            self.rootMenuFrame.addMenuItem('Add Login Access', self._loginAccessWindowSelected)

    def evQuit(self, *args):
        #        AppDB.closeDB()
        MyGolf.withdraw()
        login.show()

#####################################################Backup & Restore Command

    def _restoreSelected(self, event=None):
        restoreMyGolfDB()

    def _backupSelected(self, event=None):
        backupMyGolfDB()


    #####################################################Login Access Window Command
    def _loginAccessWindowSelected(self, event=None):
        self.startLoginAccessWindow()

    def startLoginAccessWindow(self):
        pass
        self.loginAccessWindow = Toplevel()
        self.loginAccessWindow.transient(self)

        myGeo = self.winfo_geometry()
        myNewGeo = '+{0}+{1}'.format((int(myGeo.split("+")[1]) + 40), (int(myGeo.split("+")[2]) + 40))
        self.loginAccessWindow.geometry(myNewGeo)
        # golferData should replace the 3
        addEditLoginAccessPopUp(self.loginAccessWindow, self.OnLoginAccessWindowClose,
                                          self.loginData).pack(fill=BOTH, expand=1)

    def OnLoginAccessWindowClose(self):
        self.loginAccessWindow.destroy()

    #####################################################Trip Configuration Window Command
    def _tripExpenseReportSelected(self, event=None):
        self.startTripExpenseReportPopUp()

    def startTripExpenseReportPopUp(self):
        pass
        self.TripExpenseReportPopUp = Toplevel()
        self.TripExpenseReportPopUp.transient(self)

        myGeo = self.winfo_geometry()
        myNewGeo = '+{0}+{1}'.format((int(myGeo.split("+")[1]) + 40), (int(myGeo.split("+")[2]) + 40))
        self.TripExpenseReportPopUp.geometry(myNewGeo)
        # golferData should replace the 3
        expenseTripReportPopUp(self.TripExpenseReportPopUp, self.OnTripExpenseReportPopUpClose,
                                          self.loginData).pack(fill=BOTH, expand=1)

    def OnTripExpenseReportPopUpClose(self):
        self.TripExpenseReportPopUp.destroy()

    #####################################################Trip Configuration Window Command
    def _tripConfigurationSelected(self, event=None):
        self.startTripConfigurationWindow()

    def startTripConfigurationWindow(self):
        pass
        self.tripConfigurationWindow = Toplevel()
        self.tripConfigurationWindow.transient(self)

        myGeo = self.winfo_geometry()
        myNewGeo = '+{0}+{1}'.format((int(myGeo.split("+")[1]) + 40), (int(myGeo.split("+")[2]) + 40))
        self.tripConfigurationWindow.geometry(myNewGeo)
        # golferData should replace the 3
        addEditTripConfigurationPopUp(self.tripConfigurationWindow, self.OnTripConfigurationWindowClose,
                                          self.loginData).pack(fill=BOTH, expand=1)

    def OnTripConfigurationWindowClose(self):
        self.tripConfigurationWindow.destroy()

    #####################################################Golfers Window Command
    def _golfersSelected(self, event=None):
        self.startGolfersWindow()

    def startGolfersWindow(self):
        self.golfersWindow = Toplevel()
        self.golfersWindow.transient(self)

        myGeo = MyGolf.geometry()
        myNewGeo = '+{0}+{1}'.format((int(myGeo.split("+")[1]) + 40), (int(myGeo.split("+")[2]) + 40))
        self.golfersWindow.geometry(myNewGeo)

        golfersWindow(self.golfersWindow, self.OnGolfersWindowClose, MyGolf.loginData).pack(fill=BOTH,expand = 1)

    def OnGolfersWindowClose(self):
        self.golfersWindow.destroy()

#####################################################Courses Window Command
    def _coursesSelected(self, event):
        self.startCoursesWindow()

    def startCoursesWindow(self):
        self.coursesWindow = Toplevel()
        self.coursesWindow.transient(self)

        myGeo = MyGolf.geometry()
        myNewGeo = '+{0}+{1}'.format((int(myGeo.split("+")[1]) + 40), (int(myGeo.split("+")[2]) + 40))
        self.coursesWindow.geometry(myNewGeo)

        coursesWindow(self.coursesWindow, self.OnCoursesWindowClose, MyGolf.loginData).pack(fill=BOTH,expand = 1)

    def OnCoursesWindowClose(self):
        self.coursesWindow.destroy()

#####################################################Courses Window Command
    def _expensesSelected(self, event):
        self.startExpensesWindow()

    def startExpensesWindow(self):
        self.expensesWindow = Toplevel()
        self.expensesWindow.transient(self)

        myGeo = MyGolf.geometry()
        myNewGeo = '+{0}+{1}'.format((int(myGeo.split("+")[1]) + 40), (int(myGeo.split("+")[2]) + 40))
        self.expensesWindow.geometry(myNewGeo)

        expensesWindow(self.expensesWindow, self.OnExpensesWindowClose, MyGolf.loginData).pack(fill=BOTH, expand=1)

    def OnExpensesWindowClose(self):
        self.expensesWindow.destroy()

#####################################################Logout Command
    def _logOutSelected(self, event):
        login.show()
        MyGolf.withdraw()

# ########################################## Convert D
#     def _setConvertDFocus(self, event):
#         self.convertDFrame.focus()
#
#     def _convertGameDetailsUpgrade(self, event=None):
#         restoreGameDetailsUpgrade()
#
#         '''
#         aMsg = "My Golf App 2015 Convert Database initiated.\nCAN BE PERFORMED ONLY ONCE.\nDo you still want to proceed with the conversion command?".format(AppDefaultLocation + BackupDirDefaultName)
#         ans = Mb.askyesno('Converting My Golf 2015 Database', aMsg)
#         if ans == True:
#             amtGolfers = CRUDGolfers.importGolfersCSV_MyGolf2015(import2015GolferDataFilename)
#             amtCourses = CRUDCourses.importCoursesCSV_MyGolf2015(import2015CoursesDataFilename)
#             amtTees = CRUDTees.importTeesCSV_MyGolf2015(import2015TeesDataFilename)
#             amtTeeDetails = CRUDTees.importTeeDetailsCSV_MyGolf2015(import2015TeeDetailsDataFilename)
#             amtMBExpenses = CRUDExpenses.importExpensesCSV_MyGolf2015(import2015ExpensesDataFilename)
#             amtGames = CRUDGames.importGamesCSV_MyGolf2015(import2015GamesDataFilename)
#             amtGameDetails = CRUDGames.importGameDetailsCSV_MyGolf2015(import2015GameDetailsDataFilename)
#             amtAddresses = CRUDAddresses.importAddressesCSV_MyGolf2015(import2015AddressesDataFilename)
#
#             aMsg = "My Golf App 2015 Convert Database was completed.\n  1. Source directory used {0}/{1}.\n" \
#                    "  2. {2} Golfers imported.\n  3. {3} Courses imported.\n  4. {4} Tees imported.\n" \
#                    "  5. {5} Tee Details imported.\n  6. {6} Expenses imported.\n  7. {7} Games imported.\n" \
#                    "  8. {8} Game Details imported.\n  9. {9} Addresses imported."\
#                    .format(AppDefaultLocation, BackupDirDefaultName,amtGolfers, amtCourses,
#                            amtTees, amtTeeDetails, amtMBExpenses, amtGames, amtGameDetails, amtAddresses)
#             Mb.showinfo('Converting My Golf 2015 Data', aMsg)
#         else:
#             aMsg = "My Golf App 2015 Convert Database command was cancelled."
#             Mb.showinfo('Converting My Golf 2015 Data', aMsg)
#
#         '''

class welcomeLoginDisplay:
    def __init__(self, parent):
        self.window = parent

    def display(self):
        self.loginWindow = Toplevel()
        self.loginWindow.resizable(False, False)
        self.loginWindow.title(AppTitle)
        self.loginWindow.protocol('WM_DELETE_WINDOW', self.evQuit)

        columnCount = 0
        rowCount = 0

        original = Image.open(AppWelcomeImg)
        image1 = resizeImage(original, welcomeLogoWidth, welcomeLogoHeight)
        panel1 = Label(self.loginWindow, image=image1)
        self.display = image1
        panel1.grid(row=rowCount, column=columnCount)

        columnCount = columnCount + 1
        mainDisplay = Frame(self.loginWindow, border=8)
        mainDisplay.grid(row=rowCount, column=columnCount, sticky=E + W + S + N)

        mainDisplayRow = 0
        mainDisplayColumn = 0
        wMsg = ttk.Label(mainDisplay, text=AppTitle, style='WindowTitle.TLabel', font=fontHugeB)
        wMsg.grid(row=mainDisplayRow, column=mainDisplayColumn, sticky=N)
        self.loginWindow.rowconfigure(mainDisplayRow, weight=0)

        mainDisplayRow = mainDisplayRow + 1
        loginFieldTitleFrame = Frame(mainDisplay)
        loginFieldTitleFrame.grid(row=mainDisplayRow, column=mainDisplayColumn, sticky=W + N)

        loginLine1Title = ttk.Label(loginFieldTitleFrame, text='Login ID:', style='LoginFieldTitle.TLabel',
                                    font=fontBigB)
        loginLine1Title.grid(row=0, column=0, sticky=N + E)

        self.selectLoginName = Entry(loginFieldTitleFrame, font=fontBigB, justify='left', width=loginDataFieldWidth)
        self.selectLoginName.grid(row=0, column=1, sticky=N + W)

        loginLine2Title = ttk.Label(loginFieldTitleFrame, text='Password:', style='LoginFieldTitle.TLabel',
                                    font=fontBigB)
        loginLine2Title.grid(row=1, column=0, sticky=N + E)
        self.selectPassword = Entry(loginFieldTitleFrame, font=fontBigB, justify='left', width=loginDataFieldWidth,
                                    show="*")
        self.selectPassword.grid(row=1, column=1, sticky=N + W)

        self.loginWindow.rowconfigure(mainDisplayRow, weight=0)

        #       updateECEShopConstant()
        mainDisplayRow = mainDisplayRow + 1
        self.startButton = ttk.Button(mainDisplay, text='Login', style='Login.TButton',
                                 command=self.loginButtonPressed)
        self.startButton.grid(row=mainDisplayRow, sticky=N)

        ############################################################ Message Bar Area
        mainDisplayRow = mainDisplayRow + 1
        self.myMsgBar = messageBar(mainDisplay)
        self.myMsgBar.grid(row=mainDisplayRow, column=0, sticky=W + S, padx=5)

        self.loginWindow.bind('<Return>', self.loginButtonPressed)
        self.loginWindow.bind('<End>', self.focusLoginButton)
        self.loginWindow.bind('<Home>', self.focusLoginName)
        self.loginWindow.bind('<Alt-E>', self.evQuit)
        self.loginWindow.bind('<Alt-e>', self.evQuit)

    def focusLoginButton(self, *args):
        self.startButton.focus()

    def focusLoginName(self, *args):
        self.selectLoginName.focus()

    def evQuit(self, *args):
        #        AppDB.closeDB()  # Close DB to enable saving of database
        MyGolf.quit()

    def hide(self):

        self.loginWindow.withdraw()

    def show(self):

        self.loginWindow.deiconify()
        self.selectPassword.delete(0,END)

    def loginButtonPressed(self, *args):
        #
        #  Will need to verify login into the DB.  Currently
        #  no such scheme is implemented using the sqlite3 engine.
        #

        #
        #  In the absence of login mechanism, use the following lines
        #
        #        self.window.deiconify()
        #        self.hide()
        #        self.window.loginData = ['Jean-Luc Derome', 'root', 0]
        #        self.window.redrawMenu(mainMenu)

        # Below is a login mechanism if required.  DB must be configured users with
        # loginID and Passwords can be entered.
        #
        if len(self.selectLoginName.get()) > 0:
            createOpenAppDB()
            loginInfo = AppCRUDLoginAccess.getLoginUser(self.selectLoginName.get())
            if len(loginInfo) != 1:
                aMsg = "ERROR: Invalid login ID/password combination."
                self.myMsgBar.newMessage('error', aMsg)
            else:
                if self.selectPassword.get() == loginInfo[0][0]:
                    MyGolf.deiconify()
                    self.hide()
                    #
                    #  loginInfo format: password, access_Level, loginID
                    #
                    if (self.selectLoginName.get() == 'root'):
                        #  Special user (not a golfer)
                        MyGolf.loginData = ['Super User', (loginInfo[0][1]), (loginInfo[0][2])]
                    else:
                        golferID = AppCRUDLoginAccess.getLoginUserGolferID(self.selectLoginName.get())
                        MyGolf.loginData = [CRUDGolfers.getGolferFullname(golferID), (loginInfo[0][1]), (loginInfo[0][2])]
                    MyGolf.redrawMenu()
                else:
                    aMsg = "ERROR: Invalid login ID/password combination."
                    self.myMsgBar.newMessage('error', aMsg)
        else:
            aMsg = "ERROR: LoginID Required."
            self.myMsgBar.newMessage('error', aMsg)


if __name__ == '__main__':
    MyGolf = App(None)
    MyGolf.withdraw()
    MyGolf.resizable(False, False)
    MyGolf.wm_title(AppTitle)

    img = ImageTk.PhotoImage(file=AppMainIcon)
    MyGolf.wm_iconphoto(TRUE, img)

    login = welcomeLoginDisplay(MyGolf)
    login.display()

    MyGolf.mainloop()