import os, csv
import datetime
#from AppCRUDGames import get_games_hdcp_diff_for_golfer
from AppConstants import AppDefaultLocation, DBDefaultName, BackupDirDefaultName, ExportDirDefaultName
from AppConstants import BackupDirDefaultName, dictmonths
from AppConstants import year, month, day
from PIL import Image, ImageTk
from dateutil import parser

def convertOrdinaltoString(ordinalDate):
    aDate=datetime.date.fromordinal(int(ordinalDate))
    aNewDate= (str(aDate.day) +  " " +  dictmonths[str(aDate.month)] + " " +  str(aDate.year))
    return(aNewDate)

def getAYearFromOrdinal(ordinalDate):
    aDate=datetime.date.fromordinal(int(ordinalDate))
    return(str(aDate.year))

def getAge(refDate, Birthday): #require ordinal dates
    deltaDate = refDate - Birthday
    return int(deltaDate/365)

def convertDateStringToOrdinal(dateStr):
    dt = parser.parse(dateStr)   # transform a date into a datetime (accepts several formats)
    return dt.toordinal()

def resizeImage(image,w,h):
    size = (w, h)
    resized = image.resize(size,Image.ANTIALIAS)
    img = ImageTk.PhotoImage(resized)
    return(img)

def calculateHdcpDiff(scoreGross, courseRating, courseSlope):
    # scoreGross and courseSlope are integer
    # courseRating is a real (float)
    if courseSlope == 0:
        return 0.0
    else:
        diff =  (scoreGross-courseRating)
        calc = diff * 113 / courseSlope
        return "%.1f" % calc

def getDBLocationFullName():
    fullAppDBName = AppDefaultLocation + "/" + DBDefaultName
    return fullAppDBName

def getBackupdirLocationFullName():
    fullAppBackupDirName = AppDefaultLocation + "/" + BackupDirDefaultName
    return fullAppBackupDirName            
    
def getExportdirLocationFullName():
    fullAppExportDirName = AppDefaultLocation + "/" + ExportDirDefaultName
    return fullAppExportDirName            
    
def getExportFilenameFullName(filename):
    fullPath = getExportdirLocationFullName() + "/" + filename
    return fullPath            
    
def widgetEnter(event):
    event.widget.focus()
    
def focusNext(self, awidget):
    '''Return the next widget in tab order'''
    awidget = self.tk.call('tk_focusNext', awidget._w)
    if not awidget: return None
#        return self.nametowidget(widget.string)
    nextWidget=self.nametowidget(awidget.string)
    nextWidget.focus()
    return "break"

def getIndex(partID, aList):
    for i in range(len(aList)):
        if int(aList[i][0]) == int(partID):
            return i
    return -1

def stripChar(aList, aString):
    newString = aString
    for item in aList:
        newString = newString.replace(item,'')
    return newString

def getKey(item):
    return(item[1])

def convertYardsToKms(aLenght):
    kms = float(aLenght) * 0.0009144
    return "%.2f" % kms

def getDBLocationFullName():
    fullAppDBName = AppDefaultLocation + "/" + DBDefaultName
    return fullAppDBName

def updateMyAppConstant(filename):
    if os.path.exists(filename):
        configDataRead = importAppConfig(filename)
        return configDataRead
    else:
        return None

def importAppConfig(filename):
    data = []
    with open(filename, 'r') as fin:
        reader = csv.reader(fin)
        for row in reader:
            data.append(row)
    return data

def exportAppConfig(filename, aDataRow):
    try:
        with open(filename, 'w', newline='', encoding='ISO-8859-1') as fout:
            csv.writer(fout).writerow(aDataRow)
        return True
    except:
        return False

def resize_image(image,h,w):
    size = (w, h)
    resized = image.resize(size,Image.ANTIALIAS)
    img = ImageTk.PhotoImage(resized)
    return(img)

def calculateHdcpList(hdcpGameList):
    hdcpGameList.sort()
    if len(hdcpGameList) >= 20:
        newHdcp = float(0.0)
        for i in range(10):
            newHdcp = newHdcp + float(hdcpGameList[i])
        newHdcp = newHdcp / float(10)
    # return"%.1f" % newHdcp
    elif len(hdcpGameList) == 19:
        newHdcp = float(0.0)
        for i in range(9):
            newHdcp = newHdcp + float(hdcpGameList[i])
        newHdcp = newHdcp / float(9)
    # return"%.1f" % newHdcp
    elif len(hdcpGameList) == 18:
        newHdcp = float(0.0)
        for i in range(8):
            newHdcp = newHdcp + float(hdcpGameList[i])
        newHdcp = newHdcp / float(8)
    # return"%.1f" % newHdcp
    elif len(hdcpGameList) == 17:
        newHdcp = float(0.0)
        for i in range(7):
            newHdcp = newHdcp + float(hdcpGameList[i])
        newHdcp = newHdcp / float(7)
    # return"%.1f" % newHdcp
    elif (len(hdcpGameList) == 15) or (len(hdcpGameList) == 16):
        newHdcp = float(0.0)
        for i in range(6):
            newHdcp = newHdcp + float(hdcpGameList[i])
        newHdcp = newHdcp / float(6)
    # return"%.1f" % newHdcp
    elif (len(hdcpGameList) == 13) or (len(hdcpGameList) == 14):
        newHdcp = float(0.0)
        for i in range(5):
            newHdcp = newHdcp + float(hdcpGameList[i])
        newHdcp = newHdcp / float(5)
    # return"%.1f" % newHdcp
    elif (len(hdcpGameList) == 11) or (len(hdcpGameList) == 12):
        newHdcp = float(0.0)
        for i in range(4):
            newHdcp = newHdcp + float(hdcpGameList[i])
        newHdcp = newHdcp / float(4)
    # return"%.1f" % newHdcp
    elif (len(hdcpGameList) == 9) or (len(hdcpGameList) == 10):
        newHdcp = float(0.0)
        for i in range(3):
            newHdcp = newHdcp + float(hdcpGameList[i])
        newHdcp = newHdcp / float(3)
    # return"%.1f" % newHdcp
    elif (len(hdcpGameList) == 7) or (len(hdcpGameList) == 8):
        newHdcp = float(0.0)
        for i in range(2):
            newHdcp = newHdcp + float(hdcpGameList[i])
        newHdcp = newHdcp / float(2)
    else:
        newHdcp = float(hdcpGameList[0])

    newHdcp = newHdcp * float(0.98)
    return "%.1f" % newHdcp

