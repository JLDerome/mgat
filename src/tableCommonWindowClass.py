import tkinter.tix as Tix
import tkinter.filedialog as Fd
import random, shutil
from tkinter import *
import tkinter.ttk as ttk
import textwrap
from tkinter.constants import *
from AppConstants import *
from AppBackupRestoreProc import backupMyGolfDB, restoreMyGolfDB
from AppClasses import *
from AppProc import getIndex
from addEditExpensePopUp import addEditExpensePopUp
from addEditGamesPopUp import addEditGamesPopUp
from addEditTripConfigurationPopUp import addEditTripConfigurationPopUp
from expensesTripReportPopUp import expenseTripReportPopUp
from addEditTeePopUp import addEditTeePopUp
from createScorecardPopUp import createScorecardPopUp
from addEditTripStatsReportPopUp import addEditTripStatsReportPopUp
from AppProc import resizeImage, widgetEnter,convertOrdinaltoString
from AppProc import getAge, convertDateStringToOrdinal, updateMyAppConstant
from AppProc import stripChar
from PIL import Image, ImageTk
from PIL.Image import NORMAL
import AppCRUD as CRUD
import AppCRUDGolfers as CRUDGolfers
import AppCRUDAddresses as CRUDAddresses
import AppCRUDExpenses as CRUDExpenses
from AppCRUDGames import calculateCurrentHdcp
from AppDialogClass import AppDisplayAbout, AppRecordDuplication, AppQuestionRequest, AppDisplayUnderConstruction
from AppImportScorecardDialog import AppImportScorecardDialog
import tkinter.messagebox as Mb
import calendar, re
import platform
from dateutil import parser

class tableCommonWindowClass(Tix.Frame):
    def __init__(self, parent, aCloseCommand, windowTitle, loginData, tableName):
        Tix.Frame.__init__(self, parent)
        parent.protocol("WM_DELETE_WINDOW", self._exitWindow)
        self.tableName = tableName
        self.accessLevel = loginData[1]
        self.loginData = loginData
        self.CloseMe = aCloseCommand
        self.parent = parent
        self.curRecNumV = Tix.IntVar()
        self.curRecNumV.set(0)
        self.curRecBeforeOp = self.curRecNumV.get()
        self.prevFind = False
        self.curFind = False
        self.curOp = 'default'
        self.recList = []
        self.updaterID = 0
        self.mainAreaRowCount = 0
        self.mainAreaColumnCount = 0
        self.findValues = []
        self.viewSet = Tix.StringVar()
        self.viewSet.set("Active")  # Active, All, or Closed
        self.sortFieldID = 0  # Last Name
        self.sortID = Tix.StringVar()
        self.sortID.set('Default')  # Department, Workgroup, Last Name
        self.deletedV = Tix.StringVar()
        self.deletedV.trace('w', self._setAsDeleted)
        ############################################################ Window Common Configuration
        self.dirty = False
        mainWInWidth = 400
        mainWinHeight = 400
        self.configure(width=mainWInWidth, height=mainWinHeight)
        self.columnCountTotal = 7
        for i in range(self.columnCountTotal):
            self.columnconfigure(i, weight=1)

        ############################################################ Window Title Area
        self.rowCount = 0
        self.columnCount = 0
        wMsg = Tix.Label(self, text=windowTitle, font=fontMonstrousB)
        wMsg.grid(row=self.rowCount, column=self.columnCount, columnspan=self.columnCountTotal, sticky=E + W)
        self.rowconfigure(self.rowCount, weight=0)

        self.displayCurFunction = AppFunctionDisplayLabel(self)
        self.displayCurFunction.grid(row=self.rowCount, column=self.columnCount, columnspan=self.columnCountTotal,
                                     sticky=E + S)

        self.displayCurUser = AppUserDisplayLabel(self, " ")
        self.displayCurUser.grid(row=self.rowCount, column=self.columnCount, columnspan=self.columnCountTotal,
                                 sticky=W + S)

        ############################################################ Main Frame Area
        self.rowCount = self.rowCount + 1
        self.columnCount = 0
        self.mainArea = Tix.Frame(self, border=3, relief='groove', takefocus=0)
        self.mainArea.grid(row=self.rowCount, column=self.columnCount, columnspan=self.columnCountTotal,
                           sticky=N + S + E + W)
        self.mainAreaColumnTotal = 21
        for i in range(self.mainAreaColumnTotal):
            self.mainArea.columnconfigure(i, weight=1)
        self.rowconfigure(self.rowCount, weight=1)

        ############################################################ Command Frame Area
        self.rowCount = self.rowCount + 1
        self.columnCount = 0

        self.myDatabaseBar = databaseCommands(self, self.accessLevel)
        self.myDatabaseBar.grid(row=self.rowCount, column=self.columnCount, sticky=N + W + S)

        #  This frame is used for commands specific to current class
        self.columnCount = self.columnCount + 1
        self.commandFrame = Tix.Frame(self)
        self.commandFrame.grid(row=self.rowCount, column=self.columnCount, columnspan=self.columnCountTotal - 2,
                               sticky=N + S)
        self.viewSortUpdate = AppViewSortFormat(self.commandFrame)
        self.viewSortUpdate.grid(row=0, column=0, sticky=E + N + S)
        self.viewSet.trace('w', self.updateViewSortDisplay)
        self.sortID.trace('w', self.updateViewSortDisplay)

        self.myNavBar = navDatabaseTool(self)
        self.myNavBar.grid(row=self.rowCount, column=self.columnCountTotal - 1, sticky=N + E + S)
        self.curRecNumV.trace('w', self.myNavBar.updateRecordNumber)

        self.rowconfigure(self.rowCount, weight=0)

        ############################################################ Message Bar Area
        self.rowCount = self.rowCount + 1
        self.columnCount = 0
        self.myMsgBar = messageBar(self)
        self.myMsgBar.grid(row=self.rowCount, column=self.columnCount, columnspan=self.columnCountTotal, sticky=W + S,
                           padx=5)
        self.rowconfigure(self.rowCount, weight=0)

        ########################################################### Display records
        self.recordFrame = Tix.Frame(self.mainArea, takefocus=0)
        self.recordFrame.grid(row=0, column=0, columnspan=self.mainAreaColumnTotal, sticky=N + S + E + W)


        wMsg = Tix.Label(self.recordFrame, text='No Records Found', font=fontMonstrousB)
        wMsg.grid(row=0, column=0)

        ############################################################ Initial Navigation State
        if len(self.recList) == 0:
            self.navigationDisable('')

        ############################################################ Common Window Binding
        self.parent.bind('<End>', self.endKeyPressed)
        self.bindAllSpecialKeys()
        self.openRequiredImages()
        self.createMenu()

    #        self.loginWindow.bind('<Home>', self.focusLoginName)

    ############################################################ Table Specific Commands
    #
    #   These commands will be redefined for each table that invokes this class
    #   Manipulation of data is specific to each tables.
    #
    def openRequiredImages(self):
        self.theListAddImage = resizeImage(Image.open(addCommandRecImage), AppButtonSmallHeight, AppButtonSmallWidth)
        self.theListAddImageGREY = resizeImage(Image.open(addCommandRecImageDisable), AppButtonSmallHeight, AppButtonSmallWidth)

        self.theListViewImage = resizeImage(Image.open(viewCommandImage), AppButtonSmallHeight, AppButtonSmallWidth)
        self.theListViewImageGREY = resizeImage(Image.open(viewCommandImageDisable), AppButtonSmallHeight, AppButtonSmallWidth)

        self.theListArchiveImage = resizeImage(Image.open(archiveCommandImage), AppButtonSmallHeight, AppButtonSmallWidth)
        self.theListArchiveImageGREY = resizeImage(Image.open(archiveCommandImageDisable), AppButtonSmallHeight, AppButtonSmallWidth)

        self.theListEditImage = resizeImage(Image.open(editRecImage), AppButtonSmallHeight, AppButtonSmallWidth)
        self.theListEditImageGREY = resizeImage(Image.open(editRecImageDisable), AppButtonSmallHeight, AppButtonSmallWidth)

        self.theListDeleteImage = resizeImage(Image.open(deleteCommandRecImage), AppButtonSmallHeight, AppButtonSmallWidth)
        self.theListDeleteImageGREY = resizeImage(Image.open(deleteCommandRecImageDisable), AppButtonSmallHeight, AppButtonSmallWidth)

        self.theListDuplicateImage = resizeImage(Image.open(duplicateRecImage), AppButtonSmallHeight, AppButtonSmallWidth)
        self.theListDuplicateImageGREY = resizeImage(Image.open(duplicateRecImageDisable), AppButtonSmallHeight, AppButtonSmallWidth)

        self.theListAddRoundImage = resizeImage(Image.open(addRoundCommandImage), AppButtonSmallHeight, AppButtonSmallWidth)
        self.theListAddRoundImageGREY = resizeImage(Image.open(addRoundCommandImageDisable), AppButtonSmallHeight, AppButtonSmallWidth)

        self.theListshow9Image = resizeImage(Image.open(show9Image), AppButtonSmallHeight,AppButtonSmallWidth)
        self.theListhide9Image = resizeImage(Image.open(hide9Image), AppButtonSmallHeight,AppButtonSmallWidth)

    def bindAllSpecialKeys(self):
        self.parent.bind("<Alt-a>", lambda viewID='All': self.basicViewsList('All'))
        self.parent.bind('<Alt-c>', lambda viewID='Closed': self.basicViewsList('Closed'))
        self.parent.bind('<Alt-d>', lambda viewID='Active': self.basicViewsList('Active')) # d for default view
        self.parent.bind('<Alt-E>', self._exitWindow)
        self.parent.bind('<Alt-e>', self._exitWindow)
        self.parent.bind('<Alt-r>', lambda mode='add': self.displayAddGameWindow('add'))
        self.parent.bind('<Alt-t>', lambda mode='add': self.displayAddTeeWindow('add'))
        self.parent.bind('<Alt-v>', self.displayStatsCompareWindow)
        self.parent.bind('<Alt-i>', self.startScorecardImport)
        self.parent.bind('<Alt-s>', lambda mode='add': self.displayCreateScorecardWindow('add'))
        self.parent.bind('<Escape>', self.cancelCommand)
        self.parent.bind('<Right>', self.nextRecord)
        self.parent.bind('<Left>', self.prvRecord)
        self.parent.bind('<Control-f>', self.findCommand)
        self.parent.bind('<Control-e>', self.editCommand)
        self.parent.bind('<Control-a>', self.addCommand)
        self.parent.bind('<Control-d>', self.duplicateCommand)
        self.parent.bind('<Control-c>', self.cancelCommand)
        self.parent.bind('<Control-s>', self.saveCommand)
        self.parent.bind('<Control-x>', self.deleteCommand)


    def unBindAllSpecialKeys(self):
        self.parent.unbind('<Alt-a>')
        self.parent.unbind('<Alt-c>')
        self.parent.unbind('<Alt-d>') # d for default view
        self.parent.unbind('<Escape>')
        self.parent.unbind('<Alt-e>')
        self.parent.unbind('<Alt-E>')
        self.parent.unbind('<Alt-r>')
        self.parent.unbind('<Alt-t>')
        self.parent.unbind('<Alt-v>')
        self.parent.unbind('<Alt-s>')
        self.parent.unbind('<Alt-i>')
        self.parent.unbind('<Right>')
        self.parent.unbind('<Left>')
        self.parent.unbind('<Control-f>')
        self.parent.unbind('<Control-e>')
        self.parent.unbind('<Control-a>')
        self.parent.unbind('<Control-d>')
        self.parent.unbind('<Control-s>')
        self.parent.unbind('<Control-x>')

    def _setAsDeleted(self, *args):
        #
        #  At a minimun, record action are disabled.
        #  Additionnal commands can be added, see person class
        #  for override example for this method.
        #
        if self.recList[self.curRecNumV.get()][self.numberOfFields - 3] == 'Y':
            self.myDatabaseBar.deleteState()
            self.recordFrame.config(highlightcolor=AppDeletedHighlightColor, takefocus=0)
            self.recordFrame.focus()
        else:
            self.myDatabaseBar.defaultState()
            self.recordFrame.config(highlightcolor=AppDefaultForeground, takefocus=0)
            self.recordFrame.focus()

    def updateViewSortDisplay(self, *args):
        self.viewSortUpdate.load(self.viewSet.get(), self.sortID.get())

    def createMenu(self):
        self.menubar = Menu(self.parent, foreground=AppDefaultForeground, font=AppDefaultMenuFont)
        self.filemenu = Menu(self.menubar, foreground=AppDefaultForeground, font=AppDefaultMenuFont, tearoff=0)
        #        filemenu.add_command(label="Close", command=self.donothing)
        self.filemenu.add_command(label="Trip Configuration", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                  command=self.displayTripConfigurationWindow)

        if (self.accessLevel == 'Root' or self.accessLevel == 'Maintenance'):
            self.filemenu.add_command(label="Backup MyGolf App", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                      command=backupMyGolfDB)
            self.filemenu.add_command(label="Restore MyGolf App", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                      command=restoreMyGolfDB)
        self.filemenu.add_separator()
        self.filemenu.add_command(label="Exit", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                  command=self._exitWindow, accelerator="Alt+E")
        self.menubar.add_cascade(label="File", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                 menu=self.filemenu)

        self.gamesmenu = Menu(self.menubar, foreground=AppDefaultForeground, font=AppDefaultMenuFont, tearoff=0)
        self.gamesmenu.add_command
        self.gamesmenu.add_command(label="Add a Round", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                   command=lambda mode='add': self.displayAddGameWindow(mode),
                                   accelerator='Alt+R')
        self.gamesmenu.add_command(label="Import Scorecard", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                   command=self.startScorecardImport,
                                   accelerator='Alt+I')
        self.menubar.add_cascade(label="Rounds", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                 menu=self.gamesmenu)

        self.coursesmenu = Menu(self.menubar, foreground=AppDefaultForeground, font=AppDefaultMenuFont, tearoff=0)
        self.coursesmenu.add_command
        self.coursesmenu.add_command(label="Add a Tee", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                   command=lambda mode='add': self.displayAddTeeWindow(mode),
                                   accelerator='Alt+T')
        self.coursesmenu.add_command(label="Create Scorecard", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                   command=lambda mode='add': self.displayCreateScorecardWindow(mode),
                                   accelerator='Alt+S')
        self.menubar.add_cascade(label="Courses", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                 menu=self.coursesmenu)


        self.expensesmenu = Menu(self.menubar, foreground=AppDefaultForeground, font=AppDefaultMenuFont, tearoff=0)
        self.expensesmenu.add_command
        self.expensesmenu.add_command(label="Add Expenses", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                   command= self.displayAddExpensesWindow)
        self.expensesmenu.add_command(label="Expense Trip Report", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                   command= self.displayExpensesTripReportWindow)
        self.menubar.add_cascade(label="Expenses", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                 menu=self.expensesmenu)


        self.reportsmenu = Menu(self.menubar, foreground=AppDefaultForeground, font=AppDefaultMenuFont, tearoff=0)
        self.reportsmenu.add_command
        self.reportsmenu.add_command(label="Trip Stats Comparison", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                    command= self.displayStatsCompareWindow,
                                    accelerator='Alt+V')
        self.reportsmenu.add_command(label="Expense Trip Report", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                    command= self.displayExpensesTripReportWindow)
        self.menubar.add_cascade(label="Reports", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                 menu=self.reportsmenu)

        self.viewsmenu = Menu(self.menubar, foreground=AppDefaultForeground, font=AppDefaultMenuFont, tearoff=0)
        self.viewsmenu.add_command(label="All (Alt-A)", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                   command=lambda viewID='All': self.basicViewsList(viewID),
                                   accelerator='Alt+A')
        self.viewsmenu.add_command(label="Active (Default)", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                   command=lambda viewID='Active': self.basicViewsList(viewID),
                                   accelerator='Alt+D')
        self.viewsmenu.add_command(label="Closed", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                   command=lambda viewID='Closed': self.basicViewsList(viewID),
                                   accelerator='Alt+C')
        self.viewsmenu.add_separator()
        self.menubar.add_cascade(label="Views", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                 menu=self.viewsmenu)

        #        self.viewsPersonnelmenu = Menu(self.menubar, foreground=AppDefaultForeground, font=AppDefaultMenuFont, tearoff=0)
        #        self.menubar.add_cascade(label="More Views", foreground=AppDefaultForeground, font=AppDefaultMenuFont, menu=self.viewsPersonnelmenu)
        self.tableSpecificMenu()
        '''
        self.setCommandmenu = Menu(self.menubar, tearoff=0)
        self.setCommandmenu.add_command(label="Delete Set", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                        command=lambda tableName=self.tableName: self.deleteSetCommand(tableName))
        self.setCommandmenu.add_command(label="Undelete Set", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                        command=lambda tableName=self.tableName: self.unDeleteSetCommand(tableName))
        self.setCommandmenu.add_separator()
        self.menubar.add_cascade(label="Set Operations", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                 menu=self.setCommandmenu)
        '''

        self.helpmenu = Menu(self.menubar, tearoff=0)
        self.helpmenu.add_command(label="About...", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                  command = self.displayAbout)
        self.menubar.add_cascade(label="Help", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                 menu=self.helpmenu)

        self.parent.config(menu=self.menubar)

    def underConstruction(self, *args):
        AppDisplayUnderConstruction(self)

    def _exitWindow(self, *args):
        if self._verifyExitSafe('form') == True:
            self.CloseMe()

    def _verifyExitSafe(self, type):
        if type == 'form':
            aPhrase = 'exiting the current form?'
        elif type == 'cancel':
            aPhrase = 'with the current cancel command?'
        else:
            aPhrase = 'with the current command?'

        if self.dirty == True:
            aMsg = "The record currently in {0} mode has not been saved.\n" \
                   "Cancelling now would result in lost of data.\n" \
                   "Do you wish to continue {1}".format(self.curOp, aPhrase)
            ans = AppQuestionRequest(self,'Cancel Current Command', aMsg)
            if ans.result == True:
                self.dirty = False
            return ans.result
        else:
            return True

    def displayAbout(self, *args):
        AppDisplayAbout(self)

    def updateUponReturnMethod(self, *args):
        #
        #  Defined in specific windows
        #
        pass

    def displayStatsCompareWindow(self, *args):
        self.statsCompareWindow = Tix.Toplevel()
        self.statsCompareWindow.transient(self)

        myGeo = self.winfo_geometry()
        myNewGeo = '+{0}+{1}'.format((int(myGeo.split("+")[1]) + 40), (int(myGeo.split("+")[2]) + 40))
        self.statsCompareWindow.geometry(myNewGeo)

        addEditTripStatsReportPopUp(self.statsCompareWindow, self.OnAddEditStatsCompareWindowClose,
                                    self.loginData).pack(fill=BOTH, expand=1)

    def OnAddEditStatsCompareWindowClose(self):
        self.statsCompareWindow.destroy()

    def displayAddTeeWindow(self, mode=None, courseID=None, teeID=None, *args):
        AddEditTeeWindow = Tix.Toplevel()
        AddEditTeeWindow.transient(self)

        myGeo = self.winfo_geometry()
        myNewGeo = '+{0}+{1}'.format((int(myGeo.split("+")[1]) + 40), (int(myGeo.split("+")[2]) + 40))
        AddEditTeeWindow.geometry(myNewGeo)
        # golferData should replace the 3

        addEditTeePopUp(AddEditTeeWindow, None, mode, self.loginData, self.updateUponReturnMethod, teeID, courseID).pack(fill=BOTH, expand=1)

    def startScorecardImport(self, *args):
        AppImportScorecardDialog(self, "Importing Scorecard")

    def displayAddGameWindow(self, mode=None, golferID=None, gameID=None, teeID = None, courseID = None,
                             aDate = None, *args):
        self.AddEditGamesWindow = Tix.Toplevel()
        self.AddEditGamesWindow.transient(self)

        myGeo = self.winfo_geometry()
        myNewGeo = '+{0}+{1}'.format((int(myGeo.split("+")[1]) + 40), (int(myGeo.split("+")[2]) + 40))
        self.AddEditGamesWindow.geometry(myNewGeo)
        # golferData should replace the 3
        if self.tableName == 'Golfers':
            addEditGamesPopUp(self.AddEditGamesWindow, self.OnAddEditGamesWindowClose, mode, gameID,
                              self.updateUponReturnMethod, self.loginData, golferID, teeID, courseID,
                              aDate, self.withdrawAddEditGamesWindow).pack(fill=BOTH, expand=1)
        else:
            addEditGamesPopUp(self.AddEditGamesWindow, self.OnAddEditGamesWindowClose, mode, gameID,
                              None, self.loginData, golferID, teeID, courseID, aDate,
                              self.withdrawAddEditGamesWindow).pack(fill=BOTH, expand=1)

    def OnAddEditGamesWindowClose(self):
        self.AddEditGamesWindow.destroy()

    def withdrawAddEditGamesWindow(self):
        self.AddEditGamesWindow.withdraw()

    def displayCreateScorecardWindow(self, mode=None, courseID = None, *args):
        pass
        self.createScorecardWindow = Tix.Toplevel()
        self.createScorecardWindow.transient(self)

        myGeo = self.winfo_geometry()
        myNewGeo = '+{0}+{1}'.format((int(myGeo.split("+")[1]) + 40), (int(myGeo.split("+")[2]) + 40))
        self.createScorecardWindow.geometry(myNewGeo)

        if self.windowTitle == 'Courses':
            courseID = self.recList[self.curRecNumV.get()][0]

        createScorecardPopUp(self.createScorecardWindow, self.OnCreateScorecardWindowClose, mode, courseID,
                             self.loginData).pack(fill=BOTH, expand=1)

    def OnCreateScorecardWindowClose(self):
        self.createScorecardWindow.destroy()

    def displayTripConfigurationWindow(self, *args):
        pass
        self.tripConfigurationWindow = Tix.Toplevel()
        self.tripConfigurationWindow.transient(self)

        myGeo = self.winfo_geometry()
        myNewGeo = '+{0}+{1}'.format((int(myGeo.split("+")[1]) + 40), (int(myGeo.split("+")[2]) + 40))
        self.tripConfigurationWindow.geometry(myNewGeo)
        # golferData should replace the 3
        addEditTripConfigurationPopUp(self.tripConfigurationWindow, self.OnTripConfigurationWindowClose, self.loginData).pack(fill=BOTH, expand=1)

        # aWindow, aCloseCommand, mode, expenseID, updateExpenseList, loginData
 #       self.wait_window(self.tripConfigurationWindow) # tripConfiguration has to terminate for current window to resume

    def OnTripConfigurationWindowClose(self):
        self.tripConfigurationWindow.destroy()

    def displayAddExpensesWindow(self):
        pass
        self.AddEditExpensesWindow = Tix.Toplevel()
        self.AddEditExpensesWindow.transient(self)

        myGeo = self.winfo_geometry()
        myNewGeo = '+{0}+{1}'.format((int(myGeo.split("+")[1]) + 40), (int(myGeo.split("+")[2]) + 40))
        self.AddEditExpensesWindow.geometry(myNewGeo)
        # golferData should replace the 3
        addEditExpensePopUp(self.AddEditExpensesWindow, None, 'add', None, self.updateUponReturnMethod, self.loginData).pack(fill=BOTH, expand=1)

    def displayExpensesTripReportWindow(self):
        pass
        self.ExpensesTripReportWindow = Tix.Toplevel()
        self.ExpensesTripReportWindow.transient(self)

        myGeo = self.winfo_geometry()
        myNewGeo = '+{0}+{1}'.format((int(myGeo.split("+")[1]) + 40), (int(myGeo.split("+")[2]) + 40))
        self.ExpensesTripReportWindow.geometry(myNewGeo)

        expenseTripReportPopUp(self.ExpensesTripReportWindow, None, self.loginData).pack(fill=BOTH, expand=1)

    def tableSpecificMenu(self):
        pass

    def sortingList(self, sortID):
        self.sortID.set(sortID)  # set the type
        self.sort()  # execute the sort

    def sort(self):
        pass

    def getTableData(self):
        pass  # Table specific

    def _getNewRecList(self):
        self.getTableData()
        self.curRecNumV.set(self.curRecNumV.get())

        if len(self.recAll) > 0:
            self.numberOfFields = len(self.recAll[0])
        else:
            self.numberOfFields = 0
        self.recList = self.recAll
        self.curRecNumV.set(self.curRecNumV.get())  # forces display of record number

    def _reDisplayRecordFrame(self):
        self.recordFrame.destroy()
        self._getNewRecList()
        self._displayRecordFrame()

    def _buildWindowsFields(self, aFrame):
        pass

    def _aDirtyMethod(self, *args):
        self.dirty = True

    def _displayRecordFrame(self):
        self.recordFrame = AppFrame(self.mainArea, 1)
        self.recordFrame.grid(row=self.mainAreaRowCount, column=self.mainAreaColumnCount,
                              columnspan=self.mainAreaColumnTotal, sticky=N + S + E + W)
        self.recordFrame.addAccentBackground()
        self.recordFrame.addDeletedHighlight()
        self.recordFrame.noBorder()
        self.recordFrame.stretchCurrentRow()

        if len(self.recList) > 0:
            self._buildWindowsFields(self.recordFrame)
            self.recordFrame.rowconfigure(self.recordFrame.row, weight=1)
            self.navigationEnable(self.accessLevel)
            self.basicViewsList('Active') # Calls the displayRec method

        elif self.curOp == 'add':
            self._buildWindowsFields(self.recordFrame)
            self.recordFrame.rowconfigure(self.recordFrame.row, weight=1)

        else:
            wMsg = Tix.Label(self.recordFrame, text='No Records Found in Current List.', font=fontMonstrousB)
            wMsg.grid(row=0, column=0, columnspan=self.recordFrame.columnTotal)
#
#   MUST BE REVIEWED
#
    def basicViewsList(self, viewID):
        if self.curOp == 'add' or self.curOp == 'edit':
            aMsg = "ERROR: Invalid request. Complete add/edit operation before changing views."
            self.myMsgBar.newMessage('error', aMsg)
        else:
            pass
            self.setViewDefault()
            if len(self.recList) == 0:
                self._buildWindowsFields(self.recordFrame)

            self.deletedFieldIdx = len(self.recAll[0]) - 3
            if viewID == 'All':
                self.viewSet.set("All")
                temp = []
                for line in range(len(self.recAll)):
                    temp.append(self.recAll[line])
                self.recList = temp
                self.curRecNumV.set(0)  # reset the record being viewed to 0
            elif viewID == 'Active':
                temp = []
                for line in range(len(self.recAll)):
                    if self.recAll[line][self.deletedFieldIdx] == 'N':  # is the item active (not deleted)
                        temp.append(self.recAll[line])
                if len(temp) > 0:
                    self.viewSet.set('Active')
                    self.recList = temp
                    self.curRecNumV.set(0)  # reset the record being viewed to 0
                else:
                    aMsg = "WARNING: There are no closed/deleted records in this table."
                    self.myMsgBar.newMessage('warning', aMsg)
            elif viewID == 'Closed':
                temp = []
                for line in range(len(self.recAll)):
                    if self.recAll[line][self.deletedFieldIdx] == 'Y':  # is the item closed (deleted)
                        temp.append(self.recAll[line])
                if len(temp) > 0:
                    self.viewSet.set('Closed')
                    self.recList = temp
                    self.curRecNumV.set(0)  # reset the record being viewed to 0
                else:
                    aMsg = "WARNING: There are no closed/deleted records in this table."
                    self.myMsgBar.newMessage('warning', aMsg)

            self.navigationEnable(self.accessLevel)
            self.myNavBar.enableNavigation()
            self.displayRec()  # display current record

    def endKeyPressed(self, Event):
        self.myDatabaseBar.saveCommand.focus()

    def nextRecord(self, *args):
        if len(self.recList) > 0:
            if self.curRecNumV.get() < (len(self.recList) - 1):  # Don't go past last record
                self.curRecNumV.set(self.curRecNumV.get() + 1)
                self.displayRec()
        self.dirty = False

    def prvRecord(self, *args):

        if len(self.recList) > 0:
            if self.curRecNumV.get() > 0:  # Don't go past first record
                self.curRecNumV.set(self.curRecNumV.get() - 1)
                self.displayRec()
        self.dirty = False

    def lastRecord(self, *args):

        if len(self.recList) > 0:
            self.curRecNumV.set((len(self.recList) - 1))
            self.displayRec()
        self.dirty = False

    def firstRecord(self, *args):

        if len(self.recList) > 0:
            self.curRecNumV.set(0)
            self.displayRec()
        self.dirty = False

    def displayRec(self, *args):
        # Table Specific
        pass

    def refresh(self, *args):
        self.clearPopups()
        self._reDisplayRecordFrame()
        self.dirty = False

    def _updateRecList(self, ID, anItem):
        self.resetCurOp()
        self.recList.append(anItem)
        self.curRecNumV.set(len(self.recList) - 1)
        newIdx = getIndex(ID, self.recList)
        self.curRecNumV.set(newIdx)
        self.sort()
        self.fieldsDisable()

    def _updateRecListAll(self, idxRecListAll, ID, anItem):
        self.recAll.pop(idxRecListAll)
        self.recList.pop(self.curRecNumV.get())  # Remove the item that was modified
        self.recAll.insert(idxRecListAll, anItem)
        self.recList.insert(self.curRecNumV.get(), anItem)
        self.curRecNumV.set(len(self.recList) - 1)
        self.resetCurOp()  # Sort has to execute in default mode
        newIdx = getIndex(ID, self.recList)
        self.curRecNumV.set(newIdx)
        self.sort() # Performs a displayRec
        self.fieldsDisable()

    def resetCurOp(self):
        if self.curFind == True:
            self.curOp = 'find'
            self.displayCurFunction.setFindResultMode()
        else:
            self.curOp = 'default'
            self.displayCurFunction.setDefaultMode()

    def setViewDefault(self):

        self.myDatabaseBar.refreshState()
        self.prevFind = False
        self.curFind = False
        self.curOp = 'default'
        self.displayCurFunction.setDefaultMode()
        self.myNavBar.updateRecordNumber()
        self.fieldsDisable()

    def windowViewDefault(self):
        self.basicViewsList('Active')
        self.sortingList('Default')
        self.dirty = False

    def refreshWindow(self, *args):
        #
        #  Refresh all DB records
        #  (Sync DB with main ECE Data)
        #

        self.myDatabaseBar.refreshState()
        self.refresh()  # reloads from Database
        self.prevFind = False
        self.curFind = False
        self.findValues = []
        self.curOp = 'default'
        self.displayCurFunction.setDefaultMode()
        self.windowViewDefault()
        self.myNavBar.updateRecordNumber()
        self.fieldsDisable()
        self.dirty = False

    def findCommand(self, *args):

        self.unBindAllSpecialKeys()
        self.myDatabaseBar.findState()
        self.myNavBar.disableNavigation()
        self.prevFind = self.curFind
        self.curOp = 'find'
        self.displayCurFunction.setFindMode()
        self.find()

    def editCommand(self, *args):

        self.unBindAllSpecialKeys()
        self.myDatabaseBar.editState()
        self.myNavBar.disableNavigation()
        self.curRecBeforeOp = self.curRecNumV.get()
        self.curOp = 'edit'
        self.displayCurFunction.setEditMode()
        self.edit()

    def duplicateCommand(self, *args):

        self.unBindAllSpecialKeys()
        self.myDatabaseBar.addState()
        self.myNavBar.disableNavigation()
        self.curRecBeforeOp = self.curRecNumV.get()
        self.curOp = 'duplicate'
        self.displayCurFunction.setDuplicateMode()
        self.clearPopups()
        self.fieldsEnable()
        self.duplicate()

    def duplicate(self):
        # Table specific.  Will be defined in calling class
        pass

    def edit(self):
        # Table specific.  Will be defined in calling class
        pass

    def find(self):
        # Table specific.  Will be defined in calling class
        pass

    def addCommand(self, *args):

        self.unBindAllSpecialKeys()
        self.myDatabaseBar.addState()
        self.myNavBar.disableNavigation()
        self.curOp = 'add'
        self.curRecBeforeOp = self.curRecNumV.get()
        self.displayCurFunction.setAddMode()
        if len(self.recList) == 0:
            self._displayRecordFrame()
        self.add()

    def add(self):
        # Table specific.  Will be defined in calling class
        pass

    def delete(self, tableName):
        self.unBindAllSpecialKeys()
        aMsg = "Are you sure you want to delete the current record?"
        ans = AppQuestionRequest(self, "Delete Record - {0}".format(tableName), aMsg)
        if ans.result == True:
            anID = self.recList[self.curRecNumV.get()][0]
            CRUD.delete(tableName, anID, convertDateStringToOrdinal(todayDate),
                         self.updaterID)  # remove from DB
            aMsg = "Current Record from table {0} has been deleted.".format(tableName)
            self.myMsgBar.newMessage('info', aMsg)
            idxRecListAll = getIndex(self.recList[self.curRecNumV.get()][0], self.recAll)
            itemToUpdateDeletedField = self.recAll[idxRecListAll]
            itemWithDeletedFieldUpdated = ()
            for i in range(len(itemToUpdateDeletedField)):
                if i == self.numberOfFields - 3:
                    itemWithDeletedFieldUpdated += ('Y',)
                else:
                    itemWithDeletedFieldUpdated += (self.recAll[idxRecListAll][i],)
            self.recAll.pop(idxRecListAll)  # remove the old one
            self.recAll.insert(idxRecListAll, itemWithDeletedFieldUpdated)  # add the new one

            if self.viewSet.get() != 'Closed':
                if self.viewSet.get() == 'All':
                    self.recList.pop(self.curRecNumV.get())  # remove from current list (only if active view)
                    self.recList.insert(self.curRecNumV.get(), itemWithDeletedFieldUpdated)
                else:
                    self.recList.pop(self.curRecNumV.get())  # remove from current list (only if active view)

            if self.curRecBeforeOp == 0 or self.viewSet.get() == 'All':
                self.curRecNumV.set(self.curRecBeforeOp)
            else:
                self.curRecNumV.set(self.curRecBeforeOp - 1)


            self.deleteDependencies(anID)


        self.myDatabaseBar.defaultState()
        self.navigationEnable(self.accessLevel)
        self.resetCurOp()
        self.dirty = False
        self.bindAllSpecialKeys()

    def deleteDependencies(self, ID):
        pass # Table specific

    def unDeleteDependencies(self, ID):
        pass # Table specific

    def saveCommand(self, *args):
        if self.save() == True:
            self.myDatabaseBar.saveState()
            self.bindAllSpecialKeys()
            if len(self.recList) == 0:
                self.navigationDisable(self.accessLevel)
            else:
                self.myNavBar.enableNavigation()
            self.curOp = 'default'
            self.dirty = False

    def save(self):
        # Table specific.  Will be defined in calling class
        pass

    def navEmptyList(self):
        self.navigationDisable(self.accessLevel)

    def clearPopups(self):
        # Table specific.  Will be defined in calling class
        pass

    def unDelete(self, tableName, *args):
        self.curRecBeforeOp = self.curRecNumV.get()

        aMsg = "Are you sure you want to undelete the current record?"
        ans = AppQuestionRequest(self, "Undelete Record - {0}".format(tableName), aMsg)

        if ans.result == True:
            pass
            anID = self.recList[self.curRecNumV.get()][0]
            CRUD.unDelete(tableName, anID, convertDateStringToOrdinal(todayDate),
                           self.updaterID)
            aMsg = "Record has been added to the Active {0} List.".format(tableName)
            self.myMsgBar.newMessage('info', aMsg)

            idxRecListAll = getIndex(self.recList[self.curRecNumV.get()][0], self.recAll)
            itemToUpdateDeletedField = self.recAll[idxRecListAll]
            itemWithDeletedFieldUpdated = ()

            for i in range(len(itemToUpdateDeletedField)):
                if i == self.numberOfFields - 3:
                    itemWithDeletedFieldUpdated += ('N',)
                else:
                    itemWithDeletedFieldUpdated += (self.recAll[idxRecListAll][i],)

            self.recAll.pop(idxRecListAll)  # remove the old one
            self.recAll.insert(idxRecListAll, itemWithDeletedFieldUpdated)  # add the new one

            if self.viewSet.get() == 'Closed':
                self.recList.pop(self.curRecNumV.get())  # remove from current list (only if active view)
                if self.curRecBeforeOp > 0:
                    self.curRecNumV.set(self.curRecBeforeOp - 1)
            elif self.viewSet.get() == 'All':
                self.recList.pop(self.curRecBeforeOp)
                self.recList.insert(self.curRecBeforeOp, itemWithDeletedFieldUpdated)  # add the new one
                self.curRecNumV.set(self.curRecBeforeOp)
            else:
                self.curRecNumV.set(self.curRecBeforeOp)

        self.myDatabaseBar.defaultState()
        self.navigationEnable(self.accessLevel)
        self.resetCurOp()
        self.unDeleteDependencies(anID)
        self.displayRec()

    def cancelCommand(self, *args):
        if self._verifyExitSafe('cancel') == True:
            self.clearPopups()

            if self.curOp == 'add' or self.curOp == 'edit' or self.curOp == 'find' or self.curOp == 'duplicate':
               self.bindAllSpecialKeys()

            self.myDatabaseBar.cancelState()
            if self.prevFind == True or self.curFind == True:
                self.displayCurFunction.setFindResultMode()
            else:
                self.displayCurFunction.setDefaultMode()
            self.curOp = 'default'
            self.navigationEnable(self.accessLevel)
            self.setRequiredFields()
            self.fieldsDisable()
            if len(self.recList) != 0:
                self.displayRec()
            else:
                self._displayRecordFrame()
            if len(self.recList) > 0:
                self._setAsDeleted()

            self.dirty = False

    def unDeleteCommand(self, *args):

        self.myDatabaseBar.deleteState()
        self.myNavBar.disableNavigation()
        if len(self.recList) == 0:
            self.navigationDisable(self.accessLevel)
        self.curOp = 'undelete'
        self.displayCurFunction.setDeleteMode()
        self.curRecBeforeOp = self.curRecNumV.get()
        self.unDelete(self.tableName)
        self.displayRec()
        self.dirty = False

    def deleteCommand(self, *args):

        self.myDatabaseBar.deleteState()
        self.myNavBar.disableNavigation()
        if len(self.recList) == 0:
            self.navigationDisable(self.accessLevel)
        self.curOp = 'delete'
        self.displayCurFunction.setDeleteMode()
        self.curRecBeforeOp = self.curRecNumV.get()
        self.delete(self.tableName)
        self.displayRec()
        self.dirty = False

    def deleteSetCommand(self, tableName, *args):

        self.myDatabaseBar.deleteState()
        self.myNavBar.disableNavigation()
        if len(self.recList) == 0:
            self.navigationDisable(self.accessLevel)
        self.curOp = 'deleteSet'
        self.displayCurFunction.setDeleteSetMode()
        self.curRecBeforeOp = self.curRecNumV.get()
        self.deleteSet(tableName)

    def deleteSet(self, tableName):
        TotalRecordsDeleted = len(self.recList)
        aMsg = "Are you sure you want to delete All {0} Records in the current Set?".format(TotalRecordsDeleted)
        ans = AppQuestionRequest(self, "Delete Set - {0}".format(tableName), aMsg)
        if ans.result == True:
            pass
            '''
           AppDB.deleteSet(tableName, self.recList, convertDateStringToOrdinal(todayDate),
                            self.updaterID)  # remove from DB
            #
            #    Following avoids reloading the whole table after removal field is updated above.
            #    Removes it from current view and the over all list is also updated to reflect
            #  the new deleted status.
            #
            for i in range(len(self.recList)):
                idxRecListAll = getIndex(self.recList[i][0], self.recAll)
                itemToUpdateDeletedField = self.recAll[idxRecListAll]
                itemWithDeletedFieldUpdated = ()
                for i in range(len(itemToUpdateDeletedField)):
                    if i == self.numberOfFields - 3:
                        itemWithDeletedFieldUpdated += ('Y',)
                    else:
                        itemWithDeletedFieldUpdated += (self.recAll[idxRecListAll][i],)
                self.recAll.pop(idxRecListAll)  # remove the old one
                self.recAll.insert(idxRecListAll, itemWithDeletedFieldUpdated)  # add the new one
            aMsg = "{0} records were removed from the {0} Active List.".format(TotalRecordsDeleted, self.tableName)
            self.myMsgBar.newMessage('info', aMsg)

            if self.viewSet.get() != 'Closed':
                for i in range(len(self.recList)):
                    self.recList.pop(0)
            else:
                self.basicViewsList('Closed')
            '''
        self.curRecNumV.set(
            0)  # Only if root was in the list, self.displayRec will detect if there is a record in the list
        self.displayRec()
        self.myDatabaseBar.defaultState()
        self.navigationEnable(self.accessLevel)
        self.resetCurOp()

    def unDeleteSetCommand(self, tableName, *args):

        self.myDatabaseBar.deleteState()
        self.myNavBar.disableNavigation()
        if len(self.recList) == 0:
            self.navigationDisable(self.accessLevel)
        self.curOp = 'unDeleteSet'
        self.displayCurFunction.setUnDeleteSetMode()
        self.curRecBeforeOp = self.curRecNumV.get()
        self.unDeleteSet(tableName)

    def unDeleteSet(self, tableName):
        TotalRecordsUnDeleted = len(self.recList)
        aMsg = "Are you sure you want to undelete All {0} records in the current Set?".format(TotalRecordsUnDeleted)
        ans = AppQuestionRequest(self, "UnDelete Set - {0}".format(tableName), aMsg)
        if ans.result == True:
            pass
            '''
            temp = []
            AppDB.unDeleteSet(tableName, self.recList, convertDateStringToOrdinal(todayDate), self.updaterID)  #
            for i in range(len(self.recList)):
                idxRecListAll = getIndex(self.recList[i][0], self.recAll)
                itemToUpdateDeletedField = self.recAll[idxRecListAll]
                itemWithDeletedFieldUpdated = ()
                for i in range(len(itemToUpdateDeletedField)):
                    if i == self.numberOfFields - 3:
                        itemWithDeletedFieldUpdated += ('N',)
                    else:
                        itemWithDeletedFieldUpdated += (self.recAll[idxRecListAll][i],)
                self.recAll.pop(idxRecListAll)  # remove the old one
                self.recAll.insert(idxRecListAll, itemWithDeletedFieldUpdated)  # add the new one
                if self.viewSet.get() != 'Closed':
                    temp.append(itemWithDeletedFieldUpdated)

            aMsg = "{0} records were added from the {1} Active List.".format(TotalRecordsUnDeleted, tableName)
            self.myMsgBar.newMessage('info', aMsg)

            if self.viewSet.get() == 'Closed':
                for i in range(len(self.recList)):
                    self.recList.pop(0)
            else:
                self.recList = temp
            '''
        self.curRecNumV.set(self.curRecBeforeOp)
        self.myDatabaseBar.defaultState()
        self.navigationEnable(self.accessLevel)
        self.resetCurOp()
        self.displayRec()

    def removeFromSetCommand(self, *args):
        if len(self.recList) > 0:
            self.recList.pop(self.curRecNumV.get())  # Remove the item that was modified
            if self.curRecNumV.get() > (len(self.recList) - 1):
                self.curRecNumV.set(len(self.recList) - 1)
            self.curRecNumV.set(self.curRecNumV.get())
            self.displayRec()
            aMsg = 'WARNING: Record was removed from the current view.'
            self.myMsgBar.newMessage('warning', aMsg)

    def setRequiredFields(self):
        pass

    def resetRequiredFields(self):
        pass

    def fieldsClear(self):
        # Table Specific
        pass

    def fieldsEnable(self):
        # Table Specific
        pass

    def fieldsDisable(self):
        # Table Specific
        pass

    def navigationDisable(self, accessLevel):
        self.myDatabaseBar.duplicateCommand.config(state=DISABLED)
        self.myDatabaseBar.buttonFind.config(state=DISABLED)
        self.myDatabaseBar.editCommand.config(state=DISABLED)
        self.myDatabaseBar.deleteButton.config(state=DISABLED)
        self.myDatabaseBar.removeFromSetButton.config(state=DISABLED)
        self.myDatabaseBar.buttonRefresh.config(state=DISABLED)
        if accessLevel == 'View':
            self.myDatabaseBar.buttonFind.config(state=NORMAL)
        self.myNavBar.disableNavigation()

    def navigationEnable(self, accessLevel):
        if self.viewSet.get() == 'Closed':  # All, Active, Closed
            if accessLevel == 'Root':
                self.myDatabaseBar.duplicateCommand.config(state=NORMAL)
                self.myDatabaseBar.buttonUndelete.config(state=NORMAL)
            else:
                self.myDatabaseBar.duplicateCommand.config(state=DISABLED)
            self.myDatabaseBar.buttonFind.config(state=DISABLED)
            self.myDatabaseBar.editCommand.config(state=DISABLED)
            self.myDatabaseBar.deleteButton.config(state=DISABLED)
            self.myDatabaseBar.removeFromSetButton.config(state=NORMAL)
            self.myDatabaseBar.addCommand.config(state=DISABLED)
            self.myDatabaseBar.buttonRefresh.config(state=NORMAL)
        elif accessLevel == 'Root':
            self.myDatabaseBar.duplicateCommand.config(state=NORMAL)
            self.myDatabaseBar.buttonFind.config(state=NORMAL)
            self.myDatabaseBar.editCommand.config(state=NORMAL)
            self.myDatabaseBar.deleteButton.config(state=NORMAL)
            self.myDatabaseBar.removeFromSetButton.config(state=NORMAL)
            self.myDatabaseBar.addCommand.config(state=NORMAL)
            self.myDatabaseBar.buttonRefresh.config(state=NORMAL)
        elif accessLevel == 'Maintenance':
            self.myDatabaseBar.duplicateCommand.config(state=NORMAL)
            self.myDatabaseBar.buttonFind.config(state=NORMAL)
            self.myDatabaseBar.editCommand.config(state=NORMAL)
            self.myDatabaseBar.deleteButton.config(state=NORMAL)
            self.myDatabaseBar.removeFromSetButton.config(state=NORMAL)
            self.myDatabaseBar.addCommand.config(state=NORMAL)
            self.myDatabaseBar.buttonRefresh.config(state=NORMAL)
        elif accessLevel == 'Update':
            self.myDatabaseBar.duplicateCommand.config(state=DISABLED)
            self.myDatabaseBar.buttonFind.config(state=NORMAL)
            self.myDatabaseBar.editCommand.config(state=NORMAL)
            self.myDatabaseBar.deleteButton.config(state=DISABLED)
            self.myDatabaseBar.removeFromSetButton.config(state=NORMAL)
            self.myDatabaseBar.addCommand.config(state=DISABLED)
            self.myDatabaseBar.buttonRefresh.config(state=NORMAL)
        elif accessLevel == 'View':
            self.myDatabaseBar.duplicateCommand.config(state=DISABLED)
            self.myDatabaseBar.buttonFind.config(state=NORMAL)
            self.myDatabaseBar.editCommand.config(state=DISABLED)
            self.myDatabaseBar.deleteButton.config(state=DISABLED)
            self.myDatabaseBar.removeFromSetButton.config(state=NORMAL)
            self.myDatabaseBar.addCommand.config(state=DISABLED)
            self.myDatabaseBar.buttonRefresh.config(state=NORMAL)

        if len(self.recList) == 0:
            self.myDatabaseBar.emptyDBState()
        else:
            self.myNavBar.enableNavigation()


