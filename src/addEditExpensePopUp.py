from popUpWindowClass import popUpWindowClass
from AppClasses import *
from AppConstants import *
from AppProc import convertDateStringToOrdinal, updateMyAppConstant
from AppProc import getAYearFromOrdinal
import AppCRUDGolfers as CRUDGolfers
import AppCRUDExpenses as CRUDExpenses

class addEditExpensePopUp(popUpWindowClass):
    def __init__(self, aWindow, aCloseCommand, mode, expenseID, aMethod, loginData):
        #
        #  loginInfo format: Updater Full Name, Login ID, updaterID
        #
        if mode == 'add':
            self.windowTitle = 'Add Trip Expenses'
        elif mode == 'edit':
            self.windowTitle = 'Edit A Trip Expenses'
        else:
            self.windowTitle = 'An Error Occur in the Call'
        popUpWindowClass.__init__(self, aWindow, aCloseCommand, self.windowTitle, loginData, mode)
        self.tableName = 'MBExpenses'
        self.aCloseCommand = aCloseCommand
        self.aWindow = aWindow
        self.loginData = loginData
        self.expenseIDForEdit = expenseID
        self.aMethod = aMethod # Edit is a reload of the expenses, Add is not assigned
        self.thisExpense = []

        self.configDataRead = updateMyAppConstant(AppConfigFile)
        self.golferComboBoxData = CRUDGolfers.getGolferCB()
        self.golferDictionary = {}
        self.reverseGolferDictionary = {}
        for i in range(len(self.golferComboBoxData)):
            golfer_full = '''{0} {1}'''.format(self.golferComboBoxData[i][1], self.golferComboBoxData[i][2])
            self.golferDictionary.update({golfer_full: self.golferComboBoxData[i][0]})
            self.reverseGolferDictionary.update({self.golferComboBoxData[i][0]: golfer_full})
        self.bigSpender = self.reverseGolferDictionary[int(self.configDataRead[0][9])]
        #
        #  loginInfo format: Updater Full Name, access_Level, loginID
        #
        self.updaterFullname = loginData[0]
        self.updaterID = self.loginData[2]
        self.displayCurUser.setUserName(self.updaterFullname)

        #######################################################################################
        # Inherited all from tableCommanWindowClass
        #
        #  This class intents to change the mainArea only.  Everything else must remain common
        #
        self.mainAreaRowCount = 0
        self.mainAreaColumnCount = 0
        self._reDisplayRecordFrame()
        self.mainArea.rowconfigure(self.mainAreaRowCount, weight=1)
        if self.openningMode == 'add':
            self.addCommand(None)

        if self.openningMode == 'edit':
            self.clearButton.grid_remove()
            self.editCommand(None)

    def add(self, *args):
        self.selectedExpenseDate.load(todayDate)
        self.selectedGolfer.load(self.bigSpender)
        self.selectedExpenseDescription.clear()
        self.selectedAdjustedAmnt.clear()
        self.selectedAmount.clear()
        if self.configDataRead[0][7] == None:
            self.selectedRate.load(1.0)
        else:
            self.selectedRate.load(self.configDataRead[0][7]) # Default currency rate of exchange
            self.selectedCurrency.load(self.configDataRead[0][8]) # Defalut Currency
        self.dirty = False
        self.selectedExpenseDescription.focus()
        self.lastUpdate.load(convertDateStringToOrdinal(todayDate), self.updaterID)
        self.deletedV.set('N')

    def edit(self, *args):
        theExpense = CRUDExpenses.get_MBExpensesForExpenseID(self.expenseIDForEdit)
        self.selectedExpenseDate.load(convertOrdinaltoString(theExpense[0][1]))
        self.selectedGolfer.load(self.reverseGolferDictionary[int(theExpense[0][7])])
        self.selectedExpenseDescription.load(theExpense[0][2])
        self.selectedAdjustedAmnt.load(theExpense[0][6])
        self.selectedAmount.load(theExpense[0][3])
        self.selectedRate.load(theExpense[0][5])
        self.selectedCurrency.load(theExpense[0][4]) # Default Currency
        self.lastUpdate.load(theExpense[0][9], int(theExpense[0][10]))
        self.dirty = False
        self.selectedExpenseDescription.focus()
        self.lastUpdate.load(theExpense[0][len(theExpense) - 2], theExpense[0][len(theExpense) - 1])
        self.deletedV.set(theExpense[0][len(theExpense) - 3])

    def _submitForm(self, *args):
        if self.validateRequiredFields() == True:
            self._updateRevisedAmount()
            self.save()
        # Must be defined in the popUp window.

    def _clearForm(self, *args):
        self.fieldsClear()
        # Must be defined in the popUp window.


    def tableSpecificMenu(self):
        pass
        '''
        self.viewsmenu.add_command
        self.viewsmenu.add_command(label="Active Golfers", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                   command=lambda viewID='Active Golfers': self.basicViewsList(viewID))

        sortsmenu = Menu(self.menubar, foreground=AppDefaultForeground, font=AppDefaultMenuFont, tearoff=0)
        sortsmenu.add_command(label="Default", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Default': self.sortingList(sortID))
        sortsmenu.add_command(label="Last Name", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Last Name': self.sortingList(sortID))
        sortsmenu.add_command(label="First Name", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='First Name': self.sortingList(sortID))
        sortsmenu.add_command(label="Birthday", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Birthday': self.sortingList(sortID))
        #
        # Next Command actually puts the menu up
        #
        self.menubar.add_cascade(label="Sorts", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                 menu=sortsmenu)
        '''

    def sort(self):
        pass
        '''
        if self.curOp == 'default':
            currentID = self.recList[self.curRecNumV.get()][0]
            if self.sortID.get() == 'Default':
                self.recList = sorted(self.recList, key=operator.itemgetter(2,1))
            elif self.sortID.get() == 'Last Name':
                self.recList.sort(key=lambda row: row[2])
            elif self.sortID.get() == 'First Name':
                self.recList.sort(key=lambda row: row[1])
            elif self.sortID.get() == 'Birthday':
                self.recList.sort(key=lambda row: row[4])
            else:
                self.recList.sort(key=lambda row: row[0])
            newIdx = getIndex(currentID, self.recList)
            self.curRecNumV.set(newIdx)
            self.displayRec()
        else:
            aMsg = "ERROR: Invalid request. Must be executed in default state only."
            self.myMsgBar.newMessage('error', aMsg)
        '''

    def _descriptionEnteredAction(self):
        pass

    def _buildWindowsFields(self, aFrame):
        #        headerList1 = ('BASIC','VALUE','VALUED','MULT','TOLE','SIZE','DESCR','NSN1','NSN2','NSN3','NSN4')
        colTotal = 2
        self.fieldsFrame = AppFieldsFrame(aFrame, colTotal)
        self.fieldsFrame.grid(row=0, column=0, sticky=N + S + E + W)
        self.fieldColList = ['Date', 'Buyer', 'Item Description', 'Amount', 'Currency', 'Rate', 'Adjusted Amount']
        self.fieldsFrame.stretchCurrentRow()

        ########################################################### Main Identification Section
        self.expenseFieldFrame = AppColumnAlignFieldFrame(self.fieldsFrame)
        self.expenseFieldFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column,
                                    columnspan=colTotal, sticky=E + W + N + S)

        self.selectedExpenseDate = AppFieldEntryDate(self.expenseFieldFrame, None, None, appDateWidth,
                                                     self._dateEnteredAction, self.myMsgBar, self._aDirtyMethod)
        self.expenseFieldFrame.addNewField('Expense Date', self.selectedExpenseDate)

        self.selectedExpenseDescription = AppFieldEntry(self.expenseFieldFrame, None, None,
                                                        appDescriptionWidth, self._aDirtyMethod)
        self.expenseFieldFrame.addNewField('Item Description', self.selectedExpenseDescription)

        self.golferCBData = AppCBList(CRUDGolfers.getGolfersDictionaryInfo())
        self.selectedGolfer = AppSearchLB(self.expenseFieldFrame, self, None, None,
                                           self.golferCBData.getList(), AppFullNameWidth, None, self.myMsgBar, self._aDirtyMethod, None)
        self.expenseFieldFrame.addNewField('Expense Paid By', self.selectedGolfer)

        self.selectedAmount = AppFieldEntryFloat(self.expenseFieldFrame, None, None, AppAmountWidth, self._aDirtyMethod, self._anAmountEntered)
        self.selectedAmount.addValidationAmount(2,7, self.myMsgBar)
        self.expenseFieldFrame.addNewField('Expense Amount', self.selectedAmount)

        self.selectedCurrency = AppSearchLB(self.expenseFieldFrame, self, None, None, AppCurrencyList,
                                                   AppCurrencyWidth, None, self.myMsgBar, self._aDirtyMethod, self._anAmountEntered)
        self.expenseFieldFrame.addNewField('Expense Currency', self.selectedCurrency)

        self.selectedRate = AppFieldEntryFloat(self.expenseFieldFrame, None, None, AppAmountWidth, self._aDirtyMethod, self._anAmountEntered)
        self.selectedRate.addValidationAmount(5,7, self.myMsgBar)
        self.expenseFieldFrame.addNewField('Exchange Rate', self.selectedRate)

        self.selectedAdjustedAmnt = AppFieldEntryFloat(self.expenseFieldFrame, None, None, AppAmountWidth, None, None)
        self.selectedAdjustedAmnt.addValidationAmount(2,7, self.myMsgBar)
        self.expenseFieldFrame.addNewField('Expense Adjusted Amount', self.selectedAdjustedAmnt)

        ########################################################### Footer Section
        self.fieldsFrame.addRow()
        self.lastUpdate = AppLastUpdateFormat(self.fieldsFrame)
        self.lastUpdate.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, columnspan=colTotal,
                             sticky=E + N + S)

        self.setRequiredFields()
        self.fieldsDisable()

    def fieldsDisable(self):
        self.selectedExpenseDescription.disable()
        self.selectedGolfer.disable()
        self.selectedExpenseDate.disable()
        self.selectedAmount.disable()
        self.selectedCurrency.disable()
        self.selectedRate.disable()
        self.selectedAdjustedAmnt.disable()

    def fieldsEnable(self):
        self.selectedExpenseDescription.enable()
        self.selectedGolfer.enable()
        self.selectedExpenseDate.enable()
        self.selectedAmount.enable()
        self.selectedCurrency.enable()
        self.selectedRate.enable()

    def fieldsClear(self):
        self.selectedExpenseDescription.clear()
        self.selectedGolfer.clear()
        self.selectedExpenseDate.clear()
        self.selectedAmount.clear()
        self.selectedCurrency.clear()
        self.selectedRate.clear()
        self.selectedAdjustedAmnt.clear()

    def setRequiredFields(self):
        # Table specific.  Will be defined in calling class
        self.selectedExpenseDescription.setAsRequiredField()
        self.selectedGolfer.setAsRequiredField()
        self.selectedExpenseDate.setAsRequiredField()
        self.selectedAmount.setAsRequiredField()
        self.selectedCurrency.setAsRequiredField()
        self.selectedRate.setAsRequiredField()

    def resetRequiredFields(self):
        # Table specific.  Will be defined in calling class
        self.selectedExpenseDescription.resetAsRequiredField()
        self.selectedGolfer.resetAsRequiredField()
        self.selectedExpenseDate.resetAsRequiredField()
        self.selectedAmount.resetAsRequiredField()
        self.selectedCurrency.resetAsRequiredField()
        self.selectedRate.resetAsRequiredField()

    def validateRequiredFields(self):
        requiredFieldsEntered = True

        if len(self.selectedExpenseDescription.get()) == 0:
            requiredFieldsEntered = False
            self.selectedExpenseDescription.focus()

        elif len(self.selectedGolfer.get()) == 0:
            requiredFieldsEntered = False
            self.selectedGolfer.focus()

        elif len(self.selectedExpenseDate.get()) == 0:
            requiredFieldsEntered = False
            self.selectedExpenseDate.focus()

        elif self.selectedAmount.get() == None:
            requiredFieldsEntered = False
            self.selectedAmount.focus()

        elif len(self.selectedCurrency.get()) == 0:
            requiredFieldsEntered = False
            self.selectedCurrency.focus()

        elif self.selectedRate.get() == None:
            requiredFieldsEntered = False
            self.selectedRate.focus()

        elif requiredFieldsEntered == False:
            self.myMsgBar.requiredFieldMessage()

        if requiredFieldsEntered == False:
            self.myMsgBar.requiredFieldMessage()
        return requiredFieldsEntered

    def _dateEnteredAction(self, *args):
        self._aDirtyMethod(None)

    def _anAmountEntered(self, *args):
        self._updateRevisedAmount()

    def _updateRevisedAmount(self):
        if self.selectedAmount.get() == None or len(self.selectedCurrency.get()) == 0 or self.selectedRate.get() == None:
            pass
        else:
            currency = self.selectedCurrency.get()
            if currency== 'CDN':
                self.selectedAdjustedAmnt.load(self.selectedAmount.get())
            elif currency== 'US':
                self.selectedAdjustedAmnt.load(self.selectedAmount.get() * self.selectedRate.get())

    def save(self):
        if self.openningMode == 'add':
            anOrdinalDate = convertDateStringToOrdinal(self.selectedExpenseDate.get())
            anAmount = self.selectedAmount.get()
            aRate = self.selectedRate.get()
            adjustedAmount = self.selectedAdjustedAmnt.get()
            aDescription = self.selectedExpenseDescription.get()
            aCurrency = self.selectedCurrency.get()
            aGolferID = self.golferCBData.getID(self.selectedGolfer.get())
            try:
                CRUDExpenses.insert_MBExpenses(anOrdinalDate,aDescription,anAmount, aCurrency, aRate, adjustedAmount,
                                               aGolferID,self.updaterID)
                aMsg = '''An expense of $ {0} by {1} has been added for your {2} Myrtle Beach trip.'''.format(round(self.selectedAdjustedAmnt.get(), 2),
                                                                self.selectedGolfer.get(), getAYearFromOrdinal(anOrdinalDate))
                self.myMsgBar.newMessage('info', aMsg)
                if self.aMethod != None:
                    self.aMethod()
                self.add(None)
            except:
                aMsg = '''ERROR: An error occurred while creating the expense of $ {0} by {1} for your {2} Myrtle Beach trip.'''.format(round(self.selectedAdjustedAmnt.get(), 2),
                                                                self.selectedGolfer.get(), getAYearFromOrdinal(anOrdinalDate))
                self.myMsgBar.newMessage('error', aMsg)

        elif self.openningMode == 'edit':
            anOrdinalDate = convertDateStringToOrdinal(self.selectedExpenseDate.get())
            anAmount = self.selectedAmount.get()
            aRate = self.selectedRate.get()
            adjustedAmount = self.selectedAdjustedAmnt.get()
            aDescription = self.selectedExpenseDescription.get()
            aCurrency = self.selectedCurrency.get()
            aGolferID = self.golferCBData.getID(self.selectedGolfer.get())
            try:
                CRUDExpenses.update_MBExpenses(self.expenseIDForEdit, anOrdinalDate,aDescription,anAmount,aCurrency,aRate,
                                               adjustedAmount, aGolferID, self.updaterID)
                if self.aMethod != None:
                    self.aMethod()
                self.aCloseCommand()

            except:
                aMsg = '''ERROR: An error occurred while saving the expense of $ {0} by {1} for your {2} Myrtle Beach trip.'''.format(round(self.selectedAdjustedAmnt.get(), 2),
                                                                self.selectedGolfer.get(), getAYearFromOrdinal(anOrdinalDate))
                self.myMsgBar.newMessage('error', aMsg)
