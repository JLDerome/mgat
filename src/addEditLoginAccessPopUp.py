from popUpWindowClass import popUpWindowClass
from AppClasses import *
from AppConstants import *
from AppProc import convertDateStringToOrdinal, updateMyAppConstant, exportAppConfig
from AppProc import getAYearFromOrdinal
import AppCRUDGolfers as CRUDGolfers
import AppCRUDExpenses as CRUDExpenses
import AppCRUDLoginAccess as CRUDLoginAccess
import tkinter.messagebox as Mb

class addEditLoginAccessPopUp(popUpWindowClass):
    def __init__(self, aWindow, aCloseCommand, loginData):
        #
        #  loginInfo format: Updater Full Name, access_Level, loginID
        #
        self.windowTitle = 'Adding Login Access'
        popUpWindowClass.__init__(self, aWindow, aCloseCommand, self.windowTitle, loginData, None)
        self.tableName = 'LoginAccess'
        self.aCloseCommand = aCloseCommand
        self.aWindow = aWindow
        self.loginData = loginData

        self.configDataRead = []
        self.golferComboBoxData = CRUDGolfers.getGolferCB()
        self.golferDictionary = {}
        self.reverseGolferDictionary = {}
        for i in range(len(self.golferComboBoxData)):
            golfer_full = '''{0} {1}'''.format(self.golferComboBoxData[i][1], self.golferComboBoxData[i][2])
            self.golferDictionary.update({golfer_full: self.golferComboBoxData[i][0]})
            self.reverseGolferDictionary.update({self.golferComboBoxData[i][0]: golfer_full})
        self.bigSpender = None
        #
        #  loginInfo format: Updater Full Name, access_Level, loginID
        #
        self.updaterFullname = loginData[0]
        self.updaterID = self.loginData[2]
        self.displayCurUser.setUserName(self.updaterFullname)

        #######################################################################################
        # Inherited all from tableCommanWindowClass
        #
        #  This class intents to change the mainArea only.  Everything else must remain common
        #
        self.mainAreaRowCount = 0
        self.mainAreaColumnCount = 0
        self._reDisplayRecordFrame()
        self.mainArea.rowconfigure(self.mainAreaRowCount, weight=1)
        self.setDefaultState()

    def setDefaultState(self):
        self.curOp='default'
        self.fieldsDisable()
        self.displayCurFunction.setDefaultMode()
        self.submitButton.disable()
        self.addButton.enable()
        self.cancelButton.disable()
        self.dirty = False
#        self.loadCurrentConfiguration()
#        self.lastUpdate.load(self.lastUpdateDate,self.updaterFullname)

    def loadCurrentConfiguration(self):
        pass
#        self.configDataRead = updateMyAppConstant(AppConfigFile)
#        self.selectedCurrency.load(self.configDataRead[0][8])
#        self.selectedRate.load(float(self.configDataRead[0][7]))
#        self.selectedEndDate.load(convertOrdinaltoString(self.configDataRead[0][1]))
#        self.selectedStartDate.load(convertOrdinaltoString(self.configDataRead[0][0]))
#        self.selectedGolfer1.load(self.golferCBData.getName(int(self.configDataRead[0][2])))
#        self.selectedGolfer2.load(self.golferCBData.getName(int(self.configDataRead[0][3])))
#        self.selectedGolfer3.load(self.golferCBData.getName(int(self.configDataRead[0][4])))
#        self.selectedGolfer4.load(self.golferCBData.getName(int(self.configDataRead[0][5])))
#        self.selectedMarker.load(self.golferCBData.getName(int(self.configDataRead[0][6])))
#        self.selectedSpender.load(self.golferCBData.getName(int(self.configDataRead[0][9])))
#        self.bigSpender = self.reverseGolferDictionary[int(self.configDataRead[0][9])]
#
#         self.updaterFullname = self.reverseGolferDictionary[int(self.configDataRead[0][10])]
#
#
        self.lastUpdateDate = self.configDataRead[0][11]  # Leave in ordinal format
        self.dirty = False

    def add(self):
        pass
        self.curOp='add'
        self.displayCurFunction.setAddMode()
        self.addButton.disable()
        self.submitButton.enable()
        self.cancelButton.enable()
        self.fieldsEnable()
        self.dirty = False

    def cancel(self):
        pass
        ans = False
        if self.dirty == True:
            aMsg = "The record currently in {0} mode has not been saved.\n" \
                   "Cancelling now would result in lost of data.\n" \
                   "Do you wish to continue with the cancel current command?".format(self.curOp)
            ans = Mb.askyesno('Cancel Current Command', aMsg)
            if ans == True:
                self.setDefaultState()
                self.fieldsDisable()
        else:
            self.setDefaultState()
            self.fieldsDisable()


    def _submitForm(self, *args):
        if self.validateRequiredFields() == True:
            self.save()
        # Must be defined in the popUp window.

    def _clearForm(self, *args):
        self.fieldsClear()
        # Must be defined in the popUp window.

    def tableSpecificMenu(self):
        pass
        '''
        self.viewsmenu.add_command
        self.viewsmenu.add_command(label="Active Golfers", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                   command=lambda viewID='Active Golfers': self.basicViewsList(viewID))

        sortsmenu = Menu(self.menubar, foreground=AppDefaultForeground, font=AppDefaultMenuFont, tearoff=0)
        sortsmenu.add_command(label="Default", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Default': self.sortingList(sortID))
        sortsmenu.add_command(label="Last Name", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Last Name': self.sortingList(sortID))
        sortsmenu.add_command(label="First Name", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='First Name': self.sortingList(sortID))
        sortsmenu.add_command(label="Birthday", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Birthday': self.sortingList(sortID))
        #
        # Next Command actually puts the menu up
        #
        self.menubar.add_cascade(label="Sorts", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                 menu=sortsmenu)
        '''

    def _exitForm(self, *args):
        if self.dirty == True:
            aMsg = "The record currently in {0} mode has not been saved.\n" \
                   "Exiting now would result in lost of data.\n" \
                   "Do you wish to continue with the exit command?".format(self.curOp)
            ans = Mb.askyesno('Restore My Golf App 2016 Database', aMsg)
            if ans == True:
                self.aCloseCommand()
        else:
            self.aCloseCommand()

    def sort(self):
        pass
        '''
        if self.curOp == 'default':
            currentID = self.recList[self.curRecNumV.get()][0]
            if self.sortID.get() == 'Default':
                self.recList = sorted(self.recList, key=operator.itemgetter(2,1))
            elif self.sortID.get() == 'Last Name':
                self.recList.sort(key=lambda row: row[2])
            elif self.sortID.get() == 'First Name':
                self.recList.sort(key=lambda row: row[1])
            elif self.sortID.get() == 'Birthday':
                self.recList.sort(key=lambda row: row[4])
            else:
                self.recList.sort(key=lambda row: row[0])
            newIdx = getIndex(currentID, self.recList)
            self.curRecNumV.set(newIdx)
            self.displayRec()
        else:
            aMsg = "ERROR: Invalid request. Must be executed in default state only."
            self.myMsgBar.newMessage('error', aMsg)
        '''

    def _descriptionEnteredAction(self):
        pass

    def _buildWindowsFields(self, aFrame):
        #cursor.execute('''CREATE TABLE LoginAccess(
        #               loginID INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
        #               loginName            CHAR(15),
        #               password             CHAR(15),
        #               access_Level         CHAR(15),
        #               deleted              char(1),
        #               lastModified         INTEGER,
        #               updaterID            INTEGER
        #               );''')
        #               FOREIGN KEY(updaterID) REFERENCES Golfers(golferID)

        #        headerList1 = ('BASIC','VALUE','VALUED','MULT','TOLE','SIZE','DESCR','NSN1','NSN2','NSN3','NSN4')
        colTotal = 4
        self.fieldsFrame = AppFieldsFrame(aFrame, colTotal)
        self.fieldsFrame.grid(row=0, column=0, sticky=N + S + E + W)
        self.fieldsFrame.stretchCurrentRow()
        ########################################################### Just a Large Picture

        original = Image.open(AppWelcomeImg)
        image1 = resizeImage(original, welcomeLogoWidthLarge, welcomeLogoHeightLarge)
        panel1 = Label(self.fieldsFrame, image=image1)
        self.display = image1
        panel1.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, sticky=E + W + N + S)

        ########################################################### Login Information Section
        self.fieldsFrame.addColumn()
        aLabel = Label(self.fieldsFrame, text="     ")
        aLabel.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column,
                               sticky=E + W + N + S)
        self.fieldsFrame.addColumn()
        aLabel = Label(self.fieldsFrame, text="     ")
        aLabel.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column,
                               sticky=E + W + N + S)

        ########################################################### Login Information Section
        self.fieldsFrame.addColumn()
        self.loginDataShellFrame = AppBorderFrame(self.fieldsFrame, 1)
        self.loginDataShellFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column,
                               sticky=E + W + N + S)
        self.loginDataShellFrame.addTitle("Login Data")
        self.loginDataShellFrame.noBorder()

        self.loginAccessDataFrame = AppColumnAlignFieldFrame(self.loginDataShellFrame)
        self.loginAccessDataFrame.noBorder()
        self.loginAccessDataFrame.grid(row=self.loginDataShellFrame.row, column=self.loginDataShellFrame.column,
                                       sticky=E + W + N + S)

        self.golferCBData = AppCBList(CRUDGolfers.getGolfersDictionaryInfo())

        self.selectedGolferID = AppSearchLB(self.loginAccessDataFrame, self, None, None,
                                           self.golferCBData.getList(), AppFullNameWidth, None, self.myMsgBar, self._aDirtyMethod, None)
        self.loginAccessDataFrame.addNewField('Golfer', self.selectedGolferID)
        self.loginAccessDataFrame.noStretchCurrentRow()

        self.selectedLoginName = AppFieldEntry(self.loginAccessDataFrame, None, None,
                                               AppFullNameWidth, self._aDirtyMethod)
        self.loginAccessDataFrame.addNewField('Login Name', self.selectedLoginName)
        self.loginAccessDataFrame.noStretchCurrentRow()

        self.selectedPassword = AppFieldEntry(self.loginAccessDataFrame, None, None,
                                               AppFullNameWidth, self._aDirtyMethod)
        self.loginAccessDataFrame.addNewField('Password', self.selectedPassword)
        self.loginAccessDataFrame.noStretchCurrentRow()

        self.selectedAccess_Level = AppSearchLB(self.loginAccessDataFrame, self, None, None,
                                                accessLevelList, AppFullNameWidth, None, self.myMsgBar,
                                           self._aDirtyMethod, None)
        self.loginAccessDataFrame.addNewField('Access Level', self.selectedAccess_Level)
        self.loginAccessDataFrame.noStretchCurrentRow()

        ########################################################### Footer Section
        self.fieldsFrame.addRow()
        self.lastUpdate = AppLastUpdateFormat(self.fieldsFrame)
        self.lastUpdate.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, columnspan=colTotal,
                             sticky=E + N + S)

        self.setRequiredFields()
        self.fieldsDisable()
#        self.clearButton.destroy()  # Not required for this application

    def addWindowSpecificCommand(self):
        self.addButton = AppCmdLineButton(self.commandFrame, 'Add', None, self.add)
        self.commandFrame.addNewCommandButton(self.addButton)
        self.addButton.addToolTip("Add Form")

        self.cancelButton = AppCmdLineButton(self.commandFrame, 'Cancel', None, self.cancel)
        self.commandFrame.addNewCommandButton(self.cancelButton)
        self.cancelButton.addToolTip("Cancel Current Operation")

    def fieldsDisable(self):
        pass
        self.selectedLoginName.disable()
        self.selectedPassword.disable()
        self.selectedAccess_Level.disable()
        self.selectedGolferID.disable()

    def fieldsEnable(self):
        pass
        self.selectedLoginName.enable()
        self.selectedPassword.enable()
        self.selectedAccess_Level.enable()
        self.selectedGolferID.enable()


    def fieldsClear(self):
        pass
        self.selectedLoginName.clear()
        self.selectedPassword.clear()
        self.selectedAccess_Level.clear()
        self.selectedGolferID.clear()


    def setRequiredFields(self):
        # Table specific.  Will be defined in calling class
        pass
        self.selectedLoginName.setAsRequiredField()
        self.selectedPassword.setAsRequiredField()
        self.selectedAccess_Level.setAsRequiredField()
        self.selectedGolferID.setAsRequiredField()

    def resetRequiredFields(self):
        pass
        self.selectedLoginName.resetAsRequiredField()
        self.selectedPassword.resetAsRequiredField()
        self.selectedAccess_Level.resetAsRequiredField()
        self.selectedGolferID.resetAsRequiredField()


    def validateRequiredFields(self):
        requiredFieldsEntered = True

        if len(self.selectedLoginName.get()) == 0:
            requiredFieldsEntered = False
            self.selectedLoginName.focus()

        elif len(self.selectedPassword.get()) == 0:
            requiredFieldsEntered = False
            self.selectedPassword.focus()

        elif len(self.selectedAccess_Level.get()) == 0:
            requiredFieldsEntered = False
            self.selectedAccess_Level.focus()

        elif len(self.selectedGolferID.get()) == 0:
            requiredFieldsEntered = False
            self.selectedGolferID.focus()

        if requiredFieldsEntered == False:
            self.myMsgBar.requiredFieldMessage()
        return requiredFieldsEntered

    def _dateEnteredAction(self, *args):
        self._aDirtyMethod(None)

    def save(self):
        #              loginID INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
        #              loginName            CHAR(15),
        #              password             CHAR(15),
        #              access_Level         CHAR(15),
        #              golferID             INTEGER,
        #              deleted              char(1),
        #              lastModified         INTEGER,
        #              updaterID            INTEGER
        pass
        aLoginName  =  self.selectedLoginName.get()
        aGolferID   =  self.golferCBData.getID(self.selectedGolferID.get())
        aPassword   =  self.selectedPassword.get()
        anAccessLevel = self.selectedAccess_Level.get()

        duplicateLoginName = CRUDLoginAccess.getLoginUser(aLoginName)
        if len(duplicateLoginName) > 0:
            aMsg = "ERROR: login Name {0}, select a different login name.".format(aLoginName)
            self.selectedLoginName.focus()
            self.myMsgBar.newMessage('error', aMsg)
        else:
            try:
                CRUDLoginAccess.insertNewLoginUser(aLoginName,aPassword,anAccessLevel,aGolferID,self.updaterID)
                aMsg = "Login Access {0} for {1} has been added to My Golf App.".format(aLoginName,self.selectedGolferID.get())
                self.selectedLoginName.focus()
                self.myMsgBar.newMessage('info', aMsg)
                self.setDefaultState()
                self.fieldsClear()
            except:
                aMsg = "ERROR: A error occur while creating your login access, contact the administrator if problem persist."
                self.selectedLoginName.focus()
                self.myMsgBar.newMessage('error', aMsg)



