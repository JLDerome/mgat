# import tesserocr
#
# from PIL import Image
#
# print(tesserocr.tesseract_version())  # print tesseract-ocr version
# print(tesserocr.get_languages())  # prints tessdata path and list of available languages
#
# image = Image.open('Golf9TrousTest.jpg')
# print(tesserocr.file_to_text('Golf9TrousTest.jpg')) # print ocr text from image

# from PIL import Image
# from tesserocr import RIL, PyTessBaseAPI, PSM
# from tesserocr import *
# from pytesser3 import image_file_to_string
#
# image = Image.open('Golf9TrousTest.jpg')
# with PyTessBaseAPI() as api:
#     api.SetImage(image)
#     boxes = api.GetComponentImages(RIL.TEXTLINE, True)
#     print('Found {} textline image components.'.format(len(boxes)))
#     for i, (im, box, _, _) in enumerate(boxes):
#         # im is a PIL image object
#         # box is a dict with x, y, w and h keys
#         api.SetRectangle(box['x'], box['y'], box['w'], box['h'])
#         ocrResult = api.GetUTF8Text()
#         conf = api.MeanTextConf()
#         print (u"Box[{0}]: x={x}, y={y}, w={w}, h={h}, "
#                "confidence: {1}, text: {2}").format(i, conf, ocrResult, **box)

import pytesseract
from PIL import Image

# print(tesserocr.tesseract_version())  # print tesseract-ocr version
# print(tesserocr.get_languages())  # prints tessdata path and list of available languages

image = Image.open('Golf9TrousTest.jpg')
print(pytesseract.image_to_string('Golf9TrousTest.jpg')) # print ocr text from image
