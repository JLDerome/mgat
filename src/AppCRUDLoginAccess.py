import sqlite3 as sql
import os
import csv
import AppCRUD as AppCRUD
from AppCRUDGolfers import getGolferFullname
import datetime
from dateutil import parser
from AppProc import getDBLocationFullName, convertDateStringToOrdinal, convertOrdinaltoString
from AppConstants import *

db=None
cursor = None
lidGolfers = 0

 #           cursor.execute('''CREATE TABLE LoginAccess(
 #              uniqueID INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
 #              loginName            CHAR(15),
 #              password             CHAR(15),
 #              access_Level         CHAR(15),
 #              golferID             INTEGER,
 #              deleted              char(1),
 #              lastModified         INTEGER,
 #              updaterID            INTEGER
 #              );''')
 #               FOREIGN KEY(updaterID) REFERENCES Golfers(golferID)



#def insertRootLoginAccess(loginName,password,access_Level,lastModified,updaterID):
#    initDB(getDBLocationFullName())
#    query = '''
#    insert into LoginAccess
#    (loginName, password, access_Level, golferID, deleted, lastModified,updaterID)
#    values (?,?,?,?,?,?,?)'''
#    ans = cursor.execute(query,(loginName,password,access_Level,None, 'N', lastModified,updaterID))
#    closeDB()


def insertNewLoginUser(loginName,password,access_Level,golferID,updaterID):
    # id is declared but is auto increment

    AppCRUD.initDB(getDBLocationFullName())
    query = '''
    insert into LoginAccess 
    (loginName,password,access_Level,golferID,deleted,lastModified,updaterID)
    values (?,?,?,?,?,?,?)'''
    ans = AppCRUD.cursor.execute(query,(loginName,password,access_Level,golferID,'N',convertDateStringToOrdinal(todayDate),updaterID))
    AppCRUD.closeDB()
    return ans
    
def exportCSV_LoginAccess(fileName):
    file = AppDefaultLocation + '/' + BackupDirDefaultName + '/' + fileName
    AppCRUD.initDB(getDBLocationFullName())
    query = '''select * from LoginAccess where loginName != 'root' '''
    data = AppCRUD.cursor.execute(query).fetchall()
    with open(file, 'w', newline='', encoding='ISO-8859-1') as fout:
        for i in range(len(data)):
            csv.writer(fout).writerow(data[i])
    AppCRUD.closeDB()
    return (len(getLoginUsers()) - int(1))  # do not count root

def importCSV_LoginAccess(fileName):
    AppCRUD.initDB(getDBLocationFullName())
    file = AppDefaultLocation + '/' + BackupDirDefaultName + '/' + fileName
    with open(file, newline='', encoding='ISO-8859-1') as fin:
        reader = csv.reader(fin)
        for row in reader:
            query = '''
            insert into LoginAccess (uniqueID,loginName,password,access_Level,golferID,deleted,lastModified,updaterID)
            values (?, ?, ?, ?, ?, ?, ?, ?)'''
            AppCRUD.cursor.execute(query, (row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7]))
    AppCRUD.closeDB()
    return (len(getLoginUsers()) - int(1)) # do not count root

def getLoginUser(loginName):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''
    select password, access_Level, uniqueID from LoginAccess where loginName = "{0}" '''.format(loginName)
    ans = AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()
    return ans

def getLoginUserGolferID(loginName):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''
    select golferID from LoginAccess where loginName = "{0}" '''.format(loginName)
    ans = AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()
    return ans[0][0]

def getGolferFullNamefromLoginID(uniqueID):
    AppCRUD.initDB(getDBLocationFullName())
    if uniqueID != 1:
        query = '''
                    select golferID from LoginAccess where uniqueID = {0}
                '''.format(uniqueID)
        ans = AppCRUD.cursor.execute(query).fetchall()
        golferID = ans[0][0]

    AppCRUD.closeDB()
    if uniqueID == 1:
        return "Super User"
    else:
        golferFullName = getGolferFullname(golferID)
        return golferFullName

def getLoginUsers():
    AppCRUD.initDB(getDBLocationFullName())
    query = '''
    select loginName, password, access_Level, uniqueID from LoginAccess'''
    data = AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()
    return data
