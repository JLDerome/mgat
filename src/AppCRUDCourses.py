import sqlite3 as sql
import os
import csv
import AppCRUD as AppCRUD
from AppProc import getDBLocationFullName, convertDateStringToOrdinal
from AppConstants import *


db=None
cursor = None
lidGolfers = 0

#CREATE TABLE Courses(
#   uniqueID INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
#   courseNAME          CHAR(60)    NOT NULL,
#   courseSNAME         CHAR(30)    NOT NULL,
#   courseLogo          CHAR(100)
#   established         INTEGER,
#   deleted             CHAR(1),
#   lastModified INTEGER,
#   updaterID INTEGER
#);

def getCoursesDictionaryInfo():
    #
    #
    #
    AppCRUD.initDB(getDBLocationFullName())
    query = '''
                    select uniqueID, courseName from Courses
                           ORDER by courseName
            '''
    temp = AppCRUD.cursor.execute(query).fetchall()
    returnList = []
    for i in range(len(temp)):
        newItem = (temp[i][0], temp[i][1])
        returnList.append(newItem)
    AppCRUD.closeDB()
    return returnList

def findCourses(courseNAME, courseSNAME, established, NoClose = False):
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    if len(courseNAME) == 0:
        courseNAMEModified = "%%"
    else:
        courseNAMEModified = courseNAME

    if len(courseSNAME) == 0:
        courseSNAMEModified = "%%"
    else:
        courseSNAMEModified = courseNAME

    if len(established) == 0:
        establishedModified = "%%"
    else:
        establishedModified = courseNAME

    query = '''
                select * from Courses where courseNAME like "{0}" and
                                            courseSNAME like "{1}" and
                                            established like "{2}"
                                      ORDER by courseNAME
            '''.format(courseNAMEModified, courseSNAMEModified, establishedModified)

    temp = AppCRUD.cursor.execute(query).fetchall()

    if NoClose == False:
        AppCRUD.closeDB()

    return temp


# ID,
#                         self.selectedLongCourseName.get(),
#                         self.selectedShortCourseName.get(),
#                         self.selectedPicture.get(),
#                         self.selectedEstablishedYear.get(),

def findIfEditCreatesCourseDuplicate(CurrentID, courseName, establishedYear, Noclose=False):
    if Noclose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''
                select uniqueID from Courses where courseName="{0}" and
                                                   established ="{1}" and uniqueID != {2}
            '''.format(courseName, establishedYear, CurrentID)

    ans = AppCRUD.cursor.execute(query).fetchall()

    if Noclose == False:
        AppCRUD.closeDB()

    if len(ans) > 0:
        return True
    else:
        return False

def findCourseIDClosestFromCourseName(nameValue, NoClose = False):
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    found = False
    searchedName = nameValue
    while found == False:
        query = '''
                    select uniqueID from Courses where courseNAME like "%{0}%" COLLATE NOCASE
                '''.format(searchedName)
        temp = AppCRUD.cursor.execute(query).fetchall()
        if len(temp) > 0:
            found = True
        else: # Remove last 2 characters to find closest match
            sizeOfSearchedName = len(searchedName)
            if sizeOfSearchedName < 3:
                break
            else:
                searchedName = searchedName[:(sizeOfSearchedName-2)]

    if NoClose == False:
        AppCRUD.closeDB()

    if len(temp) > 0:
        return temp[0][0]
    else:
        return None

def find_course(nameValue, sNameValue):
    AppCRUD.initDB(getDBLocationFullName())
    if len(nameValue) > 0 and len(sNameValue):
        query = '''
            select * from Courses where courseNAME like '{0}' or courseSNAME like '{1}' '''.format(nameValue, sNameValue)
    elif len(nameValue) > 0:
        query = '''
            select * from Courses where courseNAME like '{0}' '''.format(nameValue)
    elif len(sNameValue) > 0:
        query = '''
            select * from Courses where courseSNAME like '{0}' '''.format(sNameValue)
    return AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()

def findCourseDuplicate(LongCourseName, ShortCourseName):
    AppCRUD.initDB(getDBLocationFullName())
    query = ''' select uniqueID from Courses where courseNAME='{0}' or courseSNAME='{1}'
            '''.format(LongCourseName, ShortCourseName)
    ans = AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()
    if len(ans) > 0:
        return ans[0][0]
    else:
        return None

def getCourseID(LongCourseName, ShortCourseName, EstablishedYear,
                lastModified):
    AppCRUD.initDB(getDBLocationFullName())
    query = ''' select uniqueID from Courses where courseNAME='{0}' and courseSNAME='{1}' and established='{2}' and
                                     lastModified={3} '''.format(LongCourseName, ShortCourseName,
                                                                EstablishedYear, lastModified)
    ans = AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()
    if len(ans) > 0:
        return ans[0][0]
    else:
        return None

def insert_courses(courseNAME, courseSNAME, courseLogo, established, deleted,
                   last_modified, updaterID):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''
    insert into Courses (courseNAME, courseSNAME, courseLogo, established, deleted,
                   lastModified, updaterID)
    values (?, ?, ?, ?, ?, ? ,?)'''
    ans = AppCRUD.cursor.execute(query, (courseNAME,courseSNAME, courseLogo, established, deleted,
                                   last_modified, updaterID))
    AppCRUD.closeDB()
    print("Add Course ans: ", ans)

def importCSV_courses(fileName):
    AppCRUD.initDB(getDBLocationFullName())
    file = AppDefaultLocation + '/' + BackupDirDefaultName + '/' + fileName
    with open(file, 'r', encoding='ISO-8859-1') as fin:
        reader = csv.reader(fin)
        for row in reader:
            # row[4] must be a valid uniqueID
            query = '''
            insert into Courses (uniqueID, courseNAME, courseSNAME, courseLogo,
                                 established, deleted, lastModified,updaterID)
            values (?, ?, ?, ?, ?, ?, ?, ?)'''
            AppCRUD.cursor.execute(query, (row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7]))
    AppCRUD.closeDB()
    return len(get_courses())

def get_course_sname(NoClose, uniqueID):
    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())

    query = '''select courseSNAME from Courses where uniqueID={0}'''.format(uniqueID)
    temp = AppCRUD.cursor.execute(query).fetchall()

    if NoClose == False:
        AppCRUD.initDB(getDBLocationFullName())
        return temp[0][0]
    else:
        return temp[0][0]

def get_course_snameMASS(uniqueID):

    query = '''select courseSNAME from Courses where uniqueID={0}'''.format(uniqueID)
    return AppCRUD.cursor.execute(query).fetchall()

def get_courses():
    AppCRUD.initDB(getDBLocationFullName())
    query = '''
    select * from Courses ORDER BY courseNAME'''
    ans = AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()
    return ans

def getCourseCB():
    AppCRUD.initDB(getDBLocationFullName())
    query = '''
    select uniqueID, courseSNAME
    from Courses ORDER BY courseSNAME'''
    return AppCRUD.cursor.execute(query).fetchall()
    AppCRUD.closeDB()

def get_course_details(idCourse):
    AppCRUD.initDB(getDBLocationFullName())
    query = ''' 
    select * from Courses
    where uniqueID = ?'''
    return AppCRUD.cursor.execute(query, (idCourse,)).fetchall()[0]
    AppCRUD.closeDB()

def get_course_name(idCourse):
    return get_course_details(idCourse)[0]

def update_courses(idCourse, Name, sName, courseLogo, established, deleted,
                   last_modified, updaterID):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''
    update Courses set courseNAME=?, courseSNAME=?, courseLogo=?,
                       established=?, deleted=?, lastModified=?,
                       updaterID=?
    where uniqueID = ?'''
    AppCRUD.cursor.execute(query, (Name, sName, courseLogo, established,
                                   deleted, last_modified, updaterID, idCourse))
    AppCRUD.closeDB()

def delete_course(idCourse):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''
    delete from Courses
    where uniqueID = ?'''
    AppCRUD.cursor.execute(query,(idCourse,))
    #
    # Make all associated Tees are also deleted
    #
    query = '''
    delete from Tees
    where uniqueID = ?'''
    AppCRUD.cursor.execute(query,(idCourse,))
    AppCRUD.closeDB()

def exportCSV_courses(fileName):
    AppCRUD.initDB(getDBLocationFullName())
    query = '''
    select * from Courses ORDER BY courseNAME'''
    data = AppCRUD.cursor.execute(query).fetchall()
    file = AppDefaultLocation + '/' + BackupDirDefaultName +'/'+ fileName
    with open(file, 'w', newline='', encoding='ISO-8859-1') as fout:
        for i in range(len(data)):
            csv.writer(fout).writerow(data[i])
    AppCRUD.closeDB()
    return len(data)

def importCoursesCSV_MyGolf2015(fileName):
    AppCRUD.initDB(getDBLocationFullName())
    with open(fileName, newline='', encoding='ISO-8859-1') as fin:
        reader = csv.reader(fin)
        for row in reader:
            try:
                separated = row[3].split("/")
                filename = separated[len(separated)-1]
            except:
                filename = " "
            query = '''
               insert into Courses (uniqueID,courseNAME,courseSNAME,courseLogo,
                                    established ,deleted, lastModified,updaterID)
                           values (?, ?, ?, ?, ?, ?, ?, ?)'''
            AppCRUD.cursor.execute(query, (row[0], row[1], row[2], filename, None, 'N',
                                           convertDateStringToOrdinal(todayDate), '1'))
    AppCRUD.closeDB()
    return len(get_courses())
