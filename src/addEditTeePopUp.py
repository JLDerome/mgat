from popUpWindowClass import popUpWindowClass
from AppClasses import *
from AppConstants import *
from AppProc import convertDateStringToOrdinal, updateMyAppConstant
from AppProc import getAYearFromOrdinal
from AppProc import getDBLocationFullName
import AppCRUDGolfers as CRUDGolfers
import AppCRUDCourses as CRUDCourses
import AppCRUDTees as CRUDTees
import AppCRUD as CRUD
import AppCRUDExpenses as CRUDExpenses
from AppDialogClass import AppRecordDuplication, AppDisplayMessage

class addEditTeePopUp(popUpWindowClass):
    def __init__(self, aWindow, aCloseCommand, mode, loginData, anUpdateUponReturnMethod, teeID=None, courseID=None):
        #
        #  loginInfo format: Updater Full Name, Login ID, updaterID
        #
        if mode == 'add':
            self.windowTitle = 'Add a New Tee'
        elif mode == 'edit':
            self.windowTitle = 'Edit an Existing Tee'
        elif mode == 'view':
            self.windowTitle = 'View Tee'
        elif mode == 'duplicate':
            self.windowTitle = 'Duplicate Tee'
        else:
            self.windowTitle = 'An Error Occur in the Call'
        popUpWindowClass.__init__(self, aWindow, aCloseCommand, self.windowTitle, loginData, mode)
        self.tableName = 'Tees'
        self.aCloseCommand = aCloseCommand
        self.aWindow = aWindow
        self.loginData = loginData
        self.teeIDForEdit = teeID
        self.courseIDatStartUp = courseID
        self.anUpdateUponReturnMethod = anUpdateUponReturnMethod # Edit is a reload of the expenses, Add is not assigned
        #
        #  loginInfo format: Updater Full Name, access_Level, loginID
        #
        self.updaterFullname = loginData[0]
        self.updaterID = self.loginData[2]
        self.displayCurUser.setUserName(self.updaterFullname)

        #######################################################################################
        # Inherited all from tableCommanWindowClass
        #
        #  This class intents to change the mainArea only.  Everything else must remain common
        #
        self.mainAreaRowCount = 0
        self.mainAreaColumnCount = 0
        self._reDisplayRecordFrame()
        self.mainArea.rowconfigure(self.mainAreaRowCount, weight=1)
        if self.openningMode == 'add':
            self.addCommand(None)
            if self.courseIDatStartUp != None:
                self.selectedCourse.load(self.courseCBData.getName(self.courseIDatStartUp))
                self.selectedCourse.disable()
                self.enableTeeInfoFields()

        if self.openningMode == 'edit':
            self.clearButton.grid_remove()
            newTitle = "Edit {0} {1} Tee".format(CRUDCourses.get_course_sname(False, self.courseIDatStartUp),
                                                 CRUDTees.get_full_tee_name_from_teeID(self.teeIDForEdit))
            self.windowTitleLabel.configure(text=newTitle)
            self.editCommand(None)

        if self.openningMode == 'view':
            self.clearButton.grid_remove()
            self.submitButton.grid_remove()
            newTitle = "View {0} {1} Tee".format(CRUDCourses.get_course_sname(False, self.courseIDatStartUp),
                                                 CRUDTees.get_full_tee_name_from_teeID(self.teeIDForEdit))
            self.windowTitleLabel.configure(text=newTitle)
            self.viewCommand()

        if self.openningMode == 'duplicate':
            self.clearButton.grid_remove()
            newTitle = "New Tee from {0} {1} Tee".format(CRUDCourses.get_course_sname(False, self.courseIDatStartUp),
                                                                 CRUDTees.get_full_tee_name_from_teeID(self.teeIDForEdit))
            self.windowTitleLabel.configure(text=newTitle)
            self.duplicateCommand(None)

    def add(self, *args):
        self.fieldsDisable()
        self.submitButton.disable()
        self.selectedCourse.enable()
        self.selectedCourse.focus()
        self.dirty = False

    def view(self, *args):
        self.fieldsDisable()
        self.selectedCourse.load(self.courseCBData.getName(self.courseIDatStartUp))
        teeData = CRUDTees.get_tee_info_for_teeID(self.teeIDForEdit)
        self.selectedTeeName.load(teeData[0][1])
        self.selectedTeeType.load(teeData[0][2])
        self.selectedTeeRATING.load(teeData[0][3])
        self.selectedTeeSlope.load(teeData[0][4])
        self.populateTeeDetails(self.teeIDForEdit)
        self.myMsgBar.clearMessage()
        self.dirty = False

    def edit(self, *args):
        self.fieldsDisable()
        self.selectedCourse.load(self.courseCBData.getName(self.courseIDatStartUp))
        teeData = CRUDTees.get_tee_info_for_teeID(self.teeIDForEdit)
        self.selectedTeeName.load(teeData[0][1])
        self.selectedTeeType.load(teeData[0][2])
        self.selectedTeeRATING.load(teeData[0][3])
        self.selectedTeeSlope.load(teeData[0][4])
        self.enableTeeInfoFields()
        self.enableTeeDetailsTable()
        self.populateTeeDetails(self.teeIDForEdit)
        self.myMsgBar.clearMessage()
        self.lastUpdate.load(teeData[0][8],
                             teeData[0][9])
        self.deletedV.set(teeData[0][7])
        self.dirty = False

    def duplicate(self, *args):
        self.fieldsDisable()
        self.selectedCourse.load(self.courseCBData.getName(self.courseIDatStartUp))
        teeData = CRUDTees.get_tee_info_for_teeID(self.teeIDForEdit)
        self.enableTeeInfoFields()

        self.selectedTeeName.clear()
        self.selectedTeeType.clear()
        self.selectedTeeRATING.clear()
        self.selectedTeeSlope.clear()

        self.populateTeeDetails(self.teeIDForEdit)
#        self.myMsgBar.clearMessage()
        self.lastUpdate.load(teeData[0][8],
                             teeData[0][9])
        self.deletedV.set(teeData[0][7])
        self.dirty = False

    def populateTeeDetails(self, teeID):
        teeDetails = CRUDTees.get_teeDetails_for_curTee(teeID)
        self.teeDetailsTable.populateTable(teeDetails)

    def _submitForm(self, *args):
        if self.validateRequiredFields() == True:
            self.save()
        # Must be defined in the popUp window.

    def _clearForm(self, *args):
        if self.openningMode == 'add':
            self.fieldsClear()
            self.add()
        # Must be defined in the popUp window.


    def tableSpecificMenu(self):
        pass
        '''
        self.viewsmenu.add_command
        self.viewsmenu.add_command(label="Active Golfers", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                   command=lambda viewID='Active Golfers': self.basicViewsList(viewID))

        sortsmenu = Menu(self.menubar, foreground=AppDefaultForeground, font=AppDefaultMenuFont, tearoff=0)
        sortsmenu.add_command(label="Default", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Default': self.sortingList(sortID))
        sortsmenu.add_command(label="Last Name", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Last Name': self.sortingList(sortID))
        sortsmenu.add_command(label="First Name", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='First Name': self.sortingList(sortID))
        sortsmenu.add_command(label="Birthday", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                              command=lambda sortID='Birthday': self.sortingList(sortID))
        #
        # Next Command actually puts the menu up
        #
        self.menubar.add_cascade(label="Sorts", foreground=AppDefaultForeground, font=AppDefaultMenuFont,
                                 menu=sortsmenu)
        '''

    def sort(self):
        pass
        '''
        if self.curOp == 'default':
            currentID = self.recList[self.curRecNumV.get()][0]
            if self.sortID.get() == 'Default':
                self.recList = sorted(self.recList, key=operator.itemgetter(2,1))
            elif self.sortID.get() == 'Last Name':
                self.recList.sort(key=lambda row: row[2])
            elif self.sortID.get() == 'First Name':
                self.recList.sort(key=lambda row: row[1])
            elif self.sortID.get() == 'Birthday':
                self.recList.sort(key=lambda row: row[4])
            else:
                self.recList.sort(key=lambda row: row[0])
            newIdx = getIndex(currentID, self.recList)
            self.curRecNumV.set(newIdx)
            self.displayRec()
        else:
            aMsg = "ERROR: Invalid request. Must be executed in default state only."
            self.myMsgBar.newMessage('error', aMsg)
        '''

    def _descriptionEnteredAction(self):
        pass

    def _buildWindowsFields(self, aFrame):
        #        headerList1 = ('BASIC','VALUE','VALUED','MULT','TOLE','SIZE','DESCR','NSN1','NSN2','NSN3','NSN4')
        colTotal = 2
        self.fieldsFrame = AppFieldsFrame(aFrame, colTotal)
        self.fieldsFrame.grid(row=0, column=0, sticky=N + S + E + W)
        ########################################################### Main Identification Section

        # CREATE TABLE Tees(
        #        uniqueID INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
        #        teeNAME         CHAR(15)    NOT NULL,
        #        teeType         CHAR(8)     NOT NULL,
        #        teeRATING       REAL        NOT NULL,
        #        teeSLOPE        SMALLINT    NOT NULL,
        #        courseID        INTEGER     NOT NULL,
        #        archived        CHAR(1),
        #        deleted         CHAR(1),
        #        lastModified INTEGER,
        #        updaterID INTEGER
        #        FOREIGN KEY(courseID) REFERENCES Courses(courseID) ON DELETE CASCADE
        #       );

        self.courseSelectionFrame = AppBorderFrame(self.fieldsFrame, 1)
        self.courseSelectionFrame.grid(row= self.fieldsFrame.row, column = self.fieldsFrame.column,
                                       columnspan = self.fieldsFrame.columnTotal, sticky = E+W+S+N)
        self.courseSelectionFrame.addTitle("Course Information")

        self.courseCBData = AppCBList(CRUDCourses.getCoursesDictionaryInfo())
        self.selectedCourse = AppSearchLB(self.courseSelectionFrame, self, 'Select a Golf Course', 'H',
                                          self.courseCBData.getList(), courseLongNameWidth, None, self.myMsgBar,
                                          self._aDirtyMethod, self.enableTeeInfoFields)
        self.selectedCourse.grid(row=self.courseSelectionFrame.row, column=self.courseSelectionFrame.column)

        self.fieldsFrame.addRow()
        self.teeInfoFrame = AppBorderFrame(self.fieldsFrame, 2)
        self.teeInfoFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column,
                               columnspan=self.fieldsFrame.columnTotal, sticky=E + W + N + S)
        self.teeInfoFrame.addTitle("Tee Information")

        self.selectedTeeName = AppFieldEntry(self.teeInfoFrame,'Enter Tee Name', 'H', courseTeeNameWidth,
                                             self._aDirtyMethod)
        self.selectedTeeName.grid(row=self.teeInfoFrame.row, column=self.teeInfoFrame.column)

        self.teeInfoFrame.addColumn()

        self.column2Data = AppColumnAlignFieldFrame(self.teeInfoFrame)
        self.column2Data.grid(row = self.teeInfoFrame.row, column = self.teeInfoFrame.column,
                              sticky = E+W+N+S)
        self.column2Data.noBorder()

        self.selectedTeeType = AppSearchLB(self.column2Data, self, None, None,
                                           teeTypeList, courseTeeTypeWidth, None, self.myMsgBar,
                                           self._aDirtyMethod, self.enableTeeDetailsTable)
        self.column2Data.addNewField('Tee Type', self.selectedTeeType)

        self.selectedTeeSlope = AppFieldEntryInteger(self.column2Data,None,None,courseTeeSlopeWidth,self._aDirtyMethod,
                                                     self.enableTeeDetailsTable)
        self.selectedTeeSlope.addValidationAmount(3, self.myMsgBar)
        self.column2Data.addNewField('Tee Slope', self.selectedTeeSlope)

        self.selectedTeeRATING = AppFieldEntryFloat(self.column2Data,None,None,courseTeeRatingWidth,self._aDirtyMethod,
                                                    self.enableTeeDetailsTable)
        self.selectedTeeRATING.addValidationAmount(1,3, self.myMsgBar)
        self.selectedTeeRATING.setAsFloat()
        self.column2Data.addNewField('Tee Rating', self.selectedTeeRATING)

        self.fieldsFrame.addRow()
        self.fieldsFrame.stretchCurrentRow()
        self.teeDetailsFrame = AppBorderFrame(self.fieldsFrame, 1)
        self.teeDetailsFrame.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column,
                                  columnspan=self.fieldsFrame.columnTotal, sticky=E + W + S + N)
        self.teeDetailsFrame.addTitle("Tee Details")
        self.teeDetailsFrame.stretchCurrentRow()

        self.teeDetailsTable = AppTeeDetailsTable(self.teeDetailsFrame, self.myMsgBar)
        self.teeDetailsTable.grid(row=self.teeDetailsFrame.row, column=self.teeDetailsFrame.column,
                                  sticky=E + N + W + S)

        ########################################################### Footer Section
        self.fieldsFrame.addRow()
        self.lastUpdate = AppLastUpdateFormat(self.fieldsFrame)
        self.lastUpdate.grid(row=self.fieldsFrame.row, column=self.fieldsFrame.column, columnspan=colTotal,
                             sticky=E + N + S)

        self.setRequiredFields()
        self.fieldsDisable()

    def enableTeeDetailsTable(self, *args):
        if self.teeDetailsTable.tableEnabled == False:
            enableTeeDetailsTableFields = True
            if len(self.selectedCourse.get()) == 0:
                enableTeeDetailsTableFields = False

            if len(self.selectedTeeName.get()) == 0:
                enableTeeDetailsTableFields = False

            if len(self.selectedTeeType.get()) == 0:
                enableTeeDetailsTableFields = False

            if len(self.selectedTeeSlope.getAsString()) == 0:
                enableTeeDetailsTableFields = False

            if len(self.selectedTeeRATING.getAsString()) == 0 or float(self.selectedTeeRATING.getAsString()) == float(0):
                enableTeeDetailsTableFields = False

            if enableTeeDetailsTableFields == True:
                self.teeDetailsTable.enable()
                aMsg = 'Tee Details Table is enabled.'
                self.myMsgBar.newMessage('info', aMsg)
                self.submitButton.enable()
                self.teeDetailsTable.holeHandicap[0].focus()

    def enableTeeInfoFields(self, *args):
        try:
            anID = self.courseCBData.getID(self.selectedCourse.get())
            if len(self.selectedCourse.get()) > 0:
    #            self.selectedCourse.disable()
                self.selectedTeeName.enable()
                self.selectedTeeType.enable()
                self.selectedTeeRATING.enable()
                self.selectedTeeSlope.enable()
                self.selectedTeeName.focus()
                aMsg = 'All fields in {0} are required before the Tee Details Table is enabled.'.format(AppDefaultRequiredFieldBackground)
                self.myMsgBar.newMessage('warning', aMsg)
        except:
            aMsg = 'ERROR: Select the course from the drop down list only.'
            self.myMsgBar.newMessage('error', aMsg)

    def fieldsDisable(self):
        self.selectedCourse.disable()
        self.selectedTeeName.disable()
        self.selectedTeeType.disable()
        self.selectedTeeRATING.disable()
        self.selectedTeeSlope.disable()
        self.teeDetailsTable.disable()

    def fieldsEnable(self):
        self.selectedCourse.enable()
        self.selectedTeeName.enable()
        self.selectedTeeType.enable()
        self.selectedTeeRATING.enable()
        self.selectedTeeSlope.enable()
        self.teeDetailsTable.enable()

    def fieldsClear(self):
        self.selectedCourse.clear()
        self.selectedTeeName.clear()
        self.selectedTeeType.clear()
        self.selectedTeeRATING.clear()
        self.selectedTeeSlope.clear()
        self.teeDetailsTable.clear()

    def setRequiredFields(self):
        # Table specific.  Will be defined in calling class
        self.selectedCourse.setAsRequiredField()
        self.selectedTeeName.setAsRequiredField()
        self.selectedTeeType.setAsRequiredField()
        self.selectedTeeRATING.setAsRequiredField()
        self.selectedTeeSlope.setAsRequiredField()


    def resetRequiredFields(self):
        # Table specific.  Will be defined in calling class
        self.selectedCourse.resetAsDeleted()
        self.selectedTeeName.resetAsDeleted()
        self.selectedTeeType.resetAsRequiredField()
        self.selectedTeeRATING.resetAsRequiredField()
        self.selectedTeeSlope.resetAsRequiredField()


    def validateRequiredFields(self):
        requiredFieldsEntered = True

        if len(self.selectedCourse.get()) == 0:
            requiredFieldsEntered = False
            self.selectedCourse.focus()

        elif len(self.selectedTeeName.get()) == 0:
            requiredFieldsEntered = False
            self.selectedTeeName.focus()

        elif len(self.selectedTeeType.get()) == 0:
            requiredFieldsEntered = False
            self.selectedTeeType.focus()

        elif len(self.selectedTeeSlope.getAsString()) == 0:
            requiredFieldsEntered = False
            self.selectedTeeSlope.focus()

        elif len(self.selectedTeeRATING.getAsString()) == 0:
            requiredFieldsEntered = False
            self.selectedTeeRATING.focus()

        elif self.teeDetailsTable.verifyRequiredFieldsTeeDetails() == False:
            self.myMsgBar.requiredTeeDetailsMessage()
            return False

        if requiredFieldsEntered == False:
            self.myMsgBar.requiredFieldMessage()
        return requiredFieldsEntered

    def save(self):
        if self.openningMode == 'add' or self.openningMode == 'duplicate':
            #
            #  First must established that tee does not exist already
            #
            CRUD.initDB(getDBLocationFullName())
            if CRUDTees.findDuplicateTeeForCourse(True, self.courseCBData.getID(self.selectedCourse.get()),
                                                  self.selectedTeeName.get(),
                                                  self.selectedTeeType.get()):

                aTitle = 'Tee Record Duplication'
                aMsg = 'The {0}-{2} Tee for course {1} already exist.\n Archive current' \
                       ' version or change the Tee name.'.format(self.selectedTeeName.get(),
                                                                 CRUDCourses.get_course_sname(True, self.courseIDatStartUp),
                                                                 self.selectedTeeType.get())
                AppRecordDuplication(self, aTitle, aMsg)

                CRUD.closeDB()
            else:
                #  Start the saving process
                ID = CRUDTees.insert_tees(True,self.selectedTeeName.get(),self.selectedTeeType.get(),
                                          self.selectedTeeRATING.get(), self.selectedTeeSlope.get(),
                                          self.courseCBData.getID(self.selectedCourse.get()),
                                          self.updaterID)

                for i in range(18):
                    CRUDTees.insert_teeDetails(True,
                                               self.teeDetailsTable.holeYardage[i].get(),
                                               self.teeDetailsTable.holeHandicap[i].get(),
                                               self.teeDetailsTable.holePar[i].get(),
                                               ID, self.updaterID
                                               )

                CRUD.closeDB()
                aMsg = 'A new Tee({0}-{2}) for course {1} has been added.'.format(self.selectedTeeName.get(),
                                                                                  self.selectedCourse.get(),
                                                                                  self.selectedTeeType.get())
                self.myMsgBar.newMessage('info', aMsg)
                if self.anUpdateUponReturnMethod != None:
                    self.anUpdateUponReturnMethod()
                if self.openningMode=='duplicate':
                    self.teeDetailsTable.clear()
                    self.duplicateCommand(None)
                else:
                    self.addCommand(None)  # Get ready to add an another Tee


        elif self.openningMode == 'edit':
            pass
            #
            #  First must established that tee does not create a Duplicate
            #
            CRUD.initDB(getDBLocationFullName())
            if CRUDTees.findIfCreatingADuplicateTeeForCourse(True, self.courseCBData.getID(self.selectedCourse.get()),
                                                             self.selectedTeeName.get(),
                                                             self.selectedTeeType.get(),
                                                             self.teeIDForEdit):
                aTitle = 'Tee Record Duplication'
                aMsg = "The Tee name {0} - {1} for course {2} already exist.\n Archived" \
                       " the current Tee OR change the Tee name.".format(self.selectedTeeName.get(),
                                                                          self.selectedTeeType.get() ,
                                                                          CRUDCourses.get_course_sname(True, self.courseIDatStartUp))
                AppRecordDuplication(self, aTitle, aMsg)
                CRUD.closeDB()
            else:
                CRUDTees.update_tees(True, self.teeIDForEdit, self.selectedTeeName.get(), self.selectedTeeType.get(),
                                     self.selectedTeeRATING.get(), self.selectedTeeSlope.get(),
                                     self.courseCBData.getID(self.selectedCourse.get()),self.updaterID)

                # cursor.execute('''CREATE TABLE TeeDetails(
                #   uniqueID  INTEGER PRIMARY KEY AUTOINCREMENT    NOT NULL,
                #   teeYardage   SMALLINT    NOT NULL,
                #   teeHandicap  SMALLINT    NOT NULL,
                #   teePar       SMALLINT    NOT NULL,
                #   teeID        INTEGER    NOT NULL,
                #   archived        CHAR(1),
                #   deleted              CHAR(1),
                #   lastModified         INTEGER,
                #   updaterID            INTEGER,

                listTeeDetailsUniqueID = CRUDTees.get_teeDetailsIDs_for_Tee(True, self.teeIDForEdit)
                for i in range(len(listTeeDetailsUniqueID)):
                    CRUDTees.update_teeDetails( True, listTeeDetailsUniqueID[i][0],
                                                self.teeDetailsTable.holeYardage[i].get(),
                                                self.teeDetailsTable.holeHandicap[i].get(),
                                                self.teeDetailsTable.holePar[i].get(),
                                                self.teeIDForEdit, self.updaterID
                                               )

                aMsg = 'Tee {0}-{2} for course {1} has been updated.'.format(self.selectedTeeName.get(),
                                                                             CRUDCourses.get_course_sname(True,self.courseIDatStartUp),
                                                                             self.selectedTeeType.get())
                CRUD.closeDB()
                if self.anUpdateUponReturnMethod != None:
                    self.anUpdateUponReturnMethod()
                AppDisplayMessage(self, "Tee Updated", aMsg)
                self.dirty = False
                self._exitForm()



